# DATABASE UPDATES

### attention : le module [`iiif`](./src/iiif.py) n'a jamais été terminé et ne fonctionne pas.

Après la création de la base de données initiale, il est nécessaire de faire 
de nouveaux ajouts et des petites modifications (en partie car des étapes demandent
à la base de données d'être déjà créée, comme la production de manifestes; ou bien 
car de nouveaux besoins sont apparus et qu'il est plus simple de faire des mises à 
jour plutôt que de complexifier plus les étapes [`1_data_preparation`](https://gitlab.inha.fr/snr/rich.data/data-preparation) 
et [`2_data2sql`](https://gitlab.inha.fr/snr/rich.data/data2sql) de migration des données).

Ces mises à jour sont possibles via un CLI, chaque mise à jour est permise
via commande du CLI. Les mises à jour ne mettent pas directement à jour la BDD,
mais génèrent des scripts SQL qu'il faut ensuite appliquer manuellement sur la BDD
(parce qu'on ne peut pas directement lancer une MAJ sur la BDD de prod sans être en SSH).

---

## Updates

Les mises à jour permises sont :

- tables `theme` et `named_entity` : création des colonnes `category` et `category_slug`,
  qui pemettent le classement des thèmes et entités nommées en catégories
- table `title` : nettoyage de la colonne `entry_name`
- table `iconography` : mise à jour de la colonne `iiif_url` avec l'ajout des manifestes manquants.

---

## Utilisation

Pour se connecter à la base de données, avoir un fichier `./confidentials/postgresql_credentials.json`
avec la structure suivante:

```python
{
  "username": "...",
  "password": "...",
  "uri": "...",
  "db": "..."
}
```

Pour utiliser, lancer les commandes:

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python main.py --help  # voir l'aide du CLI pour l'utilisation
```

---
