# import shutil
# import os

import click

from src.category import pipeline as category_pipeline
from src.title import pipeline as title_pipeline
from src.utils.constants import OUT
from src.utils.engine import build_engine

@click.group()
@click.option("-s", "--silentsql", help="don't echo SQL queries", is_flag=True)
@click.pass_context
def cli(ctx, silentsql):
    """
    a command line interface to run various cleanup and update routines
    on the richelieu database. all scripts generate SQL scripts which
    can be used to update local and remote clones of the database (rather
    than directly modifying the database)
    """
    ctx.ensure_object(dict)
    ctx.obj["SQL_ECHO"] = False if silentsql else True
    ctx.obj["ENGINE"] = build_engine(sql_echo=ctx.obj["SQL_ECHO"])

    click.echo("* db_updates cli is running")
    if silentsql:
        click.echo("* sql output is silent")


@cli.command()
@click.pass_context
def category(ctx):
    """
    add categories to `theme` and `named_entity` database tables

    generates an SQL script which will create and populate 2 new
    columns for the tables `theme` and `named_entity`:
        - category      (Text): the human readable category name
        - category_slug (Text): a slugified, url-safe version of the category name, to build urls

    theme/named entity to category mapper is predefined in `in/`
    and hard-coded in `src/category/read_and_prepare()`.
    """
    print("* generating scripts to create and populate `category` and `category_slug` columns for tables `theme` and `named_entity`")
    category_pipeline(engine=ctx.obj["ENGINE"])
    print("* sql scripts generated and written to `out/` !")


@cli.command()
@click.pass_context
def title(ctx):
    """
    clean the `title.entry_name` column

    somehow, in this column, titles have not been stripped of
    leading/trailing spaces. here, we generate an sql script
    which will update titles to their stripped version.

    this process requires no manual input.
    """
    print("* cleaning the `title.entry_name` column")
    title_pipeline(engine=ctx.obj["ENGINE"])
    print("* sql scripts generated and written to `out/` !")


if __name__ == "__main__":
    cli()
