-- 1st transaction: create column
 BEGIN;


ALTER TABLE IF EXISTS theme ADD COLUMN IF NOT EXISTS category TEXT, ADD COLUMN IF NOT EXISTS category_slug TEXT;


COMMIT;

-- 2nd transaction: import data
 BEGIN;

UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr17eaf451869db4d64b5d9942651d7ed5b';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr17eaf451869db4d64b5d9942651d7ed5b';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr172497300c70d42b08b576c5a0ba5ae0a';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr172497300c70d42b08b576c5a0ba5ae0a';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr1e64f3432d53947078327dda486945fe7';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr1e64f3432d53947078327dda486945fe7';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1c3ea8a465de347579a3796e1b1601ec2';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1c3ea8a465de347579a3796e1b1601ec2';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1d8e1fd8dc42e4c58b4fd7615800fea19';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1d8e1fd8dc42e4c58b4fd7615800fea19';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr18fd23c3d4abf44349bbddc372dbf38c0';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr18fd23c3d4abf44349bbddc372dbf38c0';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1bc9332b2db5f45ee9dec8433307f8e7f';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1bc9332b2db5f45ee9dec8433307f8e7f';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr15e227dac15a4446d8b8b18a7e3213d8e';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr15e227dac15a4446d8b8b18a7e3213d8e';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr186bb021ce8dd406a873d35dc90fa7089';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr186bb021ce8dd406a873d35dc90fa7089';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1ad5e330413f5489aa8b8e6e700343550';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1ad5e330413f5489aa8b8e6e700343550';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1b56ce7199bc94c7db5cfbcbb2849220d';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1b56ce7199bc94c7db5cfbcbb2849220d';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr1826f3518eecc49a4a030c9520c051b81';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr1826f3518eecc49a4a030c9520c051b81';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr1a5a5ddd859b4463f96c2ec2e7ba23686';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr1a5a5ddd859b4463f96c2ec2e7ba23686';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr192b3c1a07807434590759c02605ada70';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr192b3c1a07807434590759c02605ada70';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1df2f987f702840baae856e9716d7928b';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1df2f987f702840baae856e9716d7928b';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr119ea3994f787479095683b7da89ccd69';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr119ea3994f787479095683b7da89ccd69';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1ce0a74ab786b4b52832f0abd0c9de4ce';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1ce0a74ab786b4b52832f0abd0c9de4ce';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr13dd95c7db88e40d1aac9c97b7b627800';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr13dd95c7db88e40d1aac9c97b7b627800';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr1ccb97047d3914dd1bab4b96c8eb65b04';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr1ccb97047d3914dd1bab4b96c8eb65b04';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr154e722ea68b64c3690d018b6ec33fc7b';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr154e722ea68b64c3690d018b6ec33fc7b';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr1b7ee097b3a27463bbdb2177f611493d6';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr1b7ee097b3a27463bbdb2177f611493d6';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr12aa2a01c1f3c4aca8d3bc878d672581a';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr12aa2a01c1f3c4aca8d3bc878d672581a';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr1edc5302c35cf4dafa2513ac5b671643a';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr1edc5302c35cf4dafa2513ac5b671643a';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr1d77c397fb44746cc9884f76efda6014d';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr1d77c397fb44746cc9884f76efda6014d';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1c04ad2f11fa54e269cecf22532dbb78f';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1c04ad2f11fa54e269cecf22532dbb78f';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr1ccc4169a977f41a995771d81522941e2';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr1ccc4169a977f41a995771d81522941e2';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1566b79275c0d49629b4f5010e4b17165';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1566b79275c0d49629b4f5010e4b17165';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr16a83b945ddc847fd861c7c0c73a296e8';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr16a83b945ddc847fd861c7c0c73a296e8';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1dda4429d380a4244ab6c8873e41c6fa1';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1dda4429d380a4244ab6c8873e41c6fa1';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr1ebda7dbc2b134bed9359d4b6a112a983';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr1ebda7dbc2b134bed9359d4b6a112a983';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr144818ee48b4f42b881c820577f1b4740';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr144818ee48b4f42b881c820577f1b4740';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr13dbd5594458b4246aea9efa5f2d5e0d9';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr13dbd5594458b4246aea9efa5f2d5e0d9';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr112d13cdd72ff41b29eb822a12ef304ea';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr112d13cdd72ff41b29eb822a12ef304ea';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr12ba1d7ec562e45ddb015082b216fa422';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr12ba1d7ec562e45ddb015082b216fa422';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr192875a7b7cc043f18d2cb23671b2f9d6';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr192875a7b7cc043f18d2cb23671b2f9d6';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr1bcc92fe461e54cd0a8d401813ff548c7';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr1bcc92fe461e54cd0a8d401813ff548c7';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr16c36e45c8bed410ab51b39b180ab2fce';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr16c36e45c8bed410ab51b39b180ab2fce';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr18d4a1ec8750245aa8973d6add21ff4a9';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr18d4a1ec8750245aa8973d6add21ff4a9';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr1415c28e6be254ee1955cbd0db1e42e76';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr1415c28e6be254ee1955cbd0db1e42e76';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr1c3af5435fdb44735bab28e2754e46017';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr1c3af5435fdb44735bab28e2754e46017';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr1d8b0b911dd7b43ff825c908263e1fcc1';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr1d8b0b911dd7b43ff825c908263e1fcc1';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1f3a4a5feda9c459ea3113e3143df97d4';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1f3a4a5feda9c459ea3113e3143df97d4';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr173472d1859474c5d86f856f73f0698cc';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr173472d1859474c5d86f856f73f0698cc';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr19e92184756e943fcac415d214aeb2991';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr19e92184756e943fcac415d214aeb2991';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr10b74be7e43c542398544e38e947203e0';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr10b74be7e43c542398544e38e947203e0';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr10205b272fb7747888543febb48094cc3';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr10205b272fb7747888543febb48094cc3';


UPDATE theme
SET category='représenter'
WHERE theme.id_uuid = 'qr1cd1740d150cd4f4692b0585449ce9cb1';


UPDATE theme
SET category_slug='representer'
WHERE theme.id_uuid = 'qr1cd1740d150cd4f4692b0585449ce9cb1';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr126d9468cd8e148149a953c88ebadd1f4';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr126d9468cd8e148149a953c88ebadd1f4';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr104e7c16895e54c06923036b67b9e61ff';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr104e7c16895e54c06923036b67b9e61ff';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr164ce2644128b47c9b4f6662141982411';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr164ce2644128b47c9b4f6662141982411';


UPDATE theme
SET category='s''habiller'
WHERE theme.id_uuid = 'qr120e19fb166164d199540bcf336b284ed';


UPDATE theme
SET category_slug='s-habiller'
WHERE theme.id_uuid = 'qr120e19fb166164d199540bcf336b284ed';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr183869658a73342a793b8525396076172';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr183869658a73342a793b8525396076172';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1e980b72c17ca44988dbaa0e502b91a76';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1e980b72c17ca44988dbaa0e502b91a76';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr133ee45feea4640bd8aaafc06cefc24d9';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr133ee45feea4640bd8aaafc06cefc24d9';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr1257e5a9d3d474555a8ea470d0decacd0';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr1257e5a9d3d474555a8ea470d0decacd0';


UPDATE theme
SET category='habiter'
WHERE theme.id_uuid = 'qr17add8c81c19f400eae331f382c4dd823';


UPDATE theme
SET category_slug='habiter'
WHERE theme.id_uuid = 'qr17add8c81c19f400eae331f382c4dd823';


UPDATE theme
SET category='se divertir'
WHERE theme.id_uuid = 'qr1bbbf4ba311694ab0be79f2c73ed466f8';


UPDATE theme
SET category_slug='se-divertir'
WHERE theme.id_uuid = 'qr1bbbf4ba311694ab0be79f2c73ed466f8';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr1dcb4eefcdb0a4b6da45a5b1400b9378e';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr1dcb4eefcdb0a4b6da45a5b1400b9378e';


UPDATE theme
SET category='consommer'
WHERE theme.id_uuid = 'qr12b95e74499464f5e80615be2aebb17c4';


UPDATE theme
SET category_slug='consommer'
WHERE theme.id_uuid = 'qr12b95e74499464f5e80615be2aebb17c4';


UPDATE theme
SET category='s''informer'
WHERE theme.id_uuid = 'qr11e60529082d845be99398d532ddc7791';


UPDATE theme
SET category_slug='s-informer'
WHERE theme.id_uuid = 'qr11e60529082d845be99398d532ddc7791';

--set not-null constraint after updating
 -- this also allows to make sure that all
 -- rows have a category before committing

ALTER TABLE IF EXISTS theme
ALTER COLUMN category
SET NOT NULL;


ROLLBACK;