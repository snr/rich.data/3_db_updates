BEGIN;

UPDATE title
SET entry_name='Angle de la rue Vivienne et de la rue Beaujolais, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1f2634b48f14b483782eb978549e555e4';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr17e17a0bf5fad4396868265cf1e0b5d12';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1c1629e077805424788a857a7e2d25390';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1ed689a4320d244188f126fbc0f1b3fda';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr11de2ede4d4cc459b860f8b01c8a5a8d6';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1a5d9afccd869469f8f6f854e558e79a3';


UPDATE title
SET entry_name='Hôtel Desmarets, 18 rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr16f3410cc4eef43f39a009f4d0be8a4c3';


UPDATE title
SET entry_name='Heurtoir avec gueule de lion, hôtel Desmarets, 18 rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr13233136083d144b5b0854b99ebccb560';


UPDATE title
SET entry_name='Porte de l''Hôtel Desmarets, hôtel de Coigny, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr14e4405bf3124432dbb7b79f9cbcabb99';


UPDATE title
SET entry_name='Bibliotéque [sic] Nle 9-11-16 3e'
WHERE title.id_uuid = 'qr18e6c02875ee045b696333c6882bce3fd';


UPDATE title
SET entry_name='Rue Richelieu 14-8-16 / Vivienne'
WHERE title.id_uuid = 'qr185033ccaf04b4a75b44fb0b06452116a';


UPDATE title
SET entry_name='Rue Richelieu 14-8-16 / Vivienne'
WHERE title.id_uuid = 'qr15f235c435bd84207a3dbf0db98ce8083';


UPDATE title
SET entry_name='Rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1093e46f95e5f4ee181ee3e77ecfb8166';


UPDATE title
SET entry_name='Palais Royal, femmes, hommes et enfant devant l''escalier menant au passage du Perron, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr11f0c40ff839346aeb73ab5775f11c10f';


UPDATE title
SET entry_name='34-36, rue Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1505f66690d4d4799bdf1364a81e23f0d';


UPDATE title
SET entry_name='18, rue Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1f327c3e5697a4e0eb3d6509943e9ec27';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr11b26389480974b87bc54feeaadfbe10c';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr17d076c0217f64c8394242453c4134b2a';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr163084aa1837b41de80e36803f318895b';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr148e028abf56041e9b5a682d35fc3f258';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr15218c6bb9cb545bba29b4497614768f6';


UPDATE title
SET entry_name='Bourse, palais Brongniart, rue du Quatre-Septembre, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr135f9b114b1bf4848a8689676d3ea090c';


UPDATE title
SET entry_name='Bourse, palais Brongniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e2efd942660d48a8b1bb9e99cd1b2b16';


UPDATE title
SET entry_name='Bourse, palais Brongniart, rue du Quatre-Septembre, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr10e495e251c0d417782fe774734f9cf7d';


UPDATE title
SET entry_name='Voiture à cheval, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1f6ad07f0ec1a4929b4b143cb98dcd0e4';


UPDATE title
SET entry_name='Vue extérieure de l''entrée de la Galerie Vivienne du côté de la rue des Petits Champs, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr17782b668507c41efb99bbe2d5bccea7c';


UPDATE title
SET entry_name='Place de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr11ae425331d3845c08fc4d895dc11c9d8';


UPDATE title
SET entry_name='Arc près de la rue Vivienne, 18 août 1855'
WHERE title.id_uuid = 'qr188ef40e27c3245c4ba79dc4ba0a88b4c';


UPDATE title
SET entry_name='Passages R. D. : galerie Vivienne. Rue Vivienne n°6.'
WHERE title.id_uuid = 'qr15cbf9b1e780148458ef5cc94ec596fde';


UPDATE title
SET entry_name='Passages R. D. : galerie Colbert. Rue Vivienne.'
WHERE title.id_uuid = 'qr1c2f631c2d69c4f0f865b8998674fced8';


UPDATE title
SET entry_name='Passages R. D. : rue Vivienne n°.'
WHERE title.id_uuid = 'qr1b872c00e30ef4eb8a1a156507c821e55';


UPDATE title
SET entry_name='Le crime de la rue Vivienne'
WHERE title.id_uuid = 'qr1cbb4195a3a55455bb38289a43e68019d';


UPDATE title
SET entry_name='Commerce de la rue Vivienne.'
WHERE title.id_uuid = 'qr1681434fdedaa4baf8c35c3e8c61c1b7b';


UPDATE title
SET entry_name='Entrée de la Bourse / par la rue Vivienne'
WHERE title.id_uuid = 'qr1ca4b799077104cd7b8379df6561b06f0';


UPDATE title
SET entry_name='Deux élévations des écuries d''Orléans, 2 et 2bis rue Vivienne, actuel 2ème arrondissement.'
WHERE title.id_uuid = 'qr143d827bb276a44ba9657d886181283b1';


UPDATE title
SET entry_name='Paris en miniature par Arnout. PL. 246 / Rue Vivienne Côté au Levant. Rue Vivienne côté au couchant.'
WHERE title.id_uuid = 'qr1cbc6f47489ed43038cd1bfc35f931615';


UPDATE title
SET entry_name='Paris en miniature, par Arnout, pl.246 / Rue Vivienne, côté au levant / Rue Vivienne, côté au couchant.'
WHERE title.id_uuid = 'qr1678d2b7c949e4ebcbb1a6a1e0dc87f3b';


UPDATE title
SET entry_name='Magasin de la Ce Moka-Zanzibar, 47, rue de Vivienne'
WHERE title.id_uuid = 'qr13563493c2d304f6b8490405de202dc4e';


UPDATE title
SET entry_name='36, Rue Vivienne/ Victor Plumier/ Photographe et Daguerréotype.'
WHERE title.id_uuid = 'qr1e836e8243c5f4e27831eaf275b0fd764';


UPDATE title
SET entry_name='Sallon exécuté Rue Vivienne par Sobre, Architecte./Pl.87.'
WHERE title.id_uuid = 'qr1ac50fa04530b4d978c67f824f067af44';


UPDATE title
SET entry_name='Place de la Bourse / Prise de la rue Vivienne / 8'
WHERE title.id_uuid = 'qr1433e99b3ad344c0a95b38eddfd96f1f7';


UPDATE title
SET entry_name='Décor, ornement; café au Grand Colbert, rue Vivienne, 1848'
WHERE title.id_uuid = 'qr16a1a26a541ac4327816f86a24957cb7e';


UPDATE title
SET entry_name='Décor, ornement; café au Grand Colbert, rue Vivienne, 1848'
WHERE title.id_uuid = 'qr1968e7294cca44abf8b3c3cf0db2e5663';


UPDATE title
SET entry_name='Restauration de l''entrée de la Bibliothèque impériale, sur la rue neuve-de-petits-champs., au coin de la rue Vivienne.'
WHERE title.id_uuid = 'qr178ebc615ac27480aa1d6150c9d6b9aba';


UPDATE title
SET entry_name='Réclame de Charles, Chapelier sis rue Vivienne et boulevard Saint-Michel'
WHERE title.id_uuid = 'qr188c629224e7f4b7aaab78ea70f406f07';


UPDATE title
SET entry_name='Décoration du Salon H..., rue Vivienne, par Sobre, Architecte[...]/Pl.88.'
WHERE title.id_uuid = 'qr1b601e57192d54109a2cc740bd27ab660';


UPDATE title
SET entry_name='Epoque Romaine / Fragments découverts rue Vivienne / PL. XXV'
WHERE title.id_uuid = 'qr13957fa6c2bac4f089390a7997bf210e9';


UPDATE title
SET entry_name='Place de la Bourse, Palais de Cristal 25, Rue Vivienne / Vêtemens (sic) d''hommes.'
WHERE title.id_uuid = 'qr16606a65d27cb400b8094fb619497f1a3';


UPDATE title
SET entry_name='Mignot, à la Belle-Jardinière, rue Vivienne, n°40, fabrique de parfumerie et brosserie.'
WHERE title.id_uuid = 'qr177e85ab8d18346f2b78f5c50a22bab22';


UPDATE title
SET entry_name='Mulot fabricant de gants : 18, rue Vivienne à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1be003104a6c74f7aaa2d3317fb116b15';


UPDATE title
SET entry_name='Investissement de la section Lepelletier par les troupes de la Convention (angle de la rue Vivienne et de la rue des Petits-Champs le 4 octobre 1795).'
WHERE title.id_uuid = 'qr13a21d782e1814518bfa9c7d159929d1e';


UPDATE title
SET entry_name='Dorure et argenture Picot et Marquet : 40, rue Vivienne à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr14b8c1ec92d8242c1a40d50733c190d98';


UPDATE title
SET entry_name='George Delas tailleur et inventeur du somatomètre : 49, rue Neuve Vivienne, vers 1839'
WHERE title.id_uuid = 'qr106210566f4824c0ba61abf4e22c7f962';


UPDATE title
SET entry_name='George Delas tailleur et inventeur du somatomètre : 49, rue Neuve Vivienne, vers 1839'
WHERE title.id_uuid = 'qr1ffa174de9a4248a29b375075a935ac36';


UPDATE title
SET entry_name='THEÂTRE/ GALERIE-VIVIENNE/ 6. RUE VIVIENNE. Près le PALAIS. ROYAL/ MATINEES/ ENFANTINES/ de Famille/ LES/ JEUDIS/ DIMANCHES/ & FÊTES/ A 2 Hes 1/2/ précises'
WHERE title.id_uuid = 'qr19f26465b993847679020ebd6274669fa';


UPDATE title
SET entry_name='Magasin de chaussures "Aux villes de France" : 51, rue Vivienne/104, rue Richelieu à Paris, Second Empire'
WHERE title.id_uuid = 'qr189a19c7cf76e4c88851f0d53d234bc81';


UPDATE title
SET entry_name='Galeries Colbert (passage Vivienne)'
WHERE title.id_uuid = 'qr1db678bf8e31e4e8fb9d637f13e02986d';


UPDATE title
SET entry_name='Plan des Galeries et Rotonde Colbert / Conduisant de la rue Vivienne à la Rue Neuve des Petits Champs et au Palais Royal par le passage des Pavillons'
WHERE title.id_uuid = 'qr140368bee52294d81a0c79ffebb560d10';


UPDATE title
SET entry_name='Alexandre Debain, facteur d''instruments de musique, 53 rue Vivienne, médaillé aux expositions de Paris en 1844 et de Toulouse en 1845'
WHERE title.id_uuid = 'qr121239d1322394f498b3fca916fcedd2b';


UPDATE title
SET entry_name='RHUM DES ILETS/ SUPERIEUR/ à tous/ LES AUTRES/ DEPÔT/ CENTRAL. 53/ Rue Vivienne,/ PARIS/ SE TROUVE DANS LES MEILLEURS ETABLISSEMENTS'
WHERE title.id_uuid = 'qr1f8cdfb6d6c924f05af06b17180171fe2';


UPDATE title
SET entry_name='Bagagisterie Gravelleau & Saint-Denis : 18, rue Vivienne à Paris, première moitié du XIXe siècle'
WHERE title.id_uuid = 'qr172687dfe15334aeb9bb6ff667bf9a2e6';


UPDATE title
SET entry_name='EMPRUNT FRANCAIS 1920/ 5%%/ REMBOURSABLE A 150 Fcs/ Souscrivez à la/ BANCO DE BILBAO/ 27, RUE VIVIENNE. PARIS'
WHERE title.id_uuid = 'qr11304e90dfffa4600ad53e0bd6725d5da';


UPDATE title
SET entry_name='THEÂTRE DE LA GALERIE VIVIENNE/ 6, RUE VIVIENNE, 6/ LA BELLE ET/ LA/ BÊTE/ FEERIE en 3 ACTES et 7 TABLEAUX./ par Mrs Alph. de JESTIERES et Ch. MORIN./ DIMANCHE, JEUDI ET FÊTES/ Matinées enfantines des familles/ à 2 H 1/2'
WHERE title.id_uuid = 'qr1d47149baad3242708fbd17b26aa547ff';


UPDATE title
SET entry_name='Marquis / Le chocolat de Paris / 44, rue Vivienne, 39 D. des Capucines, 4, Pl. Victor-Hugo / 40, Bd. Raspail, 18, Ave. Matignon / 91, Rue de Rivoli'
WHERE title.id_uuid = 'qr1b8a525243eb241868a83adebc77f10c8';


UPDATE title
SET entry_name='JARDIN/ D''HIVER/ 49 RUE VIVIENNE/ Fauteuils 3f./ Promenoir 2f. ECLAIRAGE ELECTRIQUE/ TOUS LES SOIRS/ A DATER DU 24/ SEPTEMBRE/ SPECTACLE-CONCERT'
WHERE title.id_uuid = 'qr165d62c1535a342a2b39eac68c649c579';


UPDATE title
SET entry_name='CASINO/ 49 RUE VIVIENNE/ TOUS LES SOIRS à 8H./ CONCERT 2F./ DIVERTISSEMENTS VARIES/ LES MERCREDIS ET SAMEDIS à 11H. 1/2/ APRES LE CONCERT/ BAL DE NUIT: 3F.'
WHERE title.id_uuid = 'qr153285e32eff94229af78d05d2f763ce4';


UPDATE title
SET entry_name='Vue aérienne de Paris : la Bourse des valeurs, palais Brongniart et la Bibliothèque nationale, rue de Richelieu. 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1562a3b52ff0b4fa8894462a5abd69588';


UPDATE title
SET entry_name='Vue aérienne de Paris : La Bourse, palais Brogniart, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr188b050c2f2654fb789c17d6999a2b89e';


UPDATE title
SET entry_name='Vue aérienne de Paris : La Bibliothèque nationale de France, site Richelieu, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1dfbbd7f30ece4bec8702c44d038398e0';


UPDATE title
SET entry_name='Vue aérienne de Paris : la Bourse des valeurs, palais Brongniart. 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr152e974989b804b6cb7214667d382b05a';


UPDATE title
SET entry_name='Vue aérienne de Paris : la Bourse des valeurs, palais Brongniart. 2ème et 9ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr1e93cf0b99351433db500c33b8bab7555';


UPDATE title
SET entry_name='Vue aérienne de Paris : la Bourse des valeurs (palais Brongniart), 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr162bfe5a57e184d64b4bf244bfc64fc71';


UPDATE title
SET entry_name='Vue aérienne de Paris : le palais et musée du Louvre, le Palais Royal et la banque de France. 1er et 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr17d94a12b7b6f47c3b3f0542fe0e408e6';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec la Bourse des valeurs, palais Brogniart. 2ème et 9ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr1f319b9fdf10b40238b79aeebc654079d';


UPDATE title
SET entry_name='Vue aérienne de Paris : Bibliothèque nationale de France, site Richelieu, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1d3402dde62d84e51993f5d6d9f316883';


UPDATE title
SET entry_name='Place de la Bourse'
WHERE title.id_uuid = 'qr1eb4b89c26baa4fd7ab2bae274c651bec';


UPDATE title
SET entry_name='Place de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr159362fe091d74ddd9185373534f5a18a';


UPDATE title
SET entry_name='Place de la Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr15bfa5fd4daf5459484bb82d2a58d7251';


UPDATE title
SET entry_name='Bureau de surveillant de voitures de place, place de la Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr16083b99d501c4c5093a164b282daa58c';


UPDATE title
SET entry_name='Bureau de surveillance de voiture, place de la Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr13965d132dd774ac5857bdc3409283810';


UPDATE title
SET entry_name='Urinoir enveloppé à six stalles, jardin de la Bourse, place de la Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1b47a1291d89849a4b787f9f9abec991e';


UPDATE title
SET entry_name='La Bourse et place de la Bourse'
WHERE title.id_uuid = 'qr1aa4d82d304584758b8a4e0fd857dbdf8';


UPDATE title
SET entry_name='Place de la Bourse.'
WHERE title.id_uuid = 'qr1f1601cdf98f240df89f25f7ec951e6fc';


UPDATE title
SET entry_name='Place de la Bourse.'
WHERE title.id_uuid = 'qr1962a850088b0456d89b15d32a97544ec';


UPDATE title
SET entry_name='Place de la Bourse'
WHERE title.id_uuid = 'qr1b4500623f35b44c7b3751ebe49b98092';


UPDATE title
SET entry_name='Bureau d''Omnibus de la Compagnie Générale, place de la Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr17e79a50f98934e82a6a38a00fe781535';


UPDATE title
SET entry_name='Vue de la Place de la Bourse'
WHERE title.id_uuid = 'qr1aec409baa3d74d968c16632e8adeffcc';


UPDATE title
SET entry_name='Maisons, place de la Bourse'
WHERE title.id_uuid = 'qr142f193bfd1584ceb8c5f22b2017ab93b';


UPDATE title
SET entry_name='Place de la Bourse. Immeuble squatté'
WHERE title.id_uuid = 'qr1ad2fab49ee924d0d891bc50f19fdfae5';


UPDATE title
SET entry_name='Place de la Bourse et rue Vivienne'
WHERE title.id_uuid = 'qr1e915edaff53a41ce88cbdcc24362109d';


UPDATE title
SET entry_name='Transplantation des arbres sur la place de la Bourse'
WHERE title.id_uuid = 'qr160ed9ae80c134aa4a1298088925d8c5a';


UPDATE title
SET entry_name='Décor, ornement; Café place de la Bourse, 1847'
WHERE title.id_uuid = 'qr1247ab8fa80574649b1039eb1409cebe8';


UPDATE title
SET entry_name='Rue des Filles-Saint-Thomas, vue prise de la place de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1a75b428ad10c4e9086495bac58d2e3f1';


UPDATE title
SET entry_name='La place de la Bourse le jour de la bataille de Reichshoffen.'
WHERE title.id_uuid = 'qr18413d3a30c034cec998e77cd7bf514d4';


UPDATE title
SET entry_name='Bourse et tribunal de Commerce.'
WHERE title.id_uuid = 'qr15e76c0a30ce44fb69bc7d38435391a04';


UPDATE title
SET entry_name='Salle du Théâtre du Vaudeville, place de la Bourse'
WHERE title.id_uuid = 'qr188aaaf8076344db8a84d9700545909b4';


UPDATE title
SET entry_name='Démolition de l''ancien théâtre du Vaudeville, place de la Bourse'
WHERE title.id_uuid = 'qr1eeb28092a5ef406aa3220222a787558d';


UPDATE title
SET entry_name='Campement d''artilleurs sur la place de la Bourse, 4 juin 1871.'
WHERE title.id_uuid = 'qr1b9a5fd82ee5549bdbf30058d8e225e8f';


UPDATE title
SET entry_name='Deux autobus devant le palais Brongniart, la Bourse de Paris, place de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1c8880007b9af4178998f6c1c5b0b3eb8';


UPDATE title
SET entry_name='Publicité pour le spectacle extraordinaire des puces travailleuses, 27 place de la Bourse'
WHERE title.id_uuid = 'qr1dcce9c16b79c40cc90a4bced8586a85f';


UPDATE title
SET entry_name='Place de la Bourse./Prise et incendie du corps de garde de la Gendarmerie, le mardi 27 au soir.'
WHERE title.id_uuid = 'qr1796e2c12c4e449a688e2ce4745cca696';


UPDATE title
SET entry_name='Incendie du Corps de Garde des Gendarmes Place de la Bourse, le Mardi 27 Juillet au soir.'
WHERE title.id_uuid = 'qr1b4438de7c0d1497dac78b72e951cafb0';


UPDATE title
SET entry_name='Hemery tailleur "Aux Deux Nuits" : 31, place de la Bourse, XIXe siècle'
WHERE title.id_uuid = 'qr196787a50c2ae47b9bd8267a97402a58b';


UPDATE title
SET entry_name='Théâtres R. D. : théâtre du vaudeville, bâti pour les nouveautés. Place de la bourse démoli en 1869.'
WHERE title.id_uuid = 'qr1f854370b094b4147aa16bc4f71187410';


UPDATE title
SET entry_name='Mardi soir/Place de la Bourse/Le Corps d''un homme tué rue des Boucheries St Honoré y fut transporté.'
WHERE title.id_uuid = 'qr13634a261f0414915b3393d46ca29455b';


UPDATE title
SET entry_name='Garde-Meuble, Place Louis quinze. Façade du Palais Royal. Colonnade du Louvre. Palais de la Bourse […]'
WHERE title.id_uuid = 'qr11a5460fed92c41d2806253cf80c9fbba';


UPDATE title
SET entry_name='Église de la Madeleine, Arc de triomphe de l''Étoile, place de la Concorde, place Vendôme, Palais Royal, château des Tuileries, palais de la Bourse, hôtel des Invalides, église Notre-Dame, Hôtel de Ville'
WHERE title.id_uuid = 'qr1c6aab08eaf8549ecabddbaaf24a81613';


UPDATE title
SET entry_name='Huit monuments du centre de Paris (fontaine Molière, place de la Concorde, fontaine Louvois, la Madeleine, la Bourse, l''Opéra, la colonne Vendôme et l''Opéra comique.)'
WHERE title.id_uuid = 'qr1c53859f8d93a425bb651f5be3e0ed485';


UPDATE title
SET entry_name='Rue du Commerce reliant la Place de la Bourse au Boulevard Bonne Nouvelle : Projet proposé par Mr Chambine dans l''intérêt et sur la demande des négociants du quartier des Jeûneurs et du sentier'
WHERE title.id_uuid = 'qr15470969831c04e19bc98c0b4c2c32375';


UPDATE title
SET entry_name='Vues de Paris. / N° 1. / La Madeleine. Arc de triomphe de l''Etoile. Tour St. Jacques. Place Vendôme. / La Bourse. Jardin du Palais Royal. Le Panthéon. Notre-Dame. / Place de la Concorde. Hôtel de Ville. Place Napoléon. Porte St Denis.'
WHERE title.id_uuid = 'qr1f09a4dcadcff49c684f91b054023ed32';


UPDATE title
SET entry_name='La Madeleine. Palais Royal. La Bourse. Arc de Triomphe de l’Étoile. / Les Tuileries. Le nouveau Louvre. Place Vendôme. Place de la Concorde et les Champs-Élysées. / Palais de Justice et Ste Chapelle. Rue de Rivoli, Gd. Hôtel du Louvre. Hôtel de Ville....'
WHERE title.id_uuid = 'qr12b39830c119c4824a932eff1608f9241';


UPDATE title
SET entry_name='"A TRIP TO PARIS", "35 THE BOURSE"'
WHERE title.id_uuid = 'qr19067d182dab44a8186319306feff6123';


UPDATE title
SET entry_name='Le Drame de la Bourse'
WHERE title.id_uuid = 'qr1a3eff0d7f08b4f109a74db23bf61f819';


UPDATE title
SET entry_name='La Bourse vue de la rue du Quatre-Septembre. 2ème arrondissement'
WHERE title.id_uuid = 'qr1988acf6e35784713bc20cb7f324a5709';


UPDATE title
SET entry_name='La Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e72f5a1dd6cd481791dc51982f14ff64';


UPDATE title
SET entry_name='Palais de la Bourse.'
WHERE title.id_uuid = 'qr1e110dda489bc4dde8463707f1b17dca7';


UPDATE title
SET entry_name='Mangin, le Marchand de crayons'
WHERE title.id_uuid = 'qr17af4b57a1bc04b16b3fe35af6a4b5c1e';


UPDATE title
SET entry_name='Marchand de valeurs dépréciées à la Bourse'
WHERE title.id_uuid = 'qr195eab0741880466793e61e6308e83de0';


UPDATE title
SET entry_name='Affiche de l''emprunt de la Défense nationale'
WHERE title.id_uuid = 'qr18fb08bb13fb64818934c787c0e0fdd96';


UPDATE title
SET entry_name='Les Carmes Billettes'
WHERE title.id_uuid = 'qr1b96d80e2fc8e4b3090c40feb2761a982';


UPDATE title
SET entry_name='Album du Daguerréotype reproduit, orné de vues de Paris, en épreuves de luxe, avec texte.'
WHERE title.id_uuid = 'qr1fb8f1fd5b2c24b7fab5257fab591321a';


UPDATE title
SET entry_name='Un alibi.'
WHERE title.id_uuid = 'qr13fc8f76d292e453dbcbba746b72c9204';


UPDATE title
SET entry_name='Domestique accompagnant son jeune maître au jardin public'
WHERE title.id_uuid = 'qr1bf41d95a083b4f30aaace6fe53912c8b';


UPDATE title
SET entry_name='Domestique conversant avec sa maîtresse'
WHERE title.id_uuid = 'qr1fea0524cb089485e9c8521e01f17e77e';


UPDATE title
SET entry_name='Je me mets trop bien à la place de votre mari [...] / 13.'
WHERE title.id_uuid = 'qr1c9a6f4140c1c427c83dce5cf2f15fa3e';


UPDATE title
SET entry_name='La place de l''Hôtel de Ville. / 30.'
WHERE title.id_uuid = 'qr1c4e4a52efe0a4243a32d8c04a4657603';


UPDATE title
SET entry_name='La place du Palais de Justice / 12.'
WHERE title.id_uuid = 'qr1d04c84ea173f42159d85a4a7d159c455';


UPDATE title
SET entry_name='La place Royale. / 4.'
WHERE title.id_uuid = 'qr1ef77c54bceab48b0adc3d871200e2718';


UPDATE title
SET entry_name='Les Fontaines cascades de la Place de la Concorde n°7 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr184ed8178df214d0baf7a258525db42c8';


UPDATE title
SET entry_name='L''OPERA AU XIXème SIECLE [planche 48 publiée dans le Charivari]'
WHERE title.id_uuid = 'qr129345e4f6f53476281a9888998fe16c6';


UPDATE title
SET entry_name='Ton mari sera toujours ton mari, tandis qu''Arthur peut en aimer une autre [...]'
WHERE title.id_uuid = 'qr1c99049e3765d4ca2a30bfb1ca14ba090';


UPDATE title
SET entry_name='AUX EAUX DE BADEN. [planche 1]'
WHERE title.id_uuid = 'qr17299f25c82aa47c7a0c2f700c1f855bb';


UPDATE title
SET entry_name='Construction du chemin de fer métropolitain municipal de Paris : salle de distribution des billets de la station "Bourse", 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr17ffe06184bf948b0b35bebf6f7be50b1';


UPDATE title
SET entry_name='Construction du chemin de fer Métropolitain municipal de Paris : quai de la station La Bourse, 2ème arrondissement'
WHERE title.id_uuid = 'qr151b395937baa437884838526300403d1';


UPDATE title
SET entry_name='Fête du 15 août 1853, orchestre devant le Palais de cristal en construction'
WHERE title.id_uuid = 'qr1bda0aa43bbef4c14a016139a9ab35b4a';


UPDATE title
SET entry_name='Porte de l''hôtel des écuries sur la rue Vivienne.'
WHERE title.id_uuid = 'qr17017f601b54745d1b9960d73966f0580';


UPDATE title
SET entry_name='Facture du dépôt de la manufacture de bougies et savon de l''Etoile, rue Vivienne 15'
WHERE title.id_uuid = 'qr1de5cb2b3bb2c47ffa7807d83ce4c8ff0';


UPDATE title
SET entry_name='Vignette de la maison Au fidèle berger, Henrion, confiseur, rue Vivienne 21'
WHERE title.id_uuid = 'qr1ab14df7678f84acaa9d7df10217e427b';


UPDATE title
SET entry_name='Adresse ornée et description de l''activité de Madame Boullanger, marchande de papiers, rue Vivienne.'
WHERE title.id_uuid = 'qr15fc054bf4be64316a0712f3058344049';


UPDATE title
SET entry_name='En-tête de lettre pour le magasin de bronzes et dorures 15, rue Vivienne, tenu par Deniere, fournisseur du roi.'
WHERE title.id_uuid = 'qr16c71deacbf444e5ea6e525277e7bf309';


UPDATE title
SET entry_name='Facture de la Grande Botterie de F. Montigaud, Rue Vivienne 2bis, Paris, 10 mai 1849'
WHERE title.id_uuid = 'qr1315f07f7b5cf4f5e821bd93b097c978a';


UPDATE title
SET entry_name='Deux en-têtes de facture pour G. Clouet & Cie, soieries rubans et nouveautés, Rue Vivienne n°18 (en haut) et A. Boulland imprimeur lithographe, Passage du Caire n°45 (en bas)'
WHERE title.id_uuid = 'qr1909198c29e5b4533a335b06be52e41d4';


UPDATE title
SET entry_name='Le Pont-Neuf, 6ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr191ccdef24a8144bea20489435984a47b';


UPDATE title
SET entry_name='Portrait d''Arnaud, actrice'
WHERE title.id_uuid = 'qr1b54e8957c0aa4028af4985aa3415382f';


UPDATE title
SET entry_name='Portrait d''Arthus, actrice'
WHERE title.id_uuid = 'qr19ac0c87a14164fac827622e24c0518b5';


UPDATE title
SET entry_name='Construction avec voie ferrée'
WHERE title.id_uuid = 'qr1904d7f10042145f99a0320b2ece00a16';


UPDATE title
SET entry_name='Portrait de Léa d''Aseo, actrice'
WHERE title.id_uuid = 'qr11f839127eec54f079e81eabe70235000';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 29]'
WHERE title.id_uuid = 'qr1526640aec97645c48eab36c2cd57db98';


UPDATE title
SET entry_name='Miroir du Coiffeur'
WHERE title.id_uuid = 'qr13ec5b03527d5431983e0fbbd2c0f9b9c';


UPDATE title
SET entry_name='Portrait de Madame Ali, actrice'
WHERE title.id_uuid = 'qr1661cea9b709e438fa0a6f0e6e42f237f';


UPDATE title
SET entry_name='Sculpture de la tête de Napoléon Ier (en empereur romain), photographiée sur un tissu, après la destruction de la colonne Vendôme lors de la Commune de Paris par les communards le 16 mai 1871. Statue d''Antoine-Denis Chaudet (1763-1810), 1er...'
WHERE title.id_uuid = 'qr151599fc032e645b0b5f4128c9518e670';


UPDATE title
SET entry_name='Barricade place Vendôme, après la destruction de la colonne Vendôme lors de la Commune de Paris le 16 mai 1871, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr15a991c0bdf84476983d70a7ddb1b0a72';


UPDATE title
SET entry_name='La Commune 1871. Portraits de Communards place de Clichy, 17ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1fe982fa0aa4544d8a22619c5468b8e9c';


UPDATE title
SET entry_name='La Marchande de violette / Quadrille pour le Piano'
WHERE title.id_uuid = 'qr107447bd6aaf54d61b572c03e2d971e50';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 89]'
WHERE title.id_uuid = 'qr1363f23a1973149338e3b1b6921742943';


UPDATE title
SET entry_name='Le palais des Tuileries : salle des Maréchaux après l''incendie de la Commune de Paris (1871). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr11e426ffb24264bf2980ee529012d8551';


UPDATE title
SET entry_name='Le quai des Tuileries, pavillon de Flore après l''incendie de la Commune de Paris (1871). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr15b66a05de6dc46269377f9ab72bcf96c';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Rokeby, chant IV'
WHERE title.id_uuid = 'qr125e32e59c31f4b83b01058bf9a066ad5';


UPDATE title
SET entry_name='Palais des Tuileries , grand vestibule après l''incendie de la Commune (1871). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr18728f636167a4cde9031a01f54be8321';


UPDATE title
SET entry_name='Le palais des Tuileries , pavillon de l''Horloge, après l''incendie de la Commune de Paris (1871). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c63d45f713e04097b4061d402028e448';


UPDATE title
SET entry_name='Grande liquidation de la maison Modèle'
WHERE title.id_uuid = 'qr13b1097e6d48a4cecb9bbd36392cd7a13';


UPDATE title
SET entry_name='Portrait de Rosine, dite Andréa, actrice (ou Aline, dite Polo)'
WHERE title.id_uuid = 'qr1648bf0bed9784be6b1ff55737c1bb85d';


UPDATE title
SET entry_name='La rue Vivienne.'
WHERE title.id_uuid = 'qr1a5ff14dc47f5476db1299b09c5da56af';


UPDATE title
SET entry_name='Enterrement du duc de Morny'
WHERE title.id_uuid = 'qr175f8523095364540b9c16e6468118bd8';


UPDATE title
SET entry_name='DIALOGUES PARISIENS. [planche nº80]'
WHERE title.id_uuid = 'qr12cbb308ddaa44efb86681128b31987c8';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 92]'
WHERE title.id_uuid = 'qr152b79839a820449e9f89715d651dcb8b';


UPDATE title
SET entry_name='CROQUIS PARISIENS. [planche nº2]'
WHERE title.id_uuid = 'qr1322699b55b414f1faf9a144addb2ce55';


UPDATE title
SET entry_name='Plan général des embellissements projetés pour le Palais Royal (nº4)'
WHERE title.id_uuid = 'qr1f9946c9931e84484a05418c2cea50389';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 93]'
WHERE title.id_uuid = 'qr1afc0782869584f85b7b8298a1c8c4dba';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 59]'
WHERE title.id_uuid = 'qr15ea14efdecc744ee8c2112b310fd9986';


UPDATE title
SET entry_name='THEÂTRE NATIONAL DE L''OPERA -COMIQUE/ LA/ NAVARRAISE/ Episode Lyrique en 2 Actes/ de/ JULES CLARETIE & HENRI CAIN/ Musique de/ J. MASSENET'
WHERE title.id_uuid = 'qr1860856d2659b4fed91c8e05ef9bf781e';


UPDATE title
SET entry_name='CROQUIS PARISIENS. [planche nº2]'
WHERE title.id_uuid = 'qr1ddec3b60a5ea44d09b153cf12116a5f8';


UPDATE title
SET entry_name='Album lithographique pour 1829 : la nourrice'
WHERE title.id_uuid = 'qr1bc30bf5b9dd24935ab36623eb18025e4';


UPDATE title
SET entry_name='Petit album lithographique pour 1829 : il se tient tout seul (IFF 95)'
WHERE title.id_uuid = 'qr16fbd7c35af0f4e2fba7395c47abb00cd';


UPDATE title
SET entry_name='DIALOGUES PARISIENS. [planche nº63]'
WHERE title.id_uuid = 'qr10b43c24a697243cdac6ed626a92c7cd0';


UPDATE title
SET entry_name='Illustrations de Walter Scott, sujets lithographiés tirés de ses romans par A. Devéria et C. Roqueplan [Couverture] (IFF 104)'
WHERE title.id_uuid = 'qr14908b8c0781b468181060100098c2983';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 181]'
WHERE title.id_uuid = 'qr1f4d7fd8231914d7ebb34a0165a4b164f';


UPDATE title
SET entry_name='Album lithographique pour 1829 : souvenirs d''enfance'
WHERE title.id_uuid = 'qr1d4a3d0e2f9184b7792951e8d5651f440';


UPDATE title
SET entry_name='Album Contes de La Fontaine : les trois commères'
WHERE title.id_uuid = 'qr1c10c75b33d0a4716b029d68343f76e6f';


UPDATE title
SET entry_name='Palais de l''Industrie - Vue du grand portique du pavillon central.'
WHERE title.id_uuid = 'qr1c6aa910182d04861b6338e51caaa6593';


UPDATE title
SET entry_name='Petit album lithographique pour 1829 : les conseils de l''Hermite (IFF 95)'
WHERE title.id_uuid = 'qr13cefde5266c44bafad607c17fa556f95';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le cocu battu et content'
WHERE title.id_uuid = 'qr12d6fb904f4dd42ce9593fd04892d0d46';


UPDATE title
SET entry_name='Album Contes de La Fontaine : la courtisane amoureuse'
WHERE title.id_uuid = 'qr15e518454d43848f38be8b60fcf464447';


UPDATE title
SET entry_name='Album Contes de La Fontaine : la chose impossible'
WHERE title.id_uuid = 'qr140ba9e134df64401804af3fe344124ad';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le cuvier'
WHERE title.id_uuid = 'qr19c5b011003e2421eb6a7a2fe93db0d63';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le psautier'
WHERE title.id_uuid = 'qr19315e87e16fd47ed8ba30ade179ef09d';


UPDATE title
SET entry_name='Album Contes de La Fontaine : les deux amis'
WHERE title.id_uuid = 'qr1ac4d147de1674c4a9a8912624d69e96a';


UPDATE title
SET entry_name='Galerie Théatrale : un paradis'
WHERE title.id_uuid = 'qr1b2c26d3cc8a04879ac48b2933eb6fb41';


UPDATE title
SET entry_name='Portrait de Jeanne Brindeau (1860-1946), comédienne.'
WHERE title.id_uuid = 'qr18201836df4cc4bc585ef15988af1a038';


UPDATE title
SET entry_name='Les déserteurs. (IFF p.38)'
WHERE title.id_uuid = 'qr13f2142dea9254f0bba74167b9546e257';


UPDATE title
SET entry_name='Henri III. / Acte III scène V.'
WHERE title.id_uuid = 'qr198bcab42dd024879813f969b5e36cca2';


UPDATE title
SET entry_name='Album de douze sujets : la famille hollandaise'
WHERE title.id_uuid = 'qr10d604dfc39e741d49212b3e0665c65eb';


UPDATE title
SET entry_name='La Commune. Photographie d''un dessin ou tableau représentant une exécussion à la prison de la Grande-Roquette, 24 mai 1871, chemin de ronde, 11ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a43887f0cff04dfc9c13f3191abc39c4';


UPDATE title
SET entry_name='La Commune. Les martyrs de la Grande-Roquette, 24 mai 1871, escalier de secours départ des victimes, 11ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ddfe4396782f4dd4803f43f4ca1fa626';


UPDATE title
SET entry_name='Album Contes de La Fontaine : Joconde'
WHERE title.id_uuid = 'qr18281430ab75642c5a00a98f7793877c7';


UPDATE title
SET entry_name='Album Contes de La Fontaine : la mandragore'
WHERE title.id_uuid = 'qr19f3011af6df14039a61bed1957148226';


UPDATE title
SET entry_name='Album Contes de La Fontaine : Nicaise'
WHERE title.id_uuid = 'qr1b0624dc83ccc4b9f9ffefdffd4282f62';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le remède'
WHERE title.id_uuid = 'qr1397c3d58068c4195b01f68ea4cec16cb';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le gascon puni'
WHERE title.id_uuid = 'qr16e65cb987d8b40d4948dd6028f59a9ed';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le tableau'
WHERE title.id_uuid = 'qr19499d45864824fb6ae3e7936bb1b2287';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le fleuve Scamandre'
WHERE title.id_uuid = 'qr1075c0641c2494f3795b8d9458dcc4c32';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le villageois qui cherche son veau'
WHERE title.id_uuid = 'qr12f4337d359414ffd99a1cd0f42f0902d';


UPDATE title
SET entry_name='Petit album lithographique pour 1829 : elle attend (IFF 95)'
WHERE title.id_uuid = 'qr1be72ce67e6de40b0ac95a590558f8af1';


UPDATE title
SET entry_name='Walter Scott. Rob Roy, chap. XVII'
WHERE title.id_uuid = 'qr17cd0807f82d54cf6b364468856b40ced';


UPDATE title
SET entry_name='Album Contes de La Fontaine : la matrone d''Ephèse'
WHERE title.id_uuid = 'qr108f4157e691343e2810bbe140752ce03';


UPDATE title
SET entry_name='Album de douze sujets : portrait de Léopoldine Hugo'
WHERE title.id_uuid = 'qr1f90782b7f01947b4a3a8aa78f0c33900';


UPDATE title
SET entry_name='Album de douze sujets : la captive, jouant du luth'
WHERE title.id_uuid = 'qr180efed28c6d64111883b0ab080d0c165';


UPDATE title
SET entry_name='NOS JOLIES PARISIENNES'
WHERE title.id_uuid = 'qr1858f17fda8b24b65b1f7c361e0611e12';


UPDATE title
SET entry_name='LE CHARIVARI [planche nº1]'
WHERE title.id_uuid = 'qr1608c0c5530ce414ea3db8160e261db93';


UPDATE title
SET entry_name='Album de douze sujets : couverture (IFF 102)'
WHERE title.id_uuid = 'qr142f3e36ddf5648b0948c5cf75ad47a75';


UPDATE title
SET entry_name='Album de douze sujets : derniers adieux de Charles Ier à ses enfants'
WHERE title.id_uuid = 'qr144f564adfcee4e9d863733a5bf33e248';


UPDATE title
SET entry_name='Album de douze sujets : la petite maman'
WHERE title.id_uuid = 'qr1cb24e36f7f614606bccd330c848b0142';


UPDATE title
SET entry_name='Album de douze sujets : Mme Gaugain'
WHERE title.id_uuid = 'qr12931b7b3597a4a2e873b4f5afa15b4c4';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Richard en Palestine, chap. VIII'
WHERE title.id_uuid = 'qr1f712efa160a64496967ff3466d022d79';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Ivanhoé, chap. XXIX'
WHERE title.id_uuid = 'qr114e3debaaaf14bee9f139288b6360dcd';


UPDATE title
SET entry_name='Album Contes de La Fontaine : on ne s''avise jamais de tout'
WHERE title.id_uuid = 'qr1c94cabade9fe46d2888fef58d60d1228';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le faucon'
WHERE title.id_uuid = 'qr199c13fb31bc0484e8514e9b1dc2701d3';


UPDATE title
SET entry_name='Album Contes de La Fontaine : le roi Candaule'
WHERE title.id_uuid = 'qr188728c0d1bef49b193ae8a8fc5504358';


UPDATE title
SET entry_name='FARIBOLES 2 [planche 31]'
WHERE title.id_uuid = 'qr1fbeb871f84854ff9bb774422dcf0dc0e';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Histoires du temps des Croisades, chapitre XXV'
WHERE title.id_uuid = 'qr1a54c96d2aab84928bcec5c8f14e1cc86';


UPDATE title
SET entry_name='Illustrations de Walter Scott : La jolie fille de Perth, chap. IX'
WHERE title.id_uuid = 'qr10c584006bde14c4180c8577afb701b75';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Kenilworth, chap. VII'
WHERE title.id_uuid = 'qr156930694a9b54466a83a6848905370a5';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Le Talisman, chapitre IV'
WHERE title.id_uuid = 'qr132e2e5b0e7b4414198f7e68db3fbed59';


UPDATE title
SET entry_name='Illustrations de Walter Scott : La jolie fille de Perth, chapitre X'
WHERE title.id_uuid = 'qr1cac9a267cbb94ac0b36cad1b1257a989';


UPDATE title
SET entry_name='Illustrations de Walter Scott : Le Talisman, chapitre V'
WHERE title.id_uuid = 'qr118691a314c4847bb80df260bc6e2dc85';


UPDATE title
SET entry_name='Plaquette publicitaire pour la maison de cosmétiques Clarks, 16 bis rue Vivienne à Paris.'
WHERE title.id_uuid = 'qr1c5b975e28b5d4691afc1e37c39088477';


UPDATE title
SET entry_name='Plan du Premier étage des bâtimens de la bibliothèque du Roy, de la Bourse et de la Compagnie des Indes, situés Rue Richelieu, rue Vivienne et Rue Neuve des Petits Champs près la Place des Victoires / Liv. V N°XIII Planche 2 / 356.'
WHERE title.id_uuid = 'qr1083d18805a74443c8ff84b428ad91819';


UPDATE title
SET entry_name='Plan au Rez-de-Chaussée des bâtimens de la bibliothèque du Roy, de la Bourse et de la Compagnie des Indes, situés Rue Richelieu, rue Vivienne et Rue Neuve des Petits Champs près la Place des Victoires / Liv. V N°XIII Planche 1ère / 355'
WHERE title.id_uuid = 'qr1c14f334d5602483d8c947b15419d5964';


UPDATE title
SET entry_name='Décor, ornement; Rideau du Vaudeville 1845'
WHERE title.id_uuid = 'qr1c048075a881e41e09c165113055c4817';


UPDATE title
SET entry_name='Décor, ornement; Ornement du rideau du Vaudeville, 1845'
WHERE title.id_uuid = 'qr15e9d482043944da59f56b193788433c1';


UPDATE title
SET entry_name='Décor, ornement; Lambrequin du rideau du Vaudeville 1846'
WHERE title.id_uuid = 'qr1dd311037d76b4a59bd66f3062cd9d5e4';


UPDATE title
SET entry_name='Théâtre du Vaudeville; entrée des acteurs'
WHERE title.id_uuid = 'qr15eb91faf10664c0cb54844e30082a2ed';


UPDATE title
SET entry_name='Théâtre du Vaudeville. - Mademoiselle Juliette, rôle de l''Illustration, dans la Planète à Paris.'
WHERE title.id_uuid = 'qr15c53c6c1999f487db5b080c8c457a312';


UPDATE title
SET entry_name='Promenade pittoresque dans Paris. / N° 21. / par Arnout / Fontaine Gaillon. Colonne de Juillet. Fontaine Louvois / Jardin Turc. Hôtel de Ville. Bains Chinois. / Théâtre des Variétés. Maison de François Ier aux Champs Élysées. Théâtre du Vaudeville'
WHERE title.id_uuid = 'qr1210ca694421f47ef85ac02230ed4f874';


UPDATE title
SET entry_name='Portrait de Gabrielle (actrice au théâtre du Vaudeville)'
WHERE title.id_uuid = 'qr1c149fe675abc48d08129b60d3fc244fc';


UPDATE title
SET entry_name='Portrait de Charles Galabert (acteur au théâtre de Vaudeville)'
WHERE title.id_uuid = 'qr14aebaf53f82e49cf818c8fda16bb5635';


UPDATE title
SET entry_name='Portrait de Mme Bodin (actrice au théâtre du Vaudeville et au Gymnase)'
WHERE title.id_uuid = 'qr156708c8e2d1f4acbb8d57f9308f1712f';


UPDATE title
SET entry_name='Intérieur de la salle du Théâtre du Vaudeville.'
WHERE title.id_uuid = 'qr1b79e50ccb3a04efca12d1bb607c15f9c';


UPDATE title
SET entry_name='Costume de l''acteur de Laferrière dans le rôle de Charles dans le neveu de Mercier au Vaudeville'
WHERE title.id_uuid = 'qr1570c422f917240568810c23ae3f450df';


UPDATE title
SET entry_name='Le théâtre des Nouveautés en 1828.'
WHERE title.id_uuid = 'qr1456ba56f28ec42bea4f1551afdb140e1';


UPDATE title
SET entry_name='Costume de Melle Déjazet dans le rôle de Catherine du "Mariage impossible" (Théâtre des Nouveautés, 1828)'
WHERE title.id_uuid = 'qr1f84cce8cd1534efc965a1229d26f8ad8';


UPDATE title
SET entry_name='Si A Paris Il Y Avait La Mer !.../La Bourse.'
WHERE title.id_uuid = 'qr196877806d9a24d9ea9d7a976c3e99122';


UPDATE title
SET entry_name='La Bourse, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c4f3149eb6f244419ba9c082779fe5f0';


UPDATE title
SET entry_name='Palais de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1d781235b5ec84888a04bff3ac6d9568b';


UPDATE title
SET entry_name='"Centenaire du monument de la Bourse construite par Brongniart, inaugurée le 4 novembre 1826"'
WHERE title.id_uuid = 'qr1bbe191e16f164dcd87605875a0592624';


UPDATE title
SET entry_name='"Centenaire du monument de la Bourse construite par Brongniart, inaugurée le 4 novembre 1826"'
WHERE title.id_uuid = 'qr1049daba252db4b38b60479fdef7256e1';


UPDATE title
SET entry_name='Bourse, palais Brongniart, rue du Quatre-Septembre, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e696586aefd649bf9510e126e16be705';


UPDATE title
SET entry_name='A la Bourse'
WHERE title.id_uuid = 'qr1bcc410405f094ddca2c07a8010fc5e7c';


UPDATE title
SET entry_name='La Bourse en construction'
WHERE title.id_uuid = 'qr17659eaf700004a6dbc4e0cd191baf14d';


UPDATE title
SET entry_name='La bourse du jour'
WHERE title.id_uuid = 'qr1a3b3264c2282465cabf2e3a315c3792b';


UPDATE title
SET entry_name='Prise de la bourse'
WHERE title.id_uuid = 'qr1ebfc5f8774014a7995bd26a658847ff4';


UPDATE title
SET entry_name='Paris - La Bourse'
WHERE title.id_uuid = 'qr11ee508a7d8314ac18c2416612440099f';


UPDATE title
SET entry_name='Plan du quartier de la Bourse'
WHERE title.id_uuid = 'qr1cf23f5deb9e941178a52be38b10ee1df';


UPDATE title
SET entry_name='Paris Ville Lumière'
WHERE title.id_uuid = 'qr1f96432ef6788442a9d37d7f01c03c837';


UPDATE title
SET entry_name='Rue des Filles-Saint-Thomas, vue prise de la place de la Bourse, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1a537a961b37c48718206f894d50c20e8';


UPDATE title
SET entry_name='Les boursicotières. / Ouverture de la Bourse.'
WHERE title.id_uuid = 'qr12fad8a2c382641498b2a15cbbc2b7641';


UPDATE title
SET entry_name='Bourse de Paris. / 258'
WHERE title.id_uuid = 'qr1ddb4407adbbd40a4886900b34323fd4a';


UPDATE title
SET entry_name='La Bourse / N°11'
WHERE title.id_uuid = 'qr1c41bd4055c8844e28873f9de3b7bb107';


UPDATE title
SET entry_name='Projet de frise pour le palais de la Bourse'
WHERE title.id_uuid = 'qr1993c4d7479f24f65b8d81ca9d6feaf11';


UPDATE title
SET entry_name='Le palais de la Bourse sous la Restauration'
WHERE title.id_uuid = 'qr1d26b806513b44cf29ed6786a1c6004d4';


UPDATE title
SET entry_name='Projet de la Bourse, 1808 ; coupe sur la longueur'
WHERE title.id_uuid = 'qr1ad4c90174be74e678e4ecae5a6ef6a39';


UPDATE title
SET entry_name='Actualités./6./Les nouveaux Damoclès de la Bourse.'
WHERE title.id_uuid = 'qr11fe68aaec05142f3b1cd9b08064fa164';


UPDATE title
SET entry_name='Quartier de la Bourse. / N°5.'
WHERE title.id_uuid = 'qr1794eccfc92d643ac8c5ba9fb9e1cf304';


UPDATE title
SET entry_name='Quartier de la Bourse. / N°5.'
WHERE title.id_uuid = 'qr1e6083cb83ea742d2bfaef2ab9bf73033';


UPDATE title
SET entry_name='Façade du Palais de la Bourse sur la rue Vivienne'
WHERE title.id_uuid = 'qr1d5f459200d1e47d99f1a1dba0b5d42e5';


UPDATE title
SET entry_name='La bourse. // Benoît, cocher de cabriolet, prit le premier une pièce de / canon et fut porté en triomphe à la bourse.'
WHERE title.id_uuid = 'qr14e36efe5cef7459ea1023add2755de50';


UPDATE title
SET entry_name='Comme vous arrivez tard à la Bourse [...].'
WHERE title.id_uuid = 'qr1c8c5acfd18f1429489d58bddd1bb5533';


UPDATE title
SET entry_name='Extrait d''un plan de Paris : Le Quartier de la Bourse'
WHERE title.id_uuid = 'qr126967c84f1994a7db288bfd3b0cf8a9e';


UPDATE title
SET entry_name='Actualités n°440 : Physionomies de Bourse.'
WHERE title.id_uuid = 'qr1a21800c90ad440e9ac16933fc9e1f188';


UPDATE title
SET entry_name='Projet de la Bourse, 1808 ; coupe sur la largeur'
WHERE title.id_uuid = 'qr1a5213580552e4871970d57d72c6d54cd';


UPDATE title
SET entry_name='Inconvénient des arbres aux alentours de la Bourse [...]. / 504'
WHERE title.id_uuid = 'qr1b2dfefdfe5854ccc95800bf5e2a8d650';


UPDATE title
SET entry_name='Arrivée à la bourse, un jour de hausse (IFF 445).'
WHERE title.id_uuid = 'qr14e9949e67a3d47c8bb7d6bff51abb543';


UPDATE title
SET entry_name='Intérieur de la Bourse [...] / La corbeille des agents [...].'
WHERE title.id_uuid = 'qr19b82cdae448249ac935cfa159c9d1a1b';


UPDATE title
SET entry_name='Plan du 2e niveau ? du quartier de la Bourse'
WHERE title.id_uuid = 'qr13e1cc3fa1ae546f0ade0518e6480815f';


UPDATE title
SET entry_name='Plan du 1er niveau ? du quartier de la Bourse'
WHERE title.id_uuid = 'qr1938bb2b6ba5c4f1e90a0d2b96aee6638';


UPDATE title
SET entry_name='Actualités : C''est signulier rien ne reprend à la bourse...'
WHERE title.id_uuid = 'qr1faa9c3782ea94254be3ed1d1c1884976';


UPDATE title
SET entry_name='Plan du rez de chaussée du nouveau Palais de la Bourse et le Tribunal de commerce, 1809'
WHERE title.id_uuid = 'qr1f7b7d5a64bf645d4b9c14ff4a5b30b1a';


UPDATE title
SET entry_name='Visite de Charles X à la Bourse de Paris, 24 novembre 1824'
WHERE title.id_uuid = 'qr1159ee9ff1b044069906d94461697040b';


UPDATE title
SET entry_name='Vues de Paris / (9) / Views of Paris / Bourse et Tribunal de Commerce. / The Bourse and Tribunal of Commerce.'
WHERE title.id_uuid = 'qr1ce5c320cdd2844faa3624d192aa13366';


UPDATE title
SET entry_name='Courtiers de marchandises et courtiers d''assurances à la bourse de Paris, 1855'
WHERE title.id_uuid = 'qr183b6f79f52324c6e9585cc8683dcc6d4';


UPDATE title
SET entry_name='Intérieur de la Bourse de Paris. [IFF, t.1, p.43]'
WHERE title.id_uuid = 'qr1eacce173deaf4cbbaee07b1132679d09';


UPDATE title
SET entry_name='Bourse de Paris ; / par / A.T. Brongniart, /Architecte.'
WHERE title.id_uuid = 'qr12c20a089f5d84940a9a63bf3fbae926a';


UPDATE title
SET entry_name='Actualités n°310 : Une 1re Représentation de la Bourse.'
WHERE title.id_uuid = 'qr1384ddadc2b804fd1990be36719783535';


UPDATE title
SET entry_name='LA CRISE MONETAIRE A PARIS. - VENTE DE L''OR DEVANT LA BOURSE.'
WHERE title.id_uuid = 'qr1709ab689b9134970a539dc785357a117';


UPDATE title
SET entry_name='Palais des Tuileries, École militaire, Louvre, Bourse et Tribunal de Commerce, fontaine de Grenelle, Hôtel de Ville, Marché des Innocents, palais de la Chambre des Pairs'
WHERE title.id_uuid = 'qr1209f9788409048f3ac71751872a67889';


UPDATE title
SET entry_name='Vue de la Nouvelle Bourse et du Tribunal de Commerce / Prise au coin de la Rue Notre-Dame des Victoires et des Filles St-Thomas / 43'
WHERE title.id_uuid = 'qr1e1273075a3f34cf7883e2eaee47c0f03';


UPDATE title
SET entry_name='A. T. Brongniart. / Architecte. / Né à Paris le 15 Février 1739. Mort le 6 juin 1813 / Palais de la Bourse de Paris, / Commencé par A. T. Brongniart en 1808.'
WHERE title.id_uuid = 'qr1e03489213a724779b654e46fe150d180';


UPDATE title
SET entry_name='Les Saltimbanques. Ô Maître Bilboquet, nous sommes flambés, ces farceurs là vont nous prendre notre public. - Ne crains rien Gringallet, ce n''est point de la concurrence, c''est de la haute comédie!!!...'
WHERE title.id_uuid = 'qr13097675e0f374d5ab008e92f1207ca0a';


UPDATE title
SET entry_name='Vues de Paris. / N° 1. / La Madeleine. Arc de triomphe de l''Etoile. Tour St. Jacques. Place Vendôme. / La Bourse. Jardin du Palais Royal. Le Panthéon. Notre-Dame. / Place de la Concorde. Hôtel de Ville. Place Napoléon. Porte St Denis.'
WHERE title.id_uuid = 'qr11728631f77d8416ba6fa18ee119df86e';


UPDATE title
SET entry_name='La Féerie de la rue'
WHERE title.id_uuid = 'qr1aac7e199f4c4489cb9d8d662f249cdae';


UPDATE title
SET entry_name='A partir du 29 NOVEMBRE lire dans le/GIL BLAS/ l''Argent/ Roman Inédit/ pAR/ E.ZOLA.'
WHERE title.id_uuid = 'qr179c43454e8684efcae8893da45ba7b38';


UPDATE title
SET entry_name='Monument à Robert Macaire'
WHERE title.id_uuid = 'qr1e40c75ed55304d078f64c5e76404e457';


UPDATE title
SET entry_name='Médaille des agents de change de Paris attribuée à M. Saint André, 1898'
WHERE title.id_uuid = 'qr1f88e732408a34efd8816a12a62a3d9ce';


UPDATE title
SET entry_name='Médaille des agents de change de Paris attribuée à M. Saint André, 1898'
WHERE title.id_uuid = 'qr1b22070fa3f6d4791b607590770a8e298';


UPDATE title
SET entry_name='Paris et ses souvenirs. / Place de la Bourse'
WHERE title.id_uuid = 'qr1fc3cf5491cb849678c1bd77af97bbf77';


UPDATE title
SET entry_name='Actualités n°1 : Monsieur Gogo et les nouvellistes de la Bourse'
WHERE title.id_uuid = 'qr115308d5a821e4d7c98d44e6e2c6a31f0';


UPDATE title
SET entry_name='La malheureuse bourse se sentant perdue..., en se voyant attaquée de tous les côtés à la fois.'
WHERE title.id_uuid = 'qr1c8191fad4b554ed2991db1af66094c89';


UPDATE title
SET entry_name='La reine Marie-Amélie, accompagnée de Madame Adélaide, des princesses Louise et Marie et du prince de Joinville, visite les blessés de Juillet à l''ambulance de la Bourse, sous la conduite du docteur Guillon, le 25 août 1830'
WHERE title.id_uuid = 'qr13c1e80e68fa6400cb0e036262e326283';


UPDATE title
SET entry_name='La reine des Français visitant les blessés de Juillet à l''ambulance de la Bourse / (25 août 1830)'
WHERE title.id_uuid = 'qr12e40efde41fe41c68da3608445388d85';


UPDATE title
SET entry_name='Comme quoi tout le monde [...] / Agréable aspect de la petite bourse [...].'
WHERE title.id_uuid = 'qr172e1bf7d446041c492e2cbc7710ad645';


UPDATE title
SET entry_name='Actualités./117./-Pr''nez !...bourse !!,tirez ! ..rgent !!, mettez dans !...caisse. Tarteifle !!! ça manque de précision,...autant !!!'
WHERE title.id_uuid = 'qr118d33ebbd83d4bd3b26ba1aad9d9b23a';


UPDATE title
SET entry_name='CITOYENS,/ A la Commune de Paris nous disons:/ Renfermez-vous strictement dans l''édification/ de nos franchises municipales.'
WHERE title.id_uuid = 'qr196a3f9b8146d44078c42ae1b7c90d922';


UPDATE title
SET entry_name='Une terrible rencontre. / 36'
WHERE title.id_uuid = 'qr15d1f438db1d14ccd95ed59224d913c48';


UPDATE title
SET entry_name='Dernier conseil des ex ministres.'
WHERE title.id_uuid = 'qr1a3c960278052421787936691c098e0f2';


UPDATE title
SET entry_name='Une idylle dans les blés. / N°72.'
WHERE title.id_uuid = 'qr12a75a0854c1b4c8ba5c869bb1c8176dd';


UPDATE title
SET entry_name='Toilette de la Ville de Paris. Vous la regarderez, mais vous n'' y toucherez plus.'
WHERE title.id_uuid = 'qr1fb6842ff643144c489c12eb217b9a984';


UPDATE title
SET entry_name='Tout est perdu ! fors la caisse.......'
WHERE title.id_uuid = 'qr105bfaaca4e8e4281b238c1e9112aba9a';


UPDATE title
SET entry_name='UN CITOYEN EXASPÉRÉ PAR LES BUFFLETERIES.- Dire que nous avons pu nous débarasser de Louis-Philippe et qu''il n''y a pas moyen de se délivrer de ces bricoles là!.......'
WHERE title.id_uuid = 'qr1e2f078337e2d4053a0d6087c61a3ea4e';


UPDATE title
SET entry_name='A la recherche d''une forêt en Champagne. / 1'
WHERE title.id_uuid = 'qr129d3fa4f499f4d3b8ecdb796dcdf93b9';


UPDATE title
SET entry_name='Actualités.156.Les parisiens ayant trouvé moyen de circuler, en temps de pluie, sur les Boulevards macadamisés'
WHERE title.id_uuid = 'qr16ae623b46e394269a373347aa624a809';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ MAIRIE DU 2me ARRONDISSEMENT/ AVIS/ Le Maire du 2me Arrondissement informe ses administrés que la distribution des cartes/ d''alimentation sera continuée'
WHERE title.id_uuid = 'qr1085bbe27d81b43c2bc8cad0db938994f';


UPDATE title
SET entry_name='Locataires et propriétaires. N° 18. Comment on comprend le balcon Espagnol à Paris.'
WHERE title.id_uuid = 'qr1ce3bb079f47447fe840a8986fc5c17b6';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ LIBERTE.- EGALITE.- FRATERNITE./ MAIRIE DU 2me ARRONDISSEMENT/ RATIONNEMENT DE LA VIANDE'
WHERE title.id_uuid = 'qr1c264c391e71c4041a0568e93b04e83be';


UPDATE title
SET entry_name='Le chapelier. / 3.'
WHERE title.id_uuid = 'qr15d69a50d0d444b81b8fdfbe39b0d8d7c';


UPDATE title
SET entry_name='A LA REINE DES FLEURS/ L T PIVER/ USINE A LA VILLETTE/ MAISON CENTRALE BOULEVARD DE STRASBOURG 10./ USINE A GRASSE/ PARFUMEUR DE S. M. L''EMPEREUR/. PARIS./ MAISON à BRUXELLES/ MAISON à LONDRES'
WHERE title.id_uuid = 'qr1dd8329e3f9794dfbab07e63d6a1c0a1a';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ N° 79 LIBERTE - EGALITE - FRATERNITE N° 79/ COMMUNE DE PARIS/ CITOYENS,/ Les réactions prend tous les masques'
WHERE title.id_uuid = 'qr1cbfe8d143d3743c08eaba24bf8fc004b';


UPDATE title
SET entry_name='Le quartier de la Banque. / 22.'
WHERE title.id_uuid = 'qr1265614c004bc4f5bade0d57f1a9d2f70';


UPDATE title
SET entry_name='La Fontaine Molière n°1 de l''album de 12 planches Les Embellissements de Paris'
WHERE title.id_uuid = 'qr1cb04f6670b7c480894a67d256df484b6';


UPDATE title
SET entry_name='Le bottier. / 7. (IFF 61)'
WHERE title.id_uuid = 'qr199fb2d598d324de0b583e077bace293f';


UPDATE title
SET entry_name='L''agent d''affaires. / 12. (IFF 61)'
WHERE title.id_uuid = 'qr19915a0f4a0ae4563872e095a4ac82206';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ LIBERTE.- EGALITE.- FRATERNITE./ MAIRIE DU 2me ARRONDISSEMENT/ SOUSCRIPTION/ POUR/ L''ARTILLERIE/ DE LA GARDE NATIONALE'
WHERE title.id_uuid = 'qr11f6a1a30ec8846bb91a30e22550d7657';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ MAIRIE DU 2me ARRONDISSEMENT/ LOGEMENTS VACANTS'
WHERE title.id_uuid = 'qr1aaaa007d6702466fbe31c3170f128cfc';


UPDATE title
SET entry_name='Les Trottoirs en Bitume... n°3 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr1b4d6a1058f63497895189a6e215e1d9e';


UPDATE title
SET entry_name='Reproduction d''un bon pour 15 francs, caisse d''utilité, passage du théâtre Feydeau, série A'
WHERE title.id_uuid = 'qr154f12a70c34b4eaf93dbf30a559c61fb';


UPDATE title
SET entry_name='641 - Paris Le Palais de la Bourse'
WHERE title.id_uuid = 'qr1a636a2eb58f046038fff8c5f3151fb4e';


UPDATE title
SET entry_name='Silhouette de profil, sur une estampe avec un texte louant les services de Ferdinand, dessinateur et silhouetteur, passage Feydeau'
WHERE title.id_uuid = 'qr1131ae433cdce4ff4bbbe153117f0750b';


UPDATE title
SET entry_name='Les Devantures des magasins de nouveautés n°9 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr10b91d6dbae774800bb2ddc8ff3311290';


UPDATE title
SET entry_name='Les Colonnes moresques du Boulevard... n°5 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr1b408467e72cc477e8777034e0ee223a8';


UPDATE title
SET entry_name='Les Horloges municipales n°4 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr10e8aac8d39144f21a51f30c8af6bea92';


UPDATE title
SET entry_name='Les inconvénients du pavage en bois n°8 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr1555edecef14543028c93609e1e5f26a4';


UPDATE title
SET entry_name='Les Arbres-fagots des quais et boulevards- n°11 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr11bd31ea4aea642fda3062f573f390924';


UPDATE title
SET entry_name='Les Bornes Fontaines - n°12 de l''album Les Embellissements de Paris'
WHERE title.id_uuid = 'qr12f789c3a988644e4bc6e134e1e25e4ae';


UPDATE title
SET entry_name='Le coiffeur. / 2.'
WHERE title.id_uuid = 'qr148cf5bde05c141348db691f008c2ef6d';


UPDATE title
SET entry_name='Galerie Vivienne en 1825.'
WHERE title.id_uuid = 'qr14438f9ccc5bf48268aee756be238fab1';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr150cc95ca89464d72858163128d11e7c4';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr166196e422f19426997fa82d8c99051ef';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1301adb5d849c4770aafceb15a9515424';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr183a6f494454f44a9908c050904a01e05';


UPDATE title
SET entry_name='Galerie Vivienne, 6, détail, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ad543d625d7c4ffb88bcab9b9a13886f';


UPDATE title
SET entry_name='"Incertitude du jour", surimpression d''éléments de la galerie Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1acfa7b8af962407aacf5e55d64bf3372';


UPDATE title
SET entry_name='Galerie Vivienne'
WHERE title.id_uuid = 'qr1d096e391223e4ce98856bb8dc1471502';


UPDATE title
SET entry_name='Maison de Bougainville, entrée de la Galerie Vivienne, rue de la Banque, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c3603cc4a2774a4d8bc2f5334961c289';


UPDATE title
SET entry_name='Petits Champs, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr159f15cf6723e406f84e8052c9812decc';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr18756a9fa58fb49028d310d8731618189';


UPDATE title
SET entry_name='Galerie Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr155253580eda14aba9ecacf512282c98b';


UPDATE title
SET entry_name='Théâtre de la Galerie Vivienne : 6, rue Vivienne à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr154c75e42ae2b4e82bd8600bb8942b41d';


UPDATE title
SET entry_name='13, galerie Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr145e0169d5dce4745b4f00077e9d99d83';


UPDATE title
SET entry_name='Facture vierge. "Mme Flamant / Galerie Vivienne".'
WHERE title.id_uuid = 'qr144ea858affa04f538c009ece77a30214';


UPDATE title
SET entry_name='En-tête de la maison Guiche, 57 galerie Vivienne'
WHERE title.id_uuid = 'qr17121f07bc00c49938e3489582086fae8';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr11e8ad0d353a04f208ab77575913e53d2';


UPDATE title
SET entry_name='Escalier, 13 galerie Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1b54d84322a124e7e8ae2953b9aa6266d';


UPDATE title
SET entry_name='Galerie Colbert, rue Vivienne, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr15121b176e83041d4877a9e424f9a973e';


UPDATE title
SET entry_name='Carte de la maison Michel, coutelier, à l''M couronnée, galerie Vivienne 58'
WHERE title.id_uuid = 'qr11a37d82a0d8744f38fa9f4f640b9ec92';


UPDATE title
SET entry_name='Passages R. D. : galerie Vivienne. Rue de la banque n°5.'
WHERE title.id_uuid = 'qr175878a938ec9459185309bd0963fec46';


UPDATE title
SET entry_name='Passages R. D. : galerie Vivienne. Rue Notre-Dame des petits-champs 4.'
WHERE title.id_uuid = 'qr152aeaf4b1b8f4f45b87080163d6f9f4b';


UPDATE title
SET entry_name='Invitation à l''inauguration de l''exposition de peinture, sculpture, architecture, à la galerie Vivienne le 14 octobre 1883'
WHERE title.id_uuid = 'qr1bec56a9fb79e44d3a3fd4cb89e084855';


UPDATE title
SET entry_name='Magasin "David" : 4 bis, rue neuve des petits champs/galerie Vivienne à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr19a3cec1218c54acfb3d2697140eeddf8';


UPDATE title
SET entry_name='EXPOSITION/ des ARTS/ INCOHERENTS/ AU PROFIT/ des GDES SOCIETES/ D''INSTRUCTION GRATUITE [...] / DU 20 OCTOBRE AU 20 NOVEMBRE 1884, DE 10 H. DU MATIN A 11 H. DU SOIR [...] GALERIE VIVIENNE/ 55.57.59'
WHERE title.id_uuid = 'qr1396f7b09802b455d9df31ca31c2d2adb';


UPDATE title
SET entry_name='Entrée du passage des Panoramas, 10, rue Saint-Marc, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr157beb24c19904a7e922b83197a0b792a';


UPDATE title
SET entry_name='« Côté Cour, le concierge... », passage Vivienne, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ee58f284de474bce82e40c0123447880';


UPDATE title
SET entry_name='Plan général des galeries Colbert et du passage Vivienne / 4e cah. / Pl 49 à 52'
WHERE title.id_uuid = 'qr1df58856665974e68b8abc2ebea277d5b';


UPDATE title
SET entry_name='Vue prise de la rue Vivienne, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr141148ce508714a49bd58546f6dd2a82f';


UPDATE title
SET entry_name='Sortie du débiteur. / Sainte-Pélagie. / N°12.'
WHERE title.id_uuid = 'qr19cc1e8c7ba944e8fa5c48e135784c7b7';


UPDATE title
SET entry_name='Projet de transformation d''un bâtiment entre la Rue Vivienne et la Rue Richelieu'
WHERE title.id_uuid = 'qr1fe50f4c26a7e4b74a05589a94f95c643';


UPDATE title
SET entry_name='Projet de transformation d''un bâtiment entre la Rue Vivienne et la Rue Richelieu / Etat avant l''installation des Villes de France'
WHERE title.id_uuid = 'qr1a6bbf9b363694531a78c942f277f1e39';


UPDATE title
SET entry_name='Projet de transformation d''un bâtiment entre la Rue Vivienne et la Rue Richelieu'
WHERE title.id_uuid = 'qr15aa844de045e43c0ba1b911631f58460';


UPDATE title
SET entry_name='Plan du quartier de la rue Vivienne avec projet de prolongement de cette rue et emplacement des nouvelles fontaines.'
WHERE title.id_uuid = 'qr16c67372932e74efdae8a8b3f9dda05ee';


UPDATE title
SET entry_name='Projet d''une place et d''une fontaine sur le boulevard Montmartre en face du prolongement de la rue Vivienne.'
WHERE title.id_uuid = 'qr18a1451f687114ce294ba1820ce01481f';


UPDATE title
SET entry_name='Projet d''éclairage de la Bourse de Paris par deux candélabres à la girafe en 1829'
WHERE title.id_uuid = 'qr1bef6f80ad3d64436877337405fcf8d98';


UPDATE title
SET entry_name='Projet d''agrandissement et de dégagement de la Bourse et Tribunal de Commerce, place de la Bourse. 1866. 2ème arrondissement'
WHERE title.id_uuid = 'qr18bb5df66520f44d88e5a16712fc6b993';


UPDATE title
SET entry_name='Projet de place devant la Bourse, en 1807, actuel 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr169a365168510418aac3596e85a1fee05';


UPDATE title
SET entry_name='Projet pour le rideau du Vaudeville.'
WHERE title.id_uuid = 'qr187607ed0954e40e38a24901be7c3628f';


UPDATE title
SET entry_name='Vue de la Porte de la Bourse en face et à l''extrémité de la rue Vivienne (Projet)'
WHERE title.id_uuid = 'qr1287c63139015474f84359a7381a1f81f';


UPDATE title
SET entry_name='Costume Parisien / Jeune élégante Revenant de la Promenade en négligée du matin / Dessinée Rue Vivienne'
WHERE title.id_uuid = 'qr18c5986a547984f18831b0ede9f7b5a44';


UPDATE title
SET entry_name='Costumes Parisiens / Béret de crêpe du magasin de Mme. Fouchet, Rue Vivienne. / Collier et Pélerine de velours. Robe de gaze.'
WHERE title.id_uuid = 'qr13be4eee4d10c441ea442aea1c452ada4';


UPDATE title
SET entry_name='Costumes Parisiens / Capote de crêpe du Magasin de Mme Fouchet, Rue Vivienne. Robe de gros / de Naples peint du Magasin de Mr Delille, Rue Ste. Anne. Echarpe damassée'
WHERE title.id_uuid = 'qr104b2f5a3713d48eb84c3db1fa7f57ad2';


UPDATE title
SET entry_name='Costumes Parisiens / 1, Béret de crêpe du Magasin de Mme. Fouchet, Rue Vivienne. / 2, Chapeau de satin. 3, Bonnet de blonde.'
WHERE title.id_uuid = 'qr119b04f1b0f664359bcb7823787e5faae';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de gros de Naples couleur saumon, du Magasin de Mme Fouchet, / Rue Vivienne, N° 2 bis. Robe de gros des Indes.'
WHERE title.id_uuid = 'qr157fb0050e1d744bcb331a10d2fa745ec';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de crêpe du Magasin de Mme Fouchet, Rue Vivienne, n°2 bis. Robe d''organdi avec broderie à jour et rempli bordé de dentelle'
WHERE title.id_uuid = 'qr10cc1669d97e44b54a1991852e723c8d4';


UPDATE title
SET entry_name='Costumes Parisiens / Costume de Longchamp. Chapeau de gros de Naples du magasin de Mme. Fouchet, / Rue Vivienne, N°. 2 bis. Robe de gros de Berlin. Canezou de tulle.'
WHERE title.id_uuid = 'qr1268b2afb586e45f8ae5319a371a589b3';


UPDATE title
SET entry_name='Costumes Parisiens / Bonnet de blonde orné de rubans et de fleurs par Mme Fouchet, Rue / Vivienne, N°2 bis. Redingote de mousseline ornée de broderies & bordée d''un tulle.'
WHERE title.id_uuid = 'qr10cefd91f66e44287a5232a24f7e8753c';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de papier frappé imitant la paille, du Magasin de Mme Fouchet, Rue Vivienne, / n°2 bis. Redingote de mousseline fermée par des noeuds garnis de dentelle'
WHERE title.id_uuid = 'qr1ef3f22436e2d4d2daa5d8d65ce3c8a36';


UPDATE title
SET entry_name='Costumes Parisiens / Béret de crêpe orné de chefs d''argent, du Magasin de Mme. Fouchet, Rue Vivienne. / Robe de crêpe garnie de ruches disposées en ifs. Manches à la DONNA MARIA.'
WHERE title.id_uuid = 'qr174bc1e0d9f4948de8ef6ad08afa53d73';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de gaze brochée du Magasin de Mme. Fouchet, Rue Vivienne, N°.2 bis. Robe d''étoffe à raies / brochées garnie de feuilles et de noeuds de ruban par Mme. Allin, Rue Croix des Petits Champs, N°.25.'
WHERE title.id_uuid = 'qr17d9832bde9824355af545951dd7f65bf';


UPDATE title
SET entry_name='Costumes Parisiens / Capote de blonde du Magasin de Mme. Fouchet, Rue Vivienne, N°.2 bis. Canezou de tulle du / Magasin de Mr. Lavigne, Boulevart Poissonnière, N°18. Robe de Chali imprimé.'
WHERE title.id_uuid = 'qr1ea27b854b78146bf8857d05913991163';


UPDATE title
SET entry_name='Costumes Parisiens / 1, Béret de velours plain. 2, Capote anglaise. 3, Bonnet de tulle et blonde. / Ces trois coeffures ont été dessinées dans le Magasin de mme Fouchet, / Rue Vivienne, n° 2bis'
WHERE title.id_uuid = 'qr1c5d1813e90fa4cfe9c4e564396f117fa';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de foulard du Magasin de Mme Fouchet, Rue Vivienne, n°2 bis. Robe de / guingamp. Fichu de tulle. Colerette à coques de ruban. Ceinture peinte. Brodequins / à pièce boutonnée sur le coude-pied'
WHERE title.id_uuid = 'qr1dde06c0114994724bb7a21cae1cb3301';


UPDATE title
SET entry_name='Costumes Parisiens / Habit vert-russe de chez Mr Barde, Rue Vivienne. Pantalon / d''étoffe du Thibet. Redingote de casimir. Pantalon de piqué'
WHERE title.id_uuid = 'qr1ebc9f49d90e7482aa61e678dedf20524';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de castor. Redingote de mérinos-cachemire, bordée d''une tresse, / du Magasin de Mr. Barde, Rue Vivienne, N°.8.'
WHERE title.id_uuid = 'qr1c85cf561a0c9434f87da41977156d5b7';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de castor. Amazone de casimir flamme d''enfer à collet de velours, / du Magasin de Mr. Barde, Rue Vivienne, N°.8. Cravate de satin. Chemisette brodée.'
WHERE title.id_uuid = 'qr118b01d556eb6450b9f52e36d73a30510';


UPDATE title
SET entry_name='Costumes Parisiens / Redingote à schall d''une nouvelle coupe par Mr. Barde, Rue Vivienne, N°.8. Gilet de piqué broché. / Cravate de gros de Naples. Pantalon de casimir. Redingote croisée par devant. Pantalon de piqué.'
WHERE title.id_uuid = 'qr1816f261351cc4988b1925fb0f950ebff';


UPDATE title
SET entry_name='Costumes Parisiens / Habit à collet de velours et boutons d''or. Gilet de satin garni de boutons / en diamans. Pantalaon de casimir. Claque à gance passementée : / Costume dessiné par Mr. Barde, Rue Vivienne, N°.8.'
WHERE title.id_uuid = 'qr182bed5dcae634299856372f4d8e48b4b';


UPDATE title
SET entry_name='Costumes Parisiens / Habit de drap de chez Mr Barde, Rue Vivienne, entièrement doublé en soie. Gilet de / gourgouran broché. Gilet de dessous en velours. Pantalon de casimir. Manteau de drap doublé de staff, et à collet de pluche. Habit de drap de chez...'
WHERE title.id_uuid = 'qr134d9f7621325499ea35a5c26c13ce553';


UPDATE title
SET entry_name='Costumes Parisiens / Habit de drap de chez Mr Barde, Rue Vivienne. Gilet / de satin par dessus un gilet à schall en piqué. Pantalon de casimir à baguette. Redingote à schall. Pantalon / de daim. Bottes molles lacées par devant. Cravate de satin boutonnée.'
WHERE title.id_uuid = 'qr12577cd1247ec4b03a993b5fd83e86565';


UPDATE title
SET entry_name='La Mode / Bonnets de blonde et de réseaux de rubans de Mme. Beauvais / rue Ste. Anne-Pardessus d''organdi orné de biais de Mesdes. / Nerrier et Richard, rue vivienne 14'
WHERE title.id_uuid = 'qr180db4b6f76f047739b56b8d87d1426cb';


UPDATE title
SET entry_name='Modes de Paris / Petit Courrier des Dames / Coiffure éxécutée par Mr. Dubois rue St. Honoré en face St. Roch, / Mantille et Bonnet en tulle brodé de Mme. Payan rue Vivienne, 15, / Etole en Rubans.'
WHERE title.id_uuid = 'qr1aac16c0fa97a497185c311937fcec2f5';


UPDATE title
SET entry_name='Modes de Paris / Petit Courrier des Dames / Chapeau en satin des Msins. de Mme. Seuriot rue deMonsigny, N°. 1. Manteau à Palmes cachemire / des Msins. de Narey, rue de grammont N° 7. Chaise de bambou Chinois des Msins. Chinois Place de / la Bourse N°...'
WHERE title.id_uuid = 'qr10a2c50eb82ee4315a31748eadb07e2ef';


UPDATE title
SET entry_name='Homme bardu, assis de trois quarts face, son chapeau posé sur un guéridon'
WHERE title.id_uuid = 'qr11fe6386a5cde41b39142f305cf1f99d9';


UPDATE title
SET entry_name='Melle de Koroff en robe brodée avec écharpe et manchon taupe garnis skungs de renard et manchon assorti et chapeau à large bord avec plumes, maison Max Fourrure'
WHERE title.id_uuid = 'qr1378958a7146b47ef9fee063efea44f7a';


UPDATE title
SET entry_name='Eventail publicitaire Les Chemins de Fer de l''Etat'
WHERE title.id_uuid = 'qr1ff6c04fe8676422bbf1bd64d5f72782b';


UPDATE title
SET entry_name='"F. Marquis"'
WHERE title.id_uuid = 'qr1f39394c16091484ca83b8fe3f9f35534';


UPDATE title
SET entry_name='Un coin de salon chez le peintre'
WHERE title.id_uuid = 'qr18b573a7fb05f4b08aff25483d4865344';


UPDATE title
SET entry_name='Mme Dorval. Rôle de Catarina Bragadini dans Angelo'
WHERE title.id_uuid = 'qr1f746d47496194398ba73e76ab7ce6473';


UPDATE title
SET entry_name='Locataires et propriétaires. 7'
WHERE title.id_uuid = 'qr135d1056bf6344409864f37cf1c45287b';


UPDATE title
SET entry_name='Victor Hugo'
WHERE title.id_uuid = 'qr163bc5da274cf48c8b3912086d8d18f98';


UPDATE title
SET entry_name='Victor Hugo et Alexandre Dumas'
WHERE title.id_uuid = 'qr1c5521ecb06bd49179d84a85efbb1b605';


UPDATE title
SET entry_name='Vue de Dinant'
WHERE title.id_uuid = 'qr1d9034b209ab4478f8dd371dbe18ef620';


UPDATE title
SET entry_name='Le Charivari, trente-troisième année, vendredi 23 décembre 1864'
WHERE title.id_uuid = 'qr1b740b9921bb24b5e9de96fcf8ae22ba8';


UPDATE title
SET entry_name='Victor Hugo'
WHERE title.id_uuid = 'qr1fb4fcbe3ceb548ed9f8ec5e369cb6577';


UPDATE title
SET entry_name='Villers-la-Ville : ruines de l''abbaye'
WHERE title.id_uuid = 'qr1dcffd9e4ceeb4829abd089cc0f1d5ac8';


UPDATE title
SET entry_name='Le Charivari, trente-unième année, samedi 8 novembre 1862'
WHERE title.id_uuid = 'qr1f699c1b1fb5a44de9c9f8d9a4426ec86';


UPDATE title
SET entry_name='Portrait de F.Pelayo Brio'
WHERE title.id_uuid = 'qr1b8277fc5438745b684d8731b183ad8e9';


UPDATE title
SET entry_name='Le Charivari, trente-septième année, dimanche 12 avril 1868'
WHERE title.id_uuid = 'qr15c5fb74877c444c8b25dd94f56ff9d1b';


UPDATE title
SET entry_name='[Odes et Ballades, livre V, Odes XVIII] A une jeune fille'
WHERE title.id_uuid = 'qr10c7e3e69eca34cefbfe977c5c2d96ba0';


UPDATE title
SET entry_name='Le Charivari, trente-quatrième année, mardi 2 mai 1865'
WHERE title.id_uuid = 'qr1e5c5937a5e8d426ea056fc81500a1471';


UPDATE title
SET entry_name='Le Charivari, trente-septième année, jeudi 16 juillet 1868'
WHERE title.id_uuid = 'qr13ddd2301740540d6b3d0aaeb8d7988a5';


UPDATE title
SET entry_name='Le Charivari, trente-neuvième année, mercredi 8 juin 1870'
WHERE title.id_uuid = 'qr10a0c04843332423c8d53437fdb202ce4';


UPDATE title
SET entry_name='68. - Paris. - La Bourse'
WHERE title.id_uuid = 'qr155ef7058b10043479082bfa505cdcb72';


UPDATE title
SET entry_name='A l''exposition des œuvres d''André Gill (Galerie Vivienne)'
WHERE title.id_uuid = 'qr15cf3c47259614822a46cc13aabea2c8e';


UPDATE title
SET entry_name='Merci, monsieur, j''n''ai besoin du bras de personne'
WHERE title.id_uuid = 'qr1cf4648a4ec10488f96dbfe2c610609fe';


UPDATE title
SET entry_name='Merci de votre politesse'
WHERE title.id_uuid = 'qr1e0a1f24f60954ae59dd38e3e2993c136';


UPDATE title
SET entry_name='Comédie bourgeoise'
WHERE title.id_uuid = 'qr1f67521d609c04298ae97e0a7b26166d2';


UPDATE title
SET entry_name='Une grande coquette'
WHERE title.id_uuid = 'qr1dfb68e8243614870a6e3d22ae2e5d9a1';


UPDATE title
SET entry_name='Mamans de comédie'
WHERE title.id_uuid = 'qr1db4bb11c24074cd5a1fd9a98806ecdb4';


UPDATE title
SET entry_name='J''n''aime pas les ricaneurs'
WHERE title.id_uuid = 'qr161df229efc5f4acdafdc8285b48cde43';


UPDATE title
SET entry_name='Laissez moi…. Je vous déteste'
WHERE title.id_uuid = 'qr1f8daf03d67814607a3127c354290572f';


UPDATE title
SET entry_name='Vous êtes bien gentil'
WHERE title.id_uuid = 'qr19256abe0b442424ab35bc8b5f1208221';


UPDATE title
SET entry_name='Monsieur, ne me remet pas'
WHERE title.id_uuid = 'qr1c54788a4ba754a48a33daf85c127f06a';


UPDATE title
SET entry_name='Serez-vous encore méchant ?'
WHERE title.id_uuid = 'qr1abba0a7a15414cc1b6ebd7727bf4e264';


UPDATE title
SET entry_name='Il veut m''épouser……. Le scélérat !!'
WHERE title.id_uuid = 'qr1078dfdcb39a8453893d98aa7f9121b94';


UPDATE title
SET entry_name='Léon !..... J''t''en prie ne te détruis pas'
WHERE title.id_uuid = 'qr160b21694fb7d45449a6cf0958168d180';


UPDATE title
SET entry_name='J''veux pleurer ci ça m''fait plaisir'
WHERE title.id_uuid = 'qr10bf01fc71870400fa2223ed8e1e5bf1c';


UPDATE title
SET entry_name='Chef d''emploi'
WHERE title.id_uuid = 'qr195e7acfc8b26434ab1835f08234c6465';


UPDATE title
SET entry_name='César Birotteau à la bourse'
WHERE title.id_uuid = 'qr1e4240a3a24fd48cc9a0ea3d97ce98e2c';


UPDATE title
SET entry_name='Robert Macaire agent d''affaires'
WHERE title.id_uuid = 'qr1b8ed88458fed450f92f731c50cfabdde';


UPDATE title
SET entry_name='Grands exercices'
WHERE title.id_uuid = 'qr13cc2b3b4511447b5939afe5e8764a8e1';


UPDATE title
SET entry_name='Imitation libre d’un tableau de Mr. Horace Vernet, représentant le massacre des Janissaires'
WHERE title.id_uuid = 'qr1caaab2072cd740a0aeabf9eafb4eb090';


UPDATE title
SET entry_name='De Balzac'
WHERE title.id_uuid = 'qr12689f95075094dd7bd82a0e2dd2b9a13';


UPDATE title
SET entry_name='Robert Macaire boursier'
WHERE title.id_uuid = 'qr1bd65e9809a5540c78c0c2ec6be315be3';


UPDATE title
SET entry_name='C''est pas l''amitié qui vous étouffe vous !'
WHERE title.id_uuid = 'qr1c8cae337d962412eb5aba51040c0bacc';


UPDATE title
SET entry_name='Une première entrevue'
WHERE title.id_uuid = 'qr18b572702994d4adcb2cb6a8df4a54a5e';


UPDATE title
SET entry_name='Le temps agréablement employé'
WHERE title.id_uuid = 'qr18cdbbcd52d9247cd99540a41c08bb16c';


UPDATE title
SET entry_name='Tempus edax rerum. Le temps grand mangeur de tout'
WHERE title.id_uuid = 'qr1a4c560029f2f4e45912f1d15b61d6a0c';


UPDATE title
SET entry_name='Le bain du créancier. / Sainte-Pélagie. / N°10.'
WHERE title.id_uuid = 'qr14186984a033446bbb2743507206d3306';


UPDATE title
SET entry_name='La paie. / Sainte-Pélagie. / N°11.'
WHERE title.id_uuid = 'qr110bdae3830184cee86de9379ab4adabb';


UPDATE title
SET entry_name='Portrait de Théophile Gautier'
WHERE title.id_uuid = 'qr1be272625978844d89096eaa5a7328cff';


UPDATE title
SET entry_name='Vue intérieure de la rotonde et de la galerie Colbert'
WHERE title.id_uuid = 'qr19cdcf1c4f5f04d3ea6cf1722839db92f';


UPDATE title
SET entry_name='Vue intérieure de la Galerie et Rotonde Colbert. Pris du passage des deux Pavillons descendant au Palais Royal'
WHERE title.id_uuid = 'qr1c665903bc5654838a27cd11ca30e9300';


UPDATE title
SET entry_name='Décor d''une travée au dessus des boutiques de la galerie Colbert'
WHERE title.id_uuid = 'qr1fbac04e8366d4248aff61d85b750ef2c';


UPDATE title
SET entry_name='Façade de la galerie Colbert du côté de la rue Neuve des petits Champs / 4e cah. / Pl. 57 à 58'
WHERE title.id_uuid = 'qr11a4366d08d3d45d890c1a2510d5a61cf';


UPDATE title
SET entry_name='Passage R. D. : galerie Colbert. Rue Notre-Dame des petits-champs n°6.'
WHERE title.id_uuid = 'qr11b07d90df00c4621adc3088636f5ade3';


UPDATE title
SET entry_name='Vue intérieure de la Galerie et Rotonde Colbert / prise du Passage des Pavillons descendant au Palais Royal.'
WHERE title.id_uuid = 'qr18f569f2faad54dc392699cfaa52fbe71';


UPDATE title
SET entry_name='Maudit col !! / What a bad stock !! / N°15'
WHERE title.id_uuid = 'qr1adc241e1d7ac4558a3f4510471fe8103';


UPDATE title
SET entry_name='La loge grillée. / The box grated. / N°14'
WHERE title.id_uuid = 'qr178b5ae854f7a48dd9c5888816c0b6a10';


UPDATE title
SET entry_name='Voilà une lecture amusante !! / What a delightful reading !! / N°13'
WHERE title.id_uuid = 'qr19f7ab2d60eb84eb7bcecd1414cbfa903';


UPDATE title
SET entry_name='La carte à payer / Et pas assez d''argent. / I must pay the bill / And I have not money enough. / N°11'
WHERE title.id_uuid = 'qr16dd166bd6c7f490aad04e94c009c29af';


UPDATE title
SET entry_name='SOIRÉES PARISIENNES. Au coin du feu.'
WHERE title.id_uuid = 'qr1e54ee2868382435e86a82028f1f27986';


UPDATE title
SET entry_name='Procès de la machine Fieschi. Pepin - Morey - Fieschi - Boireau - Bescher. Dessinés à la Cour des Pairs, par Daumier'
WHERE title.id_uuid = 'qr19ae54f9e7e3a4d949fec20327a6a8eb1';


UPDATE title
SET entry_name='La consultation'
WHERE title.id_uuid = 'qr19f39fcc731474e9597a180c73db06306';


UPDATE title
SET entry_name='L''amateur d''huitres. / The lover of oisters. / N°4'
WHERE title.id_uuid = 'qr1e7c5fa5e0f9d4da0ab3aa42f4f270b93';


UPDATE title
SET entry_name='Intérieur du passage Colbert, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1a94ec3a653e94582a81688029f41ff25';


UPDATE title
SET entry_name='Entrée du passage Colbert, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr168b2a6d2ecf843958504a8f9845cdf05';


UPDATE title
SET entry_name='Passage Colbert, 2ème arrondissement, Paris'
WHERE title.id_uuid = 'qr1cb81a4b062254fd4b9cc850ef174a0ea';


UPDATE title
SET entry_name='Plan général des galeries Colbert et du passage Vivienne / 4e cah. / Pl 49 à 52'
WHERE title.id_uuid = 'qr15ae73c5941da4857996c905016ca2d0e';


UPDATE title
SET entry_name='L''Ecole des Voyageurs/N°2/La table d''hôte (au relais)'
WHERE title.id_uuid = 'qr1436dcdf99dcd44ab9641d523ef9348e2';


UPDATE title
SET entry_name='Bibliothèque Nationale de France, 8 rue des Petits-Champs, 2ème arrondissement.'
WHERE title.id_uuid = 'qr1939c935ff45d482f999fc96e21a4400f';


UPDATE title
SET entry_name='Lettre de changement d''adresse de la maison Franchet, joailler bijoutier, au 22 rue Vivienne'
WHERE title.id_uuid = 'qr183a4018feaa9479caae2e5ddb1003b30';


UPDATE title
SET entry_name='Portrait d''homme'
WHERE title.id_uuid = 'qr1de9c7e533c944201ab76c1e32607606a';


UPDATE title
SET entry_name='Carton d''invitation au Théâtre Vivienne pour "Les Hommes aimés" (...), conférence humoristique par Paul Delvaux.'
WHERE title.id_uuid = 'qr160db4dc3565b4a63b99cb057eebe2e01';


UPDATE title
SET entry_name='Spectacles (Titre de la série)'
WHERE title.id_uuid = 'qr139e2c960e2384358af6951a39ad73ee2';


UPDATE title
SET entry_name='THEÂTRE de la GALERIE VIVIENNE/ ALADIN/ OU/ LA LAMPE/ MERVEILLEUSE/ OPERETTE-FEERIE/ nouvelle/ en/ 3 actes, un prologue/ et 8 tableaux de/ Mr. ERNEST DEPRE/ MUSIQUE DE/ Mr. ALBERT RENAUD/ jouée par/ Mr. PAUL LEGRAND/ et/ toute la Troupe/ enfantine.'
WHERE title.id_uuid = 'qr15a91db5aa4c046bb98a20fd2a500fe08';


UPDATE title
SET entry_name='Portrait de Théophile Gautier (1811-1872), (écrivain, poète)'
WHERE title.id_uuid = 'qr131032d8de9c144ed9a433441ba760759';


UPDATE title
SET entry_name='Nadar (Gaspard-Félix Tournachon, dit) (Paris, 06–04–1820 - Paris, 21–03–1910), photographe'
WHERE title.id_uuid = 'qr10b90bdce748a441c99d6938a4df3081f';


UPDATE title
SET entry_name='1037 Paris. - La place de la Bourse et de la rue de la Bourse.'
WHERE title.id_uuid = 'qr197655d7390b54a61a277c8d1d844a950';


UPDATE title
SET entry_name='464 - Ancien Paris. - La Bourse vers 1830'
WHERE title.id_uuid = 'qr18953b3bf425f4fc5b08fd3217a2954f9';


UPDATE title
SET entry_name='Paris. - La Bourse'
WHERE title.id_uuid = 'qr1ebfd0150b9e54d5793e3b6a8b6d9ae6d';


UPDATE title
SET entry_name='4283. Paris - La Bourse /Elevée de 1808 à 1827, par Brogniart et Labarre /Modifiée en 1903.'
WHERE title.id_uuid = 'qr1077dbd8ca24c4f2b8b37c8efc352a61d';


UPDATE title
SET entry_name='621. Paris - La Bourse'
WHERE title.id_uuid = 'qr17a933298a7a7427184958f712386c014';


UPDATE title
SET entry_name='Paris. - La Bourse'
WHERE title.id_uuid = 'qr143f72c8eb4004b26a08d0c4caeb48df0';


UPDATE title
SET entry_name='Paris. /La Place De La Bourse'
WHERE title.id_uuid = 'qr1737ffc233a50478287fe5056581aa52b';


UPDATE title
SET entry_name='5012. - Paris. - Place de la Bourse.'
WHERE title.id_uuid = 'qr162c0615636df4268abfe8034fb3d501c';


UPDATE title
SET entry_name='Paris. - La Bourse'
WHERE title.id_uuid = 'qr1eeee57ed3c234bafbf7f7d4242e94d9b';


UPDATE title
SET entry_name='Autobus Renault TN6C'
WHERE title.id_uuid = 'qr174fb372902924bbbaf4f19bc557d74e2';


UPDATE title
SET entry_name='Deux publicités, l''une pour le magasin de nouveautés Au Grand Colbert, l''autre pour la maison A. Marty, Aux Dames Françaises'
WHERE title.id_uuid = 'qr14587623af1a942319acf0849bec2e578';


UPDATE title
SET entry_name='Vêtements, étoffes (Titre de la série)'
WHERE title.id_uuid = 'qr14dbc33f92f4640edb18327c0286a2bcc';


UPDATE title
SET entry_name='Magasins de Nouveautés / Aux villes de France'
WHERE title.id_uuid = 'qr158e77f0d009e4e2db30922835a3ee5e8';


UPDATE title
SET entry_name='Rue Vivienne / Galerie Colbert / F (Titre de la série)'
WHERE title.id_uuid = 'qr19e1a8119c27a45679b7b7b03e430fbf4';


UPDATE title
SET entry_name='La Bourse de Paris. Rue Vivienne, un groupe d''individus regarde la vitrine du ”Crédit de la Bourse. Or. Argent. Change” derrière laquelle apparait l''affichage des différents taux des monnaies, or et argent. Au fond, on aperçoit de profil, la brasserie ”Le Vaudeville” : [photographie] / Jean Marquis'
WHERE title.id_uuid = 'qr1fd2666812f604a8b90f9572dd7658347';


UPDATE title
SET entry_name='Rue des Filles Saint-Thomas : [photographie] / [Charles Marville]'
WHERE title.id_uuid = 'qr11d0db86d2d564f31aa5b499aa2b30228';


UPDATE title
SET entry_name='Place de la Bourse : [photographie] / [Charles Marville]'
WHERE title.id_uuid = 'qr11385119c4a1c4b409f8a07dc46815d79';


UPDATE title
SET entry_name='Paris et ses environs. La place de la Bourse : [photographie] / Fiorillo, Mieusement, N. D., X. et L. P. photographes'
WHERE title.id_uuid = 'qr18530ce4e807c4e9d99a49eee09560826';


UPDATE title
SET entry_name='[Boutiques parisiennes. Bourse de Paris, place de la Bourse (2e arr.)] : [photographie] /'
WHERE title.id_uuid = 'qr1833f98fbc82b47a8b2799c0ce52bc53a';


UPDATE title
SET entry_name='[Paris et ses environs. Place de la Bourse. Bourse] : [photographie] / Frith''s Séries'
WHERE title.id_uuid = 'qr1cf89b0d815874c4189571598ddb6ce49';


UPDATE title
SET entry_name='Paris. Colonne Morris pour affiches militaires et administratives [place de la Bourse] : [photographie] / cliché Identité judiciaire'
WHERE title.id_uuid = 'qr1a1e06b739a304bcd87591187d03ba63d';


UPDATE title
SET entry_name='[Boutiques parisiennes. Banque. Cote de la Bourse et de la Banque, Cote Vidal, 1, place de la Bourse ; Magasin de pipes, Au Pacha, 3, place de la Bourse (2e arr.)] : [photographie] / L. Ferriot'
WHERE title.id_uuid = 'qr1cf04d969c46a4b5682cd7c044fb9e25c';


UPDATE title
SET entry_name='[Boutiques parisiennes. Transports. Omnibus à chevaux, lignes AS et AB, 10, place de la Bourse (2e arr.)] : [photographie] /'
WHERE title.id_uuid = 'qr177beaabe8b9249b98aca3525dcc8e0fc';


UPDATE title
SET entry_name='La Bourse de Paris. Notre-Dame des Victoires (2e arr.). Une foule exclusivement masculine se presse dans la rue et sur les trottoirs. A gauche, les grilles qui entourent la Bourse et au fond, la place de la Bourse : [photographie] / Jean Marquis'
WHERE title.id_uuid = 'qr1d53d851842854b28a56c12f391288a92';


UPDATE title
SET entry_name='La Bourse de Paris. La place de la Bourse. Vue sur la rue et la façade avec les hautes colonnes, l''horloge qui marque 1h 15. Des groupes d''hommes attendent devant les grilles : [photographie] / Jean Marquis'
WHERE title.id_uuid = 'qr1d31d0ae25bd340d5b98ef5b239238e2c';


UPDATE title
SET entry_name='[Paris. La place de] la Bourse déserte'
WHERE title.id_uuid = 'qr1ebe4a7187d054a059862e52bc3001cb6';


UPDATE title
SET entry_name='[Paris. La place de] la Bourse déserte'
WHERE title.id_uuid = 'qr187cf73e69ce24f0cbaf559766d838f4a';


UPDATE title
SET entry_name='[Paris. Vespasienne à six stalles surmontée d''un réverbère] : [photographie] / [Charles Marville]'
WHERE title.id_uuid = 'qr176f446942a9d43e49a3335626f388a4c';


UPDATE title
SET entry_name='Paris et ses environs. La place de la Bourse'
WHERE title.id_uuid = 'qr1be1a0322469645389dbda762af70ffa9';


UPDATE title
SET entry_name='Paris et ses environs. La Bourse'
WHERE title.id_uuid = 'qr114893d2cd56b4e2480b123314d8bfd4a';


UPDATE title
SET entry_name='Menuiserie : devantures de boutiques'
WHERE title.id_uuid = 'qr17b8985c9192a48a7b726268b3d132a01';


UPDATE title
SET entry_name='[Paris.] Réouverture de la Bourse : [photographie] / [Identité judiciaire]'
WHERE title.id_uuid = 'qr1843b79f400ca475ba990e7eee98de803';


UPDATE title
SET entry_name='Paris. La Bourse à 15 heures : [photographie] / cliché Identité judiciaire'
WHERE title.id_uuid = 'qr1007132b161a14203b371d8c20ef2a578';


UPDATE title
SET entry_name='Paris et ses environs. Le Pont Royal et le pavillon de Flore. Place de la Bourse : la foule et les fiacres'
WHERE title.id_uuid = 'qr1319dbb752fde4c66bb1106feeb19acdb';


UPDATE title
SET entry_name='Rue Joquelet : [photographie] / [Charles Marville]'
WHERE title.id_uuid = 'qr1c58ceb1fccf14e82be94e8791150ae48';


UPDATE title
SET entry_name='2e arrondissement. Vue prise au-dessus de la place du Carrousel'
WHERE title.id_uuid = 'qr10756e71a32874135a6dce9b19908a90d';


UPDATE title
SET entry_name='Paris et ses environs. La rue de la Banque et la Bourse : [photographie] / Fiorillo, Mieusement, N. D., X. et L. P. photographes'
WHERE title.id_uuid = 'qr179f865be126641d9b321b12831af6f00';


UPDATE title
SET entry_name='La Bourse de Paris. Vue globale de l''intérieur du bâtiment de la Bourse, du plafond richement décoré et extrêmement haut qui repose sur des arcades jusqu''au rez-de-chaussée en pleine effervescence et au milieu ”la Corbeille”, le coeur de la Bourse : [photographie] / Jean Marquis'
WHERE title.id_uuid = 'qr193b65c5b9f734f8cb2e3400cd830c63d';


UPDATE title
SET entry_name='Foire de Paris du 10 au 25 mai. Administation 8 pl. de la Bourse, Paris : [affiche] / F. Calais'
WHERE title.id_uuid = 'qr18453a129446a4aa290d942f8585ad876';


UPDATE title
SET entry_name='Paris et ses environs. La Bourse : [photographie] / Fiorillo, Mieusement, N. D., X. et L. P. photographes'
WHERE title.id_uuid = 'qr197b091f1003344d28203e6d6c9ef2326';


UPDATE title
SET entry_name='[Paris]. Galerie Colbert. [Vues prises de la rotonde et vers la rue des Petits-Champs]'
WHERE title.id_uuid = 'qr1a1a3699312d546598987e27aa9b99fb2';


UPDATE title
SET entry_name='[Paris]. Galerie Colbert. [Vues prises de la rotonde et vers la rue des Petits-Champs] : [photographie]'
WHERE title.id_uuid = 'qr1139117d55fb24d38b8c94ccdadb62359';


UPDATE title
SET entry_name='Paris. Station d''omnibus devant la Bourse'
WHERE title.id_uuid = 'qr17835643f921b4d6abba97c8478b30948';


UPDATE title
SET entry_name='Paris industriel et historique. 2me arrondissement. Quartier Feydeau : [vers 1840] / dressé et mesuré par Feuillet-Dumas ; gravé par Henriot'
WHERE title.id_uuid = 'qr1acbef4261e5941cabdc2adb6df484920';


UPDATE title
SET entry_name='A vendre à l''amiable en dix neuf lots les terrains des anciens ateliers des messageries royales. S''adresser à Mtre Yver, notaire Rue St Honoré, n° 422, et à l''Administration des Messageries Royales, rue Notre Dame des Victoires, n° 22 : vers 1845'
WHERE title.id_uuid = 'qr17d2b6f7045f54afb88ef454ee3693212';


UPDATE title
SET entry_name='Compagnie parisienne de distribution d''électricité. 23 rue de Vivienne. [Plan des zones de distribution.]'
WHERE title.id_uuid = 'qr14d4b96c4b31a4b8197f039cc35b1a922';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr104d9caa1678e4936b74033df64797c56';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr1156d919b4d544a9ea5eace578d819b1e';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr15d16ece6d61e462dac385b02144735bc';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr19b27762b35cb4b529072a6ba6649ae5c';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr186f895187db644e7b52736ca50f1b232';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr11683e843c9164c26ac2ec671e10960b4';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr1c13663c9971a4e1ebf10e0655567ff23';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr11949d3049987465c9e4b49f7f29447f2';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr16ffb4feeb53446f19c2fcf3e67447b9e';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr15629609fd1e445bcbf0cba074b978c74';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr1de33e21d9ff94353931141e0fc6f1bc0';


UPDATE title
SET entry_name='Paris. Rue Vivienne'
WHERE title.id_uuid = 'qr1e880a67684ef4d9f9b77f874ad4317ee';


UPDATE title
SET entry_name='Paris. Théâtre du Vaudeville, place de la Bourse'
WHERE title.id_uuid = 'qr140517c67009f480f831a5e06bb0953ff';


UPDATE title
SET entry_name='Paris. Théâtre du Vaudeville, place de la Bourse'
WHERE title.id_uuid = 'qr10c524322f4e54b3eb2065b9903960861';


UPDATE title
SET entry_name='Paris. Théâtre du Vaudeville, place de la Bourse'
WHERE title.id_uuid = 'qr1a7e67815d26c4f7f8d3ce3d2613910f7';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr100009fa7d14c442493dca88393e3baf6';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr118bf6cab7ec0474aac93f2e47d22861e';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr10c2dbf05209046f69b2978caea2a0307';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1f1e8c35260134516b5f78744faa6e8a6';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr13aad7c04af234c20855717f098d65341';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1d4c1d4058cc5468b95afa219292d238c';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1fc5db354f76f47e7b9ddedc14bdad8f9';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1c4ae9180cd984afc98839689608870f5';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr16dfb47e75fa64b3995f5f8862de1b664';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1ec715cc6f44b4d33bc7d8dbea63e1e21';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1ef2186a8c7b24c6eab703cd4231f1352';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1b2d5ddfec06f4459a64503ade8231719';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr12536ea608f0f4009b314e55bf59814fd';


UPDATE title
SET entry_name='Paris. Place de la Bourse'
WHERE title.id_uuid = 'qr1237af7b6fd404f08a81100544d985b6d';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr133a52dc22d7e4334bf98bd39ba9b23a1';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1a40b0aa5b4414d2d905f1695dc25f722';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr184d74b255d7347fda548649d458bfff4';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr196b9f58cd6f74ed6a233f9658d698c16';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1c5ed71eb14484c87b32d93d0295fc7f9';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1d7fe38b9e4a0474a91bbe1e2d0ad193c';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1e0a1d18042da4267bdb9598b29a80b99';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr19db88d2b4d124b9290b0f18b89d5e9af';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1a5a5619f3bbe40138da88f8fc23ab240';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1928f854e5be34a4c880e44de9f5ddc9a';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr16a0b252dff6a483f8b7f62d2b2e1b63d';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, types, boursiers, spéculateurs, financiers'
WHERE title.id_uuid = 'qr1a15c105b2440450280b3cedeba5a51d1';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr14ca23b5e7c434b468228bcac6dcad36d';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr19b92017fec754d87b5a79930c7929a8a';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr187afa7d6cde342d5804483c70f29cca5';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr151a4948f92d849e7b45140b257d26808';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr19ef12577f308434c8b09b427cf8f5506';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr1a0b9092c19c1463faf6ea717ffae400e';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr12f36e43c582d46db93963a2e4a904c16';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr14f2eb2c7978d44dea8da728be45eaa99';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr19c87fd33cd33407db68b35da013ca6c7';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, humour et caricatures'
WHERE title.id_uuid = 'qr1e0f21fb418eb49f0aff54af4ef1e6c88';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr1bf521216409141eba1655d038dd451fa';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr1b0a6ced75d8746a9b74539afcacea17b';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr1315e6a52410a45a6946a4ae53c572962';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr1d72b760452fc43fda5dd50e4ea12897c';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr170ac4390f98e4d9daa239897805880f3';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, événements'
WHERE title.id_uuid = 'qr157e4dc6280bc4503a3401ce19a8a0117';


UPDATE title
SET entry_name='Paris. Vues diverses et plan du deuxième arrondissement'
WHERE title.id_uuid = 'qr179e6105eb6f5491d85e7e4fc92990954';


UPDATE title
SET entry_name='Paris. Vues diverses et plan du deuxième arrondissement'
WHERE title.id_uuid = 'qr14bda0f8811514848b3a66ce210065fbe';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr12449587bbcb744c6b0ba8045db2bcc21';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr11a00db4204fe4043846a1d1543a9a046';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr19dd40c9f75224b84bbec5d28b5113ad0';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr1640b3731c4ad48d6a882b7028f4e3be0';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr13dfc0d6eceaa4840aed32f8782199bbe';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr12ef44ac1ee2d4a4baaab24d4f2aedca4';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr172f2881c13e44376b53fd3eabc58909a';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr19b07628a743f4358825f756f8596233c';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr18282a5004e8c42c188c68b7666ecfdae';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr1de242802920a477a9b6b44c90b199eeb';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr1dfc3573e30094412b2125ea2ec098903';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr1f96702a150f4410092ba24d02280dac6';


UPDATE title
SET entry_name='Paris. La Bourse des valeurs, divers'
WHERE title.id_uuid = 'qr19dd72ab93cc74b2baedc7101081db058';


UPDATE title
SET entry_name='Grand Restaurant Vivienne Maison Alix - 2 rue Vivienne'
WHERE title.id_uuid = 'qr16ff355111cc94aae831cd6ad86381311';


UPDATE title
SET entry_name='Grand Etablissement de Bouillon - 2 rue Vivienne (passage Colbert)'
WHERE title.id_uuid = 'qr13f41402931754386a5e98e24585ed752';


UPDATE title
SET entry_name='Grande Table d''Hôte Vivienne - 2 rue Vivienne (passage Colbert)'
WHERE title.id_uuid = 'qr183017f4b0934440ca3cb5b5a8edc6b8e';


UPDATE title
SET entry_name='Restaurant Table d''Hôte de la Bourse et de la Banque - 2 rue Vivienne (passage Colbert)'
WHERE title.id_uuid = 'qr1113b4703b5c948df8f82ee3bca47ba1b';


UPDATE title
SET entry_name='Le Grand Colbert - 2 rue Vivienne (Galerie Colbert)'
WHERE title.id_uuid = 'qr11ff8fc10f3fb46169ca9e72d073409ff';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr16c91701728354b00b696be0a11e7206e';


UPDATE title
SET entry_name='Grand Bouillon Restaurant Maison Calmels et Causse - 2-2 bis rue Vivienne (Galerie Colbert)'
WHERE title.id_uuid = 'qr1201e2c3b565147afb2871f2d642b3f5f';


UPDATE title
SET entry_name='Vaudeville - 29 rue Vivienne'
WHERE title.id_uuid = 'qr126e14f35dbb14d59b15653ae1086e8a8';


UPDATE title
SET entry_name='Grand Restaurant des Finances - 45 rue Vivienne'
WHERE title.id_uuid = 'qr19932f0c82fff4da18a6d0e1ae587b2da';


UPDATE title
SET entry_name='Tabary''s Restaurant - 45 rue Vivienne'
WHERE title.id_uuid = 'qr1aa00277b96f94cd18ae8f157b706983e';


UPDATE title
SET entry_name='Bar de l''Etoile - 46 rue Vivienne'
WHERE title.id_uuid = 'qr1dfc1feb688694c5c86a8ed2f35accd98';


UPDATE title
SET entry_name='Grand Restaurant Vivienne - 47 rue Vivienne'
WHERE title.id_uuid = 'qr1185fa90c95234e47a0817da793895a83';


UPDATE title
SET entry_name='Table d''Hôte de la Bourse, Grand Restaurant de la Bourse - 47 rue Vivienne'
WHERE title.id_uuid = 'qr18b1933d48f0943baac3445ee6591b0ff';


UPDATE title
SET entry_name='Auberge d''Alsace-Lorraine - 47 rue Vivienne'
WHERE title.id_uuid = 'qr1e6dc2970cbe143678f91c0e84525b4f4';


UPDATE title
SET entry_name='Bar des Variétés - 48 rue Vivienne'
WHERE title.id_uuid = 'qr1140e59c05bc14965ad40393ede91f7df';


UPDATE title
SET entry_name='Cercle des arts libéraux - 49 rue Vivienne'
WHERE title.id_uuid = 'qr18447c38cdf8949d58250497c3bdffdce';


UPDATE title
SET entry_name='Restaurant Legrain-Legeard - 53 rue Vivienne'
WHERE title.id_uuid = 'qr1c595a81af3a14dfca8cd25d5f297da2b';


UPDATE title
SET entry_name='Columbia et Royal Bar-lock à écriture visible ... Agence générale F. Rubsam, 38bis rue Vivienne Paris : [affiche] / PF Grignon'
WHERE title.id_uuid = 'qr126ffca4fdead4d56a1f59b5cb3038999';


UPDATE title
SET entry_name='Thés de la Porte chinoise, importation directe des meilleurs thés de la Chine ... A la "Porte Chinoise" 36 rue Vivienne Paris : [affiche]'
WHERE title.id_uuid = 'qr148d25e69ae1741bca81b1fbc579aa360';


UPDATE title
SET entry_name='Le Floral, Paris 38 rue Notre-Dame des Victoires. Place de la Bourse et chez les principaux grainiers, épiciers etc... : [affiche]'
WHERE title.id_uuid = 'qr1bb06bf3ee2ab4cfea036f968647a70be';


UPDATE title
SET entry_name='Bonnet [étiquette commerciale]'
WHERE title.id_uuid = 'qr132fcb84009504698b1f8a128872e650f';


UPDATE title
SET entry_name='Bonnet [papier pour gainage de boîte]'
WHERE title.id_uuid = 'qr15b9c2df2004d47eb9bb77f94b528d22a';


UPDATE title
SET entry_name='The serpentine dance, Loïe Fuller (recto, verso) [carte commerciale] (anglais)'
WHERE title.id_uuid = 'qr179210210f450413287adec2bf8ad99bb';


UPDATE title
SET entry_name='[Moulin (recto, verso)] [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr12e5fc7f3317f43918b3d040ed04c7c0a';


UPDATE title
SET entry_name='[Papillon] (recto, verso) [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr1888239cbcce44e4c872aeec54f3b6a17';


UPDATE title
SET entry_name='Savon Louis XV [emballage]'
WHERE title.id_uuid = 'qr1efaa3454f7434297b5994fdf690065f4';


UPDATE title
SET entry_name='La danza serpentina, Loïe Fuller (recto, verso) [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr113c27e995a1145c38e95d438b50238d1';


UPDATE title
SET entry_name='[Japonaise] (recto, verso) [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr17660d7de923b47cf83608a974a74b77a';


UPDATE title
SET entry_name='Savon Louis XV ; Essence Louis XV ; Parfumerie Louis XV ; Poudre de riz Louis XV [étiquette commerciale]'
WHERE title.id_uuid = 'qr1f4a15bf27b9441a0821052f2216406e8';


UPDATE title
SET entry_name='[Couple en tenue de soirée (recto, verso] [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr18e448f32f9714764b6616ca13230b08e';


UPDATE title
SET entry_name='[Chinois] (recto, verso) [carte commerciale] (portugais)'
WHERE title.id_uuid = 'qr184978b7b394e4ab2b4ad8295d3e885b8';


UPDATE title
SET entry_name='Charles (recto) [carte commerciale]'
WHERE title.id_uuid = 'qr16c6186a9fc5f4e939971c9823ec3ae2b';


UPDATE title
SET entry_name='Charles (verso) [carte commerciale]'
WHERE title.id_uuid = 'qr1ba3e4d111cef4df5bacae48a72cb4919';


UPDATE title
SET entry_name='F. Marquis [cartonnage pour gainage de boîte ]'
WHERE title.id_uuid = 'qr13ea1f48d93aa4b7ca93f042d271a7164';


UPDATE title
SET entry_name='F. Marquis [étiquette commerciale]'
WHERE title.id_uuid = 'qr1de68aa0bd6d645e98dfac093b29b78f6';


UPDATE title
SET entry_name='F. Marquis [cartonnage pour gainage de boîte ]'
WHERE title.id_uuid = 'qr1e80006d168db423889d6101c74270252';


UPDATE title
SET entry_name='F. Marquis [étiquette commerciale]'
WHERE title.id_uuid = 'qr1b2a8ea376505468ab0aa617c8932d793';


UPDATE title
SET entry_name='F. Marquis [cartonnage pour gainage de boîte ]'
WHERE title.id_uuid = 'qr1c7b69d2bf3464f899340389ad7cb8d51';


UPDATE title
SET entry_name='Lire dans le Gil Blas, l''Argent. Roman inédit par E. Zola : [affiche] / Chéret'
WHERE title.id_uuid = 'qr1d02b0f0f31d24289b49b229df4ca20d7';


UPDATE title
SET entry_name='Demain la neige.Club Méditerranée, 8 rue de la Bourse Paris (2e) : [affiche] / Dlem'
WHERE title.id_uuid = 'qr16be8a06fbf324541b6c61589e9c9bbc9';


UPDATE title
SET entry_name='Exposition des femmes célèbres et des personnalités féminines françaises du XIXe siècle : salle de La Fronde, 47 rue Vivienne : [Affiche]'
WHERE title.id_uuid = 'qr15a00b6abf03d4f84a7db53c34d476df1';


UPDATE title
SET entry_name='Exposition des femmes célèbres et des personnalités féminines françaises du XIXe siècle : conférence samedi 28 octobre, à 9h. du soir, 47 rue Vivienne : [Affiche]'
WHERE title.id_uuid = 'qr116586f5e35ee4961a8d76d2ec018313b';


UPDATE title
SET entry_name='Sauteurs (Acrobats)'
WHERE title.id_uuid = 'qr1582ce1e6f6d645a0adc2c8e45f0d946a';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1e8d9651f2b6c4dbb89a44a55f292586b';


UPDATE title
SET entry_name='Que je voudrais avoir vos ailes'
WHERE title.id_uuid = 'qr11dcd7a472bc34f56a8e61d240a45842b';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr18690674cfb254f18bec53ad2fdeca8b2';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1281075459e9c4862bc55ea095d28c7be';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1895c72c8f07841cdb9cee5b114989fcd';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1d309fb558a674e63b55017d35d6fff29';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1db43dcdee687478082af4ac4423ff733';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr11c3864fc641442d3ad66145db01fd302';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1c57a2ee60d27435b821c2392332478de';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr19c1452e69fbe48039603858f5540193b';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr10ce50748cbe84061a05480a51b7050bc';


UPDATE title
SET entry_name='Rue Vivienne la nuit'
WHERE title.id_uuid = 'qr1ca5bb6fbacb24c299bd9d2ecb011b4cc';


UPDATE title
SET entry_name='Rue Vivienne la nuit'
WHERE title.id_uuid = 'qr1f80d620ec4a54d4b8faea00534a84943';


UPDATE title
SET entry_name='Rue Vivienne la nuit'
WHERE title.id_uuid = 'qr1cf6b62a141c44285b6b5c135f64dcec2';


UPDATE title
SET entry_name='Les Amusements de l''hiver'
WHERE title.id_uuid = 'qr138cbba3b7a3f4fa4ae439dc8f8913c95';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1aaccdd46d2f84e288ba471828798401d';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr1d8fd229a88164b089cc6c5449bed7450';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr15c09a49e4beb4f209b08230d9524f099';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr1878652bf53fd42f2baf8ed5f03b17ba8';


UPDATE title
SET entry_name='Le Psautier'
WHERE title.id_uuid = 'qr16be5eaa16a7249b097ea99f8958a3d2b';


UPDATE title
SET entry_name='Riez pourtant, du sort ignorez la puissance'
WHERE title.id_uuid = 'qr16d35cdebb32f460298f9717388df79b0';


UPDATE title
SET entry_name='Didon'
WHERE title.id_uuid = 'qr1b2af03170fc544658fb7ca5fc4967074';


UPDATE title
SET entry_name='Spahis'
WHERE title.id_uuid = 'qr1bba8487a761a4bb5996f7500064a1eda';


UPDATE title
SET entry_name='La soeur de Didon'
WHERE title.id_uuid = 'qr136dd3e29d2844b42a43f6a3582cf2ec3';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr186d96b6e4635471198ba676c3429f0f2';


UPDATE title
SET entry_name='Title-page of the Contes de La Fontaine'
WHERE title.id_uuid = 'qr1373a7e4d515b426abd57232a5898f870';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr17baaa1537eb74a9abf4226624fe37354';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr1033542c502954f429682824e0f46deca';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1ea61ed5307f34b288581b4a5df27a763';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr18c9b04c65d494b5ab1b281f2cf27da7a';


UPDATE title
SET entry_name='La Matrone d''Ephèse'
WHERE title.id_uuid = 'qr106868e8d4cbf427194ecde109da8244f';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1b80019128fd840d2ad61d4be4dbb7e8f';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr101ced721fa2c478286de79a13013d51a';


UPDATE title
SET entry_name='Capitaine de frégate, petitie tenue, Capitaine de vaisseau, Grande tenue'
WHERE title.id_uuid = 'qr1d96e8d0a7c46482f9cfb79f0dc7c5e85';


UPDATE title
SET entry_name='Nicaise'
WHERE title.id_uuid = 'qr1c8e64c20722844ba9114fecd277dc297';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr16a568374d5564c98ba21c70fffe5dfc6';


UPDATE title
SET entry_name='La Courtisanne Amoureuse'
WHERE title.id_uuid = 'qr12cbd6a379112430182ce77719d254ee2';


UPDATE title
SET entry_name='Le Fleuve Scamandre'
WHERE title.id_uuid = 'qr18769620c44934ccf81c81dfcf2af7067';


UPDATE title
SET entry_name='Zouaves'
WHERE title.id_uuid = 'qr1c27596145eda47d4b85a6c20c79fe9df';


UPDATE title
SET entry_name='F. Cramer Esq.r'
WHERE title.id_uuid = 'qr1a2fc2a71304642f0944f5dbe40f57a10';


UPDATE title
SET entry_name='Leo XIII'
WHERE title.id_uuid = 'qr195e9dbc227e145029c05a13f9ce0f0c3';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1f59fcdcf6c564787a36fc66f05cd30a1';


UPDATE title
SET entry_name='Le tableau'
WHERE title.id_uuid = 'qr11561cce1ac1e4808b7eeea48c47a7cdd';


UPDATE title
SET entry_name='Beauvais Poque, Colonel, aide-de-camp du général Lafayette, blessé dans la forêt de Rambouillet le 3 Août Mil huit cent trente'
WHERE title.id_uuid = 'qr1317e4280450047cdb7b79b10b7ce1196';


UPDATE title
SET entry_name='Les Trois Commères'
WHERE title.id_uuid = 'qr1ce09f3edda774a3fafba203d9178ef97';


UPDATE title
SET entry_name='Le tableau'
WHERE title.id_uuid = 'qr1ed20032ec9534ab4bdee1ab25451d809';


UPDATE title
SET entry_name='La Mandragore'
WHERE title.id_uuid = 'qr156f23c8bbb324e199cc57b9247fe634a';


UPDATE title
SET entry_name='Les Trois Commères'
WHERE title.id_uuid = 'qr1df6c20a7309745d2aedc577198797970';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr124421721512b427eb7012fb1f0375ec9';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr166720fd29deb449faf774fcdf2bb60d0';


UPDATE title
SET entry_name='Aspirants'
WHERE title.id_uuid = 'qr1dab55616010d4f628b836dd3bd0058da';


UPDATE title
SET entry_name='Le Remède'
WHERE title.id_uuid = 'qr192bc0459b5fa4134a61be24ad162109c';


UPDATE title
SET entry_name='Les Deux Amis'
WHERE title.id_uuid = 'qr1fe50770782974611a1e89798ede57004';


UPDATE title
SET entry_name='Le Cuvier'
WHERE title.id_uuid = 'qr1dfb6c75e8f474454902a2a8b832db2aa';


UPDATE title
SET entry_name='Le C... Battu et Content'
WHERE title.id_uuid = 'qr15ba933d97700414f9949eb3fe3d7e412';


UPDATE title
SET entry_name='Une Queue'
WHERE title.id_uuid = 'qr16ccf0e9bc81142cfb45ad1ab0755f4b1';


UPDATE title
SET entry_name='Troupe ambulante'
WHERE title.id_uuid = 'qr118da0a4f22af4f46a41eb24301e4e2f3';


UPDATE title
SET entry_name='Les bienfaiteurs (The benefactors) (The benefactors)'
WHERE title.id_uuid = 'qr1ad1b236550d64f2b956e50218f6048a9';


UPDATE title
SET entry_name='Comedie Bourgeoise (le mariage de figaro)'
WHERE title.id_uuid = 'qr1e7a030c879d84c0f93d57bffcb5943f5';


UPDATE title
SET entry_name='Un parterre (Stalls) (Stalls)'
WHERE title.id_uuid = 'qr1053ca42cff204979b929bfc01617c661';


UPDATE title
SET entry_name='Un comité de lecture (Reading committee) (Reading committee)'
WHERE title.id_uuid = 'qr1afd69af32b2b4616979f6f62a479f8f3';


UPDATE title
SET entry_name='An ancien camarade (An old friend) (An old friend)'
WHERE title.id_uuid = 'qr1ac95fdcc357347199c9d1b170454429b';


UPDATE title
SET entry_name='Un foyer'
WHERE title.id_uuid = 'qr1e0c49e217aba46dfb53b6c53b64efc0f';


UPDATE title
SET entry_name='Un Paradis (Upper balcony) (Upper balcony)'
WHERE title.id_uuid = 'qr14a81c99962e443ba901cfa2caae9ec9c';


UPDATE title
SET entry_name='Cabaleurs (Conspirators) (Conspirators)'
WHERE title.id_uuid = 'qr120cb947898c44bceaa06f1e8afd68e4d';


UPDATE title
SET entry_name='Troupe ambulante (Itinerant troupe) (Itinerant troupe)'
WHERE title.id_uuid = 'qr1d61bccc7aace4109bda1f2303055a0dc';


UPDATE title
SET entry_name='Le Parisien'
WHERE title.id_uuid = 'qr1a210ea2d784b4c49993c5940eda20528';


UPDATE title
SET entry_name='Joconde'
WHERE title.id_uuid = 'qr1e9ead808e1874ac089b9c2ec93365879';


UPDATE title
SET entry_name='Le Villageois qui cherche son veau'
WHERE title.id_uuid = 'qr1de180a4a0a354c9183b32627566750d2';


UPDATE title
SET entry_name='Acte III, scène V (Masquerade) (Masquerade)'
WHERE title.id_uuid = 'qr17980050da055475d9af2566e3381b470';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1e7758d8d08144a528f6cca10b767160e';


UPDATE title
SET entry_name='Königin Luise'
WHERE title.id_uuid = 'qr14a8c7c81a4f348f49ee21a8a9bf8062e';


UPDATE title
SET entry_name='S. E. Monseig.r le Ministre de l''Intérieur Comte Decazes'
WHERE title.id_uuid = 'qr16304ee9bc51b4b6da4d8f0d9954b262c';


UPDATE title
SET entry_name='Le Gascon Puni'
WHERE title.id_uuid = 'qr13a2494f4274b48ecbc8a778066cc2455';


UPDATE title
SET entry_name='La Chose Impossible'
WHERE title.id_uuid = 'qr17df18342af2d40c8b8ce3a1a06850827';


UPDATE title
SET entry_name='Une représentation (A performance) (A performance)'
WHERE title.id_uuid = 'qr173487f7e23c84ef3ba7eb006233f906d';


UPDATE title
SET entry_name='Le Quiquopro'
WHERE title.id_uuid = 'qr1864182be1fcb46d2b79d5f7292c1a348';


UPDATE title
SET entry_name='Chasseurs d''Afrique'
WHERE title.id_uuid = 'qr11114c6bbd77e426082210745bd80febd';


UPDATE title
SET entry_name='Que je voudrais avoir vos ailes'
WHERE title.id_uuid = 'qr1b4f23ae5807444e9a6737e8cf6d446bb';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr12eb8eeee4e38414cb1c487f3cf41c63e';


UPDATE title
SET entry_name='Maison des champs'
WHERE title.id_uuid = 'qr1dae2e2b83e2f42cf995a96af01bea59a';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr176dc975e37a6416bb820156b48446b22';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1074df493ea0f455da213ce908127c5fd';


UPDATE title
SET entry_name='Parisiennes'
WHERE title.id_uuid = 'qr1b5701d0fbe49449496f26c3a0d431902';


UPDATE title
SET entry_name='Le Pré Catelan'
WHERE title.id_uuid = 'qr15396858f25934a819819b1ae68c8d6a6';


UPDATE title
SET entry_name='Le domestique: Monsieur ferait bien de mettre son viel habit... (The servant: Sir, you''d better put your old suit on...) (The servant: Sir, you''d better put your old suit on...)'
WHERE title.id_uuid = 'qr171c49e2398534171bc8722332262af1d';


UPDATE title
SET entry_name='Le 14 Juin'
WHERE title.id_uuid = 'qr1376d1a0c72ff4dc4a7ddf116961d244a';


UPDATE title
SET entry_name='Two young Indians'
WHERE title.id_uuid = 'qr151d02d605d234c0fb64f2a2aeb4f7ea1';


UPDATE title
SET entry_name='A l''Odéon (At the Odéon) (At the Odéon)'
WHERE title.id_uuid = 'qr1c3de3fce078d44ea837eebc1b1d6ff3d';


UPDATE title
SET entry_name='Valse des Sylphes'
WHERE title.id_uuid = 'qr1232ba8f3146c4f63973c3ac976d546a4';


UPDATE title
SET entry_name='View of London from Greenwich'
WHERE title.id_uuid = 'qr1b2f37ddbb4d043f69f5332718587b81a';


UPDATE title
SET entry_name='Une leçon de botanique (Botany lesson) (Botany lesson)'
WHERE title.id_uuid = 'qr153856a1ec8f94d9cbced8b5db8aa1366';


UPDATE title
SET entry_name='Une visite de jour de l''an (A visit on New Year''s Day) (A visit on New Year''s Day)'
WHERE title.id_uuid = 'qr195423ec09cec4dab8148dfc52402968e';


UPDATE title
SET entry_name='Fouché'
WHERE title.id_uuid = 'qr1fd1259cc282b4a5cb434450d74042e3b';


UPDATE title
SET entry_name='Voila donc mon pot de fleurs qui va avoir du soleil... (There, my flower pot will have its sunny spot...) (There, my flower pot will have its sunny spot...)'
WHERE title.id_uuid = 'qr1e811c838b13e4076aaa77ec928e59edd';


UPDATE title
SET entry_name='Le Chasseur'
WHERE title.id_uuid = 'qr1816be9f5f5a846819b2e4652e1344f58';


UPDATE title
SET entry_name='Un Othello Parisien soupçonnant sa chaste moitié (A Parisian Othello suspecting his chaste wife) (A Parisian Othello suspecting his chaste wife)'
WHERE title.id_uuid = 'qr1cfb5de2be36e46c0b52a8b17fd51cedc';


UPDATE title
SET entry_name='Je vous disais tout à l''heure que je ne serais pas fâché que l''affaire d''Orient s''arrangeât (As I was saying, I wish the Oriental question could be solved...) (As I was saying, I wish the Oriental question could be solved...)'
WHERE title.id_uuid = 'qr1a573f39f6cff46f3ab54932722522f9b';


UPDATE title
SET entry_name='Le Chasseur'
WHERE title.id_uuid = 'qr1e1cc8d13efdd4483ab8003358bb5e70c';


UPDATE title
SET entry_name='Voila donc mon pot de fleurs qui va avoir du soleil... (There, my flower pot will have its sunny spot...) (There, my flower pot will have its sunny spot...)'
WHERE title.id_uuid = 'qr18b4d90dcfd8040889abc195a04b7f35b';


UPDATE title
SET entry_name='La quêteuse, ou Pour les pauvres s''il vous plait'
WHERE title.id_uuid = 'qr16187b223feec41eebb220d5c2b1b4d5b';


UPDATE title
SET entry_name='Le Roi Solitaire'
WHERE title.id_uuid = 'qr17db748c19bc84e3595b160b800fb19f6';


UPDATE title
SET entry_name='Villegiature (Holidays) (Holidays)'
WHERE title.id_uuid = 'qr1ad5cb52e493847e5989100a4c9b34ca5';


UPDATE title
SET entry_name='L''anneau d''Hans Carvel'
WHERE title.id_uuid = 'qr1c9cbfcef23444222bfb28bea0e30e9e2';


UPDATE title
SET entry_name='Vous m''excuserez, je vais me mettre à table... (If you''ll excuse me, I''m going to have lunch...) (If you''ll excuse me, I''m going to have lunch...)'
WHERE title.id_uuid = 'qr1594cf8b24750449393cd1ceaeebd063a';


UPDATE title
SET entry_name='J. Haydn'
WHERE title.id_uuid = 'qr13f8a19a797f14f61be5753761f3bad17';


UPDATE title
SET entry_name='La question russe traitée dans un divan (The Russian question discussed on a sofa) (The Russian question discussed on a sofa)'
WHERE title.id_uuid = 'qr16068feef413a47bdae081f7c6ed05452';


UPDATE title
SET entry_name='Ronsard au Tombeau de Marie'
WHERE title.id_uuid = 'qr15225ac0978984f8a97a3f1d0b2f881fb';


UPDATE title
SET entry_name='Croyant l''apercevoir (Thinking they saw it) (Thinking they saw it)'
WHERE title.id_uuid = 'qr1d7d7df1da4ba471e8a3b2cd995a71476';


UPDATE title
SET entry_name='Conduite bien digne d''un ancien pharmacien (A behaviour fitting a former pharmacist) (A behaviour fitting a former pharmacist)'
WHERE title.id_uuid = 'qr18b367836bf4148b4a6b597b2b53dad86';


UPDATE title
SET entry_name='Une ascension en automne... (Ascent in Autumn...) (Ascent in Autumn...)'
WHERE title.id_uuid = 'qr13cba0d8eaf3546c5a954aabbbe72367b';


UPDATE title
SET entry_name='N''bougez pas! vous êtes superbe comme ça (Don''t move! You look great.) (Don''t move! You look great.)'
WHERE title.id_uuid = 'qr1b2885a504ca64f149b683ef03b153cde';


UPDATE title
SET entry_name='Le retour à Paris (Return to Paris) (Return to Paris)'
WHERE title.id_uuid = 'qr1acb6006e30fc4611ac3f05f3685fe812';


UPDATE title
SET entry_name='Les crétins! on leur peint un tableau religieux et ils rient... (Idiots! You paint a religious scene and they laugh....) (Idiots! You paint a religious scene and they laugh....)'
WHERE title.id_uuid = 'qr1b6cde02eaa3a4b4685245dc5b28b4abc';


UPDATE title
SET entry_name='Voyons, c''est-y fini?... c''est tout d''même fatiguant de se reposer aussi longtemps qu''ça... (Look, are you finished yet?... it is tiring to rest for such a long time....) (Look, are you finished yet?... it is tiring to rest for such a long time....)'
WHERE title.id_uuid = 'qr1f5984cde0f064fc29173f11802cb466e';


UPDATE title
SET entry_name='Devant le tableau de M. Manet (In front of Mr. Manet''s painting) (In front of Mr. Manet''s painting)'
WHERE title.id_uuid = 'qr14a13e85ad5aa449899ddd0c5741bfb71';


UPDATE title
SET entry_name='Monsieur Prudhomme, je me noie!.... (Mister Prudhomme, I''m drowning!...) (Mister Prudhomme, I''m drowning!...)'
WHERE title.id_uuid = 'qr1d8f8af988b074ec19f67f246c41fbafa';


UPDATE title
SET entry_name='La Visiteuse (The Visitor) (The Visitor)'
WHERE title.id_uuid = 'qr10fd2f47f2fa04f5eb21426028f6265cd';


UPDATE title
SET entry_name='Devant le tableau de Mr Gustave Moreau (Before Mr Moreau''s painting) (Before Mr Moreau''s painting)'
WHERE title.id_uuid = 'qr18d533b85daf24fd89eab8921c16da8b0';


UPDATE title
SET entry_name='La première leçon de natation (First swimming lesson) (First swimming lesson)'
WHERE title.id_uuid = 'qr1a92c6ce39ad941bb89af2e205b024e42';


UPDATE title
SET entry_name='Oui Monsieur Rifolet, on dit que les Cosaques n''ont l''air de marcher sur Constantinople que pour mieux nous tromper... (Yes Mister Rifolet, the Cossacks are said to be marching against Constantinople in the sole purpose of tricking us...) (Yes Mister Rifolet, the Cossacks are said to be marching against Constantinople in the sole purpose of tricking us...)'
WHERE title.id_uuid = 'qr134e703baabd0430c913c99743d4151ab';


UPDATE title
SET entry_name='Pauvre fleur! pauvre femme!'
WHERE title.id_uuid = 'qr10219726a786543b5916cc10c9a886ba2';


UPDATE title
SET entry_name='La chiromancie, nouveau passe-temps des bons Parisiens... (Chiromancy, the new pastime for Parisians...) (Chiromancy, the new pastime for Parisians...)'
WHERE title.id_uuid = 'qr158806bfa46954c7382228858ff81fda4';


UPDATE title
SET entry_name='Voyons, ne soyez donc pas bourgeois comme ça.... (Come on, don''t be such a middle-class man....) (Come on, don''t be such a middle-class man....)'
WHERE title.id_uuid = 'qr11657ce3ebd004209b2cfa9a781b55042';


UPDATE title
SET entry_name='Devant les tableaux de Meissonier (In front of Meissonier''s paintings) (In front of Meissonier''s paintings)'
WHERE title.id_uuid = 'qr13477fa436e4e4633abd42543be237ffa';


UPDATE title
SET entry_name='Recevant la férule'
WHERE title.id_uuid = 'qr14d775c6cbde046faa170dda03a269356';


UPDATE title
SET entry_name='Le Mari Confesseur'
WHERE title.id_uuid = 'qr1474d71507bf440b19988a59d69687d0f';


UPDATE title
SET entry_name='Mossieu le propriétaire! (Mr Landlord!) (Mr Landlord!)'
WHERE title.id_uuid = 'qr1b85f9478e1e94a91935e3bd03c4ef944';


UPDATE title
SET entry_name='Mars: Monsieur Prud''homme, je vous ai fait bien peur en arrivant!... (Mars: Mister Prud''homme, I scared you when I arrived didn''t I?...) (Mars: Mister Prud''homme, I scared you when I arrived didn''t I?...)'
WHERE title.id_uuid = 'qr1bcc04bed715c46869f8a18e7d020f258';


UPDATE title
SET entry_name='Le malade: comment docteur, pas même un oeuf à la coque!... (The patient: what, doctor? not even a boiled egg?...) (The patient: what, doctor? not even a boiled egg?...)'
WHERE title.id_uuid = 'qr1180384260c414c798bdd3dc4edf8388f';


UPDATE title
SET entry_name='En v''là un qu''est un drôle de corps... y tire l''portrait d''un arbre qui n''produit seulement point des pommes (Here''s a weird one... portraying a tree that doesn''t even bear apples...) (Here''s a weird one... portraying a tree that doesn''t even bear apples...)'
WHERE title.id_uuid = 'qr1f0726574d56749cdb6a16a3da97362ec';


UPDATE title
SET entry_name='Eh! bien voisin.... pourquoi avez-vous poussé un si grand cri.... (Well, neighbour, why did you shout that loud?) (Well, neighbour, why did you shout that loud?)'
WHERE title.id_uuid = 'qr1123599b9c98c4893a04260fc02eb8c9b';


UPDATE title
SET entry_name='Quelle société abatardie et et corrompue que la nôtre!.... (Ah, the bastardized and corrupted society we''re living in!....) (Ah, the bastardized and corrupted society we''re living in!....)'
WHERE title.id_uuid = 'qr1f62ec7127a434044b8fc7b9d7a06d69d';


UPDATE title
SET entry_name='A femme avare galant escroc'
WHERE title.id_uuid = 'qr13ee0e48f51634c2e8a4dbd0accf77f48';


UPDATE title
SET entry_name='Viens donc..., mon ami, je ne trouve pas ce tableau joli... (Say my dear, I do not think this painting is beautiful) (Say my dear, I do not think this painting is beautiful)'
WHERE title.id_uuid = 'qr17619fb77545f4ee98adb223fe013d9ab';


UPDATE title
SET entry_name='Etant parvenu à s''introduire dans l''intérieur de la Bourse... (Having managed to go inside the Bourse...) (Having managed to go inside the Bourse...)'
WHERE title.id_uuid = 'qr181009150b4aa4555b95bc6323f90d62a';


UPDATE title
SET entry_name='Monsieur veut-il des crèpes?... (Would you like some pancakes, Sir?) (Would you like some pancakes, Sir?)'
WHERE title.id_uuid = 'qr181ba314d921c438d924bd2c27311cc75';


UPDATE title
SET entry_name='Dis donc, notre homme, faut-y avoir une drôle d''idée pour faire sa portraiture comme ça (I say, one must have weird ideas to have his portrait done like that!) (I say, one must have weird ideas to have his portrait done like that!)'
WHERE title.id_uuid = 'qr15acec83fa73f4b0b98648e431f88d93a';


UPDATE title
SET entry_name='La Liseuse (The Reader) (The Reader)'
WHERE title.id_uuid = 'qr1c38abc5c153f44f69576869c02ed05eb';


UPDATE title
SET entry_name='Quelle fichue idée j''ai eu de venir chasser dans ces satanées plaines de la Sologne ...'
WHERE title.id_uuid = 'qr18f8588ee8afb47378e677fdcca405a1b';


UPDATE title
SET entry_name='Les paysagistes; le premier copie la nature, le second copie le premier (Paysagists; the first works after nature, the second works after the first) (Paysagists; the first works after nature, the second works after the first)'
WHERE title.id_uuid = 'qr1db642514182b4ed8b80d5a87e0b6c679';


UPDATE title
SET entry_name='Le Roi Candaule'
WHERE title.id_uuid = 'qr120ae66d5003a425d812cc81d904b1291';


UPDATE title
SET entry_name='Quel fichu temps pour battre le rappel! (Such a bad weather for a call to arms!) (Such a bad weather for a call to arms!)'
WHERE title.id_uuid = 'qr140a5ef6675174ae5a832a9be588aa1ee';


UPDATE title
SET entry_name='Qui?...'
WHERE title.id_uuid = 'qr1f6c24b1a747a43fd9355f16383d79f10';


UPDATE title
SET entry_name='Eh! bien crois-tu que je serai embarassé pour vendre avantageusement cette étude là (Well, do you reckon I will have any trouble selling this study at a good price?) (Well, do you reckon I will have any trouble selling this study at a good price?)'
WHERE title.id_uuid = 'qr1adeedb7fe2754759bccfbb0f1efa8567';


UPDATE title
SET entry_name='La Servante justifiée'
WHERE title.id_uuid = 'qr1958c48f8b3d2470fa78e330832608fa0';


UPDATE title
SET entry_name='Tiens papa, te v''là!... (Look daddy, there you are!...) (Look daddy, there you are!...)'
WHERE title.id_uuid = 'qr1be2c250741ac4b469e2e5bca637d0109';


UPDATE title
SET entry_name='Le Néophyte'
WHERE title.id_uuid = 'qr17e32b196c8094e4882048958137f7c46';


UPDATE title
SET entry_name='Dire que nous v''là Parisiens! (And to think we''re Parisian now!) (And to think we''re Parisian now!)'
WHERE title.id_uuid = 'qr10aa7a6675c5047fcb353f2181f857657';


UPDATE title
SET entry_name='Les trois disciples de Mr Cobden se livrant à une dernière tentative pour faire apprécier aux cosaques tous les charmes de la paix (Mg Cobden''s three disciples in their final attempt at showing the delights of peace to cossacks) (Mg Cobden''s three disciples in their final attempt at showing the delights of peace to cossacks)'
WHERE title.id_uuid = 'qr179e05ac75075451ca6b96d5c35ad0b30';


UPDATE title
SET entry_name='Le visiteur: oh! pour le coup voilà une composition qui est réellement insensée... (The visitor: oh! now that''s a weird composition....) (The visitor: oh! now that''s a weird composition....)'
WHERE title.id_uuid = 'qr15804bd5e31d940ee9c47cd36f095947d';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr112a99fe9fa4c4912a5858c1139d637a7';


UPDATE title
SET entry_name='Le Calendrier des Vieillards'
WHERE title.id_uuid = 'qr14b3da5d8bd7f4278b7ffc46a90c4a989';


UPDATE title
SET entry_name='Un dialogue à la Pointe St Eustache'
WHERE title.id_uuid = 'qr1aced36c6bd944526b47564a428059334';


UPDATE title
SET entry_name='Les Trois Commères'
WHERE title.id_uuid = 'qr1d7518760716c4652a036360f4a791993';


UPDATE title
SET entry_name='Sapeurs pompiers'
WHERE title.id_uuid = 'qr15d04ccbb69aa42a5b3057a6dbb13ea3a';


UPDATE title
SET entry_name='L''adieu'
WHERE title.id_uuid = 'qr1eadba74b103e48648e505a64add88e3f';


UPDATE title
SET entry_name='Artistes en train d''examiner le tableau d''un rival (Artists examining a painting done by a rival) (Artists examining a painting done by a rival)'
WHERE title.id_uuid = 'qr1fdcccb4fc29e468b9f4fd7814a51e652';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1291410ecad3f40379a3cae18bf84711f';


UPDATE title
SET entry_name='Fais tinter ta clochette'
WHERE title.id_uuid = 'qr1b2c7c3c1b37c44979a342333ea44bf66';


UPDATE title
SET entry_name='On ne s''avise jamais de tout Series: Series: Contes'
WHERE title.id_uuid = 'qr197a5756c7b4240399d49450b908af69c';


UPDATE title
SET entry_name='Mamans de comédie'
WHERE title.id_uuid = 'qr11f2474f2929641c6a24dbd1bb2ec12b1';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1ad7f235522ae4c37bdf628ef6a5bd100';


UPDATE title
SET entry_name='Une débutante'
WHERE title.id_uuid = 'qr1bd1a912c60ac4ffebfbd6074ffc32695';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr11befdfbd2b4743799139c6d554cc6aa3';


UPDATE title
SET entry_name='Capitaine de vaisseau, Lieutenant de vaisseau'
WHERE title.id_uuid = 'qr1581669c3869e4587b5c35df52a47198c';


UPDATE title
SET entry_name='Hussards, Régt.'
WHERE title.id_uuid = 'qr184055c91d6f14875b6b81e8ab6ca2722';


UPDATE title
SET entry_name='Trop & trop peu, (ou les gens du jour et du lendemain)'
WHERE title.id_uuid = 'qr12bf89e038884414eb7df79239b0e3e18';


UPDATE title
SET entry_name='Chasseurs à pied'
WHERE title.id_uuid = 'qr1f9da9f531dfb4ca78ff055b42bdd6910';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1bf8be9680e0b4343b3904981d6287d48';


UPDATE title
SET entry_name='Leçon de déclamation (A lesson in oration) (A lesson in oration)'
WHERE title.id_uuid = 'qr1aa87316763c8492a9297f198016aef0b';


UPDATE title
SET entry_name='Chef d''emploi'
WHERE title.id_uuid = 'qr167a045e822094ae1a03c52e0b595193b';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr151dd77b26ac043adb87ed7972638e827';


UPDATE title
SET entry_name='Le corps de ballet'
WHERE title.id_uuid = 'qr18b863b9cb96341cd83dd296684c57cd0';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr12a4a5781f0a34429be5671d6524f7fb2';


UPDATE title
SET entry_name='Tragédiens (Tragic actors) (Tragic actors)'
WHERE title.id_uuid = 'qr1116addb52ab1412fb9a1b15bcdfda54a';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1f6b9a2bcbae74b1c9ed708c701a6d07f';


UPDATE title
SET entry_name='Le Follet'
WHERE title.id_uuid = 'qr1fef7d3ed739847b3a5b756663ab6973a';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr17f6dc59e6b9740409ba4437bb281294a';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr153a5066c5ea8422984d482d4baf41647';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr1c1ed0b7b446249079e557b3156109496';


UPDATE title
SET entry_name='Une loge'
WHERE title.id_uuid = 'qr1f9b8c020f4d64831a8cb864cd3012cda';


UPDATE title
SET entry_name='Phénomènes'
WHERE title.id_uuid = 'qr17c386c00e0a04aae90f4180924e316b6';


UPDATE title
SET entry_name='Cabaleurs (Conspirators) (Conspirators)'
WHERE title.id_uuid = 'qr12536d40f472c491d864aa6ad3a0f09d7';


UPDATE title
SET entry_name='Sauteurs (Acrobats) (Acrobats)'
WHERE title.id_uuid = 'qr1aae078289f77467eae5dc55dc7ec8676';


UPDATE title
SET entry_name='Le Faucon'
WHERE title.id_uuid = 'qr1c2a6c9b2e1bb40a7bc9ca3df3068e759';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr130087e257cf44d8ab100b4cf402a475f';


UPDATE title
SET entry_name='Gendarmerie mobile'
WHERE title.id_uuid = 'qr136cdd82f826e44449f7c5438fa88a752';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1828c097afb7e46eda96d0537019298ec';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1ad368e75f1d34e49af1076fc023538c9';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr17db1d94866634008aca6cb46a2eabaf0';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr13cd15c9b02d54648a70e6f3c92b8ed4e';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr168c134f327324b01bce14f9e2cefc1c7';


UPDATE title
SET entry_name='Une indisposition'
WHERE title.id_uuid = 'qr1dadeb1342a7d4261ad265a3c34a05388';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr18bffd7e977d24faf91f8ed58cddad91c';


UPDATE title
SET entry_name='Le derrière de la toile (Behind the curtain) (Behind the curtain)'
WHERE title.id_uuid = 'qr10a810629fddb4850bcbd6e9babf6feeb';


UPDATE title
SET entry_name='Un foyer'
WHERE title.id_uuid = 'qr13fc417459aaf43adba8f23cc285585ca';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr11b31f314c9c147adadda805da86e7989';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1fa83cafe3ee444ff9b5bad92a0e6d9a3';


UPDATE title
SET entry_name='Modes de Paris'
WHERE title.id_uuid = 'qr184286fd448584db88dbedab87c304900';


UPDATE title
SET entry_name='Garde Républicaine'
WHERE title.id_uuid = 'qr1a2ee9be4c0f9492d99c571c00a0ee8f1';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr10005fa8222e64f81a93c5488e6eb25e1';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1dc9754f3146044c49182b226b03f984d';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr188ae652ee0934be89f8f48b34e24601a';


UPDATE title
SET entry_name='Le Petit courrier des dames'
WHERE title.id_uuid = 'qr114d212de93ab48d196545ebf153e4be8';


UPDATE title
SET entry_name='Phénomènes (Phenomena) (Phenomena)'
WHERE title.id_uuid = 'qr183eab56e8c63453eba3a84fda75086db';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1c97baf2749b1409995d3e5808b036a93';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr15d1d15cd9e814a0b9ddd6bfb3352df2b';


UPDATE title
SET entry_name='Le Follet'
WHERE title.id_uuid = 'qr1f98591c2006c4922b44ddc015f48c9a9';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1e60eedfc767943098decb3b0dead4571';


UPDATE title
SET entry_name='Une grande coquette (A great flirt) (A great flirt)'
WHERE title.id_uuid = 'qr184b22bb1e2a14a699eac8df1d7edcc90';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr18c63a11e2df145d7ba397b76e4c96258';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1e25f8feb8444485a8d85feb0e2855964';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr103038a845b9647c3b86d7c51bc3e0db7';


UPDATE title
SET entry_name='Une répétition'
WHERE title.id_uuid = 'qr1a717a208cec646ff882b2c094574274a';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr1669bce0f1cb14edeb31eb372fcb47604';


UPDATE title
SET entry_name='Sapeurs du génie'
WHERE title.id_uuid = 'qr1e6dbceca5f46488a86e4d19437b95762';


UPDATE title
SET entry_name='Le Follet'
WHERE title.id_uuid = 'qr19f268a668e39449f83d78a9eb8dc7564';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr19fa50f708d7443e7bb1546c6d5c6fa69';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr12f3e1efad70540afb75b0690cf245cb5';


UPDATE title
SET entry_name='Journal du Monde Elegant'
WHERE title.id_uuid = 'qr152f9a83dfe1243a2bd8bd513ebf6dd41';


UPDATE title
SET entry_name='Journal des modes ridicules'
WHERE title.id_uuid = 'qr18c08a8b7d08746e9a168883400eca31a';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr13f4a2d19189744bf9c9e7e76d4198e95';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr16cf2561f752f4bf6a3ce203250a26838';


UPDATE title
SET entry_name='Garde nationale à cheval'
WHERE title.id_uuid = 'qr1b4adffcf11eb42f696b79b395b893cfd';


UPDATE title
SET entry_name='Douanes'
WHERE title.id_uuid = 'qr1bf88d677a56740509e430567e4e8bcbd';


UPDATE title
SET entry_name='Journal des ridicules'
WHERE title.id_uuid = 'qr13d71a35e7f3c4981865606cfc40baf9b';


UPDATE title
SET entry_name='Garde nationale'
WHERE title.id_uuid = 'qr15bee72beacd54ccdba10837a372ce346';


UPDATE title
SET entry_name='Le Moniteur de la Mode'
WHERE title.id_uuid = 'qr176deb1b3e1dd476e890a309c0c8e778a';


UPDATE title
SET entry_name='Les Béarnaises'
WHERE title.id_uuid = 'qr1799704914c014643af7e03422f839b66';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr119b39416f2f14718bb46de5cef99df19';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr1da27a2da45064777b7cd59034c28e5d8';


UPDATE title
SET entry_name='The Reefer'
WHERE title.id_uuid = 'qr1dd5fe28f8df046c8b3129c2894c4194c';


UPDATE title
SET entry_name='Mme Dorval, rôle de Catarina Bragadini dans Angelo, Comédie Française'
WHERE title.id_uuid = 'qr11dfb5789968f46c5aee6196ceb457941';


UPDATE title
SET entry_name='Mme Favart'
WHERE title.id_uuid = 'qr10a332a1f0e3f4e4bb267845a71ac2f3a';


UPDATE title
SET entry_name='M.lle Rachel Artiste et Pensionnaire du Théâtre Français'
WHERE title.id_uuid = 'qr1c1a3a402c0f14ccab85e70db4d2f7ba2';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr16ad1487c80354b84b95c50224251481a';


UPDATE title
SET entry_name='Costume de chasse du temps de Louis XIV'
WHERE title.id_uuid = 'qr1814fda1394d748be98deeacc01fc1fe3';


UPDATE title
SET entry_name='L''ancien négociant (The former merchant) (The former merchant)'
WHERE title.id_uuid = 'qr10538dcdaa10e4a6f842985e9cd6010fb';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr158b886a18e2e456a8d65bf0274d064dc';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr11cdcff93f68c4cfa865cf7884b5759d3';


UPDATE title
SET entry_name='Les Modes Parisiennes'
WHERE title.id_uuid = 'qr15d299e9f19e647d8805eb90cdad7569b';


UPDATE title
SET entry_name='Ce que le bourgeois est convenu de nommer une petite distraction (What the bourgeoisie calls a slight distraction) (What the bourgeoisie calls a slight distraction)'
WHERE title.id_uuid = 'qr191e74ce9cdfe43b5b8d622d07738d715';


UPDATE title
SET entry_name='Palais de la Bourse'
WHERE title.id_uuid = 'qr1d64d6361073144babc899afdc131eee5';


UPDATE title
SET entry_name='La Bourse'
WHERE title.id_uuid = 'qr1b222452739e5482ab66c39b3972c0567';


UPDATE title
SET entry_name='La Bourse'
WHERE title.id_uuid = 'qr103df9d6796d74991bd86497940e9afcc';


UPDATE title
SET entry_name='Un obligeant Cicerone (A helpful guide) (A helpful guide)'
WHERE title.id_uuid = 'qr1288b40e8619048d9bc4a8c998c2d8854';


UPDATE title
SET entry_name='La Danse des Vivants'
WHERE title.id_uuid = 'qr1b42b55512fce421e9d1af1c5e6718672';


UPDATE title
SET entry_name='La Danse des Vivants'
WHERE title.id_uuid = 'qr1a2e1ef25a7f54c0499a4742f8b63ed70';


UPDATE title
SET entry_name='Une panique à la Bourse (Panic at the Stock Exchange) (Panic at the Stock Exchange)'
WHERE title.id_uuid = 'qr12c6eb09d20a749e587993e5fe0dea4f4';


UPDATE title
SET entry_name='Paris pendant le siège'
WHERE title.id_uuid = 'qr1800e8a06ca53464a850bcccc7c5f20b9';


UPDATE title
SET entry_name='Qué qu''vous êtes donc devenu, M''sieu Lebrun?... (What''s up with you, Mr Lebrun?) (What''s up with you, Mr Lebrun?)'
WHERE title.id_uuid = 'qr17e14c9a418ff46229f022b607bebf8e4';


UPDATE title
SET entry_name='Grands Exercices de souplesse de reins, et de voltige exécutés devant le palais de la Bourse'
WHERE title.id_uuid = 'qr19a2cfaa3499a4459813ad6998587d2ad';


UPDATE title
SET entry_name='Imitation libre d''un tableau de Mr. Horace Vernet, représentant le massacre des Janissaires'
WHERE title.id_uuid = 'qr13300e903602e459491fa0766b87823e4';


UPDATE title
SET entry_name='L''indispensable visite chez le tailleur du palais royal (The mandatory visit to the tailor at the Palais Royal) (The mandatory visit to the tailor at the Palais Royal)'
WHERE title.id_uuid = 'qr130375878a4214bebb7d0e1b6cbb06f96';


UPDATE title
SET entry_name='Tenues de soirée sans façon'
WHERE title.id_uuid = 'qr1172ebb84f11a438ba4ebf36a07d821f9';


UPDATE title
SET entry_name='Vue du Pont Royal (prise du Pont Louis XV)'
WHERE title.id_uuid = 'qr1031dccc7fde8477bb03f188ea55377f0';


UPDATE title
SET entry_name='Blois (Loir et Cher)'
WHERE title.id_uuid = 'qr12be5f9095a7c4dd388b9741377069019';


UPDATE title
SET entry_name='Title-page, text and two sheets with two images each, for ''Paris and its Environs'' (London: Jennings, 1829)'
WHERE title.id_uuid = 'qr1ffcc6c036dba48dfa472b0b5d55f722b';


UPDATE title
SET entry_name='L''huitre (The oyster) (The oyster)'
WHERE title.id_uuid = 'qr1c690c53423844f288d8f55ce46743b56';


UPDATE title
SET entry_name='Boulevard du Temple'
WHERE title.id_uuid = 'qr1ff5b5d4483704b11b6043d03f82bc8cf';


UPDATE title
SET entry_name='Rotonde, passage colbert'
WHERE title.id_uuid = 'qr1911e1f13417047149e80b1827a675bc5';


UPDATE title
SET entry_name='Les Mauvais conseils'
WHERE title.id_uuid = 'qr1de66ae97a3514b4bb6994a3993dd2c76';


UPDATE title
SET entry_name='L''Intempérance'
WHERE title.id_uuid = 'qr1243846fc6d81475e814293029605568b';


UPDATE title
SET entry_name='[Recueil de menuiserie et de décorations intérieures et extérieures] [suivi de] Antiquités, monumens et vues pittoresques du Haut-Poitou [suivi de] Recueil de monumens ornés'
WHERE title.id_uuid = 'qr1f8bc3cf77ad94b1c958ab8ca744e41d9';


UPDATE title
SET entry_name='4ème Cahier Pl. 20 - [Décorations extérieures de boutiques] - Rue Saint Benoit ; Rue Neuve des Petits-Champs ; Place de la Bourse ; Rue Neuve Saint-Eustache'
WHERE title.id_uuid = 'qr1ceb9926f8a27448792a55050b8a37b83';


UPDATE title
SET entry_name='[Recueil de menuiserie et de décorations intérieures et extérieures] [suivi de] Antiquités, monumens et vues pittoresques du Haut-Poitou [suivi de] Recueil de monumens ornés'
WHERE title.id_uuid = 'qr19d70c61b5efe4701bd45e6c5d01de4fe';


UPDATE title
SET entry_name='4ème Cahier Pl. 22 - [Décorations extérieures de boutiques] - Rue Vivienne ; Rue du Four ; Rue de Richelieu'
WHERE title.id_uuid = 'qr19d34db2d212149e9b2c62aba76e55c37';


UPDATE title
SET entry_name='Paris, Hôtel Desmarais, 18 Rue Vivienne : [heurtoir, IIe arrt]'
WHERE title.id_uuid = 'qr16594d7800a764021a4a573f2e9fb9614';


UPDATE title
SET entry_name='Paris, Hôtel Desmarais, 18 Rue Vivienne : [IIe arrt]'
WHERE title.id_uuid = 'qr105029b92faac4c8a893eebfba8f8e304';


UPDATE title
SET entry_name='Paris, Bibliothèque Nationale sur la Rue Vivienne, Ancien Hôtel Mazarin : [cour intérieure, IIe arrt]'
WHERE title.id_uuid = 'qr14422f94b88664ca580ba3095e33f5d86';


UPDATE title
SET entry_name='Paris, Bibliothèque Nationale sur la Rue Vivienne, Ancien Hôtel Mazarin : [cour intérieure, IIe arrt]'
WHERE title.id_uuid = 'qr1ff97d7ab91a740c39cdaa2707b9a9a9d';


UPDATE title
SET entry_name='Paris, Bibliothèque Nationale sur la Rue Vivienne, Ancien Hôtel Mazarin : [cour intérieure, IIe arrt]'
WHERE title.id_uuid = 'qr1ecfa94e4140a4fe4824b3951ec80ff03';


UPDATE title
SET entry_name='Paris, Bibliothèque Nationale sur la Rue Vivienne, Ancien Hôtel Mazarin : [cour intérieure, IIe arrt]'
WHERE title.id_uuid = 'qr17800605463e3448e8f985e615f3bb429';


UPDATE title
SET entry_name='[L''Art ancien, photographies des collections célèbres, par Franck. Volume 2]'
WHERE title.id_uuid = 'qr1727577378b074c009029f432c1ad7df6';


UPDATE title
SET entry_name='L''Art ancien, photographies des collections célèbres, par Franck. Volume 1'
WHERE title.id_uuid = 'qr1b938c4e74a004c98842662160aa5610e';


UPDATE title
SET entry_name='[Dessins de costumes pour le Théâtre du Vaudeville (Paris)]'
WHERE title.id_uuid = 'qr12188870b075740b3b62951f0ef979bcd';


UPDATE title
SET entry_name='Scène des fleurs dansée par Mademoiselle Taglioni, Académie royale de musique'
WHERE title.id_uuid = 'qr15328037ca9e74d0c85fd9ad0492b3cd4';


UPDATE title
SET entry_name='Maisons à l''angle de la rue Neuve Vivienne et du boulevard Montmartre, Plan de l''entresol ; Plan du rez-de-chaussée ; Plan souterrain, planche 139'
WHERE title.id_uuid = 'qr1594f413e2ee9404b83bf95e4a0c3fab6';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr160527890f8c44a8da0af1b41c1296188';


UPDATE title
SET entry_name='Maisons à l''angle de la rue Neuve Vivienne et du boulevard Montmartre, Plan du 6ème étage ; Plan du 4ème étage ; Plan du 1er, 2ème et 3ème étage, planche 140'
WHERE title.id_uuid = 'qr1a795cc90f45f4c9695d6c212bf15509c';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1598a01fe3f694029aba4a9a419ade9b7';


UPDATE title
SET entry_name='Maisons à l''angle de la rue Neuve Vivienne et du boulevard Montmartre, coupe, planche 141'
WHERE title.id_uuid = 'qr175cb2c79cf29448399c9c149a144ba2c';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1f15039e300074a38bc791ee5f52c0b36';


UPDATE title
SET entry_name='Maisons à l''angle de la rue Neuve Vivienne et du boulevard Montmartre, élévations sur le boulevard, planche 142'
WHERE title.id_uuid = 'qr19a13d0f6a8224c8794fcb65001e26725';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr10cfd2e4885574a0d81e1e80fc6d08ac2';


UPDATE title
SET entry_name='Maison rue Neuve Vivienne, n°37, Plan du rez-de-chaussée ; Plan du 1er étage, planche 151'
WHERE title.id_uuid = 'qr16fb000e57a6d459a9930eaa0ce3c874d';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1443aa17f689c4f4fb63e27fe5ac42d3d';


UPDATE title
SET entry_name='Maison rue Neuve Vivienne, n°37, élévation, planche 152'
WHERE title.id_uuid = 'qr199582916199f49f491b5eb373a071ecf';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr143d74add474c46aea6d46d2e9ffe6d7a';


UPDATE title
SET entry_name='Maison rue Neuve Vivienne, n°44, plan du 1er étage ; Plan du 2ème étage ; Plan de l''entresol ; Plan des caves ; Plan du rez-de-chaussée, planche 156'
WHERE title.id_uuid = 'qr170cc13203c874c208896f279693726d6';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr137808741f9b84e8c9eb53ac4e861a02f';


UPDATE title
SET entry_name='Maison rue Neuve Vivienne, n°44, élévation, planche 157'
WHERE title.id_uuid = 'qr154ca911de5454a38a0ed46bad3e927c6';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr108cfcc45f18e4862831cdee3fdf509e2';


UPDATE title
SET entry_name='Maison place de la Bourse, n°10, Plan du 1er étage ; Plan du rez-de-chaussée ; Plan des caves, planche 146'
WHERE title.id_uuid = 'qr1087a5d3c38bf4e13856dcdbf849a862e';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1ac6815f076c64067b71009f0591625d4';


UPDATE title
SET entry_name='Maison place de la Bourse, n°10, élévation, planche 147'
WHERE title.id_uuid = 'qr1a8d310fcc3ea4ec79ab289e7d69d66d4';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1ad8950a762dd4046b234f73a7f91ae9a';


UPDATE title
SET entry_name='Maison place de la Bourse, n°8, Plan des caves ; Plan du rez-de-chaussée, planche 131'
WHERE title.id_uuid = 'qr13af451673ff04f90a550832f05195564';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr10f60ac9317ec4d0e8a10b8918d5961f1';


UPDATE title
SET entry_name='Maison place de la Bourse, n°8, Plan de l''entresol ; Plan du 1er étage, planche 132'
WHERE title.id_uuid = 'qr11a52724903d84c419488934f6f360630';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1dab8c9573988458d93a225e81843e67e';


UPDATE title
SET entry_name='Maison place de la Bourse, n°8, Elévation sur la place de la Bourse ; Coupe, planche 133'
WHERE title.id_uuid = 'qr1a0ff9ffc941145849828a5c6a200f97c';


UPDATE title
SET entry_name='[Maison à l''angle de la rue Vivienne et du Boulevard]. 3eme Feuille. Plan du 6eme Etage. Plan du 4eme Etage. Le 5eme est peu différent du 4eme : [dessin] / J. Lecointe. Arch.'
WHERE title.id_uuid = 'qr18809db507c1b4c1fbdce7c98f12dc21d';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1e85e2d5062ad40de8243d3fefc9e349e';


UPDATE title
SET entry_name='Salles de vente des commissaires priseurs au département de la Seine. Place de la Bourse, n°2, Plan du rez-de-chaussée ; Plan du 1er étage, planche 108'
WHERE title.id_uuid = 'qr1e5f8f19afdcf4586ab642cc5b4b3cebb';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr16963f57823954eb9bfb74abaca1588ca';


UPDATE title
SET entry_name='Salles de vente des commissaires priseurs au département de la Seine. Place de la Bourse, n°2, Coupe, planche 109'
WHERE title.id_uuid = 'qr107f94d6236b74a98a02cd88a88b404ae';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr1814af942d551462b974f832f06512fe6';


UPDATE title
SET entry_name='Salles de vente des commissaires priseurs au département de la Seine. Place de la Bourse, n°2, Elévation sur la place de la Bourse, n°2, planche 110'
WHERE title.id_uuid = 'qr160f70678171f4f3184aeabe216da1791';


UPDATE title
SET entry_name='Paris moderne ou Choix de maisons construites dans les nouveaux quartiers de la capitale et dans ses environs, levées, dessinées, gravées et publiées par Normand fils,... . Première partie [et] Deuxième partie, contenant, outre les maisons, des édifices d''utilité publique de second ordre, tels que marchés, fontaines, bains, écoles, théâtres, bazars, etc., etc.'
WHERE title.id_uuid = 'qr16864862a4d2345f0a1db020499a69074';


UPDATE title
SET entry_name='Mademoiselle Jenny Colon, rôle de Sarah'
WHERE title.id_uuid = 'qr10fb0ec6e20d8487fa6b103c96fa276b4';


UPDATE title
SET entry_name='Dessins de sculpture architecturale'
WHERE title.id_uuid = 'qr12e6a6b0ce9174e2fa751bca7fe40dba6';


UPDATE title
SET entry_name='l''Artisan Moderne'
WHERE title.id_uuid = 'qr115d63aab9c9e447d8cbe5ac1c3323de7';


UPDATE title
SET entry_name='Paris, Ancien Hôtel Mazarin, Bibliothèque Nationale : [fontaine, IIe arrt]'
WHERE title.id_uuid = 'qr16cd6f646e9eb4401a6829c2cd29fbf8f';


UPDATE title
SET entry_name='Paris, Ancien Hôtel Mazarin, Bibliothèque Nationale : [cour intérieure, IIe arrt]'
WHERE title.id_uuid = 'qr1165670f9e9f44df787f9c344156bbd71';


UPDATE title
SET entry_name='Vue du palais de la Bourse Estampe'
WHERE title.id_uuid = 'qr123e28ae6e47c4e9fb4327380577c4973';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La place de la Bourse , vue de la rue Notre-Dame-des-Victoires'
WHERE title.id_uuid = 'qr1b572fcb8182940b0b6d6c39b686feaee';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La place de la Bourse pavoisée pour les fêtes de la Victoire des 13 et 14 juillet'
WHERE title.id_uuid = 'qr14e7ffb807af44530b85dda0b5c93b46b';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La place de la Bourse , vue de la rue Notre-Dame-des-Victoires'
WHERE title.id_uuid = 'qr152ab5e3c8fd148b3b5588bbd3b71b139';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Place de la Bourse et la rue Réaumur , vues de la rue Notre-Dame-des-Victoires'
WHERE title.id_uuid = 'qr1cecba64202f84c9b8bd7d67810b576db';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La place de la Bourse , vue de la rue Notre-Dame-des-Victoires'
WHERE title.id_uuid = 'qr189a467fd5fc04e3893265041ab594f40';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Place de la Bourse vue de la rue de la Banque'
WHERE title.id_uuid = 'qr1ee7652cc13004431a7c1136808e1451f';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La rue Notre-Dame-des-Victoires vue de la place de la Bourse'
WHERE title.id_uuid = 'qr133fe2311814a4a50bef8ef41d3f398b1';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France Les rues Vivienne et Feydeau place de la Bourse'
WHERE title.id_uuid = 'qr13add511805464b5a99cbc5f48a154d10';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Place de la Bourse'
WHERE title.id_uuid = 'qr16029379eab8d45ea81098001117f56a9';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La place de la Bourse , vue de la rue Vivienne'
WHERE title.id_uuid = 'qr1d45dc1ef368048c588dcf4e45d903614';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Bourse de Paris , palais Brongniart'
WHERE title.id_uuid = 'qr14551329422404cf4a1d8ec5a8b768cb7';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La rue de la Bourse et la Bourse de Paris ( palais Brongniart ) depuis la rue des Colonnes'
WHERE title.id_uuid = 'qr13e6b5877cfe2407facec6995ba6b0ecb';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La rue de la Bourse et la Bourse de Paris ( palais Brongniart ) depuis la rue des Colonnes'
WHERE title.id_uuid = 'qr1a857d3ad9bbb44fb813ca72f55a63a67';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France L&apos; emprunt national de 1920 à la Bourse de Paris , palais Brongniart'
WHERE title.id_uuid = 'qr1fe44251f5fa84ffbbb620a83180cc137';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France L&apos; emprunt national de 1920 à la Bourse de Paris , palais Brongniart'
WHERE title.id_uuid = 'qr1bd9b7d139eb64abd9d6b26ddd0c64a4c';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France L&apos; emprunt national de 1920 à la Bourse de Paris , palais Brongniart'
WHERE title.id_uuid = 'qr1c8be843ff1b74b94875feffc0fffad3c';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Bourse décorée pour les fêtes de la Victoire des 13 et 14 juillet 1919'
WHERE title.id_uuid = 'qr18d4df245ab454a599aa4c3bef090d731';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France L&apos; emprunt national de 1920 à la Bourse de Paris , palais Brongniart'
WHERE title.id_uuid = 'qr12ea1d0421f0e41ccaf90ef02500418d1';


UPDATE title
SET entry_name='Paris ( IIe arr . ) , France La Bourse décorée pour les fêtes de la Victoire des 13 et 14 juillet 1919'
WHERE title.id_uuid = 'qr1a7cb08dffe3a4f2d95557c9fde6f67e4';


UPDATE title
SET entry_name='Partant pour la Syrie [ ] Paroles et Musique de La Reine Hortense Paris, Colombier, Editeur, Rue Vivienne, 6 : [estampe]'
WHERE title.id_uuid = 'qr15e6658a0547148fc81516d41e6ca2f11';


UPDATE title
SET entry_name='[Galerie Colbert, rue Vivienne] : [Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr153919a7283c54c77859131fcc0c1d18a';


UPDATE title
SET entry_name='[Galerie Colbert, Rue Vivienne] : [Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1a28db6e4a80542ceb911685152d87ef1';


UPDATE title
SET entry_name='Grand succès du Théatre Impérial de l''Opéra-Comique, Mignon... : Opéra en trois actes... paroles de MM. Carré et Jules Barbier..., musique de Ambroise Thomas.... Des mêmes auteurs, Hamlet..., partition, piano et chant..., Heugel et Cie, 2 bis, rue Vivienne, éditeurs pour la France et l''étranger. : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1156deb343b0647459784d4971c0a6729';


UPDATE title
SET entry_name='La Fille invisible, opéra-comique en 3 actes et 4 tableaux. Poème de Mrs de St Georges & Dupin. Partition orchestre et parties séparées [...]. Musique de A. Boieldieu. A Paris, Maison Boieldieu, Sylvain St. Etienne, sucesseur, rue Vivienne, 53, près le Boulevart [sic]. : [affiche] / Victor Coindre'
WHERE title.id_uuid = 'qr1681071905dac4124bdad9f5cba30a258';


UPDATE title
SET entry_name='La Fille invisible, opéra-comique en 3 actes et 4 tableaux. Poème de Mrs de St Georges & Dupin. Partition orchestre et parties séparées [...]. Musique de A. Boieldieu. A Paris, Maison Boieldieu, Sylvain St. Etienne, sucesseur, rue Vivienne, 53, près le Boulevart [sic]. : [affiche] / Victor Coindre'
WHERE title.id_uuid = 'qr13f40c4a2bd1240d4b49554877b5394d7';


UPDATE title
SET entry_name='[Maison à l''angle de la rue Vivienne et du Boulevard]. 5eme feuille. Coupe sur la largeur de la Cour Boulevard : [dessin] / J. Lecointe. arch.'
WHERE title.id_uuid = 'qr1d768d7983aa64ed1b47efba11a364fe9';


UPDATE title
SET entry_name='[Maison à l''angle de la rue Vivienne et du Boulevard]. 2eme [feuille]. Plan du Ier 2eme et 3eme Etage. Plan de l''Entresol : [dessin] / J. Lecointe. Arch.'
WHERE title.id_uuid = 'qr1a8e25f92f366468cb69e7a2d3cff7d00';


UPDATE title
SET entry_name='Élévation de la Maison à l''angle de la Rue Vivienne et du Boulevard. Face sur le Boulevart [sic]. 4eme feuille : [dessin] / J. Lecointe arch.'
WHERE title.id_uuid = 'qr1068aecccaaef49c3bbe5053b3aecb01a';


UPDATE title
SET entry_name='Maison à l''angle de la rue Vivienne et du Boulevard [Nouveau Frascati. Construite en 1833]. 1ere [feuille]. Plan du rez de Chaussée. Plan de l''Étage Souterrain, sur un étage de caves : [dessin] / J. Lecointe. arch.'
WHERE title.id_uuid = 'qr17e054a58f8fc4339a169ce7fb38595a0';


UPDATE title
SET entry_name='Hôtel Desmarais [i.e. Desmarets] 18 Rue Vivienne : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr17c3a32f33a7446c0b2da522cd2658c5b';


UPDATE title
SET entry_name='[Entrée de la galerie Vivienne sur la rue Neuve des Petits-Champs] : [dessin]'
WHERE title.id_uuid = 'qr10e76ba9c34a844e9adf438b6c80d53f1';


UPDATE title
SET entry_name='Frascati 49 rue Vivienne. Chef d''orchestre Litolff et Arban concerts, bals.. Mr et Mme Armanini mandolinistes... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1801d40ec5b0a46b0b5f6b17c9b0fe4ea';


UPDATE title
SET entry_name='Commerce de la rue Vivienne : [estampe] / [non identifié]'
WHERE title.id_uuid = 'qr18a35691ccd1d4199ba6d6998692eae26';


UPDATE title
SET entry_name='Cachemires et dentelles de la maison Frainais et Gramagnac, 32. Rue Feydeau et Rue Richelieu 82 : [estampe]'
WHERE title.id_uuid = 'qr1014ea3a5e8844a84a7435c63d157af87';


UPDATE title
SET entry_name='Frascati 49 rue Vivienne. Tous les soirs à 8 heures Litolf concerts, Arban concerts, bals : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr18ad348fe6b444dcf99335755620c5d0a';


UPDATE title
SET entry_name='Colle Hirou 3 Rue Vivienne, 65 Faubourg Montmartre : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1f1cee9fa2c83488cb18b3500682d4c4e';


UPDATE title
SET entry_name='Colle Hirou 3 Rue Vivienne, 65 Faubourg Montmartre : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr132156ca58c3b45f7ba039ab7212030eb';


UPDATE title
SET entry_name='Au Prince Eugène 17 rue Vivienne ouverture... des nouveaux magasins de vêtements d''hommes et d''enfants... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1838ed45c170c45f4866da5b2db8ca8d2';


UPDATE title
SET entry_name='Le Faisceau : [estampe]'
WHERE title.id_uuid = 'qr130673738c70a41229793ed0d4e2464ea';


UPDATE title
SET entry_name='A Saint-Petersbourg : [estampe]'
WHERE title.id_uuid = 'qr17405bcddfed6404bb77126d57b9da75f';


UPDATE title
SET entry_name='A Sébastopol : [estampe]'
WHERE title.id_uuid = 'qr1deeb05eead98403b8790f85ea026f34d';


UPDATE title
SET entry_name='A Sébastopol : [estampe]'
WHERE title.id_uuid = 'qr1f56371a0e7f641a9a97ec273ccc54781';


UPDATE title
SET entry_name='Excellente charge militaire faite à l''Empereur de Russie, au moment où il croyait s''asseoir bien tranquillement sur le Divan : [estampe]'
WHERE title.id_uuid = 'qr1f9b620d21e754bcd9c92776302b441cf';


UPDATE title
SET entry_name='Enthousiasme russe : [estampe]'
WHERE title.id_uuid = 'qr176c242197c844d0b8c11db7318debc88';


UPDATE title
SET entry_name='Je pourrais laisser ceci à monsieur, de huit francs.. : [estampe]'
WHERE title.id_uuid = 'qr168526618fed94f889e90113bad2df600';


UPDATE title
SET entry_name='L''armée de réserve de l''empereur de Russie : [estampe]'
WHERE title.id_uuid = 'qr13faa23a07bf542c490534143b9753c47';


UPDATE title
SET entry_name='Bal de l''Opéra : [estampe]'
WHERE title.id_uuid = 'qr1781a1ff043cf4a00b1b16145d13afca6';


UPDATE title
SET entry_name='Casino 49 rue Vivienne tous les jours à 8 h... bal de nuit : [affiche] / [L. Chouquet]'
WHERE title.id_uuid = 'qr19c335221ca38476fa3a768c062a8bab4';


UPDATE title
SET entry_name='Les Grandes usines, tableau de l''industrie contemporaine par Turgan...chez Michel Lévy frères, rue Vivienne 2 et à la Librairie Nouvelle... : [affiche] ([Variante imprimée sur papier bleu]) / Bertrand ; Coste'
WHERE title.id_uuid = 'qr1572a2f30a33f481aa2b00c3788d1c90f';


UPDATE title
SET entry_name='Les Grandes usines, tableau de l''industrie contemporaine par Turgan...chez Michel Lévy frères, rue Vivienne 2 et à la Librairie Nouvelle... : [affiche] ([Variante imprimée sur papier orange]) / Bertrand ; Coste'
WHERE title.id_uuid = 'qr1ab7114d5eefb415fb1acec73d97f70f3';


UPDATE title
SET entry_name='Cirque de l''Impératrice Champs Élysées : [estampe]'
WHERE title.id_uuid = 'qr1d7b508c72954482390e09ee1eea2dbab';


UPDATE title
SET entry_name='Salle des séances du Corps Législatif : [estampe]'
WHERE title.id_uuid = 'qr1a8e7b93761fa442498d3bff04f5f3189';


UPDATE title
SET entry_name='Th. de la Renaissance. Les douze femmes de Japhet, vaudeville-opérette de MM. Antony Mars et Maurice Desvallières. Musique de Victor Roger. Paris, Au Ménestrel, 2bis rue Vivienne : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr13a5f5a61cbcb49eb8edf85d5850e7170';


UPDATE title
SET entry_name='Hôtel-de-Ville de Paris Salle du Trône : [estampe]'
WHERE title.id_uuid = 'qr177c653ccec9f4fe4a850565a77966d3b';


UPDATE title
SET entry_name='Caricature : [estampe] : Vue intérieure d''une baraque'
WHERE title.id_uuid = 'qr1590d1e092e13429e929638a51cab8b91';


UPDATE title
SET entry_name='Pasquinade : [estampe]'
WHERE title.id_uuid = 'qr1df384d754ae94de89958c72692f26daf';


UPDATE title
SET entry_name='Plus d''aphyxie ni d''anémie [ ] Nouveau Poêle de l''ing[énieu]r Duthuit [...] chauffage au pétrole [...], maison de vente : 53 rue Vivienne, Paris : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr150e31e3224904966a3e1c3686bd340c7';


UPDATE title
SET entry_name='"C''est drôle tout d''même !... dire qu''on ne parle pas la même langue et qu''on s''entend à merveille !", déclare un soldat français, qui serre la main à un soldat anglais et à un soldat turc : [estampe]'
WHERE title.id_uuid = 'qr163022b7a494e4d1291cc95289acbdea9';


UPDATE title
SET entry_name='Le Roi Citoyen : [estampe]'
WHERE title.id_uuid = 'qr160ec8f03f36c46b3ac5f0db77ec9d050';


UPDATE title
SET entry_name='La fiancée de Lammermoor : Et l''oiseau tomba aux pieds de Lucie // dont la robe fût tachée de quelques gouttes // de sang. N° 17 : [estampe] ([1er état, avec dans la marge du haut le titre suivi des mots "Walter Scott." et "Chap. XXIX.", et dans celle du bas la signature de Delacroix, la mention de l''imprimeur, la légende explicative de la scène, en français et en anglais, un numéro éditorial, une date, et les noms et adresses des éditeurs]) / Delacroix fec.t'
WHERE title.id_uuid = 'qr19588f4d20781408d9345c89c2f15423c';


UPDATE title
SET entry_name='The Bride of Lammermoor (The |Bride of Lammermoor, Chapter XX)'
WHERE title.id_uuid = 'qr1bb408791627c4c5fbb9595300b4a96cd';


UPDATE title
SET entry_name='La fiancée de Lammermoor. Walter Scott. Chap. XXIX : Et l''oiseau tomba aux pieds de Lucie dont la robe fut tachée de quelques gouttes de sang. N°17 : [estampe] / Delacroix fec.t'
WHERE title.id_uuid = 'qr1a57b776e0c634851b6b6822ae78fa348';


UPDATE title
SET entry_name='Scène des fleurs, dansée par Mlle Taglioni (Académie Royale de Musique) / Janet Lange'
WHERE title.id_uuid = 'qr123fd911fa9ab49828f1baa7e66433e8b';


UPDATE title
SET entry_name='Liqueur de l''Abbaye du Canigou, tonique, digestive, réconfortante... : Société parisienne du Canigou, 28 rue Vivienne, Paris : [affiche] / Ogé'
WHERE title.id_uuid = 'qr12d30b11dcc3b4848942166e41a86875d';


UPDATE title
SET entry_name='Plan et coupe d''un magasin situé au coin de la rue Vivienne et de la rue Colbert destiné à agrandir la Bibliothèque Nationale'
WHERE title.id_uuid = 'qr15869802d88794b69aca789905f820f5c';


UPDATE title
SET entry_name='Description d''un monument trouvé dans une maison rue Vivienne, a Paris , par M. de Vialart-Saint-Morys.'
WHERE title.id_uuid = 'qr1afa7f36a63f0414ea62fe9c49bdeb322';


UPDATE title
SET entry_name='Adèle Jullien enceinte, âgée de 25 ans : [photographie] / Mayer frères'
WHERE title.id_uuid = 'qr17beb2e5b0b3e4b99a4efc814a0ac5dc2';


UPDATE title
SET entry_name='Autobus [de la Compagnie générale des omnibus de Paris, ligne Passy-Bourse, angle rue de la Bourse - rue Vivienne, Paris, 2e arrondissement] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1bd2fbdb50a3b41e9add29a430924c839';


UPDATE title
SET entry_name='2e quadrille brillant sur l''Enfant prodigue opéra de D.F.E. Auber par Pilodo : [estampe] / Victor Coindre [sig.]'
WHERE title.id_uuid = 'qr1556dc54ad1c640c295743be28a8f9dd4';


UPDATE title
SET entry_name='Les trois disciples de M.r Cobdem [sic] se livrant à une dernière tentative pour faire apprécier aux cosaques tous les charmes de la paix : [estampe]'
WHERE title.id_uuid = 'qr150b64f7b96724da2be3e0c4cc567badd';


UPDATE title
SET entry_name='Le Colin-maillard russe, - Jeu nouveau mais dangereux : [estampe]'
WHERE title.id_uuid = 'qr13fe7249f1b6c4bcb9d4fec6e72401745';


UPDATE title
SET entry_name='Palais de Cristal, place de la Bourse, rue Vivienne 25. Vêtements d''hommes et d''enfants : [affiche] / [non identifie]'
WHERE title.id_uuid = 'qr119573cf5e05347c4b3fd16516fab84d9';


UPDATE title
SET entry_name='Troisième Race : [estampe]'
WHERE title.id_uuid = 'qr1f1b7ba544d544dbaa218bf8f2764f047';


UPDATE title
SET entry_name='"Enflammant le courage de ses Cosaques - Il sera distribué aux plus braves des chandelles d''honneur !" annonce un chef cosaque en passant devant le front de ses troupes : [estampe]'
WHERE title.id_uuid = 'qr184c9905bc4b04d6b80c9a6ff8b5e2bea';


UPDATE title
SET entry_name='Le 30 juillet au Louvre : [estampe]'
WHERE title.id_uuid = 'qr1a5f9755730364e5996c0940610b7f704';


UPDATE title
SET entry_name='Eugénie Impératrice des Français. - Napoléon III Empereur des Français : [estampe]'
WHERE title.id_uuid = 'qr10185d98b49c44e92bee98b20fce1ad72';


UPDATE title
SET entry_name='Les trois députés du Congrès de la Paix [L''un d''eux est un Écossais]. - Ils n''ont pas voulu écouter nos sages conseils, laissons-les s''arranger entre eux, maintenant... [etc.] : [estampe]'
WHERE title.id_uuid = 'qr1283ef4132d3b4cf5915a60ae761f0d64';


UPDATE title
SET entry_name='"Qui vive ?" demande une sentinelle russe empanachée à Robert Macaire et à Bertrand, qui s''approchent de la frontière : [estampe]'
WHERE title.id_uuid = 'qr19796b425d3e04076b57322dce1c7db7f';


UPDATE title
SET entry_name='Napoléon III. - S. M. l''Impératrice : [estampe]'
WHERE title.id_uuid = 'qr1faa8888635154f8181371c8f324e2e32';


UPDATE title
SET entry_name='Napoléon III. - S. M. l''Impératrice : [estampe]'
WHERE title.id_uuid = 'qr1fdbb3c89203c4d639058dd51bb2a0561';


UPDATE title
SET entry_name='Les Cosaques pour rire Album de Quarante Caricatures : [estampe]'
WHERE title.id_uuid = 'qr1c07bc219a96a472d933d03fa7e0ec380';


UPDATE title
SET entry_name='Napoléon, Duc de Reichstadt : [estampe]'
WHERE title.id_uuid = 'qr134495ec733714a10ac745383f87ea976';


UPDATE title
SET entry_name='Politiquant sur un poële. - Voyez-vous, m''ossieu Fibochon, la situation est grave, nous sommes sur un volcan !... [etc.] : [estampe]'
WHERE title.id_uuid = 'qr17a35facd44734f2995fefc23e53f8de0';


UPDATE title
SET entry_name='[Gabrielle Krauss (Desdemona)] : [Otello (Théâtre impérial italien)] / [Alfred Lemoine] ; [d''après la phot. de Trinquart]'
WHERE title.id_uuid = 'qr14191b1e3d59e430e9be7580e3bc1c70b';


UPDATE title
SET entry_name='Gabrielle Krauss (Desdemona) : Otello (Théâtre impérial italien) / Alfred Lemoine ; d''après la phot. de Trinquart'
WHERE title.id_uuid = 'qr17be98e2506474b20a97f647735dc40b0';


UPDATE title
SET entry_name='Caricature : [estampe] : Un Désespoir. Faut-il être bête et malheureux !'
WHERE title.id_uuid = 'qr1edf207807d6a46058b18de131e955e2e';


UPDATE title
SET entry_name='Th. de la Renaissance. Les douze femmes de Japhet, musique de Victor Roger. Vaudeville-Opérette de M.M. Antony Mars et Maurice Desvallières : Paris - Au Ménestrel, 2 bis rue Vivienne : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1391b5904833844e2b8a8943702ed7ebc';


UPDATE title
SET entry_name='Louis-Philippe I.er Roi des Français : [estampe]'
WHERE title.id_uuid = 'qr18a448b86509c439eb7b5fbbf5bbb3477';


UPDATE title
SET entry_name='Vue générale du Sacré Coeur, prise de la rue Vivienne : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr1091f49a92b1144e7abed2c91d5463e02';


UPDATE title
SET entry_name='Vue générale du Sacré Coeur, prise de la rue Vivienne : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr162778660b9e44ddeb3c1df739c2e1f43';


UPDATE title
SET entry_name='Tombeau de l''Empereur aux Invalides : [estampe]'
WHERE title.id_uuid = 'qr17ccca5652e0149b49eff87058d37d29c';


UPDATE title
SET entry_name='Le Grand Salon Carré et la Galerie du Louvre un jour d''Étude : [estampe]'
WHERE title.id_uuid = 'qr12ccc7f7098ae43368683b43b776ccc6a';


UPDATE title
SET entry_name='Aux Mânes des Citoyens morts pour le salut de la France, Sous la Colonnade du Louvre, le 29 Juillet 1830 !... Par leurs frères d''armes : [estampe]'
WHERE title.id_uuid = 'qr16b708f1d317949fc99beddcace426e52';


UPDATE title
SET entry_name='Marie-Amélie, Reine des Français : [estampe]'
WHERE title.id_uuid = 'qr1993b3aa4aaab48c18fb76481dfe6610d';


UPDATE title
SET entry_name='Exposition des Arts Incohérents au profit des pauvres de Paris... du 15 octobre au 15 novembre 1883 : 55-57-59 Galerie Vivienne... : [affiche] / H. Gray'
WHERE title.id_uuid = 'qr14ac81c40e3984ef88883e6fa4378260e';


UPDATE title
SET entry_name='Macédoine Patriotique. N° 1 : [estampe]'
WHERE title.id_uuid = 'qr1f976c87d415e424ab4c15c0db570b1d0';


UPDATE title
SET entry_name='"A la sortie du Théâtre de la Gaité, après une représentation des Cosaques A Bas le Cosaque ! à bas !...", crie la foule, en menaçant un officier, qui se réfugie sur la plate-forme arrière d''une voiture, en criant : "Mais je ne suis pas un Général russe, je suis un Chasseur français !!!..." : [estampe]'
WHERE title.id_uuid = 'qr1123c85721beb4f3e8ec8a97f97c8150c';


UPDATE title
SET entry_name='Rue de Rohan : [estampe]'
WHERE title.id_uuid = 'qr1be1c6493386e483b816a7c4410c00e27';


UPDATE title
SET entry_name='Désolation des Cosaques de la Bourse le jour oû [sic] l''on annonce que les Turcs ont remporté une victoire : [estampe]'
WHERE title.id_uuid = 'qr1019c8c8e118e4f349378d32da50adad6';


UPDATE title
SET entry_name='Réfection de la rue Vivienne : vue générale : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr13b3638c8540f414e94b38c602c236dfc';


UPDATE title
SET entry_name='M''entends-tu ? : [estampe]'
WHERE title.id_uuid = 'qr1efa3249663854879b478f9e042bcd09e';


UPDATE title
SET entry_name='En Italie Album par Cham : [estampe]'
WHERE title.id_uuid = 'qr14db7503b14fb4a23949574a683fdcca6';


UPDATE title
SET entry_name='Entrée dans un omnibus, rue Notre-Dame de Lorette : [estampe]'
WHERE title.id_uuid = 'qr1f5f5c6a041b749fa9baf4e497876c1a7';


UPDATE title
SET entry_name='Les Zouaves Album par Cham : [estampe]'
WHERE title.id_uuid = 'qr1579ec09ef6614c2b932e4b47c7a31f64';


UPDATE title
SET entry_name='Les oies du Frère Philippe. tirant les Rois : [estampe]'
WHERE title.id_uuid = 'qr1c02db295fd814782b1acc2d5d09106ba';


UPDATE title
SET entry_name='Costumes. Oh ! C''est bien ça : [estampe]'
WHERE title.id_uuid = 'qr1c76d1fc50d334b5bbfd3de0bba346c62';


UPDATE title
SET entry_name='Réfection de la rue Vivienne : lancement du gravier goudronné : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr176e2c7de23c0409398a7442681ed0825';


UPDATE title
SET entry_name='Les Boulevards de Paris. - 2.e partie : Du Cadran-Bleu [restaurant au coin de la r. Charlot] à la rue Neuve Vivienne : [estampe]'
WHERE title.id_uuid = 'qr11e344a583e854138a3dd3dffd72fd36f';


UPDATE title
SET entry_name='Album artistique de la Reine Hortense : [estampe]'
WHERE title.id_uuid = 'qr1885e9ebc090f418cad9f193a5bd37f12';


UPDATE title
SET entry_name='Théâtre de la Galerie Vivienne, 6 rue Vivienne. Le Chat botté, féerie en 3 actes et 7 tableaux de Mr Alphonse de Jestières, musique de Mr A. Bérou... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1c694ea900b8d46abaee13e5154f3ab79';


UPDATE title
SET entry_name='Porte Saint-Martin : [estampe]'
WHERE title.id_uuid = 'qr1dfcda161cab543909e65d2bdca4ce4f0';


UPDATE title
SET entry_name='Tiens..., tu te mets en autrichien, toi, pour faire la cuisine !... [etc.] : [estampe]'
WHERE title.id_uuid = 'qr133c50b01a81c456f91d79dcc15d07c6e';


UPDATE title
SET entry_name='Départ pour Rambouillet : [estampe]'
WHERE title.id_uuid = 'qr16a6653e21b8d43078c28ee196d293de4';


UPDATE title
SET entry_name='Le Jardin d''Hiver Polka Pour le Piano [ ] : [estampe]'
WHERE title.id_uuid = 'qr154dc74a59b8649439ea02b0d3cd44862';


UPDATE title
SET entry_name='Hamlet: opéra en 5 actes, paroles de M.M. Michel Carré et Jules Barbier, musique de Ambroise Thomas. Paris, Au Ménestrel, 2 bis rue Vivienne, Heugel et Cie Editeurs pour la France et l''étranger : [affiche] / Emile Vernier lith ; A de Neuville pinx'
WHERE title.id_uuid = 'qr1bcc9c05e6ed44f409588b2e62124e933';


UPDATE title
SET entry_name='Hamlet: opéra en 5 actes, paroles de M.M. Michel Carré et Jules Barbier, musique de Ambroise Thomas. Paris, Au Ménestrel, 2 bis rue Vivienne, Heugel et Cie Editeurs pour la France et l''étranger : [affiche] / Emile Vernier lith ; A de Neuville pinx'
WHERE title.id_uuid = 'qr15438dc5f7eab43ef9da3f3fd53426c12';


UPDATE title
SET entry_name='Mais cette caisse n''est pas à vous ! : [estampe]'
WHERE title.id_uuid = 'qr198b81b3b66bf4a548392fc60a9572792';


UPDATE title
SET entry_name='Mais cette caisse n''est pas à vous ! Elle doit être à nous : [estampe]'
WHERE title.id_uuid = 'qr19d0519eff4a74e1d8dc862e2a7126b54';


UPDATE title
SET entry_name='La Française. Chanson patriotique de Gustave Nadaud : [estampe]'
WHERE title.id_uuid = 'qr1c7e96b49597740f693beb71bc7cdc0f5';


UPDATE title
SET entry_name='Une juste punition : [estampe]'
WHERE title.id_uuid = 'qr176975c774a6b4699abef43ef4c9cfbae';


UPDATE title
SET entry_name='Ivanhoé [Fronte-Boeuf et le Juif] : Chien maudit, issu d''une race maudite ! // vois tu ces balances-. il faut que tu m''y // pèse mille livres d''argent : [estampe] / Delacroix fec.t'
WHERE title.id_uuid = 'qr14057b2abe8fc422aadfe1f89315aff0e';


UPDATE title
SET entry_name='Most accursed dog of an accursed race seest thou // this scales ? in these very scales shalt thou weigh // me out a thousand silver pounds'
WHERE title.id_uuid = 'qr1f7ba641f78a6479683c7fb6165ffd111';


UPDATE title
SET entry_name='Vous permettez..., pour ce à quoi votre laurier vous sert... et puis si vous aviez un peu de thym, par la même occasion... ça serait fameux !.. : [estampe]'
WHERE title.id_uuid = 'qr1f1b35a37b3f445a09f8478dd348d49a5';


UPDATE title
SET entry_name='Cré-nom!... mon capitaine..., quelle cavalerie que la cavalerie autrichienne!... impossible de la rejoindre ! [etc.] : [estampe]'
WHERE title.id_uuid = 'qr1e896163a6b7445b4bd78d53343dd5333';


UPDATE title
SET entry_name='Cré-nom !... mon capitaine..., quelle cavalerie que la cavalerie autrichienne !!... impossible de la rejoindre ! [etc.] : [estampe]'
WHERE title.id_uuid = 'qr1311c689149f14600a804aa5bc425106f';


UPDATE title
SET entry_name='Portrait de Napoléon I, en buste, de profil dirigé à gauche dans un rond : [estampe]'
WHERE title.id_uuid = 'qr1fc8977c0f7234ab682e95bfb4d6c7cbe';


UPDATE title
SET entry_name='Portrait de l''Impératrice Marie-Louise, en buste, de tr. q. à dr. : [estampe]'
WHERE title.id_uuid = 'qr1fe2c91e8dc024592aed323dffd683f29';


UPDATE title
SET entry_name='Daniel Steibelt, né à Berlin en 1765, mort à Petersbourg en 1823 / Alfred Lemoine lith.'
WHERE title.id_uuid = 'qr197f7a6b9bc6c4b669a0ee5fc517a1ec8';


UPDATE title
SET entry_name='Ces bons Autrichiens : [estampe]'
WHERE title.id_uuid = 'qr139517b5be5164d75a2a3750aa0370213';


UPDATE title
SET entry_name='Une juste punition : [estampe]'
WHERE title.id_uuid = 'qr143f51f165c1d401f9eab869b8792fba9';


UPDATE title
SET entry_name='Chant des Niçois Hymne à la France par Léopold Amat : [estampe]'
WHERE title.id_uuid = 'qr11764015d3fe8447bbfabace913cb8b07';


UPDATE title
SET entry_name='Les Plaisirs de Baden : Album de trente lithographies : [estampe]'
WHERE title.id_uuid = 'qr13c2782c3649d42c59eaa99e173e1e27c';


UPDATE title
SET entry_name='[La Soeur de Duguesclin] : [estampe] ([1er état, sans aucune lettre et avant de nombreux travaux, notamment sur les vêtements de la femme du second plan ou encore sur la torche dont la flamme est importante]) / [Eugène Delacroix]'
WHERE title.id_uuid = 'qr15aa63691c72f429f882930910b24f238';


UPDATE title
SET entry_name='Duguesclin''s Sister'
WHERE title.id_uuid = 'qr1df2c743b1c3c436a94b2b1d0f995036b';


UPDATE title
SET entry_name='[Duguesclin] : [estampe] ([1er état, sans aucune lettre]) / [Eugène Delacroix]'
WHERE title.id_uuid = 'qr1d977c5bfcc524e3c91a90b156baae84d';


UPDATE title
SET entry_name='Duguesclin à cheval'
WHERE title.id_uuid = 'qr1d274d4b4ff184843875c7f32ff19dadc';


UPDATE title
SET entry_name='[Duguesclin] : Sous les pieds des chevaux le pont levis résonne [sic] ; // C''est lui, c''est Monseigneur !...et dans la vaste cour // Chacun veut des premiers saluer son retour : [estampe] / Delacroix del.t'
WHERE title.id_uuid = 'qr1b0d21ca38d91438390901f4b46cb52b2';


UPDATE title
SET entry_name='Duguesclin à cheval'
WHERE title.id_uuid = 'qr18163824495b545a8a970eda2a65a0099';


UPDATE title
SET entry_name='Chroniques de France // Dessinées et Lithographiées // M.M. Boulanger, Delacroix, Devéria // et C. Roqueplan [Page de couverture] : [estampe] / [Eugène Delacroix]'
WHERE title.id_uuid = 'qr1ff77c40e5b43473b84349aab3dc88e2f';


UPDATE title
SET entry_name='[Le truc d''Arthur, comédie d''Alfred Duru et henri Chivot : documents iconographiques]'
WHERE title.id_uuid = 'qr1110dc4379b2545ac963126b8933fc6bf';


UPDATE title
SET entry_name='[Le train de plaisir, vaudeville d''Alfred Hennequin, A. de Saint-Albin et Arnold Mortier : documents iconographiques]'
WHERE title.id_uuid = 'qr15996be6e559644a39a56c0fe12821d6a';


UPDATE title
SET entry_name='[La dame de Monsoreau, drame d''Alexandre Dumas et Auguste Maquet : estampe]'
WHERE title.id_uuid = 'qr187a34fa32ec949448f2e2dfaa9252970';


UPDATE title
SET entry_name='Nouvelle invention, cuillère dégraisseuse automatique pour tables & cuisines ... en vente chez tous les bijoutiers - orfévres - quincaillers ... Maison N. Caillar Bayard, fabricants, 26 rue Vivienne, Paris : [affiche] / [non signée]'
WHERE title.id_uuid = 'qr127524547f4114191ba300207c985d427';


UPDATE title
SET entry_name='Librairie internationale - 15 boulevard Montmartre - Au coin de la rue Vivienne . Paris, guide rédigé par les principaux littérateurs et savants français illustré par les premiers artistes... 2 magnifiques volumes... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1a6cde418dbf848eea8df2a9242fcdf14';


UPDATE title
SET entry_name='1r quadrille brillant sur l''Enfant prodigue opéra de D.F.E. Auber par Strauss : [estampe] / Victor Coindre [sig.]'
WHERE title.id_uuid = 'qr17f1d8a0ae3944b698de90864bdc448e2';


UPDATE title
SET entry_name='[Bibliothèque Royale ou Nationale, projets d''aménagement ou de reconstruction sur le site de la rue de Richelieu [entre 1828 et 1853]]. 2, [Plan du quartier de la rue Notre-Dame-des-Victoires] : [dessin, plan] / [Louis Visconti ?]'
WHERE title.id_uuid = 'qr12bbe60d1f8d64d6587752f81b50971b9';


UPDATE title
SET entry_name='19/3/26, place de la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1bee90d1cc2654c118ec05b5ef41e0ca0';


UPDATE title
SET entry_name='19/3/26, place de la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1748298b35a104638aba3f623d0f1f65b';


UPDATE title
SET entry_name='19/3/26, place de la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr175df0ca64d1e43a39c4a39a8a9a94e14';


UPDATE title
SET entry_name='[Place de la Bourse] : [dessin] / [Victor-Jean Nicolle]'
WHERE title.id_uuid = 'qr15f2403a6dfde410f91382507149b522a';


UPDATE title
SET entry_name='Place De La Bourse : [estampe]'
WHERE title.id_uuid = 'qr11aacc1ff4dd146818578f791e9360ab4';


UPDATE title
SET entry_name='Place De La Bourse : [estampe]'
WHERE title.id_uuid = 'qr145b1b80024ad461bba156160400e89b0';


UPDATE title
SET entry_name='Autobus express, place de la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1f5fa6d770731465b980d907f7c7aaf03';


UPDATE title
SET entry_name='13/7/25, bal populaire [place de la Bourse, à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr121865df52c6143a8a7ed275cfd3ed2f5';


UPDATE title
SET entry_name='12-7-13, emménagement Cochon [une famille de sans domicile fixe encadrée par des agents de police, place de la Bourse] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1931dff88829e4b75a4a47c934843d760';


UPDATE title
SET entry_name='Grève des autobus : place de la Bourse, les chauffeurs discutent : [photographie de presse] / Peyr.'
WHERE title.id_uuid = 'qr1bb91d697dd904b84b844218b77d721c3';


UPDATE title
SET entry_name='Jacquelin et son tricycle-balayeur, Place de la Bourse : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr19058288d55224c52a9ab1ba610eccadd';


UPDATE title
SET entry_name='Grève des autobus : Place de la Bourse, le public attendant la reprise : [photographie de presse] / Peyr.'
WHERE title.id_uuid = 'qr1ee889e8f815f4bda9b1fdd932df07823';


UPDATE title
SET entry_name='Fête Nationale : le bal, place de la Bourse : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1479264f87a084d9da00a39870065b85d';


UPDATE title
SET entry_name='10/11/27, minute de silence à la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr195e8101d84154273ac32edc2260c7ac5';


UPDATE title
SET entry_name='Paris, la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1db90f1274f504a0caefc8064e49801a1';


UPDATE title
SET entry_name='Concours de musique, une audition place de la Bourse [foule entourant les musiciens] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1b73651a252ff493fbe54e68c7b8c60c3';


UPDATE title
SET entry_name='Promenades dans Paris. 1889 / Photographies réunies par le baron De Vinck'
WHERE title.id_uuid = 'qr1dbcdeeaf431a43b990b5870654479ec5';


UPDATE title
SET entry_name='La Bourse'
WHERE title.id_uuid = 'qr10d7bcff087994a91bfd77b742cfd4224';


UPDATE title
SET entry_name='Promenades dans Paris. 1889 / Photographies réunies par le baron De Vinck'
WHERE title.id_uuid = 'qr14123ac0bcf904a92a368e275740e3b8b';


UPDATE title
SET entry_name='La Bourse'
WHERE title.id_uuid = 'qr170e899a4975d426abc44a015f587387d';


UPDATE title
SET entry_name='Promenades dans Paris. 1889 / Photographies réunies par le baron De Vinck'
WHERE title.id_uuid = 'qr1c02d8fe35fd04d87985f6b457465f898';


UPDATE title
SET entry_name='La Bourse'
WHERE title.id_uuid = 'qr16ca166a5fdce4668849535ef965b54b2';


UPDATE title
SET entry_name='[Affiches pour la souscription à l''emprunt national devant un bureau provisoire de la banque de France, Place de la Bourse à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1d6b543234f4a48519e56507e3c2a712d';


UPDATE title
SET entry_name='[Affiches pour la souscription à l''emprunt national devant un bureau provisoire de la banque de France, Place de la Bourse à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1e3bd41fe197844cfa58caddaaa02fc20';


UPDATE title
SET entry_name='[Affiches pour la souscription à l''emprunt national devant un bureau provisoire de la banque de France occupant le restaurant Champeaux, Place de la Bourse à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1bf0e188ed8304862aca117d9fe70bb37';


UPDATE title
SET entry_name='10/11/26, minute de silence à la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1c8d65953d8344618a08c78c173ccb56e';


UPDATE title
SET entry_name='Napoléon à l''ouverture du cercueil : [estampe]'
WHERE title.id_uuid = 'qr115045195b2224f1f84c8634db3f7dbfd';


UPDATE title
SET entry_name='Cercueil de l''Empereur Napoléon, à bord de la Belle Poule Commandant, Prince de Joinville : [estampe]'
WHERE title.id_uuid = 'qr18117ed7e891640e69dae02f18d9ac355';


UPDATE title
SET entry_name='11/11/28, anniversaire de l''armistice, la minute de silence à la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1f6d3d6b5c4c74e669748daa60e32dc02';


UPDATE title
SET entry_name='10/11/23, 2 minutes de silence à la Bourse [anniversaire de l''armistice] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr14325819aab3640c1a4b463e4517e3203';


UPDATE title
SET entry_name='10/11/25, anniversaire de l''armistice, la minute de silence à la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr15c7f204db203483b971aa9c309b61d5f';


UPDATE title
SET entry_name='La Place de la Bourse, le 15 août, et la rue de la Bourse : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr109e2d9b135714900a708f96ce2d8a26e';


UPDATE title
SET entry_name='La Place de la Bourse, le 15 août, et la rue de la Bourse : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr1f7370cb51ff149f69ee901a4ce0ebb04';


UPDATE title
SET entry_name='La Place de la Bourse, le 15 août, et la rue de la Bourse : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr1ae88cfc94fd64810b08ef7086b6b8c06';


UPDATE title
SET entry_name='Quartier de la Bourse : [estampe] / Henry Monnier'
WHERE title.id_uuid = 'qr181d301eb707544d696dd4ce8e3f0934b';


UPDATE title
SET entry_name='[Rideau de jardin du théâtre des Nouveautés / Pierre-Luc Charles Cicéri]'
WHERE title.id_uuid = 'qr1ff24b71ea8ca4273b48359a162ff7202';


UPDATE title
SET entry_name='[Esquisse de décor pour le théâtre des Nouveautés : ville renaissance]'
WHERE title.id_uuid = 'qr1e66cf3cd618c48ac9d4d0daf05ea6ee3';


UPDATE title
SET entry_name='Paris : vues et monuments / dessinés et lithographiés en couleur par Jules Arnout'
WHERE title.id_uuid = 'qr12d0360c7bf36478c9c22babf8046302d';


UPDATE title
SET entry_name='Bourse'
WHERE title.id_uuid = 'qr1dfa4c63739dd4c29ab48e3621c55b162';


UPDATE title
SET entry_name='Succès éclatant à l''Opéra-Comique. Gibby. La Cornemuse de Clapisson. En vente au bureau central de musique, 29 place de la Bourse : [affiche avec lettre complète] / [Non identifié]'
WHERE title.id_uuid = 'qr1a7f6cbb027794067af62684caa9b75c1';


UPDATE title
SET entry_name='Cavaignac : [estampe]'
WHERE title.id_uuid = 'qr1c39e1b847e2c4847aa2545cf61deaf8c';


UPDATE title
SET entry_name='Canonnade de la Place Baudoyer : [estampe]'
WHERE title.id_uuid = 'qr11ac238929e84402191dfc814144acb40';


UPDATE title
SET entry_name='27 Juillet 1830. Incendie Du Corps De garde Des gendarmes place de la Bourse, 10 heures du soir : [estampe]'
WHERE title.id_uuid = 'qr18984888963384da8a0a1904471113742';


UPDATE title
SET entry_name='[La Corde de pendu] Costume d''Hoster, rôle de l''Ecorchél, en Coureuv, dans la corde de pendu. Cirque national : [estampe] / Levilly [sig.]'
WHERE title.id_uuid = 'qr1a8cc49cbba2f4226a6f74161a6fcfc0b';


UPDATE title
SET entry_name='Maison Travault, Boulevard du Temple N° 50, Portrait de Gérard [Jacques] (Fieschi) de Lodève (Hérault) agé de 42 ans, Mécanique infernale : [estampe]'
WHERE title.id_uuid = 'qr12737a263f9df4aedabd4794c9f7fe92d';


UPDATE title
SET entry_name='"Maison d''où l''attentat a été commis Boulevard du Temple n° 50." "Portrait de Gérard (Jacques) de Lodève (Herault) agé de 42 ans." et "Mécanique Infernale" : [estampe]'
WHERE title.id_uuid = 'qr16c356aa072d045cdb9defd13c55fc229';


UPDATE title
SET entry_name='Napoléon III Empereur des Français. souvenir du 30 Janvier 1853 : [estampe]'
WHERE title.id_uuid = 'qr17ea3d7d519fb45f1bd3de06a0ddb2edb';


UPDATE title
SET entry_name='Eugénie Impératrice des Français. 30 Janvier 1853 : [estampe]'
WHERE title.id_uuid = 'qr18c39b2bf43ac4f1a8d9d08d354109cf5';


UPDATE title
SET entry_name='Paris et ses environs'
WHERE title.id_uuid = 'qr18a4f4948b02f4a21938b51d5d567e64a';


UPDATE title
SET entry_name='Paris : place de la Bourse'
WHERE title.id_uuid = 'qr129fb7c6c24714dfc9ec550cff641e03f';


UPDATE title
SET entry_name='Exposition universelle de 1855. Plan-guide du Palais des Beaux-Arts seul autorisé et vendu à l''intérieur : [estampe]'
WHERE title.id_uuid = 'qr10d70412480a2422b9b4acc90dd819b7e';


UPDATE title
SET entry_name='La paix du monde Congrès de Paris Mars 1856 : [estampe]'
WHERE title.id_uuid = 'qr11db275911e25426586adaa8f0405df25';


UPDATE title
SET entry_name='Façade du Restaurant de M.r Champeaux, rue des Filles S.t Thomas, Place de la Bourse. // Déposé... : [estampe] / [J. M. Mixelle]'
WHERE title.id_uuid = 'qr1b3f3e74222aa4e7881c48cd34e7cec30';


UPDATE title
SET entry_name='Notices politiques et biographiques Des 28 Candidats proposés par le Centre Démocratique-Socialiste. Ledru-Rollin (Né à Paris en 1808) : [estampe]'
WHERE title.id_uuid = 'qr1fd11b7ff6402446f8c5887bfd5be8ede';


UPDATE title
SET entry_name='Faust // Tragédie // de M. de Goethe [verso de la page de couverture] : [estampe] ([1er état]) / [Achille Devéria] ; [Eugène Delacroix]'
WHERE title.id_uuid = 'qr14000421b0e924dda9d0413cdc4ce62ca';


UPDATE title
SET entry_name='[Bibliothèque Royale, propriétés particulières du côté de la rue Vivienne, 1833]. 3, Figuré du mur mitoyen Entre la Propriété de Mr de Cierlans et la Bibliothèque du Roi : Figuré du mur mitoyen entre la Propriété de Mr Charuel et la Bib.que du Roi : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr1c83851c8d42545be959fa83ee10be462';


UPDATE title
SET entry_name='[Bibliothèque Royale, propriétés particulières du côté de la rue Vivienne, 1833]. 2, Figuré des murs mitoyens et de Cloture Entre les propriétés de MM Carette, de Saint-Romain, de Combret, Pasquier, et la Bibliothèque du Roi : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr194f75f543ed845fb88afeca13482c7d6';


UPDATE title
SET entry_name='Grand Bazar Vivienne. 2 rue Vivienne, 6 rue des Petits-Champs. Bon marché exceptionnel : [affiche]'
WHERE title.id_uuid = 'qr14f021746de684227be8d3d403f9bb28e';


UPDATE title
SET entry_name='Bibliothèque Nationale sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr16e67a5e315e5430a8b258d9c041de66e';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1a5681b162ea34538b54d7c7aac8e2512';


UPDATE title
SET entry_name='Bibliothèque Nationale sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1ad100b2f8d3f4eb38cd1b8ccf7011b44';


UPDATE title
SET entry_name='Bibliothèque Nationale, sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr11b15f192fdc9489483852acc958be1f5';


UPDATE title
SET entry_name='[Portrait d''enfant, à mi-jambes, assis de trois-quarts à gauche, accoudé à une table] : [photographie] / Pierre Eugène Fixon'
WHERE title.id_uuid = 'qr120d11a622595442ebe4970c54128960d';


UPDATE title
SET entry_name='[Portrait d''homme, à mi-corps, assis, de face, tête de trois-quarts à droite, tenant un livre] : [photographie] / Pierre Eugène Fixon'
WHERE title.id_uuid = 'qr18cd1fd50ac0e43ccbc323f7e7eecf67b';


UPDATE title
SET entry_name='Théâtre-Galerie Vivienne, 6 rue Vivienne... Matinées enfantines... : [affiche] / [non signé]'
WHERE title.id_uuid = 'qr19d0fdcd69c6142a0ac9ad4b2591e68c4';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne]. 1, [Projet pour le dessin des parterres (plan d''ensemble du jardin) et la distribution du bâtiment du Télégraphe (plans du rez-de-chaussée et de l''étage)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1fa06e420d91144ef965520c73b59cba7';


UPDATE title
SET entry_name='Bibliothèque Nationale, sur la r. Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1eeee30e13ae64e8fac421f79ead8dd66';


UPDATE title
SET entry_name='[Portrait de garçon en uniforme de collégien à mi-jambes, debout portant un brassard de communiant] : [photographie] / Pierre Eugène Fixon'
WHERE title.id_uuid = 'qr1dcea3e246f294141a05c9462063be627';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. 5, Bibliothèque du Roi. Travaux de 1831. Attachement N° 3 des figurés : 1.ere & 2.e assise des murs de face sur la rue Vivienne & sur la Cour : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr1b79d261519c84de2a031454d97afb54e';


UPDATE title
SET entry_name='[Bibliothèque impériale, plans d''ensemble]. 27, [Plan général du rez-de-chaussée, avec détail des propriétés particulières de la rue Vivienne] : [estampe, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1a401720984a443659421f7b79f0d4f69';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne]. 3, BIBLIOTHEQUE IMPERIALE // Grille sur la rue Vivienne // sur une échelle de 0,05 p.r m.tre : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1d55a322720cb4ef28d0398e0efb6c47c';


UPDATE title
SET entry_name='[Portrait de femme, à mi-corps, de face, accoudée à une table] : [photographie] / Victor Plumier'
WHERE title.id_uuid = 'qr1fee6666da2d94c238d931dc6b44cfb88';


UPDATE title
SET entry_name='[Portrait d''homme, à mi-genoux, debout, tête de trois-quarts à droite, l''avant-bras droit reposant sur une meuble] : [photographie] / Victor Plumier'
WHERE title.id_uuid = 'qr164d288dcf8b04ef192d2ff58d424be55';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [11], Bibliothèque du Roi. Travaux de 1831. Attachement N° 7 Bis des Figurés : assises du côté droit de l''avant-corps principal sur la rue Vivienne : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr13ea34a8db9934259b7bcb35ef4e2c590';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [12], Bibliothèque du Roi. Travaux de 1831. Attachement N° 8 des Figurés : 11.e & 12.e Assises des murs de face sur la rue Vivienne et sur la Cour : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr183d492c0ce4c484db3020943400a4ecf';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. 14, Bibliothèque du Roi. Travaux de 1831. Attachement N° 9 des Figurés : 13.me & 14.me Assises des murs de face sur la Cour et sur la rue Vivienne : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr198de0128810a47b5a09f05c3c7817d9e';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [9], Bibliothèque du Roi. Maçonnerie. Travaux de 1831. Attachement N° 7 des Figurés : 9eme & 10eme assises des murs de face sur la rue Vivienne et sur la cour : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr193ad4b48640e41fa88b40a45dd2b878c';


UPDATE title
SET entry_name='[Bibliothèque Royale, relevé des plans vers 1847 (deuxième série)]. 6, BIBLIOTHEQUE ROYALE. ETAT ACTUEL : élévation sur la rue Vivienne : élévation sur la rue de Richelieu : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr1a18bb99e689547dc80838c4913f581a5';


UPDATE title
SET entry_name='Théatre de la Galerie Vivienne, 6, rue, Vivienne. La Belle et la Bête, féérie en 3 actes... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1b771e161ed3840839f429db1c1f0fad9';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 8, BIBLIOTHEQUE IMPERIALE // Travaux en Raccord : Attachement de Maçonnerie N° 7 // Mons.r Victor Lemaire Entrepreneur : Exercice 1857 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1eef771b5ca6f4208b45e59c50a6ab2fb';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. 3, Bibliothèque du Roi. Travaux de 1831. Attachement N° 2 Bis des Figurés : massif en moellon neuf (hourdi en mortier de chaux et sable) devant supporter le plancher bas du rez-de-chaussée de la partie de galerie en construction sur la rue Vivienne : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr1c6832d80bfe84774b22ea1fec328e463';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. 13, Bibliothèque du Roi. Travaux de 1831. Attachement N° 8 (bis) des Figurés : détails des Claveaux et de l''Arrière-Voussure de l''entrée principale sur la rue Vivienne : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr1782b87ab1ab04227885ed54310ba13cc';


UPDATE title
SET entry_name='Caricature : [estampe] : Un Triomphe. - Compensation'
WHERE title.id_uuid = 'qr1c4979277c79f4fbeac8b1320e1b56c2f';


UPDATE title
SET entry_name='[Bibliothèque Royale, relevé des plans vers 1847 (troisième série)]. 9, Elévation existante sur la rue Vivienne ; Elévation existante sur la rue de Richelieu : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr1c79c2c93225f400dbc5a3f4de90cfbad';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 19, Bibliothèque Impériale // bâtiment de l''administration // Façade sur la rue Vivienne : [dessin, plan] / H. Labrouste'
WHERE title.id_uuid = 'qr1033cd25001c84add8fc9db0540934930';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [10], 9.e assise : 10.e assise : 11.e assise : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr12f03ec84e59f4f4099fb29c5fa763ba0';


UPDATE title
SET entry_name='[Bibliothèque impériale, plans d''ensemble]. 35, BIBLIOTHÈQUE IMPÉRIALE. Service d''incendie : [dessin, plan] / [Agence des travaux de la Bibliothèque impériale]'
WHERE title.id_uuid = 'qr19cddb8856ebf438185d0e0a96200e830';


UPDATE title
SET entry_name='[Portrait de petite fille, en pied, assise, dans un fauteuil, de trois-quarts à droite] : [photographie] / Mayer frères'
WHERE title.id_uuid = 'qr149e72f1d6fdc436db358d23f2ec9ae6e';


UPDATE title
SET entry_name='[Portrait d''homme, à mi-genoux, assis, de trois-quarts à gauche, près d''un rideau] : [photographie] / Pierre Eugène Fixon'
WHERE title.id_uuid = 'qr1249501a507ec47ee861dcc13bf8608e0';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne]. 10, Poinçon milieu : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1861219646d644b4195f42334e4cac7e0';


UPDATE title
SET entry_name='[Le portefaix, opéra-comique de Gomis et Scribe : costume de Chollet (Gasparillo) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr156df8e26fd664172870156de6d5c1ced';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne (attachement de maçonnerie, 1856)]. 5, BIBLIOTHEQUE IMPERIALE // Pavillon de droite // Détails des Façades : Attachement // de Maçonnerie N° 7 // M.r Lemaire, Entrepreneur : Exercice 1856 // Monsieur Labrouste, Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr16c17580049f44a83ba9dc1ab5852d0e1';


UPDATE title
SET entry_name='[Bibliothèque Nationale, projet de reconstruction, 1850]. 12, FAÇADE SUR LA RUE VIVIENNE : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr1db8a247c9c4c42cbabe72e3c06a5640f';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [2], Bibliotheque du Roi. Attachement de maçonnerie. Travaux de 1831. Attachemt N°2 : dérasement fait sur la dernière assise de libage en roche des deux murs de la Galerie Vivienne : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr147fbd517d59045ef81a9922be1ae9760';


UPDATE title
SET entry_name='Melle Robert dans "La filleule des fées" / [lithogr. signée A. L.]'
WHERE title.id_uuid = 'qr13c0a4926061d4ca0bf3b1d25fcc1b4c4';


UPDATE title
SET entry_name='[Bibliothèque impériale, plans d''ensemble]. 34, Projet d''achèvement // par H. Labrouste // acquisition des maisons sur la rue // Colbert et sur la rue Vivienne : [dessin, plan]'
WHERE title.id_uuid = 'qr1b7f458843dd6423abeb2c8b84350c373';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 5, BIBLIOTHEQUE IMPERIALE // Détail de la démolition à l''angle des rues Vivienne et des Petits Champs : Attachement // de Maçonnerie N° 6 // Mons.r Victor Lemaire Entrepreneur : Exercice 1857 // Mons.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr17cf1000ba3b14a6b8d0eb254ddccd426';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne (attachement de maçonnerie, 1856)]. 6, BIBLIOTHEQUE IMPERIALE // Détail du Bahut de la grille sur la rue Vivienne : Attachement de Maçonnerie N° 8 // Mons. V.r Lemaire Entrepreneur : Exercice 1856 // Mons. Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr12816586ab59440b484bc7ec3e60e6844';


UPDATE title
SET entry_name='Pasquinade : [estampe]'
WHERE title.id_uuid = 'qr151d5f854c38444228d8f2cf386aa7fac';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 18, BIBLIOTHEQUE IMPERIALE // Elévation de la façade sur la Rue Vivienne avant sa restauration (1.er Etage) : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1883027811e09451fa3023891fd38f72e';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. [8], Bibliotheque du Roi. Maçonnerie. Attachement N° 6. Travaux de 1831 : huitième Assise : septième Assise : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr1fc2fab5eceea4898b50c9a445377ec25';


UPDATE title
SET entry_name='[Bibliothèque Royale, construction d''un bâtiment rue Vivienne, 1831]. 7, Bibliotheque du Roi. Maçonnerie. Attachement N° 5. Travaux de 1831 : sixième Assise : cinquième Assise : [dessin, plan] / [Agence des travaux de la Bibliothèque Royale]'
WHERE title.id_uuid = 'qr1fe88c086fdda4c2696fb01ca6430bb99';


UPDATE title
SET entry_name='[Bibliothèque impériale, bâtiment du Télégraphe]. 1, Bibliothèque impériale : figure en élévation du mur séparant la bibliothèque impériale de la maison rue Vivienne N° 3 : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr159fbb67da9ce4bfc8c7a74b989c0bf51';


UPDATE title
SET entry_name='10/11/27, concours de la meilleure ouvrière de France, 11 rue Vivienne : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr16ee1ccc8b54e4846b50ed16e469ff88e';


UPDATE title
SET entry_name='Mathurin-Augustin Balthazar Barbereau / Franck, Paris'
WHERE title.id_uuid = 'qr1efd3879f50944624a7f9550288ab9a30';


UPDATE title
SET entry_name='Académie nationale de musique. La Korrigane,ballet fantastique de MM. F. Coppée et L. Mérante. Musique de Ch. M. Widor. Publication du Ménestrel, Paris, 2 bis rue Vivienne, Heugel & fils éditeurs...En vente ici (portr de Rosita Mauri) : [affiche] / E. Buval'
WHERE title.id_uuid = 'qr1543427c471a94e239b3a418453c6a47e';


UPDATE title
SET entry_name='M.me Ugalde, rôle de la princesse dans "La dame de pique" / A. L.'
WHERE title.id_uuid = 'qr1204391ff06e7477db4a477058a3ddb39';


UPDATE title
SET entry_name='Melle Robert dans "La filleule des fées", Théâtre de la Nation / [lithogr. signée A. L.]'
WHERE title.id_uuid = 'qr1fbf6c624d1a543b89808411eb995b2ca';


UPDATE title
SET entry_name='M.me Ugalde, rôle de la princesse dans "La dame de pique" (2e acte) / A. L.'
WHERE title.id_uuid = 'qr1a47c5255cdb84e5fbedbf12875befd8b';


UPDATE title
SET entry_name='Bibliothèque Nationale sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr155cf3c6f39934874be90fa5fe94d7029';


UPDATE title
SET entry_name='Hôtel Desmarais [i.e. Desmarets] 18 Rue Vivienne : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1d0d3afcfe22b4f3da4212bba1b53d0fd';


UPDATE title
SET entry_name='Bibliothèque Nationale, sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1b32aab6671dc49ac9a7450f4a2505b34';


UPDATE title
SET entry_name='Bibliothèque Nationale, sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr18740e0ed26b4458fb2f69d3284b69a53';


UPDATE title
SET entry_name='Bibliothèque Nationale, sur la rue Vivienne : ancien Hôtel Mazarin : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1447a346b2ee74a60be23288f4133f40d';


UPDATE title
SET entry_name='Angle de la rue Beaujolais et Vivienne : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr18cd1390cba8f4544aa749b6249cf2bf7';


UPDATE title
SET entry_name='Jardin d''hiver, Champs-Élysées : [estampe]'
WHERE title.id_uuid = 'qr1a87dff2fceab430aa5bdf544760a528f';


UPDATE title
SET entry_name='Le voisin a eu une fameuse idée... : [estampe]'
WHERE title.id_uuid = 'qr13117ecb89649452590ea0551f3eada9f';


UPDATE title
SET entry_name='Officier russe suivi de son brosseur : [estampe]'
WHERE title.id_uuid = 'qr1c49fe732fc1c4865a782eb754b388320';


UPDATE title
SET entry_name='A Odessa : [estampe]'
WHERE title.id_uuid = 'qr17283677aa86e4e9d925e3a00bc013d49';


UPDATE title
SET entry_name='La Grèce mise en ébullition : [estampe]'
WHERE title.id_uuid = 'qr13442a3da0a67484e9234fe9d4a185af0';


UPDATE title
SET entry_name='"Allons, camarades... faut trinquer avec les amis... vous pouvez avaler ça sans scrupule, ça n''est pas du vin !...", dit une cantinière à un soldat turc, en lui offrant de l''eau-de-vie pour trinquer avec un soldat anglais et un soldat français : [estampe]'
WHERE title.id_uuid = 'qr166597c2a709e4b3f8b0e5653f959db94';


UPDATE title
SET entry_name='La marine russe en traitement : [estampe]'
WHERE title.id_uuid = 'qr1dd0931d62b7d4c43a84529c251e9db82';


UPDATE title
SET entry_name='"Voilà la Guerre !..... Sauve qui peut !" s''écrient [...] quelques-uns des invincibles guerriers qui ont fait jusqu''ici le plus bel ornement du système de la paix à tout prix, au moment où ils viennent d''apprendre qu''il ne serait pas impossible qu''il pût arriver une chance douteuse de guerre à l''encontre des Etats-Unis [...] ces guerriers sont occupés à faire, à leur manière, leurs préparatifs militaires. L''un d''eux, le plus GRRRRAND de tous [...] le duc d''Orléans a jeté loin de lui son sabre et s''empresse de couper ses moustaches qu''il aime mieux noircir au cosmétique de la rue Vivienne, qu''au feu : [estampe]'
WHERE title.id_uuid = 'qr10e9708ab115e417785946c0bd9466c3b';


UPDATE title
SET entry_name='Encore un inconvénient : [estampe]'
WHERE title.id_uuid = 'qr19422955017204e4491ed47333738349d';


UPDATE title
SET entry_name='[Bibliothèque Nationale, projet de reconstruction, 1850]. 5, COUPES SUR LES COURS PARALLELES A LA RUE VIVIENNE : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr110c6e86ff24b43268afcec525459b519';


UPDATE title
SET entry_name='[Bibliothèque Nationale, projet de reconstruction, 1850]. 12, FAÇADE SUR LA RUE VIVIENNE : [dessin, plan] / Louis Visconti'
WHERE title.id_uuid = 'qr17bf6e05341cc45c2a5437020a56bade3';


UPDATE title
SET entry_name='[Bibliothèque Royale ou Nationale, projets d''aménagement ou de reconstruction sur le site de la rue de Richelieu [entre 1828 et 1853]]. 1, [Plan du rez-de-chaussée des bâtiments élevés au Sud, à l''Ouest et au Nord de la deuxième cour de l''ancien Trésor, actuel jardin de Vivienne, et plan du premier étage du bâtiment Nord] : [dessin, plan] / [Louis Visconti ?]'
WHERE title.id_uuid = 'qr1b13e5211bc4740d982e5e0a247f9df79';


UPDATE title
SET entry_name='Loïsa Puget (M.me Gustave Lemoine) / M. Alophe'
WHERE title.id_uuid = 'qr137e22339443641889c7b80c2b67c1af6';


UPDATE title
SET entry_name='Anna Van Ghell / Franck, phot.'
WHERE title.id_uuid = 'qr19647c3f0fab04c26a32f0e0c9b0f3143';


UPDATE title
SET entry_name='[Ambroise Thomas] / Pierre Petit photographe'
WHERE title.id_uuid = 'qr16f5da6d257fd4717b81962768c14e8e9';


UPDATE title
SET entry_name='M.me Sontag, rôle de Luisa dans "Le Tre nozze" / A. L.'
WHERE title.id_uuid = 'qr197ca41d178ce4ee89affccf332070fa5';


UPDATE title
SET entry_name='La Dame de Pique, schottisch pour le piano par Pilodo : [Delphine Ugalde, rôle de la princesse] / Victor Coindre'
WHERE title.id_uuid = 'qr1e6f1a3ec3afe4ed2bfaf9cbe15f253f8';


UPDATE title
SET entry_name='Anna Van Ghell / Franck, phot.'
WHERE title.id_uuid = 'qr1693b47bcb546440bbb3e6bec49352624';


UPDATE title
SET entry_name='Mlle Emma Livry, "Le Papillon" / [lithogr. de] Jacotin ; d''après la phot. Disderi'
WHERE title.id_uuid = 'qr1e4df155271fa491d8d8f91b86735fe1a';


UPDATE title
SET entry_name='Pauline Viardot / V. Plumier, phot.'
WHERE title.id_uuid = 'qr1fed83547f6b8484aba15354507f7faca';


UPDATE title
SET entry_name='[La Nina de la rue Vivienne, vaudeville de Dartois et Gabriel : costume de Minette (Nina)]'
WHERE title.id_uuid = 'qr1717137f3973946f2b6e1f7df42d547ee';


UPDATE title
SET entry_name='Bourse de Paris [foule devant la Bourse] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1212456825cde428f8e53a348c8471fda';


UPDATE title
SET entry_name='[Bourse de Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr18e13580f16a64d41873e982269b4c4ed';


UPDATE title
SET entry_name='Bourse de Paris, 3-4-1910 [Paris, 2e arrondissement] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr16786b2fe5ade454fa6a6df7e53527449';


UPDATE title
SET entry_name='Bourse de Paris [la corbeille] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr128734124debd44dc8fbf36550576f729';


UPDATE title
SET entry_name='[Bourse de Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1d5b3810ecc05444f95e92ebf8cfdeef7';


UPDATE title
SET entry_name='Bourse de Paris : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr12a6c78ea3f4b4202ab4cc3e0a593c6a3';


UPDATE title
SET entry_name='Bourse à Paris, 3-4-1910 [Paris, 2e arrondissement] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1f60fc95b6fd3490b95af7575121ed5ca';


UPDATE title
SET entry_name='La Bourse à Paris, 2-4-1910 [Paris, 2e arrondissement] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr18bc984c4eef2460ca1d95c3dafe8bbfa';


UPDATE title
SET entry_name='[Bourse de Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1580e3d5995434847abd0c57c97c82dec';


UPDATE title
SET entry_name='Bourse de Paris : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1894cf56fa4f94715bfb627e7f1d712c6';


UPDATE title
SET entry_name='Bourse de Paris : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1698190aac125492e9d9bcd2e24352f2c';


UPDATE title
SET entry_name='Bourse de Paris : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr11c47ab79fcc748ec98cf17f40706f8df';


UPDATE title
SET entry_name='La Bourse de Paris] : [estampe] ([Fumé]) / Doré ; Dumont'
WHERE title.id_uuid = 'qr121c3199cd94e47538641017dd4a16eca';


UPDATE title
SET entry_name='20/7/26, les abords de la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1b23b791df82c4f168d8a6aa79abdf884';


UPDATE title
SET entry_name='Le Gamin de Paris quittant le Gymnase en faisant voile pour les Variétés : [estampe]'
WHERE title.id_uuid = 'qr19366e9295e804359927ca00284a72ddf';


UPDATE title
SET entry_name='[La Bourse] : [dessin]'
WHERE title.id_uuid = 'qr11c8a6acac00b40aa9094a7ae23f9aabb';


UPDATE title
SET entry_name='[La Bourse] / Dessiné par Montaut'
WHERE title.id_uuid = 'qr1f65b99a926194fc7b3392394e2f346f9';


UPDATE title
SET entry_name='La Bourse : [estampe]'
WHERE title.id_uuid = 'qr1fb6e13123a5f4a21a06d599cb40cf2c5';


UPDATE title
SET entry_name='La Bourse : [estampe]'
WHERE title.id_uuid = 'qr18e39daebbca8465db456418dce382fc4';


UPDATE title
SET entry_name='La Bourse, la corbeille : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1f6884a8b301a43b0a45e7e91f4096149';


UPDATE title
SET entry_name='Mystères de Paris, Statuettes par Emile Thomas 6 fr. L. Vatinel, Boulon fres Editeurs; En vente chez Susse frères, Pl. de la Bourse... : [affiche] ([Variante de couleurs]) / [Non identifié]'
WHERE title.id_uuid = 'qr18f385d0bdbab43e19c7576a44c8c869f';


UPDATE title
SET entry_name='La Bourse pendant la guerre, côté face : la poste : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr15a749338e84e4e369bfcdd1936f97336';


UPDATE title
SET entry_name='20/2/19 [i.e. 19/2/20] M. [Frédéric François-] Marsal à la Bourse : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1d4e845dcb6e943b48bbae85c0e771243';


UPDATE title
SET entry_name='[La double échelle, opéra-comique d''Ambroise Thomas et Eugène de Planard : costume de Fargueil (Lucas)]'
WHERE title.id_uuid = 'qr1e5f86f46fa774fb4b5b28aec0a373f4e';


UPDATE title
SET entry_name='[L''an mil, opéra-comique de Grisar, Mélesville et Foucher : costume de Roy (Godefroy)]'
WHERE title.id_uuid = 'qr169045a93482946b48543226f11bb86b6';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costumes de Mme Cinti-Damoreau (rôle d''Angèle)]'
WHERE title.id_uuid = 'qr15624b1c60ae8464f8c92620695a533c8';


UPDATE title
SET entry_name='[Les deux reines, opéra-comique de Scribe, Arnould et Monpou : costume d''Inchindi (Koller) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr184a97e9c2112433daf95204e9d676883';


UPDATE title
SET entry_name='[L''ambassadrice, opéra-comique de Scribe et Saint-Georges : costume de Madame Cinti-Damoreau (Henriette) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr18f865b0f76d84047b33026e5576aacf0';


UPDATE title
SET entry_name='[La double échelle, opéra-comique d''Ambroise Thomas et Eugène de Planard : costume de Couderc (le Chevalier)]'
WHERE title.id_uuid = 'qr144057550dca04f9993149de732f50ddc';


UPDATE title
SET entry_name='[La double échelle, opéra-comique d''Ambroise Thomas et Eugène de Planard : costume de Mlle Prévost (Susanne)]'
WHERE title.id_uuid = 'qr16319eb551b1b469f8a5c756c5a3b1c8d';


UPDATE title
SET entry_name='[Marguerite, opéra-comique de Boieldieu, Scribe et Planard : costume de Mademoiselle Berthault (Justine)]'
WHERE title.id_uuid = 'qr1def41bdb3edf44d8aa2529e4d312e33a';


UPDATE title
SET entry_name='[Le perruquier de la Régence, opéra-comique de Planard, Duport et Thomas : costume de Chollet (Fléchinel)]'
WHERE title.id_uuid = 'qr1d51e66945d3c401fa1e0733f38cf121e';


UPDATE title
SET entry_name='Robin des bois, opéra de Weber, Castil-Blazeet Sauvage : costume de Jansenne (rôle de Tony) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr12874d0490cd1486080a1e0422d24f79f';


UPDATE title
SET entry_name='[La dame d''honneur, opéra-comique de Duport, Monnais et Despréaux : costume de Mademoiselle Prévost (Marguerite)]'
WHERE title.id_uuid = 'qr19773e9b21e22489c91b3277ab7aa0823';


UPDATE title
SET entry_name='[Régine ou les deux nuits, opéra-comique de Scribe et Adam : costume de Mademoiselle Berthault (Tiennette)]'
WHERE title.id_uuid = 'qr16c3cb571f05c48bd9a3665ec4e31ac8f';


UPDATE title
SET entry_name='[L''éclair, opéra-comique de Planard, Saint-Georges et Halévy : costume de Couderc (Sir Georges) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1306639605ada4c95bd0f27da473b26dc';


UPDATE title
SET entry_name='La minute de silence à la Bourse de Paris : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr174ab33a7566b41b1b5749f591ebd280c';


UPDATE title
SET entry_name='22/9/24, autobus express [Paris, direction Bourse] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1fe82a85045e04fa89229161b7584e0f7';


UPDATE title
SET entry_name='[La figurante ou l''amour et la danse, opéra-comique de Scribe, Dupin et Clapisson : costume de Mademoiselle Rossy (Palmyre)]'
WHERE title.id_uuid = 'qr18eba7a7f43d84142ac458fdce6c7b793';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Couderc (rôle d''Horace)]'
WHERE title.id_uuid = 'qr1a1596e14ec3a4bdea6a84e262e31091c';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Mme Cinti-Damoreau (rôle d''Angèle)]'
WHERE title.id_uuid = 'qr14adc8497d6cb49ce84cce6aee8562583';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Mme Cinti-Damoreau (rôle d''Angèle)]'
WHERE title.id_uuid = 'qr198f5dd32f12b44e98d5722df924587f1';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Thénard (Mergy) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1dddb64bbc9364508a3a09ced5bf8e45e';


UPDATE title
SET entry_name='[Le postillon de Longjumeau, opéra-comique de Leuven, Brunswick et Adam : costume de Mademoiselle Prévost (Madeleine) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr16eca9bc1b42b4c0f819be2b316a7aa4b';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Mlle Berthault (rôle de Brigitte)]'
WHERE title.id_uuid = 'qr15ef2779ceac64658af4c6b510051222f';


UPDATE title
SET entry_name='L''Ambassadrice. Acte III. Scène II : [estampe]'
WHERE title.id_uuid = 'qr1820da49441654bde9d39390f4f5a2cfd';


UPDATE title
SET entry_name='[Les gondoliers, opéra-comique en deux actes de Blangini, Champeaux et Bréant de Fontenay : costume de Léon (Bettino) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1286d34afd1db4fdfae988e15697b0fbd';


UPDATE title
SET entry_name='L''an mil. Scène III : [estampe]'
WHERE title.id_uuid = 'qr105933e328dc54e10bd5f2baa59bfe75d';


UPDATE title
SET entry_name='[Le remplaçant, opéra-comique de Scribe, Bayard et Batton : costume de Couderc (Pichot)]'
WHERE title.id_uuid = 'qr14cdd847005ad412aba0291a583cb951a';


UPDATE title
SET entry_name='[Le remplaçant, opéra-comique de Scribe, Bayard et Batton : costume de Jenny Colon (Marie)]'
WHERE title.id_uuid = 'qr100c90aeb5e574ec1998dd3724f083154';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Moreau-Sainti (rôle de Don Juliano de Massarena)]'
WHERE title.id_uuid = 'qr12c690991c42c45c3a33d16264ae8fd41';


UPDATE title
SET entry_name='[Piquillo, opéra de Dumas, Nerval et Monpou : costume de Jenny Colon (Dona Sylvia)]'
WHERE title.id_uuid = 'qr15faaa4872af54228a37b267e398e1a9e';


UPDATE title
SET entry_name='[Les chaperons blancs, opéra-comique d''Auber et Scribe : costume de Mademoiselle Prévost (Marguerite) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1f3a792cb724f4de492322f73e7555434';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber eet Scribe : costume de Féréol (rôle de Tsing-Sing) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1376965d99b604408877b75361eba7019';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Mme Pradher (rôle de Péki) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr14364fd03f3c74d989eb5645c1c67c1b5';


UPDATE title
SET entry_name='[Guise ou les états de Blois, drame lyrique de Planard, Saint-Georges et Onslow : costume de Jenny Colon (Paulette)]'
WHERE title.id_uuid = 'qr167e29d1c8a9442f5b5aac2153d3dae18';


UPDATE title
SET entry_name='[Le chalet, opéra-comique de Scribe, Mélesville et Adam : costume de Couderc (Daniel) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr13128201157e243bf997ad85cd94355b0';


UPDATE title
SET entry_name='[Robin des bois, opéra de Weber, Castil-Blazeet Sauvage : costume de Mme Casimir (rôle d''Anna) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr176650df4d4dc4123b45ab3d3c0cc28eb';


UPDATE title
SET entry_name='[Le portefaix, opéra-comique de Gomis et Scribe : costume de Mademoiselle Rifaut (Térésita) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1c7e147b8e92c4889818dcf64399d31a3';


UPDATE title
SET entry_name='[Marguerite, opéra-comique de Boieldieu, Scribe et Planard : costume de Mademoiselle Berthault (Justine)]'
WHERE title.id_uuid = 'qr10c4a8829358b444789220538c72baf24';


UPDATE title
SET entry_name='[La prison d''Edimbourg, opéra-comique de Scribe, Planard et Carafa : costume de Mademoiselle Massy (Jenny) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1f31502625d38402ea491550c215264fd';


UPDATE title
SET entry_name='[La double échelle, opéra-comique d''Ambroise Thomas et Eugène de Planard : costume de Mlle Olivier (la Marquise)]'
WHERE title.id_uuid = 'qr1f5a2372cbf3d4267a1686cb798693d8e';


UPDATE title
SET entry_name='[Lestocq ou L''intrigue et l''amour : planche de costumes / dessins de Mélingue]'
WHERE title.id_uuid = 'qr1c7e2c91b9d9c4bf5a79ab7f2071bca7b';


UPDATE title
SET entry_name='[Micheline, opéra comique d''Adolphe Adam : costume de Mme Pradher (rôle de Micheline) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1d1fae50e8f0a4ead93025c0c12a692ac';


UPDATE title
SET entry_name='[Le domino noir, opéra-comique de Scribe et Auber : estampes]'
WHERE title.id_uuid = 'qr1786b05d703b54c64ae944da6bbb489ac';


UPDATE title
SET entry_name='[L''an mil, opéra-comique de Grisar, Mélesville et Foucher : costume de Madame Rossi (Blanche)]'
WHERE title.id_uuid = 'qr17c0f53b485e74c6ba7b014672882119a';


UPDATE title
SET entry_name='[Les chaperons blancs, opéra-comique d''Auber et Scribe : costume de Chollet (Prince Louis) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1bc25ff39eb094d2b92ab05c9a19150ff';


UPDATE title
SET entry_name='[La dame d''honneur, opéra-comique de Duport, Monnais et Despréaux : costume de Mademoiselle Prévost (Marguerite)]'
WHERE title.id_uuid = 'qr1bb0f21bfbc0c48818499627fac4f322a';


UPDATE title
SET entry_name='[Paris et Londres : esquisse de décor]'
WHERE title.id_uuid = 'qr1fd85b94b7af849b49db1592f5a88e6a2';


UPDATE title
SET entry_name='Scène de l''opéra de Jérusalem Trio avec trois Croisés de face à l''auberge du bon ermite : ici on chante à pied et à cheval : [estampe]'
WHERE title.id_uuid = 'qr12cd891a4df024e038676826e2ade78ce';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Thenard (rôle de Lestocq) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr10233e48ffb374096b42ea7e889b109ca';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume d''Inchindi (rôle de Tchin-Kao) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1a6d9ea8f90144da997fe2f2fa913ddaf';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Mme Pradher (rôle d''Elisabeth) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1d17dc6fecc104151b2890e84aaa6d189';


UPDATE title
SET entry_name='[L''éclair, opéra-comique de Planard, Saint-Georges et Halévy : costume de Madame Pradher (Mme Darbel) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1267ca4ab37df4cd688cf13f598e571dd';


UPDATE title
SET entry_name='[Le revenant, opéra-comique de Calvimont et Gomis : costume de Thénard (Sténie) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1ff337d6e6a9f42cdbbca36f80e0454ed';


UPDATE title
SET entry_name='[Le revenant, opéra-comique de Calvimont et Gomis : costume de Boulard (Sir Arundel) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr16e2fc14fd0df4329a8f7d022e64b04b7';


UPDATE title
SET entry_name='[Le chalet, opéra-comique de Scribe, Mélesville et Adam : costume de Madame Pradher (Betly) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1210c150b4f8042168efff11bd92b6fd2';


UPDATE title
SET entry_name='"C''est donc toujours vous qui payez ma brave dame ?" demande, en recevant une bourse, le "joueur de violon" à la France, tandis que le bal, où Louis-Napoléon conduit une jolie dame, bat son plein à l''arrière-plan : [estampe]'
WHERE title.id_uuid = 'qr18c6378c2e0344edf9078fc73eccfaf68';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Grignon (rôle de Lord Edford)]'
WHERE title.id_uuid = 'qr1576f448de82249ffa2f0de8fac1135fa';


UPDATE title
SET entry_name='[Le brasseur de Preston,opéra-comique d''Adam, Leuven et Brunswick : costume de Chollet (Robinson)]'
WHERE title.id_uuid = 'qr11cff4cab867541a4aafe7668461fc976';


UPDATE title
SET entry_name='[Le perruquier de la Régence, opéra-comique de Planard, Duport et Thomas : costume de Henri (Pierre-le-Grand)]'
WHERE title.id_uuid = 'qr13b38546063104dd29cf9b7df0b040b3f';


UPDATE title
SET entry_name='[Micheline, opéra comique d''Adolphe Adam : costume de Féréol (rôle de Maclou) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr16886d7f7e6914249a846b555a6b8610c';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1106ea8d063b34262b8d7f158760262d5';


UPDATE title
SET entry_name='[Le brasseur de Preston,opéra-comique d''Adam, Leuven et Brunswick : costume de Henri (Sergent Toby)]'
WHERE title.id_uuid = 'qr13c30b9a45691417c8bcb27593d1a3b6e';


UPDATE title
SET entry_name='Les deux reines, opéra-comique de Scribe, Arnould et Monpou : costume de Mademoiselle Prévost (Marie) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr18c54854322f746e49f42b8e6b853d821';


UPDATE title
SET entry_name='[Le remplaçant, opéra-comique de Scribe, Bayard et Batton : costume de Révial (Georges)]'
WHERE title.id_uuid = 'qr13563f2ea46954410bfafbf955e8f6b3f';


UPDATE title
SET entry_name='[L''an mil, opéra-comique de Grisar, Mélesville et Foucher : costume de Mademoiselle Berthault (Berthe)]'
WHERE title.id_uuid = 'qr1e8a4e251b7e34068877e7ff2d58631a7';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Féréol (Cantarelli) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1e52dd83adf8d484cbfaed82774d0e6cd';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Mme Casimir (rôle de Stella) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr11d1146a089eb4ac9a23cf48fdbda370b';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Révial (rôle de Dimitri) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1372e61549cdf451ba5f694e2414d02e2';


UPDATE title
SET entry_name='[Piquillo, opéra-comique d''Alexandre Dumas et Gérard de Nerval : estampes]'
WHERE title.id_uuid = 'qr1a8d73f20c1d6407d897f9cd6aa0be4aa';


UPDATE title
SET entry_name='Entrée d''Henry V à Paris, pour faire pendant à celle d''Henry IV : [estampe]'
WHERE title.id_uuid = 'qr12755223ea9fe46a89d3996c14f5756e6';


UPDATE title
SET entry_name='[La dame d''honneur, opéra-comique de Duport, Monnais et Despréaux : costume de Jansenne (marquis de Vaudreuil)]'
WHERE title.id_uuid = 'qr175dce24b22a844b18ff12c6304e3beb0';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Mademoiselle Massy (Nicette) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr130ca8cd5a9b04e43b3efc72db1d5dd5c';


UPDATE title
SET entry_name='[Le panier fleuri, opéra-comique de Thomas, Leuven et Brunswick : costume de Chollet (rôle de Beausoleil)]'
WHERE title.id_uuid = 'qr1231773aa84b24249ac0a0d3b451cc11a';


UPDATE title
SET entry_name='La minute de silence à la Bourse de Paris : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr109ca2f59447844228b4c1ad1c52da9a9';


UPDATE title
SET entry_name='[La double échelle, opéra-comique d''Ambroise Thomas et Eugène de Planard : costume de Fleury (Le Sénéchal)]'
WHERE title.id_uuid = 'qr140538f593b4d498db4187b7c83307052';


UPDATE title
SET entry_name='[Le proscrit ou le tribunal invisible, opéra-comique de Carmouche, Saintine et Adam : costume de Boulard (Piétro) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1120c378775c44bbe840f75bcdc69e180';


UPDATE title
SET entry_name='Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Madame Ponchard (Marguerite reine de Navarre) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1955ea113a6954412889dabac31dce233';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra- féerie de Scribe et Auber / dessin de Jules Bourdet]'
WHERE title.id_uuid = 'qr1949a28c66f38443687834ed1f4a75d1e';


UPDATE title
SET entry_name='[Lequel?, opéra-comique de Leborne, Duport et Ancelot : costume de Mademoiselle Berthault (Charlotte)]'
WHERE title.id_uuid = 'qr19eb399e69f114175856b3344c986d55f';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Mme Boulanger (rôle de Iacinthe)]'
WHERE title.id_uuid = 'qr1941d4194fdff4a888864c6edb9c7a75e';


UPDATE title
SET entry_name='[L''an mil, opéra-comique de Grisar, Mélesville et Foucher : costume de Jansenne (Raoul)]'
WHERE title.id_uuid = 'qr119539998957442f591b8b90f40d52b01';


UPDATE title
SET entry_name='[Le perruquier de la Régence, opéra-comique d''Eugène de Planard et Paul Duport : acte 2 scène 13 : estampe]'
WHERE title.id_uuid = 'qr172bee05b88de47fd811a26391669c40f';


UPDATE title
SET entry_name='L''Ambassadrice. Acte III. Sc. dern. : [estampe]'
WHERE title.id_uuid = 'qr16ae85f062d5e47ccbcefcd5172ab90e5';


UPDATE title
SET entry_name='[Le postillon de Longjumeau, opéra-comique de Leuven, Brunswick et Adam : costume de Chollet (Chapelou) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr18cf67929ef47484f8eb4d5a185ef5207';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Melle Massy (rôle de Catherine) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr14b5544a6bd994ba5b1a3732b527fff26';


UPDATE title
SET entry_name='Entrée d''Henry V à Paris, pour faire pendant à celle d''Henry IV : [estampe]'
WHERE title.id_uuid = 'qr1719e0c083aac4b0590fbf9b510c47c3d';


UPDATE title
SET entry_name='L''an mil. Scène XII : [estampe]'
WHERE title.id_uuid = 'qr1efab9a13c0ca48ec9867e58f4f0a97d4';


UPDATE title
SET entry_name='Les répétitions de théâtre (suite) / lithogr. par Eustache-Lorsay'
WHERE title.id_uuid = 'qr15e143dd185c0408099d961d36145f26f';


UPDATE title
SET entry_name='Costumes et accessoires du Domino noir : [estampe]'
WHERE title.id_uuid = 'qr114b7074b21fe4853b80a07eedfed0783';


UPDATE title
SET entry_name='[Ludovic, opéra-comique d''Hérold, Halévy et de Saint-Georges : costume de Féréol (Grégorio) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr100f5767f27324ecabc694acd29cd2718';


UPDATE title
SET entry_name='Le domino noir. Opéra-comique : Gil Perez (Mr Roy). Horace (Coudere). Juliano (Moreau-Sainti). Sirésille (Mme Damoreau-Cinti). Jacinthe (Mme Boulanger). L''ambassadeur (Grignon) : [estampe]'
WHERE title.id_uuid = 'qr1071812e84d70453a9acd9d7a8aa050f0';


UPDATE title
SET entry_name='[Le chalet, opéra-comique de Scribe, Mélesville et Adam : costume d''Inchindi (Max) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1b0b106ded53e463ab857f315771d79af';


UPDATE title
SET entry_name='[Lequel?, opéra-comique de Leborne, Duport et Ancelot : costume de Henry (Jacques)]'
WHERE title.id_uuid = 'qr19799b690f1734f2f81907fc98c5c7890';


UPDATE title
SET entry_name='udovic, opéra-comique d''Hérold, Halévy et de Saint-Georges : costume de Madame Pradher (Francesca) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr13244addd45d34d38a5862311019587bb';


UPDATE title
SET entry_name='[Le perruquier de la Régence, opéra-comique de Planard, Duport et Thomas : costume de Jenny Colon (Agathe)]'
WHERE title.id_uuid = 'qr11c918e1682c54158a2e2b7b807e2ea4e';


UPDATE title
SET entry_name='[La chatte blanche : esquisse de décor / Pierre-Luc Charles Cicéri]'
WHERE title.id_uuid = 'qr114780e6e274246eead3ae8c288dd8d8a';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Thenard (rôle d''Yanko) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1662faa68068e4aa7b938e684d4614328';


UPDATE title
SET entry_name='Le remplaçant, scène du 2me acte, Théâtre de l''Opéra Comique : [estampe] / Eug. Forest [sig.] ; lith. Caboche, Grégoire & C.ie'
WHERE title.id_uuid = 'qr1a7deb64286f34d278f5c0a0ee2f869cc';


UPDATE title
SET entry_name='[L''éclair, opéra-comique de Planard, Saint-Georges et Halévy : costume de Madame Camoin (Henriette) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1cdafc96c35fb4ac7b023bf99f70f0fe3';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Thenard (rôle de Lestocq) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr13995074a2b8541d19ae7b160f8de663c';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Mme Pradher (rôle de Péki) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr13fdfdabfd3b545d7920513b1f0492a99';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Révial (rôle du Prince Yang) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr18c5c2b6888484ddb9de2747006a26e8e';


UPDATE title
SET entry_name='[Le cheval de bronze, opéra-féérie d''Auber et Scribe : costume de Mme Ponchard (rôle de Taojin) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr12598d5f6b06f4518b876a2e460908c97';


UPDATE title
SET entry_name='L''Incomparable, la vraie, l''unique mine d''or, L''ETERNELLEGOGOFOUNTAIN [sic], je donne à volonté !!! : [défet de périodique] / Jean Veber'
WHERE title.id_uuid = 'qr1bc2b80f3e10b47ea9033250d208e421e';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1c8f3f8a66f6e4e44a1fed9947d277528';


UPDATE title
SET entry_name='[Guise ou les états de Blois, drame lyrique de Planard, Saint-Georges et Onslow : costume de Mademoiselle Prévost (Mme de Sauve)]'
WHERE title.id_uuid = 'qr1512f3fdd077b48aaa3917bc5b7bf937d';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Mme Pradher (rôle d''Elisabeth) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr152766248ec9e4c9aae7d05d4c4baef22';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Madame Casimir (Isabelle) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr19e395db834b44f778cc5c5977558d55a';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Mme Pradher (rôle d''Elisabeth) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr10894beb933f0436eb217ab9ea1ddee14';


UPDATE title
SET entry_name='[Le remplaçant, opéra-comique de Scribe, Bayard et Batton : costume de Jenny Colon (Marie)]'
WHERE title.id_uuid = 'qr1e9e313232fc744f0b2d173332e3314e9';


UPDATE title
SET entry_name='Rue de Charenton : [estampe]'
WHERE title.id_uuid = 'qr1fba6d9ccc96a45d297c466bc7691be59';


UPDATE title
SET entry_name='Rue Saint-Antoine : [estampe]'
WHERE title.id_uuid = 'qr114bf24ec81e74aab8e584191fcb0e8fb';


UPDATE title
SET entry_name='[Une journée de la Fronde ou la maison du rempart, opéra-comique de Mélesville et Carafa : costume de Madame Ponchard (duchesse de Longueville / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr184d03b7a1fa345b88d4cf672ddaa3af7';


UPDATE title
SET entry_name='Milon / Benjamin'
WHERE title.id_uuid = 'qr154ba24aef891411b911f973f80bed60a';


UPDATE title
SET entry_name='Scribe'
WHERE title.id_uuid = 'qr12db695f210774e9ca123b5823ca64096';


UPDATE title
SET entry_name='La paix du monde Congrès de Paris Mars 1856 : [estampe]'
WHERE title.id_uuid = 'qr1a212a4ba66c04335a3476ace15c39afc';


UPDATE title
SET entry_name='Guise, ou les états de Blois. Acte II, scène V ; Guise, ou les états de Blois. Acte III, scène XV : Album : [estampe]'
WHERE title.id_uuid = 'qr18ea4c061c68d4232ba8fc80b639463e0';


UPDATE title
SET entry_name='[Le revenant, opéra-comique de Calvimont et Gomis : costume de Clara Margueron (Sara) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr11072f1e74c4d43b9b4d60a5af9a35964';


UPDATE title
SET entry_name='[Le revenant, opéra-comique de Calvimont et Gomis : costume de Lemonnier (Sir John) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr15000f946bc9e4726a77e19176480482d';


UPDATE title
SET entry_name='Je vous disais bien que cette nouvelle Carotte est trop forte [etc.] : [estampe]'
WHERE title.id_uuid = 'qr14044962e7f294b0bbb0c077152acf2c8';


UPDATE title
SET entry_name='Épisode du voyage à Cherbourg : [estampe]'
WHERE title.id_uuid = 'qr128326655d8fc4c1d9c1078e581f98650';


UPDATE title
SET entry_name='Scènes de la Bourse : la foule sur les marches : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr10c992c8cc07f442e936451e665944c12';


UPDATE title
SET entry_name='[Le brasseur de Preston,opéra-comique d''Adam, Leuven et Brunswick : costume de Mademoiselle Prévost (Effie)]'
WHERE title.id_uuid = 'qr1aa82630ac3e7408f8685fa32f29b3bd8';


UPDATE title
SET entry_name='[Le domino noir, opéra de Scribe et Auber : costume de Roy (rôle de Gil Perès)]'
WHERE title.id_uuid = 'qr114fbf1e57d5041c7b85e9b454c09c6e7';


UPDATE title
SET entry_name='[Les chaperons blancs, opéra-comique d''Auber et Scribe : costume de Chollet (Prince Louis) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1b392f4f4b59e44b387b5827f57bc9025';


UPDATE title
SET entry_name='Massol / [lithogr. de] Benjamin Roubaud'
WHERE title.id_uuid = 'qr18be6b8a746a346b1b5fb85d2a5a5d4af';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Henri (rôle de Golofkin) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr16737a246c7f649fe8b7a97ba6043965e';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Lemonnier (Comminge) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr11242ea1d4dc24aadbb0180ee361227bd';


UPDATE title
SET entry_name='[La reine d''un jour, opéra-comique de Scribe, Saint-Georges et Adam : costume de Masset (Marcel)]'
WHERE title.id_uuid = 'qr1ee06016d70804d5cbbfae92c15bddc79';


UPDATE title
SET entry_name='Les répétitions de théâtre'
WHERE title.id_uuid = 'qr16d8d3aa52eb34d80bf0cb6fdd6a8579f';


UPDATE title
SET entry_name='Tout n''est pas plaisir en voyage : [estampe]'
WHERE title.id_uuid = 'qr102da1845b6dd4fae9ae221015959d3a3';


UPDATE title
SET entry_name='L''offre d''une position stable : [estampe]'
WHERE title.id_uuid = 'qr197eeccf57adb4fe2a8c8644248e81b34';


UPDATE title
SET entry_name='[Sarah ou l''orpheline de Glancoé, opéra-comique de Mélesville et Grisar : costume de Mlle Jenny Colon (Sarah) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1d8181231266f451fa5509a81e2319a17';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Melle Peignat (rôle d''Eudoxie) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr14b1793c27ba64e01a1c2d39f399ea635';


UPDATE title
SET entry_name='La prison d''Edimbourg, opéra : ?ie de l''entracte : [estampe] / A.B. [sig.] ; lit. Bonnat F.res'
WHERE title.id_uuid = 'qr1769d75b4986b4fa7bbe6293e1556d40d';


UPDATE title
SET entry_name='[Le mauvais oeil, opéra-comique de Scribe, Lemoine et Puget : costume de Madame Cinti-Damoreau (Inès) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1729501d1c5504f0a98032f3a47dcc7a9';


UPDATE title
SET entry_name='[Le coureur de veuves : esquisse de décor / Pierre-Luc Charles Cicéri]'
WHERE title.id_uuid = 'qr150eda27558934277859b466164b3dd6d';


UPDATE title
SET entry_name='Regrets superflus ! : [estampe]'
WHERE title.id_uuid = 'qr19c527b04143e4647b6d3ab2c0321cc85';


UPDATE title
SET entry_name='Bivouac au Boulevard du Temple : [estampe]'
WHERE title.id_uuid = 'qr1fd30a2060c8444a8b1fd494e4a967621';


UPDATE title
SET entry_name='Les Écossais à Paris : [estampe]'
WHERE title.id_uuid = 'qr1e68e6f2c17134233a3eb8784b7c9f45f';


UPDATE title
SET entry_name='Anvers ; mariage du Prince Leopold et de la Princesse Astrid : minute de silence à la Bourse : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1b2b38f26089f4f01af1c430e3e79082f';


UPDATE title
SET entry_name='[Cosimo, opéra-bouffon de Saint-Hilaire, Duport et Prévost : costume d''Henri (Faribolini) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1dafda9736c3345f29c427b4828895ebc';


UPDATE title
SET entry_name='[Les deux reines, opéra-comique de Scribe, Arnould et Monpou : costume de Féréol (Magnus) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1049d4526c67946c3a4d20cdf9ace0310';


UPDATE title
SET entry_name='[La dame d''honneur, opéra-comique de Duport, Monnais et Despréaux : costumes de Mademoiselle Augusta (Mme de Nangis) et de Henri (Lajolais)]'
WHERE title.id_uuid = 'qr168dc2d92710a42fc8989274e3bb3e0a8';


UPDATE title
SET entry_name='[Guise ou les états de Blois, drame lyrique de Planard, Saint-Georges et Onslow : costume de Henri (Larchant)]'
WHERE title.id_uuid = 'qr1f8f76db51d3e411697354afbc6699e63';


UPDATE title
SET entry_name='[Guise ou les états de Blois, drame lyrique de Planard, Saint-Georges et Onslow : costume de Chollet (duc de Guise)]'
WHERE title.id_uuid = 'qr1268765714391442b9a84bee08d457ce5';


UPDATE title
SET entry_name='Les répétitions de théâtre (suite) / lithogr. par Eustache-Lorsay'
WHERE title.id_uuid = 'qr1c3b2d9f23a0f42149937fb96b2e7ec12';


UPDATE title
SET entry_name='[Le postillon de Longjumeau, opéra-comique de Leuven, Brunswick et Adam : costume de Mademoiselle Prévost (Madeleine) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1f5a7e1cf78024c478cfdd7d8604b4e85';


UPDATE title
SET entry_name='Enlèvement d''une barricade par les Gardes Mobiles : [estampe]'
WHERE title.id_uuid = 'qr113c6e0884b424934a03bac45e3003c56';


UPDATE title
SET entry_name='Le danger de visiter une flotte quand on n''a pas le pied marin : [estampe]'
WHERE title.id_uuid = 'qr164a71008c88e44b29245dc5ef7d2e281';


UPDATE title
SET entry_name='[Le proscrit ou le tribunal invisible, opéra-comique de Carmouche, Saintine et Adam : costume de Thénard (Donati) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1689f9eb1623e47d48426dde817be72fe';


UPDATE title
SET entry_name='[Lestocq, opéra-comique de Scribe et Auber : costume de Deslandes (rôle de Strolof) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr157eeebef24154c148cb6cfdb3517773c';


UPDATE title
SET entry_name='Tocqueville : [estampe] (2ème état) / h[onoré] D[aumier] 49 ; Imp. Aubert & C.ie'
WHERE title.id_uuid = 'qr1bbe07f9079634dc4875b4c005ac721db';


UPDATE title
SET entry_name='[Le pré aux clercs, opéra-comique de Planard et de Hérold : costume de Madame Casimir (Isabelle) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr13bdcfcde1a6a4ad1a3e94c704dddd289';


UPDATE title
SET entry_name='[Le bandit : esquisse de décor de l''acte I / Pierre-Luc Charles Cicéri]'
WHERE title.id_uuid = 'qr1cd559aba26624809a7a0e293ca16177a';


UPDATE title
SET entry_name='Enlèvement d''une barricade, rue Perdue (Quartier du Panthéon) : [estampe]'
WHERE title.id_uuid = 'qr106294bd01bc742ac8adfa370326f9568';


UPDATE title
SET entry_name='[Ludovic, opéra-comique d''Hérold, Halévy et de Saint-Georges : costume de Lemonnier (Ludovic) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr1088a2d3aa96c48ada20408b797855874';


UPDATE title
SET entry_name='Mlle Jenny Colon et Chollet, dans Piquillo, acte deuxième : [estampe] / Eug. Forest [sig.] ; lith. Caboche'
WHERE title.id_uuid = 'qr163df89ae1e8f4917a698db12164563ce';


UPDATE title
SET entry_name='Canonnade de la Place Baudoyer : [estampe]'
WHERE title.id_uuid = 'qr13f1163cab0fc46a2be56748086503bb1';


UPDATE title
SET entry_name='Duprez à l''Opéra dans Jérusalem délivrée : [estampe] / M. Deschamps [sig.] ; Bertrand [sig.]'
WHERE title.id_uuid = 'qr1ad074e85fa314da4afa64654d776eea5';


UPDATE title
SET entry_name='[Les gondoliers, opéra-comique en deux actes de Blangini, Champeaux et Bréant de Fontenay : costume de Madame Boulanger (Berta) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr170ece3a24d8646adb74379bc3aeb1362';


UPDATE title
SET entry_name='[L''éclair, opéra-comique de Planard, Saint-Georges et Halévy : costume de Chollet (Lionel) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr137e002a5766446a28ac4cb2466128a0d';


UPDATE title
SET entry_name='Une conversion inattendue : [estampe]'
WHERE title.id_uuid = 'qr1280d07a9fb2f42238c956333a29d170c';


UPDATE title
SET entry_name='Ni l''un, ni l''autre ! : [estampe]'
WHERE title.id_uuid = 'qr19d299db97aeb443d9b19b3566f56d47d';


UPDATE title
SET entry_name='Balayage des marches de la Bourse de Paris après la séance : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr1c42adb1dd5f4466caa47515d9410fcfa';


UPDATE title
SET entry_name='Décor du 3e Acte du Domino Noir : [estampe]'
WHERE title.id_uuid = 'qr1e8fd34f3513b4bba9c3bcbebd0a18ba9';


UPDATE title
SET entry_name='Ambroise Thomas / M. Alophe'
WHERE title.id_uuid = 'qr113eeaa0b44f94a9bb52b3c4fdba55c6f';


UPDATE title
SET entry_name='Quel singulier jeu !... chaque joueur se trouve avoir le Roi !.. : [estampe]'
WHERE title.id_uuid = 'qr1ee4d2a39cbed4540bb1b308bd27b6b3e';


UPDATE title
SET entry_name='Le candidat de la Gazette de France pour la Présidence de la République : [estampe]'
WHERE title.id_uuid = 'qr11270ce068ed8420fa33bc1dce3a2fec1';


UPDATE title
SET entry_name='[Le proscrit ou le tribunal invisible, opéra-comique de Carmouche, Saintine et Adam : costume de Madame Casimir (Antonia) / gravé par Maleuvre]'
WHERE title.id_uuid = 'qr145487a92f9744377b13b19b1f0001780';


UPDATE title
SET entry_name='Carte de Commissaire spécial du "Comité central Électoral en faveur de la Candidature de Louis Napoléon Bonaparte. 12, Boulevard Montmartre et 11, Passage Jouffroy [...] délivrée au Citoyen Paris, Capitaine rue Saint-Honoré" : [estampe]'
WHERE title.id_uuid = 'qr1877aebabadcc45879ea1ddf091258c5a';


UPDATE title
SET entry_name='"C''est donc toujours vous qui payez ma brave dame ?" demande, en recevant une bourse, le "joueur de violon" à la France, tandis que le bal, où Louis-Napoléon conduit une jolie dame, bat son plein à l''arrière-plan : [estampe]'
WHERE title.id_uuid = 'qr1606f663c6d94436ca202cd98e84fddac';


UPDATE title
SET entry_name='Le Triomphe de la Loi du 31 Mai : [estampe]'
WHERE title.id_uuid = 'qr1aec8f79c354b40e0b0aa6c2f532c9b8a';


UPDATE title
SET entry_name='Le domino noir. Acte II. Revue du théâtre. Opéra comique : [estampe]'
WHERE title.id_uuid = 'qr12da277019d0247779a5e3d1280c8a039';


UPDATE title
SET entry_name='Vous avez beau crier : ça ira bien... ça va tout seul !... : [estampe]'
WHERE title.id_uuid = 'qr1e8b4c92aed0f466d988cd1239f33e802';


UPDATE title
SET entry_name='Bourse des valeurs : la reprise des affaires, les acheteurs devant les tableaux : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr10652087425b843aca8087c757cdfe538';


UPDATE title
SET entry_name='"Barrière Poissonnière. Poursuite des insurgés, dans le clos Saint-Lazare". Au bas, à dr., d'', Ed. de Beaumont : [estampe]'
WHERE title.id_uuid = 'qr1605b3dd302674497a149be62ac860a8d';


UPDATE title
SET entry_name='Licenciement de la Société du Dix-Décembre : [estampe]'
WHERE title.id_uuid = 'qr1eacbd2a9ea884b628620789eaabaaa19';


UPDATE title
SET entry_name='Le domino noir. Premier Acte Théâtre de l''opéra Comique : [estampe] / Eug. Forest [sig.]'
WHERE title.id_uuid = 'qr11745410116704e57a2c36a0bad483d53';


UPDATE title
SET entry_name='Voyez Berryer, célèbre horticulteur, Fesant [sic] ici sa ronde matinale : [estampe]'
WHERE title.id_uuid = 'qr162578815b56149d1b35052363738b0c1';


UPDATE title
SET entry_name='Le remboursement des 45 centimes nouveau tour financier inventé par le célèbre prestidigitateur Berryer : [estampe]'
WHERE title.id_uuid = 'qr183c7fabb8cca43e584f55425b43d3d22';


UPDATE title
SET entry_name='"Les dames de la halle abusant de la permission" qui leur est donnée d''embrasser le Président, se précipitent sur lui, ou, quand elles ne peuvent l''embrasser, se contentent de Léon Faucher (qui porte sa Légion d''Honneur), et de Jacques Berger, préfet de la Seine : [estampe]'
WHERE title.id_uuid = 'qr1028c63123a7b42eb83077c3048b4d0b8';


UPDATE title
SET entry_name='Vive Napoléon !... Vive la sociale !... Vive le Président... Vive la République ! Vive l''Empereur ! : [estampe]'
WHERE title.id_uuid = 'qr1bf9eed897e6b486ea7796e3cb0522c91';


UPDATE title
SET entry_name='[Le brasseur de Preston, opéra-comique d''Adolphe de Leuven et Brunswick : acte 2, scène 10 : estampe]'
WHERE title.id_uuid = 'qr198f79d41b6be465491275407f227cc7d';


UPDATE title
SET entry_name='Projet d''une nouvelle pièce de cinq francs à faire frapper par l''hotel des monnaies de la République Française : [estampe]'
WHERE title.id_uuid = 'qr10508c2f6566045a0bc5612ae2bad8847';


UPDATE title
SET entry_name='Les aveugles : [estampe]'
WHERE title.id_uuid = 'qr1db5f32d8ea5c41e28c60a9ac0acdd3cc';


UPDATE title
SET entry_name='Philippe Musard au Bal de l''Opéra : un triomphe aussi agréable que périlleux'
WHERE title.id_uuid = 'qr18d7a8483701f423f957e84b79eb93c01';


UPDATE title
SET entry_name='"Attaque de l''entrée du Faubourg du Temple", gardée par une barricade toute fumante : [estampe]'
WHERE title.id_uuid = 'qr12d504fad6dc54bdd9a4947384997e327';


UPDATE title
SET entry_name='Les Principaux Personnages de la Comédie qui se joue en ce moment aux Champs-Élysées : [estampe]'
WHERE title.id_uuid = 'qr1a9778127f59e496b9e5fbcdc844813ae';


UPDATE title
SET entry_name='Chameau à Paris [l''animal dans une rue aux abords du palais Brongniard monté par son maître, un attroupement autour de lui] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr14db80ff266d54d7791d995b43d312484';


UPDATE title
SET entry_name='Jacquelin et son tricycle-balayeur, Place de la Bourse : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr17e0d5b541742423eab63a284ad964c93';


UPDATE title
SET entry_name='[Escalier au 13 de la Galerie Vivienne ] : [photographie] / [Atget'
WHERE title.id_uuid = 'qr1c0e21ff0a1e64d8e9c3846d33b319600';


UPDATE title
SET entry_name='[Escalier au 13 de la Galerie Vivienne ] : [Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1bc02fb35bb84403c877c0ebd390fdca5';


UPDATE title
SET entry_name='Théâtre de la Galerie Vivienne ... La Belle et la bête. Féerie en 3 actes et 7 tableaux'
WHERE title.id_uuid = 'qr1394378ab879740a69fc1d0b9bf5993cf';


UPDATE title
SET entry_name='Portrait de Joséphine, Impératrice des Français, en buste, de 3/4 dirigé à droite'
WHERE title.id_uuid = 'qr197acfa5466b9484ca4c9580765291d1a';


UPDATE title
SET entry_name='Amélie Beauharnais, Imperatrice du Brésil'
WHERE title.id_uuid = 'qr1778cad9566eb42fd8b354dbafb3059c1';


UPDATE title
SET entry_name='Sir Hudson Lowe'
WHERE title.id_uuid = 'qr1f0ee0877d358442d9632f78e7e97aea9';


UPDATE title
SET entry_name='Tombeau de Napoléon à Saint Hélène. Lieut.t au 91.e rég.t de l''armée anglaise, préposé à la garde du tombeau'
WHERE title.id_uuid = 'qr16b9766bf9b524b5b9dea40759a82e456';


UPDATE title
SET entry_name='Café de Rohan - 1 place du Palais Royal, Guerre mondiale (1914-1918)'
WHERE title.id_uuid = 'qr1f2e4416fd4574f72a9fe32e8bb7b6d64';


UPDATE title
SET entry_name='Plans du quart nord-est du Quadrilatère Richelieu (1850-1940) : plans d''ensemble, bâtiment Colbert- Vivienne et bâtiment sur les courettes nord. Bâtiment Colbert- Vivienne . Plan d''ensemble. Rez-de-chaussée. Mobilier : casiers'
WHERE title.id_uuid = 'qr18de444dac644451db6c0bc9dc6c4dbbe';


UPDATE title
SET entry_name='Plans du quart nord-est du Quadrilatère Richelieu : bâtiment Vivienne . Pavillon du milieu. Rez-de-chaussée. Vestibule. Equipement technique : chauffage, conduit de chaleur'
WHERE title.id_uuid = 'qr132ffd1d82b6f472281fcb5a5feeeca60';


UPDATE title
SET entry_name='Avant ! - Pendant ! - Après'
WHERE title.id_uuid = 'qr1a5149003b0714d0c9dd2e4c2d0f48a78';


UPDATE title
SET entry_name='Promesse de mariage : [estampe] / Henry Monnier'
WHERE title.id_uuid = 'qr1b0ab2105ccf04bb788fe5edb3d23ea87';


UPDATE title
SET entry_name='Plans du quart sud-est du Quadrilatère Richelieu (1850-1940). Jardin. Plan d''ensemble. 1890-1900 : aménagement. Equipement technique : réseau d''incendie'
WHERE title.id_uuid = 'qr1be36156256454f33972e5941aebf8ae3';


UPDATE title
SET entry_name='Portraits de Boireau, Morey, Pepin et Fieschi'
WHERE title.id_uuid = 'qr198be3a15a10c4b88bf3190c57e8d04f6';


UPDATE title
SET entry_name='Pierrot-journal'
WHERE title.id_uuid = 'qr1879084d90abb427ca3bc0317df049681';


UPDATE title
SET entry_name='Macédoine Patriotique. N° 3'
WHERE title.id_uuid = 'qr169ff79b8167e4433be5237bb1c2d2247';


UPDATE title
SET entry_name='L''Étoile de France Polka'
WHERE title.id_uuid = 'qr19c4a3b0afb194007ab6a5a9b36d208ab';


UPDATE title
SET entry_name='Les Confessions de Ninon de Lenclos publiées par Eugène de Mirecourt... paraissant dans Le Journal du Jeudi... Administration : Passage Colbert, 28 : [affiche] / Donjean lit Auteur : Donjean, Gustave (1800-1899). Peintre du modèle'
WHERE title.id_uuid = 'qr18d0477f46426441d808e99e965840870';


UPDATE title
SET entry_name='Louis-Napoléon Bonaparte, Président de la République française. Fondateur de la Constitution de 1852, Élu le 22 Décembre 1851, par 7,500,000 votes. Chef de la nation française : [estampe]'
WHERE title.id_uuid = 'qr13f0a333a74f44c2080d250125ef67b90';


UPDATE title
SET entry_name='Tombeau de l''Impératrice Joséphine : [estampe]'
WHERE title.id_uuid = 'qr138f1d61d1173440d9bd12426712dd453';


UPDATE title
SET entry_name='Le Favori de la Reine ... par A. Arnould et N. Fournier publié par Le Journal du Jeudi.. . Illustrations de F. Philippoteaux. Administration Passage Colbert, 28 : [affiche] / G. Perrichon ; F. Philippoteaux'
WHERE title.id_uuid = 'qr1c411093263704e41997b3b64d20b0b31';


UPDATE title
SET entry_name='La Reine Margot par Alexandre Dumas, publiée par le journal "les Bons Romans" : administration : passage Colbert, 26 : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr104c4f90e31ea4d5fb6c815f0cb4d6280';


UPDATE title
SET entry_name='Les Compagnons de Jéhu par Alexandre Dumas. Les Bons romans journal illustré, [26] passage Colbert : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr126329c3f03274944a3eb5f1124ce1862';


UPDATE title
SET entry_name='La Reine Margot par A. Dumas. Publiée par le journal les Bons romans, administration : passage Colbert, 26 : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1b57e27e08c384e96a719ab89c2a775ec';


UPDATE title
SET entry_name='Charles ?? (Ses chapeaux), marque déposée, 9fr 90, brosse, carton et initiales compris... 33 rue Vivienne ; 18 bould St michel'
WHERE title.id_uuid = 'qr1186709d8eed3478faa5dce09dd377593';


UPDATE title
SET entry_name='[s.n.]'
WHERE title.id_uuid = 'qr1c473bae210ef404083ea76cb1b768e4f';


UPDATE title
SET entry_name='[Talma] / Franck phot. ; [d''après une estampe]'
WHERE title.id_uuid = 'qr1b87c8e3d7cdb4c24b5e88c99744d666c';


UPDATE title
SET entry_name='Franck (1816-1906). Photographe'
WHERE title.id_uuid = 'qr11adb8baafcd04fc0a72f6af07410cdb7';


UPDATE title
SET entry_name='Bibliothèque nationale coupe suivant le grand axe de la salle ovale : [dessin] / JL Pascal'
WHERE title.id_uuid = 'qr189c53f8f7ae64eab880eeed29ec6b806';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne (attachement de maçonnerie, 1860)]. 1, BIBLIOTHEQUE IMPERIALE // Jardin sur la Rue Vivienne. Bassin : Attachement // de Maçonnerie N° 20 // Dallemagne et Vernaud Entrepreneurs : Exercice 1860 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1018177c7ae864f9abd2670487d80cd89';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf et ses abords (attachement de maçonnerie, 1858)]. 3, BIBLIOTHEQUE IMPERIALE / Cour N° 10 : Attachement // de Maçonnerie N° 1 // M.rs V.r Lemaire Dallemagne // et Vernaud Entrepreneurs : Exercice 1858 // Mons.r Labrouste Architecte // Mons.r André Inspecteur : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1cb1f9a973e92471f814b46957ecdb2f1';


UPDATE title
SET entry_name='[Bibliothèque nationale, pavillon de l''escalier (?) (attachement de maçonnerie, 1872)]. 8, BIBLIOTHEQUE NATIONALE // Surélévation du mur mitoyen avec // la propriété de Monsieur [sic] // Hôtel des Etrangers – Rue Vivienne n° 3 : Attachement // de Maçonnerie N° 1 // F. Vernaud Entrepreneur : Exercice 1872 // M. Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Nationale]'
WHERE title.id_uuid = 'qr156133e944f024f33bf748c14fe8b29ca';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858)]. 6, BIBLIOTHEQUE IMPERIA[LE] // Bâtiment en aile à gauche dans la cour de l''horloge // Complément des Caves : Attachement // de Maçonnerie N° 8 // M.r Victor Lemaire Entrepreneur : Exercice 185[8] // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1fb0364611c1043cabf6a8d229d4cacb3';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858)]. 8, BIBLIOTHEQUE IMPERIALE // Détail du Perron de gauche dans la Cour de l''horloge : Attachement // de Maçonnerie N° 6 // Vor Lemaire Dallemagne & Vernaud // Entrepreneurs : Exercice 1858 // Monsieur Labrouste Architecte // Monsieur André Inspecteur : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr189c71415a9c04b88aa21e2f6f7508b29';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858 ?)]. 2, Réfection de la cour : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1e742cf36bc52456b87b0aee5370a01f6';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858 ?)]. 5, BIBLIOTHEQUE [IMPERIALE] // Bâtiment en Aile à gauche dans la [Cour de] l''Horloge // Détail des Murs de Refend : Attachement de Maçon[nerie N° …] / Mons.r Victor Lemaire Entrepreneur : Exercice 185[8 ?] / Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr128d3c172c18144f6a1c4c3199a6a682e';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1862)]. 1, BIBLIOTHEQUE IMPERIALE // Pavillon de l''horloge : Attachement // de Maçonnerie N° 1 // A. Dallemagne & Vernaud // Entrepreneurs : Exercice 1862 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1e589936583834edab614790423a9f846';


UPDATE title
SET entry_name='Salle Vivienne... Soirée Olivier Métra... : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1404f052140bf49798790ee77d063a827';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf et ses abords (attachement de maçonnerie, 1858)]. 9, BIBLIOTHEQUE IMPERIALE : Attachement // de Maçonnerie N° 4 // M.M. V.or Lemaire // Dallemagne & Vernaud Entrepreneurs : Exercice 1858 // Mons.r Labrouste Architecte // Mons.r André Inspecteur : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1b25be60548f24bd195794b59763d0c21';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 12, Espace occupé actuellement par le cours d''archéologie : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr119dec6b4880d45fba03f036f6e8a6441';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf et ses abords (attachement de maçonnerie, 1858)]. 4, BIBLIOTHEQUE IMPERIALE // Travaux divers : Attachement // de Maçonnerie N° 5 // M.r Victor Lemaire Entrepreneur : Exercice 1858 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1f0028d063dbc496faf1615c05eb9f2e7';


UPDATE title
SET entry_name='[Bibliothèque impériale, aile construite par Louis Visconti, rue Vivienne (attachement de maçonnerie, 1855)]. 3, Bibliothèque Impériale // Attachement de Démolition à Exécuter : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1b9208da3d0324e48b744e42e195373d4';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne (attachement de maçonnerie, 1858)]. 10, BIBLIOTHEQUE IMPERIALE // Détail des 2 Vases en Roche neuve de Soissons au dessus // des Piédestaux de chaque côté du Perron dans la Jardin sur la rue Vivienne : Attachement // de Maçonnerie N° 5 // V.or Lemaire Dallemagne // et Vernaud Entrepreneurs : Exercice 1858 // Mons.r Labrouste Architecte // Mons.r André Inspecteur : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr12cdc929a438146d29a13603ad678d3e7';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 11, BIBLIOTHEQUE IMPERIALE // Détail des Façades du Pavillon neuf à Rez-de-Chaussée : Attachement de // Maçonnerie N° 17 // Mons.r V.or Lemaire, Entrepreneur : Exercice 1855 // Monsieur Labrouste, Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale] Auteur : Agence des travaux de la Bibliothèque nationale (Paris).'
WHERE title.id_uuid = 'qr1c83de9cfad844df8879872b4b204c282';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 7, BIBLIOTHEQUE IMPERIALE // Travaux divers // Reconstruction de la partie des [sic] l''Angle des Rues Vivienne et des Petits Champs : Attachement de Maçonnerie N° 9 // Monsieur Victor Lemaire Entrepreneur : Exercice 1857 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr14912ddfb28af4ece8c3c5ade309f5c44';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 2, BIBLIOTHEQUE IMPERIALE // Travaux en Raccord // Rez-de-Chaussée et Entresol : Attachement de Maçonnerie N° 8 // Mons.r Victor Lemaire Entrepreneur : Exercice 1857 // Mons.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr190fdd6ac0fc34b96b2f19c7f66cde358';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 4, BIBLIOTHEQUE IMPERIALE // Travaux en Raccord : Attachement // de Maçonnerie N° 3 // M.r Victor Lemaire Entrepreneur : Exercice 1857 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr162a4e139b9c4400cb9d0afa391b3295a';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 3, BIBLIOTHEQUE IMPERIALE // Travaux en Raccord : Attachement de Maçonnerie N° 1 // Monsieur V.or Lemaire Entrepreneur : Exercice 1857 // Mons. Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1c1b8c0a098674ba4be211fbbdbffd665';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1856)]. 3, BIBLIOTHEQUE IMPERIALE // Travaux en raccord : Attachement // de Maçonnerie N° 3 // M.r V.or Lemaire Entrepreneur : Exercice 1856 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1dbeb7ef4ec4c4625b4d9ed0247c2b6df';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858)]. 1, [BIBLIOTHEQUE] IMPERIALE // Démolitions : Exercice 1858 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1058b9dedf5244a698d3f56a4a7d78255';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 7, BIBLIOTHEQUE IMPERIALE // Travaux divers : Attachement de // Maçonnerie N° 5 // M.r V.or Lemaire Entrepreneur : Exercice 1855 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr11cf7ad576fb248dab4da28a6d7ab1b08';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 6, BIBLIOTHEQUE IMPERIALE // Rez de Chaussée : Attachement // de Maçonnerie N° 6 // M.r V.or Lemaire (Entrepreneur) : Exercice 1855 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr13384a703f8534addaf087d14d764c8e0';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 8, BIBLIOTHEQUE IMPERIALE // Détail des Façades dans la grande Cour : Attachement // de Maçonnerie N° 3 // M.r V.or Lemaire Entrepreneur : Exercice 1855 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1f2cad92521b5456cb144a4d06cb6444a';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne (attachement de maçonnerie, 1856)]. 7, BIBLIOTHEQUE IMPERIALE // Travaux Divers : Attachement // de Maçonnerie N° 15 // Monsieur V.r Lemaire, Entrepreneur : Exercice 1856 // Monsieur Labrouste, Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr17440803008f24f099f74b4133e9ca033';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 3, Cour de l''administration : [dessin, plan] / H. Lab'
WHERE title.id_uuid = 'qr14cc13ef3802a4a1cb49a0192a15732a1';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 26, Bibliothèque Impériale : [dessin, plan] / H. Labrouste'
WHERE title.id_uuid = 'qr118938bc116b64753a06cd6d6aad61340';


UPDATE title
SET entry_name='Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 13, Bibliothèque Impériale // Bâtiment d''administration de la bibliothèque // Sur la rue des petits champs : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1dba254b10bb9454cac5599111de49136';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 8, Vestibule du cours d''archéologie : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr179bac7d7f3f14b71a4d57a8c2153f587';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 10, Salle des Cours // détail du mur et piles devant supporter les planchers : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1ee6c145ae5ba435388499f56965a81a2';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 4, Cour de l''administration : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr10cc07818cf8d4f9fbe2a759469cf78b9';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 9, Vestibule de la Salle des cours : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr12cfb72ee56e54e8f9322461664ef5afc';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 1, Bibliothèque Impériale // 1.er projet : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr13659b273e44f4f1db7fb1784a0264347';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 7, Vestibule du cours d''archéologie : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr14c5584823bf640adb429d9b5f4a8df44';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 2, Bibliothèque Impériale // 2.e projet : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr15adcda715a594af49495b3d86de5cd70';


UPDATE title
SET entry_name='16 rue de Valois'
WHERE title.id_uuid = 'qr13e64664a25b14a36a9669f9682b927f1';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 23, Bibliothèque Impériale // Cour de l''ancien Trésor : [dessin, plan] / H. Labrouste'
WHERE title.id_uuid = 'qr10ea34cda38cf4f3fbef0c86949b1527a';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 8, [Étude de dallage pour le nouveau trottoir en hémicycle] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1112b3ea45c134232a2b6628a51bbbeca';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 25, [Toitures (modèles pour les poinçons A et B des toitures : élévations)] : [dessin, plan] / H. Labrouste'
WHERE title.id_uuid = 'qr1e7587d5f22b14e97b30f8214ae9962be';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 1, [Socle du réverbère, avec bahut et banc de pierre à la suite (élévations de face et de profil, plan)] : [dessin, plan] / H. Lab.'
WHERE title.id_uuid = 'qr1a1b19628eed14e2fa9b70e072c99c79c';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 5, [Étude de dallage pour le nouveau trottoir en hémicycle] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr102b021a1e6dd4041ba78caa09e3c051f';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 6, Bâtiment de l''administration // Rue Vivienne // Côté de l''horloge // Caves : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1a2c5164d98ae4021ac4ab8e9a10dbfa1';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 9, [Étude de dallage pour le nouveau trottoir en hémicycle] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr12febbcae8e4546579bed463a04497045';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 5, [Aile Nord, sous-sol (plan de la partie Est)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1c5e56f6591ce445aa402e576ac3a7eb4';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 11, Bâtiment de l''horloge // Elévation intérieure de la façade sur la cour dans la hauteur du 1.er Etage ; Coupe d''un soffite : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr10e60f24bf3604364b116708d704d241a';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 4, [Aile Nord, sous-sol (plan de la partie Ouest)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr171028a4d7d14423ab724573aa2b72e00';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 15, [Portail sur la rue des Petits-Champs (élévation partielle avec amorce de la grille, élévation latérale et plan)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr16fc0cb9e964f49a1bb12e0eb02c12f79';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 22, [Aile Nord, façade sur la cour (modèle pour les deux ornements à volutes encadrant l''horloge : élévation et coupe verticale)] : [dessin, plan] / H. Labrouste'
WHERE title.id_uuid = 'qr1d2063904ad0d4c299b863650c8d42ddc';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 10, BIBLIOTHEQUE IMPERIALE // Bâtiment sur la rue Vivienne // Travaux divers : Attachement de Maçonnerie N° 5 // M.r Victor Lemaire Entrepreneur : Exercice 1857 // Mons.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1ebaeccb52d5647b7b5ba1a02840b5dc4';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 16, [Portail sur la rue des Petits-Champs (élévation partielle)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr11d3dd067a2cf4d69b56741ca2b15f05e';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 2, [Modèle pour les deux réverbères (élévation)] : [dessin, plan] / H. Lab.'
WHERE title.id_uuid = 'qr13876ea7de44946af87c394b172ed28ce';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (cour)]. 6, Cour de l''administration // dallage : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1b8457233d7094e6683569e370fad639e';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1858)]. 7, BIBLIOTHEQUE [IMPERI]ALE // Travaux divers : A[ttachement] // de Maçonner[ie N° ….] // M.r Victor Lemaire Entrepreneur : Exercice 1858 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1a4f99c695b9449eb8404c17fd80f645c';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 14, [Portail sur la rue des Petits-Champs (élévation avec amorce de la nouvelle grille de part et d''autre)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1a894c023c1f247028a8c10c32535332b';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 9, BIBLIOTHEQUE IMPERIALE // Détail du Perron : Attachement // de Maçonnerie N° [sic] // M.r Victor Lemaire Entrepreneur : Exercice 1855 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1da87e106f46b4984bb24626ce15e6957';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (bâtiments)]. 21, [Aile Nord, façade sur la cour (élévation de la partie supérieure du pavillon dit de l''Horloge, avec détail de la partie droite du fronton et détail du motif couronnant ledit fronton)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1696d8598ebaa4603b5faa9792e99d511';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 10, BIBLIOTHEQUE IMPERIALE // Pavillon Neuf : Attachement // de Maçonnerie N° 14 // M.r V.or Lemaire Entrepreneur : Exercice 1855 // M.r Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr11d4f760352b04c9cbd70b0ff678601fb';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1856)]. 2, BIBLIOTHEQUE IMPERIALE // Pavillon neuf // Détails du Perron : Attachement // de Maçonnerie N° 2 // M.r V.or Lemaire Entrepreneur : Exercice 1856 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr149acf96a4ca74884af0d8a697f8153d5';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 1, BIBLIOTHEQUE IMPERIALE // Travaux en Raccord // Rez-de-Chaussée et Entrsol [sic] : Attachement de Maçonnerie N° 4 // Mr Victor Lemaire Entrepreneur : Exercice 1857 // Mr Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1562ac758e18e40c8b32d5b8fd8678b60';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857 ?)]. 11, BIBLIOTHEQU[E IMPERIALE] // Bâtiment en aile à gauche dans [la cour de l''] Horloge : [Attachement] // de Maçonnerie N° […] // [Monsieur Victor Lemaire] Entrepreneur : Exer[cice …] // Monsieur Labro[uste Architecte] : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr152ed582d1ef34dbda95736081e7e1649';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 12, BIBLIOTHEQUE IMPERIALE // Détail des Façades du Pavillon neuf au 1.er Etage : Attachement de // Maçonnerie N° 18 / Mons.r V.or Lemaire, Entrepreneur : Exercice 1855 // Monsieur Labrouste, Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1a1c55d95eb7c49599c8aa20c32d2b8f3';


UPDATE title
SET entry_name='Motifs historiques d''architecture et de sculpture d''ornement, pour la composition et la décoration extérieure des édifices publics et privés - Choix de fragments empruntés à des monuments français du commencement de la Renaissance à la fin de Louis XVI...volume 2'
WHERE title.id_uuid = 'qr1248137c9a72a4be688f6a0515a608e94';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1855)]. 13, BIBLIOTHEQUE IMPÉRIALE // Pavillon neuf et façade entre ce Pavillon et // celui de l''Horloge : Attachement // de Maçonnerie N° 19 // Mons.r V.or Lemaire, Entrepreneur : Exercice 1855 // Monsieur Labrouste, Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale'
WHERE title.id_uuid = 'qr1ed3fc275a1c6405f8513ba1c2d91abc1';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1856)]. 1, BIBLIOTHEQUE IMPERIALE // Travaux exécutés dans la Cave // à côté du Pavillon Neuf : Attachement // de Maçonnerie N° 1 // M.r V.or Lemaire Entrepreneur : Exercice 1856 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr1711057304df24e03af8334af57057411';


UPDATE title
SET entry_name='Bibliothèque impériale, jardin Vivienne]. 7, Péron [sic] sur le jardin Vivienne : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr135ffbbc2bb514ffdb720ef503c1073cf';


UPDATE title
SET entry_name='[Bibliothèque impériale, hôtel Tubeuf (attachement de maçonnerie, 1857)]. 9, « BIBLIOTHEQUE IMPERIALE // Grands Travaux // Bâtiment en aile sur la rue Vivienne : Attachement // de Maçonnerie N° 2 // M.r Victor Lemaire Entrep.r : Exercice 1857 // Monsieur Labrouste Architecte : [dessin, plan] / [Agence des travaux de la Bibliothèque Impériale]'
WHERE title.id_uuid = 'qr11b9489ae12d64d62a0c646d21bea7b35';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne]. 6, Bibliothèque Impériale // Trottoir sur la rue Vivienne : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1974606bf422f466d93ed0a64cbba7308';


UPDATE title
SET entry_name='[Bibliothèque impériale, jardin Vivienne]. 5, [Projet d''urinoirs publics sur le trottoir, dans l''angle rentrant formé par la rencontre de la grille du jardin et du pavillon Nord-Est de l''hôtel Tubeuf (élévation et plan)] : [dessin, plan] / [Henri Labrouste]'
WHERE title.id_uuid = 'qr1c04fcb2da8334962938fbdfe51b32e35';


UPDATE title
SET entry_name='Lettre et entourage de lettres des magasins du grand Colbert'
WHERE title.id_uuid = 'qr15dc0f2d370404c30b0303ace51d14fe6';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr15ae923725e50433a83d94451b4f1e643';


UPDATE title
SET entry_name='Planche 40 - boutique de Noël pâtissier, passage des Panoramas et 42 rue Neuve Vivienne'
WHERE title.id_uuid = 'qr12914a8ce023a4bf7ad7b6da29d4953de';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr1533cf34723094a8c99f0152f2bb3368f';


UPDATE title
SET entry_name='Planche 46 - boutiques 57 rue Neuve Vivienne'
WHERE title.id_uuid = 'qr180574c4cb85646b8b1ef46ca5d315d9e';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr116f5c740fec34716a3da3026f458744c';


UPDATE title
SET entry_name='Planches 49 et 50 - boutique de Millelot confiseur, passage des Panoramas et 48 rue Neuve Vivienne'
WHERE title.id_uuid = 'qr12d51ff51916749d7bb342d6ad0d48c7a';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr178501b3e314a418c8b7ab60c7fe73327';


UPDATE title
SET entry_name='Planche 57 - boutiques 37 rue Neuve Vivienne'
WHERE title.id_uuid = 'qr1dbee885bf9714919894c380ec3bed2f7';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr1f740b454a5a34d6780b4c926bc03dc42';


UPDATE title
SET entry_name='Planche 61 - galerie Colbert et galerie Vivienne'
WHERE title.id_uuid = 'qr18ecb91002a594cb1a181e9111e2df808';


UPDATE title
SET entry_name='Nouveau recueil de menuiserie et de décorations intérieures et extérieures, comme intérieurs d''appartements, salles de bals et de concerts, foyers de théâtres, intérieurs et extérieurs de boutiques... décorations de fêtes et cérémonie... le tout levé, dessiné et gravé par Thiollet et H. Roux'
WHERE title.id_uuid = 'qr126c75684fc604432894351bae846b2af';


UPDATE title
SET entry_name='Planche 66 - Laudrimont lingerie, 29 place de la Bourse'
WHERE title.id_uuid = 'qr143b8fc2f4f474c4fae02396089db382d';


UPDATE title
SET entry_name='Vieux Paris (1908), Hôtel de la Chancellerie d''Orléans, sur la Rue de Valois (Ier arrt) : façade et enseigne'
WHERE title.id_uuid = 'qr1d1c1fc03b7954c7fb202b9fb6d5712bf';


UPDATE title
SET entry_name='Palais Royal : [galerie, Ier arrt]'
WHERE title.id_uuid = 'qr1bdfa7616ddd74be89649f9ce8ce989b9';


UPDATE title
SET entry_name='Palais Royal (Ier arrt) : entrée sud'
WHERE title.id_uuid = 'qr103abd8e911064c0a888bba770e6278e9';


UPDATE title
SET entry_name='Palais Royal : galerie, Ier arrt'
WHERE title.id_uuid = 'qr11d9d521d29a647cc9980fcebacbde319';


UPDATE title
SET entry_name='Conseil d''Etat, Palais Royal : escalier, détail, Ier arrt'
WHERE title.id_uuid = 'qr10ea175f4a5424b42a3c6e9fbfb60ce9a';


UPDATE title
SET entry_name='Conseil d''Etat, Palais Royal : escalier, détail, Ier arrt'
WHERE title.id_uuid = 'qr173c945354c484abeb343853537d8109d';


UPDATE title
SET entry_name='Conseil d''Etat, Palais Royal : vestibule et escalier, Ier arrt'
WHERE title.id_uuid = 'qr1928d038d54814999b73834715188e362';


UPDATE title
SET entry_name='Conseil d''Etat, Palais Royal : escalier, Ier arrt'
WHERE title.id_uuid = 'qr1cf7c5c8f230f49a3a942cc845672a4a0';


UPDATE title
SET entry_name='Palais Cardinal ou Palais Royal'
WHERE title.id_uuid = 'qr156e8297c4edd4a208fe5d9e904cdb338';


UPDATE title
SET entry_name='Palais Royal du côté du jardin'
WHERE title.id_uuid = 'qr1150bc2a79f744373a24cc1e5e01563e6';


UPDATE title
SET entry_name='Palais Royal du côté du jardin'
WHERE title.id_uuid = 'qr1cd35584715284fec92e98ef2d4f669b5';


UPDATE title
SET entry_name='Veüe du Palais Royal du côté du jardin'
WHERE title.id_uuid = 'qr11b6142fd9fb840278d492c25fcd91dce';


UPDATE title
SET entry_name='Vue exterieure et perspective de la salle préparée par la ville de Paris, pour le festin donné a leurs majestés a l''occasion de la naissance de monseigneur le dauphin le 21 janvier 1782'
WHERE title.id_uuid = 'qr1d66e89658599464daa51b3d076b9f112';


UPDATE title
SET entry_name='Pl. 24 : Palais Royal. Bâtiments sur le jardin. Elévation'
WHERE title.id_uuid = 'qr11ced913de48e4daeac5f9c4b32e7bf76';


UPDATE title
SET entry_name='Pl. 24 : Palais Royal. Bâtiments sur le jardin. Elévation'
WHERE title.id_uuid = 'qr1bf3e3e7af25b41e79892d92101a6c4ae';


UPDATE title
SET entry_name='Motifs historiques d''architecture et de sculpture d''ornement, pour la composition et la décoration extérieure des édifices publics et privés - Choix de fragments empruntés à des monuments français du commencement de la Renaissance à la fin de Louis XVI...volume 2'
WHERE title.id_uuid = 'qr18e82f010d0414b39a6ddd88a73518c00';


UPDATE title
SET entry_name='Pl. 25 : Palais Royal. Bâtiments sur le jardin. Arcades et pilastres'
WHERE title.id_uuid = 'qr1f4dd718fc5314cb38e5eb389e17ba06e';


UPDATE title
SET entry_name='Pl. 26 : Palais Royal. Bâtiments sur le jardin. Croisée du 1er étage'
WHERE title.id_uuid = 'qr12c90b2d0a0804dcabf84caf349874e45';


UPDATE title
SET entry_name='Motifs historiques d''architecture et de sculpture d''ornement, pour la composition et la décoration extérieure des édifices publics et privés - Choix de fragments empruntés à des monuments français du commencement de la Renaissance à la fin de Louis XVI...volume 2'
WHERE title.id_uuid = 'qr1dd55dccd55864388b1e0f3cc5d02b9aa';


UPDATE title
SET entry_name='Pl. 27 : Palais Royal. Bâtiments sur le jardin. Couronnement'
WHERE title.id_uuid = 'qr1cddff1d4facb4a47bfbd212743c394bc';


UPDATE title
SET entry_name='Motifs historiques d''architecture et de sculpture d''ornement, pour la composition et la décoration extérieure des édifices publics et privés - Choix de fragments empruntés à des monuments français du commencement de la Renaissance à la fin de Louis XVI...volume 2'
WHERE title.id_uuid = 'qr1b3aa0fc52fa747c4b1ac7c6ff6d9a38c';


UPDATE title
SET entry_name='Pl. 99 : Candélabre à girondole exécuté pour le Palais Royal'
WHERE title.id_uuid = 'qr198233de2c40e4ff9b10c3270a9cf8d96';


UPDATE title
SET entry_name='Louis-Alexandre Romagnesi, Recueil des dessins représentant les sculptures qui se trouvent dans l''établissement de L.A. Romagnesi, Paris, vers 1825'
WHERE title.id_uuid = 'qr1fe1967df4c2548fbb6ff90edbcf3837b';


UPDATE title
SET entry_name='Pl. 3 Morceaux en grand du jardin du Palais Royal'
WHERE title.id_uuid = 'qr1be5f0d00c8774ed89bbcc931f4276f47';


UPDATE title
SET entry_name='[L’Architecture à la mode où sont les nouveaux dessins pour la décoration des bâtimens et jardins, par les plus habils architectes sculpteurs peintres menuisiers jardiniers serruriers &c. Tome 2]'
WHERE title.id_uuid = 'qr100ee8e1bc85346dab4eb6c8f16f86d79';


UPDATE title
SET entry_name='Pl. U 1 : Escalier en fer exécuté au palais Royal dans les boutiques de la nouvelle galerie vitrée'
WHERE title.id_uuid = 'qr1209b8f19aa6f435e88b6c2d5c072eb22';


UPDATE title
SET entry_name='[Recueil à l''usage des serruriers : recueil factice]'
WHERE title.id_uuid = 'qr13c649afd753649a298969080ae3d50fc';


UPDATE title
SET entry_name='7ème Cahier Pl. 37 - Pavillons-Tente : Urinoir du jardin des Tuileries ; Pavillon du jardin du Palais-Royal'
WHERE title.id_uuid = 'qr15296a65f72964e628e42449e1bff8dca';


UPDATE title
SET entry_name='Recueil de menuiserie et de décorations intérieures et extérieures'
WHERE title.id_uuid = 'qr1919cd61bb56f4d1998a62dc4950de9ef';


UPDATE title
SET entry_name='Au Palais royal. M. Francès, M. Raimond'
WHERE title.id_uuid = 'qr131b3f2b556cb46a78dd515ae5c1bb4f5';


UPDATE title
SET entry_name='70 dessins de Cappiello'
WHERE title.id_uuid = 'qr1da58d1be279b452394492af8d1be73b0';


UPDATE title
SET entry_name='Vue du jardin du Palais Royal prise de la galerie vitrée'
WHERE title.id_uuid = 'qr1d3d3bed1a0ab490493a4bab51d7c3297';


UPDATE title
SET entry_name='OD 11 (5) : Maquette de Monsieur Cappelli, 1869, plafond de la salle du Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr13491e465d81c437ea9cd27f137ae0899';


UPDATE title
SET entry_name='Dessins de décors de théâtres]'
WHERE title.id_uuid = 'qr1effcee3e6d9847ce9ddbe55fb93fec68';


UPDATE title
SET entry_name='Vuë perspective du Palais Royal'
WHERE title.id_uuid = 'qr16e4de7d5a28242e48ecc4a16d1fae763';


UPDATE title
SET entry_name='Histoire de la ville de Paris, composée par D. Michel Félibien, reveue, augmentée et mise au jour par D. Guy-Alexis Lobineau, tous deux prêtres religieux Bénédictins de la Congrégation de Saint Maur, justifiée par des preuves authentiques [...]. Tome second'
WHERE title.id_uuid = 'qr1c8ed862b11d14bb68f848bb17a4395d8';


UPDATE title
SET entry_name='Lettre autographe signée de Horace Vernet à M. de Cailleux, secrétaire général des musées royaux, Rome, 10 janvier 1834'
WHERE title.id_uuid = 'qr1c9b6f39eb79446fd983ca7e504c8f94b';


UPDATE title
SET entry_name='p. 54 Le Palais Royal'
WHERE title.id_uuid = 'qr17c43cae132f34df19105ac65766d0453';


UPDATE title
SET entry_name='Les Curiositez de Paris, de Versailles, de Marly, de Vincennes, de Saint Cloud, et des environs : avec les adresses pour trouver facilement tout ce qu''ils renferment d''agréable et d''utile, ouvrage enrichi d''un grand nombre de figures'
WHERE title.id_uuid = 'qr1650315b57fc54fb6b96a74393cf6022d';


UPDATE title
SET entry_name='P. 314 Le Palais Royal'
WHERE title.id_uuid = 'qr19ff47babc485440b8fe6b6c3bd9aafbf';


UPDATE title
SET entry_name='Description historique de la ville de Paris et de ses environs par feu M. Piganiol de la Force : Tome second'
WHERE title.id_uuid = 'qr1b53ee2ea5bf14b719ec412d80a18014a';


UPDATE title
SET entry_name='n.p. Plan du quartier du Palais Royal'
WHERE title.id_uuid = 'qr1d5d18064aaab487fb1860632db542147';


UPDATE title
SET entry_name='Recherches critiques, historiques et topographiques sur la ville de Paris, depuis ses commencens connus jusqu''à présent, avec le plan de chaque quartier. Tome premier, contenant les quartiers suivans : La Cité, Saint Jacques de la Boucherie, Sainte-Opportune, Le Louvre, Le Palais-Royal'
WHERE title.id_uuid = 'qr1bb7db0a035394cf6a400521255dc1055';


UPDATE title
SET entry_name='Tome 1, p. 155 Le Palais Royal en vue d''oiseau'
WHERE title.id_uuid = 'qr1ddeb260117f44bb3b85a61e93774f6d7';


UPDATE title
SET entry_name='Curiosités de Paris, de Versailles, Marly, Vincennes, Saint-Cloud, et des environs. Tome premier ; Nouvelle édition, augmentée de la description de tous les monuments, édifices et autres curiosités, avec les changemens qui ont été faits depuis la dernière édition...'
WHERE title.id_uuid = 'qr127d8e7f2ac02468e9fe0d347a1936ffc';


UPDATE title
SET entry_name='n.p. 1er projet d''une porte pour un des vestibules du Palais Royal au rez de chaussée en face du grand escalier'
WHERE title.id_uuid = 'qr1c57dfb1018c04f92ad2ba078e6e51cae';


UPDATE title
SET entry_name='Les Œuvres d''architecture de Pierre Contant d''Ivry, architecte du Roi. Première partie'
WHERE title.id_uuid = 'qr12d5fd045c3b34f58a2bb3bd36771ad68';


UPDATE title
SET entry_name='n.p. Projet d''une porte pour un des vestibules du Palais Royal à Paris 1766 ; 2ème projet d''une fontaine publique proposé pour le faubourg St Germain à Paris'
WHERE title.id_uuid = 'qr11437b0c814bb4b79b3306248ca46fd89';


UPDATE title
SET entry_name='Les Œuvres d''architecture de Pierre Contant d''Ivry, architecte du Roi. Première partie'
WHERE title.id_uuid = 'qr1d1546015ee2042aea3149a052b49d860';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr14c67f71868694e549cc8d6f7894ad798';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr16fd9b7b6766548aaa5e66a4eadd4c577';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1ff8858dcebe2426faef433f11735d834';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr134b1434408fa46c2bcbf910adcf35970';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr12d9ff0dee5b54561a5bb76f927f273a7';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr1c6fbec6797c048a193faaf6ed361820b';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1b04a42f0e62b42bea59fab37e6989c6c';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr10edc002e38824722bae2d0f4505b1abf';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1dec458a775514e83b9388c646ec602cb';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr10ba5c4ae63b949779d87cbe0acf77eb1';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1e788cc19189f4c11a0896028b3eae2b9';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr16442b138a25b4ade8aa30a1e93357763';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr19a3b05c1e47e4df4bea8df6c6e15ec1d';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1c7fd2ebb557c45c5a89af9382edf148e';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr1c45544013c0541d695becf6bc975ee4d';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1779c2232652843b6889c19dd64483597';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr15a9a2f65034d4670b687060794331f1d';


UPDATE title
SET entry_name='Dessins d''Antoine Coypel, décoration de la galerie du Palais-Royal et de la Chancellerie d''Orléans'
WHERE title.id_uuid = 'qr1995a9cb82e2647a08ff3bc367710a24d';


UPDATE title
SET entry_name='Inventaire général des dessins du Musée du Louvre et du Musée de Versailles : Ecole française.Tome IV : [Corot-Delacroix]'
WHERE title.id_uuid = 'qr148c4386655824ecbab7c1d298b56a2c8';


UPDATE title
SET entry_name='N° 1201. Costume de Mlle Déjazet, rôle de Mlle Dangeville (costume de Jacquot), dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1cc7e900a986d42b7b36d49e97c95039a';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr11a206c26fa0948a794f2b4e8f500bd32';


UPDATE title
SET entry_name='N° 1202. Costume de Mlle Déjazet, rôle de Mlle Dangeville (Mse de Nesles), dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr17639e3600d9c4dec9a9fc87495ad4a2e';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr171a493763cc54481b3c888040f898c28';


UPDATE title
SET entry_name='N° 1203. Costume de Mlle Déjazet, rôle de Mlle Dangeville (Tchingka), dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1997f2a1af668442b84df3b299da16362';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1dd8e0f136bdd40ca970ec289c569b1a6';


UPDATE title
SET entry_name='N° 1206. Costume de Mlle Déjazet, rôle d''Emmanuel, dans les deux Pigeons : vaudeville, Théâtre du Palais-Royal (Acte IV)'
WHERE title.id_uuid = 'qr155f5a385f1594e299bc8dbb8df5547aa';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr148e1fbd70e6e45ceb822715f4aca0db0';


UPDATE title
SET entry_name='N° 1207. Costume de Mlle Déjazet, rôle d''Emmanuel, dans les deux Pigeons : vaudeville, Théâtre du Palais-Royal (Acte III)'
WHERE title.id_uuid = 'qr1945d03c2a95a43e8bbfc4ffaffb73181';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1b8639230965843c7936fd9cfed0c4ea2';


UPDATE title
SET entry_name='N° 1264. Costume de Mlle Déjazet, rôle de la Gitana, dans Rothomago : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr17c99a9c4f821402b91f74b8724d55c69';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr131e09fe7c77948039da24527e0a41b4a';


UPDATE title
SET entry_name='N° 1286. Costume de Mlle Déjazet, rôle de Nanon, dans Nanon, Ninon et Maintenon: vaudeville, Théâtre du Palais-Royal (Acte Ier, 1er costume)'
WHERE title.id_uuid = 'qr199c2b9351986473987d67a2ab47475de';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr12dd8b0c4689b4bdb8891fb71cf64e125';


UPDATE title
SET entry_name='N° 1287. Costume de Mlle Déjazet, rôle de Nanon, dans Nanon, Ninon et Maintenon: vaudeville, Théâtre du Palais-Royal (Actes I, II, III)'
WHERE title.id_uuid = 'qr12c52e386e85241f4910c10b88567c705';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1fcc87e00324a4a88b5e7f61e961a84fa';


UPDATE title
SET entry_name='N° 1301. Costumes de Signor Camprubi, et de la Signora Dolores, Théâtre du Palais-Royal (Pas Syrien)'
WHERE title.id_uuid = 'qr14b4eb319582f43c5ab4d27980cdf0a88';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr127b80c375fbe44b497d937505b0584e1';


UPDATE title
SET entry_name='N° 1306. Costume de Mme Grassot, rôle de Gabrielle, dans Gabrielle ou les Aides-de-Camp : vaudeville, Théâtre du Palais-Royal (Acte Ier)'
WHERE title.id_uuid = 'qr1cb47ce5f31984238a7bfce9ec9cc9d76';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr13dcc375d52114eb0bbb475964575be27';


UPDATE title
SET entry_name='N° 1307. Costume de Mme Grassot, rôle de Gabrielle, dans Gabrielle ou les Aides-de-Camp : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr12c63268b2b114d6cb2e1419909a991f8';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1d574b19577044e2e86e2835eefb8e97a';


UPDATE title
SET entry_name='N° 1315. Costume d''Alcide Tousez, rôle de Bel-Avoir, dans les Trois Quenouilles : féerie, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1670f7d7ede794314a23d4814b3737d08';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr15ac2ef17d4e04f0f981116e5e1dff1c9';


UPDATE title
SET entry_name='N° 1316. Costume de Mlle Mary, rôle de Zerbine, dans les Trois Quenouilles : féerie, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1056d263d83224f578b73db61ea41ebba';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr17fa060de644e410ab00cfbdd51e62d27';


UPDATE title
SET entry_name='N° 1326. Costume de Mme Leménil, rôle de Manon-Giroux, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Actes I et II)'
WHERE title.id_uuid = 'qr17e0ee1da04be4812ad37389dd65c8e71';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1d0ff43ee38d3468abab83c7f662a3cf2';


UPDATE title
SET entry_name='N° 1327. Costume de Mme Leménil, rôle de Mina, dans Pascal et Chambord : vaudeville, Théâtre du Palais Royal (Acte II)'
WHERE title.id_uuid = 'qr1ea4a1feb4b6f4d088585129d10cd33fe';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr11881e9a0f8054769a077a2c47024486e';


UPDATE title
SET entry_name='N° 1333. Costume de Mlle Déjazet, rôle d''Argentine, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr164489d4cfe0d4240a9d85542e15bedab';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr16596332086b949149a1e230e15df1979';


UPDATE title
SET entry_name='N° 1334. Costume de Mlle Déjazet, rôle de Flora, dans Argentine : vaudeville, Théâtre du Palais-Royal (Acte Ier)'
WHERE title.id_uuid = 'qr1e71e80a59c6940299178217326dc7003';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1e3466e72797a40558ec49bd92553915a';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1f139a7d79336452da05b505cccb8e1ea';


UPDATE title
SET entry_name='N° 1344. Costume d''Achard, rôle du Torréador, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr10cb85e4cbd964225b56eb1894a531cc7';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1a60d848117ef45dca7e967d4ca9bc77f';


UPDATE title
SET entry_name='N° 1345. Costume de Mme Leménil, rôle de Hyacinthe, dans le Torréador : vaudeville, Théâtre du Palais-Royal (Acte Ier)'
WHERE title.id_uuid = 'qr160c388bd26404a3995c17dd0c290fae7';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr19130b6d8ce8e48d5a4ad0bf2d50413f0';


UPDATE title
SET entry_name='N° 1346. Costume de Mmes Leménil et Pernon, rôles de Hyacinthe et de Dona Maria, dans le Torréador : vaudeville, Théâtre du Palais-Royal (Actes II et III)'
WHERE title.id_uuid = 'qr1353fc04a68af40fd8650ffff0d5f3694';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1a4d9d21ef2fe4477a4685051ade8cdfe';


UPDATE title
SET entry_name='N° 1354. Costume de Mlle Déjazet, rôle de Richelieu, dans les Premières Armes de Richelieu : vaudeville, Théâtre du Palais Royal (Acte Ier)'
WHERE title.id_uuid = 'qr10fd75f1ff144467ebf1430d386a3db9e';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr115a069d186dc4aeeb259b3330b7ed226';


UPDATE title
SET entry_name='N° 1355. Costume de Mlle Déjazet, rôle de Richelieu, dans les Premières Armes de Richelieu : vaudeville, Théâtre du Palais Royal (Acte II)'
WHERE title.id_uuid = 'qr115e3c3a9343a47bdaa61cf577c527333';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr19419b473c6f240c59960a9899dbc71bc';


UPDATE title
SET entry_name='N° 1365. Costume de Mme Dupuis, rôle de Charlotte, dans les Avoués en Vacances : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1ac50d8c3acd7409ebb9c54f97cd68bb5';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr19e40af8555c340b68c3f352100e40b84';


UPDATE title
SET entry_name='N° 1398. Costume de Mlle Déjazet, rôle d''Indiana, dans Indiana et Charlemagne : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr137aa83c82b8a47cba55c6eacb248d006';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VII'
WHERE title.id_uuid = 'qr1d7c05abaf5854c959ee2690b3a25163e';


UPDATE title
SET entry_name='N° 1401. Costume de Mme Grassot, rôle de Mme de Pompadour, dans la Journée aux Eventails : vaudeville, Théâtre du Palais Royal (Acte Ier)'
WHERE title.id_uuid = 'qr1d244704dd9f3499192b84d664457df56';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1f28e513e65aa472cadf77144e143eb5d';


UPDATE title
SET entry_name='N° 1402. Costume de Mme Grassot, rôle de Mme de Pompadour, dans la Journée aux Eventails : vaudeville, Théâtre du Palais Royal (Acte II)'
WHERE title.id_uuid = 'qr11163aeb95de74bdc878b29c6a4b1162d';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1d6dc37ff6f9549a494212c7c636df40d';


UPDATE title
SET entry_name='N° 1403. Costume de Mme Dupuis, rôle de Caquet, dans Cocorico : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr195510785d93f4cd08626797676c3a413';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1f2cf01787f934fe789fe957759cc7961';


UPDATE title
SET entry_name='N° 1408. Cosstume de Mme Grassot, rôle d''Alizon, dans Bob : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr1faa5a66013194a0c934dc68c74cbe561';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr181fdfdc4eaa745aa8cc76cc867d383d0';


UPDATE title
SET entry_name='N° 1426. Costume de Derval, rôle du Chevalier Thiais, dans Trianon : vaudeville, Théâtre du Palais-Royal (Acte Ier, en meunier)'
WHERE title.id_uuid = 'qr19834dc8091c147f5a37223c305dca3a8';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1576e0c37b4fc4e2188dc8d0a060d1a54';


UPDATE title
SET entry_name='N° 1427. Costume de Mlle Pernon, rôle de la Marquise, dans Trianon : vaudeville, Théâtre du Palais-Royal (Acte Ier, en bergère)'
WHERE title.id_uuid = 'qr1fef43f8440234851a7ae5774808629f7';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr185cc96ee81314119b9a2938a0bb8e6ca';


UPDATE title
SET entry_name='N° 1460. Costume de Mlle Dupuis, rôle de Nicole, dans la Permission de dix heures : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr13dd6157f0f104348b5cb19fdc1e6c9b9';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr12c9cb7796d1842b88cfe97b520abfde3';


UPDATE title
SET entry_name='N° 1461. Costume d''Achard, rôle de la Rose-Pompon, dans la Permission de dix heures : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr136735042f59a4dba8610a72f4900f079';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1e8783b17fa954c4f8504cac66b03ba30';


UPDATE title
SET entry_name='N° 1472. Costume de Mlle Déjazet, rôle de Mlle Sallé, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr15a8a6565dcf94b84b1f47100dbaa7fce';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr13c644cf06fc44fe8ae8500829b620896';


UPDATE title
SET entry_name='N° 1480. Costume de Mlle Pernon, rôle de Mme Rabourdin, dans Lucrece : vaudeville, Théâtre du Palais-Royal (Acte II, en bohémienne)'
WHERE title.id_uuid = 'qr1da7e53c8d92e4802848d7df20df092e0';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr107c07e3eeecf4113b891a0964af43103';


UPDATE title
SET entry_name='N° 1494. Costume de Mlle Dupuis, rôle de Mina : vaudeville, Théâtre du Palais Royal (2e costume)'
WHERE title.id_uuid = 'qr18685befd083f469794f77c49204effaf';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1fc7d3b33368241a6b8672f212886e565';


UPDATE title
SET entry_name='N° 1507. Costume de Mlle Déjazet, rôle du Vicomte de Létorières, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Acte Ier)'
WHERE title.id_uuid = 'qr131743048dd2d47589d0d8f5036636382';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr109b8fc2981784c8e97023ec8b98b20a6';


UPDATE title
SET entry_name='N° 1508. Costume de Mlle Déjazet, rôle du Vicomte de Létorières, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Actes 2 et 5)'
WHERE title.id_uuid = 'qr17c591aded4ee4c509091e639897bdd48';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1fb33d79ac37d45b79492701aeb106b05';


UPDATE title
SET entry_name='AD075PH_UPF1307'
WHERE title.id_uuid = 'qr1684e7d11661849e697cfc7dbd09df7b4';


UPDATE title
SET entry_name='N° 1527. Costume de Mlle Dupuis, rôle de Francisquine, dans Tabarin : vaudeville, Théâtre du Palais-Royal (Actes I et III)'
WHERE title.id_uuid = 'qr15d3eedb412fa4ae2965eec1604b757ff';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1bf39a114afec4035bc8b4d639f4a0c31';


UPDATE title
SET entry_name='N° 1547. Costume de Derval, rôle de Georges, dans les deux Couronnes : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1634f747c91d84f07aa097a616241b186';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr118a7647d42ee4dd8a061c8fe7de79bd1';


UPDATE title
SET entry_name='N° 1566. Costume de Mlle Fargueil, rôle de Catherine II, dans la Dragonne : vaudeville, Théâtre du Palais-Royal (Actes I et II)'
WHERE title.id_uuid = 'qr15e3cc1b8676d4af99bebda829e3a0023';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1f6c62b893e2a4089845190f1e8959f01';


UPDATE title
SET entry_name='N° 1572. Costume de Mlle Déjazet, rôle de Charlotte Clapier, dans le Capitaine Charlotte : vaudeville, Théâtre du Palais-Royal (Acte Ier)'
WHERE title.id_uuid = 'qr131939acf0d444de5b11a61dd824a395b';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr17966821a730f42d8b9e02b5d800cf3d4';


UPDATE title
SET entry_name='N° 1573. Costume de Mlle Déjazet, rôle de Charlotte Clapier, dans le Capitaine Charlotte : vaudeville, Théâtre du Palais-Royal (Actes I et II)'
WHERE title.id_uuid = 'qr1f15b108bc6a24475a17ee2a4fa3705e3';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr181b707d3ea6b49d287154c7d11fba0ca';


UPDATE title
SET entry_name='N° 1635. Costume de Mlle Déjazet, rôle de Carlo, dans Carlo et Carlin, Théâtre du Palais-Royal (1er costume)'
WHERE title.id_uuid = 'qr125002f9b1e4f4325b839b62fea904491';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VIII'
WHERE title.id_uuid = 'qr1f39d869d3eaf4a52a2542e49d65b404d';


UPDATE title
SET entry_name='N° 1012. Costume de Mme Dupuis, rôle de Léona dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1a3410c6c734d404db8977969e0adc857';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr147987ea3c1b244ec978aa92a45b141d6';


UPDATE title
SET entry_name='N° 1013. Costume de Mlle Augustine, rôle de Sanchette, dans Léona : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1c5d7847855264926ab7c46d8864f61ab';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1872f8ad807234484bc87c505d9cddd6a';


UPDATE title
SET entry_name='N° 1018. Costume de Mlle Dejazet, rôle de Gaudriole, dans les Chansons de Désaugiers : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1ef8d0fcd326a441aae6049fe675f77ff';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr19fd802b3f20e4db1a1fd3622845a2f0b';


UPDATE title
SET entry_name='N° 1045. Costume de Mme Dupuis, rôle de Louison, dans la Marquise de Prétintaille : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1b77aa35fff39447980f6bcd2039db2a1';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr18485b5bb28c048519ccf9c60e601e15f';


UPDATE title
SET entry_name='N° 1046. Costume de Mme Dejazet, rôle de la Marquise de Prétintaille, dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr10f0f2f5f797c4f37a712756412a24cad';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1098d6116bc5e4ba1bac433dd0bd32058';


UPDATE title
SET entry_name='N° 1061. Costume de Mlle Déjazet, rôle d''Arthur, dans l''Oiseau Bleu : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr17bee3ccd91864e61bd465822d693496f';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr133e253d32e6c46d2930a8f747c0a6d58';


UPDATE title
SET entry_name='N° 1086. Costume de Mlle Déjazet, rôle de Cadet Buteux, dans les Chansons de Desaugiers : vaudeville, Théâtre du Palais-Royal (Acte 1er)'
WHERE title.id_uuid = 'qr15fad99fe80f141fe91a97528b69fc17f';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1482cb1b91ed245e7880f862cce1e7992';


UPDATE title
SET entry_name='N° 1090. Costume de Mlle Déjazet, rôle de la Chercheuse d''Esprit, dans Madame Favart : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr19d5e108e870649aba8ba806195098706';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1e5603e42e06a49ea94de69f94e4949ad';


UPDATE title
SET entry_name='N° 1094. Costume de Mlle Déjazet, rôle de Mme Favart dans la pièce de ce nom : vaudeville, Théâtre du Palais-Royal (Acte III)'
WHERE title.id_uuid = 'qr14f5985516a6f4e289f94b1dbe257dbd0';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1d6308f8dbb4d48d7b275bac8be138655';


UPDATE title
SET entry_name='N° 1112. Costume de Mlle Déjazet, rôle de Javotte, dans la Comtesse du Tonneau : vaudeville, Théâtre du Palais-Royal (Acte 1er)'
WHERE title.id_uuid = 'qr14fec57d551bf4d9a876525ab905c1bdd';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1194b68503c694154bb249659f2245f5b';


UPDATE title
SET entry_name='N° 1113. Costume de Mlle Déjazet, rôle de Javotte (en Circassienne), dans la Comtesse du Tonneau : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr11cb4486486164e7985e7489acaeea570';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr14cb8ca575d534b8eb232334c1d031685';


UPDATE title
SET entry_name='N° 1122. Costume d''Alcide Tousez, rôle de de Bobêche, dans Bobêche et Galimafré : vaudeville, Théâtre du Palais-Royal (Acte II)'
WHERE title.id_uuid = 'qr19685309ecaf9414b9f97e131638a887b';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr132ffae15d97c4e8eafe25c46d79f9a42';


UPDATE title
SET entry_name='N° 1123. Costume de Lemenil, rôle de Galimafré, dans Bobêche et Galimafré : vaudeville, Théâtre du Palais-Royal (Actes II et III)'
WHERE title.id_uuid = 'qr17fc91de888a34eff9ec9a276ae9e9cdc';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1ad3eb9aa57db4325813d65ce957a1643';


UPDATE title
SET entry_name='N° 1148. Costume de Mlle Dupuis, rôle de Thérèse dans le Mémoire d''une Blanchisseuse : vaudeville, Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr123f78d6ec13740c59e6f20cf63bcd82d';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume VI'
WHERE title.id_uuid = 'qr1f8818bcb0a1846639cf12822216a8fb8';


UPDATE title
SET entry_name='Café de Rohan - 1 place du Palais Royal, Guerre mondiale (1914-1918)'
WHERE title.id_uuid = 'qr17c1a8bc00b75462698ada30e8aa2d858';


UPDATE title
SET entry_name='N° 810. Costume de Melle Dejazet, rôle de Sophie Arnould dans la pièce de ce nom : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1bed735d31bce48ea8a292825553716b5';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr13bf3d279826a4a85b77bd511ffe6c776';


UPDATE title
SET entry_name='N° 846. Costume de Mme Paul, rôle de Rébecca, dans la Révolte des Femmes : vaudeville, Théâtre du Palais Royal (acte II)'
WHERE title.id_uuid = 'qr135b533cbb6304a2aae0ec075f09e6c3a';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr1d3a1c02cc1094bbab5c37e5412525a38';


UPDATE title
SET entry_name='N° 886. Costume de Melle Déjazet, rôle de Charles dans le Triolet Bleu : vaudeville, Théâtre du Palais Royal (tableaux I, II et III)'
WHERE title.id_uuid = 'qr103613141ad6e484290ee2a6aab5ef207';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr147e705df989d4584874aec6bb42fd714';


UPDATE title
SET entry_name='N° 901. Costume de Melle Déjazet, rôle de Thérésina dans Judith et Holopherne : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr19bef724a676f4dcd9a36167e0b619045';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr10b1449d1b262444b95068005118a6f40';


UPDATE title
SET entry_name='N° 902.Costume de Melle Déjazet, rôle de Thérésina dans Judith et Holopherne : vaudeville, Théâtre du Palais Royal (acte II)'
WHERE title.id_uuid = 'qr177163970cd7745678b1d2906ea4a941e';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr198385c24c9044e209e104232e492dc21';


UPDATE title
SET entry_name='N° 903. Costume d''Alcide Touzez, rôle de Merinos dans Judith et Holopherne : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr16b392de740394861918d9aab3fc9c0c8';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr168ac786cafce4916800696eb61bb6586';


UPDATE title
SET entry_name='N° 921. Costume de Sainville, rôle d''un Charlatan dans Les deux Borgnes : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1743205c01931496ba8caf0c12351a9d8';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr1d795ac92c93142f9884443d5a76d2b43';


UPDATE title
SET entry_name='N° 964. Costume de Melle Déjazet, rôle de Christine, dans la Croix d''Or : vaudeville, Théâtre du Palais Royal (acte Ier)'
WHERE title.id_uuid = 'qr1706679e719974e8c80543a8fe06e3b05';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr16d305a1a510c44b49b96faedd2eefec9';


UPDATE title
SET entry_name='N° 968. Costume de Melle Mary, rôle de Floretta, dans les Marais-Pontins : vaudeville, Théâtre du Palais Royal (acte Ier)'
WHERE title.id_uuid = 'qr15c9218a1836f494092bd60b017a61655';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr11090fcc1e18640c6a1e32d95c52178a1';


UPDATE title
SET entry_name='N° 999. Costume de Melle Déjazet, rôle de Périchole dans la pièce de ce nom : vaudeville, Théâtre du Palais Royal (second costume, en Vierge du Soleil)'
WHERE title.id_uuid = 'qr1ef1e43c7b72643b78edceea3cfeb3660';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr154f049d36fa94aa8986ba3264b88a69e';


UPDATE title
SET entry_name='N° 1000. Costume de Melle Déjazet, rôle de Périchole dans la pièce de ce nom : vaudeville, Théâtre du Palais Royale (Ier costume)'
WHERE title.id_uuid = 'qr1440271eb1bf54a93b795c96da97438dd';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume V'
WHERE title.id_uuid = 'qr1c783dd7f76ad4e55a15cf1544499b05f';


UPDATE title
SET entry_name='N° 722. Costume de Mlle Déjazet, rôle du Duc d''Orléans, dans l''enfance de Louis XII : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr164190b50e53441d6b359ba13c66d9f59';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume IV'
WHERE title.id_uuid = 'qr17afd8bb5c3624638805b77e4919d1693';


UPDATE title
SET entry_name='N° 723. Costume de Gaston, rôle de Leslie, jeune archer écossais dans l''Enfance de Louis XII : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1b18b0a0ef7e045b28357382ede6d9e01';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume IV'
WHERE title.id_uuid = 'qr1720b96c500f84879bbf7c4bec8fa47e0';


UPDATE title
SET entry_name='N° 778. Costume de Mlle Delisle, rôle de Catherine dans Santeul ou le Chamoine au cabaret : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr12aea2ea015ea494c84f686aab3ab118f';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume IV'
WHERE title.id_uuid = 'qr140f09479fc414c81bd5e3c13a54e653a';


UPDATE title
SET entry_name='N° 793. Costume de Paul rôle du Prince d''Ornain, dans Sophie Arnould : vaudeville, Théâtre du Palais Royal (acte II)'
WHERE title.id_uuid = 'qr1cb231a54eea14730aaa9287d81b75df3';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume IV'
WHERE title.id_uuid = 'qr1c8e5509e821c4fee80f61b233154a79e';


UPDATE title
SET entry_name='N° 800. Costume de Mlle Déjazet rôle de la Liberté, dans les Chansons de Beranger : vaudeville, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr10e83d62c5cda4414a375f1743bf14f2c';


UPDATE title
SET entry_name='Petite galerie dramatique, ou Recueil des différents costumes d''acteurs des théâtres de la capitale. Volume IV'
WHERE title.id_uuid = 'qr17a52f73cd85749eabb35f8f0f91f45a3';


UPDATE title
SET entry_name='Pl. 7 [Réunion et achèvement des Tuileries, du Louvre et du Palais royal (1831)]'
WHERE title.id_uuid = 'qr1ecd0dc84229044ac832d29ba9856a4f9';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr1253c5ada4d8f4708af0a7c35d098f329';


UPDATE title
SET entry_name='Pl. 33 [Plan général du Palais royal et dépendances : rez-de-chaussée (1679)]'
WHERE title.id_uuid = 'qr145d616abf5114e88a42a733d54ccb6cd';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr180857232f87840bf9a563cc5e7fe52b8';


UPDATE title
SET entry_name='Pl. 34 [Plan général du Palais royal et dépendances : rez-de-chaussée (1748)]'
WHERE title.id_uuid = 'qr10d5fe6584fde4ed6ba1c063dfedce4a3';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr10509aa0e091543e89cf5f068c9d1ef46';


UPDATE title
SET entry_name='Pl. 35 [Plan général du Palais royal et dépendances : rez-de-chaussée (1780)]'
WHERE title.id_uuid = 'qr161d31b453ff74b8c904c35931acf1128';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr1777c4066e9564393b3f0a82721b0085f';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1c06d9aa3b6d44a0bad90360ccdb3460c';


UPDATE title
SET entry_name='Pl. 36 [Plan général des batimens et jardin du Palais royal avec les augmentations projetées par M. Louis architecte (1781)]'
WHERE title.id_uuid = 'qr1b707fbcfd7c5451d83bed521694b0b9c';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr10316c36c87444a4abf434cc45fa1b664';


UPDATE title
SET entry_name='Pl. 37 [Plan général du rez-de-chaussée du Palais royal par M. Louis architecte (1784)]'
WHERE title.id_uuid = 'qr1011d6d80064d4e2d88819b3497a0a99b';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr1cf43eb9be3834bde8bc64d58f1597699';


UPDATE title
SET entry_name='Pl. 38 [Plan du Palais royal : rez-de-chaussée (1829)]'
WHERE title.id_uuid = 'qr11fa9e04fb47f49acaec5d5ab6ac59acd';


UPDATE title
SET entry_name='Plans de plusieurs châteaux, palais et résidences de souverains de France, d''Italie, d''Espagne et de Russie, A.P.P.P.L.P.D.R.D.R. [au plan projeté pour le Palais du roi de Rome...'
WHERE title.id_uuid = 'qr16acbbb0320634cf7b729f1e29e8ebbce';


UPDATE title
SET entry_name='Immeuble 12 rue de Valois'
WHERE title.id_uuid = 'qr1568fbee354774846a581db319104cdb4';


UPDATE title
SET entry_name='AD075PH_UPF1305.JPG'
WHERE title.id_uuid = 'qr14c2a7ce87c25459390d4f1ce05ae8e7f';


UPDATE title
SET entry_name='Immeuble 14 rue de Valois'
WHERE title.id_uuid = 'qr1a3db8d0b38be49bfbe343a5727902aad';


UPDATE title
SET entry_name='AD075PH_UPF1306.JPG'
WHERE title.id_uuid = 'qr12eb5dc0bb92d45b8b741230242e13f68';


UPDATE title
SET entry_name='Immeuble 16 rue de Valois'
WHERE title.id_uuid = 'qr10e22d799ae3046d190967b7abd81079b';


UPDATE title
SET entry_name='AD075PH_UPF1307.JPG'
WHERE title.id_uuid = 'qr16fdd495f54fa41f48e6ae29939f16ae5';


UPDATE title
SET entry_name='Immeuble 18 rue de Valois'
WHERE title.id_uuid = 'qr1a30e83a428c7436496642b42f21a058f';


UPDATE title
SET entry_name='AD075PH_UPF1308.JPG'
WHERE title.id_uuid = 'qr1d7a139d69aa44b6984e4cbbadad1da9b';


UPDATE title
SET entry_name='Immeuble 20 rue de Valois'
WHERE title.id_uuid = 'qr1d3e439cc7f214de5908051bbbd619442';


UPDATE title
SET entry_name='AD075PH_UPF1309.JPG'
WHERE title.id_uuid = 'qr1babf1c00472d45ce9562dbaa4f06c851';


UPDATE title
SET entry_name='Eclairage, Cour des Fontaines'
WHERE title.id_uuid = 'qr1b8760ff901cf4dda8ac3afe362aeca17';


UPDATE title
SET entry_name='AD075PH_00147'
WHERE title.id_uuid = 'qr1fcb18df1959f4ff49ae8128889ec875a';


UPDATE title
SET entry_name='Eclairage, Cour des Fontaines'
WHERE title.id_uuid = 'qr1eb7b2ea3f91c4d6fb6a92c768e511ffa';


UPDATE title
SET entry_name='AD075PH_00247'
WHERE title.id_uuid = 'qr1b0022c14463948d68153bc17449ff2a1';


UPDATE title
SET entry_name='Grand Restaurant Bouvier - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr119ed9fc133ae441889225f832a53ca7f';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1e140423318ff4512aa9efc3ecb0b3418';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr17859c79adcf945079b1b59c6ca5a78ae';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1b2ce7cc1ed824e3da9220976a12c79f8';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr169d6eb77f2c843019d552aa791f34957';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1cb3a3473ebf34b7988a59f573e0fdf26';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr170068781135347bf89d5445cf8aaa3e1';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1c5398beab0044b5facaa880aad692503';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1bd07b4bf1863401b9587d506e56830b8';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr19e195429b3a1420fb77cbf7aca808e8b';


UPDATE title
SET entry_name='Grand Restaurant E. Vidrequin - 40-41 galerie de Montpensier'
WHERE title.id_uuid = 'qr1d1a716dbaf7d466bab16555fe4529350';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr11ed9e33bcef74ae39353048101f098f1';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr1758c9b7e5da04710a1c74c978cfc2c63';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr19160f74b68a1492a94f7a4513b2ba6a3';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr1a6fbdc56c5f94b35a36a84183552e3d2';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr1e101820c1ea74581bd8eb78d1a32bb43';


UPDATE title
SET entry_name='Aux 5 Arcades - 65 galerie de Montpensier'
WHERE title.id_uuid = 'qr1ec6c653369e648aeae95b08bcef466f2';


UPDATE title
SET entry_name='Grand Restaurant Montpensier - 7 rue de Montpensier'
WHERE title.id_uuid = 'qr19020256e773c486b9a103f93251d176a';


UPDATE title
SET entry_name='Grand Restaurant Montpensier - 7 rue de Montpensier'
WHERE title.id_uuid = 'qr124580f5cdd2a4769a36378b6a3e84549';


UPDATE title
SET entry_name='Grand Restaurant Montpensier - 7 rue de Montpensier'
WHERE title.id_uuid = 'qr1438f37586c4849908ac19f7e6eacfa13';


UPDATE title
SET entry_name='Grand Restaurant Montpensier - 7 rue de Montpensier'
WHERE title.id_uuid = 'qr1726f2ec8c2d143aea97b135367a057d4';


UPDATE title
SET entry_name='Buffet-Bar américain du Théâtre du Palais-Royal - 38 rue de Montpensier'
WHERE title.id_uuid = 'qr1d5f81808e5a0468f9864049c14a11e45';


UPDATE title
SET entry_name='Café-Bar du Palais-Royal - 47 rue de Montpensier'
WHERE title.id_uuid = 'qr1eb5070adfde4494fbbae1d2fd8984ee3';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr138fc37bd2138450980337dd6c4a8a25b';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr11fa3ee1aba504c3aa0841f81cfcdfbf8';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr1dacf6616b65d4ec3a48ff9fe28cab859';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr124bf3637957949dbbad19c11a4ee9280';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr19e3e1444976e409eab4a0144fe1a0950';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr170ce649878734f758dc96d880e6c58d8';


UPDATE title
SET entry_name='Salon Corazza, Café-Restaurant Corazza - 9-12 rue et galerie de Montpensier (Palais Royal)'
WHERE title.id_uuid = 'qr1a47d9ab018c543aabce730d6486dd626';


UPDATE title
SET entry_name='Chevet Palais-Royal - Palais Royal, Menu officiel de la Ville de Paris.'
WHERE title.id_uuid = 'qr188e4086b18be412191b94d9aec167261';


UPDATE title
SET entry_name='La Rotonde - Jardin du Palais Royal'
WHERE title.id_uuid = 'qr11d095e6e24b0481690fb13833c8f1987';


UPDATE title
SET entry_name='La Rotonde - Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1ddb9ee99adcc45c8bcde166c529f9003';


UPDATE title
SET entry_name='La Rotonde - Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1b0796f1e8ad649879868c4394fc79940';


UPDATE title
SET entry_name='La Rotonde - Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1b6bdf74deef74eb8bec4ad33ae2ea46f';


UPDATE title
SET entry_name='La Rotonde - Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1d21821208c64428db7b63b9047650243';


UPDATE title
SET entry_name='Café de Rohan - 1 place du Palais Royal, Guerre mondiale (1914-1918)'
WHERE title.id_uuid = 'qr1022ef477ec5740479e3419ad26f8053c';


UPDATE title
SET entry_name='Café de Rohan - 1 place du Palais Royal, Guerre mondiale (1914-1918)'
WHERE title.id_uuid = 'qr1521e9863f91d4756a695686d6337c666';


UPDATE title
SET entry_name='Café de Rohan - 1 place du Palais Royal, Guerre mondiale (1914-1918)'
WHERE title.id_uuid = 'qr127d7c57562ed44e183b7776541156725';


UPDATE title
SET entry_name='Véry Frères Restaurateurs - 83 place de la Maison Egalité (actuelle galerie de Beaujolais)'
WHERE title.id_uuid = 'qr1b0cc7fb7130647888cf32566e669fe7b';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr189bc87b3eb244faea58f4133b08b4f87';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1c8e8e51c4a4642ecafbb0a11e18a6222';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr15560219c35304bde9fcd746c647ebe0f';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1a8f31a5f6f224d03bead300c5e797fd3';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr14582272e0bdd4ac298689ff28ff3d780';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1a07fe851b6bc45d9ad980879e36330ab';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr158c8e22560174aa1ad00e97e3b5e0591';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr115b8b6035bcb4a13b092f13f624d66dd';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr15ac8b7625a4d4088b5965dd1fc20b500';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr15bf094b874484aef9e5f0b75c686302d';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr118b51e662f264370a90d2fc45dd29822';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr11ac61708b6074b3db72e5b952989b89c';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1c7b219eb9ef444589cd053254ffc717e';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr152373cc937374ca6b185e54cb93cb698';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr115ad84f32af34b48be61ce5df9808129';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1864c7759f2c5426f8858c3de9f2b2775';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1315ea4ec15c04dc7aea8c9526e79cbb9';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1dec718487da24372a548ee5c8e476060';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1bd16958fd09f41c8be32016faa35cfe2';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1abbe7b25f65348628dc84632a36d0aac';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr185bd77a8109b4c4aa61b3ca47435c1b3';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1d5cd3657a61340b2962f1c0cc86f7a23';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr102748c03c1744f6cbe86a710bca51052';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr1da9d5e148abd4e01a204fa30dfc18060';


UPDATE title
SET entry_name='Palais Royal. Le Conseil d''État'
WHERE title.id_uuid = 'qr16e3230229440499cbaa6ad3a187bc37d';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1dd1e7072443a4492bc5227317d68e7ce';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1830c44adcc1d4848abc3ddbaf8f502cb';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1728d70adb7b845acb67a1603cfe8c235';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr102757490298846f398146b384135f98a';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1c593fee7eb26485fae634bb4d79c36d8';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1c63940d0a724431d91db2336dc09f646';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1f6947dd3348446f48fe56ff0f4c18c86';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1bd59ba93a66e4045917dd348eac0ee76';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1af1a8dbc5baf4b20b8b4598c080a4477';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1119f2cf680f540b391d1fec95657ab80';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr147006c49564f439abf395d37393fe7a1';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1676e858e536a43eba6ccaa82e816a3a4';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr124b5c6967fd94722bdaf5f6a3b39e4e4';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr13c004a7801d04a72ad92214134dcbce9';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr18b33ebf41289465897ddad489adf1414';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1d86997cbc3454aea99465fa71e57922f';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1df7ee686bfcb40e2b39e9312cb362844';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr19eb7b8dd8ca74986a83095573ac22df5';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1d6937b82e51b43d9962f147d80c30b16';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr104e2c32e929e40c09fb5e58177cdb499';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1790e91b31b194067aa7a0ea08a9cf86b';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1b1405b1cc00740f490996588fb4a282b';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr19cd6c5083d0044468d348d78f692a3be';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1e5cba76c3d9e4812b280cdc38f2d55e8';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr138a85d98ffb044c4b44f646403146574';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr14700720049d6494fa5f119c8908f27b1';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1b4731c801f9245e2b0a99fa427cea17a';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr11e72664847214841933e99b11c8de9be';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr156e1a51b79e5477089d28ae2c4cc216b';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1f49cce8a6535489ebcc3a7d0e64ce7ab';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr165048491b451470687afbec2108a4407';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr173e57251069b4cc2a8a9fcf649c9add9';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr148ca9d77d64146bc90ed553d50d31b9e';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1f5fc5cb700094389b78c6822ddc4268d';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1274ac44eac354e038f86f47abd3421b7';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1ef277d6a41f4406984afaef2c6f90b39';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1cd5cba20758c4158b4831fa0e56e91ca';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1eb522a904eb7485b8ed3a3e26bcc0a68';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr16774a8dfa60743b295b5032f54161421';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1cd773f4ed627469baa530e1fc352866f';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1818ce27cf2c6485c8f74c788236bc07d';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr131701b78620d4e7a8e237d9e4993d1db';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr151481e33bd9f4c059011fb25b29314f5';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1990ddc4ed46d4effb2319ea1a8c8d00a';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1eafb977b41b54b39b93e03dcbde7e599';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr154e44859bf654d1ba9ed3539aff3753f';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1e74ed005a7d84a4390bd84c0b5766d45';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr116e7f2684cd2494798d20631e3ca3f68';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr14d291440249b4573a76965d6e3d80f98';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr19c2d1422fad64e0ca41073e8bbd4161e';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1b77464fb70c1402c8f2cca38105b3374';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1923ddbd98a4644f5bd770f886c607303';


UPDATE title
SET entry_name='Paris. Rue des Bons-Enfants, n° 19 et rue de Valois, n° 10'
WHERE title.id_uuid = 'qr15cafad50174044699803818c5e9ff795';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants, n° 19 et rue de Valois, n° 10]. Hôtel dit de la Chancellerie d''Orléans. Rez-de-chaussée. Salons. Grand salon'
WHERE title.id_uuid = 'qr1f70ccf6259c04a3ca3fa4f2ce641a486';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants, n° 19 et rue de Valois, n° 10]. Hôtel dit de la Chancellerie d''Orléans. Rez-de-chaussée. Salons'
WHERE title.id_uuid = 'qr1fec61a8699ac421983b22aeff91fd3cc';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants, n° 19. Hôtel de la Chancellerie d''Orléans. Façade sur cour]'
WHERE title.id_uuid = 'qr1fd012d169a8e462ba20db381edeeb161';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1bd7160b647a84040b8bdeb1d409f4b7e';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1d40d50bcce9e467fadd50ce05088b892';


UPDATE title
SET entry_name='Place du Palais Royal (1er arr.). Une courageuse marchande de quatre saisons roule une voiture bondée de fruits d''où émerge un de ses petits enfants'
WHERE title.id_uuid = 'qr1c4c360f843de4bc2a79a9c3797e09bac';


UPDATE title
SET entry_name='Madame et son filleul [pièce de Maurice Hennequin, Palais royal]'
WHERE title.id_uuid = 'qr1fc3988ea21d740a4b8d9e0ca2c523c7d';


UPDATE title
SET entry_name='Paris et ses environs. Les jardins du Palais Royal'
WHERE title.id_uuid = 'qr1e3bd7c531e814246a21b2db75cb983f2';


UPDATE title
SET entry_name='Paris et ses environs. Les jardins du Palais Royal'
WHERE title.id_uuid = 'qr10937777fce414b3f89ea8c43ca6322cd';


UPDATE title
SET entry_name='Paris et ses environs. Les jardins du Palais Royal'
WHERE title.id_uuid = 'qr191380bb1af964126bdb38a72c54d0ae9';


UPDATE title
SET entry_name='Paris et ses environs. Les jardins du Palais Royal'
WHERE title.id_uuid = 'qr1e0a6f96fb99745f487d0f5fec38ecccd';


UPDATE title
SET entry_name='Paris et ses environs. Le Palais Royal'
WHERE title.id_uuid = 'qr10fbc62946568404e9756481e4613eba0';


UPDATE title
SET entry_name='Tous les soirs, Théâtre du Palais-Royal. La marmotte'
WHERE title.id_uuid = 'qr130cf56cbfb584adabdc6e81a1dd6fc7d';


UPDATE title
SET entry_name='[Paris. Rue de Valois. Entrée du passage de la] Cour des Fontaines'
WHERE title.id_uuid = 'qr10498a1eb65384e7e924f8204e77bd080';


UPDATE title
SET entry_name='[Paris. Rue de Valois]'
WHERE title.id_uuid = 'qr13c75122c5d64466a9907b9cca3f92f7b';


UPDATE title
SET entry_name='[Paris. Rue de Valois]'
WHERE title.id_uuid = 'qr1bddb4bc0190940138d4530132b9b77c5';


UPDATE title
SET entry_name='[Paris, rue de Valois. Cartes postales]'
WHERE title.id_uuid = 'qr11924c6bd9f474180991f5fa90afd7e53';


UPDATE title
SET entry_name='[Paris, rue de Valois. Cartes postales]'
WHERE title.id_uuid = 'qr182efacaf180e4620ba44128d42a865ce';


UPDATE title
SET entry_name='[Paris, rue de Valois. Cartes postales]'
WHERE title.id_uuid = 'qr1ff20626eaf4f4e5c99d6489d04817425';


UPDATE title
SET entry_name='[Paris.] Indication d''un abri et soupirail revêtu d''une couche de plâtre pour éviter les gaz, rue de Valois'
WHERE title.id_uuid = 'qr1f4869c554b2643339ce38416ab84f19e';


UPDATE title
SET entry_name='[Lampadaire, rue de Valois, devant le passage menant au Palais royal]'
WHERE title.id_uuid = 'qr11e176fa72c3a409d99b13a3dfad3b0d9';


UPDATE title
SET entry_name='[Boutiques parisiennes. Glacier, pâtissier. Glacier du Palais Royal, 6, rue de Valois (1er arr.)]'
WHERE title.id_uuid = 'qr11d4b5a85083e43f3bbc7efe394283cb3';


UPDATE title
SET entry_name='[Déjeuners de bienfaisance] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr1117c37e76ff6467b999c157e96f6784b';


UPDATE title
SET entry_name='[Le chef] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr1786f3a63025c45adadfc7251b1917725';


UPDATE title
SET entry_name='[La desserte] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr16b8ffe52d1654a19a279dd5b9f681fcd';


UPDATE title
SET entry_name='[Les tickets] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr116fad950fc864ba69d4e93cc7d9aad5d';


UPDATE title
SET entry_name='[La table aux desserts] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr1eef360e8eb6548f3bdcedc679adbb46f';


UPDATE title
SET entry_name='[Le restaurant] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr1b1a7cb4fd5f14c6bb17abcc4e7b8a307';


UPDATE title
SET entry_name='[Le restaurant] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr1de4785c106744d919daa3dd496c62475';


UPDATE title
SET entry_name='[Le restaurant] Les déjeuners de bienfaisance (Oeuvre du Maréchal [Pétain]). Déjeuner de Madame Coupet au 23 rue de Valois, Paris [1er arr.]'
WHERE title.id_uuid = 'qr19913d051dc854e8bbf8e3cc2019e8c7f';


UPDATE title
SET entry_name='Banquet Funck-Brentano'
WHERE title.id_uuid = 'qr13e2560cbd2974cff9f77796411e05d89';


UPDATE title
SET entry_name='[Recueil iconographique. Rue de Beaujolais (Paris)]'
WHERE title.id_uuid = 'qr19dcbd8e758324162b9a3b3cba6aa7583';


UPDATE title
SET entry_name='[Boutiques parisiennes. Bandagiste. Bandages et suspensoirs assortis, Maison Millan Ainé, 22, rue de Beaujolais (1er arr.)'
WHERE title.id_uuid = 'qr108b92eb9f4414a399ed7b0ab2298874d';


UPDATE title
SET entry_name='Passage du Perron. 9 rue Beaujolais'
WHERE title.id_uuid = 'qr160af78834c4345c8a50706415a1f113b';


UPDATE title
SET entry_name='Passage des deux Pavillons. 6 rue Beaujolais'
WHERE title.id_uuid = 'qr1466d6fd4d7b54329a00c2fc772fcc6a8';


UPDATE title
SET entry_name='Passage Beaujolais, vue prise vers la rue de Beaujolais'
WHERE title.id_uuid = 'qr15a6fd451ed504ab585e30bcddddd8270';


UPDATE title
SET entry_name='Paris et ses environs. Le Conseil d''Etat au Palais Royal'
WHERE title.id_uuid = 'qr19e25d032b87a4ef281272f96a7a1b654';


UPDATE title
SET entry_name='Les Bateleurs du Palais Royal. 13, 14, 15 août 1944'
WHERE title.id_uuid = 'qr1b6185efd80ac40ae8843c62e80d5b897';


UPDATE title
SET entry_name='[Maisons d''écrivains : Colette. Fenêtres de son appartement, rue de Beaujolais n° 9, sur les jardins du Palais-Royal. Vue prise de ce dernier]'
WHERE title.id_uuid = 'qr135f22b87d0034b7f8657cb7bafd31dab';


UPDATE title
SET entry_name='Paris et ses environs. Le Palais Royal'
WHERE title.id_uuid = 'qr141bdd5a893c84591baa874890fa44818';


UPDATE title
SET entry_name='Paris et ses environs. Le Palais Royal et les jardins'
WHERE title.id_uuid = 'qr148bceab76734420f839d6e2d3d935bd9';


UPDATE title
SET entry_name='[Boutiques parisiennes. Jardins. Jardin du Palais Royal, groupe d''enfants (1er arr.)]'
WHERE title.id_uuid = 'qr197f87c56e04c430f98e240d77c94d7c6';


UPDATE title
SET entry_name='[Paris. Palais Royal. Vue de la façade principale prise de la place du Palais-Royal]'
WHERE title.id_uuid = 'qr18d2b336c419f483483a668b7e7cd58be';


UPDATE title
SET entry_name='[Boutiques parisiennes. Groupes d''élégantes, cour du Palais-Royal (1er arr.)]'
WHERE title.id_uuid = 'qr17bc09e3cf31449f4b7257613fc746ca9';


UPDATE title
SET entry_name='Colette à son domicile au Palais Royal'
WHERE title.id_uuid = 'qr139aadf54a33c4940a2923f434241aa19';


UPDATE title
SET entry_name='Le Palais-Royal et son jardin. Le jardin, pris vers le nord-est'
WHERE title.id_uuid = 'qr1eaf0bb6e0f82456ca36db8868cc489f7';


UPDATE title
SET entry_name='300 : Station du Palais Royal. Construction des accès'
WHERE title.id_uuid = 'qr1c4dcf820a93d4c0d818383db2999d7b9';


UPDATE title
SET entry_name='299 : Station du Palais Royal. Construction des accès'
WHERE title.id_uuid = 'qr19c018333a46541f287698835483a63bf';


UPDATE title
SET entry_name='Transformations de Paris, des années 50 aux années 80. Deux militaires et un femme avec une voiture d''enfant dans les jardins du palais Royal (1er)'
WHERE title.id_uuid = 'qr1d8164af56cf84ee7a1c745e6b08f9dc9';


UPDATE title
SET entry_name='Le Palais-Royal et son jardin. Façade sur la cour d''honneur, vue prise de celle-ci vers le sud'
WHERE title.id_uuid = 'qr1b486d46be8644c7dba5b408acc9be858';


UPDATE title
SET entry_name='18 rue de Valois'
WHERE title.id_uuid = 'qr1fb66fbaf789a4624bc7a06a8f5921e38';


UPDATE title
SET entry_name='Le Palais-Royal et son jardin. Grille et façades des galeries à l''ouest du jardin'
WHERE title.id_uuid = 'qr15ffbc64a9ada484b9719d5243c860065';


UPDATE title
SET entry_name='Transformations de Paris, des années 50 aux années 80. Cour du Palais Royal (1er)'
WHERE title.id_uuid = 'qr16bb405399c5f464782ed35267909e791';


UPDATE title
SET entry_name='Paris et ses environs. Enfants devant un kiosque de marchand de jouets dans les jardins du Palais Royal'
WHERE title.id_uuid = 'qr1b4ff5e65522a4c97bb6a8d78febe0dfc';


UPDATE title
SET entry_name='Le Palais-Royal et son jardin. Grille et façades des galeries à l''ouest du jardin'
WHERE title.id_uuid = 'qr1f3a9a23607b946489034b4d2f4ea9207';


UPDATE title
SET entry_name='Réjane en costume de japonaise, dans la pièce d''Henry Meilhac "Ma camarade", créée en 1883 au théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1cd10449dc50949b0a5c14051ce967ea4';


UPDATE title
SET entry_name='Théâtre des Variétés, la Carotte, le grand succès comique actuel du Palais Royal'
WHERE title.id_uuid = 'qr1e3cf29abb7e04dc5a443b8b0ff97d87b';


UPDATE title
SET entry_name='Prochainement Tournée Frédéric Achard. Les joies de la paternité. Comédie en 3 actes de MM A. Bisson et Vast-Ricouard grand succés du Palais Royal'
WHERE title.id_uuid = 'qr12ba81b61ed384b0dbb2de5c19ebe6c9d';


UPDATE title
SET entry_name='La merveilleuse journée [pièce en trois actes de MM. Yves Mirande et Gutave Quinson, Palais-Royal, Privilège Rivers]'
WHERE title.id_uuid = 'qr17e8781fe9c25447fb8d44ff03e901475';


UPDATE title
SET entry_name='Les Deux canards [pièce de Tristan Bernard et Alfred Capus, Théâtre du Palais Royal]'
WHERE title.id_uuid = 'qr138ab9339d0fa4fedbcd0eced76234c56';


UPDATE title
SET entry_name='Au premier de ces messieurs [pièce d''Yves Mirande et Mouëzy-Eon, Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1ffec643f39de4de391c351c1ab004466';


UPDATE title
SET entry_name='Au premier de ces messieurs [vaudeville en trois actes de Yves Mirande et Mouëzy-Eon], tournée du théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1cb126f74b1884e6c85ca542fc1ea806a';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Plan du Palais-Royal et du jardin'
WHERE title.id_uuid = 'qr175fc1440133c4ca9aae7881a1bb1d1aa';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Plan du Palais-Royal et du jardin'
WHERE title.id_uuid = 'qr11f9273d4bf244d0b88f3696763d7ba33';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Palais-Royal en 1679'
WHERE title.id_uuid = 'qr12dcab5f33ef74202952e8161c1a70d7c';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Palais Royal, 1645'
WHERE title.id_uuid = 'qr17835daa3cc2f4967a27109a62ca39c18';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Palais-Royal, 1794'
WHERE title.id_uuid = 'qr148ea355b809443169d29abde338a4e8f';


UPDATE title
SET entry_name='[Paris. Le Palais-Royal]. Palais Royal, 1645'
WHERE title.id_uuid = 'qr1ce6cc91b390844aca2082dfc8b6d7828';


UPDATE title
SET entry_name='[Paris. Jardin du] Palais-Royal'
WHERE title.id_uuid = 'qr1d95249871c4d4d02b92d60dfd643daf5';


UPDATE title
SET entry_name='[Paris. Jardin du] Palais-Royal'
WHERE title.id_uuid = 'qr1a544a589585444998937da89aa5495aa';


UPDATE title
SET entry_name='[Paris. Jardin du] Palais-Royal'
WHERE title.id_uuid = 'qr16b4ec68ca2294ecc8e9f62f654bf7305';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, événements de 1830]'
WHERE title.id_uuid = 'qr1a3e55978e2934bf48abdb2343dd81a81';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, événements de 1830]'
WHERE title.id_uuid = 'qr118a15eedbe3c41b49d3f1c0685364de4';


UPDATE title
SET entry_name='Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1521b78a92ab7438cba0b88a79bb79d82';


UPDATE title
SET entry_name='Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1b82c42b93840407c8e30ed344d35bba1';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1e68a848d1320455c82e271f47ced5621';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr100d95e00801d448c8ac54cbaa370301e';


UPDATE title
SET entry_name='[Paris. Palais-Royal, dessins d''architecture]'
WHERE title.id_uuid = 'qr14d868da19840488d9a47bab422a4feab';


UPDATE title
SET entry_name='[Paris. Palais-Royal, dessins d''architecture]'
WHERE title.id_uuid = 'qr1f830a756923449febc5e0515796697b1';


UPDATE title
SET entry_name='[Paris. Palais-Royal, dessins d''architecture]'
WHERE title.id_uuid = 'qr1ebe5066e5171435b8f904ecea65e2550';


UPDATE title
SET entry_name='[Paris. Palais-Royal, dessins d''architecture]'
WHERE title.id_uuid = 'qr16f007b1d2c654baaa1afac74242aad9b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, dessins d''architecture]'
WHERE title.id_uuid = 'qr1a9006da399a4437c812c1bb91e9250f8';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal, rue de Montpensier, n° 38. 1er arr. Avant-scène.]'
WHERE title.id_uuid = 'qr1cce2d334d53b40728788888c8b079bf2';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal, rue de Montpensier, n° 38. 1er arr. Décoration et rideau de la scène.]'
WHERE title.id_uuid = 'qr14f26d7d120e4484aa0f7ce0225a62e00';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal, rue de Montpensier, n° 38. 1er arr. La scène et les loges.]'
WHERE title.id_uuid = 'qr12adc15babef64c3787479c61af92984f';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal, rue de Montpensier, n° 38. 1er arr. La scène, rideau tiré.]'
WHERE title.id_uuid = 'qr1d80b40ac38d147eeb506d637c1a2980b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les magasins]'
WHERE title.id_uuid = 'qr10ca2d90e99214b79b309920fac957ad1';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les magasins]'
WHERE title.id_uuid = 'qr17d44f8ec613a4f6fb4ecad7d6104ea86';


UPDATE title
SET entry_name='[Paris. Palais-Royal, le Conseil d''Etat]'
WHERE title.id_uuid = 'qr1c3a7e8cdaa834e2a97be0e5e56978b14';


UPDATE title
SET entry_name='[Paris. Palais-Royal, le Conseil d''Etat]'
WHERE title.id_uuid = 'qr15fd9b6854f704fb69a2771fc192de017';


UPDATE title
SET entry_name='[Paris. Palais-Royal, projets]'
WHERE title.id_uuid = 'qr19b974ac29a2d4498860a12ab803b0963';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le Cirque, extérieur et intérieur]'
WHERE title.id_uuid = 'qr197d72501124f4c4bb8009a1c9f2b9de3';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le Cirque, extérieur et intérieur]'
WHERE title.id_uuid = 'qr14d810871fb3f4305b84ada020a56aa8a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le Cirque, extérieur et intérieur]'
WHERE title.id_uuid = 'qr123bc7fffdc7443c7b258dd51dc5c8606';


UPDATE title
SET entry_name='[Paris. Palais-Royal, escaliers]'
WHERE title.id_uuid = 'qr1122dc6aa73204c82b65a94aaf69d5edf';


UPDATE title
SET entry_name='[Paris. Palais-Royal, escaliers]'
WHERE title.id_uuid = 'qr1242aa375a8914027b4cf333bae44f350';


UPDATE title
SET entry_name='[Paris. Palais-Royal, escaliers]'
WHERE title.id_uuid = 'qr149c9b664a5174308b1c9e34e36b86d40';


UPDATE title
SET entry_name='[Paris. Palais-Royal, escaliers]'
WHERE title.id_uuid = 'qr1e49f087fafd74e0a966037e49a8e028d';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1aaf308c86a7f4b9c90f12095f2d20d7d';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1be9227d268e3408abd3555911920fa42';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1bb2b0bf22c7a4f788793af37b02f6795';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1630a95349d6a4240b308ada4d74a9fa9';


UPDATE title
SET entry_name='[Paris. Théâtre du Palais-Royal]'
WHERE title.id_uuid = 'qr1275fbe60365d48b2b9c1e2d712cbc4f2';


UPDATE title
SET entry_name='[Paris. Palais-Royal, première cour ou cour de l''Horloge, cour des Fontaines et cour d''honneur]'
WHERE title.id_uuid = 'qr17dc1b3377ee342ec94262b914803058b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, première cour ou cour de l''Horloge, cour des Fontaines et cour d''honneur]'
WHERE title.id_uuid = 'qr12080e4b36dba4856881c2dfd12bbc268';


UPDATE title
SET entry_name='[Paris. Palais-Royal, première cour ou cour de l''Horloge, cour des Fontaines et cour d''honneur]'
WHERE title.id_uuid = 'qr1b72aed9135644abb9c6940226782e9cc';


UPDATE title
SET entry_name='Palais Royal. Vues anciennes'
WHERE title.id_uuid = 'qr13f776ac1692a47b1a31be95300e4a3f7';


UPDATE title
SET entry_name='Palais Royal. Vues anciennes'
WHERE title.id_uuid = 'qr105667b79812941968f66b555fe4d315b';


UPDATE title
SET entry_name='Palais Royal. Vues anciennes'
WHERE title.id_uuid = 'qr1abd666b1048547358c3e2b313225d4e8';


UPDATE title
SET entry_name='[Paris. Palais-Royal, côté jardin et façade côté jardin]'
WHERE title.id_uuid = 'qr169f0fe7436fe4f67aece6505677c562d';


UPDATE title
SET entry_name='AD075PH_UPF1308'
WHERE title.id_uuid = 'qr1ef9fd0b634fa4a269492ca1faa2cc73e';


UPDATE title
SET entry_name='[Paris. Palais-Royal, côté jardin et façade côté jardin]'
WHERE title.id_uuid = 'qr1f24b0f551e2c459883f91459984d0e71';


UPDATE title
SET entry_name='[Paris. Palais-Royal, côté jardin et façade côté jardin]'
WHERE title.id_uuid = 'qr11d5c81aa99414b63b98f58ebc360ee92';


UPDATE title
SET entry_name='[Paris. Palais-Royal, côté jardin et façade côté jardin]'
WHERE title.id_uuid = 'qr19f4afb34d03745e69cbd4a39e5219284';


UPDATE title
SET entry_name='[Paris. Palais Royal. Façade incendiée]'
WHERE title.id_uuid = 'qr1ca080735b7d24dd2883d9f0404a7a97d';


UPDATE title
SET entry_name='[Paris. Palais Royal. Façade incendiée]'
WHERE title.id_uuid = 'qr15b5e799c4d544bbab43a18b148a1b0d2';


UPDATE title
SET entry_name='[Paris. Palais Royal. Façade incendiée]'
WHERE title.id_uuid = 'qr1c6990ca6f9734628aeb5f2e472aa8944';


UPDATE title
SET entry_name='[Paris. Palais Royal. Aile gauche incendiée]'
WHERE title.id_uuid = 'qr18b18c588dd9a4c26be4d3ba51c4b7219';


UPDATE title
SET entry_name='[Paris. Palais Royal. Aile gauche incendiée]'
WHERE title.id_uuid = 'qr194266d6a8adc48a3923a8e614addd15a';


UPDATE title
SET entry_name='[Paris. Palais Royal. Aile gauche incendiée]'
WHERE title.id_uuid = 'qr1c465c52f4b824d399895b63481adf289';


UPDATE title
SET entry_name='[Paris. Palais Royal. Aile gauche incendiée]'
WHERE title.id_uuid = 'qr11304909a50ef41cbb564478b712a40c4';


UPDATE title
SET entry_name='[Paris.] Place du Palais Royal. [Palissade entourant un empilement de bois]'
WHERE title.id_uuid = 'qr1efaf8b3acf44435198e041bc9e5d884f';


UPDATE title
SET entry_name='[Paris.] Place du Palais Royal. [Palissade entourant un empilement de bois]'
WHERE title.id_uuid = 'qr19917d067146b4dbb8a60dfd9c812c753';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, statues et canon]'
WHERE title.id_uuid = 'qr1aa757d0ce53f4ac5abe758cedfe97b0f';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, statues et canon]'
WHERE title.id_uuid = 'qr1a068d426d62942ceb4ca2468be5801f1';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, statues et canon]'
WHERE title.id_uuid = 'qr18f1da58efe284e3bab3f12e07a40c7d5';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, sous la Révolution française]'
WHERE title.id_uuid = 'qr1defd34fdd6fe44459647f3dc7c3c0ca6';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, sous la Révolution française]'
WHERE title.id_uuid = 'qr1cb2cd9781c504b66be70680506554f88';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, sous la Révolution française]'
WHERE title.id_uuid = 'qr189345dbca1e7495a87ad2805e52a75f4';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, sous la Révolution française]'
WHERE title.id_uuid = 'qr1ca17ed1d7c5c4c8db578b0ee4442038c';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, sous la Révolution française]'
WHERE title.id_uuid = 'qr1a8ad1c100dda4c51b47bead4a33875f8';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr1c4d2518821904d0da306a8ca300c4f8c';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr1900aaf5591b3468d807d354858b34db3';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr1a103c95ebdcd4cb484b5addd11c259f0';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr1c9eb0f5777b948f4b35dfce8e2b39f77';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr1dc51f87d96714779ad6992585a5caf2d';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr127854ae1c2c54acd937a437483e65065';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr187c2527ee0234923993a90f7fea24c04';


UPDATE title
SET entry_name='[Paris. Place du Palais-Royal]'
WHERE title.id_uuid = 'qr19d2cd8878f934e69b65f582fe2f70151';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr14a6ccca3263b4d98ba159bce64275a79';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr10530ebfe06b54c2aaff837b6a725e018';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr1f0807392f0da494caab111ee15d2259b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr1f8562e4c676645fab9ee496d3db96564';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr12198c6eb53434a2c9f7233bf352800e8';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr1a94c9c627ca2429c83c53b4cd4d2dfb6';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr173dcb5fbc07b41ada45470a6572c8775';


UPDATE title
SET entry_name='[Paris. Palais-Royal, cafés et restaurants]'
WHERE title.id_uuid = 'qr19b67effd68854285b1c89443f5f96efe';


UPDATE title
SET entry_name='[Paris. Palais-Royal et jardin, sous la neige. 1er arr.]'
WHERE title.id_uuid = 'qr1133674eeaa424ea785967bcb8689ed54';


UPDATE title
SET entry_name='[Paris. Palais-Royal et jardin, sous la neige. 1er arr.]'
WHERE title.id_uuid = 'qr1f34b32b96b7144319fb5b70aff05d72d';


UPDATE title
SET entry_name='[Paris. Palais-Royal, le canon qui tonnait autrefois à midi pile. 1er arr.]'
WHERE title.id_uuid = 'qr121fab52a222c4c82a3dff75b808fcaa3';


UPDATE title
SET entry_name='[Paris. Le Palais Royal et le jardin, vers le musée du Louvre. 1er arr.]'
WHERE title.id_uuid = 'qr1b8980a98cdc943d8ab164a5b41031b41';


UPDATE title
SET entry_name='[Paris. Le Palais Royal et le jardin, vers le musée du Louvre. 1er arr.]'
WHERE title.id_uuid = 'qr1d2f8de5745624f35a0a636e42810092b';


UPDATE title
SET entry_name='[Paris. Le Palais Royal et le jardin, vers le musée du Louvre. 1er arr.]'
WHERE title.id_uuid = 'qr1dddd77dac9ce483d95e62e5b4f63b41c';


UPDATE title
SET entry_name='[Paris. Jardin du Palais Royal. 1er arr.]'
WHERE title.id_uuid = 'qr1a61c9bdb8213454981d0fa3523409fea';


UPDATE title
SET entry_name='[Paris. Jardin du Palais Royal. 1er arr.]'
WHERE title.id_uuid = 'qr1d1cbf9bc4d3e490cb91b83b4a334e7dc';


UPDATE title
SET entry_name='[Paris. Le Palais Royal et le jardin, vers le musée du Louvre. 1er arr.]'
WHERE title.id_uuid = 'qr106bf119889844e758210764a5e16306d';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1c529367adcc240fc9885e56bf76656de';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1d26261a5fa804f3cae7f16b3f014f111';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr12e33c4b76c5749e2b44c63cc8d3b42b6';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1fbcea28a51e2463b9baa81c3bd555dab';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr193fa2db02a9e42d49a9b80ead23c50e9';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr162b6bd00cbd84beb89329099fe98a175';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr17c09e4b43d8c4cadac65c58d67fd2db9';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1da7a21f47e834153a6f112a9d8a0a5d1';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1297d449e068f4961a1eae3b053146356';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr129c203af3dc245409eba4dc98a21dd96';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr105d1f698941d4ac5aef6cc0e5ad06b99';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr1d84cc43c7f704a22a6097b7a986db606';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr12be2eacdd2044916a183aae1ae0f7326';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr1ad17bc54055b4f4c9ebcb129dd38c74a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr1e5036a021c2d491e9ad5e35c4df99a76';


UPDATE title
SET entry_name='[Paris. Palais-Royal, intérieur]'
WHERE title.id_uuid = 'qr10100da07ec974f3abfc42fb8feaf663e';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1ff1096a722f1499bbd6680f4736572ee';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1c284b33a3f60436ba86e88985168fab9';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr18c32a21cde484f558c3b4b33d79712c2';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr124b404ce5a114d36bda99c2d98296572';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1c0f93aad7a8e4a99ae8b1e7aae1da4de';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr13e09c0cc3d4e4a55873e3ff2456bc722';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1c18889e603074ff48dccb8dd378168b4';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr1367fc6fd2bf5453dad84c4eb32a9262d';


UPDATE title
SET entry_name='Palais-Royal. Extérieur'
WHERE title.id_uuid = 'qr122c6f06c82f249a0b8b522f001fd99f9';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr1af707636616f458aa3b6cb0b2000f69a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr1e70c5c7e449046a593f3054419e6c93a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr1038c723b5f8b40c69214057e7d841c71';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr1bbd85d92be8146179e3d2c95b3b10621';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr1fe41422bb12b4b8592d2bd438b2929f6';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr165a121ac061243b1a8f542ea697ffac4';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr150ae603f20c1499fa1828c34dd267195';


UPDATE title
SET entry_name='[Paris. Palais-Royal, plans]'
WHERE title.id_uuid = 'qr12f32e79116c047e0aaa088760a1b1d30';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr10d17329cd59442a7bf2ee4d9dae08b66';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr182157b3298ae42a3b8a1258941179b86';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr15b05cd7ab0284e48b9f3831be8bab886';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr1d7654aa72d8a4c4ca3887e7a39316837';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr199f417d62f2a43c7b7972534d4309f9b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr19155969cd88a42d5995cfed255582c75';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr13a434847ea4f4bccac2c2cfb65e9cdef';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr1f949c73338a44bc6b62b7f4ecc7eb67f';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins, le public]'
WHERE title.id_uuid = 'qr1206a2aebe6de4be7b550dd018d4ed3ae';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr1d5d6350fe5fe4fb2a8d4c34d38be78c5';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr1118c24d606b24fa0845c0da4393ea934';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr1f900fb4300f94dda9c3f3992112c1f4a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr12ac6e39757f243348e77e0adeaa366ad';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr144b73bd931c3485d87013b68c0445915';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr18aef3c7b46444b3ca2794d53a754dfcb';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr17707318699aa4dd798677d6d8dcc0de2';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr10262d8775e5e4064b243d178c04830fb';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr18a18038b06274320bfdd09016ba6124d';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr14eb9e83ffb004608a529d316ef1159cd';


UPDATE title
SET entry_name='[Paris. Palais-Royal, Façade principale et autres façades]'
WHERE title.id_uuid = 'qr19d7cbdeb9d3a49b08c8252b15fc6336e';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1492298f8242b44aa8d7a98a17d711449';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr18b95ab382c2343a6a4d6a84a503b19a2';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr12284a20d597f4bd7a66e8edc9880ea9a';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1979841aa11fc445588e1355777592c1e';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1ccbf91755a33421e8c63013f280d57e4';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr18e7efd29978c4f82bd551c7c42fd1750';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr17050f58c12684544bdd8ef40e888da91';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr121afaf319c9d4cbfb2c2ef44e9898b8a';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1ae2a68267a6f47fdb1e89d209d260d2a';


UPDATE title
SET entry_name='Galerie du Palais-Royal'
WHERE title.id_uuid = 'qr1482d4f83c1e64f3b934d1923c91cd1b7';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr130a3aa7749d84710bc1db503d3ce1029';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr12be784265f464c26ad5dd21b63c10db7';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr124c386dab4014cb7a2b9d0fb5600c1bc';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr12393cf1820c1466f8867f0f215cbfecd';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr17f4e6aaeca50462d9d5bec610c0774b0';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr190d177545c914eebb4aeda6f3fd9bc3b';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1696fb760e1664082b8cc079011740ffd';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr18dc2d762731548f994c6093b53da8dbe';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1bfb1ce3fa318415eb7aba5b3a6a780c0';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1e70ec5ed88f643f3ba3580c1e3db128f';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1d5eaa63117de46a389c6adc40ea77909';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1838d5c76af954002860149d13d3cf4c1';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr1b8893251448248dca9ab0cd840f84c9c';


UPDATE title
SET entry_name='[Paris. Palais-Royal, les galeries]'
WHERE title.id_uuid = 'qr130104f9ce08646779ac678cf812ce826';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr15189001f15e74bb187b29bd56706bf05';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1116b0a2de011434d9d773a552314002c';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr19c8f013eba474fda8ef4c9195b1f9642';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1704f2e39264a4ef7902d6541135329ee';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr199e5a605ddf643a099c1d2aa27aea31c';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1fca460f5ef574d3a8fe4d97ac10bc978';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr166d48310d62c402e86be88be6eb34511';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr132894a382f1d476bafb3f7be40c66103';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1a9a2f20390704dd6a2386e271c964ec3';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr187c79ceba8ff4e388ffdba18897bd9b3';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1cc4efd126bd049f6a264a36bd1845f34';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr16705d44e130f4c07b0db64235dbde098';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1f345b435ff8144c093214d89d85ea04f';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1da66c64b4c2b413f84c5e0ceffd7ef1d';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1feb04fb3cde042038baa0ed6b67576b4';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1948925bb3b88406c925305f7aa14b32c';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr1cce33f040d0f4d42adfbbcb5a28eb96a';


UPDATE title
SET entry_name='[Paris. Palais-Royal, jardins]'
WHERE title.id_uuid = 'qr193ee683af1ee4d2b9d0ffde627b66832';


UPDATE title
SET entry_name='Paris et ses environs. Galerie du Palais Royal'
WHERE title.id_uuid = 'qr1da112e7712684e5c823c8a948d8aeff4';


UPDATE title
SET entry_name='N°48 [Paris], Palais-Royal'
WHERE title.id_uuid = 'qr195096b91c92d49a6ad569d489fc76e7c';


UPDATE title
SET entry_name='N°6 [Paris], Palais-Royal'
WHERE title.id_uuid = 'qr129049273d5764ae8973f61f5e2af96e7';


UPDATE title
SET entry_name='Cour du Palais-Royal [1er arr.]'
WHERE title.id_uuid = 'qr1dd2f3b20593f4d8c899ed1ba83719102';


UPDATE title
SET entry_name='Galerie [de Montpensier] du Palais-Royal [1er arr.]'
WHERE title.id_uuid = 'qr13a38b3a383034449b3494586a7a7b784';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr1e2eb7a4e0e214556bf13aa39ee13ddc8';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr1683f10994bce4f9f870dda3964504d1f';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr1e1b832ceef0448be914740282bfe0448';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr13b98c4f007334d8a886c124f2fe2d75d';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr1941e9d34e0d940b7b5bda86735bc7ed4';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr14b53b6c0eab84cb0b12373c5b34850d7';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr1da1e511c088a40e998718acd89f3b9d2';


UPDATE title
SET entry_name='[Femme souriante]'
WHERE title.id_uuid = 'qr1f37214e980ba489f829d135b866d2569';


UPDATE title
SET entry_name='[Bateleur]'
WHERE title.id_uuid = 'qr151cde2c7151149d7b8635263eae0f48f';


UPDATE title
SET entry_name='[Un bateleur]'
WHERE title.id_uuid = 'qr182d19b1dc6bb4bfa8d3b15aa3d3953df';


UPDATE title
SET entry_name='[Des bateleurs]'
WHERE title.id_uuid = 'qr177dfa558bdd44e7e82b99a642d21c7fc';


UPDATE title
SET entry_name='[La foule]'
WHERE title.id_uuid = 'qr19ffd02fa69fc491bbf8cccf146136d10';


UPDATE title
SET entry_name='[Bateleurs masqués]'
WHERE title.id_uuid = 'qr181edc682a23449bd9eb516239d6cf642';


UPDATE title
SET entry_name='[Le soldat-danseur]'
WHERE title.id_uuid = 'qr16ed28ed038b04df88b22266fa75c6ab9';


UPDATE title
SET entry_name='[Un couple sur scène]'
WHERE title.id_uuid = 'qr121f1243002f144d2a03ad484c00a1e73';


UPDATE title
SET entry_name='[Un peintre en action]'
WHERE title.id_uuid = 'qr1cae6186119ee49cdac74295982676c6b';


UPDATE title
SET entry_name='[Tireuse de cartes]'
WHERE title.id_uuid = 'qr17b74aa1b53d84019985735b9e65c1fa4';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr187991f215b764fde8765b3e6e9a0b0c2';


UPDATE title
SET entry_name='[Des bateleurs]'
WHERE title.id_uuid = 'qr19acc7690fec2460794aa1616c606b77e';


UPDATE title
SET entry_name='[Chanteurs]'
WHERE title.id_uuid = 'qr14cabe8c6676c454c98efd20ffedab32f';


UPDATE title
SET entry_name='[La foule]'
WHERE title.id_uuid = 'qr1880a473a9eff41189ebbf85b71b6d69a';


UPDATE title
SET entry_name='[Musiciens]'
WHERE title.id_uuid = 'qr15172ec2e002e439eb66fb42b864008d2';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr1f7391f64ca8f4266911336c3aee315fe';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr167c5d36f61fe47b3a3acdcefc5648982';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr160954e2fbe074b11922257308528bd5d';


UPDATE title
SET entry_name='[Le soldat-danseur]'
WHERE title.id_uuid = 'qr11a45ffed2b64439c9f482407a9558ef5';


UPDATE title
SET entry_name='[Un peintre en action]'
WHERE title.id_uuid = 'qr1b47137ddcfba4388aaeebb0566cace36';


UPDATE title
SET entry_name='[Les "comédiens" saluent]'
WHERE title.id_uuid = 'qr12d97994a368742a4aae7de8a43ad7b18';


UPDATE title
SET entry_name='[Chanteurs]'
WHERE title.id_uuid = 'qr12b7e31b5218547a6ab22452ed91300a2';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr115258945f69e4234ac535a3e5e72e354';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr1c3aea9be5a0c4e878382c9f40cb2eb40';


UPDATE title
SET entry_name='[Musiciens]'
WHERE title.id_uuid = 'qr177ddfd63d45c43df8c5b1c06a22679ce';


UPDATE title
SET entry_name='[Préparation en coulisses]'
WHERE title.id_uuid = 'qr16bc28d50a3374b30bd84f33bacbdb6e6';


UPDATE title
SET entry_name='[Deux clowns]'
WHERE title.id_uuid = 'qr12f595e8d3a07497e91991dea8220299a';


UPDATE title
SET entry_name='[Bateleurs masqués]'
WHERE title.id_uuid = 'qr15607397754aa4cadab7be7e98f0a147b';


UPDATE title
SET entry_name='[Un bateleur]'
WHERE title.id_uuid = 'qr1664509283e1a41b88f10dc6ae55c2e4e';


UPDATE title
SET entry_name='[Enfants dans le public]'
WHERE title.id_uuid = 'qr17d0ccfba49a64d8bba8caf710c99027b';


UPDATE title
SET entry_name='[Un peintre en action]'
WHERE title.id_uuid = 'qr1d24bf03114de4dcab81e25c8b9670f84';


UPDATE title
SET entry_name='[Chanteurs]'
WHERE title.id_uuid = 'qr115b1dafe90804460a04c4913593cfcd6';


UPDATE title
SET entry_name='[Repos des danseuses]'
WHERE title.id_uuid = 'qr137d2078f41ea43b8af0f5e311e52a149';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr19a58a97a99e74e3f97ebf78eb74d11f7';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr1f85a407d24af4079866fd7b8d8281d31';


UPDATE title
SET entry_name='[Une foule attentive]'
WHERE title.id_uuid = 'qr12b12c351db9f4a01bead185582f75717';


UPDATE title
SET entry_name='[Chanteurs]'
WHERE title.id_uuid = 'qr11a9c261f077f4a4882b8b7e431f5bc66';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr1589d456d5d3e4b649a8ea65f9107a5e2';


UPDATE title
SET entry_name='[Duel de soldats]'
WHERE title.id_uuid = 'qr14a451ad2f1f94ebd84a12e0698a7ff8f';


UPDATE title
SET entry_name='[Danseuses sur scène]'
WHERE title.id_uuid = 'qr1c70ec210d09f4253895884fd2c333c55';


UPDATE title
SET entry_name='[Femme souriante]'
WHERE title.id_uuid = 'qr182aa3e95d0284aad958d59275ae5c5b8';


UPDATE title
SET entry_name='[Groupe de femmes]'
WHERE title.id_uuid = 'qr124e10e78027b4b4b8d1820f812fcdc6d';


UPDATE title
SET entry_name='[Deux clowns]'
WHERE title.id_uuid = 'qr1c5e7adc0aecb4c779c1704b420441c91';


UPDATE title
SET entry_name='[Autour de la table]'
WHERE title.id_uuid = 'qr1781089cb99da4f4598b4508566ee55b6';


UPDATE title
SET entry_name='[Les "comédiens" saluent]'
WHERE title.id_uuid = 'qr1c6d483d534424107bbee80f6eb4012d1';


UPDATE title
SET entry_name='[Les "comédiens" saluent]'
WHERE title.id_uuid = 'qr186107f7963424d0695759664161bb8e8';


UPDATE title
SET entry_name='[Musiciens]'
WHERE title.id_uuid = 'qr169abed22be8b47cf9243745bee77abf1';


UPDATE title
SET entry_name='[Femmes et poussette]'
WHERE title.id_uuid = 'qr13b3a3b1f89154d9eb48b0c11e586371f';


UPDATE title
SET entry_name='[Autour de la table]'
WHERE title.id_uuid = 'qr1064fa84155614f04b13b276331af4ee8';


UPDATE title
SET entry_name='[Enfants dans la foule]'
WHERE title.id_uuid = 'qr13360f774d11f48a0acaf2796cd460c09';


UPDATE title
SET entry_name='[Un lutteur au repos]'
WHERE title.id_uuid = 'qr1814a495c2d894119bd6710492e279885';


UPDATE title
SET entry_name='[Les "comédiens" saluent]'
WHERE title.id_uuid = 'qr122d603ad1b16469f9d664043dee4efa1';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr1fef004b900c24226bdba14be5d60e032';


UPDATE title
SET entry_name='[Un couple sur scène]'
WHERE title.id_uuid = 'qr1d02c3d3a6f9742699131a2afcb7277f3';


UPDATE title
SET entry_name='[Deux clowns]'
WHERE title.id_uuid = 'qr1ad04341399df4061ab9280ceced9cf1d';


UPDATE title
SET entry_name='[Une foule attentive]'
WHERE title.id_uuid = 'qr1ff73a6c912ea4ab7b82d2fc7b5dccbc3';


UPDATE title
SET entry_name='[Bateleur]'
WHERE title.id_uuid = 'qr16a46290af7ec4fe883bd4ce1482a19bc';


UPDATE title
SET entry_name='[Un lutteur au repos]'
WHERE title.id_uuid = 'qr1f3ac63177b874a76b227e760c272b775';


UPDATE title
SET entry_name='[Un bateleur]'
WHERE title.id_uuid = 'qr1f196bb6f15984cc7bf39cc626e74d970';


UPDATE title
SET entry_name='[Statue dans le jardin]'
WHERE title.id_uuid = 'qr13743d2c7e5b74486a190263e0dbbbf2a';


UPDATE title
SET entry_name='[Duel de soldats]'
WHERE title.id_uuid = 'qr1713611cd1ac942f1800568635629bc98';


UPDATE title
SET entry_name='[Autour de la table]'
WHERE title.id_uuid = 'qr1de327f5cfd85495fbfba737257cf9305';


UPDATE title
SET entry_name='[Musiciens]'
WHERE title.id_uuid = 'qr1e12964a0dfb240d68c2be60bb3591b43';


UPDATE title
SET entry_name='[Un numéro]'
WHERE title.id_uuid = 'qr1d9a6656f31214e1ba6ad1e776058c3d8';


UPDATE title
SET entry_name='[Homme dans une tour de château fort]'
WHERE title.id_uuid = 'qr1c874184e13c14caaa1d6132648877ecf';


UPDATE title
SET entry_name='20 rue de Valois'
WHERE title.id_uuid = 'qr17c6d2ce0dc9c46298a86936b827845b0';


UPDATE title
SET entry_name='[Filles autour de la partition]'
WHERE title.id_uuid = 'qr1f18001cc4acc464d9edf78996564d6cd';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr1812238df04a84b5db529055a006846bb';


UPDATE title
SET entry_name='[Filles autour de la partition]'
WHERE title.id_uuid = 'qr1b956558890794fb0afb3b8639ce3be13';


UPDATE title
SET entry_name='[Enfants autour du bassin]'
WHERE title.id_uuid = 'qr1d93473adf39e43c180230bc259980508';


UPDATE title
SET entry_name='[Homme et chien sur scène]'
WHERE title.id_uuid = 'qr187ddc2bab64249f6b265df03cbef7043';


UPDATE title
SET entry_name='[Un numéro devant une foule attentive]'
WHERE title.id_uuid = 'qr1d2e7d1c7c6a0486db33f9cd9d7661f60';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr16dff1e36e2234648bcd6b26009498af4';


UPDATE title
SET entry_name='[Fille lisant une partition]'
WHERE title.id_uuid = 'qr15e042c3ce50c43f6aa60d55a4a1d8754';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr19718b1fbc3d64ce98f11b3505061f68b';


UPDATE title
SET entry_name='[Homme dans une tour de château fort]'
WHERE title.id_uuid = 'qr1bb455d45d122415c881049eb84890ee2';


UPDATE title
SET entry_name='[Un soldat un peu sourd]'
WHERE title.id_uuid = 'qr123e349da44d640b081208751869bc460';


UPDATE title
SET entry_name='[Enfants autour du bassin]'
WHERE title.id_uuid = 'qr1d66c907b998546ad891fc6c6aa0e6f04';


UPDATE title
SET entry_name='[Prise d''une photographie]'
WHERE title.id_uuid = 'qr15b67e644461e43e5b98f00b94e1883b4';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr16bb38e99ac70459f979e5baff7216e34';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr1ba9e5c1992c34aa48e37b9d987986748';


UPDATE title
SET entry_name='[Tickets à l''entrée]'
WHERE title.id_uuid = 'qr17931b8ee3181417488d9b8dfa7cdbe57';


UPDATE title
SET entry_name='[Femme lisant les lignes de la main]'
WHERE title.id_uuid = 'qr1098d1d7813684617a0b84efb022c241e';


UPDATE title
SET entry_name='[La foule observe un numéro]'
WHERE title.id_uuid = 'qr1ff539f3eb0ab4d62b6457fb22e33efc3';


UPDATE title
SET entry_name='Pl. 13, Vue du jardin du Palais royal'
WHERE title.id_uuid = 'qr14f5c54d968e14036a37b20aa438c38f6';


UPDATE title
SET entry_name='France panoramique, Paris : 25, Palais-Royal, Conseil d''Etat'
WHERE title.id_uuid = 'qr1290ba1eaa06e47ceb65e172ce1814f82';


UPDATE title
SET entry_name='France panoramique, Paris : 50, Palais royal, les jardins'
WHERE title.id_uuid = 'qr16565a8ed9a77479ea2b00d15032983c3';


UPDATE title
SET entry_name='Vue du Palais-Royal'
WHERE title.id_uuid = 'qr174483ab76c7544dfb29867f245f9e9d1';


UPDATE title
SET entry_name='[Vues de Paris et de la France]. F. 47. Arc de triomphe du Carrousel. Palais royal'
WHERE title.id_uuid = 'qr16d153c0627c34a64ab76dc9af9f049e1';


UPDATE title
SET entry_name='[Vues de Paris et de la France]. F. 48. Palais royal. Louvre'
WHERE title.id_uuid = 'qr150aab9932e4f40bdac8ab1921b7ace41';


UPDATE title
SET entry_name='Station du Palais Royal. Construction des accès'
WHERE title.id_uuid = 'qr1dde8642257374ed9aec088f87ca5cdae';


UPDATE title
SET entry_name='Fol. 6. "Le Premier pas d''un jeune officier cosaque au Palais royal"'
WHERE title.id_uuid = 'qr16cff9467c88b407199260e68d3347cfd';


UPDATE title
SET entry_name='Durand del. Janinet sculp. N° 39 [Paris], Vue du Palais-Royal côté de l''entrée'
WHERE title.id_uuid = 'qr1eb43e735d36b4a6596e4feba74b1ee84';


UPDATE title
SET entry_name='Durand del. Janinet sculp. N° 16 [Paris], Vue du palais Royal'
WHERE title.id_uuid = 'qr1166cd69c0c5046a19c91ac41686edded';


UPDATE title
SET entry_name='Durand del., Janinet sculp., n° 14 [Paris], Vue du palais Royal prise du jardin'
WHERE title.id_uuid = 'qr1c90f4ad6bcad447ebdee7f961945305c';


UPDATE title
SET entry_name='Durand del., Janinet sculp., n° 15 [Paris], Vue du palais Royal prise du côté de l''entrée'
WHERE title.id_uuid = 'qr1efdaf0b2b6c345e39bc97151aaca09c9';


UPDATE title
SET entry_name='82. [Paris], 2me vue du jardin du Palais-Royal, prise des galeries de bois / Courvoisier del. Aubert fils sculpsit.'
WHERE title.id_uuid = 'qr18a5669432e4c4f89a75fbf0e8dc8f3e4';


UPDATE title
SET entry_name='69. [Paris], 1re Vue du jardin du Palais-Royal, prise de la rotonde / Dessiné par Courvoisier. Gravé par Eugène Aubert.'
WHERE title.id_uuid = 'qr1452990efad7842b189a99c4178b55539';


UPDATE title
SET entry_name='67. Paris],Vue du Palais-Royal et du château d''eau, prise de la place de ce palais / Dessiné par Courvoisier. Gravé par Blanchard aîné père.'
WHERE title.id_uuid = 'qr1a2b6d986126c46498dde0e0574cdac49';


UPDATE title
SET entry_name='Sergent del. Le Campion sculp. N° 81. [Paris], Vue du jardin du Palais-Royal, avec le nouveau cirque'
WHERE title.id_uuid = 'qr1b54edc03bba44331b26deda4da06aa94';


UPDATE title
SET entry_name='Testard del. Roger sculp. N° 27. [Paris], Vue de l''intérieur du Palais-Royal'
WHERE title.id_uuid = 'qr127bf778c53cd4cf48e4a10cda8122905';


UPDATE title
SET entry_name='Testard del. J-A. Le Campion sculp. N° 32. [Paris], Vue du Palais-Royal : construit sur les desseins de Jacques Mercier'
WHERE title.id_uuid = 'qr1216df5549b5b4ae080f167b0f377c139';


UPDATE title
SET entry_name='Testard del. Roger scul.p. : N° 27 [Paris], Vue de l''intérieur du Palais-Royal'
WHERE title.id_uuid = 'qr126e6a7a26d0a465bb89a204a7986d352';


UPDATE title
SET entry_name='Sergent del. Le Campion scul.p. : N° 81 [Paris], Vue du jardin du Palais-Royal, avec le nouveau cirque'
WHERE title.id_uuid = 'qr17923a63a4b2840b3b0cd703f76bcf764';


UPDATE title
SET entry_name='Testard del. J-A. Le Campion scul.p. : N° 32 [Paris], Vue du Palais-Royal : construit sur les desseins de Jacques Mercier'
WHERE title.id_uuid = 'qr13d6615e6d6c74659a62c53c163e315b2';


UPDATE title
SET entry_name='[4]. [Paris], Les nouveaux bâtimens du Palais-Royal. / Durand del. Janninet scul.'
WHERE title.id_uuid = 'qr163936b9731924789bd1992d8d9cdbe41';


UPDATE title
SET entry_name='Testard del. Guyot sculp. N° 21 [Paris], Vue du portail de Saint-Gervais. Construit sur les desseins de Jacques Desbrosses architecteTestard del. J-A. Le Campion sculp. N° 32 [Paris], Vue du Palais-Royal. construit sur les desseins de Jacques Mercier'
WHERE title.id_uuid = 'qr1939769b0bbc5473ca0f0b1b9fcd9cc41';


UPDATE title
SET entry_name='Testard del. Guyot sculp. N° 21 [Paris], Vue du portail de Saint-Gervais. Construit sur les desseins de Jacques Desbrosses architecteTestard del. J-A. Le Campion sculp. N° 32 [Paris], Vue du Palais-Royal. construit sur les desseins de Jacques Mercier'
WHERE title.id_uuid = 'qr1828eaf29970548d9932471be542682dd';


UPDATE title
SET entry_name='4e arrondissement. Vue prise au-dessus du pont des Arts'
WHERE title.id_uuid = 'qr17c16fd4503d64130b1b2508ca6eecdf8';


UPDATE title
SET entry_name='240 : Station du Palais Royal. Construction des accès. Vue prise de l''intérieur de la station'
WHERE title.id_uuid = 'qr11381b8118da14d419a70b111bf09cf76';


UPDATE title
SET entry_name='241 : Station du Palais Royal. Construction des accès. Vue prise de l''intérieur du souterrain à deux voies'
WHERE title.id_uuid = 'qr1fdfa95eef87b4a96993065173d20ca30';


UPDATE title
SET entry_name='259 : Station du Palais Royal. Construction des accès'
WHERE title.id_uuid = 'qr1d13efdbf352b4423a1efaaa7dba834b4';


UPDATE title
SET entry_name='236 : Station du Palais Royal. Construction des accès'
WHERE title.id_uuid = 'qr17007b449ca9c4339b355e1238221fca4';


UPDATE title
SET entry_name='179 : Station du Palais Royal. Ancien égout Rivoli'
WHERE title.id_uuid = 'qr1d19a67a88a3c457684b758e0a2b1b61d';


UPDATE title
SET entry_name='180 : Station du Palais-Royal, ancien égout Rivoli'
WHERE title.id_uuid = 'qr1f8417311be4b4a51b2d18ba50d9821c9';


UPDATE title
SET entry_name='253 : Station du Palais Royal, construction des accès'
WHERE title.id_uuid = 'qr15c94060b79ba4738b423d25b37294690';


UPDATE title
SET entry_name='Station du Palais Royal... ; Place de la Concorde… ; Station des champs-Elysées… ; Rue de Lyon…'
WHERE title.id_uuid = 'qr1f1c991575aef419aa9a452582c0dcc2c';


UPDATE title
SET entry_name='f. 25v-26r [Notes de l''artiste] ; Fontaine, Jardin du Palais royal. Paris 8 juin 1853.'
WHERE title.id_uuid = 'qr1b478c5ad245b429381265f051810e291';


UPDATE title
SET entry_name='Le Palais royal, incendie du 24 mai 1871'
WHERE title.id_uuid = 'qr16f9a3b7656124d71b1bf2976ad7cc2e4';


UPDATE title
SET entry_name='Les Ruines de Paris et de ses environs, 1870-1871. N° 14 : le Palais royal incendié, vue extérieure'
WHERE title.id_uuid = 'qr156a704958a1f44bd986b355aafa2b1ed';


UPDATE title
SET entry_name='Restaurant Henri IV - 160 galerie de Valois'
WHERE title.id_uuid = 'qr1b13818ac2fb94d19abe2e680b3505622';


UPDATE title
SET entry_name='[Maquette réalisée par le photographe avant impression du livre]. Paris. Photographies de Jean Roubier, préface de Georges Duhamel. P. 40. Le Palais-Royal : façade principale de la Cour d''honneur'
WHERE title.id_uuid = 'qr171c100c7bd6b4e3485cb392e83453381';


UPDATE title
SET entry_name='Commande C1. 343 : Palais-Royal. Chambre à coucher de la Princesse Clotilde. Table à ouvrage'
WHERE title.id_uuid = 'qr1d3c9ff08fbd448b9a3cf1b228f3da609';


UPDATE title
SET entry_name='Commande C1. 370 : Palais-Royal. Chaise'
WHERE title.id_uuid = 'qr1db3d788e347a4a6cba8065ce74cb97e8';


UPDATE title
SET entry_name='Commande C1. 345 : Palais-Royal. Salon de la Princesse Clotilde. Meuble d''angle'
WHERE title.id_uuid = 'qr1c78049dc3e484cf5bd4f584dc6061eec';


UPDATE title
SET entry_name='Commande C1. 341 : Palais-Royal. Chambre à coucher de la Princesse Clotilde. Table à ouvrage'
WHERE title.id_uuid = 'qr1c0a3f97b9e2c42e890586861a7251d3f';


UPDATE title
SET entry_name='Commande C1. 344 : Palais-Royal. Salon de la Princesse Clotilde. Tables gigogne'
WHERE title.id_uuid = 'qr1629bad08629849b6bada2d16b53c0ba7';


UPDATE title
SET entry_name='Commande C1. 347 : Palais-Royal. Salon de la Princesse Clotilde. Chaise'
WHERE title.id_uuid = 'qr1422121088b4c46ceb58100bac5966498';


UPDATE title
SET entry_name='Commande C1. 340 : Palais-Royal. Chambre à coucher de la Princesse Clotilde. Table à ouvrage'
WHERE title.id_uuid = 'qr12e258c95148a42048177c936eb6304f0';


UPDATE title
SET entry_name='Commande C1. 346 : Palais-Royal. Salon de la Princesse Clotilde. Jardinière'
WHERE title.id_uuid = 'qr1fb6e18bd609e4fa3a3c6b5a028d90fa1';


UPDATE title
SET entry_name='Commande C1. 342 : Palais-Royal. Chambre à coucher de la Princesse Clotilde. Table à ouvrage'
WHERE title.id_uuid = 'qr1d040e153e639467ab5c571966da359dd';


UPDATE title
SET entry_name='Commande C1. 137 : Palais-Royal. Salon de la Princesse Clotilde. Ecran'
WHERE title.id_uuid = 'qr1a377d2ab9f5b41e199421dab33b7578a';


UPDATE title
SET entry_name='Commande C1. 139 : Palais-Royal. Salon de la Princesse Clotilde. Meuble à deux corps'
WHERE title.id_uuid = 'qr1e1f8f3dc7b844063a2813e94edac95cd';


UPDATE title
SET entry_name='Commande C1. 133 : Palais-Royal. Salon de la Princesse Clotilde. Chaise'
WHERE title.id_uuid = 'qr1e73a0e27880246e3aa720c061d9aa8e6';


UPDATE title
SET entry_name='Commande C1. 127 : Palais-Royal. Chambre de la Princesse Clotilde. Causeuse'
WHERE title.id_uuid = 'qr1ca55f24aa4604d3c91843d28f58e8b2c';


UPDATE title
SET entry_name='Commande C1. 123 : Palais-Royal. Chambre de la Princesse Clotilde. Ecran'
WHERE title.id_uuid = 'qr1b6b1115b0f9b4d478c0aacf568399f7b';


UPDATE title
SET entry_name='Commande C1. 131 : Palais-Royal. Salon de la Princesse Clotilde. Causeuse'
WHERE title.id_uuid = 'qr178e2b4caf0144b749a63dfa84eecb149';


UPDATE title
SET entry_name='Commande C1. 129 : Palais-Royal. Chambre de la Princesse Clotilde. Psyché'
WHERE title.id_uuid = 'qr13e773162f317466387ec1017748a5689';


UPDATE title
SET entry_name='Commande C1. 142 : Palais-Royal. Salon de la Princesse Clotilde. Table ronde'
WHERE title.id_uuid = 'qr1ea07c87cdbc940ceb8e5c8387c22009f';


UPDATE title
SET entry_name='Commande C1. 132 : Palais-Royal. Salon de la Princesse Clotilde. Confortable [fauteuil]'
WHERE title.id_uuid = 'qr17783107d263c482bb9232d7a7d7937c3';


UPDATE title
SET entry_name='Commande C1. 126 : Palais-Royal. Chambre de la Princesse Clotilde. Chaise longue'
WHERE title.id_uuid = 'qr1ac2996ad2da9441bb4767cbd79cf3f5a';


UPDATE title
SET entry_name='Commande C1. 122 : Palais-Royal. Chambre de la Princesse Clotilde. Guéridon'
WHERE title.id_uuid = 'qr15f1736f74f994781821c8314c63ba180';


UPDATE title
SET entry_name='Commande C1. 140 : Palais-Royal. Salon de la Princesse Clotilde. Bureau'
WHERE title.id_uuid = 'qr1eb5325dd05a04e7b935c4a7cc6cb8e43';


UPDATE title
SET entry_name='Commande C1. 119 : Palais-Royal. Chambre de la Princesse Clotilde. Baldaquin'
WHERE title.id_uuid = 'qr1be4b84856b0d4e52bf3893a06ccc361e';


UPDATE title
SET entry_name='Commande C1. 125 : Palais-Royal. Chambre de la Princesse Clotilde. [Confortable] chaise'
WHERE title.id_uuid = 'qr17e626dec06b84f40bf160bcc4a023370';


UPDATE title
SET entry_name='Commande C1. 121 : Palais-Royal. Chambre de la Princesse Clotilde. Table de nuit'
WHERE title.id_uuid = 'qr1bcc53762a90541adbbc4173a31c96d3c';


UPDATE title
SET entry_name='Commande C1. 128 : Palais-Royal. Chambre de la Princesse Clotilde. Tabouret'
WHERE title.id_uuid = 'qr1556da9e4af8f434bbac7880587c0acbb';


UPDATE title
SET entry_name='Commande C1. 124 : Palais-Royal. Chambre de la Princesse Clotilde. Confortable [fauteuil]'
WHERE title.id_uuid = 'qr1c5d1e5e918584f9ca813d4632be62b24';


UPDATE title
SET entry_name='Commande C1. 120 : Palais-Royal. Chambre de la Princesse Clotilde. Commode'
WHERE title.id_uuid = 'qr19eb94fb070d042b59649ff0c0c7b4df6';


UPDATE title
SET entry_name='Commande C1. 138 : Palais-Royal. Salon de la Princesse Clotilde. Console'
WHERE title.id_uuid = 'qr1d108c262c2984ebcbf541aaa2a3c269a';


UPDATE title
SET entry_name='Commande C1. 134 : Palais-Royal. Salon de la Princesse Clotilde. Fauteuil de bureau'
WHERE title.id_uuid = 'qr11cb08fc7e2e44084868719997527d3b4';


UPDATE title
SET entry_name='Commande C1. 130 : Palais-Royal. Salon de la Princesse Clotilde. Divan'
WHERE title.id_uuid = 'qr1af36e7dff79a4d61af4f3dff24fd8454';


UPDATE title
SET entry_name='Commande C1. 141 : Palais-Royal. Salon de la Princesse Clotilde. Table de canapé'
WHERE title.id_uuid = 'qr1e831a41e3b19404bafd829ceb01a9a5c';


UPDATE title
SET entry_name='Commande C1. 118 : Palais-Royal. Chambre de la Princesse Clotilde. Lit'
WHERE title.id_uuid = 'qr12165849e4d604209a8240ba905e1dbe4';


UPDATE title
SET entry_name='Commande C1. 136 : Palais-Royal. Salon de la Princesse Clotilde. Tabouret de pied'
WHERE title.id_uuid = 'qr1dfde44ae75d745139bb4d5361c1488a5';


UPDATE title
SET entry_name='Commande C1. 148 : Palais-Royal. Salon de la Princesse Clotilde. Miroir'
WHERE title.id_uuid = 'qr10743e21092f14bba985ceebafad0c017';


UPDATE title
SET entry_name='1 L''ambassadeur [texte d''Eugène Scribe, Mélesville].'
WHERE title.id_uuid = 'qr1e06f320d35784c6baf7c28d229a21cb9';


UPDATE title
SET entry_name='2 Les beignets à la cour [texte de Benjamin Antier, rôle de Louis XV, créé en mars 1835].'
WHERE title.id_uuid = 'qr1ccbbfdb8cbd54a07b866bd0e5878492f';


UPDATE title
SET entry_name='3 Capitaine Charlotte, second acte [comédie-vaudeville en deux actes, par Bayard et Dumanoir, créée en décembre 1841, rôle titre].'
WHERE title.id_uuid = 'qr1de30418f254e460e90349ef96d8f238d';


UPDATE title
SET entry_name='4 tCapitaine Charlotte, 3e costume [comédie-vaudeville en deux actes, par Bayard et Dumanoir, créée en décembre 1841, rôle titre].'
WHERE title.id_uuid = 'qr14ecb0177621d414c97b449a0593906e4';


UPDATE title
SET entry_name='5 Carlo et Carlin, 1er costume [comédie en deux actes, mêlée de chant, par Mélesville et Dumanoir, rôle de Carlo Bertinazzi, créée le 28 février 1844].'
WHERE title.id_uuid = 'qr172a4e763f9934ce48ed4c925a211c0cc';


UPDATE title
SET entry_name='6 Les Chansons de Béranger, 1er costume [ Le Tailleur et la Fée, ou les Chansons de Béranger, conte fantastique, mêlé de couplets, par Vanderburch, Ferdinand Langlé et De Forges, créé le 3 aout 1831, rôles de la Fée et de la Danseuse].'
WHERE title.id_uuid = 'qr14c0d7e2173c0401d9e7494db69e7d51f';


UPDATE title
SET entry_name='7 Les Chansons de Béranger, 3e costume [ Le Tailleur et la Fée, ou les Chansons de Béranger, conte fantastique, mêlé de couplets, par Vanderburch, Ferdinand Langlé et De Forges, créé le 3 aout 1831, rôles de la Fée et de la Danseuse].'
WHERE title.id_uuid = 'qr15b29f598f7fe405f8f2ade0253c146d9';


UPDATE title
SET entry_name='8 Les Chansons de Béranger, 4e costume [ Le Tailleur et la Fée, ou les Chansons de Béranger, conte fantastique, mêlé de couplets, par Vanderburch, Ferdinand Langlé et De Forges, créé le 3 aout 1831, rôles de la Fée et de la Danseuse].'
WHERE title.id_uuid = 'qr18da13ccc5c4a46dfa79a3be768e5f021';


UPDATE title
SET entry_name='9 Les Chants de Béranger [souvenir en trois tableaux, par Clairville et Lambert-Thibous, créé le 17 octobre 1857, rôles de Roger-Bontemps et de la Mère Toby].'
WHERE title.id_uuid = 'qr170390edbc2ae4780938a0c1b8fe1deba';


UPDATE title
SET entry_name='10 Chansons de Desaugiers, 1er acte, 1er costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr182f5ca90b8274d4c9c8e766ec87e9f29';


UPDATE title
SET entry_name='11 Chansons de Desaugiers, 2nd costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr12bbc837a669d4afc8d18f3ef284c68a1';


UPDATE title
SET entry_name='12 Chansons de Desaugiers, 3e costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr1b58d2c6d40eb491a8c6256bddc844ac6';


UPDATE title
SET entry_name='13 Chansons de Desaugiers, 4e costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr17a89b425148d41a6904b6e98f7cd8826';


UPDATE title
SET entry_name='14 Chansons de Desaugiers, 5e costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr1c2edfcee244940c585ef827b1929997b';


UPDATE title
SET entry_name='15 Chansons de Desaugiers, 6e costume [Les Chansons de Désaugiers, comédie en cinq actes, mêlée de couplets, par ThéauIon, Chazet et Frédéric de Courcy, créée le 9 février 1836, rôles de Cadet-Buteux, Mme Denis, Rosine, Margot, la Duchesse, le Franc-vaurien, et la Gaudriole].'
WHERE title.id_uuid = 'qr16d5bb7ff70434fb59f75fec67d87d8b9';


UPDATE title
SET entry_name='16 Mlle de Choisy, 1er costume [Mademoiselle de Choisy , comédie-vaudeville en deux actes, par H. de Saint-Georges et Bernard Lopez, créée le 3 avril 1848, rôle-titre].'
WHERE title.id_uuid = 'qr1a6b45d54b0634e2180c298c06219fb46';


UPDATE title
SET entry_name='17 Colombine, 1er costume [Colombine, ou les Sept péchés capitaux,comédie-vaudeville en un acte, par Carmouche et Eugène Guinot, créée le 12 mars 1850, rôle de Thérésa Baletti].'
WHERE title.id_uuid = 'qr19336f62e272f47669060eca4368a473a';


UPDATE title
SET entry_name='18 Colombine, 2e costume [Colombine, ou les Sept péchés capitaux,comédie-vaudeville en un acte, par Carmouche et Eugène Guinot, créée le 12 mars 1850, rôle de Thérésa Baletti].'
WHERE title.id_uuid = 'qr136ca98b5c2e841a1b60e1f4d8cfa9e48';


UPDATE title
SET entry_name='19 Colombine, 3e costume [Colombine, ou les Sept péchés capitaux,comédie-vaudeville en un acte, par Carmouche et Eugène Guinot, créée le 12 mars 1850, rôle de Thérésa Baletti].'
WHERE title.id_uuid = 'qr1095555f0bfa64d4587d644e5792fb73f';


UPDATE title
SET entry_name='20 Colombine, 4e costume [Colombine, ou les Sept péchés capitaux,comédie-vaudeville en un acte, par Carmouche et Eugène Guinot, créée le 12 mars 1850, rôle de Thérésa Baletti].'
WHERE title.id_uuid = 'qr1945eccfe08534836a8970feaf36c3a78';


UPDATE title
SET entry_name='21 Colombine, 5e costume [Colombine, ou les Sept péchés capitaux,comédie-vaudeville en un acte, par Carmouche et Eugène Guinot, créée le 12 mars 1850, rôle de Thérésa Baletti].'
WHERE title.id_uuid = 'qr139c8131d1c6144c8a021d3c3769d085c';


UPDATE title
SET entry_name='22 Comtesse du Tonneau, 2me acte [ La Comtesse du Tonneau, ou les Deux Coutsines, comédie-vaudeville en deux actes, par Théaulon et Chazet, créée le 13 avril 1837, rôle de Jeanneton]'
WHERE title.id_uuid = 'qr1e4c88bec2865427083a126cda6a3176d';


UPDATE title
SET entry_name='23 Comtesse du Tonneau, 2me acte [ La Comtesse du Tonneau, ou les Deux Coutsines, comédie-vaudeville en deux actes, par Théaulon et Chazet, créée le 13 avril 1837, rôle de Jeanneton].'
WHERE title.id_uuid = 'qr1f20b9112fbed4033948694ceb63ba2ee';


UPDATE title
SET entry_name='24 Un conte de fée, 1er acte, 1er costume [Un Conte de fées, comédie en trois actes, mêlée de chant, par De Leuven, Brunswick et Alexandre Dumas, créée le 29 avril 1845, rôle de la Marquise de Villani].'
WHERE title.id_uuid = 'qr1da8431f7997f48f9a0aa7a10f951da15';


UPDATE title
SET entry_name='25 Un conte de fée, 2nd acte, second costume [Un Conte de fées, comédie en trois actes, mêlée de chant, par De Leuven, Brunswick et Alexandre Dumas, créée le 29 avril 1845, rôle de la Marquise de Villani].'
WHERE title.id_uuid = 'qr1b627b997fbbc4a06925ca9d2ed407130';


UPDATE title
SET entry_name='26 Un conte de fée, 3e acte, 4ème costume [Un Conte de fées, comédie en trois actes, mêlée de chant, par De Leuven, Brunswick et Alexandre Dumas, créée le 29 avril 1845, rôle de la Marquise de Villani].'
WHERE title.id_uuid = 'qr1491d8c942fb5481e85389ffb3734e5e7';


UPDATE title
SET entry_name='27 La croix d''or [comédie en deux actes, mêlée de chants, par De Rougemont et Charles Dupeuty, créée le 2 mai 1835, rôle de Christine].'
WHERE title.id_uuid = 'qr1fdfbbf709702448284a7caba4d8c9b69';


UPDATE title
SET entry_name='28 Dangeville, 1er costume [Mademoiselle Dangeville, comédie en un acte, mêlée de chant, par De Villeneuve et Ch. de Livry créée le 10 avril 1838, rôles de Mlle Dangeville, Jacquot, la Marquise de Nesle et Tching-Ka].'
WHERE title.id_uuid = 'qr1a93e932f359f4101b4c55485571d8d88';


UPDATE title
SET entry_name='29 Mlle Dangeville, 2e costume [Mademoiselle Dangeville, comédie en un acte, mêlée de chant, par De Villeneuve et Ch. de Livry créée le 10 avril 1838, rôles de Mlle Dangeville, Jacquot, la Marquise de Nesle et Tching-Ka].'
WHERE title.id_uuid = 'qr13896a1df0d16494286cf2cdf3d45900e';


UPDATE title
SET entry_name='30 Mlle Dangeville, 3e costume [Mademoiselle Dangeville, comédie en un acte, mêlée de chant, par De Villeneuve et Ch. de Livry créée le 10 avril 1838, rôles de Mlle Dangeville, Jacquot, la Marquise de Nesle et Tching-Ka].'
WHERE title.id_uuid = 'qr1e086c6d9119347ef987fd704b9263b8d';


UPDATE title
SET entry_name='31 Mlle Dangeville, 4e costume [Mademoiselle Dangeville, comédie en un acte, mêlée de chant, par De Villeneuve et Ch. de Livry créée le 10 avril 1838, rôles de Mlle Dangeville, Jacquot, la Marquise de Nesle et Tching-Ka]'
WHERE title.id_uuid = 'qr17c26add07194476588bab5aebac71dbe';


UPDATE title
SET entry_name='32 Dejazet au sérail, 1er costume [Mademoiselle Déjazet au sérail, ou le Palais-Royal en 1872, vaudeville en un acte, par Bayard et De Lurieu, créé le 28 mars 1843, rôle de Mlle Déjazet].'
WHERE title.id_uuid = 'qr1af333692274c4ac98e1a8776e048a75e';


UPDATE title
SET entry_name='33 Dejazet au sérail, 2e costume [Mademoiselle Déjazet au sérail, ou le Palais-Royal en 1872, vaudeville en un acte, par Bayard et De Lurieu, créé le 28 mars 1843, rôle de MlJe Déjazet].'
WHERE title.id_uuid = 'qr1732530aebf34498a9798cbae7ca9964b';


UPDATE title
SET entry_name='34 2 âmes, 1er costume [pièce non identifiée].'
WHERE title.id_uuid = 'qr1820e3015c9d243b4b897d13c2552045a';


UPDATE title
SET entry_name='35 Les 2 âmes, second costume [pièce non identifiée].'
WHERE title.id_uuid = 'qr186462ca2eb504366afaaebb51c35af97';


UPDATE title
SET entry_name='36 Les deux pigeons, 1er costume [Les Deux Pigeons, comédie-vaudeville en quatre actes, par Xavier Saintine et Michel Masson, créée le 5 juin 1838, rôle d''Emmanuel].'
WHERE title.id_uuid = 'qr18209fc02df3049b8a84ce67ae18a1135';


UPDATE title
SET entry_name='37 Les deux pigeons, 2e costume [Les Deux Pigeons, comédie-vaudeville en quatre actes, par Xavier Saintine et Michel Masson, créée le 5 juin 1838, rôle d''Emmanuel].'
WHERE title.id_uuid = 'qr10d3cfee886344fff9699c48139b788ce';


UPDATE title
SET entry_name='38 Les deux pigeons, 3e costume [Les Deux Pigeons, comédie-vaudeville en quatre actes, par Xavier Saintine et Michel Masson, créée le 5 juin 1838, rôle d''Emmanuel].'
WHERE title.id_uuid = 'qr163a6ec76a13642e38ca8287f6965b67c';


UPDATE title
SET entry_name='39 Les deux pigeons, dernier costume [Les Deux Pigeons, comédie-vaudeville en quatre actes, par Xavier Saintine et Michel Masson, créée le 5 juin 1838, rôle d''Emmanuel].'
WHERE title.id_uuid = 'qr1e7362abf94db4755b48d3aaf99fe5169';


UPDATE title
SET entry_name='40 L''enfant de l''amour, 1er acte [L''Enfant de l''amour, ou les Deux Marquis de Saint-Jacques, comédie-vaudeville en trois actes, par Bayard et Eugène Guinot , créée le 20 mars 1847, rôle de Jacques]'
WHERE title.id_uuid = 'qr1e3284ad49e704d779a30ef33823327a6';


UPDATE title
SET entry_name='41 L''enfant de l''amour, second costume [L''Enfant de l''amour, ou les Deux Marquis de Saint-Jacques, comédie-vaudeville en trois actes, par Bayard et Eugène Guinot , créée le 20 mars 1847, rôle de Jacques]'
WHERE title.id_uuid = 'qr1e317823524a248c382d5db8f45751c08';


UPDATE title
SET entry_name='42 Mme Favart, 1er acte [Madame Favart, comédie en trois actes, mêlée de chants, par Xavier Saintine et Michel Masson, créée le 26 décembre 1836, rôle de Marie].'
WHERE title.id_uuid = 'qr1c288e0a13b5e42e19f366b43bf17d297';


UPDATE title
SET entry_name='43 Mme Favart, second acte, second costume [Madame Favart, comédie en trois actes, mêlée de chants, par Xavier Saintine et Michel Masson, créée le 26 décembre 1836, rôle de Marie]'
WHERE title.id_uuid = 'qr1b7e3f51baf2a45ffb167ce67f8c6e3b9';


UPDATE title
SET entry_name='44 Mme Favart, second acte, 3e costume [Madame Favart, comédie en trois actes, mêlée de chants, par Xavier Saintine et Michel Masson, créée le 26 décembre 1836, rôle de Marie].'
WHERE title.id_uuid = 'qr1a7159d820bd043289b4faaa7198c5ff6';


UPDATE title
SET entry_name='45 Mme Favart, 3e acte, 4e costume [Madame Favart, comédie en trois actes, mêlée de chants, par Xavier Saintine et Michel Masson, créée le 26 décembre 1836, rôle de Marie].'
WHERE title.id_uuid = 'qr16cc41c21b8f5446c96f0799b3427a73e';


UPDATE title
SET entry_name='46 La fille de Dominique, 1er costume [ La Fille de Dominique, comédie-vaudeville en un acte, par De Villeneuve et Charles de Livry, créée le 22 juin 1833, rôle de Catherine Biancolelli].'
WHERE title.id_uuid = 'qr1f3a893c66b974e5bb163cf399915a1c8';


UPDATE title
SET entry_name='47 La fille de Dominique, second costume [ La Fille de Dominique, comédie-vaudeville en un acte, par De Villeneuve et Charles de Livry, créée le 22 juin 1833, rôle de Catherine Biancolelli].'
WHERE title.id_uuid = 'qr1c55f6a994bc743618130ca836b0ecc10';


UPDATE title
SET entry_name='48 La fille de Dominique, 4e costume [ La Fille de Dominique, comédie-vaudeville en un acte, par De Villeneuve et Charles de Livry, créée le 22 juin 1833, rôle de Catherine Biancolelli].'
WHERE title.id_uuid = 'qr1d07b0477fec44a89957a6f752d37a84e';


UPDATE title
SET entry_name='49 Fils de l''homme [François, duc de Reichstadt, dans le Fils de l''Homme, souvenir de 1824, par Paul de Lussan (Eugène Sue et De Forges, créé le 28 décembre 1830, rôle-titre].'
WHERE title.id_uuid = 'qr140ee52227eca40848ffa8c4dfbd45baf';


UPDATE title
SET entry_name='50 La fiole, 2eme costume [Fiole de Cagliostro, vaudeville en un acte, par Anicet-Bourgeois, Dumanoir et Brisebarre, créé le 23 décembre 1835, rôles de la Baronne de Murville et de Suzanne de Murville, sa petite-fille]'
WHERE title.id_uuid = 'qr144359316034b4c868bf4f822ba24e9c8';


UPDATE title
SET entry_name='51 Frétillon, 1er acte [Frétillon, ou la Bonne fille, vaudeville en cinq actes, par Bayard et Decomberousse, créé le 13 décembre 1834, rôle de Camille, dit Frétillon].'
WHERE title.id_uuid = 'qr125e8ae9ef4a54d92a9313314bdfa1d41';


UPDATE title
SET entry_name='52 Frétillon, 2e acte [Frétillon, ou la Bonne fille, vaudeville en cinq actes, par Bayard et Decomberousse, créé le 13 décembre 1834, rôle de Camille, dit Frétillon].'
WHERE title.id_uuid = 'qr14ef82e757ffe45d494791f86eb213d30';


UPDATE title
SET entry_name='53 Frétillon, 3e acte [Frétillon, ou la Bonne fille, vaudeville en cinq actes, par Bayard et Decomberousse, créé le 13 décembre 1834, rôle de Camille, dit Frétillon].'
WHERE title.id_uuid = 'qr127f5010145ef4c919b66595618871af9';


UPDATE title
SET entry_name='54 Frétillon, 4e acte [Frétillon, ou la Bonne fille, vaudeville en cinq actes, par Bayard et Decomberousse, créé le 13 décembre 1834, rôle de Camille, dit Frétillon].'
WHERE title.id_uuid = 'qr1bd64fb4edf794b08b76e4479da57fad9';


UPDATE title
SET entry_name='55 Frétillon, 5e acte [Frétillon, ou la Bonne fille, vaudeville en cinq actes, par Bayard et Decomberousse, créé le 13 décembre 1834, rôle de Camille, dit Frétillon].'
WHERE title.id_uuid = 'qr1e9d329a1401a40caa33c4d8110ed1883';


UPDATE title
SET entry_name='56 tG. Bernard, 1er costume [Gentil-Bernard, ou L''art d''aimer, comédie en cinq actes, mêlée de couplets, par Dumanoir et Clairville, créée le 18 mars 1846, rôle-titre].'
WHERE title.id_uuid = 'qr1ec07e1ebe56b4e7fb97ada36919f9d87';


UPDATE title
SET entry_name='57 G. Bernard, 3e acte [Gentil-Bernard, ou L''art d''aimer, comédie en cinq actes, mêlée de couplets, par Dumanoir et Clairville, créée le 18 mars 1846, rôle-titre].'
WHERE title.id_uuid = 'qr15da8a6b1d79c483296eac3711aa3d6ea';


UPDATE title
SET entry_name='58 G. Bernard, 4e costume [Gentil-Bernard, ou L''art d''aimer, comédie en cinq actes, mêlée de couplets, par Dumanoir et Clairville, créée le 18 mars 1846, rôle-titre].'
WHERE title.id_uuid = 'qr19b5cd6ad2c3844a7a243305930faa161';


UPDATE title
SET entry_name='59. Jud. et [sic], costume du 1er acte [Judith et Holoplzcrlle, vaudeville en deux actes, par Théaulon, Nézel et Overnay, créé le 26 août 1834, rôle de Thérésina].'
WHERE title.id_uuid = 'qr1db84b443c2f741d58b83cb6a60540065';


UPDATE title
SET entry_name='60 ud. et [sic], costume du second acte [Judith et Holoplzcrlle, vaudeville en deux actes, par Théaulon, Nézel et Overnay, créé le 26 août 1834, rôle de Thérésina].'
WHERE title.id_uuid = 'qr10bd9529ba5274ce291213481af1d6c50';


UPDATE title
SET entry_name='61 Lauzun, 1er costume [Le Marquis de Lauzun, comédie en un acte, mêlée de couplets, par Carmouche et Eugène Guinot, créée le 17 janvier 1848, rôle-titre].'
WHERE title.id_uuid = 'qr18b15935b70f14d8a81d24f27f437779d';


UPDATE title
SET entry_name='62 Lauzun, second costume [Le Marquis de Lauzun, comédie en un acte, mêlée de couplets, par Carmouche et Eugène Guinot, créée le 17 janvier 1848, rôle-titre].'
WHERE title.id_uuid = 'qr14aef66a4fb7246948987ec85f6d96e0c';


UPDATE title
SET entry_name='63 Lauzun, 3me costume [Le Marquis de Lauzun, comédie en un acte, mêlée de couplets, par Carmouche et Eugène Guinot, créée le 17 janvier 1848, rôle-titre].'
WHERE title.id_uuid = 'qr149c9c81fbe894266aa6c0ba215f506dd';


UPDATE title
SET entry_name='64 Lauzun, 4e costume [Le Marquis de Lauzun, comédie en un acte, mêlée de couplets, par Carmouche et Eugène Guinot, créé le 17 janvier 1848, rôle-titre].'
WHERE title.id_uuid = 'qr11bff379e483e4f13a2cbc64a44adb87e';


UPDATE title
SET entry_name='65 Louis Douze [LEnfance de Louis XII, ou la Correction de nos pères, comédie-vaudeville en un acte, par Mélesville, Simonnin et Th . Nézel, créée le 10 décembre 1831, rôle du duc d''Orléans].'
WHERE title.id_uuid = 'qr1cb5998332ad24ad5ab3632efad8997b0';


UPDATE title
SET entry_name='66 Lulli, 1er acte [Lully, ou les Petits violons de Mademoiselle, comédie en deux actes, mêlée de chant, par Dumanoir et Clairville, créée le 14 janvier 1850, rôle-titre].'
WHERE title.id_uuid = 'qr187500f2f717b46b9a3ddeca7f47b0ed8';


UPDATE title
SET entry_name='67 Lulli, second acte [Lully, ou les Petits violons de Mademoiselle, comédie en deux actes, mêlée de chant, par Dumanoir et Clairville, créée le 14 janvier 1850, rôle-titre].'
WHERE title.id_uuid = 'qr13300c47505f4474cb2c1412645747b48';


UPDATE title
SET entry_name='68 Maîtresse de langue [La Maîtresse de langues, comédie en un acte, mêlée de chant, par De Saint-Georges, De Leuven et Dumanoir, créée le 21 février 1838,ôle de Léonide].'
WHERE title.id_uuid = 'qr19934c3d590864900a8b3ccee57fafaf0';


UPDATE title
SET entry_name='69 Marion De Lorme, second costume [Marion Carmélite, comédie-vaudeville en un acte, par Bayard et Dumanoir, créée le 19 octobre 1836, rôle-titre].'
WHERE title.id_uuid = 'qr18b2967de6f84435989f9d55e6728b9a5';


UPDATE title
SET entry_name='70. La marquise de Prétentaille, 1er costume [La Marquise de Prétint aille, comédie-vaudeville en un acte, par Bayard et Dumanoir, créée le 23 avril 1836, rôle-titre].'
WHERE title.id_uuid = 'qr1245bb7529e82444a89859fbcf9cda42f';


UPDATE title
SET entry_name='[Mlle Hady. Palais-Royal]'
WHERE title.id_uuid = 'qr18d4cefa14b0e4f599b180aab69153741';


UPDATE title
SET entry_name='71 La marquise de Prétentaille, 2me costume [La Marquise de Prétint aille, comédie-vaudeville en un acte, par Bayard et Dumanoir, créée le 23 avril 1836, rôle-titre].'
WHERE title.id_uuid = 'qr1233e52563f6849d9859c438530832b43';


UPDATE title
SET entry_name='72 Nanon, Ninon et Maintenant, 1er acte [Nanon, Ninon et Maintenons ou les Trois boudoirs, comédie en trois actes, mêlée de chants, par Théaulon, Dartois et Lesguillon, créée le 22 mars 1839, rôle de Nanon].'
WHERE title.id_uuid = 'qr19d4805136155411fac76c13f83e79738';


UPDATE title
SET entry_name='73 Nanon, second costume [Nanon, Ninon et Maintenons ou les Trois boudoirs, comédie en trois actes, mêlée de chants, par Théaulon, Dartois et Lesguillon, créée le 22 mars 1839, rôle de Nanon].'
WHERE title.id_uuid = 'qr12637a4e6ba2f484a9c714b83753629fc';


UPDATE title
SET entry_name='74 Périchole, 1er costume [La Périchole, comédie en un acte, mêlée de chant, par Théaulon et De Forges, créée le 21 octobre 1835, rôle-titre].'
WHERE title.id_uuid = 'qr1df9d64d2baeb410083507974d7605237';


UPDATE title
SET entry_name='75 Périchole, 2e costume, la religieuse [La Périchole, comédie en un acte, mêlée de chant, par Théaulon et De Forges, créée le 21 octobre 1835, rôle-titre].'
WHERE title.id_uuid = 'qr1b196f5d1625c41a0ae2f60392213726e';


UPDATE title
SET entry_name='76 Périchole, 3e costume [La Périchole, comédie en un acte, mêlée de chant, par Théaulon et De Forges, créée le 21 octobre 1835, rôle-titre].'
WHERE title.id_uuid = 'qr17662861200f54fdba1fbfd4134a42ea5';


UPDATE title
SET entry_name='77 Le philtre [Le Philtre champenois, comédie-vaudeville en un acte, par Mélesville et Brazier, créée le 19 juillet 1831, rôle de Catherine].'
WHERE title.id_uuid = 'qr19c94440731bc49b5a4a8f7b2a4712f34';


UPDATE title
SET entry_name='78 2eme revue [spectacle non identifié].'
WHERE title.id_uuid = 'qr119aab72dfb1f458f9a4847df52532a00';


UPDATE title
SET entry_name='79 Richelieu, 1er acte [Les Premières armes de Richelieu, comédie en deux actes, mêlée de couplets, par Bayard et Dumanoir, créée le 3 décembre 1839, rôle-titre].'
WHERE title.id_uuid = 'qr1d325c377d2914bdf9fee3c81ba69c5c1';


UPDATE title
SET entry_name='80 Richelieu, second acte, second costume [Les Premières armes de Richelieu, comédie en deux actes, mêlée de couplets, par Bayard et Dumanoir, créée le 3 décembre 1839, rôle-titre].'
WHERE title.id_uuid = 'qr13e0b63d4e2a94671b39e91b4424baa5a';


UPDATE title
SET entry_name='81 Mlle Sallé, 1er costume [Mademoiselle Sallé, comédie en deux actes, mêlée de couplets, par Saintine, Bayard et Dumanoir, créée le 29 juin 1841, rôle-titre].'
WHERE title.id_uuid = 'qr13dbf37dac29c4d62a22827e1880b1945';


UPDATE title
SET entry_name='82Mlle Sallé, second costume [Mademoiselle Sallé, comédie en deux actes, mêlée de couplets, par Saintine, Bayard et Dumanoir, créée le 29 juin 1841, rôle-titre].'
WHERE title.id_uuid = 'qr1712b7fea1e51461883085ad706e9cfa0';


UPDATE title
SET entry_name='83 Le scandale [Un Scandale, folie-vaudeville en un acte, par Duvert et Lauzanne, créée le 18 janvier 1834, rôle de Mme Fromageot].'
WHERE title.id_uuid = 'qr12b367da102d643f2aff3330d611543b0';


UPDATE title
SET entry_name='84 Sophie Arnould [Sophie Arnould, comédie-vaudeville en trois actes, par A. de Leuven, De Forges et Ph. Dumanoir, créée le 11 avril 1833, rôle-titre].'
WHERE title.id_uuid = 'qr18bdc919f534b45bcbe835d6a252dd9d7';


UPDATE title
SET entry_name='85 Sophie Arnould, costume du second acte [Sophie Arnould, comédie-vaudeville en trois actes, par A. de Leuven, De Forges et Ph. Dumanoir, créée le 11 avril 1833, rôle-titre].'
WHERE title.id_uuid = 'qr1622cf04ecef7453ea25b5be3733a8b94';


UPDATE title
SET entry_name='86 Sophie Arnould, costume du 3e acte [Sophie Arnould, comédie-vaudeville en trois actes, par A. de Leuven, De Forges et Ph. Dumanoir, créée le 11 avril 1833, rôle-titre].'
WHERE title.id_uuid = 'qr1339411dadb1f45c382f51a85c04d17e0';


UPDATE title
SET entry_name='87 Sous clé [Sous clef, comédie-vaudeville en un acte, par De Leuven, De Forges et Dumanoir, créée le 22 mai 1833, rôle d''Atala].'
WHERE title.id_uuid = 'qr1c4549507288745429bd25d3a0bb04163';


UPDATE title
SET entry_name='88 Suzanne, 1er acte [Suzanne, comédie-vaudeville en deux actes, par Mélesville et Eugène Guinot, créée le 28 novembre 1837, rôle-titre].'
WHERE title.id_uuid = 'qr192ba6a86eea64f6aa96969c765591f8a';


UPDATE title
SET entry_name='89 Suzanne, second acte [Suzanne, comédie-vaudeville en deux actes, par Mélesville et Eugène Guinot, créée le 28 novembre 1837, rôle-titre].'
WHERE title.id_uuid = 'qr15a63033c13ec4e1497bcc3c47d1650ea';


UPDATE title
SET entry_name='90 Le Sylphe, second costume [Follet, ou le Sylphe, vaudeville en deux actes, par Rochefort, Varin et Desvergers, créé le 26 juin 1832, rôle-titre].'
WHERE title.id_uuid = 'qr1f0005415d4454458bcb4f9c930f42aae';


UPDATE title
SET entry_name='91 Sylphe, 2me acte [Follet, ou le Sylphe, vaudeville en deux actes, par Rochefort, Varin et Desvergers, créé le 26 juin 1832, rôle-titre].'
WHERE title.id_uuid = 'qr13509ffc2bd634ae38b7dadca98529c15';


UPDATE title
SET entry_name='92 Le tailleur et la fée, les Chansons de Béranger, 1er acte, 1er costume [Le Tailleur et la Fée, ou les Chansons de Béranger, conte fantastique, mêlé de couplets, par Vanderburch, Ferdinand Langlé et De Forges, créé le 3 août 1831, rôle de la Fée et de la Danseuse].'
WHERE title.id_uuid = 'qr11205ca0e1c2b41ee97058d52c71d71b0';


UPDATE title
SET entry_name='93 Le trompette de Meurengo [Le trompette de Marengo, chanson d''Edouard Donvé, 1853].'
WHERE title.id_uuid = 'qr1f2a0887f328c462584a97619ec488868';


UPDATE title
SET entry_name='94 [Sans titre, spectacle non identifié].'
WHERE title.id_uuid = 'qr1a0ce858c8710446183cb811920e98717';


UPDATE title
SET entry_name='95 [Sans titre, mais avec la citation: "Je voudrais pourtant bien me dire adieu] [Un Conte de fées, comédie en trois actes, mêlée de chant, par De Leuven, Brunswick et Alexandre Dumas, créée le 29 avril 1845, rôle de la Marquise de Villani]'
WHERE title.id_uuid = 'qr1819396edde164b9c805541d95c61300b';


UPDATE title
SET entry_name='Dîner de La Pierre'
WHERE title.id_uuid = 'qr1ce1e3f3a52b744cb92a0c594262a8195';


UPDATE title
SET entry_name='Promotion de Novi-Bazar'
WHERE title.id_uuid = 'qr16f8a0527a1f648e1b24537a00d3c3a6d';


UPDATE title
SET entry_name='Restaurant "Chez Camille Desmoulins" - 5 rue de Beaujolais'
WHERE title.id_uuid = 'qr1a57f51fd9c5d40e79a13c41c1d10bb79';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr1b46f9ddc3edf49b5b525263c77556891';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr159a9af4543b645fb8b6c0d91e88ff216';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr1232366a24b6d481caf3adb83fecc776e';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr18c27b757ad5e405da4d5ac4a9c63e9ea';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr113bbf99884844b93be7382ce86fbe272';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr1d8a665c545784c01b912c043e1d52584';


UPDATE title
SET entry_name='Restaurant Stanislas - 145 galerie de Valois'
WHERE title.id_uuid = 'qr12e054d66258c4aa3a7173de920484ec7';


UPDATE title
SET entry_name='Restaurant Demory et Colle - 116 galerie de Valois'
WHERE title.id_uuid = 'qr14871c4a0aa214d199977371783258802';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr19f25d0af20fb429abb0de4156f3ffa7e';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr135caf7e9a2cc483f826c266edfb90d36';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr1e6729484fc824bddbc43781fb6020ac3';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr1629a267c4637436abe7de11bb36c83da';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr13d4747e19ee7406b823011d907943e01';


UPDATE title
SET entry_name='Restaurant Richefeu - 167 galerie de Valois'
WHERE title.id_uuid = 'qr1931a640367714b6784ab52201af073d0';


UPDATE title
SET entry_name='Restaurant Henri IV - 160 galerie de Valois'
WHERE title.id_uuid = 'qr1a7bfed6939bf414cb3153183e7cbc902';


UPDATE title
SET entry_name='Restaurant Henri IV - 160 galerie de Valois'
WHERE title.id_uuid = 'qr1d79afe814abd401ab5bcc1c7b4f17326';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr114a314025f02481191235315fa303ba0';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr115f2f967f49940f78152955e9276e5da';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr1f72badccd9084f4fbd98f54ec5359048';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr18dcb50ae2fcf4b94993d2cebe555ccbf';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr15d41f1e60569415488a51a5151b7db31';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr15671d9448b7d4f3c868607d0d5d19820';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr16fd0769c4ddd430dbcc179312ac36b11';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr1f8c81f5ebed8456b9d26c8a1a8487bb2';


UPDATE title
SET entry_name='Etablissement de Bouillon du Dîner National - 173 galerie de Valois'
WHERE title.id_uuid = 'qr1df16cec3126644fc90f1aece16e125db';


UPDATE title
SET entry_name='Rue Montpensier'
WHERE title.id_uuid = 'qr1657b31150ba84a159116cf181b7d6da7';


UPDATE title
SET entry_name='Signal lumineux [rue de Montpensier, Paris, 1er arrondissement]'
WHERE title.id_uuid = 'qr11a04f6e0cc8d49faa4780689efcc4b65';


UPDATE title
SET entry_name='Passage Richelieu : 11 rue Montpensier'
WHERE title.id_uuid = 'qr1b1e68a63932e4d54ba0b1cc34cac4272';


UPDATE title
SET entry_name='[Passage Hulot] : [41 Rue Montpensier, juin 1906]'
WHERE title.id_uuid = 'qr13cd2757de31b421db964743b62e30744';


UPDATE title
SET entry_name='Passage Beaujolais : 47 rue Montpensier [mai 1906]'
WHERE title.id_uuid = 'qr113b9eb7f184e47dd831df52cff936873';


UPDATE title
SET entry_name='La Clémence du Président par Auguste Paulé'
WHERE title.id_uuid = 'qr15dc96004c93d45109ff8bdd65fd2307e';


UPDATE title
SET entry_name='Arcade de la rue de Rivoli, cour des comptes, jardin du Palais-Royal;'
WHERE title.id_uuid = 'qr124314e7972b94e85aab3c1c2fa694a32';


UPDATE title
SET entry_name='Galerie d''Orléans (Palais-Royal), jardin du Palais-Royal;'
WHERE title.id_uuid = 'qr1378f443123864b4cb3e79d73424ac2b2';


UPDATE title
SET entry_name='[Rue de Beaujolais]'
WHERE title.id_uuid = 'qr1a1b7aa18854b4300a729082d4a21f2b8';


UPDATE title
SET entry_name='Angle de la rue de Valois et Beaujolais : maison à 9 étages R. de Valois 48 [Mai 1906]'
WHERE title.id_uuid = 'qr1d1c15a3b0c3644b5bc81088ae86d188e';


UPDATE title
SET entry_name='Angle de la rue de Valois et Beaujolais : au coin la maison à 9 étages [juin 1906]'
WHERE title.id_uuid = 'qr1ad1fd41bc4fd45979ce6047f1f2b687b';


UPDATE title
SET entry_name='Entrée du Palais-Royal : Rue de Valois'
WHERE title.id_uuid = 'qr174d443dc0aa24d62acb86c8438166e61';


UPDATE title
SET entry_name='Groupe des délégués internationaux au 35e congrès de l''Union des sociétés de gymnastique de France [i.e. 35e anniversaire de la création de l''USGF, photo prise devant le Grand Hôtel du Palais Royal, rue de Valois]'
WHERE title.id_uuid = 'qr19e7c3f1e55b7425f805d5985221e8f9b';


UPDATE title
SET entry_name='Hôtel Mélusine 8 Rue de Valois'
WHERE title.id_uuid = 'qr11e7b6d3200664cbdaa3be8d2ea7dca6c';


UPDATE title
SET entry_name='[Omnibus tiré par des chevaux, près du 9 rue de Valois, lieu de travail du photographe Zulimo Chiesi]'
WHERE title.id_uuid = 'qr19c6e4116ccdb4d78a52fe6a30184e6ed';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans sur la rue de Valois'
WHERE title.id_uuid = 'qr1682377bcf3344bd29ea32ba20dff48e4';


UPDATE title
SET entry_name='R. de Valois : [Restaurant du "Boeuf à la mode"]'
WHERE title.id_uuid = 'qr1d51017ca14074e29ad7124f3437ea9b9';


UPDATE title
SET entry_name='Hôtel Fontaine-Martel où habita Voltaire en 1732 et 33 : 20 rue de Valois'
WHERE title.id_uuid = 'qr1aa7a271e1312403990ec42539b08c959';


UPDATE title
SET entry_name='[Restaurant du Boeuf à la mode]'
WHERE title.id_uuid = 'qr1c2cf556eaab24dfeb4a52b2bcf91d07c';


UPDATE title
SET entry_name='Jardin du Palais-Royal'
WHERE title.id_uuid = 'qr1b7dec7c731eb4cb089bfd53acdf63ff9';


UPDATE title
SET entry_name='Galerie de bois du Palais Royal en 1820'
WHERE title.id_uuid = 'qr1bd4e45f85aa3413eabbc96d699e37bbd';


UPDATE title
SET entry_name='Voie publique, urinoirs : urinoirs du boulevard Sébastopol, des Champs-Elysées, de la Place du Palais-Royal, des boulevards, colonnes-affiches'
WHERE title.id_uuid = 'qr1114426a5865e42fb8390e29935abc877';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans : 19 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1af0ae3f1c7c44eda8f448c2de2f28638';


UPDATE title
SET entry_name='La rue des Bons-Enfants et la Chancellerie d''Orléans au n°19'
WHERE title.id_uuid = 'qr14726bd9fa82d450aa0342dcc6d3f6a7a';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans : 19 rue des Bons-Enfants'
WHERE title.id_uuid = 'qr1cf03a293f9d74612a564fc642c4b7a3f';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans : 19 rue des Bons-Enfants'
WHERE title.id_uuid = 'qr1813778eeeb0144b69bb04daec42aa6cc';


UPDATE title
SET entry_name='Plan d''une maison appartenante à Madame d''Argenton, rue des Bons-Enfants à Paris'
WHERE title.id_uuid = 'qr103086c3e0e4545cda8a04bd35016b7ab';


UPDATE title
SET entry_name='Palais-Royal, Vu à vol d''oiseau.'
WHERE title.id_uuid = 'qr15d94c134bfe34f88aea4827960fe64c9';


UPDATE title
SET entry_name='Palais-Royal. Façade du coté de la Place'
WHERE title.id_uuid = 'qr12bd972f8e17f401dac4d9eb371e33859';


UPDATE title
SET entry_name='Palais Royal (Trait de courage d''un jeune homme de l''Ecole Polytechnique Journée du 28 Juillet 1830)'
WHERE title.id_uuid = 'qr126847ade599347fd96f7703b0f9940d0';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1a4c9efacb553447b8ae1cf68f6d5dabe';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1a5b11e2a34844349bd92278d1c073c3c';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1df9eb89dbba64be7ad7b3cc116cf4a0b';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1b086905e4cf74235a8fa81c3272c0a5c';


UPDATE title
SET entry_name='[Palais-Royal] : [passage du Perron, mai 1906]'
WHERE title.id_uuid = 'qr1e79d914f8a484dd79cd049863780b187';


UPDATE title
SET entry_name='[Le Palais-Royal]'
WHERE title.id_uuid = 'qr1abb13f3c1ba146e79fd06757f9c321c8';


UPDATE title
SET entry_name='[Le Palais-Royal]'
WHERE title.id_uuid = 'qr1bf531fc36d8a45cda8fae76e9606b852';


UPDATE title
SET entry_name='[Galerie et jardins du Palais-Royal]'
WHERE title.id_uuid = 'qr1ff404f0ba89f41aa80d81259235d9512';


UPDATE title
SET entry_name='Cour du Palais-Royal'
WHERE title.id_uuid = 'qr1da6fd75c9a6c4ef0abda2237a7392e06';


UPDATE title
SET entry_name='[Les galeries de bois au Palais-Royal]'
WHERE title.id_uuid = 'qr14229bce7d4734b02bc5dcf6163759db0';


UPDATE title
SET entry_name='[Galeries du Palais-Royal]'
WHERE title.id_uuid = 'qr183b0a13b312d47a5b96131b759a7c257';


UPDATE title
SET entry_name='Veuë du fort Royal fait en l''année 1650 dans le Jardin du Palais Cardinal pour le divertissement du Roy'
WHERE title.id_uuid = 'qr17e9fc76ff8834fbfad3f83f8149aab47';


UPDATE title
SET entry_name='[Vue du Palais-Royal en 1800]'
WHERE title.id_uuid = 'qr166a042474cc343388656a080b3475450';


UPDATE title
SET entry_name='[Jardin du Palais-Royal]'
WHERE title.id_uuid = 'qr176e90bc6035a40658922e05497d7bc61';


UPDATE title
SET entry_name='Palais Royal de Paris'
WHERE title.id_uuid = 'qr18ade41aa51594dbdb9ebfc2c6f2db829';


UPDATE title
SET entry_name='9/6/23, fête de la Renaissance au Palais Royal, visite de M. Millerand'
WHERE title.id_uuid = 'qr15071b6119d5145cea157bdca00f0c957';


UPDATE title
SET entry_name='Malheur arrivée au Palais Royal'
WHERE title.id_uuid = 'qr1c5a21a708f5c4a6a8ca11ce18ede829f';


UPDATE title
SET entry_name='Paris : Place du Palais Royal'
WHERE title.id_uuid = 'qr151cdb6467641413e85088510c4f1510b';


UPDATE title
SET entry_name='8 juin 1923, fêtes de la Renaissance du Palais Royal'
WHERE title.id_uuid = 'qr1673e9fa8a55d4ebe836ab351faa1eec2';


UPDATE title
SET entry_name='22/9/26, le petit canon du Palais Royal'
WHERE title.id_uuid = 'qr14319254ac69d463cb9ef33250cf2efed';


UPDATE title
SET entry_name='[Le n° 113 au Palais-Royal]'
WHERE title.id_uuid = 'qr18a4b5653da02411bb487b38a6317de80';


UPDATE title
SET entry_name='[Le n° 113. Palais-Royal. 1815. La sortie du n° 113]'
WHERE title.id_uuid = 'qr16e2fe2b2503e407396a42aa9cd4f735d';


UPDATE title
SET entry_name='1924, taxi Bellanger [place du Palais Royal]'
WHERE title.id_uuid = 'qr1acdfe581582c4528a32adb737cd91040';


UPDATE title
SET entry_name='[7 ou 8] juin 1923, fête de la Renaissance du Palais Royal'
WHERE title.id_uuid = 'qr12aa7ad3d76e44f49868623b63cd8f451';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923]'
WHERE title.id_uuid = 'qr19cd14a1b1a36401faafa47974651378b';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923]'
WHERE title.id_uuid = 'qr1aa370969dca34a649f546717707f16b6';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, mode ancienne [9 juin 1923]'
WHERE title.id_uuid = 'qr1481045f1d3c14a6aa0880d98cce8630e';


UPDATE title
SET entry_name='Taxi de Dion [place du Palais Royal]'
WHERE title.id_uuid = 'qr1d0d530c52b494c8b9b29607250b9bdd6';


UPDATE title
SET entry_name='7 juin 1923, fête de la Renaissance du Palais Royal, la marchande d''oublies'
WHERE title.id_uuid = 'qr14e76c997dfd24d53819f287358625869';


UPDATE title
SET entry_name='Vûe et Perspective du Palais Royal du côté du Jardin'
WHERE title.id_uuid = 'qr1c35ae75e66f741bb8af07ead029254a3';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923]'
WHERE title.id_uuid = 'qr10328cd7600eb42bc9212ab77bbef0bb3';


UPDATE title
SET entry_name='Jardin du Palais Royal, Paris, 12 juin 1895'
WHERE title.id_uuid = 'qr11b870abb91d74c2b9a4021b958cf3f44';


UPDATE title
SET entry_name='Vue Perspective du Palais Royal du côté du Jardin'
WHERE title.id_uuid = 'qr139c0bfebb5f446b987198334fe4c76b4';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923]'
WHERE title.id_uuid = 'qr10b7b50c77b0048d7a8cadd6fa6a4e6dc';


UPDATE title
SET entry_name='Foyer [du théâtre] Montansier'
WHERE title.id_uuid = 'qr19b433c55d1de44cb91fe3c70771cb99f';


UPDATE title
SET entry_name='[Les arcades du Café de Foy] : [Camille Desmoulins au Palais-Royal]'
WHERE title.id_uuid = 'qr1e113d6fb0a894c02b054b0493c0bf680';


UPDATE title
SET entry_name='[Le rez-de-chaussée du Foyer de la Montansier]'
WHERE title.id_uuid = 'qr11b3de65103aa40359abf580966bed76d';


UPDATE title
SET entry_name='Jardin Palais Royal'
WHERE title.id_uuid = 'qr1cbb74c5ba23d4bf7b4f228fe45957e00';


UPDATE title
SET entry_name='12-7-21, discours de M. [Francisco León] de la Barra [inauguration du monument au génie latin, jardins du Palais-Royal]'
WHERE title.id_uuid = 'qr16c88371764f84a6fbaf658f879dbb300';


UPDATE title
SET entry_name='Paris, le Conseil d''Etat [depuis la place du Palais-Royal]'
WHERE title.id_uuid = 'qr166c692e857984509a7277254dc6d4870';


UPDATE title
SET entry_name='[Façade du Palais-Royal sur le jardin]'
WHERE title.id_uuid = 'qr1322f276918e043bbb7f288748c2ceb86';


UPDATE title
SET entry_name='Le café de la Rotonde. Palais-Royal'
WHERE title.id_uuid = 'qr18b55c2600ff24d2bbf29cbdf7fb1dc65';


UPDATE title
SET entry_name='Discours de Manuel de Peralta [inauguration du monument au génie latin, jardins du Palais-Royal, le 12 juillet 1921]'
WHERE title.id_uuid = 'qr18dd5e23d85c34b2bbe1acd66a242f376';


UPDATE title
SET entry_name='Place du Palais-Royal, 1849'
WHERE title.id_uuid = 'qr1a65974ea5f054927944d32a1590ca5f2';


UPDATE title
SET entry_name='Plan général du Palais-Royal et de ses environs'
WHERE title.id_uuid = 'qr1bb0c367d1b9c45249b57c8f028fa71bc';


UPDATE title
SET entry_name='7 juin 1923, fête de la Renaissance du Palais Royal'
WHERE title.id_uuid = 'qr1522d1d8755e9407fa29887dd88668e68';


UPDATE title
SET entry_name='Fonds Clémançon. II. Archives photographiques. Vues d''ensemble des salles de spectacles. Théâtres, cinémas et autres établissements situés à Paris. Jardins du Palais Royal. Illuminations des pièces d''eau'
WHERE title.id_uuid = 'qr182a34557109e4c108293fdb15e30c6b4';


UPDATE title
SET entry_name='8 juin 1923, fête de la Renaissance du Palais Royal [jeune femme déguisée devant un stand de luminaires]'
WHERE title.id_uuid = 'qr185953dfec3f54351922a4055441c1c71';


UPDATE title
SET entry_name='Monument au génie latin [inauguration aux jardins du Palais-Royal, 12 juillet 1921]'
WHERE title.id_uuid = 'qr159781e24c4994b298ef39e19faa20166';


UPDATE title
SET entry_name='F. Nash, J. Stephanoff, Les arcades du Palais-Royal vers 1820, extrait de : Victor Champier, Le Palais-Royal d’après des documents inédits (1629-1900), Paris, Société de propagation des livres d’art, 1900, p. 95.'
WHERE title.id_uuid = 'qr1c654b89fd39540e6bedd2df69b837d55';


UPDATE title
SET entry_name='Une Matinée du Palais Royal : [estampe] : ou la bonne... Occasion'
WHERE title.id_uuid = 'qr1b7e87fe376594e1ea0bf077e023a18e8';


UPDATE title
SET entry_name='[L''Ancien Palais-Royal]'
WHERE title.id_uuid = 'qr1891b2298bf01432380eeac97a5ec63ef';


UPDATE title
SET entry_name='Le Palais Royal sous Louis XVI'
WHERE title.id_uuid = 'qr1a3d63674ee3246c48373aaa8ee3a6f2b';


UPDATE title
SET entry_name='Monument de Victor Hugo par Rodin [inauguré au Palais-Royal en 1909]'
WHERE title.id_uuid = 'qr1a1dac2449d83453caffda81d59623c56';


UPDATE title
SET entry_name='Jardin Palais Royal'
WHERE title.id_uuid = 'qr193273dabaa0c4a6b8002cd7ff4eb487e';


UPDATE title
SET entry_name='Fonds Clémançon. II. Archives photographiques. Vues d''ensemble des salles de spectacles. Théâtres, cinémas et autres établissements situés à Paris. Jardins du Palais Royal. Illuminations des pièces d''eau'
WHERE title.id_uuid = 'qr19eb39d607e56461da05f5fc79e287492';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923] ]'
WHERE title.id_uuid = 'qr1478340faf68e4b7e9bfdbdaea4645c6c';


UPDATE title
SET entry_name='[Vue du Palais-Royal et du jardin]'
WHERE title.id_uuid = 'qr1ab6c4fbdd77c4c77bb957fb2dd3c3ff2';


UPDATE title
SET entry_name='[7 ou 8] juin 1923, fêtes de la Renaissance du Palais Royal'
WHERE title.id_uuid = 'qr1c7928f32ff7642eb8082e7eeea62f44c';


UPDATE title
SET entry_name='8 juin 1923, fête de la Renaissance du Palais Royal [jeune fille déguisée devant un stand de fleurs]'
WHERE title.id_uuid = 'qr18e263be56a394ca5b22ae4e27217a779';


UPDATE title
SET entry_name='14-3-13, obsèques de Mr A. Poincaré [i.e. Alfred Picard, le corbillard sort de la cour du Palais-Royal]'
WHERE title.id_uuid = 'qr19e03e6515e8a48d3bf9cd7579c6227f9';


UPDATE title
SET entry_name='Jeune Elégant se promenant aux Palais Royal // pour fixer les Caprices de sa Soiré'
WHERE title.id_uuid = 'qr10c40de10f40d4b778eeeb193ffc5fa8a';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, modes anciennes [9 juin 1923]'
WHERE title.id_uuid = 'qr1328457d7bcc14a5e8cf28708de69d237';


UPDATE title
SET entry_name='Jardin du Palais Royal'
WHERE title.id_uuid = 'qr17acf525cf87a4783912149fa501ee36e';


UPDATE title
SET entry_name='Veüe de la Gallerie du Palais Royal à Paris'
WHERE title.id_uuid = 'qr13d8d06c31ef04202991f571d957307c9';


UPDATE title
SET entry_name='Fêtes de la Renaissance au Palais Royal, mode ancienne [9 juin 1923]'
WHERE title.id_uuid = 'qr10c11cb210f584cbda77bedf51cc91536';


UPDATE title
SET entry_name='[Vues de Paris et de ses environs]'
WHERE title.id_uuid = 'qr1d542cd91fdd64e9a80bf894a841f4bae';


UPDATE title
SET entry_name='[Vues de Paris et de ses environs]'
WHERE title.id_uuid = 'qr180d1cf25468644b9833516396060d537';


UPDATE title
SET entry_name='Fonds Clémançon. II. Archives photographiques. Vues d''ensemble des salles de spectacles. Théâtres, cinémas et autres établissements situés à Paris. Jardins du Palais Royal. Illuminations des pièces d''eau'
WHERE title.id_uuid = 'qr1f084a6efbcc54628a7f923634ac57a6f';


UPDATE title
SET entry_name='Les Ruines de Paris, 1871'
WHERE title.id_uuid = 'qr14a527dad2b5c4442861f4866a5925b78';


UPDATE title
SET entry_name='9/3/28, conseil national économique au Conseil d''Etat [Palais Royal, 1er arrondissement]'
WHERE title.id_uuid = 'qr106f0ccf7b91947a383de2838f381a3fc';


UPDATE title
SET entry_name='Mr de Coyllin. Palais Royal. Un fantôme. Mr de Coyllin (Grassot). De Lauzun (Derval). Mr de Coyllin : Donnez vous donc la peine de vous asseoir'
WHERE title.id_uuid = 'qr15be1830f99da4651aaaffd97be147333';


UPDATE title
SET entry_name='Mannequin du pape brulé au Palais Royal : [estampe] : le 6 Avril 1791'
WHERE title.id_uuid = 'qr1f3e42f6ccb4a44a896c8aef1a9c71381';


UPDATE title
SET entry_name='Fonds Clémançon. II. Archives photographiques. Vues d''ensemble des salles de spectacles. Théâtres, cinémas et autres établissements situés à Paris. Jardins du Palais Royal. Illuminations des pièces d''eau'
WHERE title.id_uuid = 'qr1201cf2d1ec384442a37ddc3b72492d83';


UPDATE title
SET entry_name='Vue de l''intérieur du nouveau cirque du Palais Royal et des ambassadeurs du Nabab Tipou.'
WHERE title.id_uuid = 'qr178655eda4e2643d89db5ed921b8675a5';


UPDATE title
SET entry_name='[La troupe du Théâtre du Palais-Royal en 1859 / dessin de Lhéritier]'
WHERE title.id_uuid = 'qr1d94e6cbaf6784ced905bcaaf099701aa';


UPDATE title
SET entry_name='Motion faite au Palais Royal par Camille Desmoulins : le 12 juillet 1789'
WHERE title.id_uuid = 'qr1d62a99d47f3a43faa4fec96df2f1f568';


UPDATE title
SET entry_name='Vue du feu prit a la salle de l''Opera de Paris le 6 avril 1763. N° 125'
WHERE title.id_uuid = 'qr1d516080d0f704528bbf572288d2fb00d';


UPDATE title
SET entry_name='[La Pissotte : café du Théâtre du Palais-Royal : dessin / de Lhéritier]'
WHERE title.id_uuid = 'qr1e8c2f273b53047b1a7de2bf79b952ddb';


UPDATE title
SET entry_name='Soirée du 30 Juin 1789. Dédiée A l''assemblée du Palais Royal : Après avoir délivré les gardes Françaises...'
WHERE title.id_uuid = 'qr16ccbafcf538648b9af8bae70552b796b';


UPDATE title
SET entry_name='Destruction du Palais-Royal. Voitures de la famille royale brûlées sur la place du Palais-Royal'
WHERE title.id_uuid = 'qr178285bd520d84c668824a254d9604a4f';


UPDATE title
SET entry_name='La Soirée du Palais Royal : ou les religieuses a bonnes fortunes'
WHERE title.id_uuid = 'qr17050f195d0b94408b0680b241eb7d82f';


UPDATE title
SET entry_name='[The Palais Royal Garden Walk]'
WHERE title.id_uuid = 'qr1dd32542da3184930b49fc77abf6ae132';


UPDATE title
SET entry_name='Mr Lucas se disant député, et faisant sa motion au Palais Royal'
WHERE title.id_uuid = 'qr1ff2d1e1aa0b741b3b5171d247d99947a';


UPDATE title
SET entry_name='Vue du jardin du Palais Royal, de ses batiments et galleries'
WHERE title.id_uuid = 'qr1487997799c274f93af169a252f571db8';


UPDATE title
SET entry_name='[Recueil. Tour de France cycliste de 1948. Journée du 30 juin. 1e étape Paris-Trouville]'
WHERE title.id_uuid = 'qr13b8c3ac4f02e4f60a4374a32564c1bab';


UPDATE title
SET entry_name='[Deux-Anes, comédie-vaudeville de Mélesville et Carmouche : estampe]'
WHERE title.id_uuid = 'qr134cef069b77c47119113f47db28014b2';


UPDATE title
SET entry_name='Manequin du pape brulé au Palais Royal : le 6 avril 1791'
WHERE title.id_uuid = 'qr1893afe97a7124ef1b627201f9e0e171e';


UPDATE title
SET entry_name='Place Vendôme. Place Louis XV. Place du Châtelet. Porte Saint-Denis. Palais-Royal. Porte Saint-Martin'
WHERE title.id_uuid = 'qr12ae399369ef7432faa79f306b2244a46';


UPDATE title
SET entry_name='Soirée du 30 Juin 1789. Dédiée à l''Assemblée du Palais Royal : Après avoir délivré les Gardes Françaises...'
WHERE title.id_uuid = 'qr137ac9e10c5a94b67bfb69bcdacb9a13c';


UPDATE title
SET entry_name='Soirée du 30 Juin 1789. Dédiée à l''Assemblée du Palais Royal : Après avoir délivré les Gardes Françaises...'
WHERE title.id_uuid = 'qr1d469a9094b214152b4b3de9e6541ed48';


UPDATE title
SET entry_name='Vue du feu prit a la salle de l''Opera de Paris le 6 avril 1763'
WHERE title.id_uuid = 'qr19cca4bb6d8c54e258f4306e9e9ad8c15';


UPDATE title
SET entry_name='[Les Vues des Maisons royales et des villes conquises par Louis XIV]'
WHERE title.id_uuid = 'qr16540c13895f54655879dac6bb8792b51';


UPDATE title
SET entry_name='Brevet de Fab.t d''Etoffes de Soie et Laine de S. A. R. Madame Duchesse de Berry. Pour le Sieur Delisle... Fait au Palais de l''Elysée-Bourbon, le 20 novembre 1826'
WHERE title.id_uuid = 'qr1ea58184dfbfa4e0f8963ee7153274f99';


UPDATE title
SET entry_name='Matinée du Palais Royal : 3 may 1791'
WHERE title.id_uuid = 'qr1e6ba13aea198480dabf2466a36f84907';


UPDATE title
SET entry_name='Promenade du Jardin du Palais Royal'
WHERE title.id_uuid = 'qr165f852262ff7478c811b46c17ab9bedf';


UPDATE title
SET entry_name='Le Palais Royal et ses abords : [placard d''annonces publicitaires pour différents commerces]'
WHERE title.id_uuid = 'qr1b8e68a68054d4b0da56263d5eda1c85f';


UPDATE title
SET entry_name='Au Port du Havre, Palais-Royal, Galerie Montpensier 66, 67. Chinoiseries...'
WHERE title.id_uuid = 'qr1761aa85838614820998e3a0db5d1a96c';


UPDATE title
SET entry_name='Camillus Desmoulins predigt Aufruhr in dem Palais Royal : den 12 Jul. 1789'
WHERE title.id_uuid = 'qr1fc5c1bf8f84e49088198a6dda18483ab';


UPDATE title
SET entry_name='La Brûlure : les citoyens du Palais Royal après avoir tenu conseil sur ce que l''on devait faire à l''égard du pape délibérement que l''on porteroit un mannequin d''osier semblable au St Père la tete couronnée d''une thiare à son col une croix une main tenant un poignard et l''autre des bulles...'
WHERE title.id_uuid = 'qr15bbe8f4c5eed4ee694013266b031b75f';


UPDATE title
SET entry_name='La Renaissance du Palais Royal : deux gentilhommes'
WHERE title.id_uuid = 'qr165a3328174b04120bb2f200038eb5b77';


UPDATE title
SET entry_name='Promenade au Palais Royal : [estampe] ; Cambacérès, Jean-Jacques Régis de (1753-1824)'
WHERE title.id_uuid = 'qr191cb063585d34981afd6bc862f741be8';


UPDATE title
SET entry_name='Motions du Palais Royal le 12 J.t 1789'
WHERE title.id_uuid = 'qr10578934b761749ffa9c4c0342f704fcb';


UPDATE title
SET entry_name='The attack on the Palais Royal'
WHERE title.id_uuid = 'qr14cf8afb5c61645918a3836baddd64cdf';


UPDATE title
SET entry_name='A Bernard Palissy...fondée en 1847. G Boutigny ... Porcelaine, Cristaux, faïence, spécialité de services de table... Palais Royal, Paris'
WHERE title.id_uuid = 'qr1252d050ccc334e768e30f995bf093839';


UPDATE title
SET entry_name='29 Juillet 1830. Ambulance Des Blessés. Cour du Palais Royal'
WHERE title.id_uuid = 'qr15df33b39ad314750919130c28f47adc1';


UPDATE title
SET entry_name='Vue de la face du Palais-Royal sur la rue S.t Honoré'
WHERE title.id_uuid = 'qr1c0a019f195f54f8fbc8f5d6814bc897b';


UPDATE title
SET entry_name='Le voilà fait'
WHERE title.id_uuid = 'qr1f6dc9a4948ca4cb588c24e0285ac5fb7';


UPDATE title
SET entry_name='M.lle Josephine occupée du soin de son commerce, faisant chit chit dans le Palais Royal'
WHERE title.id_uuid = 'qr159afb6d206b746e3aad9a53dbc266263';


UPDATE title
SET entry_name='Plafond du milieu de la grande gallerie du Palais royal. : [l''assemblée des dieux]'
WHERE title.id_uuid = 'qr110a0b11dec6c43ecb60fdb6c29f12f34';


UPDATE title
SET entry_name='[Enée et Achate apparaissant à Didon] : [estampe] ([État avec l''adresse de Buldet ajoutée])'
WHERE title.id_uuid = 'qr17b056a5176f14d2a8b7d71b567c32937';


UPDATE title
SET entry_name='Une Grande partie du peuple a été témoin du juste châtiment de l''abbé insolent'
WHERE title.id_uuid = 'qr1de40510c317048558e97743fa5003f86';


UPDATE title
SET entry_name='Palais-Royal Lecture du Moniteur le lundi 26 juillet à 7 heures du soir'
WHERE title.id_uuid = 'qr1abd3eae5140042d58cae73ef4fbe9108';


UPDATE title
SET entry_name='Le petit canon du jardin du Palais Royal'
WHERE title.id_uuid = 'qr1a1119e5b47e04b46a863263ebd053a79';


UPDATE title
SET entry_name='Paris, le Palais Royal Vue prise au-dessus du Café de la Rotonde'
WHERE title.id_uuid = 'qr1b316a4efdce34e93b24d1bf2f2c6fe5e';


UPDATE title
SET entry_name='Le Premier pas d''un jeune Officier Cosaque au Palais-Royal'
WHERE title.id_uuid = 'qr1fc1ca73bc2804958a7d41ac559c42fdd';


UPDATE title
SET entry_name='12/3/28, [i.e. 12/6/28], semaine coloniale [danseur]'
WHERE title.id_uuid = 'qr19d0df9f7958b4d25928d80e7a217a06d';


UPDATE title
SET entry_name='Le café de la Rotonde au Palais-Royal'
WHERE title.id_uuid = 'qr15a0d4fb85e9140b282d59893e75b5a6d';


UPDATE title
SET entry_name='Bal donné au Palais-Royal, le samedi 11 février, par LL. AA. le prince Napoléon et la princesse Clotilde'
WHERE title.id_uuid = 'qr1f16451b3c29a4e788483816019df5798';


UPDATE title
SET entry_name='26 juillet 1830. Lecture Des Ordonnances Dans Le Moniteur, Au Jardin du Palais Royal'
WHERE title.id_uuid = 'qr1f638185674084dfbbe2b4430dc575dc7';


UPDATE title
SET entry_name='[Métamorphose des vaisseaux d''Enée en Néréides]'
WHERE title.id_uuid = 'qr180044d69fc9b4a718cfb0bac1270bd9d';


UPDATE title
SET entry_name='Attaque du Château d''Eau Place du Palais Royal'
WHERE title.id_uuid = 'qr12bb6312ab96d4a9bbcc8b8238ae261bc';


UPDATE title
SET entry_name='Fête du Palais-Royal : costume de la révolution'
WHERE title.id_uuid = 'qr1cef7a5c094974202867be66d4191e165';


UPDATE title
SET entry_name='Caricature contre Cambacerès, d''Aigrefeuille et La Vieuville : [estampe] : Promenade au Palais royal.'
WHERE title.id_uuid = 'qr1c7ef2ebdbffc4827aee2ef8ae82fead9';


UPDATE title
SET entry_name='Fête du Palais-Royal : costume 1830 marchande de plaisir'
WHERE title.id_uuid = 'qr1422e314438de45809df56cc962e1f373';


UPDATE title
SET entry_name='Paris et ses environs, Palais-Royal'
WHERE title.id_uuid = 'qr18a40acbc4e0a4660874e3e91be852854';


UPDATE title
SET entry_name='Hamilton. Palais-Royal'
WHERE title.id_uuid = 'qr1c91db08a9754482fa067aef9ea60174a';


UPDATE title
SET entry_name='Le meilleur joueur d''échec du monde, [Alexandre Alekhine] : pendant une partie [silmutanée au café de la Rotonde au Palais-Royal, Paris]'
WHERE title.id_uuid = 'qr194c99be1e7e647589e37f7ffe7659844';


UPDATE title
SET entry_name='Le Marchand d''argent batonné : le samedi un des vendeurs d''argent du Palais Royal s''etant presenté sur la place fut surpris par le peuple et batu d''importance a coup de canne et si la Garde ne l''eut tiré des mains de ceux qui lui administroient si galamment les interets de l''action indigne commise la veille par un de ses camarade...'
WHERE title.id_uuid = 'qr195b670c18ae34f759cd81fdc26cf7522';


UPDATE title
SET entry_name='LA PROMENADE PUBLIQUE'
WHERE title.id_uuid = 'qr164f6118be64544efb057a3c6422e5005';


UPDATE title
SET entry_name='Passage de la cour des fontaines : Palais-Royal'
WHERE title.id_uuid = 'qr18ff766544489451a90b259be732585cc';


UPDATE title
SET entry_name='[Vénus fait forger des armes à Énée]'
WHERE title.id_uuid = 'qr1be134207db5149efb7884d3cf9e300d4';


UPDATE title
SET entry_name='la Renaissance du Palais Royal : Mme. Maria Vérone, costumée'
WHERE title.id_uuid = 'qr1fd82bdf212c14744aab883a40530ad33';


UPDATE title
SET entry_name='Paris, vue du Jardin du Palais-Royal'
WHERE title.id_uuid = 'qr184c09c7acbd749b99179284253188bf1';


UPDATE title
SET entry_name='Les Trente deux filles, dans l''Allée des soupirs'
WHERE title.id_uuid = 'qr19f5ebc3d2fa042d5b1e4615dae6a1b30';


UPDATE title
SET entry_name='Fête du Palais-Royal : costume 1830'
WHERE title.id_uuid = 'qr1a4779939eb8c4475a8d416f108e518b4';


UPDATE title
SET entry_name='12/3/28, [i.e. 12/6/28], semaine coloniale, empereur d''Annam'
WHERE title.id_uuid = 'qr1966830d4cc92478faa3f7915a75502c4';


UPDATE title
SET entry_name='Vue du Palais-Royal'
WHERE title.id_uuid = 'qr191a11a6a6cb14a7f881652ac9fe12a45';


UPDATE title
SET entry_name='Les abonnés au Canon du Palais-Royal'
WHERE title.id_uuid = 'qr101870127f04d4fe783c2b163b9ddd1a6';


UPDATE title
SET entry_name='Vues de Paris : les jardins du Palais Royal'
WHERE title.id_uuid = 'qr1f79a9471f7324eceb6fe1ae11c61be5b';


UPDATE title
SET entry_name='Les Alliés à la Rotonde du Palais Royal'
WHERE title.id_uuid = 'qr139900a78d5ca4e79a0268b26d378a7f3';


UPDATE title
SET entry_name='M.lle La Pierre, jeune géante ; prussienne : agée de 19 ans, de 6 pieds 2 pouces de hauteur. On la voit au Palais Royal, arcade n°33'
WHERE title.id_uuid = 'qr130c4bc9b2d314155810b7e9d0a933420';


UPDATE title
SET entry_name='12/3/28, [i.e. 12/6/28], semaine coloniale [danseuses]'
WHERE title.id_uuid = 'qr1cf221f890ce9482ba0b3dbfc54e6b847';


UPDATE title
SET entry_name='Suite de la Promenade au Palais Royal'
WHERE title.id_uuid = 'qr166407486b2fa4186972bad65eb674b4e';


UPDATE title
SET entry_name='Le Bref du pape brulé au Palais Royal le 4 may 1791 par un grand nombre de citoyens, et l''on y a joint un manequin représentant Sa Sainteté'
WHERE title.id_uuid = 'qr108e21288c83e43c8a5d797cf07a3bc4b';


UPDATE title
SET entry_name='Arrivée du Duc d''Orléans au Palais Royal, dans la soirée du 29 Juillet 1830'
WHERE title.id_uuid = 'qr121b3d242511342f1aab12dcc7218035f';


UPDATE title
SET entry_name='Vues de Paris : arcades intérieures du Palais Royal'
WHERE title.id_uuid = 'qr126141bfb8fa9419094c8b8f2db618f17';


UPDATE title
SET entry_name='Les promenades parisiennes. Jardin des Tuileries - Bassin des Tuileries - Les Champs Elysées - Jardin du Palais Royal - Le bois de Vincennes - Le bois de Boulogne - Le lac d''Enghien - Le parc de Saint Cloud - Longchamp. N°140'
WHERE title.id_uuid = 'qr17534504372494eca90170e7809d48e45';


UPDATE title
SET entry_name='7/2/27, Conseil d''Etat, M. Poincaré et son chef de cabinet,'
WHERE title.id_uuid = 'qr1c8ecde9ab503475e9d8aa16091e80c44';


UPDATE title
SET entry_name='La Renaissance du Palais Royal : deux dames du 1er Empire vendant des gravures'
WHERE title.id_uuid = 'qr1a6480ee8609a4163bb67e6c18e8abb40';


UPDATE title
SET entry_name='Berger, Fonst [8 mai 1904, Palais Royal]'
WHERE title.id_uuid = 'qr1841b9fe8a9d1439aa2022bcd61b71c07';


UPDATE title
SET entry_name='L''Embarras du choix ou les Anglais au Palais Royal'
WHERE title.id_uuid = 'qr1998399e44bba48f38143ff6c3caf7829';


UPDATE title
SET entry_name='[De g. à d. le marquis de Chasseloup-Laubat, M. de Goethals, Berger, les trois premiers du championnat de sabre, mai 1904, Palais Royal]'
WHERE title.id_uuid = 'qr1eeac2f170c1348fdb0eb1225cd0f0475';


UPDATE title
SET entry_name='[Change des assignats au Perron du Palais-Royal]'
WHERE title.id_uuid = 'qr1c6065e27d3a142f09b03b3e206ece7d0';


UPDATE title
SET entry_name='Un peuple entier, sorti des foyers domestiques, ondule en murmurant sur les places publiques (lecture du Moniteur au Palais-Royal);'
WHERE title.id_uuid = 'qr181300c934b09475e921f2ad88c091ee3';


UPDATE title
SET entry_name='26 juillet 1830. Scène Au Cabinet De Lecture Galerie d''Orléans'
WHERE title.id_uuid = 'qr1b16005130cbb4fbd8f8202796e28df30';


UPDATE title
SET entry_name='F. 7. Le Palais-Royal ; f. 8. Chambre des Députés;'
WHERE title.id_uuid = 'qr1d146b818765f494a9245bf6a52291be6';


UPDATE title
SET entry_name='[Album de 82 phot. en dépliant de Paris par L.-P. Lefranc, à Eu, en 1900, don R. Bonaparte]'
WHERE title.id_uuid = 'qr11afa293f25664f20b7064667d42b8fd5';


UPDATE title
SET entry_name='Louis Philippe, Duc d''Orléans Lieutenant Général du Royaume, sort du Palais-Royal pour se rendre à l''Hôtel de ville. (31 Juillet 1830.)'
WHERE title.id_uuid = 'qr101581ca3a985404e97282cc5c390ca1c';


UPDATE title
SET entry_name='M.me Leménil, rôle de Poussette, dans Sylvandire, acte IV, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1f97243c297644f5e9b70772225d0af88';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr15988f6ef8a6146c79db9075443638f99';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr16773a6772e90478f96253031bf15f9fc';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr1633a78ca99b149b3beaa6afac50da933';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr123d9e8d39d3b40a7bdf67f0cdf481604';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr12c085c4ab55248b1b15fc5c9d3452200';


UPDATE title
SET entry_name='[Arletty dans "La viscosa" de Rip au Palais-Royal]'
WHERE title.id_uuid = 'qr1611f94bb85614c819be2a3f79ba53119';


UPDATE title
SET entry_name='Grassot, rôle de Tourangeot dans Sylvandire, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr10ceac693a0f948a5bc2a5899eea2daeb';


UPDATE title
SET entry_name='[Levassor dans Le Lait d''ânesse au Palais-Royal'
WHERE title.id_uuid = 'qr1596e64f031154192a021dbb7211f26b9';


UPDATE title
SET entry_name='Me Doreil. (Palais-Royal). "Le Viglione"'
WHERE title.id_uuid = 'qr17e28aa8899a94b729a9855b07053499d';


UPDATE title
SET entry_name='Me Doreil. (Palais-Royal). "Le Viglione"'
WHERE title.id_uuid = 'qr11b5fafadfb7047c1b8903681fbdf05cb';


UPDATE title
SET entry_name='Me Doreil. (Palais-Royal). "Le Viglione"'
WHERE title.id_uuid = 'qr16d5ea580b8784928b463ecae98f82b64';


UPDATE title
SET entry_name='Me Doreil. (Palais-Royal). "Le Viglione"'
WHERE title.id_uuid = 'qr1a54b27e1b3dd4364971627a6b8c89559';


UPDATE title
SET entry_name='Me Doreil. (Palais-Royal). "Le Viglione"'
WHERE title.id_uuid = 'qr1103d463e572f47459a51d69f59160be6';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr14997f2d043fe46b68fb8beff17c66e6e';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr110fffbfec6424772ae850d339b3a6539';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr11d64b18f871f4f57af72439f96a4be50';


UPDATE title
SET entry_name='[Brasseur (Groseillier) dans La Mariée du Mardi-Gras, Théâtre du Palais-Roya'
WHERE title.id_uuid = 'qr120e0bebb64ad4623af732920eba1d7c5';


UPDATE title
SET entry_name='M.me Leménil, rôle de Poussette, dans Sylvandire, acte III, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr13beb68125617422fb5fd86dbd879dba2';


UPDATE title
SET entry_name='Me Evans. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1c054d77b49c34d348d5de03e98fb156a';


UPDATE title
SET entry_name='. Lavigne. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr13452626cdf734a62905e43f0e39047a0';


UPDATE title
SET entry_name='Jane Hading. Palais royal. Actéon'
WHERE title.id_uuid = 'qr139c18c16621c4fffbf81ebf66b53ce18';


UPDATE title
SET entry_name='[Evans. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr18bd357748de44069b9a4f1edbdb3cd90';


UPDATE title
SET entry_name='Me Boode. Palais-Royal'
WHERE title.id_uuid = 'qr1a0a2230ed5e44ab4ba32ff990046c508';


UPDATE title
SET entry_name='[Evans]. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1456da13baff94efa82a8b0747216370f';


UPDATE title
SET entry_name='Renaud. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr157c6744386814f9d94701dea2a5f09f6';


UPDATE title
SET entry_name='Delbernardi et Froment. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr167488b7d41b14266a66f5ea8e24d91e6';


UPDATE title
SET entry_name='Dezoder. Palais Royal. Actéon'
WHERE title.id_uuid = 'qr1a2fb9a5d561640f3b448c1f2d76e268f';


UPDATE title
SET entry_name='Foncine Durieux. Palais-Royal. Gotte'
WHERE title.id_uuid = 'qr185624f83ae7b47f386921a1c102219ea';


UPDATE title
SET entry_name='Foncine Durieux. Palais-Royal. Gotte'
WHERE title.id_uuid = 'qr1b37763eec03a4960b174ac577f50e365';


UPDATE title
SET entry_name='M.elle Reynold, rôle de Lisbeth, dans Le plus heureux des trois, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr13db9d1a62ef841fc85e217908d2bebe3';


UPDATE title
SET entry_name='Luguet, Brasseur & Gil Perès, au 2e acte de La sensitive du Palais-Royal, scène de la courte paille'
WHERE title.id_uuid = 'qr1dfe9eee8312549da9240b74c25c57586';


UPDATE title
SET entry_name='M.elle Duverger, rôle de Batistine, dans Le lait d''ânesse, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1639f531c185a4ce093ba3ca8075ac780';


UPDATE title
SET entry_name='[Troupe du Théâtre du Palais-Royal vers 1850 / dessin de Lhéritier]'
WHERE title.id_uuid = 'qr1f59db8f40aca4b6491998d3cc94f869f';


UPDATE title
SET entry_name='[Germaine Risse et Jeanne Fusier dans une scène de "Les soeurs Mirette" au Palais-Royal]'
WHERE title.id_uuid = 'qr19678dfc8e64947638d29c7fd3cda1b98';


UPDATE title
SET entry_name='[La troupe du Théâtre du Palais-Royal : portrait de groupe par Lhéritier]'
WHERE title.id_uuid = 'qr1afab97c58fe24ba1b08dd35ce5efbd01';


UPDATE title
SET entry_name='31/8/32, démolition de la Rotonde [dans les jardins du Palais-Royal]'
WHERE title.id_uuid = 'qr1598b64bb0a8e4722a2851c86c54d3f3f';


UPDATE title
SET entry_name='31/8/32, démolition de la Rotonde [dans les jardins du Palais-Royal]'
WHERE title.id_uuid = 'qr1ab79ea6fcaf54533bc1f34ad9ba31d51';


UPDATE title
SET entry_name='Brasseur, rôle d''Arlequin, dans Les trois fils de Cadet Roussel, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr13295545d1e9245e5914c2c08ec5adcd6';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femme de paille'
WHERE title.id_uuid = 'qr18987f5d464f14da4b26118cb02e6cf4e';


UPDATE title
SET entry_name='M. Dailly. Palais-Royal. Revue [Joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1867ad3e07a704df5bc4235c302035980';


UPDATE title
SET entry_name='[Gillette. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr186e06cd75c70426fbfadd08f9c778288';


UPDATE title
SET entry_name='Hugonnet. Palais-Royal. [Les joyeusetés de Paris i.e Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1042865dfd36c49c78f247bb6f11717ef';


UPDATE title
SET entry_name='Leroux. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1ccd92604674946d8bc024825d64547b2';


UPDATE title
SET entry_name='[Réjane. Palais-Royal]. Ma Camarade'
WHERE title.id_uuid = 'qr1714bcbea5ff0435486c348326c3bba92';


UPDATE title
SET entry_name='[Réjane]. Palais-Royal. [Ma camarade]'
WHERE title.id_uuid = 'qr19f19a0e50c0e4d97a8b9d54ecf771405';


UPDATE title
SET entry_name='Réjane. Palais-Royal. Ma camarade'
WHERE title.id_uuid = 'qr105f7bc0f5a0c423ca24e3d081c873b7b';


UPDATE title
SET entry_name='Réjane. [Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr12721d539c6a8472cba850f30a907521a';


UPDATE title
SET entry_name='Clein. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1a3e96bd64f0049f89b101fc37b8a09df';


UPDATE title
SET entry_name='Clein. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1a281dea9b51945ada4067772438853ea';


UPDATE title
SET entry_name='Ellen Andrée. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1c04cdbd97d954102a91ac3db4973b4bd';


UPDATE title
SET entry_name='Dailly. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1aca6c36ac67c407ca887fe80324414b5';


UPDATE title
SET entry_name='[Jane Hading. Palais royal. Actéon]'
WHERE title.id_uuid = 'qr1cd52a366a0cf44898ad9e5f868bcfdc6';


UPDATE title
SET entry_name='[Jane Hading. Palais royal. Actéon]'
WHERE title.id_uuid = 'qr1b0e51d07c7f24a8492f3181ab9e125fe';


UPDATE title
SET entry_name='Jane Hading. Palais royal. Actéon'
WHERE title.id_uuid = 'qr1fe301809214f4e59bda3dd0500fb696b';


UPDATE title
SET entry_name='Dezoder. [Palais-Royal. Actéon]'
WHERE title.id_uuid = 'qr19831621acfea4053b2ffc3e8d55d3632';


UPDATE title
SET entry_name='[Dezoder. Palais Royal. Actéon]'
WHERE title.id_uuid = 'qr13501acacf98d4677a4c2d8caa7d647d2';


UPDATE title
SET entry_name='[Dezoder. Palais Royal. Actéon]'
WHERE title.id_uuid = 'qr1a52a5a28c08d4eb7b662e2bdb8c346dd';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femme de paille'
WHERE title.id_uuid = 'qr1ccf2ab6f880447c5804f621cde86d6ac';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femmes de paille'
WHERE title.id_uuid = 'qr184758a287b5c4a499da1ea41b8f6df24';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femmes de paille'
WHERE title.id_uuid = 'qr1545e20c5824f41f78d7d6fbf43cb0f3b';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femme de paille'
WHERE title.id_uuid = 'qr15a3c1d9219c0466ba47990d43b1f999b';


UPDATE title
SET entry_name='Evans. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr19fccc468226b4a0b91b9cc9fa0d61c82';


UPDATE title
SET entry_name='Evans. Palais-Royal. [Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr170975ecec4024213b0097e0f832b6c05';


UPDATE title
SET entry_name='Elven. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année'
WHERE title.id_uuid = 'qr174a46eb6781a4eb2a979539573467a3f';


UPDATE title
SET entry_name='Elven. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr18b217bc4a2e248bf932feeb23db35ae8';


UPDATE title
SET entry_name='Froment. [Palais-Royal]. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1bcf07f2087d448c28c907631ba4f0a59';


UPDATE title
SET entry_name='Bonnet, Dailly et Hugonnet. [Palais-Royal]. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr13fdccd08fdd94f6ea1661c9ec7dd36ef';


UPDATE title
SET entry_name='Mes Marley et Thérésine. (Palais-Royal). "Monsieur l''abbé"'
WHERE title.id_uuid = 'qr14f8727031555439aa1c10144d03fd537';


UPDATE title
SET entry_name='Bonnet. [Palais-Royal]. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr13a6755f1bd8a4fa28705833b8960833e';


UPDATE title
SET entry_name='Me Thérésine. (Palais-Royal). "Monsieur l''abbé"'
WHERE title.id_uuid = 'qr182e1f2907f134a81ae2cfb6d8fc1e01d';


UPDATE title
SET entry_name='Ellen Andrée. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr13151f3aeb2554b0da0490eb6a82dae5e';


UPDATE title
SET entry_name='Delbernardie [i.e. Del Bernardie]. Palais-Royal. Ma camarade'
WHERE title.id_uuid = 'qr13e5d442c1b954b52ae926a982beb4c59';


UPDATE title
SET entry_name='Delbernardi [i.e. del Bernardi]. Palais Royal. [Ma camarade]'
WHERE title.id_uuid = 'qr1b64b0e7365fa4453993f692ba12f8a67';


UPDATE title
SET entry_name='Delbernardie [i.e. Del Bernardie]. Palais-Royal. Ma camarade'
WHERE title.id_uuid = 'qr18e8501a5f92d46e48fe1d738607104bd';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr15ee1e7dc4b434cc691c0582e3e3da496';


UPDATE title
SET entry_name='Hugonnet. [Palais-Royal]. Les joyeusetés de Paris [i.e Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr11c4433a0146b4ce1a69b479bf66798de';


UPDATE title
SET entry_name='Froment. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr14570f91fdd0a4ec9a35dca447b5ce89c';


UPDATE title
SET entry_name='Bonnet. Palais-Royal. [Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr12178424128c440d980c94f4a4b24c3c7';


UPDATE title
SET entry_name='Elven. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr15f829d607e4d4583bd19d923d9329d89';


UPDATE title
SET entry_name='Dezoder. Palais Royal. Actéon'
WHERE title.id_uuid = 'qr15d19ac57ede54be8a02877b341d7c15b';


UPDATE title
SET entry_name='Lavigne et Dezoder. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr17d795445164c4393b1f8bbe39edf3659';


UPDATE title
SET entry_name='Lavigne et Dezoder. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1cd28634650924b92b3f81d22b8c1a5b9';


UPDATE title
SET entry_name='M. Dailly. Palais-Royal. Revue [Joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1e62f73e22f7045b2a698fb99574d6ed2';


UPDATE title
SET entry_name='Mlle Davray. Palais-Royal. Revue [Joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1d5b2e9d317ea487ea3477aa8b4eca271';


UPDATE title
SET entry_name='Gillette. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr18a7714b4059b4014b7c3d259841fd0bf';


UPDATE title
SET entry_name='Gillette. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr15763dfe2b2c44599a487efbd49277b6f';


UPDATE title
SET entry_name='[Gillette. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr17da801daef4f447e9f3d5a40558a4ec4';


UPDATE title
SET entry_name='Mlle Lavigne. Palais-Royal. Revue [Joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1bccd919c387a4f5c96698c0d92d6c62d';


UPDATE title
SET entry_name='Mlle Lavigne. Palais-Royal. Revue [Joyeusetés de l''année]'
WHERE title.id_uuid = 'qr135835c6d352f434c85c25f5c544e8fa5';


UPDATE title
SET entry_name='A. Lavigne. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1dd4ce918ca744833a837095517d8eccb';


UPDATE title
SET entry_name='[Renaud]. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr17bd626ff938e49b9a2af95fd7178a51c';


UPDATE title
SET entry_name='Renaud. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1ac3fa4262d844e94b6ea4ab036c6cb23';


UPDATE title
SET entry_name='Leroux. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr16e01152e8d9a45faa77c2056d7830b00';


UPDATE title
SET entry_name='Bonnet. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1e3c539bba3614844add188cca52209d2';


UPDATE title
SET entry_name='Bonnet, Dailly, Hugonnet. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1070e579cfa2d42e189e9d417c5bd13b7';


UPDATE title
SET entry_name='[Clein. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr160bf4fe4c35f4ffea1090653cfd89067';


UPDATE title
SET entry_name='Clein. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr14571a5662cc346efaf32aa1775d3a63a';


UPDATE title
SET entry_name='Clein. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr17260a477d23e4ca5a2fb5f2ca2ce525e';


UPDATE title
SET entry_name='Delbernardi. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr119bf10d5078248fc88f69ee4015597b7';


UPDATE title
SET entry_name='[Réjane. Palais-Royal]. Ma Camarade'
WHERE title.id_uuid = 'qr14a7ae2da306e4f8d9207e8e516593842';


UPDATE title
SET entry_name='Réjane. [Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr124b6b09fc5054fb4b885c830ea1311ef';


UPDATE title
SET entry_name='[Réjane. Palais-Royal. Ma Camarade]'
WHERE title.id_uuid = 'qr1429eaa0f50c34f66b4ac5e82ee09bb30';


UPDATE title
SET entry_name='Ellen Andrée. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr11132705924334bf08092801d0cd4e7f2';


UPDATE title
SET entry_name='[Le boulet, comédie de Pierre Wolff : photographie]'
WHERE title.id_uuid = 'qr19c1b673cdd6841388da7c79cbbc0f2cb';


UPDATE title
SET entry_name='Me Jane Simon [i.e. Symon]. (Palais-Royal)'
WHERE title.id_uuid = 'qr10323f59af22e4e4c9617e0c1c8be63fe';


UPDATE title
SET entry_name='Lucie d''Avray. Palais Royal'
WHERE title.id_uuid = 'qr138f6c72c61ff4ab3b570669c1739360f';


UPDATE title
SET entry_name='Me Yvonne de Courtenay. (Palais-Royal)'
WHERE title.id_uuid = 'qr10d9bde64f1c143fc905c5b5711c70cb0';


UPDATE title
SET entry_name='[Bonnet. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr18f42a2b41c224986bace68b961d0971a';


UPDATE title
SET entry_name='Elise Durieux. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr1b0dbade4654146879f0af712a81cf7e0';


UPDATE title
SET entry_name='[Mlle Marot. Palais royal]'
WHERE title.id_uuid = 'qr1b0b3d342adad49b5bc8ab7e4e0b86bdc';


UPDATE title
SET entry_name='[Dezoder. Palais Royal]'
WHERE title.id_uuid = 'qr1b1908e03c7b340bda8cfab33dddc491c';


UPDATE title
SET entry_name='Dailly. [Palais-Royal]. Les joyeusetés de l''année'
WHERE title.id_uuid = 'qr1a885bfe838814ceea531149bf93ea4f2';


UPDATE title
SET entry_name='Davray. Palais-Royal. Les joyeusetés de l''année'
WHERE title.id_uuid = 'qr17d4d2ab9f4974fa48e624e99aa5c54b0';


UPDATE title
SET entry_name='[Jane Hading. Palais royal. Actéon]'
WHERE title.id_uuid = 'qr110af77ab87ab4c63ba9eb811f0d004a3';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr1a7967f3630da44ed9069e37822d355c6';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr1b19bd2a471ad4db3b4cf3061938cfab7';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr1f0dabd23b22642a3a274105851cd7b8d';


UPDATE title
SET entry_name='Francès. Palais-Royal'
WHERE title.id_uuid = 'qr1c0de064c005a4116b1265e501eb963a9';


UPDATE title
SET entry_name='M. Terville "Palais Royal"'
WHERE title.id_uuid = 'qr1285b009b0f8141f78bbe673394761581';


UPDATE title
SET entry_name='M. Liesse. [(Palais-Royal)]'
WHERE title.id_uuid = 'qr14e04a584b81645749318c76336f5e0a8';


UPDATE title
SET entry_name='Hamilton. Palais-Royal :'
WHERE title.id_uuid = 'qr1a1736844012d4d7da67641a18ee1d9eb';


UPDATE title
SET entry_name='[Les fêtards, opérette d''Anthony Mars, Maurice Hennequin et Victor Roger : photographies]'
WHERE title.id_uuid = 'qr1d9da969aeb3445ffa6ad69e40393735f';


UPDATE title
SET entry_name='[Les Boulinard, vaudeville de Maurice Ordonneau, Albin Valabregue et Henri Kéroul : documents iconographiques]'
WHERE title.id_uuid = 'qr105ec0134a0bb4d05af6f39a7297083ea';


UPDATE title
SET entry_name='Mlle Nobert. Palais-Royal'
WHERE title.id_uuid = 'qr1bf16549145f7443dbdc7a432075617eb';


UPDATE title
SET entry_name='Francès. Palais-Royal'
WHERE title.id_uuid = 'qr17ed62cc333704136a3b1e804b99db0c9';


UPDATE title
SET entry_name='[Réjane. Palais-Royal]'
WHERE title.id_uuid = 'qr133ad2d82ea6f41ffa6560052a9b64963';


UPDATE title
SET entry_name='[Mlle Quérette. Palais royal]'
WHERE title.id_uuid = 'qr13a450b6ebc984931985a19b0b188a4fc';


UPDATE title
SET entry_name='M. Boyer. (Palais-Royal)'
WHERE title.id_uuid = 'qr1ad22c954af67466da5bcc6ad85ed2d10';


UPDATE title
SET entry_name='Raimond. Palais-Royal'
WHERE title.id_uuid = 'qr1927c40a86fba4224b6a7e82fe708b891';


UPDATE title
SET entry_name='Lavergne. Palais royal'
WHERE title.id_uuid = 'qr141a201e08ed14de1a9233b1735f8c167';


UPDATE title
SET entry_name='Aimée Samuel. Palais-Royal'
WHERE title.id_uuid = 'qr17f26475820214a9db75b0b65396ef040';


UPDATE title
SET entry_name='Aimée Samuel. Palais-Royal'
WHERE title.id_uuid = 'qr16a61ca20039544e6b2d3bb0c258360c4';


UPDATE title
SET entry_name='Francès. Palais-Royal'
WHERE title.id_uuid = 'qr1fa5f4b9731794ef38949ba6b854d8609';


UPDATE title
SET entry_name='Aimée Samuel. Palais-Royal'
WHERE title.id_uuid = 'qr12393e69d39434c9184194efb237387e8';


UPDATE title
SET entry_name='[Lucie d''Avray. Palais Royal] : [photographie, tirage de démonstration]'
WHERE title.id_uuid = 'qr10a5804120646440caefc3cce2521f711';


UPDATE title
SET entry_name='[Lucie d''Avray. Palais Royal]'
WHERE title.id_uuid = 'qr1822e06352fc346fdbf19b3fc3816930c';


UPDATE title
SET entry_name='Me Bonnet. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr160a9c003bfa04f2fb814f650d342f372';


UPDATE title
SET entry_name='M. Armand Marie. Palais-Royal'
WHERE title.id_uuid = 'qr1eb7d2d0fdcc34dcd850066538a831bf7';


UPDATE title
SET entry_name='M. Armand Marie. Palais-Royal'
WHERE title.id_uuid = 'qr198f1f42ea3214ce8af0edf28c9dc3f55';


UPDATE title
SET entry_name='Descorval. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr1f4dc80e3f37243d6ba5231a31d3f7d04';


UPDATE title
SET entry_name='Descorval. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr11ef7592977b243269493d132f99a4323';


UPDATE title
SET entry_name='Mme Thierret, Palais royal'
WHERE title.id_uuid = 'qr199c8a39362a34d6fb00e79209b122210';


UPDATE title
SET entry_name='Mme Thierret, Palais royal'
WHERE title.id_uuid = 'qr1a623625f5554436f8d129fdf739db084';


UPDATE title
SET entry_name='M. Bellucci. Palais-Royal'
WHERE title.id_uuid = 'qr11a40171e7c2b43bc99dd3ad84c5a6329';


UPDATE title
SET entry_name='Perrot. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr1c221539f172840e188aafa3f99651208';


UPDATE title
SET entry_name='Mlle Hady. Palais-Royal'
WHERE title.id_uuid = 'qr11d03283d97684476ac436a171373c574';


UPDATE title
SET entry_name='M. Bellucci. Palais-Royal'
WHERE title.id_uuid = 'qr139807e02fcd4463f892bef12ed1d01db';


UPDATE title
SET entry_name='M. Terville "Palais Royal"'
WHERE title.id_uuid = 'qr10294bbfcc632448ea5954f937631959e';


UPDATE title
SET entry_name='[Dezoder. Palais Royal]'
WHERE title.id_uuid = 'qr145642467536c4977aa822fb4224f1094';


UPDATE title
SET entry_name='[Charvet. Palais Royal]'
WHERE title.id_uuid = 'qr1407df8a05e0e4470bdab385e2f313e10';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr1d197f1ff3b0c4e2b889cbbe565e88af8';


UPDATE title
SET entry_name='Jeanne Derville. Palais-Royal'
WHERE title.id_uuid = 'qr1b4376502f21b494d9898a89ecae3bd50';


UPDATE title
SET entry_name='Jane Derville. Palais-Royal'
WHERE title.id_uuid = 'qr1d3b6c539b6e847e69e2727934f4ad910';


UPDATE title
SET entry_name='Charlotte Bie. Palais-Royal'
WHERE title.id_uuid = 'qr1ea7cd826e96b46ccb7f7b5d19247d7c0';


UPDATE title
SET entry_name='Charlotte Bié. Palais-Royal'
WHERE title.id_uuid = 'qr1d84e70f4050c4d61804b588099976b35';


UPDATE title
SET entry_name='Fournier, Cora Berti. Palais-Royal'
WHERE title.id_uuid = 'qr1cb2b8e2d07cb47a49983b056fc16f438';


UPDATE title
SET entry_name='Charlotte Bié. Palais-Royal'
WHERE title.id_uuid = 'qr105586a298c1d449cb6bbd3650f9575bb';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr1fe46a9d3632d4b67a3555781bb1cf8c0';


UPDATE title
SET entry_name='Leroux. Palais-Royal'
WHERE title.id_uuid = 'qr194a705d334f64a13afc63e2ebe45683f';


UPDATE title
SET entry_name='Me Lavigne. (Palais-Royal)'
WHERE title.id_uuid = 'qr1934e2462590b4740afacec118fde939b';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée. La Cigale. Palais Royal]'
WHERE title.id_uuid = 'qr175ae9cf8e8224565b2d5204f2c2c61bd';


UPDATE title
SET entry_name='M[lle] Marot. Palais royal'
WHERE title.id_uuid = 'qr1a315a6b0d5804ec88d4100ad3cfbafbd';


UPDATE title
SET entry_name='M[lle] Marot. Palais royal'
WHERE title.id_uuid = 'qr1ad0fd53a21584e4aac2b62c671480882';


UPDATE title
SET entry_name='[Leroux]. Palais-Royal'
WHERE title.id_uuid = 'qr1e7cd6e0398f944979fbeb11c4b41ddfc';


UPDATE title
SET entry_name='Me Delbernardi. (Palais-Royal)'
WHERE title.id_uuid = 'qr1a28c456b4ca9445cb5070cb0827bf3c3';


UPDATE title
SET entry_name='Me Delbernardi. (Palais-Royal)'
WHERE title.id_uuid = 'qr1408d2fde11584e838f10b61b9ae3ec3e';


UPDATE title
SET entry_name='[M. Leroux. (Palais-Royal)]'
WHERE title.id_uuid = 'qr1ebb0c192deaf4292ae12648eefbecece';


UPDATE title
SET entry_name='Charlot. Directeur du Palais-Royal'
WHERE title.id_uuid = 'qr1023ce7a51500422885623d59efd92520';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr167de339aa28d452dba3f1cea206c57c6';


UPDATE title
SET entry_name='Ch. Lamy. Palais-Royal : [photographie, tirage de démonstration]'
WHERE title.id_uuid = 'qr11faa360fc0aa444a86f21badce599623';


UPDATE title
SET entry_name='Gerny. Palais-Royal : [photographie, tirage de démonstration]'
WHERE title.id_uuid = 'qr1015ad14b49294fa4bb90d385dd63b970';


UPDATE title
SET entry_name='Dezoder. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr19c7b6b826be94a8d940ed9cc24160980';


UPDATE title
SET entry_name='[Le château à Toto, opéra-bouffe d''Henri Meihac, Ludovic Halévy et Jacques Offenbach]'
WHERE title.id_uuid = 'qr1e6f8583ffc534d2a80efbe2bd0681218';


UPDATE title
SET entry_name='M. Leroux. (Palais-Royal)'
WHERE title.id_uuid = 'qr1c2b76c49ccec4d01a95e293bb79ae233';


UPDATE title
SET entry_name='M. Fordyce. Palais-Royal'
WHERE title.id_uuid = 'qr17745d2fb74bd4af58dde9aa586c94725';


UPDATE title
SET entry_name='M. Garandet (Palais Royal)'
WHERE title.id_uuid = 'qr19393f174d6a3419f97227c058ab9c37a';


UPDATE title
SET entry_name='M. Colombet. Palais-Royal'
WHERE title.id_uuid = 'qr1782668ea65974df7baabaa75d4dc3816';


UPDATE title
SET entry_name='Me Lhéritier. [Palais-Royal]'
WHERE title.id_uuid = 'qr1a6eacd89c84144489744019cd3b4572e';


UPDATE title
SET entry_name='Gerny. Palais-Royal. [La revue]'
WHERE title.id_uuid = 'qr126e69fde951348d99a489e564b78c061';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr1987a48d4687042e99a73b2f8d1a42e04';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr1820c4d9790ca465bbb22313cf7f26775';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr1f674580e80d341c9a3d576c358129aec';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr1394163ba5d1746f3bfd5716318c50324';


UPDATE title
SET entry_name='M[lle] Descorval. Palais royal'
WHERE title.id_uuid = 'qr17188651a801349928fde5a77bf766690';


UPDATE title
SET entry_name='Dezoder. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr18a80ba50f6324a1b91424064975f5696';


UPDATE title
SET entry_name='Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr12ec2c5c10f86438e8f25994fa50c86a2';


UPDATE title
SET entry_name='Mlle Faber. Palais-Royal'
WHERE title.id_uuid = 'qr1b62e31737f3f40cb80c61b3d0a76a6ce';


UPDATE title
SET entry_name='Mlle Corciade. Palais-Royal'
WHERE title.id_uuid = 'qr1d6776f35e6aa4cd696418b2f2944502c';


UPDATE title
SET entry_name='Jousset. Palais-Royal'
WHERE title.id_uuid = 'qr14c3ea8d793a9477d8228a29096278150';


UPDATE title
SET entry_name='Ellen Andrée. Palais-Royal'
WHERE title.id_uuid = 'qr1d8afab1d1ac7404ebee8d38005880ced';


UPDATE title
SET entry_name='Dalmy. Palais royal'
WHERE title.id_uuid = 'qr1e9dd6e36829e45f38cf0b0afbd2e4d5a';


UPDATE title
SET entry_name='Dalmy. Palais royal'
WHERE title.id_uuid = 'qr1c2adb0527e6244d98ab64a1e2e842e68';


UPDATE title
SET entry_name='Aymé. Palais Royal'
WHERE title.id_uuid = 'qr174f4fccb62364e0ca999403a3a0b968b';


UPDATE title
SET entry_name='Aymé. Palais Royal'
WHERE title.id_uuid = 'qr10bd93740cfd7414ea1d71eb2e2a7bcf4';


UPDATE title
SET entry_name='Aymé. Palais Royal'
WHERE title.id_uuid = 'qr18a339adeeb4445778584559681edb636';


UPDATE title
SET entry_name='Didelot. Palais royal'
WHERE title.id_uuid = 'qr12f32dd5c016f42398fd851804cfce71d';


UPDATE title
SET entry_name='Didelot. Palais royal'
WHERE title.id_uuid = 'qr17798e76876fd4e569fc9f813d710eedf';


UPDATE title
SET entry_name='Lucie d''Avray. Palais Royal'
WHERE title.id_uuid = 'qr195721259ef0747c1ac4d55109cd4f360';


UPDATE title
SET entry_name='M. Charlot. Directeur. Palais royal'
WHERE title.id_uuid = 'qr15cf6c85342614835be5440eccb1da24e';


UPDATE title
SET entry_name='Faivre. Palais Royal'
WHERE title.id_uuid = 'qr1b4918d7d09a846ed8955bd474a0bbb38';


UPDATE title
SET entry_name='Faivre. Palais Royal'
WHERE title.id_uuid = 'qr188698f5da4964b0a931e8e43435fa9c2';


UPDATE title
SET entry_name='Davray. [Palais-Royal. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr11d567c92cc3b49d8ab2688edb20463a7';


UPDATE title
SET entry_name='Mlle Brésil. Palais-Royal. Femmes de paille'
WHERE title.id_uuid = 'qr1c9134143b9a64c6d8586ca0d4c2a2a86';


UPDATE title
SET entry_name='Me Nicolle Bernard. (Palais-Royal)'
WHERE title.id_uuid = 'qr16f7995f17f4a4b2ba35c548b75890c00';


UPDATE title
SET entry_name='Me Nicolle Bernard. (Palais-Royal)'
WHERE title.id_uuid = 'qr1d8def703744d4794a5ec9be5e9696426';


UPDATE title
SET entry_name='[Leroux. Palais-Royal]'
WHERE title.id_uuid = 'qr1a30e1ad43dd347a4938fdbef9a612c6a';


UPDATE title
SET entry_name='Aimée. Palais Royal. "Paris Canard"'
WHERE title.id_uuid = 'qr11edc9fee4bbb426caa76028ad7f2e724';


UPDATE title
SET entry_name='Jeanne Derville. Palais-Royal'
WHERE title.id_uuid = 'qr18583cdccbcc046c497b2e87cee9fd702';


UPDATE title
SET entry_name='Mlle Chénel. Palais-Royal'
WHERE title.id_uuid = 'qr1f8328ee041a94734861ebd3cb47fbf4c';


UPDATE title
SET entry_name='Le Brun. Palais-Royal'
WHERE title.id_uuid = 'qr1962dd65086334bb98eda9e85e0fa396e';


UPDATE title
SET entry_name='Miette. Palais-Royal'
WHERE title.id_uuid = 'qr19fb62635ff57427fbbd0ab50397522a6';


UPDATE title
SET entry_name='Le Brun. Palais-Royal'
WHERE title.id_uuid = 'qr1a6bbca5065ae4612a9ffc446efb69f5a';


UPDATE title
SET entry_name='M. Hurteaux. Palais-Royal'
WHERE title.id_uuid = 'qr1dd8dd28a60b64bd884aa8f10e7cb78a7';


UPDATE title
SET entry_name='Jeanne Derville. Palais-Royal'
WHERE title.id_uuid = 'qr119ec5b0191e04f5faf852430da1ca7f5';


UPDATE title
SET entry_name='Le Brun. Palais-Royal'
WHERE title.id_uuid = 'qr1f0bd6333690b4550bcc3ea61d0c612e8';


UPDATE title
SET entry_name='Mlle Berland. Palais-Royal'
WHERE title.id_uuid = 'qr1da5e6f5cb46f47a693ae824967ad58f9';


UPDATE title
SET entry_name='Elven. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année'
WHERE title.id_uuid = 'qr16b6cf0baed654c608fb7af824cf15267';


UPDATE title
SET entry_name='Montbazon. Ecuyère. Palais-Royal. Les joyeusetés de Paris [i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr17474229383eb4457a30850fb1fb3b6eb';


UPDATE title
SET entry_name='Elven. [Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr1733afca7c90349878fab682b6c417322';


UPDATE title
SET entry_name='Dorby. Palais-Royal'
WHERE title.id_uuid = 'qr10fd2e1f813c94d9992082844624211bd';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. Palais Royal'
WHERE title.id_uuid = 'qr1a525257435f147e5820546302f343094';


UPDATE title
SET entry_name='Gerny. [Palais-Royal]. La revue'
WHERE title.id_uuid = 'qr1d017cb316c2c41f8ac5fdf2564e183c1';


UPDATE title
SET entry_name='Gerny. [Palais-Royal]. La revue'
WHERE title.id_uuid = 'qr134c3040579a24d28a12ee89d79d5b0bc';


UPDATE title
SET entry_name='[Aymé. Palais Royal]'
WHERE title.id_uuid = 'qr12f40fefe0cac4103ab3cdd4ef265f98b';


UPDATE title
SET entry_name='[Aymé. Palais Royal]'
WHERE title.id_uuid = 'qr14425ffa82d2644b8ba9ceab88dd08a59';


UPDATE title
SET entry_name='Me Alice Lavigne. [(Palais-Royal)]'
WHERE title.id_uuid = 'qr13ad4182b176a4aec8898d1636ba3217a';


UPDATE title
SET entry_name='Me A. Lavigne. Palais-Royal'
WHERE title.id_uuid = 'qr1ee65c93ac32c49c5b85df5c38599eaac';


UPDATE title
SET entry_name='Me Alice Lavigne. (Palais-Royal)'
WHERE title.id_uuid = 'qr15a3b9a7e1d9145748d24a12632e9e9a4';


UPDATE title
SET entry_name='M. Didier. Palais-Royal'
WHERE title.id_uuid = 'qr19ea9b80e615640d9b38a97d976aa8c97';


UPDATE title
SET entry_name='M. Calvin. Palais-Royal'
WHERE title.id_uuid = 'qr1fbd892bbb7b5456189bc59d19cc3de25';


UPDATE title
SET entry_name='[Clothilde. Palais Royal]'
WHERE title.id_uuid = 'qr1c0e5919e2aa74862a5cf43b85cf9ee78';


UPDATE title
SET entry_name='Clothilde. Palais Royal'
WHERE title.id_uuid = 'qr1bf927a4c97434c6fad1e8f41c25b9d3d';


UPDATE title
SET entry_name='Me Descorval et M. Huguenet. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr110db1301fbbb48d68820173d0aa27cfb';


UPDATE title
SET entry_name='Me Milher. [Palais-Royal]'
WHERE title.id_uuid = 'qr1723eca2470a74e7cb3938a17c49c0f70';


UPDATE title
SET entry_name='Me Milher. [Palais-Royal]'
WHERE title.id_uuid = 'qr1f5816935615f4408a5ddc4294fb7fb06';


UPDATE title
SET entry_name='Marot. Palais Royal'
WHERE title.id_uuid = 'qr11f2c34da52704ff29061a3987f220aa8';


UPDATE title
SET entry_name='Charvet. Palais. Royal'
WHERE title.id_uuid = 'qr17059cb3981a84f20b364330fbac4c64b';


UPDATE title
SET entry_name='[Réjane]. Palais-Royal'
WHERE title.id_uuid = 'qr173627f901e1247638dd74981ba94ed98';


UPDATE title
SET entry_name='[Bonnet. Palais-Royal. Les joyeusetés de Paris i.e. Les joyeusetés de l''année]'
WHERE title.id_uuid = 'qr12592338c4f204dc1a5adc204d4179322';


UPDATE title
SET entry_name='Miette, Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr15ff230779cef433899de86f8fe80438c';


UPDATE title
SET entry_name='Eugène Héros. Palais-Royal'
WHERE title.id_uuid = 'qr19d721563dd6341b39571b9b591e9eead';


UPDATE title
SET entry_name='Mlle Corciade. Palais-Royal'
WHERE title.id_uuid = 'qr1dc0ba0cc639e43b2bf01af1896b64a49';


UPDATE title
SET entry_name='Jousset. Palais-Royal'
WHERE title.id_uuid = 'qr1eb27b805351f4bc1bb4c7d802a4068f8';


UPDATE title
SET entry_name='Mlle Cortez. Palais-Royal'
WHERE title.id_uuid = 'qr1d4320d9c604747469f1b8f923f0bf89a';


UPDATE title
SET entry_name='[Lucie d''Avray. Palais Royal]'
WHERE title.id_uuid = 'qr17c016ab9fdc4419a9673a563ef6086c4';


UPDATE title
SET entry_name='Aymé. Palais Royal'
WHERE title.id_uuid = 'qr198f32d96555b403a85b23b9891f16020';


UPDATE title
SET entry_name='[Lucie d''Avray. Palais Royal]'
WHERE title.id_uuid = 'qr18cdce3234eff4d10895e9e1902f391d7';


UPDATE title
SET entry_name='Legrand. Palais royal'
WHERE title.id_uuid = 'qr1f5ea7b89c2d8405f86003a96deab4ed3';


UPDATE title
SET entry_name='Mlle Nobert. Palais-Royal'
WHERE title.id_uuid = 'qr19be519f7d7b14be3a108a6af454ce653';


UPDATE title
SET entry_name='Gorby. Palais-Royal'
WHERE title.id_uuid = 'qr118b4ab5370c840e391f33105770bdc9c';


UPDATE title
SET entry_name='Andrée Ellen. Palais-Royal'
WHERE title.id_uuid = 'qr1cf74be9fc58d4121a88db46064e3620e';


UPDATE title
SET entry_name='Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr1c7fdaadb449546d6b17dd6a87b37461e';


UPDATE title
SET entry_name='Jousset. Palais-Royal'
WHERE title.id_uuid = 'qr153390193734a4c4bbff8977b82953e47';


UPDATE title
SET entry_name='Jousset. Palais-Royal'
WHERE title.id_uuid = 'qr17db4f3f5604f421897b558e0b22d053d';


UPDATE title
SET entry_name='Gorby. Palais-Royal'
WHERE title.id_uuid = 'qr11dc6cbccbbc244b2bbba43eb59b537f0';


UPDATE title
SET entry_name='Lavergne. Palais royal'
WHERE title.id_uuid = 'qr148249486b0864b078d4e77f51268e6d6';


UPDATE title
SET entry_name='Lavergne. Palais royal'
WHERE title.id_uuid = 'qr1046ac3fb07034c0ba745a5b31132d33a';


UPDATE title
SET entry_name='Lavergne. Palais royal'
WHERE title.id_uuid = 'qr12e6e80f537a44294ab1edb74d172ee80';


UPDATE title
SET entry_name='Raimond. Palais-Royal'
WHERE title.id_uuid = 'qr1ae0979d5c6894aebbffcdcae12658e8b';


UPDATE title
SET entry_name='Foncine Durieux. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr1b55b93fc7d904270ad777b5aa2aff089';


UPDATE title
SET entry_name='Lavergne. Palais royal'
WHERE title.id_uuid = 'qr1018dce4b5158463fbb035d1f07b4e896';


UPDATE title
SET entry_name='Me Yvonne de Courtenay. (Palais-Royal)'
WHERE title.id_uuid = 'qr134390b7e01954b538ff79d019130289b';


UPDATE title
SET entry_name='Cooper. Palais-Royal'
WHERE title.id_uuid = 'qr1b0102c75d4204247bfe44db9fe157446';


UPDATE title
SET entry_name='Me Miette. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr16c62bad9cc114738a1825096f0c9fd17';


UPDATE title
SET entry_name='Descorval. Palais-Royal. La Revue'
WHERE title.id_uuid = 'qr133ecb742919247268b214b91a726f7ae';


UPDATE title
SET entry_name='Mme Thierret, Palais royal'
WHERE title.id_uuid = 'qr18744ba44a9884c3f8a7992da87540538';


UPDATE title
SET entry_name='M. Boyer. (Palais-Royal)'
WHERE title.id_uuid = 'qr1362924b857a74532b5781cb725fc1187';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. La Cigale. Palais Royal'
WHERE title.id_uuid = 'qr10f860f2de59e449e8a5e6d217c503393';


UPDATE title
SET entry_name='Me Boode. Palais-Royal'
WHERE title.id_uuid = 'qr1683cd1c62ff5480fbdca17bcd725e94e';


UPDATE title
SET entry_name='Dezoder. Palais Royal'
WHERE title.id_uuid = 'qr120311f634456463e89455b681442754c';


UPDATE title
SET entry_name='Andral. Palais Royal'
WHERE title.id_uuid = 'qr10d782f5a85274f9e87f10f05b90606af';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. La Cigale. [Palais Royal]'
WHERE title.id_uuid = 'qr1bb65849de1494eb78302b522da118f81';


UPDATE title
SET entry_name='Ellen Andrée. Palais-Royal'
WHERE title.id_uuid = 'qr1f4b62093578d4213ac03d5c18b090097';


UPDATE title
SET entry_name='Ellen Andrée. Palais-Royal'
WHERE title.id_uuid = 'qr1d29d1722fc484226a933b59baf635733';


UPDATE title
SET entry_name='Elven. Palais-Royal'
WHERE title.id_uuid = 'qr160f0ad125911462a8d875a67d9d9c077';


UPDATE title
SET entry_name='Elven. Palais-Royal'
WHERE title.id_uuid = 'qr17594a619b9bd434da5c72047da948d53';


UPDATE title
SET entry_name='Miette, Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr155cab2ab57c44fe7979401496e6cc33d';


UPDATE title
SET entry_name='Gerny. Palais-Royal'
WHERE title.id_uuid = 'qr198d25a11f88b46b4a618c60e43950179';


UPDATE title
SET entry_name='M. Galipaux. Palais-Royal'
WHERE title.id_uuid = 'qr1199053478529430b829f79e904f80293';


UPDATE title
SET entry_name='Jane Delorme. Palais-Royal'
WHERE title.id_uuid = 'qr1cc0fe75916e94ac482a2ff98ff5f9f59';


UPDATE title
SET entry_name='[Réjane]. Palais-Royal'
WHERE title.id_uuid = 'qr19cba721c05bd43378e2cefba3a9dda22';


UPDATE title
SET entry_name='M[lle] Quérette. Palais royal'
WHERE title.id_uuid = 'qr15df324083e334874a7890020196acd84';


UPDATE title
SET entry_name='M[lle] Quérette. Palais royal'
WHERE title.id_uuid = 'qr10fe14e7612804b79b6ec670d6f627931';


UPDATE title
SET entry_name='Aimée. Palais Royal. "Paris Canard"'
WHERE title.id_uuid = 'qr17f394f797c964f9c9a5d4f779cd7bb08';


UPDATE title
SET entry_name='Charvet. Palais Royal'
WHERE title.id_uuid = 'qr1af2747c95b0c466290b8343f023cb51e';


UPDATE title
SET entry_name='M. Colombet. [Palais-Royal]'
WHERE title.id_uuid = 'qr1f123e398f0e44d7db5ea22204cb8f457';


UPDATE title
SET entry_name='M. Fordyce. Palais-Royal'
WHERE title.id_uuid = 'qr142e7a5ae17fa43938b2bf2b397edcc11';


UPDATE title
SET entry_name='[Me Renaud]. Palais-Royal'
WHERE title.id_uuid = 'qr1ba1ec8d840e643f78499e744b262ac19';


UPDATE title
SET entry_name='M. Liesse. (Palais-Royal)'
WHERE title.id_uuid = 'qr12b6612db2ddb4609a9d9bc4ea22de811';


UPDATE title
SET entry_name='M[lle] Jousset. Palais royal'
WHERE title.id_uuid = 'qr1184ff00f889c4e728500f885a3b26202';


UPDATE title
SET entry_name='Mlle Rerwick. [Palais-Royal]'
WHERE title.id_uuid = 'qr1009b507eee8f4779bc9e671ec94a8102';


UPDATE title
SET entry_name='[Leroux. Palais-Royal]'
WHERE title.id_uuid = 'qr1829784def7734c509435d5d50560abb2';


UPDATE title
SET entry_name='Me Renaud. Palais-Royal'
WHERE title.id_uuid = 'qr1bbe6fbf060ed461fb4a0fb9473284e3e';


UPDATE title
SET entry_name='Me Lhéritier. [Palais-Royal]'
WHERE title.id_uuid = 'qr150a7d9906fea4df7943ee66bb5fe3873';


UPDATE title
SET entry_name='Delbernardi. Palais-Royal'
WHERE title.id_uuid = 'qr12b4ce86255c948e99fea099b0aee098a';


UPDATE title
SET entry_name='Delbernardi. Palais-Royal'
WHERE title.id_uuid = 'qr1c99c6ccd6430444a9af3f10dbae12161';


UPDATE title
SET entry_name='Charles Lamy. Palais-Royal'
WHERE title.id_uuid = 'qr103e17c622ac54411983de8f65fea159f';


UPDATE title
SET entry_name='M. Bellot. Palais-Royal'
WHERE title.id_uuid = 'qr1e8a1ebd01ee14c4785f56a1b29683b1f';


UPDATE title
SET entry_name='Me Yvonne de Courtenay. (Palais-Royal)'
WHERE title.id_uuid = 'qr15c784d3359d4482ca7042e287ba70448';


UPDATE title
SET entry_name='Me Yvonne de Courtenay. (Palais-Royal)'
WHERE title.id_uuid = 'qr1d3e78e48b03d42e58c5ba2c036fd6238';


UPDATE title
SET entry_name='Miette, Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr1e82331d748cf453495b7a9d675060766';


UPDATE title
SET entry_name='M. Boisselot. Palais-Royal'
WHERE title.id_uuid = 'qr1d7d7a2ca00be4e00944fbfd125ad0f65';


UPDATE title
SET entry_name='Legrand. Palais royal'
WHERE title.id_uuid = 'qr1fce3182cf6024fa68492ff3247e27997';


UPDATE title
SET entry_name='Descorval. Palais-Royal. La Revue'
WHERE title.id_uuid = 'qr1b38da3fe43e244788cf8f49d5246eea1';


UPDATE title
SET entry_name='[Charvet. Palais Royal]'
WHERE title.id_uuid = 'qr1ef3edfff32fc47efaa108215279d8469';


UPDATE title
SET entry_name='Derauny. Palais-Royal'
WHERE title.id_uuid = 'qr1beb7f6097faf4033afe9fa17ada6e0f1';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. La Cigale. [Palais Royal]'
WHERE title.id_uuid = 'qr15d512800d0a04cdfb3a801bc868d2df3';


UPDATE title
SET entry_name='M. Saint-Germain. [Palais-Royal]'
WHERE title.id_uuid = 'qr16a8b6b9b283e463286ce4c203d0fc07c';


UPDATE title
SET entry_name='M. Calvin. [Palais-Royal]'
WHERE title.id_uuid = 'qr11be2ced6937549dbb2e8a928307cb5a5';


UPDATE title
SET entry_name='M. Bellot. [Palais-Royal]'
WHERE title.id_uuid = 'qr187844bce422748f3b002613bc5ca0d95';


UPDATE title
SET entry_name='Mlle Rerwick. Palais-Royal'
WHERE title.id_uuid = 'qr19aadea054baf483bab19754cd73fc4c0';


UPDATE title
SET entry_name='Me Renaud. [Palais-Royal]'
WHERE title.id_uuid = 'qr1af898194c06f4b48bf60229bd3687854';


UPDATE title
SET entry_name='M. Garandet (Palais Royal)'
WHERE title.id_uuid = 'qr1b033b90ca8504b5db59990d29ebdec0f';


UPDATE title
SET entry_name='Charvet. Palais Royal'
WHERE title.id_uuid = 'qr141e55bb1947f4cc0bfc1cfda13d21a4a';


UPDATE title
SET entry_name='M[lle] Descorval. Palais royal'
WHERE title.id_uuid = 'qr1b425ff23eb0a4b26969ec3a1dd5dba7d';


UPDATE title
SET entry_name='Elven. Palais-Royal'
WHERE title.id_uuid = 'qr181cf01201fca4360accb441937d03114';


UPDATE title
SET entry_name='Elven. Palais-Royal'
WHERE title.id_uuid = 'qr16dd2b2fe8e0f4527b62532f10c37d48a';


UPDATE title
SET entry_name='Dezoder. Palais-Royal'
WHERE title.id_uuid = 'qr179ee962ae6c6467eb56fc737cc683451';


UPDATE title
SET entry_name='Ellen Andrée. Palais-Royal'
WHERE title.id_uuid = 'qr1c1c0aef5b2744804b7fa4f0986779b8b';


UPDATE title
SET entry_name='[Mlle Quérette. Palais royal]'
WHERE title.id_uuid = 'qr1a6e952bfd74b40a089f34d144f34de60';


UPDATE title
SET entry_name='Foncine Durieux. Palais-Royal. La revue'
WHERE title.id_uuid = 'qr1a74fa198413c4e10b18b6e04c5590d4e';


UPDATE title
SET entry_name='Elise Durieux. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr1c632b8aa7be641528f33e599fd28aa29';


UPDATE title
SET entry_name='[Mlle Hady. Palais-Royal]'
WHERE title.id_uuid = 'qr1f9eff5ec820c4a6aa4b096b4f4bd7f6a';


UPDATE title
SET entry_name='[Me Renaud. Palais-Royal]'
WHERE title.id_uuid = 'qr19e2b167088b04ea7b6c3f50fa68dba8e';


UPDATE title
SET entry_name='Gerny. [Palais-Royal]. La revue'
WHERE title.id_uuid = 'qr1500693a3f0924b6db7fd302644491ed5';


UPDATE title
SET entry_name='M. Mesmaecker père. [Palais-Royal]'
WHERE title.id_uuid = 'qr1a06c49e3f0bf41e3b7387aae8aa75be2';


UPDATE title
SET entry_name='M. Mesmaecker père. [Palais-Royal]'
WHERE title.id_uuid = 'qr14510d1c2fa094ff0bc780cba2fe5653a';


UPDATE title
SET entry_name='Me A. Lavigne. Palais-Royal'
WHERE title.id_uuid = 'qr169da0f9a1660470897e27d9ad586343b';


UPDATE title
SET entry_name='M. Saint-Germain. [Palais-Royal]'
WHERE title.id_uuid = 'qr1068fe8491e444c30bf3caa34124b726d';


UPDATE title
SET entry_name='M. Saint-Germain. [Palais-Royal]'
WHERE title.id_uuid = 'qr1e8959d02603142ceb521dfa120ed40c1';


UPDATE title
SET entry_name='M. Saint-Germain. [Palais-Royal]'
WHERE title.id_uuid = 'qr154aaf5cd26ce4fd9b0683400828fd5d1';


UPDATE title
SET entry_name='Me Lavigne. (Palais-Royal)'
WHERE title.id_uuid = 'qr1e39fbc8a2e1b469cb84abae4a2ac940a';


UPDATE title
SET entry_name='Me Franck-Mel. Palais-Royal'
WHERE title.id_uuid = 'qr18af90a17e2d54cad919c9c2e7d4c5224';


UPDATE title
SET entry_name='Me Franck-Mel. Palais-Royal'
WHERE title.id_uuid = 'qr1b550a1308eb042d0a9b002cbbc5f0915';


UPDATE title
SET entry_name='Me Cheirel. [Palais-Royal]'
WHERE title.id_uuid = 'qr13c96cea8d9c34add8dd82ba68a9a6652';


UPDATE title
SET entry_name='Me Cheirel. [Palais-Royal]'
WHERE title.id_uuid = 'qr18f64a38c99554880a5b1a6b2de9da341';


UPDATE title
SET entry_name='Marot. Palais Royal'
WHERE title.id_uuid = 'qr1bb55a7744a5343ccb6e6ce4728329f2f';


UPDATE title
SET entry_name='Mes Lavigne, Renaud et Dézoder. Palais-Royal. Revue'
WHERE title.id_uuid = 'qr111b009b40a6c49899a1f9762a1b65b23';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. La Cigale. Palais Royal'
WHERE title.id_uuid = 'qr1b7f507c03dc546a88626c4a66c97d3ca';


UPDATE title
SET entry_name='[Ellen André i.e. Andrée]. Palais Royal'
WHERE title.id_uuid = 'qr13364ac72cc8a4ac290711e5dab9468ce';


UPDATE title
SET entry_name='M. Didier. [Palais-Royal]'
WHERE title.id_uuid = 'qr18245f79011344f5b971e66fc8fc5e60e';


UPDATE title
SET entry_name='Le duc de Chartres'
WHERE title.id_uuid = 'qr1075e57a3ce1e4bbfbf9b4398040965bb';


UPDATE title
SET entry_name='[Le café des comédiens, vaudeville en 1 acte d''Hippolyte et Théodore Cogniard : estampe]'
WHERE title.id_uuid = 'qr141feeb0b7857419a9d087f8be822696b';


UPDATE title
SET entry_name='Théâtre du Palais Royal : Alcide Tousez, garde champêtre (dans les baigneuses). Mme Leménil (dans la canne et le parapluie). Sainville (roi des pommes de terre). Ravel (Jonathas). Nathalie, la mazurka (dans les pommes de terre), Grassot (dans son rôle du Boeuf gras) : [estampe]'
WHERE title.id_uuid = 'qr1788684b3c79945a88408ca119b334d4a';


UPDATE title
SET entry_name='M.lle Schneider, rôle de Bérénice Lamazou, [dans La mariée du Mardi-Gras], Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1037cb8d0567944479c67b7306256f9fe';


UPDATE title
SET entry_name='Théâtre du Palais Royal. La Marquise de Prétintaille'
WHERE title.id_uuid = 'qr16e666f320cb64e589b5dbabe6c4be804';


UPDATE title
SET entry_name='M.lle Dubouchet, rôle de Colombine, dans Les trois fils de Cadet Roussel, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr1de4025f2fcda4fbe910b39a95da63cd8';


UPDATE title
SET entry_name='[Costumes du Théâtre du Palais-Royal : dessins / de Lassouche]'
WHERE title.id_uuid = 'qr10b8298a34d754df1bfc282f541c90664';


UPDATE title
SET entry_name='M.me Duverger dans Sylvandire, 5.e acte, Théâtre du Palais Royal'
WHERE title.id_uuid = 'qr18c373dd66d734590a8199bc49a9bdaec';


UPDATE title
SET entry_name='Jacqueline Delubac, souvenirs de "La grande vie" au Palais-Royal, soubrette impertinente vue sans pertinence'
WHERE title.id_uuid = 'qr1ddc5797f6eb346f98ded760a63a99447';


UPDATE title
SET entry_name='Tournée Lassouche du Palais Royal et des Variétés. Un troupier qui suit les bonnes'
WHERE title.id_uuid = 'qr167e5c504ea4746a8b4ec48fb807589a8';


UPDATE title
SET entry_name='[Jean Torgnole, comédie de Lambert-Thiboust et Eugène Grangé]'
WHERE title.id_uuid = 'qr10b9ee66c125e45f1aa88f5a180e73cee';


UPDATE title
SET entry_name='Beef Chocolat [...] à la viande crue stérilisée des trappistines de Lyon (sans arrière goût de viande) [...]. Chocolaterie de la Drôme (Tain) [...]. Dégustation & vente péristyle de Chartres, 19-20-21, Palais-Royal [...]'
WHERE title.id_uuid = 'qr1b2cbb611ebb1475285ebc356f031e59e';


UPDATE title
SET entry_name='[Les deux Canards : maquette d''affiche : neuf comédiens et deux canards]'
WHERE title.id_uuid = 'qr14bc5153783b04ffeb237c4d692b1a717';


UPDATE title
SET entry_name='Aimée Samuel et Lucy Jousset'
WHERE title.id_uuid = 'qr1cd27c81751d3407bbaa80e3310999890';


UPDATE title
SET entry_name='Affiche indiquant que le géant P. Butterbrodt se voit au Palais-Royal, aux entresols du sieur Curtius'
WHERE title.id_uuid = 'qr1ff4e4567a5774ba1910a45f3548cf5e4';


UPDATE title
SET entry_name='[Les chemins de fer, vaudeville d''Eugène labiche, A. Delacour et A. Choler]'
WHERE title.id_uuid = 'qr1689cbd49ce394b6ab8959f2e299dd34a';


UPDATE title
SET entry_name='Théâtre du Palais-Royal. Les Fêtards, opérette. Paroles de Antony Mars et Maurice Hennequin. Musique de Victor Roger.'
WHERE title.id_uuid = 'qr1fc6ec49ddf0641b9ba29f55e71b148d7';


UPDATE title
SET entry_name='...La Captive des Mohawks par Edward S. Ellis En vente à la librairie de E. Dentu... Palais Royal...Edition illustrée de 17 jolies gravures...'
WHERE title.id_uuid = 'qr14d947a1bab994f75b317683497dac120';


UPDATE title
SET entry_name='[Rosine Maurel, Mistinguett et Adrien Le Gallo dans "Tais-toi, mon coeur !" de Maurice Hennequin, Pierre Veber]'
WHERE title.id_uuid = 'qr1beef7a699217470e91cffc50edad712e';


UPDATE title
SET entry_name='AD075PH_UPF1309'
WHERE title.id_uuid = 'qr1b66c7eda72ff4bc3802a0bd70a61a97b';


UPDATE title
SET entry_name='Madeleine Dolley dans le Petit Café, pièce en 3 actes de M. Tristan Bernard, Théatre du Palais-Royal'
WHERE title.id_uuid = 'qr19ee80fca36bc4633904906df021bd352';


UPDATE title
SET entry_name='"Les Mystères du Palais-Royal" par Xavier de Montépin 10c. la livraison illustrée...'
WHERE title.id_uuid = 'qr17145efc2866145b7a1b462466bec3582';


UPDATE title
SET entry_name='Homme et Femme de qualité, en conversation au Palais Royal'
WHERE title.id_uuid = 'qr1c74e415144d64f9f9d055cff5141e7ce';


UPDATE title
SET entry_name='[Georges Hurteaux, Marcelle Yrven et Georges Berr dans "Le satyre" de Georges Berr et Marcel Guillemaud]'
WHERE title.id_uuid = 'qr18eeb83e39db643dcb4b282cf27da4d6c';


UPDATE title
SET entry_name='Yrven et Templey'
WHERE title.id_uuid = 'qr1560ba2d904ad42d69649d0d9b23326c8';


UPDATE title
SET entry_name='Théâtre du Palais-Royal. Château à Toto. Musique de J. Offenbach. Opéra-bouffe en 3 actes. Paroles de MM. H. Meilhac et L. Halévy'
WHERE title.id_uuid = 'qr1cddb9aa12ba643159ef0f1c839920beb';


UPDATE title
SET entry_name='Prochainement Fusier du Théâtre du Palais Royal...'
WHERE title.id_uuid = 'qr110bf2e1173cc4ddfaa7c44b65409d041';


UPDATE title
SET entry_name='[Henri Cooper dans "La poudre aux moineaux" de Maurice Desvallières et Lucien Gleize]'
WHERE title.id_uuid = 'qr104e1d1bdb1e249e1ac59a0231bcf53d0';


UPDATE title
SET entry_name='[Adrien Le Gallo dans "Aimé des femmes" de Maurice Hennequin et Georges Mitchell]'
WHERE title.id_uuid = 'qr18f09810c875640b3b4cd70955e0a27bc';


UPDATE title
SET entry_name='[Léonie Yahne dans "Madame Gribouille" de Abel Tarride et Adolphe Chenevière]'
WHERE title.id_uuid = 'qr104383145ee544ef19f7c25581c313b4c';


UPDATE title
SET entry_name='Calvat [et] Danjou'
WHERE title.id_uuid = 'qr19cd9fd3d8de1410286649cc468970b6b';


UPDATE title
SET entry_name='[Germain, Armande Cassive et Adrien Le Gallo dans "La présidente" de Maurice Hennequin, Pierre Veber]'
WHERE title.id_uuid = 'qr1eb8e8df5f16f42b5a06dea495a68fee0';


UPDATE title
SET entry_name='[Mademoiselle Sallé, vaudeville de Bayard, Dumanoir, Saintine : costume de Virginie Dejazet (Mademoiselle Sallé)]'
WHERE title.id_uuid = 'qr11bbb3366902f490e88433b3a47d968d8';


UPDATE title
SET entry_name='[Les Willis, vaudeville de Jules Cordier et Pittaud de Forges : costume de Charlotte Dupuis (Mina)]'
WHERE title.id_uuid = 'qr1ec923e8da5b44df78ba0f4783623c573';


UPDATE title
SET entry_name='Théâtre du Palais Royal. La Croix d''Or'
WHERE title.id_uuid = 'qr11f9b9343314e48f68b6bf63a05195af1';


UPDATE title
SET entry_name='La comtesse du Tonneau. Acte 2e : Revue du Théâtre. Palais Royal'
WHERE title.id_uuid = 'qr10d29418c049542aaaf70741fbe1992b7';


UPDATE title
SET entry_name='Le triolet bleu : Théâtre du Palais Royal / Bourdet [sig.] ; lith. de Benard'
WHERE title.id_uuid = 'qr1c57f109f521f4f258c95c1ad54502599';


UPDATE title
SET entry_name='Les folies dramatiques. Vaudeville en cinq actes par MM. Dumanoir et Clairville représenté pour la première fois, à Paris, sur le théâtre du Palais Royal, le 2 mars 1853'
WHERE title.id_uuid = 'qr1be505cfe534643c9806a1801b88bd61d';


UPDATE title
SET entry_name='Suzanne, deuxième acte, théâtre du Palais Royal : Le monde dramatique'
WHERE title.id_uuid = 'qr1a431319f7c5d4e5ebc8f16dbaabf4fb6';


UPDATE title
SET entry_name='Le train de plaisir, par MM. Hennequin, Mortier et Saint-Albin, Théâtre du Palais-Royal : chronique théâtrale'
WHERE title.id_uuid = 'qr162a19a45ec0641de8dad687577acda7d';


UPDATE title
SET entry_name='Théâtre du Palais-Royal. L''Enfant du Faubourg'
WHERE title.id_uuid = 'qr10c27c98e4c944983bfa1a0f690ddf472';


UPDATE title
SET entry_name='métamorphoses du jour par Grandville, texte par H. de Beaulieu... On souscrit chez Garnier frères, 6 rue des Saints-Pères, et Palais-Royal, 215'
WHERE title.id_uuid = 'qr1fb997af9627c40ea8a39600202be720e';


UPDATE title
SET entry_name='Mode du jour N° 5'
WHERE title.id_uuid = 'qr132ffe9a00cde49feaea446947267ba43';


UPDATE title
SET entry_name='Chit Chit !... : [estampe] / tiré du cabinet du citoyen Darlet'
WHERE title.id_uuid = 'qr19c292e9172f74cbb976f81cfdb3f3a17';


UPDATE title
SET entry_name='Chit chit... ; Par ici !...'
WHERE title.id_uuid = 'qr116d2d53b494b46509f7ce12650075681';


UPDATE title
SET entry_name='[Un fil à la patte, comédie de Georges Feydeau : estampes]'
WHERE title.id_uuid = 'qr1d4ba8c84e1874d8c82481767205cb896';


UPDATE title
SET entry_name='[Le brésilien, comédie d''Henri Meilhac et Ludovic Halévy : documents iconographiques]'
WHERE title.id_uuid = 'qr1eaba6222e7d746e3913e3981fc9e2443';


UPDATE title
SET entry_name='[La permission de dix heures, vaudeville de Melesville et Carmouche : costume d''Achard (la Rose Pompom)]'
WHERE title.id_uuid = 'qr15618c8fe532846e0af3210ab5e68ef09';


UPDATE title
SET entry_name='[Les joyeusetés de l''année, revue d''Albert de Saint Albin : dessins de costumes / par Job]'
WHERE title.id_uuid = 'qr1eb7c5fad43a7467c8c6733e8c8641875';


UPDATE title
SET entry_name='[Les petites Godin, vaudeville de Maurice Ordonneau : photographies]'
WHERE title.id_uuid = 'qr12610b9e5c07f40089a73d99af3e1d064';


UPDATE title
SET entry_name='[Le dindon, pièce de Georges Feydeau : documents iconographiques]'
WHERE title.id_uuid = 'qr17d8507fedaac42bb95b016c32127a318';


UPDATE title
SET entry_name='[La culotte, vaudeville de Louis Artus et André Sylvane : documents iconographiques]'
WHERE title.id_uuid = 'qr11be5c5c77a8345d8a89c5609402865d3';


UPDATE title
SET entry_name='Montre chronomètre'
WHERE title.id_uuid = 'qr14a77c502417040619f3b1be1029f2fd9';


UPDATE title
SET entry_name='Montre demi-chasseur'
WHERE title.id_uuid = 'qr105874fa1b25540438aa1347d49dfd509';


UPDATE title
SET entry_name='Montre d''aveugle'
WHERE title.id_uuid = 'qr1d7642b375aad40e4b76663fcba5b7bd4';


UPDATE title
SET entry_name='Papier de montre'
WHERE title.id_uuid = 'qr1b4aeeeb0a9e44321af371e0759b6b585';


UPDATE title
SET entry_name='Mécanisme d''alarme'
WHERE title.id_uuid = 'qr11c65fbec184b43ca8ba59fe7fde287c6';


UPDATE title
SET entry_name='Montre calendrier ; montre lunaire'
WHERE title.id_uuid = 'qr1e573f99aeeae489495b3de9b889cd8eb';


UPDATE title
SET entry_name='Montre'
WHERE title.id_uuid = 'qr12cc4c674484c42a1ad798c56d53a4f32';


UPDATE title
SET entry_name='Cadran pilier ; cadran d''altitude ; boîte'
WHERE title.id_uuid = 'qr1a4491779145f4b8c963534f04725dfca';


UPDATE title
SET entry_name='Collier ; coffret à bijoux'
WHERE title.id_uuid = 'qr1698c59f0166f43f292e06b0897265dee';


UPDATE title
SET entry_name='Soucoupe'
WHERE title.id_uuid = 'qr14d94db8715784f9d9aa145fcf324255f';


UPDATE title
SET entry_name='A peep at the French Monstrosities : le Palais Royal de Paris'
WHERE title.id_uuid = 'qr1713b34ab4c4c420a83f5049a705c5fe3';


UPDATE title
SET entry_name='La Galerie du Palais Royal'
WHERE title.id_uuid = 'qr16fb93826f7f0450d804abc9910d63a7c';


UPDATE title
SET entry_name='Le repas du chat ou honni soit qui mal y pense'
WHERE title.id_uuid = 'qr11239c8e5eea8499ab41a44f2189f19d3';


UPDATE title
SET entry_name='La Brûlure'
WHERE title.id_uuid = 'qr14eddedcdcc274ae6b50693fa89b90584';


UPDATE title
SET entry_name='Spéculateur dramatique'
WHERE title.id_uuid = 'qr198079d35937e46d9b76287d3b2701204';


UPDATE title
SET entry_name='Chute de l''arbre de Cracovie'
WHERE title.id_uuid = 'qr12140a74cda814e06920bf932fb7d43d6';


UPDATE title
SET entry_name='Modes de Paris : petit courrier des dames'
WHERE title.id_uuid = 'qr13ff8464659024fda9e022f7ea46c09d5';


UPDATE title
SET entry_name='Jardins du Palais-Royal'
WHERE title.id_uuid = 'qr1e0e1a3547d1445749aae6e9160e91426';


UPDATE title
SET entry_name='Palais-Royal (jardin)'
WHERE title.id_uuid = 'qr171b0fc7e58e24402adb0882e516ff980';


UPDATE title
SET entry_name='La Promenade publique au Palais-Royal en 1792'
WHERE title.id_uuid = 'qr1ffa6190bdc5e44658dcdb4271149cea7';


UPDATE title
SET entry_name='Projet pour le reposoir du Palais-Royal lors de la Fête-Dieu de 1736. La Fête-Dieu, ou fête du Saint-Sacrement, se tenait le jeudi suivant la fête de la Trinité, soixante jours après Pâques – en France, elle est aujourd’hui fêtée le dimanche qui suit. Des processions fastueuses étaient organisées dans Paris, ponctuées de stations à des reposoirs. La construction éphémère de celui-ci, commandé à Servandoni en 1736 par le duc d’Orléans, éblouit ses contemporains.'
WHERE title.id_uuid = 'qr1e7e64fd15271433c9725b5abe52675ab';


UPDATE title
SET entry_name='Ruines de la Commune - le Palais-Royal, place du Palais-Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1fbbd7074d0394490b8bb43600f2e2e64';


UPDATE title
SET entry_name='L''œuvre des femmes de France. La quête faite par les dames du Palais-Royal pendant la représentation donnée au profit de l''œuvre'
WHERE title.id_uuid = 'qr1c005a4fc3fec4e0fbf2b47d42c7a82f6';


UPDATE title
SET entry_name='Palais-Royal, place du Palais-Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16e20b2f1cdfe4579a161bc8dda82d99b';


UPDATE title
SET entry_name='La Place du Palais-Royal, au clair de lune'
WHERE title.id_uuid = 'qr1661e42056f124f959030f37760e318f5';


UPDATE title
SET entry_name='Palais-Royal'
WHERE title.id_uuid = 'qr1366fe6a79f6e4308adb29a1c245fd6d4';


UPDATE title
SET entry_name='Plan du salon à l’italienne du Palais-Royal et du petit appartement du Régent : variante de la desserte par l''escalier'
WHERE title.id_uuid = 'qr1e666b0e5ab0a49089b2aed1390dd17c5';


UPDATE title
SET entry_name='Comédie Française : 1680-1687 rue Mazarine, 1687-1770 rue de l''Ancienne Comédie, à partir de 1802 Palais Royal, Palais Royal : jardin, cirque, scènes diverses, vues sur le jardin depuis 1780. / E (Titre de la série)'
WHERE title.id_uuid = 'qr13ade7cb7e0d3403abb19094f5ae20d79';


UPDATE title
SET entry_name='Vue générale du Palais-Royal'
WHERE title.id_uuid = 'qr1b04f21bbc75a4c418dfe6caecf8a16e9';


UPDATE title
SET entry_name='Galeries du Palais-Royal'
WHERE title.id_uuid = 'qr1501002b9b99f4433b032720bc972e357';


UPDATE title
SET entry_name='Prise du château d''eau, place du Palais-Royal, le 24 février 1848'
WHERE title.id_uuid = 'qr1e4560a53b927435892835ca54cc118b8';


UPDATE title
SET entry_name='Plan des galeries du Palais-Royal, rue de Richelieu, Ier arrondissement, Paris'
WHERE title.id_uuid = 'qr113a51caf370840dd94c7b2bf0cf1cf7f';


UPDATE title
SET entry_name='Jardins du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr12f57247f06594cd48acd23ee1ba81945';


UPDATE title
SET entry_name='Jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr183ba69d6b52f436a8547d841582050a3';


UPDATE title
SET entry_name='Jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ea0d55ee2087482ab6c4e0f2e955386c';


UPDATE title
SET entry_name='Salle du théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr114a40d232ae4494487d7239580a844c9';


UPDATE title
SET entry_name='La galerie d''Orléans au Palais-Royal'
WHERE title.id_uuid = 'qr100ab2b441eda4f0b8176353399b6cba9';


UPDATE title
SET entry_name='Galerie du Palais-Royal (fragment?)'
WHERE title.id_uuid = 'qr1585f787e24174a88bc27fdadfb202017';


UPDATE title
SET entry_name='Portrait de Howey, (actrice au Palais-Royal en 1867)'
WHERE title.id_uuid = 'qr15b7466d7825c4d0fa58281773b0f10eb';


UPDATE title
SET entry_name='Portrait de Cazalès (actrice au théâtre du Palais-royal et des Variétés)'
WHERE title.id_uuid = 'qr117b51f61926044ae805515085795201a';


UPDATE title
SET entry_name='Portrait de Cazalès (actrice au théâtre du Palais-royal et des Variétés)'
WHERE title.id_uuid = 'qr1a5ea600e6ed64d5696630b574bb2e7b4';


UPDATE title
SET entry_name='Portrait de Rose Blanche (actrice au Théâtre du Palais-Royal et à l''Athénée)'
WHERE title.id_uuid = 'qr114787dff63fc45dcbe5f94bd9498e49f';


UPDATE title
SET entry_name='Portrait de Hervé, (Florimond Ronger, dit), (1825-1892), compositeur (opérette) et chef d''orchestre du Palais-Royal en 1851'
WHERE title.id_uuid = 'qr161f1223c042449738f326223dbaedcd1';


UPDATE title
SET entry_name='Portrait de Hervé, (Florimond Ronger, dit), (1825-1892), compositeur (opérette) et chef d''orchestre du Palais-Royal en 1851'
WHERE title.id_uuid = 'qr19d010311fdf54fffbb0419c13b4157ad';


UPDATE title
SET entry_name='Portrait de Hervé, (Florimond Ronger, dit), (1825-1892), compositeur (opérette) et chef d''orchestre du Palais-Royal en 1851'
WHERE title.id_uuid = 'qr1567cbcb576de4bb0920df3da432c940d';


UPDATE title
SET entry_name='Portrait de Howey, (actrice au Palais-Royal, 1867)'
WHERE title.id_uuid = 'qr18f8ff6a84b51445b82c4969871821873';


UPDATE title
SET entry_name='Portrait de Howey, (actrice au Palais-Royal, 1867)'
WHERE title.id_uuid = 'qr1dd4a00cb1ecd43b990aa1d8c1602ebb7';


UPDATE title
SET entry_name='La place du Palais-Royal en 1813'
WHERE title.id_uuid = 'qr18f4d6075b51a453ab9d5945d8be58684';


UPDATE title
SET entry_name='Jardins du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16a748a2b44714f11a192fdbf11f90a4f';


UPDATE title
SET entry_name='Jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr12e71852e139349c4b3d151f0054b0290';


UPDATE title
SET entry_name='Prise du château d''eau, place du Palais-Royal, le 24 février 1848'
WHERE title.id_uuid = 'qr119fbad2a6a0d423ca8a8caf45d76d786';


UPDATE title
SET entry_name='Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14bcb2c7b29ae4922b6b2e65ff459d429';


UPDATE title
SET entry_name='Le Monde Parisien/M.Hyacinthe, du Palais-Royal'
WHERE title.id_uuid = 'qr1a1501f27fcbc49818eecfba2e53ee877';


UPDATE title
SET entry_name='Le Palais-Royal de 1789 à 1804'
WHERE title.id_uuid = 'qr10a76093d9c124bf1824a09c950dd0cfc';


UPDATE title
SET entry_name='Le Palais-Royal sous Louis XVI'
WHERE title.id_uuid = 'qr1bdc10dcb3c264b95b61a3e8ac408f451';


UPDATE title
SET entry_name='Portrait de Berthou, actrie au Palais-Royal'
WHERE title.id_uuid = 'qr161f9447109da4a08b05a2551986c0249';


UPDATE title
SET entry_name='Portrait de Berthou, actrie au Palais-Royal'
WHERE title.id_uuid = 'qr19b2bbc60818645619020a046eb6b4cc2';


UPDATE title
SET entry_name='Portrait de Berthou, actrie au Palais-Royal'
WHERE title.id_uuid = 'qr1c1242267e108420dad4c896dbd96f81e';


UPDATE title
SET entry_name='Vue de la cour du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c97098f17def41a0b254459b649316fb';


UPDATE title
SET entry_name='Caveau des aveugles. Palais-Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr19b6a3dd53224411393a260bc6a97ac5e';


UPDATE title
SET entry_name='Caveau des aveugles. Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ad7c9e121cf648b9b5af348dac0bf067';


UPDATE title
SET entry_name='Caveau des aveugles. Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1213cc01eec8d4f68b2fc37cc2c7009a1';


UPDATE title
SET entry_name='Caveau des aveugles. Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18f4078be73074649b2a12e9f1dbcd013';


UPDATE title
SET entry_name='Façade incendiée du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr19663fa9bbf884b38957a0d214cffdf7e';


UPDATE title
SET entry_name='Façade incendiée du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18937eb5aca1047799e23d95b8aacb818';


UPDATE title
SET entry_name='Galerie d''Orléans du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1cc723de0e9794369a3603938c8be7c42';


UPDATE title
SET entry_name='Galerie d''Orléans du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr15e77ba6a7e874f90a38e89465cb6a255';


UPDATE title
SET entry_name='Bassin du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1a8b8dcf931e749668d0c9c81fc16f071';


UPDATE title
SET entry_name='Bassin du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bebeda37fdce4b22b74ed52651fcbb87';


UPDATE title
SET entry_name='Promeneurs au jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr15b1fc2a56e7842bf8ba9e5e9fd47753a';


UPDATE title
SET entry_name='Une allée du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16821ff40551e4be39bdc76be438d5fbc';


UPDATE title
SET entry_name='Une allée du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1081d18871e5444a0b8ce21cb8653045b';


UPDATE title
SET entry_name='Une allée du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr135b3bd00921c4f33b11dd6f1e3de7c41';


UPDATE title
SET entry_name='Vue de la façade du Palais-Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1532b72c6f3814220a9dcbae25ceb6548';


UPDATE title
SET entry_name='Vue de la façade du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1e191709ff69647438eb529d227a5fa5c';


UPDATE title
SET entry_name='Le cirque du Palais-Royal, vue intérieure.'
WHERE title.id_uuid = 'qr1151dd7ec6a06455eae9274d6f0f0ea47';


UPDATE title
SET entry_name='Evènement du 10 juillet 1789, fermentation au Palais-Royal'
WHERE title.id_uuid = 'qr19185df59552d4154bf800ddc3c8b61cb';


UPDATE title
SET entry_name='Aux trois ordres. Enseigne d''un marchand de cocardes au Palais-Royal. Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1aee4efe93c034f97ada2547f7ff7016f';


UPDATE title
SET entry_name='Le Palais-Royal - Les bâtiments côté du jardin'
WHERE title.id_uuid = 'qr18d0b81789518469ea9ee35c9a34d0da3';


UPDATE title
SET entry_name='Le Palais-Royal - Les bâtiments du côté du jardin'
WHERE title.id_uuid = 'qr12604479583d34435a56e3d232edf08d0';


UPDATE title
SET entry_name='97 – La promenade au Palais-Royal (Desrais)'
WHERE title.id_uuid = 'qr1f800416ecec441089083ca0d4581202a';


UPDATE title
SET entry_name='Ancienne Façade du Palais-Royal (côté de la Place)'
WHERE title.id_uuid = 'qr12f6c6973c28e49fd8bd4231acf4326af';


UPDATE title
SET entry_name='Projet d''architecture en rotonde pour un café au Palais-Royal'
WHERE title.id_uuid = 'qr10d9ca41075014147a9b776729e1bc546';


UPDATE title
SET entry_name='Vue du jardin du Palais-Royal près de la rotonde'
WHERE title.id_uuid = 'qr13032482141374784a8068d68606b67e1';


UPDATE title
SET entry_name='Entrée du Palais-Royal, rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c06e034d306c4dd6b07dc8f05a9d3d60';


UPDATE title
SET entry_name='Ruines de la Commune de Paris, 1871. La façade du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d76e4c276a59492c9577a04a58c32bfa';


UPDATE title
SET entry_name='L''incendie de l''Opéra, vu des jardins du Palais-Royal, le 8 juin 1781'
WHERE title.id_uuid = 'qr12199a1cacaff4a8a8dfe9b9f90a1456a';


UPDATE title
SET entry_name='Portrait-charge d''un inconnu, acteur comique au Palais-Royal'
WHERE title.id_uuid = 'qr126886f577c21431f9e3cae36903470f0';


UPDATE title
SET entry_name='Le Palais-Royal en 1670 (reconstitution)'
WHERE title.id_uuid = 'qr18a7380c470e74bc480c68230e0534671';


UPDATE title
SET entry_name='Le Rémouleur du Théatre Séraphin. Enseigne-marionnette, Palais-Royal'
WHERE title.id_uuid = 'qr1e7201829104c4c478eae874309c72cb4';


UPDATE title
SET entry_name='Le Palais-Royal en 1827 - Jardins et galerie de bois'
WHERE title.id_uuid = 'qr136ca42b6b23b4622b2c6c4104791a21e';


UPDATE title
SET entry_name='Le Palais-Royal en 1827 - Jardins et galerie de bois'
WHERE title.id_uuid = 'qr13465c082d53246debe6ca0da9fdc2d5b';


UPDATE title
SET entry_name='Projet de décoration de différentes pièces au Palais-Royal en 1778'
WHERE title.id_uuid = 'qr130e91e3e47f44a24b2191a6bfd19181c';


UPDATE title
SET entry_name='Aile gauche incendiée du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr179efd15a3a5c4abb96ae0ea12bb0b174';


UPDATE title
SET entry_name='Façade du Théâtre Français, place du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1727dd958081c41f68382d988211d484a';


UPDATE title
SET entry_name='Incendie du château d''eau, place du Palais-Royal, le 24 février 1848'
WHERE title.id_uuid = 'qr11759dee7dc0d4f1a9c718616357e7244';


UPDATE title
SET entry_name='Les délices de Paris. - Un diner dans un restaurant du Palais-Royal. Suite des mêmes délices. - Monsieur, une loge au Palais-Royal... moins chère qu''au bureau... Mr. Grassot dans trois pièces... Régalez votre dame de Mr. Grassot!'
WHERE title.id_uuid = 'qr1a009bbb5c0724f2680535e19177c19c0';


UPDATE title
SET entry_name='Projet de transformation du portique du Palais-Egalité (Palais-Royal), fin XVIIIè. (avec les rabats)'
WHERE title.id_uuid = 'qr1fdf0fd655f6647bb81e7b4c3e0320f7d';


UPDATE title
SET entry_name='Le jardins du Palais-Royal, vers 1820, actuel 1er arrondissement'
WHERE title.id_uuid = 'qr179b8f4b8c6374651990e0d5c0da0db3d';


UPDATE title
SET entry_name='Contretype de Marville : Aménagement de la place du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1b52531ddaa364b6089c5716502bba65d';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal) et son bichon'
WHERE title.id_uuid = 'qr176e4afcb1f3b4592a1cbb92920c33811';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal) et son bichon'
WHERE title.id_uuid = 'qr10fe86cdcf499487b89285e8222d1fdd2';


UPDATE title
SET entry_name='Théâtre-français, place du Palais-Royal, rue de Rivoli, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr11f06f999776f4fd3b9ba289d86349051';


UPDATE title
SET entry_name='Apollon et la chèvre Amalthée, statue du jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1887217760ba9451bb87cd3f485cb4fc8';


UPDATE title
SET entry_name='Orateur haranguant la foule au Palais-Royal (Camille Desmoulins ?) [recto]'
WHERE title.id_uuid = 'qr11a528c0a620b4ac092f7e6ee31c6e1c0';


UPDATE title
SET entry_name='Vue intérieure des ruines de l''Opéra du Palais-Royal après l''incendie de 1763'
WHERE title.id_uuid = 'qr12321ff1d4f904ae498d15930562a8813';


UPDATE title
SET entry_name='La Promenade au Palais-Royal'
WHERE title.id_uuid = 'qr19dc07bc195cf4c29b881a5c7df28509b';


UPDATE title
SET entry_name='Le Jardin du Palais-Royal'
WHERE title.id_uuid = 'qr1dd1315440cd64fd598e484a4c520b585';


UPDATE title
SET entry_name='Ruines du Palais-Royal'
WHERE title.id_uuid = 'qr1795bd97333d543ea9e8b3c871ed25c72';


UPDATE title
SET entry_name='Opéra du Palais-Royal'
WHERE title.id_uuid = 'qr17a8d5867646b40fea8cf8c1db4238693';


UPDATE title
SET entry_name='Place du Palais-Royal'
WHERE title.id_uuid = 'qr1131cece67bf6424f963d511adc974c6b';


UPDATE title
SET entry_name='Palais-Royal, élévation de la face extérieure du salon à l''italienne sur la rue de Richelieu'
WHERE title.id_uuid = 'qr131db0ab05b114d09ada4ccb59cc4142c';


UPDATE title
SET entry_name='Deux femmes assises près d''un kiosque, jardin du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1013152f446cd44b79d29c1e0d9343c24';


UPDATE title
SET entry_name='Jeune fille piquée au pied par un serpent, statue du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1926a3668938b4128ae7974b59a3b845f';


UPDATE title
SET entry_name='Mademoiselle Baron dans le rôle de la sœur Louise de la Miséricorde (jeux forains, théâtre Montansier, Palais-Royal)'
WHERE title.id_uuid = 'qr196e4f9009100474e9181c8e3af7dda9a';


UPDATE title
SET entry_name='Passage du Perron, Entrée des Galeries du Palais-Royal. 9, rue de Beaujolais, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr161454c28d3e24888a4359f5835e716b1';


UPDATE title
SET entry_name='Le duc d''Orléans signant la proclamation de la lieutenance générale du royaume, le 31 juillet 1830, au Palais-Royal'
WHERE title.id_uuid = 'qr1c7b90b09cc3c46809fe3212e2aad474e';


UPDATE title
SET entry_name='Place et façade du Palais-Royal (actuel conseil d''Etat), 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1562151603f384ef7b234977381650221';


UPDATE title
SET entry_name='Les Gardes Françaises amènés au Palais-Royal'
WHERE title.id_uuid = 'qr14324b191e35147179d219bee81466aed';


UPDATE title
SET entry_name='Détail d''escalier, Palais-Royal (actuel conseil d''Etat), 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1b9ef66fa30c94ecca91a8d09e63a7554';


UPDATE title
SET entry_name='Portrait-charge de Masson, acteur au Palais-Royal et maître de chapelle à Saint-Roch'
WHERE title.id_uuid = 'qr11fcd179dc8234c97934e75c97ccd76ac';


UPDATE title
SET entry_name='Etiquette de la maison Fayolle, rubans et croix d''ordre, 180 galerie de Valois, Palais-Royal'
WHERE title.id_uuid = 'qr1e0c5775e6ca544ddb26661ec7fb82bab';


UPDATE title
SET entry_name='Le café des Aveugles, au Palais-Royal'
WHERE title.id_uuid = 'qr16116b405419d4b4fa7d41ec8912f3039';


UPDATE title
SET entry_name='Le jardin et le cirque du Palais-Royal'
WHERE title.id_uuid = 'qr146421163aa5c4c13932725b93b6d0748';


UPDATE title
SET entry_name='La cour intérieur du Palais-Royal.'
WHERE title.id_uuid = 'qr12fd382006bce4668afb82066f5eaa647';


UPDATE title
SET entry_name='Le café de la Rotonde au Palais-Royal'
WHERE title.id_uuid = 'qr18e14e7fa144645069eccd1c3e32680c2';


UPDATE title
SET entry_name='Le père Ducland au Palais-Royal'
WHERE title.id_uuid = 'qr145b109d8c77444468494ae9c5d692b43';


UPDATE title
SET entry_name='Le jardin du Palais-royal. / 17.'
WHERE title.id_uuid = 'qr155d5aeaa4da143069332b2c2f5eabb81';


UPDATE title
SET entry_name='Détail de la rampe d''escalier, Palais-Royal (actuel conseil d''Etat), 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18b58d651938543458ff8308374a80e70';


UPDATE title
SET entry_name='Dîner du 7 avril 1884 organisé par la Société des Gens de lettres, au restaurant Richard, au Palais-Royal'
WHERE title.id_uuid = 'qr1e99e7cbaec284336b6e8f1445773f431';


UPDATE title
SET entry_name='Plan en perspective du Palais-Royal flanqué à droite de la Salle de Spectacle/ Dessiné et gravé par la Boissière (1679)'
WHERE title.id_uuid = 'qr17677ad62fb9d4faeafe664f93fd8802b';


UPDATE title
SET entry_name='Théâtre de l''Opéra. - Salle du Palais-Royal (ancienne salle de Molière), incendiée le 6 avril 1763.'
WHERE title.id_uuid = 'qr1377b73d995f441ae8b9cedc40f14ead6';


UPDATE title
SET entry_name='Révolution de 1848. / Combat et prise du château d’eau, place du Palais-Royal / 24 février'
WHERE title.id_uuid = 'qr17105e66da8334d3f9606da27cb5a75ad';


UPDATE title
SET entry_name='Carte de la maison Fayolle Pouteau successeur, croix et rubans d''ordre, 180 galerie de Valois, Palais-Royal'
WHERE title.id_uuid = 'qr1a1b369ab164749d4bd828b002370245c';


UPDATE title
SET entry_name='Portrait d''Ericourt (actrice au Palais-royal)'
WHERE title.id_uuid = 'qr194f3303e0de1413cb4c8e12a1edde273';


UPDATE title
SET entry_name='Portrait de Kid, théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1a333293642364475bd6ec84d4e6da487';


UPDATE title
SET entry_name='Madame Very restaurateur au Palais-Royal'
WHERE title.id_uuid = 'qr1e5ab225a6fc6436187416c8bcc557b4c';


UPDATE title
SET entry_name='N°49. Dévastation du Palais-Royal.'
WHERE title.id_uuid = 'qr15530ee2150fe4464b2eb88f93be0618a';


UPDATE title
SET entry_name='Portrait d''Héricourt, (actrice au Palais-Royal)'
WHERE title.id_uuid = 'qr1d1a2bbe50b63437eba3d0371523bd0b0';


UPDATE title
SET entry_name='Portrait d''Héricourt, (actrice au Palais-Royal)'
WHERE title.id_uuid = 'qr1fa25402083a843a9b472d4cebbf06215';


UPDATE title
SET entry_name='Portrait d''Héricourt, (actrice au Palais-Royal)'
WHERE title.id_uuid = 'qr1e23d532e682c4cb6b71107938946a9f9';


UPDATE title
SET entry_name='Projet de décoration intérieure pour le Palais-Royal'
WHERE title.id_uuid = 'qr1467122cb36e544819a526da56bf851ca';


UPDATE title
SET entry_name='Disposition des places du Théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1359f7c6664d74287a951d45dfc856c64';


UPDATE title
SET entry_name='Vue générale du jardin du Palais-Royal'
WHERE title.id_uuid = 'qr122416a385cb9459984c0d2b3e9a7271f';


UPDATE title
SET entry_name='Construction du chemin de fer métropolitain municipal de Paris : construction des accès, station du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr19d3c76f2fc0d467a9afe6728b577355b';


UPDATE title
SET entry_name='Portrait-charge d''Etienne Augustin Tousez, dit Alcide (1806-1850), acteur comique au théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr1ac14df052d16480d96f3392eefcfc2d8';


UPDATE title
SET entry_name='Portrait-charge d''Etienne Augustin Tousez, dit Alcide Tousez (1806-1850), acteur au théâtre du Palais-Royal'
WHERE title.id_uuid = 'qr11fd0e2886eb2482180f298486291bcf8';


UPDATE title
SET entry_name='Jardins du Palais-Royal, actuel ministère de la Culture (Pavillon Valois, 1er arrondissements. (1880)'
WHERE title.id_uuid = 'qr1ef9f93ddce85406eac32d501c8db92bf';


UPDATE title
SET entry_name='Le marchand de volailles au Palais-Royal, sous le régime de la Commune'
WHERE title.id_uuid = 'qr1446f383d3cea4096ab650143550f7ba2';


UPDATE title
SET entry_name='Détails du bassin et de la gerbe d''eau du Palais-Royal, actuel 1er arrondissement'
WHERE title.id_uuid = 'qr18ffe05af951b40ef8764f87e80d00dcb';


UPDATE title
SET entry_name='Le quart d''heure de Rabelais au Palais-Royal. [...]'
WHERE title.id_uuid = 'qr1aeac415ec7fc4c45919c38b8697b0d63';


UPDATE title
SET entry_name='Théâtres R. D. : palais-royal'
WHERE title.id_uuid = 'qr1c393a23d6945460199c9ceb91348cdf9';


UPDATE title
SET entry_name='Portrait d''Emma (actrice au théâtre du Palais-royal)'
WHERE title.id_uuid = 'qr11a5dfe98389140c4b6188b6b1886fd05';


UPDATE title
SET entry_name='Portrait d''Emma (actrice au théâtre du Palais-royal)'
WHERE title.id_uuid = 'qr12509a3c14f9f414f8cc17e01c4111045';


UPDATE title
SET entry_name='Les ruines de Paris - Ce qui reste du Palais-Royal'
WHERE title.id_uuid = 'qr1d7c7808463014a288f125d0412259659';


UPDATE title
SET entry_name='REVOLUTION DE 1848. / DESTRUCTION DU PALAIS-ROYAL'
WHERE title.id_uuid = 'qr14ca89815b172412f8fce3e558ad672e9';


UPDATE title
SET entry_name='Portrait de Blanche (actrice au Théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr14631ed28739d426ca272aacf97900117';


UPDATE title
SET entry_name='Vue générale du ci-devant Palais-Royal'
WHERE title.id_uuid = 'qr168c7d5408fa84a119163b753d2572f8f';


UPDATE title
SET entry_name='La petite Edith Piaf du Palais-Royal, Paris'
WHERE title.id_uuid = 'qr134796949a5f549558a54335656f9dc1e';


UPDATE title
SET entry_name='Rue Camille-Desmoulins / allant de la Bourse de commerce à l''avenue de l''Opéra / Vue de la traversée du Jardin du Palais-Royal'
WHERE title.id_uuid = 'qr18a6b48b3b67641ae915777c20f6c37c1';


UPDATE title
SET entry_name='Portrait de Christiane (actrice aux Bouffes et au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr17a4fb52d25a7484981edc47bde0bd4e5';


UPDATE title
SET entry_name='Portrait de Christiane (actrice aux Bouffes et au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr126db7a0749064c85a643302caafda045';


UPDATE title
SET entry_name='Portrait de Christiane (actrice aux Bouffes et au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr1774dc374359b4a1fa70113eed91ca354';


UPDATE title
SET entry_name='Portrait de Christiane (actrice aux Bouffes et au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr17e23cafe628f4f0f86b92a8db40e9694';


UPDATE title
SET entry_name='Portrait de Christiane (actrice aux Bouffes et au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr12af2900e61ef460e8dbfc618dd125ad3';


UPDATE title
SET entry_name='Colonnades du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18de27bc2b8ec464c98d01b5d0e1a8c58';


UPDATE title
SET entry_name='Incendie place du Palais-Royal, 24 février 1848'
WHERE title.id_uuid = 'qr1dbcf2983ec34479a9e07b5d92eb7031f';


UPDATE title
SET entry_name='Attaque du poste du château d''Eau sur la place du Palais-Royal'
WHERE title.id_uuid = 'qr1d37a069f622d47d3ad6497b9b461b8d1';


UPDATE title
SET entry_name='Portrait de Caroline (actrice ? au théâtre du Palais-royal)'
WHERE title.id_uuid = 'qr1e964947aa59a4b28b2a667bf92a55b99';


UPDATE title
SET entry_name='Place du Palais-Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14fb6862754184b8cb9f2d40346ba95c8';


UPDATE title
SET entry_name='Décoration de la salle Gombeau, côté des croisées, au Palais-Royal'
WHERE title.id_uuid = 'qr1906688fc47624bb286257826ebd85f70';


UPDATE title
SET entry_name='Projet de décoration de la salle dite de Gombeau au Palais-Royal'
WHERE title.id_uuid = 'qr14caf7e17ce924798b04e264ab6b5a4da';


UPDATE title
SET entry_name='Prise du château d''eau, place du Palais-Royal, le 24 février 1848. Actuel 1er arrondissement'
WHERE title.id_uuid = 'qr1a86f6018b8d941588308d91977bd58a0';


UPDATE title
SET entry_name='Révolution française : Le siège du Palais-Egalité, Palais-Royal, actuel 1er arrondissement, le 27 janvier 1793'
WHERE title.id_uuid = 'qr18d93e9dbfd5e40dc850343e6cd49cf43';


UPDATE title
SET entry_name='Palais-Royal, escalier du Conseil d''Etat, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1fc938f4004484e73a2d00e611cc2a24a';


UPDATE title
SET entry_name='Palais-Royal, vestibule du grand escalier, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1daa93512b3274a33af9b4a8b08842662';


UPDATE title
SET entry_name='Palais-Royal, escalier du Conseil d''Etat, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr157007946a0c54e42b2312dac342e7d19';


UPDATE title
SET entry_name='Plan au 1er étage de la partie ouest du Palais-Royal.'
WHERE title.id_uuid = 'qr14055bd47939140cc80d6e70721be50a1';


UPDATE title
SET entry_name='Portrait d''Hélène Emma (actrice au théâtre de l''Ambigü et du Palais-royal)'
WHERE title.id_uuid = 'qr102bc766df7d54187b9281a8ac7da7df7';


UPDATE title
SET entry_name='Portrait d''Hélène Emma (actrice au théâtre de l''Ambigü et du Palais-royal)'
WHERE title.id_uuid = 'qr1bfcfaea5a9c34493aaa65171b199de71';


UPDATE title
SET entry_name='Palais-Royal, plan des caves, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr14122f5ad7914492687b2d8b78ff364d0';


UPDATE title
SET entry_name='Portrait charge de Grassot dans un rôle féminin au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr1d7fa26ad00024c0ab293da3ef7bf2894';


UPDATE title
SET entry_name='Camille Desmoulins au Palais-Royal, peinture attribuée à Debucourt.'
WHERE title.id_uuid = 'qr1d97fe7597c084dd4908b0f36606a9dd8';


UPDATE title
SET entry_name='Portrait de Léona Cellié (actrice au théâtre de l''Athénée et du Palais-royal)'
WHERE title.id_uuid = 'qr15497d10b55304a949712d0b45b8321f3';


UPDATE title
SET entry_name='Le Palais-Royal, vue intérieure de la galerie de pierre du côté de l''Est.'
WHERE title.id_uuid = 'qr161d95655425843899648424f61209e17';


UPDATE title
SET entry_name='Femme donnant l''aumône pour les incendiés du Bazar de la Charité au Palais-Royal.'
WHERE title.id_uuid = 'qr1d52036fbe1184d719e0b959f1b18a265';


UPDATE title
SET entry_name='Palais-Royal, galerie d''Orléans, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr14b3d9f30673c42379d0eeab0b3d1b5b9';


UPDATE title
SET entry_name='L''indispensable visite chez le tailleur du Palais-Royal. / N°2'
WHERE title.id_uuid = 'qr1dd744e61b0304373972fa72e4206e861';


UPDATE title
SET entry_name='Portrait de Blanche Gabrielle Damis (actrice au théâtre du Vaudeville et du Palais-Royal)'
WHERE title.id_uuid = 'qr19eb13e99f5504975851989222874f52c';


UPDATE title
SET entry_name='Portrait de Blanche Gabrielle Damis (actrice au théâtre du Vaudeville et du Palais-Royal)'
WHERE title.id_uuid = 'qr17fd1828047d6499c873e2435d26cac63';


UPDATE title
SET entry_name='Portrait de Blanche Gabrielle Damis (actrice au théâtre du Vaudeville et du Palais-Royal)'
WHERE title.id_uuid = 'qr1c384d75f8ad145d39cd301e769401719';


UPDATE title
SET entry_name='Portrait de Blanche Gabrielle Damis (actrice au théâtre du Vaudeville et du Palais-Royal)'
WHERE title.id_uuid = 'qr17e32824e00604f948fab5c789145c84f';


UPDATE title
SET entry_name='Portrait de Blanche Gabrielle Damis (actrice au théâtre du Vaudeville et du Palais-Royal)'
WHERE title.id_uuid = 'qr108f8c8a7533e4e7cb0a1e1ff917e3a52';


UPDATE title
SET entry_name='Palais-Royal, plan du rez-de-chaussée, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c59da12143464dbf8ffd91a6b5be4c4e';


UPDATE title
SET entry_name='Palais-Royal, plan du premier étage, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1cb1635de64544be58935db4604727fae';


UPDATE title
SET entry_name='Palais-Royal, place du Théâtre Français, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e9f76a4bd1544598a6a71399509c5da7';


UPDATE title
SET entry_name='Palais-Royal, place du Théâtre Français, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr13f363c18af1b44948fdec66792b308a6';


UPDATE title
SET entry_name='Palais-Royal, plan du rez-de-chaussée, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr103e2e36b7e144a23a222bfdf511bac55';


UPDATE title
SET entry_name='Madame Romain, la Belle Limonadière au café de Mille-colonnes, Palais-Royal.'
WHERE title.id_uuid = 'qr17aac26643f1548f491bedb945dac2f9d';


UPDATE title
SET entry_name='Projet de bassin dans le jardin du Palais-Royal, actuel 1er arrondissement'
WHERE title.id_uuid = 'qr1884bc1cc04804737abfd5d41a8edb6f0';


UPDATE title
SET entry_name='Les ruines de Paris - Le grand vestibule du premier étage du Palais-Royal.'
WHERE title.id_uuid = 'qr19170b6cb386042588b00b8053727d5b9';


UPDATE title
SET entry_name='Incendie du lycée des Arts ou cirque du Palais-Royal, le 15 décembre 1798'
WHERE title.id_uuid = 'qr1a66cf8f499ac413e81e2afc7ef214946';


UPDATE title
SET entry_name='L''assassinat de M. Le Peletier de Saint-Fargeau chez M. Ferrier, restaurateur au Palais-Royal, actuel 1er arrondissement, le 20 janvier 1793.'
WHERE title.id_uuid = 'qr1d589e455b59e41519c6f2d79da7be2b4';


UPDATE title
SET entry_name='Palais-Royal, réunion d''un corps militaire canadien dans un salon, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1313f9dc647724720aa878e30869995cf';


UPDATE title
SET entry_name='Palais-Royal, réunion d''un corps militaire canadien dans un salon, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1801ea836715e4010ae35d64d7bf7e73e';


UPDATE title
SET entry_name='Messieurs G. Feydeau, / P. Mussay et L. Boyer/ fêteront avec les Artistes du Théâtre du/ Palais-Royal la 100e Représentation/ du "Dindon", le Mardi 5 Mai 1896 [...]'
WHERE title.id_uuid = 'qr1ef1074a4fbe3415780e1fafa85ec3c6d';


UPDATE title
SET entry_name='Portrait charge de Lhéritier dans "Ah! Que l''amour est agréable", en 1862 au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr14aaf3b3adda5410ebcee4bfb5be21f5f';


UPDATE title
SET entry_name='Portrait charge de Hyacinthe (1814-1867) dans le rôle d''un danseur au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr1d8909940dce045a3bf867b593aa4d9d8';


UPDATE title
SET entry_name='Portrait charge de Luguet (1813-1904) en costume de scène au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr153f869c235c841e5be6e66a78cfa40f8';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1e5f3dba65dc042208449fa06f3293df9';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr13c1065a65d72453895b154fa58a5b5cf';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1e6dd9ccf34d249e9af481551b2670944';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr198ceb3a939864aabb5c7c72a4be94339';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1c0b0265a0bb744b4a30e931b000035f9';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr19bb48b72df884fce825ddb2c9a93c527';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1283d4bdc9c7945c095f1d73ee86faa4c';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr199e34d13424e42a688f44ecb3517fccd';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1c0a2e2f2e75b4eedb7d4014506491437';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1dad93183215e4059af41dacf83830220';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr13553bc43c2744fe9a67ef73644803e92';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1a13153498ed14256bcabcf707395ea06';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr1944df8172afd46d68da7ef063fcdb8c6';


UPDATE title
SET entry_name='Portrait d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr140a172b02d8e4cfda86dca9329190b81';


UPDATE title
SET entry_name='Cercle-club des amis de la Constitution au Palais national (Palais-Royal), novembre 1848'
WHERE title.id_uuid = 'qr17ab2b4f6b3f544bf8184de360689b7d2';


UPDATE title
SET entry_name='Voiture de la famille royale brulées, sur la place du Palais-Royal'
WHERE title.id_uuid = 'qr151b67391b201415f9a809b64034f5619';


UPDATE title
SET entry_name='Portrait de Clothilde Charlotte Charvet ( -1895), (actrice au théâtre de Palais-royal)'
WHERE title.id_uuid = 'qr1224092ef4ef446eeb279ae0d9c9f91f9';


UPDATE title
SET entry_name='Portrait de Clothilde Charlotte Charvet ( -1895), (actrice au théâtre de Palais-royal)'
WHERE title.id_uuid = 'qr15c4ac548880e4a3b8c2cf3fa5507d874';


UPDATE title
SET entry_name='Grand vestibule du Palais-Royal, 1817 (Histoire du Palais Royal, Pl.15).'
WHERE title.id_uuid = 'qr1b4eb07972fb74ac58c9e0e2d52ce677b';


UPDATE title
SET entry_name='Motion au jardin du Palais-Royal, Camille Desmoulins appelle le peuple à la résistance, le 12 juillet 1789.'
WHERE title.id_uuid = 'qr17945ef9653fd4e9b820b8dd18c1802cb';


UPDATE title
SET entry_name='Coin : Incendie du château d''eau, place de Palais-Royal, le 24 février 1848'
WHERE title.id_uuid = 'qr1ef967b180f1a4b64aef159e9da86d92b';


UPDATE title
SET entry_name='Portrait de Daubray (Michel René Thibaut) (1837-1892), (comique au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr1c8143a0f94a24333ac591595b325b115';


UPDATE title
SET entry_name='Portrait de Daubray (Michel René Thibaut) (1837-1892), (comique au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr116621644ded7456b9d69c1c878e41a27';


UPDATE title
SET entry_name='Portrait de Daubray (Michel René Thibaut) (1837-1892), (comique au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr1ec6fe4b1d8cb428480d45bde2b65ba60';


UPDATE title
SET entry_name='Portrait de Daubray (Michel René Thibaut) (1837-1892), (comique au théâtre du Palais-Royal)'
WHERE title.id_uuid = 'qr1121520dff87a404f93b8f6b0aa89933b';


UPDATE title
SET entry_name='Portrait-mosaïque d''Emilie Keller ( -1889), (actrice aux théâtres des Variétés et du Palais-Royal)'
WHERE title.id_uuid = 'qr17204eb08cf0e4c0ca202027112747bdf';


UPDATE title
SET entry_name='Plantation de l''arbre de la Liberté à Paris au jardin du Palais national (Palais-Royal), 1848'
WHERE title.id_uuid = 'qr19d6cb6897a3b4e8bbccfc18c76fd6448';


UPDATE title
SET entry_name='Palais-Royal, réunion d''un corps militaire canadien dans un salon, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1bf78d112c7124508be463d7672c2cc82';


UPDATE title
SET entry_name='Portrait charge de Grassot dans le rôle de l''étrangleur dans "En avant les Chinois", en 1858 au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr1b61e7701f26b42d88d7b401d15ce496d';


UPDATE title
SET entry_name='Théâtre de l''Opéra. - 2e salle du Palais-Royal, incendiée le 8 juin 1781.'
WHERE title.id_uuid = 'qr125557d2a71054b4f9a050f0a0ea364c3';


UPDATE title
SET entry_name='Plan / du / Palais-Royal. / Entresol du rez-de-chaussée. / Année 1833.'
WHERE title.id_uuid = 'qr1d1a36cc7f0f44450a953e344cea767a5';


UPDATE title
SET entry_name='Cortège impérial passant devant le Palais-Royal, le jour du sacre de Napoléon, 2.XII.1804'
WHERE title.id_uuid = 'qr1f4b2aba35963460dbb311a05886bd70f';


UPDATE title
SET entry_name='Bal donné au Palais-Royal, le samedi 11 février, par LL. AA. le prince Napoléon et la princesse Clotilde.'
WHERE title.id_uuid = 'qr1ef0f59ff8338441b973badd623a9b7ca';


UPDATE title
SET entry_name='Portraits charges de Brasseur, Gil-Pérès, Grassot, Lessouche, Luguet et Hyacinthe en costumes de scène, Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr19dd0eebb81e142fd800d5c020675f783';


UPDATE title
SET entry_name='Meunier-Very Frères, Restaurateurs, / Palais-Royal, n°83, et rue du Beaujolais, n°11 (menu)'
WHERE title.id_uuid = 'qr14ea58dc747474b6aa63a9b5d5196b14d';


UPDATE title
SET entry_name='L''Eclipse/Huitième Année/N°370/Dimanche 5 Septembre 1875/Les théâtres de Paris - Le Palais-Royal, par Gill.'
WHERE title.id_uuid = 'qr11f9a3723509c4db2a2b2e51b04790ebc';


UPDATE title
SET entry_name='Portrait charge de Gil-Pérès dans le rôle de l''homme-canon "En avant les Chinois" de Labiche, en 1858 au Théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr1fdc40b1861af4be29e58fe9d52a2ba73';


UPDATE title
SET entry_name='Au Gourmand ; Enseigne du marchand de comestibles Corcellet, 104, Palais-Royal, ancienne galerie du Lycée, actuellement galerie de Valois'
WHERE title.id_uuid = 'qr1c45c748980aa44cb8872749a947fb6f5';


UPDATE title
SET entry_name='Invitation au Bal de nuit paré et travesti, donné le 23 février 1884 dans les Salons Richefeu, Palais-Royal, par la Fantaisie lyrique'
WHERE title.id_uuid = 'qr1a6591fc1a7b64ff0b179668232258d10';


UPDATE title
SET entry_name='L''assassinat de Louis Michel Le Peletier de Saint-Fargeau chez Ferrier, restaurateur au Palais-Royal, actuel 1er arrondissement, le 20 janvier 1793. Vignette accompagnant le portrait de Le Peletier, nº 18 des portraits des Tableaux historiques de la...'
WHERE title.id_uuid = 'qr1b3e230336f984ea78f781deaaba15fe9';


UPDATE title
SET entry_name='Planche nº 77, numérotée 86, des Tableaux historiques de la Révolution française (1791-1804). L''assassinat de M. Le Peletier de Saint-Fargeau chez M. Ferrier, restaurateur au Palais-Royal, actuel 1er arrondissement, le 20 janvier 1793.'
WHERE title.id_uuid = 'qr1791cf3bf57f14a3c8314c78c3aeab893';


UPDATE title
SET entry_name='Obsèques de S.A.I. le prince Jérôme Bonaparte. - Exposition de la dépouille mortelle du prince dans la grande galerie du Palais-Royal.'
WHERE title.id_uuid = 'qr1f06441b49c2945fea7a0ac759567e5ed';


UPDATE title
SET entry_name='Portrait de Paul Mussay, (Paul Bacharach ? dit, 1847-), acteur au théâtre des Variétés en 1872 et directeur du théâtre du Palais-Royal.'
WHERE title.id_uuid = 'qr1d7b6601f370f4f0da4493605f1324052';


UPDATE title
SET entry_name='Révolution française : planche nº 45 des Tafereelen van de Staatsomwenteling in Frankrijk (1794-1807). L''assassinat de M. Le Peletier de Saint-Fargeau chez M. Ferrier, restaurateur au Palais-Royal, actuel 1er arrondissement, le 20 janvier 1793.'
WHERE title.id_uuid = 'qr184c9f36c01954aabb7c9eb85a082205c';


UPDATE title
SET entry_name='Etat des parts des comédiens du Palais-Royal, de la troupe de Guénegaud et des comédiens français depuis l''arrivée de Molière à Paris jusqu''au règlement de 1685 [...].'
WHERE title.id_uuid = 'qr12c7ffb43a11f4e0590649a99443f9edb';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Thomas Levassor (1808-1870), acteur au Théâtre du Palais-Royal, rôle de "Grillo" dans "La Prova d"un opera-seria"'
WHERE title.id_uuid = 'qr11c8d057fd7154aee95564ef88d3860b3';


UPDATE title
SET entry_name='PAR PRIVILEGE DU ROI,/ Avec Permission de Monseigneur le Lieutenant-Général de Police./ THEATRE DES VARIETES./ LES VARIETES/ DONNERONT aujourd''hui Dimanche 16 Janvier 1785,/ AU PALAIS-ROYAL, La 17e Représentation/ DU RAMONNEUR PRINCE,/ ET DU PRINCE...'
WHERE title.id_uuid = 'qr1be5298a416524c9789e828887873e130';


UPDATE title
SET entry_name='Portrait de Jean-Laurent Kopp (1812-1872), acteur.'
WHERE title.id_uuid = 'qr18e1013af15574bba9e1a2760ebc6d20a';


UPDATE title
SET entry_name='Portrait de Jean-Laurent Kopp (1812-1872), acteur.'
WHERE title.id_uuid = 'qr126559853a67048c58ed41a3ddbd1ac72';


UPDATE title
SET entry_name='La Veüe du Palais Royal du costé du Jardin'
WHERE title.id_uuid = 'qr1a2196f986eff42d0ab18a1e6498fe66f';


UPDATE title
SET entry_name='[P]AR PRIVILEGE DU ROI,/ Avec Permission de Monseig[n]eu[r] [l]e Lieutenant-Genéral [sic] de Police./ [THEATRE DES VARIETES./ [?] S VARIETES/ [DONNE]RONT aujourd''hui Lundi 10 Janvier 1785,/ AU PALAIS-Royal,/ [?] FAUX TALISMAN'
WHERE title.id_uuid = 'qr18cbac0232e324b3fb0fcb7d536e3b2c1';


UPDATE title
SET entry_name='Plan du salon à construire au Palais Royal'
WHERE title.id_uuid = 'qr16fbb5ed9849f4247a6c2b72a32e3dd9b';


UPDATE title
SET entry_name='Le Palais Royal que le Cardinal de Richelieu fit bastir'
WHERE title.id_uuid = 'qr1ae470a83e2e642daaba6888539c835c8';


UPDATE title
SET entry_name='[PAR] PRIVILEGE DU ROI,/ [Avec la Pe]rmission de Monseigneur le Lieutenant-Général de Police./ [THEAT]RE DES VARIETES. [?] [V]ARIETES/ [?] [aujou]rd''hui Dimanche 23 Janvier 1785,/[?] [PAL]AIS-ROYAL,/ [?] RAISONNABLE,/ Piece [sic] en un Acte, en prose,...'
WHERE title.id_uuid = 'qr188422cd7497a482c8c0dfacdbf1111fa';


UPDATE title
SET entry_name='Le Palais Royal en ruines, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ed76c7e818954fd68219706c1de7d8ef';


UPDATE title
SET entry_name='Comble du Grand Escalier du Palais Royal sur la rue de Valois.'
WHERE title.id_uuid = 'qr1b48274610096444b9b1635dcd8ab2277';


UPDATE title
SET entry_name='Comble du Grand Escalier du Palais Royal sur la rue de Valois.'
WHERE title.id_uuid = 'qr11d84c4f3063f4829be07079f4ffd47a2';


UPDATE title
SET entry_name='Le Palais Royal et la cour d''honneur , sous Louis-philippe, en 1839. Actuel 1er arrondissement.'
WHERE title.id_uuid = 'qr1d1e037b5caad4d1391108d6bf7cdbed1';


UPDATE title
SET entry_name='François Levée, conseiller municipal de Paris, 1923'
WHERE title.id_uuid = 'qr1d22e203e96ef44c780cabf462d93c63e';


UPDATE title
SET entry_name='Le palais cardinal en la rue St Honoré [sic]'
WHERE title.id_uuid = 'qr1c46ebf1b603c4724b35c0b5d60a09e75';


UPDATE title
SET entry_name='Chantier de démolition place du Palais Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a851771396154d3b93df5bedc0dae9fb';


UPDATE title
SET entry_name='Rue de Valois, détail balcon, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e6135b7131ba440fbffb6938e1596b40';


UPDATE title
SET entry_name='Portrait de Lhéritier, acteur du Palais Royal'
WHERE title.id_uuid = 'qr18b81b98bcf9042c0a00042759caf35d9';


UPDATE title
SET entry_name='Portrait de Raymond, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr188e8268b7bbc4e10868e1777975299ae';


UPDATE title
SET entry_name='Sept costumes féminins.'
WHERE title.id_uuid = 'qr138468c6a632d412b9fba61b8d6d36b05';


UPDATE title
SET entry_name='La Promenade Publique'
WHERE title.id_uuid = 'qr11ac63dc7cc6a46368a667b4a29550dd9';


UPDATE title
SET entry_name='La Chambre des députés présente au duc d''Orléans l''Acte qui l''appelle au trône et la Charte de 1830. (7 Aout 1830)'
WHERE title.id_uuid = 'qr13e3ea37301ab4588a40028e445bd9c6a';


UPDATE title
SET entry_name='[Raphaël sortant de la maison de jeu]'
WHERE title.id_uuid = 'qr17e7724e5884b4217be8c5e3b6d61328f';


UPDATE title
SET entry_name='Enterrement du Prince Jérôme Bonaparte'
WHERE title.id_uuid = 'qr18384008dc65b4f3d9547e449d0d0412b';


UPDATE title
SET entry_name='La musique serbe au jardin du palais Royal : les musiciens'
WHERE title.id_uuid = 'qr1e83b9cf7d8354c839d8086ee15f35a59';


UPDATE title
SET entry_name='Mort de Le Pelletier St Fargeau.'
WHERE title.id_uuid = 'qr1324ea99ef838482db5fdfda3678220c6';


UPDATE title
SET entry_name='Motion donnée par Camille Desmoulins au Palais Egalité (Palais Royal), événement de la Révolution française, le 12 juillet 1789.'
WHERE title.id_uuid = 'qr1bda670a2f5e3486a9b7c81e3262b039d';


UPDATE title
SET entry_name='Assassinat de Lepelletier St Fargeau'
WHERE title.id_uuid = 'qr1e87629291e9847409d1d7cfb59e28351';


UPDATE title
SET entry_name='Assassinat de le Pelletier St. Fargeau'
WHERE title.id_uuid = 'qr1b6dbdb023c81492dbf695278568afed9';


UPDATE title
SET entry_name='Assassinat de Le Pelletier de St. Fargeau / Le 20 Janvier 1793.'
WHERE title.id_uuid = 'qr1bad95c0f59854f4e8887eb80e04817d5';


UPDATE title
SET entry_name='Assassinat de Michel Le Pelletier / Le 20 Janvier 1793.'
WHERE title.id_uuid = 'qr185f4ede62cad4c779755720d8956f85d';


UPDATE title
SET entry_name='20 janvier 1793. - Assassinat de Lepelletier de Saint-Fargeau.'
WHERE title.id_uuid = 'qr153ac3b53504f4b5c8de0d7248bc7e266';


UPDATE title
SET entry_name='ASSASSINAT DE LE PELLETIER, MAISON DE FEVRIER RESTAURATEUR, le 20 Janvier 1793 : ou 30 Nivôse An Ier de la République.'
WHERE title.id_uuid = 'qr1aa70691c02674ca19f2747bfb0aa458d';


UPDATE title
SET entry_name='Tableau de Paris. Cabinet d''Aisance, Palais Royal, Théâtre français, le Bureau, Madame, différents Characteurs'
WHERE title.id_uuid = 'qr11241e3655a3a4346b0bf723f12a891d3';


UPDATE title
SET entry_name='Tableau de Paris. Palais Royal, l''Ecrivain publique avec ses aides paisanes, fille des Supplicants, un Vétéran etc'
WHERE title.id_uuid = 'qr1ed95180da67e41fbb92006d46387c7e4';


UPDATE title
SET entry_name='20 janvier. 30 Nivose. Assassinat de Michel Le Pelletier St. Fargeau.'
WHERE title.id_uuid = 'qr16961a2584e9d4c28b1ae749bc6566ade';


UPDATE title
SET entry_name='Effigie du Pape Pie VI brûlé au Palais Royal / le 4 Mai 1791. / Révol. de Paris / N°95. Pag.186.'
WHERE title.id_uuid = 'qr141841de8377f492cbdfc691d85f0bdd5';


UPDATE title
SET entry_name='Le restaurant d''huîtres au Palais Royal.'
WHERE title.id_uuid = 'qr1c2d32f9437ee4d5fa1ff624ed2faa23a';


UPDATE title
SET entry_name='La maison de Jeux au Palais Royal'
WHERE title.id_uuid = 'qr1e772126c61804d5abadc6b445ff61f8f';


UPDATE title
SET entry_name='Le Charivari, quarante-unième année, lundi 2 septembre 1872'
WHERE title.id_uuid = 'qr1c307f29d38ba4358a4363b7034ec4422';


UPDATE title
SET entry_name='Sac'
WHERE title.id_uuid = 'qr1f7d409e9719f4261b7c9026e9258a868';


UPDATE title
SET entry_name='La Promenade du jardin du Palais Royal, 1787'
WHERE title.id_uuid = 'qr1c629803b00f14e689a9fb70526dd3925';


UPDATE title
SET entry_name='Pendule à la bacchante'
WHERE title.id_uuid = 'qr1de77165b394247f8857b3cab79c207eb';


UPDATE title
SET entry_name='Les presses modernes'
WHERE title.id_uuid = 'qr1920922bec9d14b8bb2cb072237e409fa';


UPDATE title
SET entry_name='Oeuvre de jeunesse'
WHERE title.id_uuid = 'qr1cd32b22285564ab397c861c48aa80665';


UPDATE title
SET entry_name='Vue du Palais royal, des galeries et du jardin.'
WHERE title.id_uuid = 'qr1740cc507ae9247eeb2c0d853013fbd2f';


UPDATE title
SET entry_name='Vue du Palais royal, des galeries et du jardin.'
WHERE title.id_uuid = 'qr102955252eeb8446dbb3feaad478775d2';


UPDATE title
SET entry_name='Vue du jardin, galeries et Palais royal.'
WHERE title.id_uuid = 'qr1e3840a92a8ff4dae83d260dae5e85756';


UPDATE title
SET entry_name='The Palais Royal Gallery''s Walk. Promenade de la gallerie du Palais Royal. (IFF 7)'
WHERE title.id_uuid = 'qr18fe51dc15e634486a57d45d652792d39';


UPDATE title
SET entry_name='Quatrevingt-treize : Les cafés, La Rotonde'
WHERE title.id_uuid = 'qr14158ceffe4e94b57bd23d11f51337eee';


UPDATE title
SET entry_name='Quatrevingt-treize : Les cafés, Foy'
WHERE title.id_uuid = 'qr1241c9ffdb0e2459b944aa59fcab3a718';


UPDATE title
SET entry_name='Quatrevingt-treize : Frelons de finance'
WHERE title.id_uuid = 'qr130ae7fdcd8ba415d851456e6eef5787a';


UPDATE title
SET entry_name='Quatrevingt-treize : Les cafés, La Rotonde'
WHERE title.id_uuid = 'qr10de02152bcba43498f85367e67d80a51';


UPDATE title
SET entry_name='Quatrevingt-treize : Frelons de finance'
WHERE title.id_uuid = 'qr194eebb8ff1e848e1be487290790e488c';


UPDATE title
SET entry_name='Veüe de la Gallerie du Palais Royal'
WHERE title.id_uuid = 'qr1cc874210f8174157af448dec896fb73a';


UPDATE title
SET entry_name='Le Parterre du Palais Royal'
WHERE title.id_uuid = 'qr1e7590a81fba146c49f7ea3e31726f841';


UPDATE title
SET entry_name='Portrait-charge d''E.-F.-M. Morel, dit Sainville (vers 1800-1854), acteur comique'
WHERE title.id_uuid = 'qr1b782bf20a84b44fe888071c581d0b89f';


UPDATE title
SET entry_name='Portrait de mademoiselle Calvat'
WHERE title.id_uuid = 'qr192f81dbd4be847f1b193f0f57346aeff';


UPDATE title
SET entry_name='Le Charivari, trente-quatrième année, vendredi 1er décembre 1865'
WHERE title.id_uuid = 'qr15717d5f366394b48aeb7dec1b441f2a3';


UPDATE title
SET entry_name='[Raphaël sortant de la maison de jeu]'
WHERE title.id_uuid = 'qr1dd756155cf3c432c99da460e8cfa058c';


UPDATE title
SET entry_name='Quatre ans de règne'
WHERE title.id_uuid = 'qr138c51b71edd34737a8b6a128be12ae74';


UPDATE title
SET entry_name='Le Charivari, trente-septième année, dimanche 12 avril 1868'
WHERE title.id_uuid = 'qr103868b4eba704146a82ba720c4591e79';


UPDATE title
SET entry_name='Les Misérables, théâtre de la Porte-St-Martin, 1878'
WHERE title.id_uuid = 'qr14b72c6877618484592601c3530160ece';


UPDATE title
SET entry_name='Le Charivari, trente-cinquième année, mercredi 23 mai 1866'
WHERE title.id_uuid = 'qr1d0108e87ac694646924b01b713c16a24';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ Liberté - Egalité - Fraternité/ ELECTIONS LEGISLATIVES/ DU 22 SEPTEMBRE 1889/ XXe Arrondt - 2e Circonscription (Charonne et Père-Lachaise)'
WHERE title.id_uuid = 'qr1f427ab0fbe3541ce9fdb9427edb54688';


UPDATE title
SET entry_name='L''afficheur'
WHERE title.id_uuid = 'qr15ea910ff26514a9c98a170701a4ac6dd';


UPDATE title
SET entry_name='LE CHARIVARI [planche nº1]'
WHERE title.id_uuid = 'qr117c6689a090f44c38561879ac9658b87';


UPDATE title
SET entry_name='Paris incendié/N°5/Le Palais Royal.'
WHERE title.id_uuid = 'qr147bdd68aaf2d40a8a3fe6362208fdbca';


UPDATE title
SET entry_name='Le XVIIIe siècle : le Palais Royal, les Philosophes, les Encyclopédistes'
WHERE title.id_uuid = 'qr1018452d187554db7836e93992ec4e969';


UPDATE title
SET entry_name='Palais Royal (arcade côté jardin). Devanture de boutique : “G. RETROU” lanterne, juin 1899. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a166c6e10b494a659e1afa2ac262c6fb';


UPDATE title
SET entry_name='Dans les jardins du Palais royal'
WHERE title.id_uuid = 'qr12bb0d89814aa42fd941ca4bd40dfe85d';


UPDATE title
SET entry_name='Visitez/ LE PALAIS ROYAL/ MAISONS NOTABLES/ & RECOMMANDEES'
WHERE title.id_uuid = 'qr1982ee045440d48198fc7080fdb394f04';


UPDATE title
SET entry_name='Modes de Paris / Le Mercure des Salons / Habit d''Amazone Gilet Corset, des Magasins de Cior-Cury, 1er Tailleur pour les Enfants, / Rue Neuve des Petits Champs, n°13, Chapeau Napolitain, de Colas Jne Chapelier, Palais / Royal, Galerie d''Orléans, n°6.'
WHERE title.id_uuid = 'qr1cf9d6cf0ba8b4668b60b15a3aaf73ebe';


UPDATE title
SET entry_name='Modes de Paris / Le Mercure des Salons / Diebitsch en drap garni de brandebours d''astracan et de soie des / ateleirs de Mr. Geissenhoffer à l''Athénée des modes rue de Richelieu / Coupe de cheveux de Mr. Nalin Palais Ral. N°. 50.'
WHERE title.id_uuid = 'qr19233d73343d847dcb341c98ae242f676';


UPDATE title
SET entry_name='Facture de Dieu Bijoutier du Duc d''Orléans et du Ministère de la Guerre, Palais Royal n°45, Galerie de Richelieu, Paris, 2 avril 1830'
WHERE title.id_uuid = 'qr1222fe12a216e4c43a089429e8f1580a6';


UPDATE title
SET entry_name='Les Croyables / Actifs du Palais ci-dev.t Royal.'
WHERE title.id_uuid = 'qr13130a7cd28b44d589a6cce62300d2f56';


UPDATE title
SET entry_name='Galerie du Palais Royal.'
WHERE title.id_uuid = 'qr1e19f3dee741c40fbb20e8b70e74c25e2';


UPDATE title
SET entry_name='Le Méridien du Palais Royal.'
WHERE title.id_uuid = 'qr129a795b15aca4814aefee3bc6434af61';


UPDATE title
SET entry_name='Suite de la Promenade au Palais Royal.'
WHERE title.id_uuid = 'qr19c892f41b96b4e23b1bed43cfe4c0f5d';


UPDATE title
SET entry_name='Promenade au Palais Royal.'
WHERE title.id_uuid = 'qr1a7802c21d9634ecc93a44a7e5276ceca';


UPDATE title
SET entry_name='Galerie de Valois, Palais Royal.'
WHERE title.id_uuid = 'qr1a3a3edb4a73f409a9e4f72189c1da412';


UPDATE title
SET entry_name='Vue du Palais Royal. / N°32'
WHERE title.id_uuid = 'qr19ad48fdd267348b3864cf0a11e03efbc';


UPDATE title
SET entry_name='Le 12 Juillet 1789. Motion de Camille Desmoulins, au Palais Royal. Contre l''oppression Royale envers l''Assemblée Nle...[Nationale]'
WHERE title.id_uuid = 'qr174dba156c55849da94daaa126d19b118';


UPDATE title
SET entry_name='Les adieux au Palais Royal / ou les suites du premier pas.'
WHERE title.id_uuid = 'qr16ddf4084fa02482ca1afeefb924f0694';


UPDATE title
SET entry_name='Le coup de canon du Palais Royal. / N°5.'
WHERE title.id_uuid = 'qr1d3c29a275ee5471386b3f75e53472ed0';


UPDATE title
SET entry_name='Vue de l''intérieur du nouveau cirque du Palais Royal [...]'
WHERE title.id_uuid = 'qr11199f3a19b294cb09077cbb11495a638';


UPDATE title
SET entry_name='Palais Royal N°3 / J''ai dîné'
WHERE title.id_uuid = 'qr133bf67122e6e42d2add439910bdc5a43';


UPDATE title
SET entry_name='Bijouterie "Maison Dieu" : 45, galerie Montpensier au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr164910554eda749c896d7eb37737221cb';


UPDATE title
SET entry_name='Lettre d''accusé de réception adressée par la maison Deschamps, Palais Royal, galerie Montpensier 19 et 20'
WHERE title.id_uuid = 'qr1513ad3b0cb6a4b5b807c9f163b99cd12';


UPDATE title
SET entry_name='Lettre d''accusé de réception adressée par la maison Deschamps, Palais Royal, galerie Montpensier 19 et 20'
WHERE title.id_uuid = 'qr141fd0b87fedd45248a67199dcafcaec8';


UPDATE title
SET entry_name='Cachet Thénard : 47, galerie Montpensier au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr165d9651fddd24e4792c89fb9debfeefa';


UPDATE title
SET entry_name='Graveur Bessaignet : 15, galerie Montpensier au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1240da97c609348c09a1c14a30e164331';


UPDATE title
SET entry_name='Facture de la maison Deschamps, Palais Royal, galerie Montpensier, 19 et 20'
WHERE title.id_uuid = 'qr1978de5bfb0bf4b6fb49fd40cb473a0cb';


UPDATE title
SET entry_name='Rue Montpensier / N°47. Passage Beaujolais.'
WHERE title.id_uuid = 'qr18c617ac4ccc14cf8a7dec0b8a2e0ebff';


UPDATE title
SET entry_name='Rue Montpensier, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr173e79f251dfe4f0fb5c8ff774569e2c6';


UPDATE title
SET entry_name='Médailliste Fayolle : 180, galerie Valois au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1dedb4fc1e19e42c79cc73a382f1f7d18';


UPDATE title
SET entry_name='Paris et ses souvenirs / Vue de la galerie d''Orléans, / prise du péristyle de Valois.'
WHERE title.id_uuid = 'qr1ee27965e57334128ba0da6ccc4dc0b54';


UPDATE title
SET entry_name='Bijouterie-horlogerie Leroy & fils : 114-115, galerie Valois au palais royal à Paris, seconde moitié du XIXe siècle'
WHERE title.id_uuid = 'qr158c3f664ae8d4030ad77875f268f04a8';


UPDATE title
SET entry_name='Graveur Burger Lévêque : 121, galerie Valois au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1888969235a5949aab9030a651806d5c3';


UPDATE title
SET entry_name='Graveur Lévêque : 121, galerie Valois au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr142dbb5d4d32245eea1c4fb2a767d8d35';


UPDATE title
SET entry_name='Graveur Lévêque : 121, galerie Valois au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr186c2d30ba1a54c088f7a9794d38aba1f';


UPDATE title
SET entry_name='Vue aérienne de Paris : le Palais-Royal et la banque de France. 1er et 2ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr13677c4dad98f4af9a16f114b2f520ae5';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec la banque de France et le jardin du Palais-Royal, 1er, 2ème, 9ème et 18ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr117cea025ad484574af6eafc930ecf622';


UPDATE title
SET entry_name='Réverbère, rue de Valois devant le passage vers le Palais Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr11460dcc0321e4ee0aab7defd5342b8b3';


UPDATE title
SET entry_name='Plan au sol des passages, des galeries et du péristyle du Palais Royal du côté de la rue de Valois en 1827'
WHERE title.id_uuid = 'qr1e40f24deb4cc42f890bb09a54444118b';


UPDATE title
SET entry_name='Carte de visite de Muret, opticien du théâtre français, au Palais royal'
WHERE title.id_uuid = 'qr1b032f57558e04964b2625c31984a7214';


UPDATE title
SET entry_name='Galerie du Costume Français d''après Watteau fils. La prude Mélite ne paraissant occupée que de ce qui doit intéresser le plus son coeur, cherche à fixer les regards en prenant l''air au Palais Royal...'
WHERE title.id_uuid = 'qr15942fcf6f2b049a4a16c1f518a2dbcc7';


UPDATE title
SET entry_name='Plans, décorations et meubles de trois salles du Palais Royal.'
WHERE title.id_uuid = 'qr175704aeae41f41949770c4557efead1a';


UPDATE title
SET entry_name='La Soirée du Palais Royal/ou les Religieuses a bonnes fortunes.'
WHERE title.id_uuid = 'qr13b1d337e88ba4f51aca9d571bc4023f1';


UPDATE title
SET entry_name='Carte de visite de Noseda et compagnie, opticien, au Palais royal'
WHERE title.id_uuid = 'qr1dac3c86ff28d41caa07c8de057ab451b';


UPDATE title
SET entry_name='Café des Aveugles au Palais Royal vers 1800.'
WHERE title.id_uuid = 'qr117be415befd44711a1e62f7529733aba';


UPDATE title
SET entry_name='Un polytechnicien s''empare d''une pièce de canon au Palais Royal.'
WHERE title.id_uuid = 'qr1eabf984372b2403a881027221cc8e229';


UPDATE title
SET entry_name='Projet d''une nouvelle entrée et d''une fontaine pour le Palais Royal.'
WHERE title.id_uuid = 'qr1b862e952188c4b0b9d90d465416800d2';


UPDATE title
SET entry_name='Camille Desmoulins au Palais Royal.'
WHERE title.id_uuid = 'qr195dc4d00e2434a268f6a0761dee809e6';


UPDATE title
SET entry_name='Décoration de la salle dite de Gombeau au Palais Royal en 1778.'
WHERE title.id_uuid = 'qr10ad5a8cb01804a55b6fb98d3d313fd0e';


UPDATE title
SET entry_name='Projet pour la décoration du Palais Royal.'
WHERE title.id_uuid = 'qr16e90114ba02f405192d3c45722ef9ddc';


UPDATE title
SET entry_name='Coupe d''un projet de galerie du Palais Royal.'
WHERE title.id_uuid = 'qr15802167c56564c238112dcd8ad5259d4';


UPDATE title
SET entry_name='Plan en coupe de différentes pièces au palais Royal.'
WHERE title.id_uuid = 'qr156c83084c30a4d7eaf1bafce21c889a1';


UPDATE title
SET entry_name='Décoration opposée aux croisées de la galerie des hommes illustres au Palais Royal'
WHERE title.id_uuid = 'qr1a7a2d5ccdc884967b44ab8df85e0463e';


UPDATE title
SET entry_name='Décoration pour la galerie des hommes illustres au Palais Royal'
WHERE title.id_uuid = 'qr1f79e11dcd4b74c2e931d67288c5be7ea';


UPDATE title
SET entry_name='Projet d''installation de la Bourse au Palais Royal'
WHERE title.id_uuid = 'qr1381c1ba3be004cf087badc48e61b59ae';


UPDATE title
SET entry_name='Etudes pour la promenade du jardin du Palais Royal'
WHERE title.id_uuid = 'qr1596d4bf275b345b0b844cb3cfadbc004';


UPDATE title
SET entry_name='Jardins du Palais Royal'
WHERE title.id_uuid = 'qr16162e3f5a5ee44d5bc1981337e1b232b';


UPDATE title
SET entry_name='Coin : Décombres, place du Palais Royal, XIXe siècle'
WHERE title.id_uuid = 'qr1db5b9b04c6c3445ab3d416d86b15bee3';


UPDATE title
SET entry_name='Incendie du château d''eau de la place du Palais royal lors de la Révolution, 24 février 1848'
WHERE title.id_uuid = 'qr108e9bbcbd1974ba98c96a53e075e2b3c';


UPDATE title
SET entry_name='Démolitions de l''ancien vestibule du Palais Royal.'
WHERE title.id_uuid = 'qr1d51b8f33e6974ef8b7ec62f517da3c95';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1798ebc7bc402424a8269608f4adeeef8';


UPDATE title
SET entry_name='Palais Royal'
WHERE title.id_uuid = 'qr1615f533bdcca42e09fd14daff1139840';


UPDATE title
SET entry_name='Palais Royal.'
WHERE title.id_uuid = 'qr1a62abb0f758643949c5d091920675bfa';


UPDATE title
SET entry_name='Ruines de la Commune - Palais Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1d5cf2b28d77e4ad1bb3032e0508811e3';


UPDATE title
SET entry_name='Salon du tribunal des Conflits, Palais royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bb1b4e84ca53441cb483deeefd4401b1';


UPDATE title
SET entry_name='Palais Royal. Lecture du Moniteur le lundi 26 juillet à 7 heures du soir.'
WHERE title.id_uuid = 'qr1c328c8618f144780b9c57e89c421affc';


UPDATE title
SET entry_name='Cinquième projet pour l''achèvement du Palais Royal'
WHERE title.id_uuid = 'qr177f7011791334b2caaf89d3ecb3a99ed';


UPDATE title
SET entry_name='Vue de l''escalier neuf de l''aile Montpensier au Palais Royal en 1831, actuel 1er arrondissement'
WHERE title.id_uuid = 'qr11cfc38621d894ed39e68540a2d59721c';


UPDATE title
SET entry_name='Affaire du Palais Royal.'
WHERE title.id_uuid = 'qr16c58f780fde54626ac850f0e5bffd4b2';


UPDATE title
SET entry_name='Le Palais Royal en 1789.'
WHERE title.id_uuid = 'qr1be626f64dd0c4a55bbee6505e4bd0215';


UPDATE title
SET entry_name='Le Palais Royal en 1810'
WHERE title.id_uuid = 'qr1a0d703f26fe847b0abf9d6f2c72808e2';


UPDATE title
SET entry_name='Motion du Palais Royal, le 12 Jet 1789. Reu. de Paris. Nº1 Page 1.'
WHERE title.id_uuid = 'qr12a95c4f50a5945e48afc48630222bf7d';


UPDATE title
SET entry_name='Escalier d''honneur du conseil d''Etat, palais Royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr167c1e1e3c6e8409389c7478bead1ac72';


UPDATE title
SET entry_name='L''escalier d''honneur du conseil d''Etat, Palais royal, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c82dc9279024481a897de1cf07a88e71';


UPDATE title
SET entry_name='Allocution donnée par des orateurs au Palais Royal, événement de la Révolution française, le 12 juillet 1789.'
WHERE title.id_uuid = 'qr1cb9669915f114b779704ca83f7fc1795';


UPDATE title
SET entry_name='Matinée du Palais Royal 3 May 1791.'
WHERE title.id_uuid = 'qr1d747a48001ac4298872384497e5ef35b';


UPDATE title
SET entry_name='Prise du Palais Royal (29 Juilllet 1830)'
WHERE title.id_uuid = 'qr1dcb6cd2bd94e4bcd91cc57d60cc67cef';


UPDATE title
SET entry_name='Incendie du Palais Royal, le 24 Mai 1871.'
WHERE title.id_uuid = 'qr14e062968a7d34847b1b42f4deb5c39d7';


UPDATE title
SET entry_name='Avant et après l''incendie 1871 / Palais Royal / 4'
WHERE title.id_uuid = 'qr18334373285a24a77984b41511ac0a87d';


UPDATE title
SET entry_name='Le Md. turc au Palais Royal, ou Le désir des femmes.'
WHERE title.id_uuid = 'qr1430e584c5be24c3990de6f441b5b78d6';


UPDATE title
SET entry_name='Vuë du Caffé du Caveau du / Palais Royal.'
WHERE title.id_uuid = 'qr1b6ed380d22cf490d8e3b3a254beba354';


UPDATE title
SET entry_name='Le premier pas d''un jeune officier cosaque au Palais Royal.'
WHERE title.id_uuid = 'qr169778cc6ee7443509d6b7ac8df9a9ec8';


UPDATE title
SET entry_name='La blonde Mélite se promenant sur le midi au Palais Royal[...]'
WHERE title.id_uuid = 'qr1aa41dee286fe46c3b1b4edb51a6ccb34';


UPDATE title
SET entry_name='La brillante Nymphe du Palais Royal[...] (IFF 88)'
WHERE title.id_uuid = 'qr1c52d4f6799b04fdba8342b62e7c2651b';


UPDATE title
SET entry_name='Barricades de la Place du Palais Royal [...]'
WHERE title.id_uuid = 'qr1195163320cd1483cbdcecaffe6dfc06f';


UPDATE title
SET entry_name='Elévation, coupe et profil du Palais Royal.'
WHERE title.id_uuid = 'qr14210281e3b3a48db885782ae2ad8f67e';


UPDATE title
SET entry_name='PRISE DU POSTE DE LA PLACE DU PALAIS ROYAL.'
WHERE title.id_uuid = 'qr12349892fc5e9441e9fa7239d0819e4c2';


UPDATE title
SET entry_name='LAMORICIERE. / (Place du Palais Royal.)'
WHERE title.id_uuid = 'qr1a9ac2ec2e7324aad9cfa79312d1c7adc';


UPDATE title
SET entry_name='Les jardins et le cirque du Palais Royal en 1791'
WHERE title.id_uuid = 'qr145ad646c7bc3430caf5eb1147a21f4b2';


UPDATE title
SET entry_name='Vue de l''intérieur du / Palais Royal.'
WHERE title.id_uuid = 'qr1f82b91841ea14abd8ff672e5348cfbce';


UPDATE title
SET entry_name='Camille Desmoulins au Palais Royal.'
WHERE title.id_uuid = 'qr1bd8c8ad64d3d404aa133ed4a7e754e89';


UPDATE title
SET entry_name='Les marchands de marons du Palais Royal.'
WHERE title.id_uuid = 'qr1b210847df1a040e3965bddceb61b2d6c';


UPDATE title
SET entry_name='Maison Lahoche, porcelaines et cristaux, maison de l''escalier de cristal, 162 Palais Royal'
WHERE title.id_uuid = 'qr1da735409d9074eceb854897bb9b3ba10';


UPDATE title
SET entry_name='Au Palais Royal/Quien un'' baignoire - Il prenait donc des bains le père Philippe ! [...]'
WHERE title.id_uuid = 'qr1721dbbeb1bd74156ad1338fdf33568a6';


UPDATE title
SET entry_name='Facture de la maison Hebert, fabricant de nécessaires et de portefeuilles, Palais Royal, galerie de Pierre 20'
WHERE title.id_uuid = 'qr1544e5b4d236e4a1eb338aa18de416103';


UPDATE title
SET entry_name='Adresse illustrée et responsable de l''activité de Brière, marchand parfumeur, Palais Royal.'
WHERE title.id_uuid = 'qr1454a33a155ce4187b8d44508e82eee3a';


UPDATE title
SET entry_name='La Grosse Mère, vacante à son commerce du soir, / au Palais Royal'
WHERE title.id_uuid = 'qr19334f55278584780b1d77a6f9c3a3c46';


UPDATE title
SET entry_name='Carte de visite de Prengrueber de Weittersheim, vendeur en optique, galerie de Bois au Palais royal'
WHERE title.id_uuid = 'qr1c4e3360699744a539f8ab660604e7d4d';


UPDATE title
SET entry_name='Vue extérieure du cirque du jardin du Palais Royal / 1799 (Histoire du Palais Royal, pl.10)'
WHERE title.id_uuid = 'qr12cfbef718823449ab34f00dba26ebce0';


UPDATE title
SET entry_name='Manequin du Pape brûlé au Palais Royal / le 6 avril 1791. / N°60.'
WHERE title.id_uuid = 'qr190471e144ffc40f4a95de0ae31428eb6';


UPDATE title
SET entry_name='Manequin du Pape, brûlé au Palais Royal / le 6 avril 1791. / N°51'
WHERE title.id_uuid = 'qr1fd0c49770ad147aeb2c51f00b7e90d2c';


UPDATE title
SET entry_name='Le grand escalier du Palais Royal.'
WHERE title.id_uuid = 'qr1c88d30dbf245491e88b43a8668c6beb5';


UPDATE title
SET entry_name='Vue du jardin du Palais Royal.'
WHERE title.id_uuid = 'qr127348af711b64e07a1e2c403ed3dac1d';


UPDATE title
SET entry_name='Vuë perspective du Palais Royal.'
WHERE title.id_uuid = 'qr1f0ec9c5e724043d8a8cd50c1d3261360';


UPDATE title
SET entry_name='Nous avons pris le palais royal [...]'
WHERE title.id_uuid = 'qr12580e2c2ed6b441a9fd3f1ee47addb76';


UPDATE title
SET entry_name='Cachet Thénard : 47, galerie Montpensier au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1aedac88b07aa4357a0cdfbfb75fe8d92';


UPDATE title
SET entry_name='Graveur Brasseux : 47, palais royal à Paris, première moitié du XIXe siècle'
WHERE title.id_uuid = 'qr1e167760270be4d1b9054772c6591271a';


UPDATE title
SET entry_name='Coupe sur la largeur et élévation de l’avant-scène / de la nouvelle salle de l’opéra projettée au Palais Royal.'
WHERE title.id_uuid = 'qr171416ed4c621430392460688f3809f89';


UPDATE title
SET entry_name='Projet pour la fontaine de la nouvelle entrée du Palais Royal : élévation, coupe et moitié du plan.'
WHERE title.id_uuid = 'qr196024537523040c4bcd76cecaf264a11';


UPDATE title
SET entry_name='Bref du pape brulé au Palais Royal le 4 May 1791, par un grand nombre de Citoyens et l''on y a joint un/ Manequin représentant sa Sainteteté.'
WHERE title.id_uuid = 'qr16f7b84d2a90845a6ad5e2cae938e03ef';


UPDATE title
SET entry_name='Actualités./35./Le sauvage du Palais Royal, offrant ses services pour la guerre du Mexique...,et même sa candidature au trône de ce pays !...avec rrrappel du peuple !...'
WHERE title.id_uuid = 'qr1f51c04ff2da14432b6e19396dfee3f8a';


UPDATE title
SET entry_name='En-tête de lettre d''A Jean de Paris, maison spéciale pour les gilets, commerce tenu par Ozaneaux, tailleur, Palais Royal 169'
WHERE title.id_uuid = 'qr1776ac3e13e464377807d291d13897204';


UPDATE title
SET entry_name='Costume de Melle Dejazet, rôle de La Liberté, / dans les chansons de Béranger. / Vaudeville / Th. du Palais Royal (IFF 19)'
WHERE title.id_uuid = 'qr196cdbe82f7c0432faade51d8368952a5';


UPDATE title
SET entry_name='Librairie antique A. Raguin : 8-9, galerie de Nemours au palais royal à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1408eff1f81bd4ab2be760c0ea012f1bc';


UPDATE title
SET entry_name='Dick Wildfire & Squire Jenkins seeing "Real Life" in the galleries of the Palais Royal.'
WHERE title.id_uuid = 'qr1cc9588184c89437285b82c84b8a83ab8';


UPDATE title
SET entry_name='Palais Royal / galerie d''Orléans / musée commercial, passage couvert, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c48581f4679546cfbc5e37bd7bae6727';


UPDATE title
SET entry_name='Carte de visite de la maison A la belle Cérès, Champion marchand orfèvre, galerie de Foi 16, Palais royal'
WHERE title.id_uuid = 'qr11234f3d17f2949eb84a3b99ccd31df74';


UPDATE title
SET entry_name='Parapluitier Antoine : 26-29, galerie de Chartres au palais royal à Paris, fin du XIXe siècle'
WHERE title.id_uuid = 'qr1b27c6d2e24bb4c64a74af440bb10073d';


UPDATE title
SET entry_name='Billet d''entrée pour le championnat individuel d''épée de France se tenant du 4 au 10 mai dans les jardins du Palais Royal'
WHERE title.id_uuid = 'qr198cf46a2a827435daf094d699c010da1';


UPDATE title
SET entry_name='Eventail brisé et boîte à éventail en 2 parties'
WHERE title.id_uuid = 'qr1f7460b0823cf41318ec864f2522d24d1';


UPDATE title
SET entry_name='Eventail publicitaire Ernest Kees'
WHERE title.id_uuid = 'qr1642011f2da2e409ba98cdb3cdab1b2a6';


UPDATE title
SET entry_name='Eventail plié et étui en 2 parties'
WHERE title.id_uuid = 'qr1c83d1658f543477cbbd221f0869f040a';


UPDATE title
SET entry_name='Monuments de Paris'
WHERE title.id_uuid = 'qr13fa8cb93e6ec41a7b0b0ba5794e840dd';


UPDATE title
SET entry_name='13ME ANNEE TOURNEES ARTISTIQUES 13ME ANNEE/ TRES PROCHAINEMENT/ LA PERCHE/ WIDIAM/ Administrateur/ Comédie Vaudeville en 3 Actes de M.M. PREVEL & MAROT/ Grand Succès du PALAIS ROYAL'
WHERE title.id_uuid = 'qr11cc01f5c84d64fe39bf0ee6e97e50b31';


UPDATE title
SET entry_name='Mlle La Pierre, jeune géante; / Agée de 17 ans, de 6 pieds 2 pouces de hauteur / On la voit au Palais Royal, arcade n°33.'
WHERE title.id_uuid = 'qr10f9df6b2aec7471f8f1eea0b12a1ce63';


UPDATE title
SET entry_name='Costume de Melle Dejazet, rôle de Gaudriole, / dans les chansons de Désaugier. / Vaudeville / Th. du Palais Royal (IFF 19 p.58)'
WHERE title.id_uuid = 'qr1c44bdad7120044be912d950c355952cf';


UPDATE title
SET entry_name='Costume de Melle Dejazet, rôle de Charles, / dans le triolet bleu. / Vaudeville / Th. du Palais Royal (IFF 19 p.58)'
WHERE title.id_uuid = 'qr14d2ea3a711d944cea785199fd8a11c53';


UPDATE title
SET entry_name='Portrait de Luguet, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr1637c8747210846d9a32f352eeb2ae4ec';


UPDATE title
SET entry_name='Costume de Melle Dejazet, rôle de Sophie Arnould, / dans la pièce de ce nom. / Vaudeville / Th. du Palais Royal (IFF 19 p.58)'
WHERE title.id_uuid = 'qr136f5d1eb4c824553b1f86645b6ce6fc2';


UPDATE title
SET entry_name='Bouton à queue'
WHERE title.id_uuid = 'qr135d44a11a55d48bbae48382ceb9509e0';


UPDATE title
SET entry_name='6.Avril Manequin du Pape brulé au Jardin de la Révolution. Ce fut le 4 Avril que / parut ce fameux bref du Pape Pie VI, [...].'
WHERE title.id_uuid = 'qr102712dbb9f0f41c791b4ec2c2a2359ff';


UPDATE title
SET entry_name='5ème tableau du recueil des gravures historiques des principaux événements de la Révolution française (1789-1799)'
WHERE title.id_uuid = 'qr15b420035c0774573976167dc5908f501';


UPDATE title
SET entry_name='Vue de Paris, n°8/Vue de la Gallerie du Palais-Royal/prise du côté de la rue des Bons Enfans.'
WHERE title.id_uuid = 'qr181dacdbb03de4ddab1278942d68793ba';


UPDATE title
SET entry_name='Vue de Paris, n°8/Vue de la Gallerie du Palais-Royal/prise du côté de la rue des Bons Enfans.'
WHERE title.id_uuid = 'qr1128e97ad154940beb5d04e30091507a0';


UPDATE title
SET entry_name='Bouton gravure rehaussée aquarelle sous verre : Jardins du Palais Royal'
WHERE title.id_uuid = 'qr15d62ff3e7def429b8aca9169241837ae';


UPDATE title
SET entry_name='Vue aérienne de Paris : le palais et musée du Louvre et le jardin des Tuileries, 1er, 2ème, 8ème et 9ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr102fdd81809f649a486290c158ba5caf4';


UPDATE title
SET entry_name='Paul Butterbrodt, pesant 476 livres. / Agé de 66 ans, on le voit au Palais Royal aux entresolles du sieur Curtius / aux arcades n°7 et 8.'
WHERE title.id_uuid = 'qr1551b1d0bfabe4d48a71d2b1fe19fa20d';


UPDATE title
SET entry_name='“Debucourt. Parade de charlatans au palais Royal (collection Strauss)”. 1er arrondissement, Paris. Jardin, estrade, public, mode.'
WHERE title.id_uuid = 'qr147b00f09ff0741188ae218403d6ac139';


UPDATE title
SET entry_name='26 Juillet 1830 / Lecture des ordonnances dans le Moniteur / Au Jardin du Palais Royal.'
WHERE title.id_uuid = 'qr132e1f5a74a0d4afb971ca4eb0456b62e';


UPDATE title
SET entry_name='24 FEVRIER 1848. / PRISE DU POSTE DU CHATEAU D''EAU PLACE DU PALAIS ROYAL.'
WHERE title.id_uuid = 'qr1e664b48904724f7bb7191a9a19a327ad';


UPDATE title
SET entry_name='Frontispice : Galerie du Palais Royal gravée d''après les tableaux des differentes écoles qui la composent : avec un abrégé de la vie des peintres et une description historique de chaque tableau par mr. l''abbé de Fontenai. Dédiée a S. A. S. monseigneur...'
WHERE title.id_uuid = 'qr15f7f13aba04b472e9ec8ec0a407cde5d';


UPDATE title
SET entry_name='Scène de Mirame au Palais-Royal d''après une gravure de Stefano Della Bella.'
WHERE title.id_uuid = 'qr1bc77eb3bd1694f769686516066b2ca41';


UPDATE title
SET entry_name='Paris incendié - Palais royal 26 Mai 1871/Paris en 1871'
WHERE title.id_uuid = 'qr1c5fbdac588b04367807a26fe3675af9d';


UPDATE title
SET entry_name='Premier feu sur le peuple par la Gendarmerie place du Palais Royal le Mercredi 28 Juillet'
WHERE title.id_uuid = 'qr1b6214cf9995e496ca4607ad0deae0405';


UPDATE title
SET entry_name='Coupe sur toute la hauteur et largeur du théâtre de l''Opéra construit au Palais Royal, par Moreau.'
WHERE title.id_uuid = 'qr11ccb7c288a0c42f69fd899ae278ab2e3';


UPDATE title
SET entry_name='1815 / n°5 / L''embarras du choix ou Les anglais au Palais Royal.'
WHERE title.id_uuid = 'qr1a189c41b63234ebe910be652a1ce0605';


UPDATE title
SET entry_name='Vue du jardin du Palais Royal, avec le nouveau cirque. / N°81. (IFF82)'
WHERE title.id_uuid = 'qr1936270e25b6b4d4bbcc59577c4ba4b02';


UPDATE title
SET entry_name='Elévation géométrale d''un côté du cirque construit dans le jardin du Palais Royal en 1787. (IFF21)'
WHERE title.id_uuid = 'qr159cc6cf063d942fdb4aef43fea423c57';


UPDATE title
SET entry_name='Vue du Palais Royal. / Galerie de Nemours 1830.'
WHERE title.id_uuid = 'qr18b410bd27171407d86cb9c28394f6345';


UPDATE title
SET entry_name='Evénement du 10 juillet 1789. / Fermentation au Palais Royal. [...].'
WHERE title.id_uuid = 'qr16831431d38d340ad9f92e521c979053a';


UPDATE title
SET entry_name='Soirée du 30 juin 1789, dédiée à l''assemblée du Palais Royal.'
WHERE title.id_uuid = 'qr1f62c880b180e46bb872f7fbcb91946a4';


UPDATE title
SET entry_name='Révolution Française : Dîner en l''honneur des gardes Françaises délivrés par le peuple, jardin du palais Royal, 1er arrondissement, Paris, le 30 juin 1789.'
WHERE title.id_uuid = 'qr175470996a5cd43dca9a0f11ecedb9987';


UPDATE title
SET entry_name='Les nouveaux batimens / du Palais Royal / 4'
WHERE title.id_uuid = 'qr1a79e352f3afe49f096da4ee09461bd80';


UPDATE title
SET entry_name='Camille Desmoulins devant le café de Foy, au Palais Royal, le 12 juillet 1789, événement de la révolution française, actuel 1er arrondissement.'
WHERE title.id_uuid = 'qr1d46b671713814334a67b51becc8913da';


UPDATE title
SET entry_name='ATTAQUE DU CHATEAU D''EAU / Place du Palais Royal, le 24 Février 1848.'
WHERE title.id_uuid = 'qr1d4a7a292531243dfbeb5d90dba678229';


UPDATE title
SET entry_name='ATTAQUE DU CHATEAU D''EAU. / Place du Palais royal.'
WHERE title.id_uuid = 'qr182ce598b06f64c2fa0c372c4175f0ab2';


UPDATE title
SET entry_name='Prise du Palais Royal, le 28 Juillet 1830.'
WHERE title.id_uuid = 'qr1634b0bd6df0c4b8f985e67b8b9d876c5';


UPDATE title
SET entry_name='Projet pour la fontaine à placer devant le Palais Royal.'
WHERE title.id_uuid = 'qr17fa2b29569f94342b3bc5280dbd84c9b';


UPDATE title
SET entry_name='Projet de bassin avec jet d''eau pour le jardin du Palais Royal, 1816'
WHERE title.id_uuid = 'qr1416d682e0fdc452c8a205b21c1fa8656';


UPDATE title
SET entry_name='1ère vue du jardin du Palais Royal. / Prise de la Rotonde.'
WHERE title.id_uuid = 'qr12413d0addde940fb8762d079e94427fc';


UPDATE title
SET entry_name='To His Royal Highness Louis Philippe, Duke of Orléans. This View of the Palais Royal, drawn in Oct(r) 1827. [...]'
WHERE title.id_uuid = 'qr12a3ee748373741fcafdabd849a3fd341';


UPDATE title
SET entry_name='Plan des batiments et jardin du Palais Royal suivant leur état en 1780.'
WHERE title.id_uuid = 'qr13475574598e54bc68363ff29b64fd50f';


UPDATE title
SET entry_name='Incendie du château d''eau place du Palais Royal, 24 février 1848'
WHERE title.id_uuid = 'qr1c8c8b41c46f74ce098f5ce0ef755d950';


UPDATE title
SET entry_name='Incendie du château d''eau place du Palais Royal, 24 février 1848'
WHERE title.id_uuid = 'qr145a1673c988d471a8c007b7f0737a93b';


UPDATE title
SET entry_name='Aubril : 139, palais royal à Paris, 1822'
WHERE title.id_uuid = 'qr10f588167d5424f96963458948027f400';


UPDATE title
SET entry_name='Portrait de Madame Delille, actrice du Palais Royal.'
WHERE title.id_uuid = 'qr1574058f3377a48d9a61e3647a3a4c8aa';


UPDATE title
SET entry_name='Portrait de Madame Lemercier, actrice du Palais Royal.'
WHERE title.id_uuid = 'qr17ab9627c24204c028ffe9a3680e44fed';


UPDATE title
SET entry_name='Portrait de Lhéritier, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr1f81a8c0f8f6c4d7a94470f0622ba5102';


UPDATE title
SET entry_name='Portrait d''Alphonsine, actrice du Palais Royal.'
WHERE title.id_uuid = 'qr179b8b675a539463695ac0199db338cfb';


UPDATE title
SET entry_name='Portrait de Calvin, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr139639dbd720a4120982b75f86b8a8ef9';


UPDATE title
SET entry_name='Portrait de Delannoy, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr105b00b9b3a5a494caa8a192975a5de3e';


UPDATE title
SET entry_name='Portrait de Hyacinthe, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr14a0c2718f3814037af94c3ed07611583';


UPDATE title
SET entry_name='Portrait de Lassouche, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr19405b1c04637479999d151b830c90ea8';


UPDATE title
SET entry_name='Portrait de Montbars, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr174e8c99d51f44e1ca71bd5c5dc8238e7';


UPDATE title
SET entry_name='Portrait de Daubray, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr1f46dfb8ddff046e1ae5d4a747f009c76';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice du Palais Royal.'
WHERE title.id_uuid = 'qr1065dc2ac7b6d4911bc968927689ca63f';


UPDATE title
SET entry_name='Portrait de Geoffroy, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr192cff895f4df4edb800ba10beb18d8bc';


UPDATE title
SET entry_name='Portrait de Pradeau, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr19731aaf0870d4da6ad801c968820b967';


UPDATE title
SET entry_name='Portrait de Berthelier, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr1ebf5efb91d2643a6a02355e1872efe04';


UPDATE title
SET entry_name='Portrait de Milher, acteur du Palais Royal.'
WHERE title.id_uuid = 'qr10a0c9cc113464693bab750b62633c5e2';


UPDATE title
SET entry_name='Portrait de Marsan, actrice au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr16ef755bd23ab4cb387869fd15fedf7b8';


UPDATE title
SET entry_name='Portrait de Marsan, actrice au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr1ffd91d4de61b48dbabfaaae15124242f';


UPDATE title
SET entry_name='Portrait de Marsan, actrice au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr11811b483e52f47da9c921baa06d58fac';


UPDATE title
SET entry_name='Portrait de Rose Janin (actrice au Palais Royal)'
WHERE title.id_uuid = 'qr1402adf7979914c11a2b93560d5f2bb2b';


UPDATE title
SET entry_name='Portrait de Rose Janin (actrice au Palais Royal)'
WHERE title.id_uuid = 'qr1c4a977fdff464b869947086a593da0ee';


UPDATE title
SET entry_name='Portrait de Martha, actrice au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr15c205550ed2a42f7b4eb52d0bb42a4ee';


UPDATE title
SET entry_name='Portrait de Dumont (actrice au Palais royal)'
WHERE title.id_uuid = 'qr145b76a887e8b44d7abc29f9418fcd3b9';


UPDATE title
SET entry_name='Portrait de Dumont (actrice au Palais royal)'
WHERE title.id_uuid = 'qr170b01f6b28b34625bde145a5fb0f0fa3';


UPDATE title
SET entry_name='Portrait de Merville, actrice au Palais Royal.'
WHERE title.id_uuid = 'qr174d2cc37c741484881ee2106d5275f4e';


UPDATE title
SET entry_name='Portrait de Merville, actrice au Palais Royal.'
WHERE title.id_uuid = 'qr17c0135d30de4447eba306b838986a1e4';


UPDATE title
SET entry_name='Portrait de Merville, actrice au Palais Royal.'
WHERE title.id_uuid = 'qr19290d8c2d0504ff8bda440116a8e1d24';


UPDATE title
SET entry_name='Portrait de Merville, actrice au Palais Royal.'
WHERE title.id_uuid = 'qr17cdb3db2708e47f6ad9c486804817f32';


UPDATE title
SET entry_name='Portrait de Merville, actrice au Palais Royal.'
WHERE title.id_uuid = 'qr1ad02428e399949a7800bf0b9cd3ca4da';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr11154a6020d7947d1a2f29b0b2116a148';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1913d15e138394031b249b657a87871ce';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr12e387c661b42427695d6c1b29b88da27';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1d9692b50d2f94a1faebfbf694c17b848';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1feeb5eeb8a6747bdbd4a337ffada9ede';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1c13b7ddcab6f487eb9a8f4366c9dc3b4';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1dc7f345f94f241dd8bf2017a7c55ded6';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1d0137cdc47e64930bac0873f33d376fc';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr191481c4d672e4ae08a53cc9e72b9428b';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr17131ce8a0070439dbf71dc0f0cc07bd8';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr193dfe36f069440a98ff4cdbef0b02279';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1063820999d1f49e9a6863d648c21178e';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr174f27e183247486b85b2bf924d4d9360';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1374f799134424ea8b48d2d5e9ac3161e';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr15f3bdcfaaa6b45c5b049d7a812459803';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr19a5815403ba44fca97d05b789c4ac52b';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr18d494e550a0c44078e6047f617c646ef';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr1fd1c2211f41c4e359dcca1d9d3271d58';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr1e68650a127fe4da6be60d20770bd0fe5';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr1aa936554ca854051af7a4039501c44a0';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr1502b8b5f9a244092b2e98704489f6380';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr139a97e2786f64998ac60c6809b483210';


UPDATE title
SET entry_name='Portrait de Lucie Verneuil (-1890), actrice au Théâtre du Palais Royal en 1865.'
WHERE title.id_uuid = 'qr132542c48f1184a7399f7b005540f0f5a';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr1ec50df75aa2e4803bf675dde23ab238f';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr1246e16e7714a4f738bca0dedc099b99f';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr1c5cbd5cd32a141ee9061a1718eca108e';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr117faa2b8c7594bd9b8758e2b6f5b43fc';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr15579d83d01344a688871b7c11a359ee5';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr17861b57cac9648e49b9c2966ae8ce1ee';


UPDATE title
SET entry_name='Portrait de Valentine Biron (actrice au Palais Royal et au Théâtre des Nouveautés)'
WHERE title.id_uuid = 'qr11e708e4e7dc248cfb5d22d38a38e9cc4';


UPDATE title
SET entry_name='Portrait de Céline Montaland, actrice du Palais Royal.'
WHERE title.id_uuid = 'qr1f78bf08107014dcea3c19df7bc11e7ae';


UPDATE title
SET entry_name='Portrait de Dubouchet (dite Plunkett), (actrice au théâtre du Palais Royal)'
WHERE title.id_uuid = 'qr1a7726dd379c44351973708443da22341';


UPDATE title
SET entry_name='Portrait de Dubouchet (dite Plunkett), (actrice au théâtre du Palais Royal)'
WHERE title.id_uuid = 'qr1b86c589bd05a42b1af8cbe624db07c92';


UPDATE title
SET entry_name='Portrait de Marie Dremond ou Dreymond (actrice au Palais Royal)'
WHERE title.id_uuid = 'qr142829bb83cd2457581072075a2e62c42';


UPDATE title
SET entry_name='Portrait de Sylvia, actrice au Théâtre du Palais Royal en 1870.'
WHERE title.id_uuid = 'qr1933d643c92584a2da7304799cb449b84';


UPDATE title
SET entry_name='Portrait de Sylvia, actrice au Théâtre du Palais Royal en 1870.'
WHERE title.id_uuid = 'qr102188c907feb4361920d70b0572ab84a';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1d885527297bb4afebf15f67438bacdbe';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr148ad1455616343f298d6438975122ac4';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr19f7946756f304edfbcd5ecab520e77a6';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1040c5556605d41968bb7160151748a8a';


UPDATE title
SET entry_name='Portrait d''Elise Damain (actrice aux théâtres du Palais Royal, de l''Ambigü et du Vaudeville)'
WHERE title.id_uuid = 'qr1b10930fe85414feebefd0b11ab9550d3';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr1d84405f515ed426dbf805ad0ba00244c';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr1e475021c044d447a9bb9cc8e38e57757';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr10872b62ee0464b0cb87e07ee06ed0fe6';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr19fa599bce53340869e392febfcb70a90';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr1f4eccb51605d47cfae08cb54ea9e7374';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr1725746a14add49b1a6bf2160fe258c95';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr121c9033190124df5ad9c55e91dab462e';


UPDATE title
SET entry_name='Portrait de Gabrielle Tanting, actrice au Théâtre du Palais Royal en 1874 et au Théâtre du Gymnase.'
WHERE title.id_uuid = 'qr12e7ed15fd9574976ac07e3535890eb4a';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr11692fb85dfab425bb2881cd0f5ec5a27';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr103ffdda74ce54bd887f4747322b774e5';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr124089f2ce2aa49faa0c69e826f5ab102';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr18438fdd5553741ca973b03b8fcf224d2';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr13ef888440ae743289a44fe06904d588c';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr1a91ce4002d214fe4b3e6817082c21356';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr17b6181b092f549c3ace65ce01624b581';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr127e907ad3f0b4943a62ba835f43517f3';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr1c66c385b96274d5d968ce052aa2af62a';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr18539b48dcac04352a92e55d67b53ed8d';


UPDATE title
SET entry_name='Portrait de Mélita, actrice au Théâtre du Palais Royal en 1875 et au Théâtre du Vaudeville en 1874.'
WHERE title.id_uuid = 'qr121179b5cdf33463e951d567c053a629d';


UPDATE title
SET entry_name='Portrait de Louise (ou Lucile ?) Durand (actrice au théâtre du Palais royal)'
WHERE title.id_uuid = 'qr16cf63fc20c0d46f392b81155d12f83b1';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr11ace13275b2540a58244f48449c04aa8';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr1814e42cdbb5d47ae820183504f6e8319';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr15ad50db1bd574687bb04662bfed6e926';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr1d08f2f53c553494c80819eab9477ce22';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr1d30b3799235c4a5e8cebc0e9c35b70a8';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr12e92770d391e4adca4c3721f4b69c5ca';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr189a2b92a8b53421ba6bb6ccbbfbe3fc0';


UPDATE title
SET entry_name='Portrait de Marie Verne (1818-1895), danseuse à l''Opéra en 1866 et au Palais Royal.'
WHERE title.id_uuid = 'qr1df21a6f4a4d542bd8d07dee762448fb2';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr11833c68e8c41426995614f7adef5f112';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr11aa498c0a2bb49609e40af0c38d158ce';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr12449ec89abb44378b3eca4d93f50bf77';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr1d077e18c9d084ac0a1d957815a9b8728';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr155927f4fc3ce41d38d7a5cfb2794aa4a';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr1bb3931be496c43ee81299ce4d044d8e6';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr1b1a6c1fd0bb543678dd2a931e9411cea';


UPDATE title
SET entry_name='Portrait de Raymonde, actrice au Théâtre des Bouffes Parisiens en 1868 et au Théâtre du Palais Royal en 1880.'
WHERE title.id_uuid = 'qr17f2ea235adf74884bee07f81f123bda9';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr13f81135906f9415284fb75c836aed983';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr1d015dca3887c478fa0597816514eda57';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr1cb6d6532e52f48658bfdf04c903e88d2';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr107e4aa5593c748959dcd8bb404944054';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr1fd766def2df44d87bfcbb28a3cb9147e';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr132bd8f73eebe456bbe85975391a3f4d7';


UPDATE title
SET entry_name='Portrait de Sylvia Sportas, dite 99 fois Sylvia, actrice au Théâtre du Palais Royal en 1871.'
WHERE title.id_uuid = 'qr1a9424891e67d48e9983f9930413a0249';


UPDATE title
SET entry_name='Restaurant "Au bœuf a la mode", balcon, rue de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr15ab9bdba68b4489b86daf6195875e4ab';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1a8d95cbac481440d9cca18edf7c69d81';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1fb743bf9997142deaf8ed285cd260cd0';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr193e65a22c3a34f0886ec20d92b1b9924';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr110377c8efbd74f7f93d9de5409892fab';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr118bd2f0b520d40e6871859acec25e086';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr12ccc4cc18e3f4cfd95164d2d1aaf167e';


UPDATE title
SET entry_name='Portrait de Gabrielle Moralès (-1880), actrice au théâtre des Folies-Dramatiques en 1875 et au théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr14644aba0f7f743e5a498bce0f6d6a90e';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr14db511a67f884f2983d8c9fa8f2dff2b';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr15bf3ff6b50814bdba8408372dd9ab068';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr16c8340bf11f842b59969d13a2e71647c';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr124c32c51fbbe44f4b3b917f33ae8d7bc';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr16db23165d99e4b278325916dd93a72c7';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr14f02e82173c646a28192c9751e2f7820';


UPDATE title
SET entry_name='Portrait de Milia (Tendel), actrice au Théâtre des Menus-Plaisirs en 1867, au Théâtre des Variétés et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr1d5d80117618348f78f6450c151db6617';


UPDATE title
SET entry_name='Portrait de Valérie Anselin, dite Valérie (1849-1883), actrice au Théâtre Déjazet en 1868 et au Palais Royal.'
WHERE title.id_uuid = 'qr15ed798b4eae34bc2b535d5565a77f5a9';


UPDATE title
SET entry_name='Portrait de Valérie Anselin, dite Valérie (1849-1883), actrice au Théâtre Déjazet en 1868 et au Palais Royal.'
WHERE title.id_uuid = 'qr1c1c8b3b7e5d842c595ef870373146215';


UPDATE title
SET entry_name='Portrait de Valérie Anselin, dite Valérie (1849-1883), actrice au Théâtre Déjazet en 1868 et au Palais Royal.'
WHERE title.id_uuid = 'qr10fcfdf55404a4d689b5fe8362da28e3b';


UPDATE title
SET entry_name='Portrait de Valérie Anselin, dite Valérie (1849-1883), actrice au Théâtre Déjazet en 1868 et au Palais Royal.'
WHERE title.id_uuid = 'qr1ee07fe490b5a400dbb892592955b43f7';


UPDATE title
SET entry_name='Portrait en buste de Gabrielle Moralès (-1880), actrice au Théâtre des Folies-Dramatiques en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr16a583f0c4cc245b39069357d89b7dc83';


UPDATE title
SET entry_name='Portrait d''Emilie Charlotte Cornu, dite Madame Provost, actrice au Théâtre du Palais Royal entre 1859 et 1865.'
WHERE title.id_uuid = 'qr1a066ebe296c74a90b8c4b6de0899d33b';


UPDATE title
SET entry_name='Portrait d''Emilie Charlotte Cornu, dite Madame Provost, actrice au Théâtre du Palais Royal entre 1859 et 1865.'
WHERE title.id_uuid = 'qr1f3f8f0a3a8b24e52b9f899ecb4dd552a';


UPDATE title
SET entry_name='Portrait d''Emilie Charlotte Cornu, dite Madame Provost, actrice au Théâtre du Palais Royal entre 1859 et 1865.'
WHERE title.id_uuid = 'qr1e3a5df62c92e4de580619260fdff00f2';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr1fa459557680547aeb74ea298cf6f697a';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr1b44aaa4e6ce342ad8fcebcaa43156cf2';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr19423408a7c404e4a862225a7bf5c7491';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr13ef0f416c9ad4b32ad200e840d2a25b0';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr1468d68815cf44747906f29e55698f7af';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr175ca8317138a4cc2a74a4a537d038ed9';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr18085076b02ed44f2874a107fccf45c94';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr1a60b649ba6d8411cbf5cd65d4e414417';


UPDATE title
SET entry_name='Portrait d''Ida Klein, actrice au Théâtre du Palais Royal (entre 1862-1865) et au Théâtre des Variétés (en 1872).'
WHERE title.id_uuid = 'qr1d62b7950efd349cdaccff011a2db31f9';


UPDATE title
SET entry_name='Portrait d''Alice Marot (v. 1855-), actrice au Théâtre des Folies Marigny en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1c4bbeeced0de45a494d8d1c5218cc70a';


UPDATE title
SET entry_name='Portrait d''Alice Marot (v. 1855-), actrice au Théâtre des Folies Marigny en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1138556fa55d948aa890c350d2210d1ea';


UPDATE title
SET entry_name='Portrait d''Alice Marot (v. 1855-), actrice au Théâtre des Folies Marigny en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1a0e0858b0da34492b326c2135a43600d';


UPDATE title
SET entry_name='Portrait d''Alice Marot (v. 1855-), actrice au Théâtre des Folies Marigny en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr1b041f78cf8944882ad49a4b2c3c59430';


UPDATE title
SET entry_name='Portrait d''Alice Marot (v. 1855-), actrice au Théâtre des Folies Marigny en 1875 et au Théâtre du Palais Royal en 1878.'
WHERE title.id_uuid = 'qr151025ac7a9c34b26bbee43acddfb2000';


UPDATE title
SET entry_name='Portrait de Berthou, actrie au Palais-Royal'
WHERE title.id_uuid = 'qr11f78af9056a2488abf45e77f9bf581df';


UPDATE title
SET entry_name='Portrait de Julie Justine Pilloy, dite Alice Ozy, actrice au Théâtre des Variétés, au Théâtre de Vaudeville et au Théâtre du Palais Royal.'
WHERE title.id_uuid = 'qr1fe566cc04a6a4b96b50e446630cf802c';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr16b25fee6841646c4b9e88064954b4b42';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1c05ff750ced340f794a0d331427b21b7';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr12b075d45130a4dad8c7324e0c0790a23';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1aa504beab5f64de3babcb40a0536be0b';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1c270220e7f234b958ed75b6c8061a43d';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr174f961d27d3640f389772f1bed155104';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr122d15f0e6f2841c3b654caa30c5dc312';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1e3a9335b10af4a96b4d14cb6dd84afb1';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr127d229e7f90f4299a7ad0545f86fd8c2';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1c8f50a0123a542be932c38e5d578c372';


UPDATE title
SET entry_name='Portrait de Nathalie Marie Hyacinthe Martine, actrice au Théâtre du Palais Royal entre 1859 et 1869 et au Théâtre des Variétés en 1869.'
WHERE title.id_uuid = 'qr1fd7d6bf9a9b8460db996b7b5b756aa64';


UPDATE title
SET entry_name='Portrait d''André Ellen, actrice au Palais Royal'
WHERE title.id_uuid = 'qr1ec8e3c35073a4dfea70a92130816cf7d';


UPDATE title
SET entry_name='Les hommes se prennent par la douceur.'
WHERE title.id_uuid = 'qr1efb667738b2d4a86830177a0364bd13c';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1bdaaf4f485ec41d195d966dcdd8d2228';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1782e0cedc9774efe937a42df6052faa7';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1ae5f159f959c4a9ab95a2d124162da21';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr19d4598274ab4416f88b5b8b67a0f1b08';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr11daebe0b2138457da8c0828b5bc6f228';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr13cceb4ca04fe44cea0ba87757853f319';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr120252ddc3d4040df858210e99bdb4e3e';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr15fa878e7a2c94fc388ca235789f9bde2';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1f3ba54c7bb894f95867b221932c81bc8';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr197c61028966e4cdab959893a2e1d033b';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr10b33a109c6b2454fb4db5e2096a962d1';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1fd7d0ee8c6ea455d938d09223424a4ba';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr15125d025f8104b928979a5e06bf96c96';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr13547fc5cf25744d4a5ef3a40c6f5b299';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1ff1b4355a4c749099d11c9e2db5c130c';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr140ee46c7b6d84f6298ac49c6e6497096';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1069bb1189b0d40dcbd9dddcdf48089e9';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1fcd60e3143f1427e8f22a256e0f67a2a';


UPDATE title
SET entry_name='Portrait de Félix Galipaux, (1860-1931), dramaturge, romancier, comédien, humoristePalais-Royal, Vaudeville, Gymnase'
WHERE title.id_uuid = 'qr1e66274c31a344045b90dfbacfdc25194';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Alfred Ravel (1814-1881), acteur'
WHERE title.id_uuid = 'qr10619fe853d0d4cd084f0a83144f3977f';


UPDATE title
SET entry_name='Portrait-charge de Louis Lemenil (?-1872), acteur. Rôle du capitaine Truguet dans "Le Conseil de Discipline"'
WHERE title.id_uuid = 'qr14ab763252dfa459b9343573f185deacf';


UPDATE title
SET entry_name='Portrait-charge de Louis Lemenil (1804 -1872), acteur. Rôle du capitaine Truguet dans "Le Conseil de Discipline"'
WHERE title.id_uuid = 'qr1239ffe83155a4565ba04df92d0e8ceba';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Frédéric Achard (1808-1856), acteur, dans "l''Enfant du Faubourg"'
WHERE title.id_uuid = 'qr11a9ba355371248a8a65246bc5a224ea6';


UPDATE title
SET entry_name='Portrait-charge de Boutigny'
WHERE title.id_uuid = 'qr12a9ca16308734120bcf67783cc8cc35e';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Frédéric Achard (1808-1856), acteur, dans "l''Enfant du Faubourg"'
WHERE title.id_uuid = 'qr1addf04a6690248fdb1521b7930413a8b';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Louis-Auguste Grassot (1804-1860), acteur au Palais Royal'
WHERE title.id_uuid = 'qr12d43f57d5470447a9c6999ce8aebe70f';


UPDATE title
SET entry_name='Portrait-charge de Pierre-Louis-Auguste Grassot (1804-1860), acteur au Palais Royal'
WHERE title.id_uuid = 'qr1a5209ba92bff4f2994fe4bbe593dd8f0';


UPDATE title
SET entry_name='Portrait sérieux de l''ex-directeur du Palais Royal et directeur du Théâtre français, Charles-Gaspard Poirson, dit Delestre-Poirson (1790-1859)'
WHERE title.id_uuid = 'qr1bf241ddbf19746fabd66c7b67902de52';


UPDATE title
SET entry_name='Perruque démesurée'
WHERE title.id_uuid = 'qr184aedfd084fa4920b3d421ccc1c6bade';


UPDATE title
SET entry_name='Portraits fidèles des maisons à la mode N°4 / La Belle Limonadière ou le café des mille colonnes.'
WHERE title.id_uuid = 'qr15a4073cb3e204c9fa31e4b933bd4f3ec';


UPDATE title
SET entry_name='Paris ( 1er arr . ) , France La rue de Montpensier vue de la rue Beaujolais'
WHERE title.id_uuid = 'qr1d5e1976a983c4af9af6c9f8cac3d0167';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France Le Conseil d'' État place du Palais-Royal'
WHERE title.id_uuid = 'qr1f96497dddfa1472893865fe0b352887d';


UPDATE title
SET entry_name='Bouillon Duval'
WHERE title.id_uuid = 'qr136917800fb8649028051975e18783e38';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France Bureau de l'' Institut International de Coopération Intellectuelle de la Société des Nations , 2 rue Montpensier ( aujourd'' hui siège du Conseil Constitutionnel )'
WHERE title.id_uuid = 'qr145ef0bb8fc404eca90a2c561292e78ae';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France Bureau de l'' Institut International de Coopération Intellectuelle de la Société des Nations , 2 rue Montpensier ( aujourd'' hui siège du Conseil Constitutionel )'
WHERE title.id_uuid = 'qr16a9aa9028d114927b73a01ad6d932563';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France Le jardin du Palais-Royal'
WHERE title.id_uuid = 'qr119081ab4fa9f42a8877fcdb523521353';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France Le jardin du Palais-Royal'
WHERE title.id_uuid = 'qr19f6e5edfcc5c48b1976f451bf4d39533';


UPDATE title
SET entry_name='Paris ( 1er arr . ) , France La rue de Beaujolais'
WHERE title.id_uuid = 'qr1522c6207c0fb4f78b9f27c166bcacadf';


UPDATE title
SET entry_name='Paris ( Ier arr . ) , France La rue de Montpensier'
WHERE title.id_uuid = 'qr190042a6203e04720863afddc19130360';


UPDATE title
SET entry_name='La Banque de France, rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1dfc5c5c28e8c497c95ba5076ac710758';


UPDATE title
SET entry_name='"Rue Radziwill - la Banque de / France / 1907" (Titre donné par l''auteur)'
WHERE title.id_uuid = 'qr17c0e723c1cc54b4eab22d4f1e06861d3';


UPDATE title
SET entry_name='La Banque de France, rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1327bbb9ca5b342299f784ab919a031b1';


UPDATE title
SET entry_name='Jeton de jeu : Banque de France, XIXe siècle'
WHERE title.id_uuid = 'qr162e54b9111164bbd878686bd5fc5e2ee';


UPDATE title
SET entry_name='Jeton de jeu : Banque de France, XIXe siècle'
WHERE title.id_uuid = 'qr1d26f491778a6496ca6664f973290e194';


UPDATE title
SET entry_name='Jeton de jeu : Banque de France, XIXe siècle'
WHERE title.id_uuid = 'qr1e9790c5f42bf45988b57548cbdf96978';


UPDATE title
SET entry_name='Bas-relief extérieur du mur d''enceinte de la Banque de France (ancien hôtel de La Vrillière), 1 rue La Vrillière, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1b63156186ca349a79b848fe21a626fe2';


UPDATE title
SET entry_name='Détail du Bas relief extérieur / gauche du n°1 rue La Vrillière / Le Droit et la Justice / dans le médaillon du centre / / la Force'
WHERE title.id_uuid = 'qr1393db8a032b04942aefd99708a413d77';


UPDATE title
SET entry_name='Façade de la Banque de France sur la rue Croix-des-Petits-Champs (1er arrondissement, Paris). Reproduction d''une gravure parue dans l''Illustration en 1872.'
WHERE title.id_uuid = 'qr1050641cc42794a409a7609af3e76569e';


UPDATE title
SET entry_name='Façade de la Banque de France sur la rue / Croix des Petits Champs (Extrait de l''Illustration : 1872) (Titre attribué), Paris. - Nouvelle façade de la Banque de France, sur la rue Croix-des-Petits-Champs.'
WHERE title.id_uuid = 'qr1bab3ede50eb3468a9d65dbee0db11052';


UPDATE title
SET entry_name='Vue extérieure sur la rue La Vrillière et vues des services intérieures de la Banque de France (1er arrondissement, Paris). Reproduction d''une gravure parue dans l''Illustration en 1846.'
WHERE title.id_uuid = 'qr1b406285b5a1543499ac1bbf20ae131e0';


UPDATE title
SET entry_name='Vue extérieure de la Banque sur la rue / La Vrillière et vues des services intérieures en / 1846 (Extrait de l''Illustration)'
WHERE title.id_uuid = 'qr13bba1f8f59fd472a85ffbaac526623f9';


UPDATE title
SET entry_name='La foule venant au remboursement des billets à la Banque de France (1er arrondissement, Paris). Reproduction d''une gravure parue dans l''Illustration en 1870.'
WHERE title.id_uuid = 'qr1d053477d456d4dc1978aebc750f8acfc';


UPDATE title
SET entry_name='La foule venant au remboursement des billets / rue La Vrillère [sic] en Aout 1870 - (Extrait de l''Illustration). (Titre attribué), Le cours forcé. - La queue des porteurs de billets, à la Banque de France.'
WHERE title.id_uuid = 'qr1695ba9dc49c44cc2ae31e6ed50f8e6e7';


UPDATE title
SET entry_name='La foule venant au remboursement des billets à la Banque de France (1er arrondissement, Paris). Reproduction d''une gravure parue dans l''Illustration en mars 1848.'
WHERE title.id_uuid = 'qr18dca8879afbb4f348d8d66c3f8f6ab3f';


UPDATE title
SET entry_name='La foule venant au remboursement des billets / rue La Vrillère [sic] en mars 1848 (Extrait de l''Illustration)'
WHERE title.id_uuid = 'qr1b35b2b735d3b472d908526c73e72d183';


UPDATE title
SET entry_name='Document officiel pour la réquisition d''une somme d''argent de la part du Comité du salut public auprès de la Banque de France, mai 1871.'
WHERE title.id_uuid = 'qr13a2a9613187d419a947af86dbd57cded';


UPDATE title
SET entry_name='Document officiel pour la réquisition d''une somme d''argent de la part du Comité du salut public auprès de la Banque de France, mai 1871.'
WHERE title.id_uuid = 'qr1a9af09fa87734aa1b3b9420e1b5766fc';


UPDATE title
SET entry_name='Galerie du duc de la Vrillière servant à l''assemblée générale des actionnaires de la Banque de France (1er arrondissement, Paris).'
WHERE title.id_uuid = 'qr110b60e0c36e040a2b9f936b982026112';


UPDATE title
SET entry_name='Immeuble avant sa démolition par la Banque de France entre 1920 et 1925, 13 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18ce39d904f6b48579b1597989877120c';


UPDATE title
SET entry_name='13 r. Radziwill / Immeuble démoli par la Banque de / France 1920-1922'
WHERE title.id_uuid = 'qr12b742691574f488d91df21ee6127d22d';


UPDATE title
SET entry_name='Immeubles de la rue Radziwill avant leur démolition par la Banque de France entre 1920 et 1925, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr13b22a31bf3c94ea8bd10e33ccd696a57';


UPDATE title
SET entry_name='Immeuble d''angle rue Radziwill n°9 / (les 5 premiers immeubles ont été démolis par / la Bq de France 1920-1925)'
WHERE title.id_uuid = 'qr1e6d7b726d042409a9a046dc341748ac3';


UPDATE title
SET entry_name='Service architecture de la Banque de France, 19 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1cc2adcfbc8594df68f55985bbceeb520';


UPDATE title
SET entry_name='19 rue Radziwill / Porte d''entrée'
WHERE title.id_uuid = 'qr1763769a7cbce4825bd5a14469c8b06b9';


UPDATE title
SET entry_name='Hall vitré de la Banque de France, Paris'
WHERE title.id_uuid = 'qr123221ad752d5420cb1d23f0d2e13d7bd';


UPDATE title
SET entry_name='Hall vitré / 1925'
WHERE title.id_uuid = 'qr134cacfb5c0a54a398b4cb7e4eeb37faf';


UPDATE title
SET entry_name='Cheminée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1079675ec7da24d3e87e42213853b2cae';


UPDATE title
SET entry_name='Vue de la cheminée de la galerie Dorée après reconstruction en 1870-1875 sous la direction de l''architecte Questel'
WHERE title.id_uuid = 'qr1b640e8764c964bf6a2dee0e2554b62a4';


UPDATE title
SET entry_name='Porte d''entrée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16f6184b9b2bb4826a78a0bafeab92727';


UPDATE title
SET entry_name='Porte d''entrée de la galerie Dorée / 1923'
WHERE title.id_uuid = 'qr1c87bf110623a4587bb864e4ce503af30';


UPDATE title
SET entry_name='Porte d''entrée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18b60e6d3376b4c669bfae3d3630676bf';


UPDATE title
SET entry_name='Vue de la galerie Dorée et de son entrée / prise de la cheminée / 1923'
WHERE title.id_uuid = 'qr16f432dca11cb4970a8a19bc4b15b8471';


UPDATE title
SET entry_name='Porte d''entrée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c28c08548c614d24a492c5ce5a8cc9cc';


UPDATE title
SET entry_name='Porte d''entrée de la galerie Dorée / 1923'
WHERE title.id_uuid = 'qr13317e543361647599436aea09c923bd7';


UPDATE title
SET entry_name='Cheminée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr17aa4c1ccf4524825a96e07c9e7607667';


UPDATE title
SET entry_name='Vue de la galerie prise de la / porte d''entrée, 1937'
WHERE title.id_uuid = 'qr1d30a41d51cf64e8f938500a9b0f996b2';


UPDATE title
SET entry_name='Cheminée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ecd3915c7e034773bc2aed6c9dcc20f3';


UPDATE title
SET entry_name='Vue de la cheminée de la galerie Dorée en 1923'
WHERE title.id_uuid = 'qr1851442db0b634cc8af9026ba9e989cc4';


UPDATE title
SET entry_name='Cheminée de la galerie Dorée de l''hôtel de Toulouse, Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c2d582a37dc648439fb2d2b1f90f84b3';


UPDATE title
SET entry_name='Vue de la Cheminée de la Galerie Dorée en 1937'
WHERE title.id_uuid = 'qr1eec0eb261d934d6fb83bd934cd5f2620';


UPDATE title
SET entry_name='Couloir, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr1e0d3a94b2b5c4ef0a6b21beeadcfd2ae';


UPDATE title
SET entry_name='Salle souterraine / partie [ds serres]'
WHERE title.id_uuid = 'qr1cc32df3abd0b46eab61ab6fcdcb40a28';


UPDATE title
SET entry_name='Porte d''accès au groupe des salles fortes, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr1ecb4b05f2b014ecb8a570403e536873a';


UPDATE title
SET entry_name='Salle souterraine / Porte d''accès au groupe des Salles fortes'
WHERE title.id_uuid = 'qr119df6729ef724d14a4c2e2f5e4d3a10e';


UPDATE title
SET entry_name='Salle de travail à colonnes, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr153f3895a27044f00ae303097d177e7cd';


UPDATE title
SET entry_name='Salle souterraine / - autre vue sur la salle de travail.'
WHERE title.id_uuid = 'qr16f57560363a14aceb63f62bda90bc558';


UPDATE title
SET entry_name='Salle de travail à colonnes, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr167d52d6ed3464646b7ee90c0c1ffe8bb';


UPDATE title
SET entry_name='Salle souterraine / Salle de travail - Etat actuel'
WHERE title.id_uuid = 'qr1224bb730c24c4ea2ba484628d1fff352';


UPDATE title
SET entry_name='Groupe des salles fortes, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr1f7ac4a1c626447b38fa5b42f01ec340f';


UPDATE title
SET entry_name='Salle souterraine / Groupe des salles fortes / dont l''une est ouverte.'
WHERE title.id_uuid = 'qr1b270d5b807314961b2446ca06ec9d4f9';


UPDATE title
SET entry_name='Plaque commémorative des travaux de la salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr167f783d0722343dc93062f5cd4c3c2e1';


UPDATE title
SET entry_name='Plaque commémorative des travaux de la Salle souterraine commencées en 1924 et achevés en 1927.'
WHERE title.id_uuid = 'qr190da3b6949b74dd3b2391ebf854429ee';


UPDATE title
SET entry_name='Porte-forte d''accès avec système de fermeture, salle souterraine de la Banque de France'
WHERE title.id_uuid = 'qr1bc2a92b0b1884f2796e9aa30fa86b4b8';


UPDATE title
SET entry_name='Hôtel de la banque de France / Porte d''entrée, rue de la Vrillère.'
WHERE title.id_uuid = 'qr17e76b769a85b4c20814f21a2942a3c6c';


UPDATE title
SET entry_name='Hôtel La Vrillère, Hôtel de Toulouse, Banque de France, rue Croix-des-petits-champs. / B (Titre de la série)'
WHERE title.id_uuid = 'qr116832358f1604f9c869275628bd5a261';


UPDATE title
SET entry_name='Actualités n°30 : A propos des Nouvelles caves de la Banque de France. - C''est moi qui voudrais être sommelier...'
WHERE title.id_uuid = 'qr19353e2e09f2f454f830d48e82f2db5fa';


UPDATE title
SET entry_name='Vue de l''hostel de la Vrilliere du Dessein de François Mansart'
WHERE title.id_uuid = 'qr1f4a9d24cf0584e129f10060f7abc1456';


UPDATE title
SET entry_name='Projet d''un Plafond qui devoit estre executé dans la grande Gallerie de la Banque a Paris.'
WHERE title.id_uuid = 'qr1502ef365bd3a46aa963d52d7aabfba7f';


UPDATE title
SET entry_name='Banque de France, an VIII'
WHERE title.id_uuid = 'qr1c36a817b80784bb3931f708b2fdd10df';


UPDATE title
SET entry_name='Banque de France, an VIII'
WHERE title.id_uuid = 'qr19827b8db59e5450692f7dde082f887ab';


UPDATE title
SET entry_name='Projet pour la banque de France'
WHERE title.id_uuid = 'qr1a1e60223d41c422c80bf91887edf8184';


UPDATE title
SET entry_name='Benjamin Dudessert'
WHERE title.id_uuid = 'qr188fae60059964e56978f48e73101dd52';


UPDATE title
SET entry_name='Je souscris pour l’ami à qui je dois tout… voilà cent sous, rendez moi cinq francs'
WHERE title.id_uuid = 'qr150c643a5b9aa46a98a381a8134319880';


UPDATE title
SET entry_name='La Caricature politique, morale, littéraire et scénique (Titre de l''ensemble)'
WHERE title.id_uuid = 'qr14031895cfc8e48f5a4f3460806898251';


UPDATE title
SET entry_name='Bertand, j''adore l''industrie...'
WHERE title.id_uuid = 'qr1d72025afff53422483225228bf35ef73';


UPDATE title
SET entry_name='Les Robert Macaires par Daumier & Ch. Philipon, galerie morale des voleurs, spéculateurs, dupeurs, tireurs, enfonceurs et blagueurs divers que nous rencontrons dans Paris (Titre de la série), Caricaturana n°1 (Titre inscrit (lettre))'
WHERE title.id_uuid = 'qr1b389f99b26d444c5b21b2689a1511fb5';


UPDATE title
SET entry_name='Trompe de la Banque, rue neuve des Bons Enfants'
WHERE title.id_uuid = 'qr16b676d05b7d34c629ed86cbfcd1e822c';


UPDATE title
SET entry_name='Vue de la Banque de France, rue Neuve-des-Bons-Enfants (actuelle rue Radziwill), 1er arrondissement, Paris. (Titre factice)'
WHERE title.id_uuid = 'qr1d8b6abcaf84241ac813ad51042dd5a8b';


UPDATE title
SET entry_name='Portrait de Magnin Joseph, (1824-1910), (homme politique, gouverneur de la Banque de France)'
WHERE title.id_uuid = 'qr1cc1d06b6212740ecb8de3a78c9c525f9';


UPDATE title
SET entry_name='Portrait de Magnin Joseph, (1824-1910), (homme politique, gouverneur de la Banque de France)'
WHERE title.id_uuid = 'qr1e1cf0ea1d90e4678b531bdad2c1da408';


UPDATE title
SET entry_name='La banque de France faisant monter son escompte.'
WHERE title.id_uuid = 'qr1deb909776344421fb57ffc4a0f3c71c2';


UPDATE title
SET entry_name='Vue prise des travaux de la Banque de France en 1865, rue Baillif et rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14ac95d5afb724e3bbf0f9253867db752';


UPDATE title
SET entry_name='Banque de France. Paris. Vue des travaux prise le 15 Octobre 1865 de la rue Baillif côté de celle neuve des Bons Enfants.'
WHERE title.id_uuid = 'qr145860c65dee247259ac134233266127e';


UPDATE title
SET entry_name='Lithographie : Actualités n°30 : A propos des Nouvelles caves de la Banque de France. - C''est moi qui voudrais être sommelier...'
WHERE title.id_uuid = 'qr14102eb1766b64df1a4566cde8d3ec553';


UPDATE title
SET entry_name='Visite de l''impératrice et du prince impérial à la Banque de France.'
WHERE title.id_uuid = 'qr1d31ad40214a74e4ca95b88d27f0e975e';


UPDATE title
SET entry_name='L''impératrice Eugénie et le prince impérial visitent la Banque de France, 23 juin 1866'
WHERE title.id_uuid = 'qr161156ce052b546b9b8cb85c5cccdfbe3';


UPDATE title
SET entry_name='La Banque De France, rue de Radziwill, 1er arrondissement Paris. 7 octobre 1869.'
WHERE title.id_uuid = 'qr1bfd5a15bd22f497d9b565e15f3e7cc33';


UPDATE title
SET entry_name='Médaille décernée à Louis Losa par la Banque de France pour avoir assuré la défense lors de la Commune, 1871'
WHERE title.id_uuid = 'qr150a3848a28474abc993516588d6f2198';


UPDATE title
SET entry_name='Médaille décernée à Amiel par la Banque de France pour avoir assuré la défense lors de la Commune, 1871'
WHERE title.id_uuid = 'qr1f4e188726e2c484381b7031803fd2adb';


UPDATE title
SET entry_name='Médaille décernée à Jules Colson par la Banque de France pour avoir assuré la défense lors de la Commune, 1871'
WHERE title.id_uuid = 'qr1aed1c3a64ec343f9a1e807e7c2c68479';


UPDATE title
SET entry_name='Charles Beslay (1795-1878) sauvant la Banque de France de la destruction, 23 et 24 mai 1871'
WHERE title.id_uuid = 'qr1075b0462bc6749bba72c7923f0d0207f';


UPDATE title
SET entry_name='Paris. - Le triage des billets à la Banque de France.'
WHERE title.id_uuid = 'qr11b50b1fbe07f4a04834be8633cdd18a6';


UPDATE title
SET entry_name='Paris. – L’extinction des billets retirés de la circulation à la Banque de France.'
WHERE title.id_uuid = 'qr13f56a3e19b274c98b89f068a2319eba0';


UPDATE title
SET entry_name='Paris. - Le remboursement des billes de cinq cent francs aux guichet de la Banque de France.'
WHERE title.id_uuid = 'qr1702ca4fe16024040b3ba7069fe241d42';


UPDATE title
SET entry_name='Médaille du centenaire de la Banque de France offerte à l''agent Amiel, 1900'
WHERE title.id_uuid = 'qr1dbf8e5b12a7f4352a99ae4796ae58791';


UPDATE title
SET entry_name='Porte d''entrée de la cour d''honneur de la Banque de France (ancien hôtel de La Vrillière), 1 rue La Vrillière, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr11ce72df69273446c9b8d7a6a6ed43cc2';


UPDATE title
SET entry_name='Façade de la Banque de France, rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1588ede17524f4e7f9bd28fdd1a116ab7';


UPDATE title
SET entry_name='Façade de la Banque de France, angle de la rue Baillif et de la rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1af5ed391f1f44707a7f7c5340ce7883e';


UPDATE title
SET entry_name='Entrée de la Banque de France (ancien hôtel de La Vrillière), 1 rue La Vrillière, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14cb6f9cddd22488ba40c5652a78cedc3';


UPDATE title
SET entry_name='"Paris - Le dépôt de l''or à la banque de France"'
WHERE title.id_uuid = 'qr1d2cc511ca5e74c3fa37bbfbb1ea036d5';


UPDATE title
SET entry_name='Versement en or de 100 francs pour la défense nationale, Banque de France, 2 décembre 1915'
WHERE title.id_uuid = 'qr1d5093ab5fbc1473db37c2c19ec292e69';


UPDATE title
SET entry_name='Versement en or de 100 francs pour la défense nationale, Banque de France, 13 juillet 1915'
WHERE title.id_uuid = 'qr1ce86583976134a27adfdb6b93e687011';


UPDATE title
SET entry_name='EMPRUNT 5%%/ DE LA DEFENSE NATIONALE/ Que se lève cette armée de l''épargne française'
WHERE title.id_uuid = 'qr189de5560d5f04372ae6c987b01900b3b';


UPDATE title
SET entry_name='EMPRUNT 5%%/ DE LA DEFENSE NATIONALE/ EN SOUSCRIVANT/ VOUS FAITES LE MEILLEUR DES PLACEMENTS,/ VOUS FAITES VOTRE DEVOIR EN AIDANT VOTRE PAYS'
WHERE title.id_uuid = 'qr113bd3dd335b44b96a8b9da1cd7443c51';


UPDATE title
SET entry_name='EMPRUNT 5%%/ DE LA DEFENSE NATIONALE/ SOUSCRIVEZ!/ L''égoïsme à cette heure n''est pas seulement une lâcheté, une/ sorte de trahison, mais c''est encore la pire des imprévoyances.'
WHERE title.id_uuid = 'qr16f2653d1e412418db86d204d023e02f5';


UPDATE title
SET entry_name='EMPRUNT 5%%/ DE LA DEFENSE NATIONALE/ Que se lève cette armée de l''épargne française'
WHERE title.id_uuid = 'qr145b1742ed1aa4e109c0507dbcac85986';


UPDATE title
SET entry_name='PRÊTEZ VOS BILLETS/ DE BANQUE A LA/ FRANCE/ PRENEZ/ DES BONS DE LA DEFENSE/ NATIONALE'
WHERE title.id_uuid = 'qr1154329885f264b3da0b1d477e16d9128';


UPDATE title
SET entry_name='DEUXIEME EMPRUNT DE LA DEFENSE NATIONALE/ AGRICULTEURS FRANCAIS!'
WHERE title.id_uuid = 'qr10c40ee62d27040caae5a6c0b8ac856ad';


UPDATE title
SET entry_name='EMPRUNT NATIONAL/ DU TRESOR FRANCAIS/ RENTES 5%% AMORTISSABLES/ EXEMPTES D''IMPOT/ EMISSIONS AU PAIR- REMBOURSEMENT AVEC PRIME/ 100frs versés seront remboursés à 150 frs/ par tirages au sort semestriels/ SOUSCRIPTION OUVERTE DANS TOUS LES ETABLISSEMENTS...'
WHERE title.id_uuid = 'qr17e084cd651d94e38b0fbb14c67d065cb';


UPDATE title
SET entry_name='Inauguration du monument des agents de la Banque de France morts pour la France, le 14 décembre 1924.'
WHERE title.id_uuid = 'qr1d113c40a9c3e4a39bbe8212679936c4a';


UPDATE title
SET entry_name='Inauguration du Monument aux morts / 14 décembre 1924 / M. Robineau, gouverneur, à sa droite M. Clémentel, à sa / gauche Maréchal Joffre / A droite et à gauche de la colonne / M. Picard Premier Sous-gouverneur / M. Leclerc Deuxième Sous Gouverneur / Le... (Titre attribué)'
WHERE title.id_uuid = 'qr13ae583d178d94bdf8792d877e5163bcb';


UPDATE title
SET entry_name='Billet de 5 francs, type 1917 (Modifié), série U.60641, n°117'
WHERE title.id_uuid = 'qr1cf05eb8188b34cef988ec35781b8a44e';


UPDATE title
SET entry_name='Billet de 10 francs, type 1915 "Minerve", série E.75740, n°707'
WHERE title.id_uuid = 'qr16f5c721573b94fd68fbac809421d1ef3';


UPDATE title
SET entry_name='Billet de 20 Francs avec montage humoristique à l''effigie d''Adolf Hitler (provenant d''un timbre-poste), étranglé par le marin-pêcheur français'
WHERE title.id_uuid = 'qr1a07787b4db524f4f90d56285b017bbb6';


UPDATE title
SET entry_name='Billet de banque de 5 Francs (français)'
WHERE title.id_uuid = 'qr10b7a63fde47649de8a9e1b0e827f5f12';


UPDATE title
SET entry_name='Billet de banque de 10 Francs (français)'
WHERE title.id_uuid = 'qr1e2cec8bbb42b451380056e02389bad38';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec la Banque de France, la Bourse de commerce et les halles centrales. 1er, 2ème, 3ème, 4ème et 10ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr13c2879f1d3f442c28a2991771374dd1c';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec la banque de France, la bourse de Commerce, les halles Centrales et la rue Montorgueil. 1er et 2ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr1dff9a04a14c14e3ba8f288fd1dba7ccc';


UPDATE title
SET entry_name='Vue aérienne de Paris : La bourse de Commerce et les halles centrales, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr13a29bd2dea4f411e8afb22a3012cdcca';


UPDATE title
SET entry_name='Halles (Titre inscrit (lettre))'
WHERE title.id_uuid = 'qr1424664fbd0134dc5b51a20b62229a489';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec la banque de France et le jardin du Palais-Royal, 1er, 2ème, 9ème et 18ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr1101b1d220307470d9439aa624da586b9';


UPDATE title
SET entry_name='Vue aérienne de Paris : le Palais-Royal et la banque de France. 1er et 2ème arrondissements, Paris.'
WHERE title.id_uuid = 'qr16c26ca5182ba4815a9b53403dca6f80f';


UPDATE title
SET entry_name='Vue aérienne de Paris : Les Halles centrales, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1684590f98c0947a9acbb0ebc924ddba7';


UPDATE title
SET entry_name='Vue aérienne de Paris : la Banque de France et le Palais-Royal, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c106ebb983d4455782af3168f8e08969';


UPDATE title
SET entry_name='Vue aérienne de Paris : le palais et musée du Louvre, le Palais Royal et la banque de France. 1er et 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ffea9f60a6814d0586b3f015d8912c2c';


UPDATE title
SET entry_name='Vue aérienne de Paris : vue générale avec les halles centrales, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1eafca6fa7fe04dea872f7ec36aef5ba6';


UPDATE title
SET entry_name='Vue aérienne de Paris : Les Halles Centrales, la bourse de commerce, le palais et le musée du Louvre, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1fb6e12bd07ec4c78afbcfd787a164504';


UPDATE title
SET entry_name='Billet de cinq francs à l''effigie de Victor Hugo'
WHERE title.id_uuid = 'qr1741b267d7aee46f880596c1885cf1518';


UPDATE title
SET entry_name='Billet de 500 francs, type 1968 Pascal, Specimen de la Banque de France, n° 0596/000000/0.0'
WHERE title.id_uuid = 'qr1c4a97405be6b4053b639c084288ed3df';


UPDATE title
SET entry_name='Hôtel du duc de Noirmoutiers (disparu), 19 Rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr111efea4f24954d01be91af971ca65589';


UPDATE title
SET entry_name='"Hotel du duc de Noirmoutiers / 19 Rue Radziwill Va disparaitre / 1907" (Titre donné par l''auteur)'
WHERE title.id_uuid = 'qr139ff8ef92137458f8e64b5bb9856f00a';


UPDATE title
SET entry_name='Hôtel, 25 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1cf19b201a18f418b96b2409727f7f4a8';


UPDATE title
SET entry_name='Escalier, intérieur de l''hôtel du président Maupeou, 17 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1f374d8dabf3c48afaf1e9abc43920bd5';


UPDATE title
SET entry_name='Hôtel du président Maupeou, 17 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d606a11472f84841b09468802239877f';


UPDATE title
SET entry_name='Hôtel Tezan ?, 21 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr117656bed09f0417ea60ebad0b206ce8a';


UPDATE title
SET entry_name='A l''angle de la rue Baillif et de la rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1127fe3c24dfb4777a5705256803d5d29';


UPDATE title
SET entry_name='Angle de la rue Baillif et de la / Rue Radziwill (démoli en partie par la / Banque de France entre 1920 et 1925) (Titre attribué)'
WHERE title.id_uuid = 'qr14fda5b6ff8314171b20b6641bcf50043';


UPDATE title
SET entry_name='Porte, 13 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ab98b8b1e5c84c8ca60a89cb791825fc';


UPDATE title
SET entry_name='Lucarne à poulie du 21 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d4815c9a2fe64bba83e06f898e297837';


UPDATE title
SET entry_name='Lucarne à poulie du 21 Radz / pour monter le blé dns les greniers / Subsiste en 1943. (Titre attribué)'
WHERE title.id_uuid = 'qr1ca24e2d26786469f9c25eb02d23f757c';


UPDATE title
SET entry_name='Hôtel du Dauphin, 21 et 23 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr131b49a155c2245298c79ff38312c4c9c';


UPDATE title
SET entry_name='21 et 23 rue Radziwill / Hôtel de [...] autrefois / Ancienne maison à l''enseigne du / dauphin / Subsistent en 1943. (Titre attribué)'
WHERE title.id_uuid = 'qr159a4d55d94884fe581256c4c8cd6ac21';


UPDATE title
SET entry_name='Ancien hôtel de Noirmoutier, 19 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr184e8b3b8bb7b4aff8bb33a42db5f9f38';


UPDATE title
SET entry_name='19 rue Radziwill / Ancien hôtel de Noirmoutier / Précédemment hôtel Colbert / (subsiste en 1943) (Titre attribué)'
WHERE title.id_uuid = 'qr14babf1df116840bcaf78dff4399d2f5e';


UPDATE title
SET entry_name='Ancien hôtel Maupeou, 17 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1b79f4c0a01064140a7258e3d97ac70f4';


UPDATE title
SET entry_name='17 rue Radziwill / ancien hôtel Maupeou / démoli en 1920 - 1925 (Titre attribué)'
WHERE title.id_uuid = 'qr1edd5ca29e8ca43f2b046ac680a582481';


UPDATE title
SET entry_name='Toitures du 19 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr10d33c4d5600e4e88bc7c0783d1e02f4b';


UPDATE title
SET entry_name='Toitures 19 rue Radziwill / subsiste en 1943 (Titre attribué)'
WHERE title.id_uuid = 'qr1557da3e983c74a52a0b7d0cf55c90a83';


UPDATE title
SET entry_name='Croisement de la rue Radziwill et de la rue Baillif, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1187a50f18e214940b8e3af8b8ce2d0cd';


UPDATE title
SET entry_name='Rue Radziwill / Vue prise de l''angle de la rue Baillif / ( La chaussée a été abaissée en 1925 devant le / portail de droite) (Titre attribué)'
WHERE title.id_uuid = 'qr16f0d779fe17845f4959bc01d21a22cdc';


UPDATE title
SET entry_name='31 et 33 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1f5ed0e8e191d4b048768249785c1693c';


UPDATE title
SET entry_name='31 et 33 rue Radwilly / Ce dernier est une des entrées du passage / Radziwill dit passage Noir avec son / escalier obscur à double mouvement / ou 2 personnes montent ou descendent / sans jamais se rencontrer. / L''autre entrée de cet immeuble est / au... (Titre attribué)'
WHERE title.id_uuid = 'qr1da69621fb244475fa1598994c02a10ec';


UPDATE title
SET entry_name='Croisement de la rue des Petits-Champs et de la rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1dfbc897c80ba45ffb71684ca5f62532c';


UPDATE title
SET entry_name='Rue Radz / Vue prise de l''angle de la rue des Petits-Champs / (Les 5 derniers immeubles à droite du 9 au 17 ont été démolis / en 1920-25.) (Titre attribué)'
WHERE title.id_uuid = 'qr162a11c9c39dc44adbcb388e13962b24f';


UPDATE title
SET entry_name='Porte, 25 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1e35c5aa3a2164bc6a21398853e30a26b';


UPDATE title
SET entry_name='25 rue Radziwill / subsiste en 1943. (Titre attribué)'
WHERE title.id_uuid = 'qr162b776cbf98649f0802bae0e983c20b6';


UPDATE title
SET entry_name='Motifs décoratifs de dessus d''une fenêtre, 19 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr17a773eb11fb74f61ac4d7bd140c101e4';


UPDATE title
SET entry_name='Angle de la rue Baillif et de la rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr108a1a0aadc2745afba41deebc16a2802';


UPDATE title
SET entry_name='Rue Radziwill / Vue prise de l''angle dela rue Baillif / (Les 3 premiers immeubles 13-15-17 démolis en / 1920 -25) / A droite, la trompe de la Galerie dorée de l''Hôtel / de la Vrillière. (Titre attribué)'
WHERE title.id_uuid = 'qr1964bc4ef370345ebab75a58ebe6beea5';


UPDATE title
SET entry_name='Rue Radziwill / Place des Victoires / Palais-Royal'
WHERE title.id_uuid = 'qr10dd99b135c524b9c9262abeedaf32b9c';


UPDATE title
SET entry_name='Elévation de la rue Radziwill, 1er arrondissement, Paris (Titre factice)'
WHERE title.id_uuid = 'qr1a5312e42c62444cbaf9c94a7c95ac8ad';


UPDATE title
SET entry_name='Ancien passage Radziwill, 35 rue Radziwill, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1b43b2cbfe6c5460290c5f40d1435b865';


UPDATE title
SET entry_name='Hôtel Thézard, 21 rue Radziwill, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1211641682b174332b0a78f8dc9f2c090';


UPDATE title
SET entry_name='Hôtel du duc de Noirmoutier, 19 rue Radziwill, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1f36036f131f04a34a930c3b26ac7826b';


UPDATE title
SET entry_name='Paris - Pancarte rue Ratzwil'
WHERE title.id_uuid = 'qr11983b0b950e34d75a37445bb55955e3e';


UPDATE title
SET entry_name='Rue Radziwill, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr105423ad323a94efa9504414c5c5e73ee';


UPDATE title
SET entry_name='Immeuble du 36 rue de Valois, jouxtant l''arrière de l''immeuble du 21 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1943ebf7438ce4be2826e273213037bd2';


UPDATE title
SET entry_name='Façade du 42 rue de Valois derrière le 29 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1fdb807e7aa9943d5844ed4485053663a';


UPDATE title
SET entry_name='46 rue de Valois, derrière le 31 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1254a1812a8c04b2c80f6640587fad841';


UPDATE title
SET entry_name='Angle de la rue Baillif et de la rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr155de5bc4de354f6db454f11f4f51b361';


UPDATE title
SET entry_name='Angle rue Baillif et rue de Valois en / direction de la rue St Honoré (Titre attribué)'
WHERE title.id_uuid = 'qr17495c6d3a41d4a1388e9e4b0050bec05';


UPDATE title
SET entry_name='Vue de la rue Baillif prise de la rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bc8816410052456da240449163777dd8';


UPDATE title
SET entry_name='Rue Baillif (vue entière) / Vue prise de la R. Croix des Petits Champs / Les immeubles de gauche ont été démolis et la / rue supprimée en 1926. (Titre attribué)'
WHERE title.id_uuid = 'qr151ee8f9ddc904d0cb828c700ae9d8ada';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE/ LIBERTE- EGALITE- FRATERNITE/ COMMISSION/ DES BARRICADES'
WHERE title.id_uuid = 'qr1a76756bc5a3c48ad993916a289c60f4f';


UPDATE title
SET entry_name='Rue de Valois et débouché de la rue Baillif, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1748632f9b7064e93a3ccd2b903fb7c4a';


UPDATE title
SET entry_name='Vue de la rue Baillif prise de la rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr122d597183753469bbd30f6d6b12c203a';


UPDATE title
SET entry_name='Vue d''une partie de la rue Baillif, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1751a374e381f4888abdeb774021c6f56';


UPDATE title
SET entry_name='Vue d''une partie de la rue Baillif prise en 1923. / Au premier plan à gauche, angle de la rue des Bons Enfants / Au deuxième plan à droite rue Radziwill. / Au fond rue de Valois (partie gauche démolie en 1926) (Titre attribué)'
WHERE title.id_uuid = 'qr11445ee3355cb406ba090bc547e82d473';


UPDATE title
SET entry_name='Immeuble, 1 rue du Colonel-Driant, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1eada094d81264a8d8085def424b2a64f';


UPDATE title
SET entry_name='Immeuble 1 rue du Colonel Driant - Vue prise de la rue Croix des / Petits Champs.'
WHERE title.id_uuid = 'qr1dc33b6c980344de3bea252bbb54b14e1';


UPDATE title
SET entry_name='Immeuble en construction, 1 rue du Colonel-Driant, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr11492305538e445b4a0bf97690c1a7623';


UPDATE title
SET entry_name='Immeuble 1 rue du Colonel Driant - En cours de construction. / Vue prise de la rue des Bons Enfants'
WHERE title.id_uuid = 'qr10da55ccb5c884483a643e4c647ed642f';


UPDATE title
SET entry_name='Façade d''immeuble, 1 rue du Colonel-Driant, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr15f3d550a42ad4b0ebbc0970cdf58c9fd';


UPDATE title
SET entry_name='Ensemble - 1 rue du Colonel Driant - 1925 -1927 / Au fond rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1a0d5b5bc7f3448fe8092c7f1acb277b2';


UPDATE title
SET entry_name='Maison à tourelle, 2 rue de la Vrillère, à l''angle de la rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr19b1a78223abc4d91806d1ac33e553316';


UPDATE title
SET entry_name='Maurin marchand tailleur : 19, rue Croix des Petits Champs à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr13eb38770bdd7437e92793cfe34fc9724';


UPDATE title
SET entry_name='Mercerie Demarne "Belle canterie" : 39, rue Croix des Petits Champs à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr1e39325f3ec744521b5b435b3ece75c5e';


UPDATE title
SET entry_name='Immeuble, 25 rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16d91e361b0ec4fa2b1d13086ce27a359';


UPDATE title
SET entry_name='Immeuble, 25 rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr161dca24287d64f1dac0905d170df51d0';


UPDATE title
SET entry_name='Ancien 21 rue Croix des Petits Champs / 1923 (Titre attribué)'
WHERE title.id_uuid = 'qr1788c7493ad474b969d5dbcab22af5ee6';


UPDATE title
SET entry_name='Porte-cochère de l''ancien hôtel de Scépeaux, 23 rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c70261a40ed14c738a52acc4d676d5d2';


UPDATE title
SET entry_name='23 rue Croix des Petits Champs / avant démolition / Précédemment hôtel de Scepeaux / Marquis de Beaupréau / 1793 / 1919'
WHERE title.id_uuid = 'qr14131e617326243ac91d3527aed117ce9';


UPDATE title
SET entry_name='Facture. "A l''Industrie Picarde, / Rue Croix-des-Petits-Champs, n°31. / Cantvelle, / Grand magasin de quincaillerie et de serrurerie de sa fabrique de Picardie (...)".'
WHERE title.id_uuid = 'qr1b7b461b0153140cab6ef85a1a9ebb777';


UPDATE title
SET entry_name='Facture. "A l''Industrie Picarde, / Rue Croix-des-Petits-Champs, n°31. / Gentillet, / Successeur de Mr Cantvelle, / Grand magasin de quincaillerie et serrurerie de Picardie (...)".'
WHERE title.id_uuid = 'qr1f6f234731c3b43eb8ae796a493bcb522';


UPDATE title
SET entry_name='Costumes Parisiens / Chapeau de gaze brochée du Magasin de Mme. Fouchet, Rue Vivienne, N°.2 bis. Robe d''étoffe à raies / brochées garnie de feuilles et de noeuds de ruban par Mme. Allin, Rue Croix des Petits Champs, N°.25.'
WHERE title.id_uuid = 'qr1eebdf1d0a1bd47a59fb6e37ddbb666a6';


UPDATE title
SET entry_name='Figure vue de dos en robe jaune ornée de noeuds sur le bas de la jupe et écharpe bleue ; figure en robe à rayures rose et blanche et de motifs végétaux et ornée de noeuds sur le bas de la jupe et écharpe blanche, Figure 2790, Costume Parisien, 1830,... (Titre factice)'
WHERE title.id_uuid = 'qr179919ce42a2545bea9dcb7709f5166be';


UPDATE title
SET entry_name='Au coin de la rue de la Vrillières et de celle Cr.x des p.is champs'
WHERE title.id_uuid = 'qr15087abe9e0e84e5ea802301fcec8c3c5';


UPDATE title
SET entry_name='Rue Coquillière, vue prise de la rue Jean-Jacques-Rousseau vers la Banque de France, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c7d447897cbe408a9bfa99f226079530';


UPDATE title
SET entry_name='Dépliant publicitaire pour l''inauguration des nouveaux magasins "Au Pauvre Diable", 1 et 3 rue Montesquieu, 11 rue Croix-des-Petits-Champs.'
WHERE title.id_uuid = 'qr1843f69e765b44c378428e3679e77df8a';


UPDATE title
SET entry_name='La rue Croix des Petits Champs et la place des Victoires, 1880'
WHERE title.id_uuid = 'qr10207476edaea41c685504413416a01d9';


UPDATE title
SET entry_name='ELECTIONS LEGISLATIVES DU 22 SEPTEMBRE 1889/ XVIIIe Arrondissement. - 2e Circonscription. - Clignancourt'
WHERE title.id_uuid = 'qr1a3abefe9592148c183b934f8674ba279';


UPDATE title
SET entry_name='LA VOIX/ NI L''UN ! ... NI L''AUTRE ! ...'
WHERE title.id_uuid = 'qr11dc21944a9d2428ba8fa0a26615266ef';


UPDATE title
SET entry_name='23, rue Croix-des-Petits-Champs, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16ce42246321a4c38b5973a1e942ff405';


UPDATE title
SET entry_name='Rue Croix-des-Petits-Champs, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr17d5e1c3c14954858aa00cab05c173254';


UPDATE title
SET entry_name='Titre en partie erronée : la photo de droite montre le 2 rue des Petits Champs'
WHERE title.id_uuid = 'qr118bd7e0902ce4d3bbcf32dfaec3b6ca5';


UPDATE title
SET entry_name='DOUX RÊVE/ AVOIR UNE BICYCLETTE/ KYMRIS/ MAGASIN DE VENTE 2 BD EMILE AUGIER PARIS-PASSY'
WHERE title.id_uuid = 'qr1b621fcb997ad49fdb2f8b912da1731e0';


UPDATE title
SET entry_name='Démolition d''une maison, 21 rue Croix-des-Petits-Champs, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr19d800b12a20c4a768151e9f3f2ff7b22';


UPDATE title
SET entry_name='Rue Croix-des-Petits-Champs, 1er arrondissement, février 1904.'
WHERE title.id_uuid = 'qr11e0e42225f9a434389ee7a45608340cc';


UPDATE title
SET entry_name='23 rue Croix-des-Petits-Champs- Février 1904. (Titre attribué)'
WHERE title.id_uuid = 'qr161094278a5bd451bbeb1a349df2697b2';


UPDATE title
SET entry_name='Graveur Le Saulnier : passage de Radziwille à Paris, XIXe siècle'
WHERE title.id_uuid = 'qr12eb6a99ae86e4bd385fd748443115c1b';


UPDATE title
SET entry_name='La mort de M. Antoine et de Cléopatre, Coriolan fléchi par sa mère, galerie de l''hôtel de Toulouse.'
WHERE title.id_uuid = 'qr13c1cb4e590c9413f8f52adcb859ee72e';


UPDATE title
SET entry_name='Romulus enfant, remis à Laurentia par Faustulus, et Paris enleve Helene, Hôtel de Toulouse.'
WHERE title.id_uuid = 'qr13371e1eed9404c518b3f4aa3ad59f867';


UPDATE title
SET entry_name='Partie de la décoration intérieure de la gallerie de l''Hôtel de Toulouse à Paris.'
WHERE title.id_uuid = 'qr1f42bc2c40af94f26938adbb4ff8e4313';


UPDATE title
SET entry_name='Emplacement de l''hôtel de Toulouse et plan du quartier'
WHERE title.id_uuid = 'qr11d4c7d7d857d4ecba7d3950240fff0c8';


UPDATE title
SET entry_name='Verrière de l''hôtel de Toulouse'
WHERE title.id_uuid = 'qr107b7939deee34b588f3b4868106ec0da';


UPDATE title
SET entry_name='Camille renvoyé les enfants des faleriens, Combat des Sabins contre les Romains, Hôtel de Toulouse.'
WHERE title.id_uuid = 'qr176e6aa3922db4c53986147cb95cb60d3';


UPDATE title
SET entry_name='Auguste ferme le temple de Janus, Hôtel de Toulouse.'
WHERE title.id_uuid = 'qr1d1c41905cad24805a90d55431db4968b';


UPDATE title
SET entry_name='Tableau destiné à la décoration de l''hôtel de Toulouse.'
WHERE title.id_uuid = 'qr19002a2c499204960a46489df3340a01a';


UPDATE title
SET entry_name='Prédiction de la sybille de Cumes, Cesar repudie Pompeia et epouse Calpurnie, Hôtel de Toulouse.'
WHERE title.id_uuid = 'qr11d517a2478e54146a10957e3bed4ca09';


UPDATE title
SET entry_name='Porche d''accès à la cour d''honneur de l''hôtel de Toulouse.'
WHERE title.id_uuid = 'qr143419817d8234707bd5af9faee19bed3';


UPDATE title
SET entry_name='Monument aux morts dans la cour d''honneur de l''hôtel de Toulouse.'
WHERE title.id_uuid = 'qr16c08ea0990d54b17bf12a227b3a38255';


UPDATE title
SET entry_name='[le Vieux Paris] Rue de la Vrillière,1898'
WHERE title.id_uuid = 'qr1a586d5091dfa493e8feeff90710e89ab';


UPDATE title
SET entry_name='En-tête de lettre pour L. Guidou, Avoué de 1ère Instance, Rue de la Vrillière n°2'
WHERE title.id_uuid = 'qr1a2cebc66e52b48e7b1cfd601315ca38a';


UPDATE title
SET entry_name='Lebreton, marchand de bric-à-brac.'
WHERE title.id_uuid = 'qr1f7da010c88b845bc9cdbd5e1e83ce912';


UPDATE title
SET entry_name='Henri V'
WHERE title.id_uuid = 'qr1cd169ab0c2464fb29126203eef82f7c2';


UPDATE title
SET entry_name='Henri (Titre inscrit (lettre)), Henri-Charles-Ferdinand-Marie-Dieudonné d’Artois, duc de Bordeaux, comte de Chambord (1820-1883) (Titre factice)'
WHERE title.id_uuid = 'qr14f852945b89d49b3ac54a977747b4e59';


UPDATE title
SET entry_name='Lettre D - p. 160-161 - du "Fernsprechverzeichnis der Deutschen Dienststellen in Gross-Paris" - répertoire téléphonique des services allemands'
WHERE title.id_uuid = 'qr1c1196665f4b44215a336bf13276eb633';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE./ LIBERTE, EGALITE, FRATERNITE./ Mairie du 2e Arrondissement de Paris/ AVIS/ AUX OUVRIERS/ SANS TRAVAIL.'
WHERE title.id_uuid = 'qr1482a02f0109b4a56ab85659327ec1114';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE./ LIBERTE, EGALITE, FRATERNITE./ MAIRIE DU 2ME ARRONDISSEMENT./ VACCINATIONS/ GRATUITES.'
WHERE title.id_uuid = 'qr13655b43577c540d7972657ef9e292819';


UPDATE title
SET entry_name='REPUBLIQUE FRANCAISE./ LIBERTE, EGALITE, FRATERNITE. MAIRIE DU 2E ARRONDISSEMENT/ ELECTIONS/ de Onze Représentans [sic]/ DU PEUPLE.'
WHERE title.id_uuid = 'qr1460d54d3258b4584ba23ac2a9b8020c8';


UPDATE title
SET entry_name='Immeuble, à l''angle de la rue de la Banque nº1 et nº2 et de la rue des Petits Pères, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr17ed429f0ad4b4677b6aa72e937722b92';


UPDATE title
SET entry_name='Immeuble, 2 rue de la Banque, à l''angle de la rue des Petits-Champs et de la rue de la Feuillade, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr16a65e1ec049a4b6aa3b0fa0772ea8b75';


UPDATE title
SET entry_name='Hôtel du Cardinal Dubois, 10 rue de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1f6480a01023b45b0bf26dd4b93c2415a';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans, Hôtel du Cardinal Dubois. 10, rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14fcf4c0ac72144c4879874e74c8eb814';


UPDATE title
SET entry_name='[Localisation erronée dans le titre 1]'
WHERE title.id_uuid = 'qr1d81bd9cc5ca5486ca64d5828ddcfd0c7';


UPDATE title
SET entry_name='Balcon du restaurant du "Bœuf à la mode", 6 rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1cb2ffe19bba648a7a7581c0578b7a1ad';


UPDATE title
SET entry_name='"Balcon Rue de Valois. Juin 1899" (Titre donné par l''auteur)'
WHERE title.id_uuid = 'qr1d6cb3ca3dd7c48d884c62e679e5502c6';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans / Hôtel du Cardinal Dubois. 10, rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr10603b212ccc3454a99119ddb610d38c7';


UPDATE title
SET entry_name='Hôtel Mélusine construit par Richelieu, 8 rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr14ffa6b054a6848f19816a9509e9e161b';


UPDATE title
SET entry_name='Chancellerie d''Orléans, façade de la rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1015fcbd007204592a4be67c016e520b4';


UPDATE title
SET entry_name='Vue de la rue de Valois et de la rue de Beaujolais prise du passage du Perron, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1dfc809bed0404a919a146b7d0829ea37';


UPDATE title
SET entry_name='Au fond rue de Valois / Rue de Beaujolais / Prise du Passage du Perron / Existe en 1942 (Titre attribué)'
WHERE title.id_uuid = 'qr1895f797b93ae4010b4407b66022fe15d';


UPDATE title
SET entry_name='Angle des rue de Valois et de Beaujolais, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1927c82e4c0514a61b9c77c103bb860d1';


UPDATE title
SET entry_name='Maison à 9 étages, 48 rue de Valois, vue prise de la rue Beaujolais, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d20cc07c02b74b67b11505d36ded9e44';


UPDATE title
SET entry_name='Angle de la rue de Valois et rue Beaujolais, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1e0b9a5adf19f47eaaf64a789b8576956';


UPDATE title
SET entry_name='Immeubles 14 et 16 rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1cc2365208b0b45b8b85f5e179032a07f';


UPDATE title
SET entry_name='Immeubles 14 et 16 rue de Valois / aujourd''hui démolis. A droite et / au fond débouché de la rue Baillif / vue prise en 1921. (Titre attribué)'
WHERE title.id_uuid = 'qr1e59aee03587447ed81fd9fbceb15db4c';


UPDATE title
SET entry_name='À la fourchette ; enseigne d''un aubergiste de l''ancien 10, rue de Valois (Hôtel de la Chancellerie d''Orléans)'
WHERE title.id_uuid = 'qr17ee37ce19e5142b79f21eb25e39d33c4';


UPDATE title
SET entry_name='Panneau décoratif à grotesques avec un panier contenant des fruits confits pour la pâtisserie Flammang, autrefois, 6 rue de Valois, 1er arrondissement.'
WHERE title.id_uuid = 'qr1046dc3dec07a4d788547ea44a4cea84d';


UPDATE title
SET entry_name='Panneau décoratif à grotesques avec une coupe contenant des fruits confits pour la pâtisserie Flammang, autrefois, 6 rue de Valois, 1er arrondissement.'
WHERE title.id_uuid = 'qr14b11d05f5505463588d335070cf4129d';


UPDATE title
SET entry_name='Panneau décoratif à grotesques avec pièce montée pour la pâtisserie Flammang, autrefois, 6 rue de Valois.'
WHERE title.id_uuid = 'qr156312920afac4286ad54e71120d923bd';


UPDATE title
SET entry_name='Panneau décoratif à grotesques avec pièce montée pour la pâtisserie Flammang, autrefois, 6 rue de Valois, 1er arrondissement..'
WHERE title.id_uuid = 'qr1797b1d4f978f455899dd6dffd58bc558';


UPDATE title
SET entry_name='Passages R. D. : passage Radziville. Rue de Valois n°48.'
WHERE title.id_uuid = 'qr1f9ee3eb9edd4483597de994eb2b97aec';


UPDATE title
SET entry_name='Hôtel du Cardinal Dubois, 10 rue de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1206d1a29e5a54d70b49eb22e3b165868';


UPDATE title
SET entry_name='Immeuble du 36 rue de Valois, jouxtant l''arrière de l''immeuble du 21 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1a08943abc2aa44598785a6a4f5da8294';


UPDATE title
SET entry_name='Façade du 42 rue de Valois derrière le 29 rue Radziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bd490712023f4f268d3c6ba675dc55dd';


UPDATE title
SET entry_name='Immeuble 12 rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ef8a74f023e64b609563e5f841881cf3';


UPDATE title
SET entry_name='Le Boeuf à la Mode - Fondé en 1792 -Menu'
WHERE title.id_uuid = 'qr144f56780f536409bb6233a55e3ac29f6';


UPDATE title
SET entry_name='Flaque d''eau, rue de Valois, Paris.'
WHERE title.id_uuid = 'qr159a9fe39f5054570a6e4a82cf7593550';


UPDATE title
SET entry_name='Le Goût du Jour. Restaurant du Bœuf à la Mode.'
WHERE title.id_uuid = 'qr17c4625e3a74a4afbba071ff3e6392024';


UPDATE title
SET entry_name='Publicité portant le menu du "Restaurant du Bœuf à la Mode".'
WHERE title.id_uuid = 'qr13b19a535c298407394b12991232f0432';


UPDATE title
SET entry_name='Boeuf à la mode'
WHERE title.id_uuid = 'qr10b04828aa9d14ea6bc619034b56ecf9f';


UPDATE title
SET entry_name='Boeuf à la mode'
WHERE title.id_uuid = 'qr1c223b4bad0f24831be2ab119bfa950a5';


UPDATE title
SET entry_name='Le bœuf à la mode/ou le costume/et les caricatures du jour.'
WHERE title.id_uuid = 'qr1e2c66dc5e0134804a8f6bb958c389bff';


UPDATE title
SET entry_name='Le Boeuf à la mode'
WHERE title.id_uuid = 'qr1079cc8d1867441c59af851677c87a837';


UPDATE title
SET entry_name='Hôtel du président Maupeou, 17 rue Radziwill, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16222e34d983f41e7b12992833175f9d0';


UPDATE title
SET entry_name='Escalier, hôtel du président Maupeou, 17 rue Ratziwill'
WHERE title.id_uuid = 'qr1c7ac1a6109964cdfb79c92f976f1b13d';


UPDATE title
SET entry_name='Escalier, hôtel du président Maupéou, 17 rue Ratziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d266fd2095e540f3a2ce5fbb6b5ab647';


UPDATE title
SET entry_name='Escalier, hôtel du président Maupéou, 17 rue Ratziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1f944607d606c4e45a3524ff6136f3ef6';


UPDATE title
SET entry_name='Escalier, hôtel du président Maupéou, 17 rue Ratziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1e6d9ce4329ab4c9d9e37ae1d31917231';


UPDATE title
SET entry_name='Escalier, hôtel du président Maupéou, 17 rue Ratziwill, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16fb6ff3810494f24ae1356eecc88a9a3';


UPDATE title
SET entry_name='Fronton sculpté, Hôtel du Cardinal Dubois, 10 rue de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr15b78c8af723b47afba3c3b58e0315956';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans / Hôtel du Cardinal Dubois. 10, rue de Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr18e227532885949b0ac21430cbcfd204c';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans / hôtel de la Roche Guyon / hôtel du cardinal Dubois, 10 rue Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1365dca9763824e2f86c1edf9ceb087d0';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans / hôtel de la Roche Guyon / hôtel du cardinal Dubois, 10 rue Valois, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bd3fb74bd8144497b7702852246bd6be';


UPDATE title
SET entry_name='Rue de Valois / N°10 (1913)'
WHERE title.id_uuid = 'qr1db93617a905c4301a9a57bd6ecf874b3';


UPDATE title
SET entry_name='Rue de Valois / N°10 (1913)'
WHERE title.id_uuid = 'qr1c2306e7158114b91845cf045790c11c3';


UPDATE title
SET entry_name='Fronton sculpté, Hôtel du Cardinal Dubois, 10 rue de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1fe1b5ad9ab8848de8357e7a6cd5580d9';


UPDATE title
SET entry_name='Cour, hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d2d4d6327fe841c98c5b908af44dd54c';


UPDATE title
SET entry_name='Façade, hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d47a768c26a44e7990e3188286fedacf';


UPDATE title
SET entry_name='Plan de l''ancien hôtel d''Argenson dit hôtel de la chancellerie d''Orléans, rue des Bons-Enfants'
WHERE title.id_uuid = 'qr15ff971833bff4790adda2d326f8bfab2';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: “passage de la porte cochère” 19, rue des Bons-Enfants et 10 rue de Valois, passage de Valois ? Sculpture, niche, allégorie féminine, chasse-roue. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr17163f98cf78a418694cba122408bb79f';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants (côté cour). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16c3ee7933ffa43c0a8d2823716115eb9';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : antichambre côté. Grand Salon. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ae9f794c982a49bba53e9ffd885f5349';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants, niche de droite (nord). Statue en pied, niche, allégorie féminine. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1d5957bb417cb4e5db733e258a876f8ce';


UPDATE title
SET entry_name='Marchand de vin "A la petite biche"'
WHERE title.id_uuid = 'qr1fac0cc5c699645cd91c1da1faa682fd7';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : antichambre côté. Cour. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1556afcf16b574560885a13cd9c2cf611';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants, niche de gauche (sud). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr11016bc3753d34364b989499cc169e861';


UPDATE title
SET entry_name='Chancellerie d''Orléans, Hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois, 1er arrondissement, Paris. “cabinet, chambre à coucher , groupe d''enfants d''Auguste Pajou”.'
WHERE title.id_uuid = 'qr1598718e1354d43568dd72c8a3e37dc43';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois, "cabinet, chambre à coucher, groupe d''enfants d''Auguste Pajou”, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1b84a5545e11544b48bd5c0b1493e6201';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants, bas relief de droite (nord). Putti. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1128b251df29f4843aa0eba5f2af723d7';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants, premier bas relief de gauche (sud). Putti. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr116089faf20fd4fe89f6d484992ca869e';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois, entrée rue des Bons-Enfants, deuxième bas relief de gauche (sud). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr12f95c4f1e2134d74873de827b3fd6f31';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois, “Grand Salon”. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr19bf7a1bea94548f6a6834e0e7924d77e';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois. “cabinet, chambre à coucher , plafond de Louis Duransseau”, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1637b0bb504a645f69464ba601bcb8c27';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : entrée rue des Bons-Enfants (côté rue). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16b064c4ceb87423c974f8fae5ce3fa1a';


UPDATE title
SET entry_name='Chancellerie d''Orléans : petit Salon nord, côté Valois. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr160e8d0116f42454f95b7a2dfdd67b69c';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon, côté Valois. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr11ff29ad6e22e4bd9b16ba2035651b03f';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : antichambre côté mitoyen, escalier. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1776f1732cb0b4f7dbbc7e0e74a59c67c';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19 rue des Bons-Enfants et 10-11 rue de Valois, 1er arrondissement, Paris. Grand salon côté sud, décors peints, sculptures, bas-reliefs.'
WHERE title.id_uuid = 'qr115e2cc5a00d34d97a6ff0e82840c47a4';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : vestibule côté Grand Salon. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1325f486dd5fe49dab3902c803a949f78';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois: antichambre côté Grand Salon. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e8c60ff3826042219cb88ab4f093cb69';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit salon nord (côté antichambre). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr113af93d9faf24246b7e8d7537c468325';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois. Grand salon côté nord, décors peints, sculptures, bas-reliefs, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr141f6578521f74a79b36037613304fb54';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois. Entrée rue des Bons-Enfants, caissons, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr16c18de3da50d4468af50c391896cc618';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois: vestibule côté cour . 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a1340df30c954bb7ab8cc08cfa41f2c9';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : vestibule côté antichambre. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1ed261a93c55045f5b455832c90f6667d';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : grand Salon, côté Valois. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a7884db89447497f8e3c3c39df399b40';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : grand Salon, côté Vestibule. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr17636958748f3451882ab4c8c9e6e5de2';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon sud côté vestibule1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr168a51111854a4c56855616994c38dcb6';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon, sud, côté mitoyen (sud). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr102736bb3a64c4f08921596c8d3c9a068';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois. “Grand Salon, plafond d''Antoine Coypel, 1708”. Décoration intérieur, architecture, bas-reliefs d’Auguste Pajou, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c9968483f69a4e60a91586035a3608cd';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon, nord, côté mitoyen (nord). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1d2f78db026bc44c2b0d1a74ba247fc85';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon, nord, côté grand Salon. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr12ab53f722f034128883a619831dfda22';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et 10 rue de Valois. “Grand Salon Plafond d’Antoine Coypel - bas relief en stuc doré d’Aug. Pajou” (Auguste Pajou), 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1f6bf6ab6d2604f8ab543256c9f7a66a5';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel Roche-Guyon, 19, rue des Bons-Enfants et rue de Valois : petit Salon, sud côté grand Salon. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a3d6067c8cde481493022dc50ea0558b';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: “passage de la porte cochère”, 19, rue des Bons-Enfants et 10 rue de Valois, passage de Valois ? Sculpture, niche, allégorie féminine, chasse-roue. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1120cfcc0ff2741e18eecedd72260d97e';


UPDATE title
SET entry_name='Chancellerie d''Orléans: vestibule (côté sud). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1394839466653485e8c47cb35e5f5fd12';


UPDATE title
SET entry_name='Papier à en-tête illustrée de Ripolin, place de Valois.'
WHERE title.id_uuid = 'qr19c7c98fd425b4101bf8828eb949e4d50';


UPDATE title
SET entry_name='Rue des Bons-Enfants, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1061f5e3d7103431b9e109efd306d1afb';


UPDATE title
SET entry_name='“HOTEL DE LA CHANCELLERIE D’ORLEANS - GRAND SALON” “Pl.73”. Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et 10 rue de Valois. Intérieur, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1a1fe8522da72452e956becb7e8b17da1';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon, 19, rue des Bons-Enfants et 10 rue de Valois: 1er arrondissement, Paris. “Grand Salon avec le bas-relief d''Auguste Pajou et camaïeu de Ch. Morinet (?)“.'
WHERE title.id_uuid = 'qr18a12d7e60943496bb81e944faf35526f';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois. Grand Salon avec le bas-relief d''Auguste Pajou et camaïeu de Ch. Morinet (?), 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr104901b8d201445fd99b94c44bfbe805c';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois : “Grand Salon bas-relief en stuc doré d''Auguste Pajou”. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1b5aab05467cd48f7ad4330ccafd2ce40';


UPDATE title
SET entry_name='Chancellerie d''Orléans ou hôtel de la Roche-Guyon: 19, rue des Bons-Enfants et 10 rue de Valois. “Grand Salon avec le bas-relief d''Auguste Pajou et camaïeu de Ch. Morinet (?)”. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr120c07c6e5572400ea84f64aaa7027ae6';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, porte surmontée d''un décor sculpté de putti ailés portant une couronne de fleurs,1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c1c1a1d1475945249d5ff5621011e6b5';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, porte surmontée d''un décor peint de putti ailés,1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1d8bce47481fd4cdea2c8a321fb24d43e';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, porte surmontée d''un décor peint de putti ailés portant des couronnes de fleurs,1er arrondissement, Paris'
WHERE title.id_uuid = 'qr13a51c0d1a5554983b2183f4313e49536';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, décor sculpté représentant Neptune et un satyre,1er arrondissement, Paris'
WHERE title.id_uuid = 'qr186d05ec2dd3940589b98da4610a0be07';


UPDATE title
SET entry_name='Vue de la salle de marbre de la Chancellerie d''Orléans, 2ème arrondissement, Paris.'
WHERE title.id_uuid = 'qr102bfea838e1c45edb6a9e53dcdf6f1dd';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16a1be4691b9c4df79a0602817940786c';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr120277f80f94a4599844586bc37f882de';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr16a26fd6cbe2442d0b85cdf57903ca327';


UPDATE title
SET entry_name='Hôtel de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr11dae30c012724fdf9521957174b06eb2';


UPDATE title
SET entry_name='UNION CENTRALE/ DES/ ARTS DECORATIFS/ COMITE DES DAMES/ Les/ Arts de la Femme/ EXPOSITION/ Rétrospective et Moderne/ 25 MAI-25 JUIN/ 1898/ 19, rue des Bons-Enfants hôtel de la Chancellerie d''Orléans.'
WHERE title.id_uuid = 'qr168a61588022b48c0bbb4bcd3641de669';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, décor sculpté représentant l''enlèvement de Perséphone par Hadès, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr12a97d9d2c4b24a82ae1f49c98bc46023';


UPDATE title
SET entry_name='Chancellerie d''Orléans / Scène de la mythologie / 1907 (Titre attribué)'
WHERE title.id_uuid = 'qr114229f3d7b8c47c9b1774ea78bd787b2';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, décor sculpté représentant une femme en toge portant une clef assise dans un char tiré par deux lions et suivi par trois personnages. 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1e3eae21a3b09449fa0013c7941a89351';


UPDATE title
SET entry_name='Chancellerie d''Orléans / Scène de la mythologie / 1907 (Titre attribué)'
WHERE title.id_uuid = 'qr1bcad1d4e67a44b2e97471ba578a24579';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, décor sculpté représentant un enlèvement (enlèvement de Psyché?). 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1f6991371497943e4bca120864b493002';


UPDATE title
SET entry_name='Chancellerie d''Orléans / Scène de la mythologie / 1907 (Titre attribué)'
WHERE title.id_uuid = 'qr163251ad5a025499786fa11f65d591aed';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, double porte ornée d''un décor sculpté représentant l''enlèvement de Perséphone par Hadès. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c206770d42c64252a724bed474651db6';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans,? double porte ornée d''un décor sculpté du Putti avec une couronne de fleurs. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ade805e0ae49489b98e2c2d897952d4e';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, miroir, cheminée et lustre. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1bb9ae512d9af4965bb0269209dacf134';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, Grand Salon. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1c1dc210017824c5e83f1036fe536b538';


UPDATE title
SET entry_name='Chancellerie d''Orléans / Grand Salon / rue de Valois / 1907 (Titre attribué)'
WHERE title.id_uuid = 'qr152c628afb76449ed8debf803dae41fc0';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, miroir reflétant la double au décor d''enlèvement de Perséphone. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1a484b0d8cfdb446e9e45a14cae70ca95';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, plafond peint. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr11801ef4f6fb44f3b99d970a863ab3e9f';


UPDATE title
SET entry_name='Vue intérieure de la Chancellerie d''Orléans, salon du Rez-de-Chaussée. 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1966d57acfad14191aa8d8038e004aac5';


UPDATE title
SET entry_name='Démontage de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr18d65cb909ae44e478ecf193f7fec0601';


UPDATE title
SET entry_name='Démontage de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1be667ca70aec4c849f8988668bcede98';


UPDATE title
SET entry_name='Démontage de la Chancellerie d''Orléans, 19 rue des Bons-Enfants, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr175fbd39468494854bfc389abff2e3233';


UPDATE title
SET entry_name='La dynamite à Paris / Explosion au commissariat de Police de la rue des Bons enfants'
WHERE title.id_uuid = 'qr1e9764494a43f4c15b5b55894d41eeb4a';


UPDATE title
SET entry_name='Passage Vérité, 11 rue des Bons-Enfants, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1da56a9fd5dff485f8dddc5eb1f966b89';


UPDATE title
SET entry_name='Entrée du passage Vérité, place de Valois, 1er arrondissement, Paris.'
WHERE title.id_uuid = 'qr1c6240e2120df4332a831d8ed9753feb4';


UPDATE title
SET entry_name='La passage Vérité et la place de Valois'
WHERE title.id_uuid = 'qr15489eeab47e34237a0ebb01607772f92';


UPDATE title
SET entry_name='Club des travailleurs et commerçants, mars 1848'
WHERE title.id_uuid = 'qr1aabe59f5e2ff4a3aa78a3afdc5d617fc';


UPDATE title
SET entry_name='Club des amis du peuple dirigé par Raspail, avril 1848.'
WHERE title.id_uuid = 'qr1db92971e249840b59ca1a8f34f63ce63';


UPDATE title
SET entry_name='Vue extérieure des Bains Montesquieu. / N°34.'
WHERE title.id_uuid = 'qr11296b98e13914fe6bc04ed52238b1b7f';


UPDATE title
SET entry_name='Passage de la Vérité et début de la rue Montesquieu, rue Bons-Enfants, 1er arrondissement, Paris'
WHERE title.id_uuid = 'qr1ac283475b26f40fdaca79ce285227053';


UPDATE title
SET entry_name='La rue des Bons Enfants en 1919 / A gauche passage de la Vérité / à droite début de la rue Montesquieu (Titre attribué)'
WHERE title.id_uuid = 'qr143d3edb9c1b3435e8cda66e1df9d8464';


UPDATE title
SET entry_name='Agrandissement des Magasins du Coin de Rue'
WHERE title.id_uuid = 'qr197b352f61338410f8167c1d98b8cc465';


UPDATE title
SET entry_name='Le Charivari, trente-septième année, dimanche 12 avril 1868'
WHERE title.id_uuid = 'qr1dcffb8e5bc0a4aabae941c33d3280aa3';


UPDATE title
SET entry_name='Portrait de Jean-Antoine Alavoine (1776-1834), architecte'
WHERE title.id_uuid = 'qr1656263bb2da1450a8514111c00fa541c';


UPDATE title
SET entry_name='Souvenir du siège de Paris, 1870-1871'
WHERE title.id_uuid = 'qr14ed32d8226c147dfaf586cfbd8684f7a';


UPDATE title
SET entry_name='La Tzigane / Dis-moi tu, Dis-moi toi. / Valse / par / Johann Strauss [couverture]'
WHERE title.id_uuid = 'qr1e1374ed3ab2e40beb4cb008d39090b7c';


UPDATE title
SET entry_name='Les pièces de théâtre jouées avant 1920. Ti à Tz (Titre de la série)'
WHERE title.id_uuid = 'qr19af0aea3f126437eb9c17957bc731a5b';


UPDATE title
SET entry_name='Le nid d''amours'
WHERE title.id_uuid = 'qr19a39aa8759674a9ba203bb14ccbb353f';


UPDATE title
SET entry_name='Voyage Sentimal, Sterne'
WHERE title.id_uuid = 'qr1cc1b6bcb1ee5426dbff8bbfd11c99395';


UPDATE title
SET entry_name='Banque de France'
WHERE title.id_uuid = 'qr1bfe0f97d126648a88e52c083b197f6ee';


UPDATE title
SET entry_name='Print ; playing-card'
WHERE title.id_uuid = 'qr1f1b1c257a3174d4491adb13bc319786e';


UPDATE title
SET entry_name='Print ; playing-card'
WHERE title.id_uuid = 'qr14953ffe8e8af4b3babc663d7d6e790a3';


UPDATE title
SET entry_name='snuff-box; box'
WHERE title.id_uuid = 'qr1538edb4b652844c28d00ee31ea593508';


UPDATE title
SET entry_name='Escalier du passage Radziwill : [dessin] / L. Ottin [Léon-Auguste Ottin]'
WHERE title.id_uuid = 'qr1eace9de9524c465585d6b101b201c5bd';


UPDATE title
SET entry_name='[Hôtel Thézan] : [21 Rue Radziwill, Juin 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1a481a08a9fb849f0a645e6f54ecc3596';


UPDATE title
SET entry_name='[Hôtel du président Maupéou (1728)] : [rue Radziwill 17, Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr170401ed3a4af47749a4039a667c50b76';


UPDATE title
SET entry_name='[Hôtel du Duc de Noirmoutiers] : [19 Rue Radziwill, juin 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr171ced59ee21b4c1da571750f79b50e8b';


UPDATE title
SET entry_name='[La Rue Radziwill, Juin 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1de12dee4f806453990d8148588ee0463';


UPDATE title
SET entry_name='Coin de la rue Radziwill et des Petits-Champs au 35 la maison à 9 étages de la rue de Valois : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1b15cd8c7185d4a428f983a010611a996';


UPDATE title
SET entry_name='Hôtel du président Maupéou [Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1e2cd30524a2b445da8de6eb5f977de62';


UPDATE title
SET entry_name='[Hôtel du Président Maupéou (1728)] : [Rue Radziwill, mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1d2479092ad254bbcae9bc321687e4bb3';


UPDATE title
SET entry_name='[Hôtel du Président Maupéou (1728)] : [Rue Radziwill 17, Mai 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr113aaf1b58a6f43c0818b7f0e3d4b55d1';


UPDATE title
SET entry_name='Plaque tournante rue Radziwill : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr19f46c45af3e64e1999c77b62124616df';


UPDATE title
SET entry_name='Première vue du cortège de Sa Majesté Napoléon I.er, empereur des Français, passant devant le Palais du Tribunat pour se rendre à Notre-Dame et y être sacré par le pape Pie VII : [estampe]'
WHERE title.id_uuid = 'qr1c34373b7d96f41b88b86cf153fe93ccd';


UPDATE title
SET entry_name='[22/4/26, inauguration de la rue du colonel Driant, discours de] M. Simon, président des anciens chasseurs de Driant : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1d3c4cf74861b47048d31d700027e5517';


UPDATE title
SET entry_name='22/4/26, inauguration de la rue du colonel Driant, discours de M. Bouju : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr10766ee35a75f42a4b1163b06f87565f5';


UPDATE title
SET entry_name='Louis XVIII. : [estampe]'
WHERE title.id_uuid = 'qr191075cd565ce405898a90bf5cef75001';


UPDATE title
SET entry_name='Album de chant 1855 de Charles d''Amboise...12 romances, 12 dessins sur chine... Chez Chaillot, Editeur de musique 354 St Honoré, près la place Vendôme... : [affiche] / [4 ill. par] Aumont, Leroux'
WHERE title.id_uuid = 'qr1bed7d84ac350417e8aaeebf24eaae303';


UPDATE title
SET entry_name='Paraitra prochainement, le Duc d''Orléans : [affiche] / Ch. Tichon'
WHERE title.id_uuid = 'qr1239deb8ebb4c4a16ba0a7f8094fdde15';


UPDATE title
SET entry_name='Au Pauvre Diable. Nouvautés [...] Paris [...] 11 rue, croix-des-petits-champs ; 1 & 3 rue Montesquieu : [affiche]'
WHERE title.id_uuid = 'qr1858d0731778344e8bf7a12a79047bb39';


UPDATE title
SET entry_name='Centaur Cycles. Paris, 9, avenue de la Grande Armée. E.F. Satchell. Directeur pour la France : [affiche] / Ch[arles] Tichon'
WHERE title.id_uuid = 'qr1628eac8b1cee4f3b9cbdf013358ef375';


UPDATE title
SET entry_name='[Maison à Tourelles 2 Rue La Vrillière appartenant en 1750 à l''architecte Leduc] : [et rue Croix des Petits Champs, Juin 1906] : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr11ce2a78840764e3dbf5745e75d1cc610';


UPDATE title
SET entry_name='Banque de France [rue Croix-des-Petits-Champs] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr16e00ed30b3a240fc9ae0387883f31d4e';


UPDATE title
SET entry_name='Banque de France [porte cochère, rue Croix-des-Petits-Champs] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr15770c87318384d7b840fcb8c0c83dc37';


UPDATE title
SET entry_name='[titre erroné, nous sommes sur l''entrée rue la Vrillière]'
WHERE title.id_uuid = 'qr1f0771d9d167245279c2c792748f5e4b1';


UPDATE title
SET entry_name='Banque de France [entrée rue Croix-des-Petits-Champs vue depuis le rue Coquillière] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr17e89e7ab7c594aa292548bac551a8736';


UPDATE title
SET entry_name='Banque de France [rue Croix-des-Petits-Champs] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1a044deb317534f2780ba568d5342af09';


UPDATE title
SET entry_name='Banque de France [rue Croix-des-Petits-Champs] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr118c93f3b7da14790b8374438e482d5b6';


UPDATE title
SET entry_name='31-7-14, crise monétaire [la foule devant la Banque de France à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1a4195962cdb3414ab1443578ff7d5a72';


UPDATE title
SET entry_name='31-7-14, crise monétaire [la foule devant la Banque de France à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr16878bc3baeea41c49d2ce22f9ac2c1e7';


UPDATE title
SET entry_name='27/9/26, un groupe de personnes attendant l''ouverture des guichets de la Banque de France pour vendre leur or : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr19733c56f006e4d9092de563d1c4e098d';


UPDATE title
SET entry_name='Le salon d''or à la Banque de France : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1c5465243db804610a758eaddf50ca61d';


UPDATE title
SET entry_name='Citoyens Un homme s''est présenté devant vous pour solliciter vos suffrages et vous a dit : [etc.] : [estampe]'
WHERE title.id_uuid = 'qr1b780bc975ebd49ef89bd1979523d05df';


UPDATE title
SET entry_name='S. A. Royale Madame, - Duchesse D''Angoulême, Née le 19 Décembre 1778 : [estampe]'
WHERE title.id_uuid = 'qr19eca0dc2f1ef4d2f9a3671de301029da';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : deux projets de trophées pour la porte d''entrée de la Galerie dorée] : [dessin] / [Antoine François Vassé]'
WHERE title.id_uuid = 'qr17aef888648be486cbe7a8d25adc8c0b0';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] Seconde maison où logeoit un evesque'
WHERE title.id_uuid = 'qr1e1d2778dc549482792e1d9b61993b400';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale du nouveau corps de logis] : [dessin]'
WHERE title.id_uuid = 'qr148df8e88b30b4a618e7e6a3ba18b5c40';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : mise au net aquarellée du dessin n° 1587] : [dessin]'
WHERE title.id_uuid = 'qr1fa3850a89cb149da86f0bf8fefbae08b';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale de la nouvelle aile gauche sans mansardes] : [dessin]'
WHERE title.id_uuid = 'qr10f97a91e42c743bc90a29a9b95dffe8f';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale du bâtiment de la Galerie] : [dessin]'
WHERE title.id_uuid = 'qr1b59c50c7c7754d35ae2ddec99e012c12';


UPDATE title
SET entry_name='14 rue de Valois'
WHERE title.id_uuid = 'qr1d38d646b0ebc425bb5301cef3fd05001';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale du nouveau bâtiment des remises adossé aux écuries à l''Ouest] : [dessin]'
WHERE title.id_uuid = 'qr189663fe4f89b4a7d9806ebbd6251d889';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] Dessein d''une partie du plafon de la gallerie dudit hôtel : [dessin] / [Pierre-Josse Joseph Perrot]'
WHERE title.id_uuid = 'qr1359a3ae360ed44048c0f235ce896c853';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : profil de l''escalier] : [dessin]'
WHERE title.id_uuid = 'qr193e39c9067944681ae97d46632032d74';


UPDATE title
SET entry_name='[Hôtel de Toulouse : 2 coupes de bâtiments avec cotes] : [dessin]'
WHERE title.id_uuid = 'qr1964693eda655499bad7b61dadf4fa84a';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale de l''ancien corps de logis et de l''aile droite] : [dessin]'
WHERE title.id_uuid = 'qr19f4b8aca32954965b16e76076f4d5960';


UPDATE title
SET entry_name='[Hôtel de Toulouse : plan du second étage avec modifications]'
WHERE title.id_uuid = 'qr1fe82084abd574570bde2df76955a1a7f';


UPDATE title
SET entry_name='[Paris : Hôtel de Toulouse, plan du 1er étage : [dessin]'
WHERE title.id_uuid = 'qr115c65fcd45e3490abd4e15e2226ee111';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : projet pour les galetas au dessus des communs de droite] : [dessin]'
WHERE title.id_uuid = 'qr18771b593cc814b8b989593b59ed06f38';


UPDATE title
SET entry_name='[Paris : Hôtel de Toulouse, plan du rez-de-chaussée avant transformation] : [dessin]'
WHERE title.id_uuid = 'qr12803de6cab52446bac1d578eddb1fb81';


UPDATE title
SET entry_name='[Plan de l''hôtel de Toulouse : seconde proposition pour le rez-de-chaussée] : [dessin]'
WHERE title.id_uuid = 'qr17de18a731a2a44a4b7dca40fff94b469';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale de l''aile de gauche et de la cage d''escalier, selon un axe Ouest-Est] : [dessin]'
WHERE title.id_uuid = 'qr1b07e2b66a1bd45b3b70a3870d44f3ba1';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : plan du nouveau premier étage] : [dessin]'
WHERE title.id_uuid = 'qr1791640d05b3544c4bc3bfe9d2f0cf98b';


UPDATE title
SET entry_name='[Hôtel de Toulouse : plan du premier étage avec premières modifications] : [dessin]'
WHERE title.id_uuid = 'qr1dd2f49ce960540d7a3f8e879845903c2';


UPDATE title
SET entry_name='[Plan de l''hôtel de Toulouse : premières modifications pour le rez-de-chaussée] : [dessin]'
WHERE title.id_uuid = 'qr154d95a1b7e414547b0553c73728406ac';


UPDATE title
SET entry_name='Plan de l''hôtel de Toulouse : [dessin]'
WHERE title.id_uuid = 'qr1929d4bd042304c6596449f9d4516ab57';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] Aisle à gauche en entran au desu des cuisines : [dessin]'
WHERE title.id_uuid = 'qr12a0befe3817041d88d8fa1066d7d56f1';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : relevé du plan définitif du second étage] : [dessin]'
WHERE title.id_uuid = 'qr122db5b73e52e41ea9f29a2f583906d08';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] Profil du bâtiment où sont les remises : [dessin]'
WHERE title.id_uuid = 'qr12fcaf8290222429db069344cacbf4bb8';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : deux coupes transversales des deux ailes en équerre formant la cour des offices] : [dessin]'
WHERE title.id_uuid = 'qr131041ebfba064e668df96c8d12b2058f';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse : coupe transversale de la nouvelle aile droite] : [dessin]'
WHERE title.id_uuid = 'qr1b3ce2b39805a4b5eb1ed25714c6df391';


UPDATE title
SET entry_name='Dernier plan de l''hostel de Toulouse : [dessin]'
WHERE title.id_uuid = 'qr1ee4d004b59fb45f4b2554ebf32fd1fb8';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] De plain pied à l''hotelle; aile de la 3e. maison faisant l''encoignure; corps de logis de la 3e. maison faisant l''encoignure : [dessin]'
WHERE title.id_uuid = 'qr1d00e6c4602a7490d8b8a4550f903542f';


UPDATE title
SET entry_name='Premier maison joignant l''aisle de l''hautel de La Vrillière : [dessin]'
WHERE title.id_uuid = 'qr14c3aa0c0c45340e0844987b4cd07f6c4';


UPDATE title
SET entry_name='[Hôtel de Toulouse : relevé du plan définitif du premier étage] : [dessin]'
WHERE title.id_uuid = 'qr12e0a3fba4745486c8afca6a5eb99009b';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] L''hotelle au bou de l''horangery dans la rue neuve-des-Bon-Enfan : [dessin]'
WHERE title.id_uuid = 'qr1c755ebdbb40a4c4b84bbf70e61ea21ad';


UPDATE title
SET entry_name='[Paris, hôtel de Toulouse] Premier à côté de l''hotelle de La Vrillière; segonde maison où loge un evesque : [dessin]'
WHERE title.id_uuid = 'qr198e6b62fff0b437cb59eb94fa0765bb8';


UPDATE title
SET entry_name='Banque de France [entrée rue La Vrillière] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1fcde7cef904a4760baf191378bcfe5a4';


UPDATE title
SET entry_name='Banque de France [entrée rue La Vrillière vue depuis la place des Victoires] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1c172104910dd4e26a976ac05ee2218fc';


UPDATE title
SET entry_name='5/7/32, tirage de la dette [dans le grand salon doré de la Banque de France] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1c745ffe1b38840708152bfdd20016a77';


UPDATE title
SET entry_name='[Restaurant du Boeuf à la mode] : [dessin]'
WHERE title.id_uuid = 'qr1860ba68f17f44ee2a95573f9d0de798c';


UPDATE title
SET entry_name='[Omnibus tiré par des chevaux, près du 9 rue de Valois, lieu de travail du photographe Zulimo Chiesi] : [photographie] / [Zulimo Chiesi]'
WHERE title.id_uuid = 'qr17c9fc9fd29214172ade81cfbdc3e1d1a';


UPDATE title
SET entry_name='Abattoir aux chevaux de la Ville de Paris. Usine à Aubervilliers (Seine), J. Dulac directeur, bureaux, 65, rue d''Hauteville, Paris [...]. Engrais-Krafft [...], poudre d''os, poussiers de laine, etc [...] : [affiche] / [non identifié'
WHERE title.id_uuid = 'qr17914dbb0d05d4846ba4cca2aa80e7fd1';


UPDATE title
SET entry_name='1r arrt. Palais royal - Dynamite! Attentat du 7 9bre à midi rue des bons enfants 21 au commissariat de police : [dessin] / [JA. Chauvet]'
WHERE title.id_uuid = 'qr1f21bb5983d754b95b2555ef93597f268';


UPDATE title
SET entry_name='1r arrt. Palais Royal - Explosion de dynamite au Commissariat de police de la rue des bons enfants n° 21 : [dessin] / JA. Chauvet'
WHERE title.id_uuid = 'qr1fa3b38ddaac04f909a32f062660469e1';


UPDATE title
SET entry_name='Ancien Hôtel de Courville : rue des Bons-Enfants 31 : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr10c1386fddf014a25a9c5eb4ab7a0c93a';


UPDATE title
SET entry_name='Ancien Hôtel - Mr Ranchin l''habitait en 1780 : 26, rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr18e354b57d06a4c74853e7126a7063301';


UPDATE title
SET entry_name='Hôtel Liancourt : 21 rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr149003f5c56c04d04b4b600a22e0ba231';


UPDATE title
SET entry_name='Hôtel Mainpoud de la Roche : 30 rue des Bons-Enfants'
WHERE title.id_uuid = 'qr1eab947b47d2f4b6aadabb6b7d4d34f90';


UPDATE title
SET entry_name='Hôtel Liancourt : 21 rue Bons-Enfants : Au 3e étage cadran solaire : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1d1e2cd30e7ad41bda6384a6fdb65d550';


UPDATE title
SET entry_name='Hôtel de la Guillonière : 28 rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr114603fef349b4867807142166dc958f9';


UPDATE title
SET entry_name='Hôtel Liancourt comte de la Roche Guyon : 21 rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1df216cf5e910429890618e5cbd1360bf';


UPDATE title
SET entry_name='Hôtel Lefebvre : lecteur du duc d''Orléans : 23 rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr1382bfaefb1d04b4cbd37c82bb6f08639';


UPDATE title
SET entry_name='Hôtel Lefebvre : 23, rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr184fb4fdec2de41359b692cf7830690f3';


UPDATE title
SET entry_name='Hôtel Liancourt : 21 rue des Bons-Enfants : [photographie] / [Atget]'
WHERE title.id_uuid = 'qr189feea6636114c3f8f098217d94233ed';


UPDATE title
SET entry_name='Médaille'
WHERE title.id_uuid = 'qr19aad5de6b88d4f82bd961df325c93bec';


UPDATE title
SET entry_name='Magnin. Sénateur. Gouverneur de la Banque [de France] : [photographie, tirage de démonstration] / [Atelier Nadar]'
WHERE title.id_uuid = 'qr186a1ba3ed8c04c76beaf102b31381cc3';


UPDATE title
SET entry_name='Banque de France, location de coffres-forts [... ] Bauche : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr1affc54f5980d4b53a7509875ba094f22';


UPDATE title
SET entry_name='Vues de Paris : la banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1a8f14e3ead2c4bf797be87df5b72090d';


UPDATE title
SET entry_name='Banque de France [entrée rue La Vrillière] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1998a96e85be34f9e99209ef5ff991478';


UPDATE title
SET entry_name='Banque de France [entrée rue La Vrillière vue depuis la place des Victoires] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr157f773aaa99c474e9b8c333826e52aeb';


UPDATE title
SET entry_name='Aux Elèves de nos Lycées et Collèges...'
WHERE title.id_uuid = 'qr1c6ed7731fa73487599fae12f4c79edc1';


UPDATE title
SET entry_name='Aux Elèves de nos Lycées et Collèges...'
WHERE title.id_uuid = 'qr1dfce47fe20584230a320301a4d18b27b';


UPDATE title
SET entry_name='31-7-14, crise monétaire [la foule devant la Banque de France à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr17ac3618d99a14b6cae910a2bf558762a';


UPDATE title
SET entry_name='31-7-14, crise monétaire [la foule devant la Banque de France à Paris] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr17bb033442a0d444ba3b88ff2d9c55ed6';


UPDATE title
SET entry_name='Un intérieur de la Banque de France pendant le change de monnaie : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1310136e65c18447da9325533189ebb47';


UPDATE title
SET entry_name='Billet de banque de cinq francs [mis en circulation le 31 juillet 1914 par la Banque de France] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr154e23e00da08402988d3761d839e6aa8';


UPDATE title
SET entry_name='Billet de banque de vingt francs [mis en circulation le 31 juillet 1914 par la Banque de France] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1397d22398b4d47bfbe52e4d9d3f87f62';


UPDATE title
SET entry_name='La Banque de France pendant le change de monnaie : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr13bd53c05f61e4c86ae1f8c747dd73821';


UPDATE title
SET entry_name='A la porte de la Banque de France pendant le change de monnaie : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr12a9d2b9f412041de8a0adb5d7053bb21';


UPDATE title
SET entry_name='La foule devant la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1000d05a1d3d742c5a9eaef307fd0be90';


UPDATE title
SET entry_name='Emprunt de la Défense nationale 5%%'
WHERE title.id_uuid = 'qr17300a6ddacbb44c59bc43550e2271fea';


UPDATE title
SET entry_name='Bons de la Défense nationale. Emission publique'
WHERE title.id_uuid = 'qr1164854ed71ab416ca3a800fbf46a3aa3';


UPDATE title
SET entry_name='A la Banque de France, le change de l''or contre billets de banque : un enfant confectionne son bulletin : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr108105ba5d11b4208a1c9add99f97425b';


UPDATE title
SET entry_name='Emprunt Défense Nationale : Banque de France (intérieur) : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1834a18812f6a422fa479ca867e841d63';


UPDATE title
SET entry_name='A la Banque de France, le change de l''or contre billets de banque : le reçu provisoire : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1ef1d86387ba54f3f85de015a9de4e6da';


UPDATE title
SET entry_name='Aucoc, syndic du Conseil Général et Municipal, administrateur de la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1212f5cdd52b1461f89a5364112c50cab';


UPDATE title
SET entry_name='A la Banque de France, la queue pour la monnaie exterieure : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1718d220539a54ddf890ae54f808c53bc';


UPDATE title
SET entry_name='Inauguration du monument aux morts de la Banque de France : discours du doyen du Conseil : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1fcc25327d8534cf4a56d6ef36969e653';


UPDATE title
SET entry_name='Portrait de M. Robineau, Gouverneur de la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr13b5a5e183b1e495793d4fbddbbaa09af';


UPDATE title
SET entry_name='[Emile] Moreau, gouverneur [de la] Banque de France : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1cc5c7c4c44b44bce803fd4a2c3ab2425';


UPDATE title
SET entry_name='27/9/26, un groupe de personnes attendant l''ouverture des guichets de la Banque de France pour vendre leur or : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1d5e44d7c154e491b811917aaf0de27e1';


UPDATE title
SET entry_name='P[ierre] Strohl, secrétaire [général de la] Banque de France : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr138322b5d37ae464ea62d48ea602e204f';


UPDATE title
SET entry_name='Banque de France : conférence monétaire, au milieu Moreau [?] : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr19a5aea6a0fd6426aa0bbcba7cbe78a9a';


UPDATE title
SET entry_name='Le salon d''or à la Banque de France : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr128716ae571d348adbe1d967cf1d25b14';


UPDATE title
SET entry_name='M. [Clément] Moret, [gouverneur-général de la] Banque de France : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1f1d810e1833943ffb494330cae290df8';


UPDATE title
SET entry_name='La façade de la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1ae3bf9c14e524e03b63c5bc52cee8864';


UPDATE title
SET entry_name='Banque de France : tirage de la loterie : la dette : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1a55e08093f374eae8fa8c4772ee86c55';


UPDATE title
SET entry_name='M. Lebrun assistant au tirage de la dette à la Banque de France : [photographie de presse] / Agence Mondial'
WHERE title.id_uuid = 'qr1af3759d4ede54f87a27839a4ee042588';


UPDATE title
SET entry_name='La réorganisation de la Banque de France : les membres du nouveau conseil général parmi lesquels on reconnaît M. Labeyrie, Jouhaux, Gauthier, etc : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1682cdbdf373341acb6ee37a50dbb60fd';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr129bea7623a194586a5c728207196b406';


UPDATE title
SET entry_name='La réorganisation de la Banque de France : les membres du nouveau conseil général parmi lesquels on reconnaît M. Labeyrie, Jouhaux, Gauthier, etc : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr19b22acae858d4284aff0bf3d91198bcc';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1037a6449684549a997a2515020f4d178';


UPDATE title
SET entry_name='Déjeuner du Docteur Schacht et de M. Labeyrie à la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1f7bba8bb46144c50aad0696a846b06a3';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : les membres du Conseil Général : MM. Labeyrie, Jouhaux, Gauthier etc... : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr103407745560546ec96a8231ebf78997d';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1ed5526e47df04a3c88e3ef8610cea5ba';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : les membres du Conseil Général : MM. Labeyrie, Jouhaux, Gauthier etc... : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1714c1b4568944813a0605e3749a87a23';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1acc522ec50ec43a7827e97c983e33528';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1f4e57f13755243cb8894026fa332a1d6';


UPDATE title
SET entry_name='AD075PH_UPF1306'
WHERE title.id_uuid = 'qr1b7411f24fb1940c7b35bcbb8c5e56012';


UPDATE title
SET entry_name='Première assemblée générale des 40.000 actionnaires de la Banque de France : M. Labeyrie et M. Neuflize : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1df34625e8c094a2196fbcc009aa8d69f';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr17b89c546bc884277aadf838f4396e25e';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr17a1282a3a73645489bbf3b909e6bf652';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr166b0de2d2c1d4e8b824cb361265fd402';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1dd03ebc279fe4176ad8e99f548b088bc';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1e5ae2079320448f28f7650268f532ce4';


UPDATE title
SET entry_name='La réorganisation de la Banque de France : vues extérieures de la Banque de France : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr151a478d848f44703a631fd5a9b946b35';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr150adfc599a7e4b31b3243d08da414c2c';


UPDATE title
SET entry_name='La réorganisation de la Banque de France : les membres du nouveau conseil général parmi lesquels on reconnaît M. Labeyrie, Jouhaux, Gauthier, etc : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1052eb55d63404942accc08be731d270c';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr17f6b14791a8b4509a4b0650387f6e05a';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1829c4844ee85498eb92499b706c49fd5';


UPDATE title
SET entry_name='La réorganisation de la Banque de France : les membres du nouveau conseil général parmi lesquels on reconnaît M. Labeyrie, Jouhaux, Gauthier, etc : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1bdd3770c4b8e4b78930f350121846635';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr107dc53d7ea67489fa318801c2dfcda73';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr16d3f1d3e34414029b5df2bf1cc0003e0';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1b818326f5e844982ae5c6646b2f6c295';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr185af50efc9ca45d0aecba2b6fd959cce';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue intérieure les bureaux, les caves, les coffres-forts : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr1c0cd0bbeb3a54fa7ba065d1ef451b0eb';


UPDATE title
SET entry_name='Réorganisation de la Banque de France : vue extérieure : [photographie de presse] / Agence Meurisse'
WHERE title.id_uuid = 'qr17d718641584a45fa93641ac6d846e585';


UPDATE title
SET entry_name='Médaille octogonale'
WHERE title.id_uuid = 'qr1616046c335cd4daba3a07abf2e8ff8e7';


UPDATE title
SET entry_name='Établissement Duval - Rue Montesquieu (vue intérieure)'
WHERE title.id_uuid = 'qr1f22855d3e0b4446a9c9de1463182e7d4';


UPDATE title
SET entry_name='Établissement Duval, Rue Montesquieu'
WHERE title.id_uuid = 'qr16bde9d2d850d4b1ab29cb99747df4e88';


UPDATE title
SET entry_name='3/11/28, [concours des apprenties de couture, de g. à d.] M. Luc, M. Worth, Mlle Goulard : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr1b6f9e8ecd4cd4de09d6d6f8f8bf9082d';


UPDATE title
SET entry_name='25/2/28, les meilleures ouvrières, [de g. à d.] Mlle Legouais, Souchand, Imbert, Boiron [réunies à la chambre syndicale de la couture, rue Montesquieu] : [photographie de presse] / [Agence Rol]'
WHERE title.id_uuid = 'qr158e22c0a6e2447109c240a663ae64e55';


UPDATE title
SET entry_name='Intérieur du café Montesquieu tenu par M. Warec au coin des Rues des Bons enfans et Montesquieu.'
WHERE title.id_uuid = 'qr1af87e3e148f64fb485c07dfaa63431c1';


UPDATE title
SET entry_name='M. René de Knyff [directeur de la Revue des Sports, sise 21 rue Croix des Petits Champs]'
WHERE title.id_uuid = 'qr18459bc75d24544babfc657a25fa1b041';


UPDATE title
SET entry_name='Notice sur l''état ancien et nouveau de la galerie de l''hôtel de Toulouse'
WHERE title.id_uuid = 'qr17a700d040f5f400e9aaaf43e638b1af0';


UPDATE title
SET entry_name='|Murs pignons de la ville de Paris]'
WHERE title.id_uuid = 'qr19a1795080fc24195b3c6d782e22d1570';


UPDATE title
SET entry_name='Billet d''entrée pour une dame au "Bal masqué et travesti" du Théâtre de la Renaissance, Salle Ventadour, à la Mi-Carême, [illisible]. Encadrement décoratif et symbolique, (personnages travestis), le tout impr. sur du papier peint : [estampe]'
WHERE title.id_uuid = 'qr18e164c140cd546a982cee1faf9babaaf';


UPDATE title
SET entry_name='«La Patrie les a vengés !!! Dédié Aux Amis de la Liberté.» Bories, Raoulx, Goubin, Pommier, en buste, de tr. q. encadrent la «Lettre... datée de Bicêtre le 8 Septembre 1822.» qui envoie au procureur général le désistement de leur pourvoi en cassation : [estampe]'
WHERE title.id_uuid = 'qr106f4c6df03054252b398ba6c51a083ec';


UPDATE title
SET entry_name='Vue de la maison du passage Radzivill, rue de Valois'
WHERE title.id_uuid = 'qr1c0267ec24f8d444fa2330857c42bd29d';


UPDATE title
SET entry_name='Maison du passage Radzivill. Coupe montrant l''escalier à double révolution'
WHERE title.id_uuid = 'qr10d8bddf0ee9e41039088262d54af2c97';


UPDATE title
SET entry_name='[Encart publicitaire Maison Pape, pianos]'
WHERE title.id_uuid = 'qr148742b63fd5c4c6da4b69648876df63a';


UPDATE title
SET entry_name='Notice sur les inventions et les perfectionnements de H. Pape'
WHERE title.id_uuid = 'qr13be448af3fef4e019df96661935be49d';


UPDATE title
SET entry_name='Piano Pape, en érable (vers 1825) (Coll. J. Chélo)'
WHERE title.id_uuid = 'qr1ed5ab397e995416a9a4ec7c9e8bacf63';


UPDATE title
SET entry_name='Arsène Meunier, Instituteur Primaire, Aux Électeurs du Département de la Seine'
WHERE title.id_uuid = 'qr17d5be804ec9d4b71b40b28a32b725fe5';


UPDATE title
SET entry_name='Cérémonial relatif au sacre et au couronnement de Leurs Majestés impériales'
WHERE title.id_uuid = 'qr1816d86ad01824ae9ad9c0eb6099451a6';


UPDATE title
SET entry_name='Grande photographie des enfants de France. Portraits d''enfants, Saint Edmé. Paris, 50 Esplanade des Invalides, rue Fabert, 50 : [affiche] / [Non identifié]'
WHERE title.id_uuid = 'qr11785b525f97a4b16a211c40924df4121';


UPDATE title
SET entry_name='Théâtre National de l''Opéra-Comique. Jean de Nivelle, opéra en 3 actes. Poème de MM. Gondinet et Ph.Gilles, [musique de ] Léo Delibes [...] : Heugel & Fils éditeurs pour la France et l''étranger [...] : [affiche] / Barbizet'
WHERE title.id_uuid = 'qr1f0ad24568114450391c54e02372e9259';


UPDATE title
SET entry_name='Académie nationale de musique. La Korrigane,ballet fantastique de MM. F. Coppée et L. Mérante. Musique de Ch. M. Widor. Publication du Ménestrel, Paris, 2 bis rue Vivienne, Heugel & fils éditeurs...En vente ici (portr de Rosita Mauri) : [affiche] / E. Buval'
WHERE title.id_uuid = 'qr1bd987878a67c4eb6891f098db81d4d48';


UPDATE title
SET entry_name='1877. Répertoire des bals de l''Opéra. Oeuvres choisies de Joseph, Edouard et Johann Strauss, de Vienne : [affiche] / [non identifié]'
WHERE title.id_uuid = 'qr15d9d4b44e13a489bb99795c6a315ef0f';


UPDATE title
SET entry_name='Le petit Faust, opéra-bouffe en 3 actes : musique de Hervé : [estampe] / Stop [sig.]'
WHERE title.id_uuid = 'qr1cf816ad538b3453ead12132b2d56c5ac';


UPDATE title
SET entry_name='Abbé François Guichené / Victor Coindre'
WHERE title.id_uuid = 'qr162cb5834659346abbd16460c676690ed';


UPDATE title
SET entry_name='Théatre de l''Athénée. Le Petit Poucet, opéra-bouffe en 3 actes et 4 tableaux, paroles de E. Leterrier et A. Vanloo. Musique de Laurent de Rillé : Airs détachés. Partition chant et piano. Quadrille, valse, polka, polka-masurka. En vente ici. : [affiche] / E. Delay'
WHERE title.id_uuid = 'qr1b4454e09729848188fafde11b42d4f55';


UPDATE title
SET entry_name='Théâtre des Fantaisies-Parisiennes. Les Deux Arlequins. Opéra-comique. Opéra-comique en 1 acte... Paroles de M. Mestépès. Musique de Emile Jonas... : [affiche] / lithographie de Léon Loire'
WHERE title.id_uuid = 'qr1f893f21068014d58b84cb94a31a53638';


UPDATE title
SET entry_name='Mines de Bruay (Pas de Calais), jeton de présence. A/ Deux mineurs de face ; R/ Trophée d''outils (1855), Barberon'
WHERE title.id_uuid = 'qr13d1cf478eca24912b1654f4ad3e74988';


UPDATE title
SET entry_name='Papeterie de la Banque de France - Emile Acker'
WHERE title.id_uuid = 'qr1c7fe4ca779444a3aa1a6c169d66a8e70';


UPDATE title
SET entry_name='Papeterie de la Banque de France'
WHERE title.id_uuid = 'qr1aa63cf0c7de24f84b77c0cbbc0fd71ea';


UPDATE title
SET entry_name='Monsieur Georges Robineau'
WHERE title.id_uuid = 'qr1a423abfea6d64ecd975cb61bc9cb219c';


UPDATE title
SET entry_name='Propriété d'' Albert Kahn , Boulogne , France Monsieur Georges Robineau'
WHERE title.id_uuid = 'qr19753ea4730de4b67ad13d65c2b7a00d3';


UPDATE title
SET entry_name='Monsieur Pierre-Eugène Fournier'
WHERE title.id_uuid = 'qr1da496769e1634c32b0eac25f3e82c626';


UPDATE title
SET entry_name='Propriété d'' Albert Kahn , Boulogne , France Monsieur Pierre-Eugène Fournier'
WHERE title.id_uuid = 'qr11b600fdcf5c140d6bde6e7a6a279a891';


UPDATE title
SET entry_name='Paris ( 1er arr . ) , France La rue de Beaujolais'
WHERE title.id_uuid = 'qr14d93aa2b60e64eec92ce3e2ebdd1f6e0';


UPDATE title
SET entry_name='1bis rue Baillif'
WHERE title.id_uuid = 'qr17676835c607f48c38fff44e8b0b3f357';


UPDATE title
SET entry_name='AD075PH_UPF0040'
WHERE title.id_uuid = 'qr17051f13adb9a4c91b37db34ab530539d';


UPDATE title
SET entry_name='3 rue Baillif, angle rue des Bons Enfants'
WHERE title.id_uuid = 'qr1d2e83e6bc1a441d99a3ea765b176e276';


UPDATE title
SET entry_name='AD075PH_UPF0041'
WHERE title.id_uuid = 'qr10667c0974d2e479bb4d26c01924ef569';


UPDATE title
SET entry_name='7 rue Baillif'
WHERE title.id_uuid = 'qr1abba832881b34c76a9a39c5219f0c5f9';


UPDATE title
SET entry_name='AD075PH_UPF0042'
WHERE title.id_uuid = 'qr113b2281c41a24d6599c889d6558612ef';


UPDATE title
SET entry_name='9 rue Baillif'
WHERE title.id_uuid = 'qr1b71c4eac0ea24dfbae58303a21837aed';


UPDATE title
SET entry_name='AD075PH_UPF0043'
WHERE title.id_uuid = 'qr1d1ac368108fe4e6f90e04941f0333404';


UPDATE title
SET entry_name='19 rue des Bons Enfants'
WHERE title.id_uuid = 'qr139ad094b653a4f38803960382b921fbb';


UPDATE title
SET entry_name='AD075PH_UPF0181'
WHERE title.id_uuid = 'qr1f225019f9f734296a80a6e9f31864ae2';


UPDATE title
SET entry_name='21 rue des Bons Enfants'
WHERE title.id_uuid = 'qr198e1602ebfa84b24adddf865a9979deb';


UPDATE title
SET entry_name='AD075PH_UPF0182'
WHERE title.id_uuid = 'qr11b83873a34bb4efe8880d5d7ec8dddf8';


UPDATE title
SET entry_name='23 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1879965eff463433abc67ba0783474f93';


UPDATE title
SET entry_name='AD075PH_UPF0183'
WHERE title.id_uuid = 'qr1f0ed12f43f9c44dc844be358bd4e4c3f';


UPDATE title
SET entry_name='24 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1575844adad26468b871697111c52885b';


UPDATE title
SET entry_name='AD075PH_UPF0184'
WHERE title.id_uuid = 'qr1dbc6fc3316594d2bbbc13fc6956704b0';


UPDATE title
SET entry_name='26 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1cb6a3e9912d74ed79eed84a2e8c41f5a';


UPDATE title
SET entry_name='AD075PH_UPF0185'
WHERE title.id_uuid = 'qr1d3ed8654ff51482ea99ccb6e2ade0e2d';


UPDATE title
SET entry_name='27 rue des bons Enfants'
WHERE title.id_uuid = 'qr1bfa01e8c2d304e69b73876db8d422705';


UPDATE title
SET entry_name='AD075PH_UPF0186'
WHERE title.id_uuid = 'qr1072a945f88dc4aa397be81331b14aee2';


UPDATE title
SET entry_name='28 rue des Bons Enfants'
WHERE title.id_uuid = 'qr14f4e901020964b78a0eb2180e7befd62';


UPDATE title
SET entry_name='AD075PH_UPF0187'
WHERE title.id_uuid = 'qr106a62685725c4472913ed5e79583bf82';


UPDATE title
SET entry_name='29 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1cd2d0b3758cf442db94b1c602e7d1957';


UPDATE title
SET entry_name='AD075PH_UPF0188'
WHERE title.id_uuid = 'qr147f1b7037bde4958a0a5b8c68bf06fb4';


UPDATE title
SET entry_name='30 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1671636f09a414107ace6c7281ee34d22';


UPDATE title
SET entry_name='AD075PH_UPF0189'
WHERE title.id_uuid = 'qr1f2564177deb143f9929e4ebbdc4c12ae';


UPDATE title
SET entry_name='31 rue des Bons Enfants'
WHERE title.id_uuid = 'qr155261beb3efb47b78172b5849181f114';


UPDATE title
SET entry_name='AD075PH_UPF0190'
WHERE title.id_uuid = 'qr10bddd8f4fd6c4f81adad2a6f9bf15ebe';


UPDATE title
SET entry_name='32 rue des Bons Enfants'
WHERE title.id_uuid = 'qr1e9ec87bb43df4b2d97b88734824d5b8d';


UPDATE title
SET entry_name='AD075PH_UPF0191'
WHERE title.id_uuid = 'qr15bd2312cf2354063ae266d95d42030e8';


UPDATE title
SET entry_name='33 rue des Bons Enfants, angle rue Baillif'
WHERE title.id_uuid = 'qr12dd8ef9e81024b00aed53eee835a92a9';


UPDATE title
SET entry_name='AD075PH_UPF0192'
WHERE title.id_uuid = 'qr139e9b858880c49089dd8561ac01b4027';


UPDATE title
SET entry_name='13 rue Croix des Petits Champs, angle rue Montesquieu'
WHERE title.id_uuid = 'qr163d55f6e0639426ca2db05e6fc9c2e3f';


UPDATE title
SET entry_name='AD075PH_UPF0420'
WHERE title.id_uuid = 'qr1749b9bbc15da43ffb4f1895d2a9168ad';


UPDATE title
SET entry_name='15 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1c2124e89eb0a4300b209331f84c057f8';


UPDATE title
SET entry_name='AD075PH_UPF0421'
WHERE title.id_uuid = 'qr1e5e942d7c9f34b7398f6bac890ed7d6f';


UPDATE title
SET entry_name='17 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr191935ace3960424fbd74a98a81adb6c5';


UPDATE title
SET entry_name='AD075PH_UPF0422'
WHERE title.id_uuid = 'qr1bf319877dbfc46d583291da6aeabde45';


UPDATE title
SET entry_name='21 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1d5bfd1a562ad4fccaa201b8e6bb79c1b';


UPDATE title
SET entry_name='AD075PH_UPF0423'
WHERE title.id_uuid = 'qr17b7b86ec62c74dd19d652c9d1d48b4ca';


UPDATE title
SET entry_name='23 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr10f2ba8852a5d46beb389acef31854028';


UPDATE title
SET entry_name='AD075PH_UPF0424'
WHERE title.id_uuid = 'qr13238f2dc07334920811a347f6ed376bb';


UPDATE title
SET entry_name='25 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr16d53f69834a443a6b8364d6f9fa1c076';


UPDATE title
SET entry_name='AD075PH_UPF0425'
WHERE title.id_uuid = 'qr11ab7c6c7afad404782cbf3805b6ea2f6';


UPDATE title
SET entry_name='27 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1c2768af9d23940a98d17d6de2f860e9c';


UPDATE title
SET entry_name='AD075PH_UPF0426'
WHERE title.id_uuid = 'qr134deeabcf2c84ad9971ab067ec8bac0c';


UPDATE title
SET entry_name='29 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1505a3231f6b345fdadc42fdc4de9cbe8';


UPDATE title
SET entry_name='AD075PH_UPF0427'
WHERE title.id_uuid = 'qr1750ea23635014325bbca11249d18e617';


UPDATE title
SET entry_name='31 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr190d937cd23894e83b1acd814da6b48d3';


UPDATE title
SET entry_name='AD075PH_UPF0428'
WHERE title.id_uuid = 'qr1db12b81e8b504455a82199c65020c265';


UPDATE title
SET entry_name='33 rue Croix des Petits Champs, angle 1 rue Baillif'
WHERE title.id_uuid = 'qr1fdef94518893496bb5382b15f8c4efab';


UPDATE title
SET entry_name='AD075PH_UPF0429'
WHERE title.id_uuid = 'qr121b5f8a8bf0648139585c6e980724944';


UPDATE title
SET entry_name='1 rue des Petits Champs, angle rue Radziwill'
WHERE title.id_uuid = 'qr10dddbd1e07fa4ff3bbedce9c4081aba1';


UPDATE title
SET entry_name='AD075PH_UPF1014'
WHERE title.id_uuid = 'qr16cd475751c1c466c915ddc0a857690ce';


UPDATE title
SET entry_name='19 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr12fffd799a0f444bc9b48e01d29c302e5';


UPDATE title
SET entry_name='AD075PH_UPF1015'
WHERE title.id_uuid = 'qr1ec322ed1309447ee8574403a50267096';


UPDATE title
SET entry_name='10 rue de Valois'
WHERE title.id_uuid = 'qr1266e507327954fe8b2b13a002f3c5a64';


UPDATE title
SET entry_name='AD075PH_UPF1304'
WHERE title.id_uuid = 'qr1392fd772ce434ed7b2db479ae3d0d1da';


UPDATE title
SET entry_name='12 rue de Valois'
WHERE title.id_uuid = 'qr15d1021c8a4184cb2859bcba176cc8316';


UPDATE title
SET entry_name='AD075PH_UPF1305'
WHERE title.id_uuid = 'qr17ae07b9bbf93428b82fa23cdb49c6174';


UPDATE title
SET entry_name='22 rue de Valois, angle rue Baillif'
WHERE title.id_uuid = 'qr14f7f1ac22939487bbd6d8d3ceb744634';


UPDATE title
SET entry_name='AD075PH_UPF1310'
WHERE title.id_uuid = 'qr12dd27aba3f804b7ba1ec92d2e606d4bb';


UPDATE title
SET entry_name='Immeuble, rue des Bons Enfants'
WHERE title.id_uuid = 'qr1dfaf64486d9147489d62718702a37e98';


UPDATE title
SET entry_name='AD075PH_ARC0075'
WHERE title.id_uuid = 'qr1e97573c473b342669fd9dde3409ca8a1';


UPDATE title
SET entry_name='2 rue de la Banque'
WHERE title.id_uuid = 'qr1870305831f12459c9f7aeeb7d82d13b0';


UPDATE title
SET entry_name='AD075PH_UPF0044'
WHERE title.id_uuid = 'qr1901db02ed1f54f6190dd01ed6b3befb4';


UPDATE title
SET entry_name='2 rue de la Banque'
WHERE title.id_uuid = 'qr1c7ed47bfa3c44e118900f5ccb206b67a';


UPDATE title
SET entry_name='AD075PH_UPF0045'
WHERE title.id_uuid = 'qr18150235086744b19807069ae4e8bbda4';


UPDATE title
SET entry_name='6 rue La Feuillade'
WHERE title.id_uuid = 'qr197c7f1e50bb14a76a6384ac77ad9afab';


UPDATE title
SET entry_name='AD075PH_UPF0768'
WHERE title.id_uuid = 'qr1fd453639d18f4b3789cef79c6a7ace06';


UPDATE title
SET entry_name='Bar de la Banque de France - 10 rue de la Vrillière'
WHERE title.id_uuid = 'qr1d151d057026d48cb9e4d05dd7dbca195';


UPDATE title
SET entry_name='Centenaire de la banque de France, 1800-1900 : [médaille]'
WHERE title.id_uuid = 'qr1020ec9f5b5134cf3b78ee98bae357431';


UPDATE title
SET entry_name='Quartier autour de la place des Victoires'
WHERE title.id_uuid = 'qr10c95b79c816649f1b1e2f04d7d84c6ac';


UPDATE title
SET entry_name='A vendre à l''amiable en dix neuf lots les terrains des anciens ateliers des messageries royales. S''adresser à Mtre Yver, notaire Rue St Honoré, n° 422, et à l''Administration des Messageries Royales, rue Notre Dame des Victoires, n° 22 : vers 1845'
WHERE title.id_uuid = 'qr1c72e97ff7ed84afcb0c694b59903f81f';


UPDATE title
SET entry_name='[Paris. La Banque de France. Rue La Vrillière, 1er arr. ] : [photographie]'
WHERE title.id_uuid = 'qr1f0cb06cece1945be86796d8ac75d2490';


UPDATE title
SET entry_name='[Paris.] La foule attendant le retrait de ses dépôts à la Banque de France la veille de la déclaration de guerre [39, rue Croix des Petits-Champs] : [photographie]'
WHERE title.id_uuid = 'qr1f1dd7de2356c449cb1fb29a4e4f3f0ec';


UPDATE title
SET entry_name='Paris. La foule devant la Banque de France dans la semaine précédant la déclaration de guerre [39, rue Croix des Petits-Champs] : [photographie]'
WHERE title.id_uuid = 'qr13d4b41ee4f4e497db687e01a355ba24c';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr128c3be883e1143148f682366b9f29c1e';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr1d5c27d19abfb445e8d634748da5e2746';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr15989289724a04f83ac550153517b719c';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr1af4df2d804294eb8b526634d685454b3';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr18e3a2d64647d48a69df18811555437ad';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr1ac4652523d1e4992b0e5d688bd4e58a9';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr1050921db9a124d1aa71377baa7181eec';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr12e1fd15ac84b431f890af82733b3bfa0';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr1960d936b0302436d9cecb4a72f0291b7';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr13f527c565f054429b1d4434ec1ad69fb';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr1ef34388e2b0b4903838ebcae846b95df';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr193e39817a19340e29e855890fee17275';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr171b13453bde34e96b0e7e3af6d52eedc';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr145c7f23388c84eac910008d9ef2a0850';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr153f5ffd1646c4191ae93622d1cc76459';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr14de0bb733d6a42c589730984272a9c33';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr16774c6529466434f821ca0b28174835c';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr16cccb9c46a6241deab55e5a950391037';


UPDATE title
SET entry_name='Rue Croix des Petits Champs, 31. Banque de France'
WHERE title.id_uuid = 'qr17d015cfccfe14e35afb8bb0afdffae1b';


UPDATE title
SET entry_name='[n° de rue du titre : erroné]'
WHERE title.id_uuid = 'qr13bd5148c84b4473e919457244791f52b';


UPDATE title
SET entry_name='39. [Paris], Vue de la Banque de France, prise de la rue Croix des Petits-Champs / Courvoisier del. Dubois sculp.'
WHERE title.id_uuid = 'qr1845ca7ba35f04616b5fceb806081f871';


UPDATE title
SET entry_name='[Paris. Tourelle de l''hôtel de la Vrillière et la banque de France, rue Croix-des-Petits-Champs depuis la place des Victoires.1er arr.]'
WHERE title.id_uuid = 'qr1cd9801852d8e403ea359ed39cf470662';


UPDATE title
SET entry_name='f. 51v-52r [Notes de l''artiste] ; Fontaine, rue de la Vrillière n° 6. Paris 16 août 1852. (quartier de la Bourse)'
WHERE title.id_uuid = 'qr1249a73ca82f34d4d874c8765cc2ef73d';


UPDATE title
SET entry_name='[Paris, rue La Vrillière. Cartes postales]'
WHERE title.id_uuid = 'qr120894871d76d48778e3a2aabf524da3d';


UPDATE title
SET entry_name='A la Petite Biche - 6 rue de la Vrillière'
WHERE title.id_uuid = 'qr14dbaeadbd7ee4312a8d712773a7a696c';


UPDATE title
SET entry_name='Bar des Primes - 10 rue de la Vrillière'
WHERE title.id_uuid = 'qr129ae6750d9324b8593776b26a5f1d21e';


UPDATE title
SET entry_name='Au Rendez-vous des Amis, Maison Friry - 8 rue de la Vrillière'
WHERE title.id_uuid = 'qr1a822342f95e24e6cb61aa53a5c28970a';


UPDATE title
SET entry_name='Plan de la rue La Vrillière. Alignements : ordonnance royale du 23 juillet 1828'
WHERE title.id_uuid = 'qr142e291fa92a54321b95848d0b50cbccc';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants, n° 19. Hôtel de la Chancellerie d''Orléans. Façade sur cour] : [photographie]'
WHERE title.id_uuid = 'qr1718fd19423fc4776a3d064ed34c50c41';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants, n° 19 et rue de Valois, n° 10]. Hôtel dit de la Chancellerie d''Orléans. Rez-de-chaussée. Salons : [photographie] / clichés Godefroy'
WHERE title.id_uuid = 'qr1e7446e25b848462397ebb314e4f1b916';


UPDATE title
SET entry_name='Plan de la chapelle Saint-Clair des Bons-Enfants d''après François Quesnel'
WHERE title.id_uuid = 'qr1667699a544ba46c992fb606a2fcd5f67';


UPDATE title
SET entry_name='[Paris]. Rue des Bons-Enfants, [n° 9 et 11]. Passage Vérité : [photographie]'
WHERE title.id_uuid = 'qr1df7bfcbccf7244ab884c5709236e217e';


UPDATE title
SET entry_name='[Paris]. Passage Vérité place de Valois [conduisant à la rue des Bons-Enfants] : [photographie] / cliché Atget'
WHERE title.id_uuid = 'qr1e64483c9af594f9e8be060beef6c75bb';


UPDATE title
SET entry_name='[Paris.] Rue des bons Enfants, n° 31. Ancien hôtel de Courville : [photographie] / cliché Atget'
WHERE title.id_uuid = 'qr12085e7f6ba604bd29f956c13a06bff74';


UPDATE title
SET entry_name='Comme vous le commerçant subit les prix, avec vous le commerçant combat ce qui fait la vie chère. Affiche primée par la Fédération nationale de l''habillement nouveautés et accessoires 18, rue des Bons enfants, Paris : [affiche] / Rick'
WHERE title.id_uuid = 'qr10e44253cfe6d4593ab3fbdd85226e536';


UPDATE title
SET entry_name='[Boutiques parisiennes. Glacier, pâtissier. Glacier du Palais Royal, 6, rue de Valois (1er arr.)] : [photographie] /'
WHERE title.id_uuid = 'qr11951ad23f0794c859571f5db3144634d';


UPDATE title
SET entry_name='[Paris. 23, rue Croix-des-Petits-Champs. Vue d''une manufacture d''habillement, Segré et Lazard. Deux plaques d''huissier au-dessus du porche] : [photographie]'
WHERE title.id_uuid = 'qr124fb6ba8be264ae6962f28038019bcf9';


UPDATE title
SET entry_name='Le Puits d''amour'
WHERE title.id_uuid = 'qr16f65e29d9b2f45328f4559d63c9c507d';


UPDATE title
SET entry_name='Restaurant Bertin, Restaurant Demeure - 5 rue de la Banque'
WHERE title.id_uuid = 'qr126994caca40c4073b4828794056c7eac';


UPDATE title
SET entry_name='Café Biard - 2 rue des Petits Pères'
WHERE title.id_uuid = 'qr1082e5a53fe9a48888997bc356a4adc31';


UPDATE title
SET entry_name='Rue La Feuillade. Elargissement à l''angle de la rue des Petits-Pères. Dégagement de la rue de la Banque. Plan parcellaire [1900]'
WHERE title.id_uuid = 'qr14b69d1158b214acdb6eabce66407e390';


UPDATE title
SET entry_name='Bar du Familistère - 1 rue des Petits Champs'
WHERE title.id_uuid = 'qr1c9b9f7b4b10e48d9be673ba0c1be78f9';


UPDATE title
SET entry_name='Café-Bar Du Bouquet - 3 rue des Petits Champs'
WHERE title.id_uuid = 'qr17360a8c2eada48cb9ca73dd7574608c4';


UPDATE title
SET entry_name='[Boutiques parisiennes. Restaurant, vins. Au Rendez-vous de la Banque, 1, rue de la Banque, et 2, rue des Petits-Champs (2e arr.)] : [photographie]'
WHERE title.id_uuid = 'qr1e957043e3a5d43f99165eaba02b7b7b7';


UPDATE title
SET entry_name='Paris souterrain'
WHERE title.id_uuid = 'qr1adcb81ada82041b7bd12869d1cc6ab94';


UPDATE title
SET entry_name='Libération de Paris. [Un groupe de passants lit un avis apposé sur un mur de] la rue des Petits-Champs, à l''angle de la rue Radziwill : [photographie] / Jean Roubier'
WHERE title.id_uuid = 'qr1cd4914acb2e841959ee894ca519869c6';


UPDATE title
SET entry_name='Rue des Petits Champs'
WHERE title.id_uuid = 'qr10485dfe6ffae4b64b4956b120f1360ff';


UPDATE title
SET entry_name='Plan de la rue Radziwill. Alignements : ordonnance royale de 1840 et décret de 1863'
WHERE title.id_uuid = 'qr12da2fb8682964283a880c25dcb3fad6d';


UPDATE title
SET entry_name='[Paris, rue Radziwill. Cartes postales]'
WHERE title.id_uuid = 'qr1da61db29c981471587a8fd04fb644508';


UPDATE title
SET entry_name='[Paris.] La queue pour le dépôt des paquets pour l''envoi aux soldats au Front, rue de Radziwill : [photographie] / [Identité judiciaire]'
WHERE title.id_uuid = 'qr15f93e6feb064483dbe6b9d36e05318c6';


UPDATE title
SET entry_name='Grand Café-Restaurant de la Jeune France - 6 rue de Valois'
WHERE title.id_uuid = 'qr1a77ed8c4a8734a4b8da51fcc5f118473';


UPDATE title
SET entry_name='Banquet Funck-Brentano'
WHERE title.id_uuid = 'qr14a2459a3923e490aa1834ac9e0525102';


UPDATE title
SET entry_name='Ouverture d''une voie nouvelle [rue du Colonel Driant] en remplacement de la rue Baillif supprimée. Plan parcellaire : [1915]'
WHERE title.id_uuid = 'qr1a045945acd29424b8e9abfe1fe3b334c';


UPDATE title
SET entry_name='Liquoriste-Bar, Maison M. F. Salsac - 38 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr15da2ce215a6b4e47927c9f0bda2cd1ec';


UPDATE title
SET entry_name='Bouillon-Restaurant des 4 Tourelles - 50 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr174abd1047ae945dab52742b2473d576d';


UPDATE title
SET entry_name='Restaurant-Table d''Hôte Martin - 16 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr1332a8561f672461ab9d7138088d6291a';


UPDATE title
SET entry_name='Grand Restaurant de la Banque et des Postes - 40 rue Croix des Petits Champs'
WHERE title.id_uuid = 'qr160de3068188949f68d10a31b311fbbf4';


UPDATE title
SET entry_name='Restaurant et Salon Louis XIV - 1 bis place des Victoires'
WHERE title.id_uuid = 'qr1e7e98abf82c24d1a9b1ad000b0fb8758';


UPDATE title
SET entry_name='[Boutiques parisiennes. Café, restaurant. Maison Lenormand, 28, rue Croix-des-Petits-Champs (1er arr.)] : [photographie]'
WHERE title.id_uuid = 'qr1b5d3a2ad12ce4846b9cea37c5670d197';


UPDATE title
SET entry_name='[Paris, rue Croix des Petits Champs. Cartes postales]'
WHERE title.id_uuid = 'qr171af4a46886344adb55c2c50e0181ad6';


UPDATE title
SET entry_name='[Boutiques parisiennes. Banque, bijoutier. Crédit industriel et commercial, succursale B ; Joaillier. Maison Paul Templier fils, 52, rue Croix des Petits-Champs, et 3, place des Victoires (1er arr.)] : [photographie] /'
WHERE title.id_uuid = 'qr199f04bb66f364a2198f9cf70a8176158';


UPDATE title
SET entry_name='Maison à loyer, rue Croix-des-Petits-Champs'
WHERE title.id_uuid = 'qr1eda89e36a163488884c3cd91eb2827d5';


UPDATE title
SET entry_name='Rue Coquillière'
WHERE title.id_uuid = 'qr1534afec48fd9482bb5cbe53a5b7d6231';


UPDATE title
SET entry_name='Bonnard-Bidault. Affichage et distribution d''Imprimés, 8, rue Montesquieu : [affiche] / Chéret'
WHERE title.id_uuid = 'qr111514be7a9af4f038e82408b6dab4d81';


UPDATE title
SET entry_name='Agrandissement des Magasins du Coin de Rue'
WHERE title.id_uuid = 'qr19956d972e662433caf10b3b5b1e9f63e';


UPDATE title
SET entry_name='[Paris. Rue Montesquieu]'
WHERE title.id_uuid = 'qr1436a1da1af9a4006bbdb1d2eadb94a37';


UPDATE title
SET entry_name='Vue intérieure du nouvel établissement d''entreprise d''Affichage et de Distribution d''imprimés, 8 rue Montesquieu'
WHERE title.id_uuid = 'qr1b24f51ea78d842179b8126659d9d4cc7';


UPDATE title
SET entry_name='Hotel Duval, 6 rue Montesquieu'
WHERE title.id_uuid = 'qr1cd194503768842d49b52b87bc58b5a05';


UPDATE title
SET entry_name='La caissière du Bouillon Duval : chanson type / musique de Emile Duhem ; paroles de Aristide Bruant'
WHERE title.id_uuid = 'qr14d02cfceeba34d5f889f2cffec40f9c4';


UPDATE title
SET entry_name='[recueil] Galerie dorée [de la Banque de France] : [photographie]'
WHERE title.id_uuid = 'qr152b02053058849f0988023b09ceb106a';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr10e3c13f79ed64ec496fc59f6b7bc5a41';


UPDATE title
SET entry_name='Coupe du principal corps de logis et élévation de l''une des aisles de l''hotel de Toulouse'
WHERE title.id_uuid = 'qr17e466988e7f84befb24e8263aca88469';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr13f73a3f36766470da2ed60f816eec9da';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr16e350f733a3246ea92c79c419a7f806c';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1ec69e86a34a044589481fa1543b15e39';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr12a64da87ef3043f1b330a0c13ed02f48';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr18fa456b9fe2b4f5ab903f79576a6850a';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1511165f215e242f09f739e6d6b98dc7d';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr11d11e4985d134aeb9c92e92d1eeea693';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1bde8560342184be5918b2428d9ca1066';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr12d09fbfed97042509d3f36ab06b193ab';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr137c9d096ab71400c9fb4d68026950bde';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1df360895b6864adda21d4a1836d69b1d';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1baa45b0e782a4d438546f2a63913c4ba';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1d862f90e868b401989fe87a80c845336';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr16741f9cb432149068fc40f89cf6eed76';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr13f716a9ab21549eaac8147bfba7d784a';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1a6a7eb97aa6b4f099be943f96b6e511c';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr14f7a9c458ba644269abe68c43d3d15c4';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1ca0c0446cca548e1a79bffa52714560c';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1031c8fec43d34036b15a2a5ca70bf685';


UPDATE title
SET entry_name='Nos financiers en robe de Chambre. Revue biographique et critique'
WHERE title.id_uuid = 'qr1a74f87307fda4616b12e2b2eb0e38578';


UPDATE title
SET entry_name='[Paris. Banque de France]'
WHERE title.id_uuid = 'qr1038ba3d7145f47da8aa340bd8fe5d4cf';


UPDATE title
SET entry_name='[recueil] Restaurant du Bœuf à la Mode - 8 rue de Valois'
WHERE title.id_uuid = 'qr131cc7f63544947e8ad4f40c0ae92e60e';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants]'
WHERE title.id_uuid = 'qr107b5ef6b0571435b90d112a329124a7f';


UPDATE title
SET entry_name='[Paris. Rue des Bons-Enfants]'
WHERE title.id_uuid = 'qr12eb274f8282842d0904b18d83570b7ea';


UPDATE title
SET entry_name='[Recueil iconographique. Rue La Vrillière (Paris)]'
WHERE title.id_uuid = 'qr170a972d5ab6c44f3a82c6fe094400b81';


UPDATE title
SET entry_name='[Recueil iconographique. Rue Croix des Petits Champs (Paris)]'
WHERE title.id_uuid = 'qr1194396bc612f405c8e9f85b05ad4bae5';


UPDATE title
SET entry_name='[Recueil iconographique. Rue de Valois (Paris)]'
WHERE title.id_uuid = 'qr1de27b3c7d1f94faba89ce782d763e6e0';


UPDATE title
SET entry_name='L''agression de la rue de la Feuillade.'
WHERE title.id_uuid = 'qr13dd408db015042758a2030a5f2513e08';


UPDATE title
SET entry_name='[Paris, rue de la Feuillade. Cartes postales]'
WHERE title.id_uuid = 'qr10de12784ea024f78bfacfe2074de043b';


UPDATE title
SET entry_name='Etablissement Duval Montesquieu - 6 rue de Montesquieu'
WHERE title.id_uuid = 'qr1fcb59ba2a30d42e496b9516434be4689';


UPDATE title
SET entry_name='Toilettes de bal des magasins du Coin de Rue - Modes d''hommes du Palais de l''industrie'
WHERE title.id_uuid = 'qr10c1a9def3be44805b3efaba4b75251b5';


UPDATE title
SET entry_name='Toilettes du Coin de Rue. - Porcelaines de Lahoche et Pannier'
WHERE title.id_uuid = 'qr109da2b611ab44d6484e673628e9cc247';


UPDATE title
SET entry_name='Modes de dame et de petite fille des Magasins du Coin-de-Rue. - Mode d''homme de Jules Dusautoy, chapeau de René pineau'
WHERE title.id_uuid = 'qr16950cbc80dec42eab132e723b4bb3fe7';


UPDATE title
SET entry_name='Exposition publique des étrennes, ouverture lundi 13 décembre [Noël 1875-étrennes 1876] : [catalogue commercial] / Maison du Pauvre Diable, 1 et 3 rue Montesquieu, rue Croix des Petits-Champs, 11, Paris'
WHERE title.id_uuid = 'qr11c3d20efe190460d8d6d79916e16e6a5';


UPDATE title
SET entry_name='[Portraits du Monde cycliste : René de Knyff]'
WHERE title.id_uuid = 'qr10b930adcb388484fa0eb13d3cfed73f1';


UPDATE title
SET entry_name='Toilettes de la M[ais]on GAY, 2 rue de la Vrillière'
WHERE title.id_uuid = 'qr1721dfc7e6d7e46b3abc55f5060e67338';


UPDATE title
SET entry_name='Toilettes de la Maison Gay fils, 2 rue de la Vrillière ; Chapeau et coiffure de Mme Aubert, 46 faubourg Poissonière ; Jupons et Ganterie de la Vénitienne, 62 chaussé d''Antin'
WHERE title.id_uuid = 'qr173a8167b256b4f42bbfbf32aebd64345';


UPDATE title
SET entry_name='Dans la cité souterraine de l''or'
WHERE title.id_uuid = 'qr13d74f25668874d1a8bccad89a7547bfc';


UPDATE title
SET entry_name='Les Maîtres de l''affiche, 1896'
WHERE title.id_uuid = 'qr12dda5ec3989c4f988433613788ce66bb';


UPDATE title
SET entry_name='Catalogue du 33e salon annuel, 8 février au 9 mars 1914, Paris, Grand Palais / Union des femmes peintres et sculpteurs'
WHERE title.id_uuid = 'qr104ad933bcf8a425ba1e2aa391c2d5140';


UPDATE title
SET entry_name='Société des artistes français, catalogue illustré du Salon de 1914'
WHERE title.id_uuid = 'qr13325e05aa11a494388861b26c880e163';


UPDATE title
SET entry_name='Reçu pour un abonnement de trois mois au Constitutionnel, 1853'
WHERE title.id_uuid = 'qr16ad04e4c84e74e16ba3740a748a1d664';


UPDATE title
SET entry_name='Lefranc & Cie, 18 rue de Valois'
WHERE title.id_uuid = 'qr1d0e70f0d1ebb4c1e846a19af69d135e8';


UPDATE title
SET entry_name='Lefranc & Cie, Paris, 18 rue de Valois'
WHERE title.id_uuid = 'qr1ec139647d9ff4322a2ca5435c63a8444';


UPDATE title
SET entry_name='Lefranc & Cie, Paris, 18 rue de Valois'
WHERE title.id_uuid = 'qr113bf3ce1ebea4a99a6ebfcc19896681d';


UPDATE title
SET entry_name='Edmond Gillet [Régent de la Banque de France]'
WHERE title.id_uuid = 'qr187383e393e884e59a4fcf6074f6edfab';


UPDATE title
SET entry_name='Grand Bouillon-Restaurant, Ancienne Maison Charpentier, Cotel puis Ribordy - 6 place de Valois'
WHERE title.id_uuid = 'qr1aabaf48178f24bd58741db4f83919378';


UPDATE title
SET entry_name='Banque de France'
WHERE title.id_uuid = 'qr1eb150a7ad57c4846a4e952578a1e1687';


UPDATE title
SET entry_name='[Milord l''Arsouille, cabaret 5 rue de Beaujolais]'
WHERE title.id_uuid = 'qr1eb7b0b70c2464370a592184147a5c11f';


UPDATE title
SET entry_name='Association de l''apprentissage dans l''industrie de la fourrure'
WHERE title.id_uuid = 'qr10f491e1a6cfb4299a9b6ddf90566fc31';


UPDATE title
SET entry_name='Élévation d''une salle d''Opéra projetée sur le terrain compris entre le Palais Royal et la rue des Bons Enfants'
WHERE title.id_uuid = 'qr13156170ec5b547af950adc6dd510302d';


UPDATE title
SET entry_name='|Postiches Odiva, 7 place de Valois]'
WHERE title.id_uuid = 'qr14ea057affdce481fb9b5e7c394500435';


UPDATE title
SET entry_name='[Beaux-Arts. Établissenemtns Lefranc, maison fondée en 1770]'
WHERE title.id_uuid = 'qr19c1f05d337c349eda9c7dc2b95c16113';


UPDATE title
SET entry_name='Fabrique de couleurs & vernis, couleurs fines et matériel pour la peinture à l''huile, Lefranc, 15 rue de la Ville l''Evêque, Paris VIIIe, 1934 : [catalogue commercial]'
WHERE title.id_uuid = 'qr11883ed1c4d8f4fb09643575bdd7ae6b4';


UPDATE title
SET entry_name='Paris (1908), Hôtel de Liancourt, 21 Rue des Bons Enfants (XXe arrt) : [porte]'
WHERE title.id_uuid = 'qr188dfaa531c654a1b843fffa743a3c7e0';


UPDATE title
SET entry_name='Paris (1908), Ancien Hôtel de Courville, 31 Rue des Bons Enfants (XXe arrt) : [façade et enseignes]'
WHERE title.id_uuid = 'qr1966da6da16174488ba47c03765331189';


UPDATE title
SET entry_name='Vieux Paris (1908), Hôtel de la Chancellerie d''Orléans, sur la Rue de Valois (Ier arrt) : [façade et enseigne]'
WHERE title.id_uuid = 'qr1961eaf2c833d49c1993e120ee2e07404';


UPDATE title
SET entry_name='[Bains, rue Montesquieu]'
WHERE title.id_uuid = 'qr1063b7ebf116f4622989372dda88a8ffd';


UPDATE title
SET entry_name='Marque du graveur en taille-douce Huyot (Frédéric), né en 1808, et qui fut le graveur attitré de la Banque de France'
WHERE title.id_uuid = 'qr12392c1c8556442d3b5a0e6223883d728';


UPDATE title
SET entry_name='[Galerie dorée de la Banque de France, à Paris]'
WHERE title.id_uuid = 'qr1efe65a9c386140cb8b5324ec69ff3d6f';

COMMIT;