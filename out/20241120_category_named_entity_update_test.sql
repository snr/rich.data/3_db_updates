-- 1st transaction: create column
 BEGIN;


ALTER TABLE IF EXISTS named_entity ADD COLUMN IF NOT EXISTS category TEXT, ADD COLUMN IF NOT EXISTS category_slug TEXT;


COMMIT;

-- 2nd transaction: import data
 BEGIN;

UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr12adfd0faf2d446eca4c303b4946ae34d';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr12adfd0faf2d446eca4c303b4946ae34d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1463ba6fade0b49e494a394567d678560';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1463ba6fade0b49e494a394567d678560';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1731c3a506f3f475580115bd9f30bf94f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1731c3a506f3f475580115bd9f30bf94f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1e85591a1216c46d99713897030ecd952';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1e85591a1216c46d99713897030ecd952';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a52ab8323fe241cd979aa4d26921a3bd';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a52ab8323fe241cd979aa4d26921a3bd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a778e7eea76d4788bbc5f2c71b9c4039';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a778e7eea76d4788bbc5f2c71b9c4039';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr120820310f39441619d9cd25bcf94f286';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr120820310f39441619d9cd25bcf94f286';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1de725623ef314865908d69b623790d11';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1de725623ef314865908d69b623790d11';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14b81819d17de4698b8ffa371ee6cbcbd';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14b81819d17de4698b8ffa371ee6cbcbd';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c3753e975d46454793262b3024dc823d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c3753e975d46454793262b3024dc823d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr165f1bc312cd141b4939065b1417fd6af';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr165f1bc312cd141b4939065b1417fd6af';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1392e4424e6a14c18a0a6828555c1d8cc';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1392e4424e6a14c18a0a6828555c1d8cc';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1af7fbd99ff074b8fa15647e2d3e0390f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1af7fbd99ff074b8fa15647e2d3e0390f';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1fa2d2c39659444efa6460d7942a0a48b';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1fa2d2c39659444efa6460d7942a0a48b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr193220373d5954434b85c06265d5b6fc9';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr193220373d5954434b85c06265d5b6fc9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15fdcc0a5e76646e59ec9b40ff28bfa75';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15fdcc0a5e76646e59ec9b40ff28bfa75';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1205e44e43b774057b49a9a3803771807';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1205e44e43b774057b49a9a3803771807';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1ade7cadcd7cc48dea940ab85e5a898bb';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1ade7cadcd7cc48dea940ab85e5a898bb';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr15aadf25251654d14b2f021e59e92abcd';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr15aadf25251654d14b2f021e59e92abcd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15083e96ba78f4247a1a39008600e12d5';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15083e96ba78f4247a1a39008600e12d5';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr172114afaaa5141ffa499e28cfa9cc639';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr172114afaaa5141ffa499e28cfa9cc639';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11dd4312fcdf44fb2bd5a227ec32601f1';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11dd4312fcdf44fb2bd5a227ec32601f1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1682860b804264cc8b1605c62861b3e64';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1682860b804264cc8b1605c62861b3e64';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr14ca3021c7a0d4c1c8bb0a7a41d22efcd';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr14ca3021c7a0d4c1c8bb0a7a41d22efcd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17480468e62344531bba76db7ad07590e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17480468e62344531bba76db7ad07590e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12e5d14e3651a486c878be5025873b686';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12e5d14e3651a486c878be5025873b686';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr182e068ae13674222ae2d9c1be46aca1b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr182e068ae13674222ae2d9c1be46aca1b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr18e709f59e98043758dd6e9ceb07a4723';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr18e709f59e98043758dd6e9ceb07a4723';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1cd8d802ee22b4f25873d15a3cd6cfc40';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1cd8d802ee22b4f25873d15a3cd6cfc40';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1492024225ade42799bbc3ba90c52f302';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1492024225ade42799bbc3ba90c52f302';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1c1d1d136d1b54c89ab5f09984bf0476e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1c1d1d136d1b54c89ab5f09984bf0476e';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1f036d029d1684fa18aae6d392e42acff';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1f036d029d1684fa18aae6d392e42acff';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1d072e74c1664477999ad1a3a8d4048af';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1d072e74c1664477999ad1a3a8d4048af';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1e15e00b392e842caaa4b8e3507a4af70';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1e15e00b392e842caaa4b8e3507a4af70';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr145b6728da02343779784d42e674b73e3';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr145b6728da02343779784d42e674b73e3';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr192bbf6790bb84966b8242cd7a7a9116a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr192bbf6790bb84966b8242cd7a7a9116a';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr11382193cbbf74e06a50bf0dcf129919b';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr11382193cbbf74e06a50bf0dcf129919b';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1dead68b23b774f09b19177822a0e6886';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1dead68b23b774f09b19177822a0e6886';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr108ea36ce1a3948328471a019b277b534';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr108ea36ce1a3948328471a019b277b534';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr134389b763b2444a0826e21c28640f80c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr134389b763b2444a0826e21c28640f80c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr13c8218089fdb4839a1dc85e686fc8a85';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr13c8218089fdb4839a1dc85e686fc8a85';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11de716a8b88047ad95e9ec92c0bcf668';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11de716a8b88047ad95e9ec92c0bcf668';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c1e1954ea39e4ca0a9542ea6ab0eaaeb';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c1e1954ea39e4ca0a9542ea6ab0eaaeb';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19468ecb90e3b439bad8916833c998173';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19468ecb90e3b439bad8916833c998173';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14b99a23f753e409287929caf2c08c749';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14b99a23f753e409287929caf2c08c749';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr10b53e9ed04794987af200fbe697a508d';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr10b53e9ed04794987af200fbe697a508d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1af2b9f769b1e43278fffd407360550ef';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1af2b9f769b1e43278fffd407360550ef';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1a744d795cdf8429cb49e149fd21ca622';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1a744d795cdf8429cb49e149fd21ca622';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr106fe2c565a1b47fca850d3dfa024365d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr106fe2c565a1b47fca850d3dfa024365d';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1499127adf74d4e81bf87b7d244f44ccb';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1499127adf74d4e81bf87b7d244f44ccb';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr17be72ca7578e42989187858c66c7ef2f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr17be72ca7578e42989187858c66c7ef2f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12a14b6adc2354c1e9685764b9a3fd938';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12a14b6adc2354c1e9685764b9a3fd938';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1f0c6863d4ee54fa69f205d211b5c07a1';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1f0c6863d4ee54fa69f205d211b5c07a1';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr109d68639dbe148e69ab46acdce41321a';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr109d68639dbe148e69ab46acdce41321a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr117a9192dd9034ed49481431cf76e94a3';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr117a9192dd9034ed49481431cf76e94a3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1ca7324a9b8a14952b56bb898a8bfcc4d';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1ca7324a9b8a14952b56bb898a8bfcc4d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1cbc001b3f3424221afac3d7fa7bbe410';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1cbc001b3f3424221afac3d7fa7bbe410';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a1a53faa053745c89c5e35ddf67e3469';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a1a53faa053745c89c5e35ddf67e3469';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr10fb261dc4a3145228e84274d9c011e21';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr10fb261dc4a3145228e84274d9c011e21';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c156ba5fdcef459c86795bdb02c3aa91';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c156ba5fdcef459c86795bdb02c3aa91';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c63c52d635b24d18b76af2236c2e0fed';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c63c52d635b24d18b76af2236c2e0fed';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c4fc6e2a9b924c63a4fbd83ad6e4a294';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c4fc6e2a9b924c63a4fbd83ad6e4a294';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e53d8b3364f44547a17615a652e3f047';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e53d8b3364f44547a17615a652e3f047';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11e2606eb75794872b55831bace185b9d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11e2606eb75794872b55831bace185b9d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d6e593cb5c0f46a6bbb3856028cfa1bc';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d6e593cb5c0f46a6bbb3856028cfa1bc';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11eb428deabc547be8635fa52adc2ac8c';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11eb428deabc547be8635fa52adc2ac8c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1c5d62f3c2a6a47548a1cdb528a22fb91';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1c5d62f3c2a6a47548a1cdb528a22fb91';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr117a65d29160b4d73b81fdb4fbbf48c54';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr117a65d29160b4d73b81fdb4fbbf48c54';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr111ad4788fa9b4c20b04234e2ca757e6b';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr111ad4788fa9b4c20b04234e2ca757e6b';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1cc142321723c4006a7e996c83ce1b08b';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1cc142321723c4006a7e996c83ce1b08b';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr10b286549855142db89df9e2b5d0c71e5';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr10b286549855142db89df9e2b5d0c71e5';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1dafd5f322a154044bca339492ac467ef';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1dafd5f322a154044bca339492ac467ef';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1062f44ad73b345eaaaad280add7203e7';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1062f44ad73b345eaaaad280add7203e7';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr18699ac154653448b90b8be8ea4d0b3d6';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr18699ac154653448b90b8be8ea4d0b3d6';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr141c0cab5bbde4cf694ae45ba35388860';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr141c0cab5bbde4cf694ae45ba35388860';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10b6654bb640a421a8beb6e4e332475d7';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10b6654bb640a421a8beb6e4e332475d7';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1658d5e18ccfa46ef88889ea9ad6da2c8';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1658d5e18ccfa46ef88889ea9ad6da2c8';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr11d846a564ec5459998cdeca193b1356b';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr11d846a564ec5459998cdeca193b1356b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr152b925599abd428e8af1d1453e4b3e35';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr152b925599abd428e8af1d1453e4b3e35';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1779b31a5f19344358fb02a6532b6c52c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1779b31a5f19344358fb02a6532b6c52c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11830adf3548c44f195e7c2a8af7d5c3f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11830adf3548c44f195e7c2a8af7d5c3f';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1deaa718ed3024933abb7b422d8d0cefc';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1deaa718ed3024933abb7b422d8d0cefc';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1119d97a822db4b48b0318b220241ab86';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1119d97a822db4b48b0318b220241ab86';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1c9e088e95d9242d8b603c45ce370014b';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1c9e088e95d9242d8b603c45ce370014b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr125b15a21be3c48e08af565ada921988a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr125b15a21be3c48e08af565ada921988a';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr19557b0b4077f4f0287bd0d32e87b4d04';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr19557b0b4077f4f0287bd0d32e87b4d04';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ce407cce2f294345a8492999606b4509';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ce407cce2f294345a8492999606b4509';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr144a5b900427142e9af8e06b7eec70226';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr144a5b900427142e9af8e06b7eec70226';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr18fc58affa80644ea9dc18d025a91f4cc';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr18fc58affa80644ea9dc18d025a91f4cc';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c72e350637a746b5adcdc760d7af1754';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c72e350637a746b5adcdc760d7af1754';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1fc75e7b747414646876e9bb19c94a7b5';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1fc75e7b747414646876e9bb19c94a7b5';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1468925564786468d9de0a7cc9dceb2e4';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1468925564786468d9de0a7cc9dceb2e4';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr121aa7c8f0a8a4c17a31228fb97e465c6';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr121aa7c8f0a8a4c17a31228fb97e465c6';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1141b5ea84e0a498a9193eb06cce2d33e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1141b5ea84e0a498a9193eb06cce2d33e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr125c2948f2b1f440382f63c915e0584b9';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr125c2948f2b1f440382f63c915e0584b9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d1ea1d26dfb64bd1a697125eea661b68';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d1ea1d26dfb64bd1a697125eea661b68';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr136fe2d20e38f420c9de7a40dba579ef0';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr136fe2d20e38f420c9de7a40dba579ef0';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1d71227fdf8fa4a4cb590d0ccc7a829f4';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1d71227fdf8fa4a4cb590d0ccc7a829f4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14811624e07514778beb95576fa508ca9';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14811624e07514778beb95576fa508ca9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr116ebc70a23a840c7a0a75bbac04d24c2';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr116ebc70a23a840c7a0a75bbac04d24c2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr121a58b4015694d969e33e8eaedc8fcaf';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr121a58b4015694d969e33e8eaedc8fcaf';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10e34f03c491545ffb7f791af3c495ce2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10e34f03c491545ffb7f791af3c495ce2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ab8289afbdbd4e91941966aa4a90f5e2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ab8289afbdbd4e91941966aa4a90f5e2';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr150389b9cca99407ea81ae4cea5ae3a1f';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr150389b9cca99407ea81ae4cea5ae3a1f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr18b65296405da4680a68ed815b939b16d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr18b65296405da4680a68ed815b939b16d';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr120d0a7f669b44ee0a3809755708ef475';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr120d0a7f669b44ee0a3809755708ef475';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11acfcb48f0aa40f2a3ed097f5f0e28b9';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11acfcb48f0aa40f2a3ed097f5f0e28b9';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr147c443d7cdd14b1fbdb19ef7a4972e64';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr147c443d7cdd14b1fbdb19ef7a4972e64';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1972a00b0edaf4af785227935d9916cb1';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1972a00b0edaf4af785227935d9916cb1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1ec77c4164a444eac870ca36653967b8a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1ec77c4164a444eac870ca36653967b8a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11c42a2ccba3044a2af52abc0a5e9c716';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11c42a2ccba3044a2af52abc0a5e9c716';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1e9d10af999ce461a9885d5351437ab1f';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1e9d10af999ce461a9885d5351437ab1f';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr17da5e13696724beda0e7f46334b1ba56';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr17da5e13696724beda0e7f46334b1ba56';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr121834c8f172f43528328f76b6afe3c48';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr121834c8f172f43528328f76b6afe3c48';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr12193b5873d604d0b94861ffd3aa5ef52';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr12193b5873d604d0b94861ffd3aa5ef52';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr139206806544b4270ae0741ca5b07614b';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr139206806544b4270ae0741ca5b07614b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10d5df22dc23f4184aa8b382617fe1637';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10d5df22dc23f4184aa8b382617fe1637';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr139cc5f1156e843fa9fe5c201b710c300';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr139cc5f1156e843fa9fe5c201b710c300';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11cc22c2747d04fa2a3aff3aec42fcc5f';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11cc22c2747d04fa2a3aff3aec42fcc5f';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr14d6a15f47a294b348f8c3c2639738103';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr14d6a15f47a294b348f8c3c2639738103';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1f56dd82ee1dc4e58a55885bd8dbd968f';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1f56dd82ee1dc4e58a55885bd8dbd968f';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr115715cd7b92e458e8a24ac9c98affb4c';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr115715cd7b92e458e8a24ac9c98affb4c';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr14e8cb5165012410fb0278fef427396ca';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr14e8cb5165012410fb0278fef427396ca';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr14f6527aae2884b918122a967f14575c7';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr14f6527aae2884b918122a967f14575c7';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr16560a0d286cb4980a056ab771a38e8a1';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr16560a0d286cb4980a056ab771a38e8a1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1b38123464bb34e77a63cea006da0322d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1b38123464bb34e77a63cea006da0322d';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1a230eacc6f2e480ba095e6d177fb6ba4';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1a230eacc6f2e480ba095e6d177fb6ba4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18a5fdd24b31b46ed933f1006a260969b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18a5fdd24b31b46ed933f1006a260969b';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1cb3cf4598fb546fba5ed971b479d2dc0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1cb3cf4598fb546fba5ed971b479d2dc0';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1489090204fff44bfb2c3a650cf212349';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1489090204fff44bfb2c3a650cf212349';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1e64acccdc7e74f1d9b9d5ce1c6cdefc8';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1e64acccdc7e74f1d9b9d5ce1c6cdefc8';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1ad0012875b7744beb79ec9c68cb204f4';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1ad0012875b7744beb79ec9c68cb204f4';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1b37647c80e0741b68f0acbcc563e6344';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1b37647c80e0741b68f0acbcc563e6344';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr15d13e0858cda40ebb09471e16746a47e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr15d13e0858cda40ebb09471e16746a47e';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr10d597d7ecd9c4a6fb0339b5862dd0d2f';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr10d597d7ecd9c4a6fb0339b5862dd0d2f';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr19b3b1dd697d74261bfdae2785647ad32';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr19b3b1dd697d74261bfdae2785647ad32';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr185a1c34b9fac460e8de574b5773bbb0b';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr185a1c34b9fac460e8de574b5773bbb0b';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr17154641b26144a65aef84d6a14bb5765';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr17154641b26144a65aef84d6a14bb5765';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d4dd01cdd58849cd83674914c01d464e';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d4dd01cdd58849cd83674914c01d464e';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr11e82362eec5443e6aaf9cf15d2486f2a';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr11e82362eec5443e6aaf9cf15d2486f2a';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1104a85d1ebea4c73a56eec0c5690adea';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1104a85d1ebea4c73a56eec0c5690adea';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1df5e2467bcc74f0bb959074dbd6eeb5a';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1df5e2467bcc74f0bb959074dbd6eeb5a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17917f21a8df349d7bac5c7016688b042';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17917f21a8df349d7bac5c7016688b042';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1dbbbaf64d77945a5a3644f3e2eb3ad5a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1dbbbaf64d77945a5a3644f3e2eb3ad5a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr199320064d0364e8e9fa8f56c3205be53';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr199320064d0364e8e9fa8f56c3205be53';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr10b06bf386f95409d8030ac4f308fb8bf';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr10b06bf386f95409d8030ac4f308fb8bf';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr15ef31e6222f74feb91e69c89c36d3d92';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr15ef31e6222f74feb91e69c89c36d3d92';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1abbec32a0a984c1b8390b0aaae57eeab';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1abbec32a0a984c1b8390b0aaae57eeab';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr18ad35fe796fe4442bff20d993c47245a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr18ad35fe796fe4442bff20d993c47245a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr19a45f9cc427446c5bd00e921a7b1dc80';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr19a45f9cc427446c5bd00e921a7b1dc80';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr186d6dd078ab543da80fcb088651df526';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr186d6dd078ab543da80fcb088651df526';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr14adfd0aaf343408e9c2fd5773a17cc50';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr14adfd0aaf343408e9c2fd5773a17cc50';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1dec3cee8fa4348ca9a4100bc641c1209';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1dec3cee8fa4348ca9a4100bc641c1209';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr137c663e5d63641acbea9aa09e8e97e7c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr137c663e5d63641acbea9aa09e8e97e7c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1bba7ccc7a26f4b3f939426893b6a1462';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1bba7ccc7a26f4b3f939426893b6a1462';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr14d5707fc95bc43c8b4646a0276948748';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr14d5707fc95bc43c8b4646a0276948748';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1b9881459f96540749d3435cbad435a36';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1b9881459f96540749d3435cbad435a36';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1dc16a0443ce1422a92a149252b3db0d6';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1dc16a0443ce1422a92a149252b3db0d6';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16731dd1882af4779a07a5724210fa66f';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16731dd1882af4779a07a5724210fa66f';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr162421b3c1d5147178eed8419aff48567';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr162421b3c1d5147178eed8419aff48567';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1abaae7ae3c5c44859376339407ba5fb1';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1abaae7ae3c5c44859376339407ba5fb1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1c0d7d981377d49fcb77e2b5e605ee813';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1c0d7d981377d49fcb77e2b5e605ee813';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1dff813791aa449e0bd3632dd1c4bc1d2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1dff813791aa449e0bd3632dd1c4bc1d2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr139ebfbc64767402e9962b7e28b8cc983';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr139ebfbc64767402e9962b7e28b8cc983';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr13850c84fb9da42c291d05bc467d1d548';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr13850c84fb9da42c291d05bc467d1d548';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr17a3600a51c2d4ef0a03a4aebb8d57094';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr17a3600a51c2d4ef0a03a4aebb8d57094';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e2bb3097f665458f9ccf4ef51a052e90';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e2bb3097f665458f9ccf4ef51a052e90';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14cde3539c06a496d879beccb1366aa70';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14cde3539c06a496d879beccb1366aa70';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr13ac50a17e43b4ce8ae52b858debfebfd';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr13ac50a17e43b4ce8ae52b858debfebfd';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c433a6c913ac477e82b7a4897396cbb1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c433a6c913ac477e82b7a4897396cbb1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr101ce3b99b6804a74a96cd819012fb2ad';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr101ce3b99b6804a74a96cd819012fb2ad';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18251621d7e3c4f38bd8084aa6ac8a546';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18251621d7e3c4f38bd8084aa6ac8a546';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1630ea47fc3d64bddb57e61eb54a7aab8';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1630ea47fc3d64bddb57e61eb54a7aab8';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr132c6b5ff5e744eb28f67fb2d5bdd49e0';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr132c6b5ff5e744eb28f67fb2d5bdd49e0';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr119272002aaa04a239722b1f7f1308ab9';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr119272002aaa04a239722b1f7f1308ab9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1316bf10b6b794fe289724fe2d54596eb';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1316bf10b6b794fe289724fe2d54596eb';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr18cdd0ba6853f4447919fb0239e6d1c3e';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr18cdd0ba6853f4447919fb0239e6d1c3e';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr14de60c1ab8024f129997d33b7522c4b9';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr14de60c1ab8024f129997d33b7522c4b9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1186f5986da3640e89f4b8e2f8463bc33';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1186f5986da3640e89f4b8e2f8463bc33';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr150da9e2fa51546cea01a799808fb459b';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr150da9e2fa51546cea01a799808fb459b';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1e19c771d228842edb718a998ba1e8ec5';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1e19c771d228842edb718a998ba1e8ec5';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1ca69443495d040c59d2cc972a9b8b94e';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1ca69443495d040c59d2cc972a9b8b94e';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1551f82e887624e9ba097ad73208d04b6';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1551f82e887624e9ba097ad73208d04b6';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14ac92a021f164345b29f3f94cce12773';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14ac92a021f164345b29f3f94cce12773';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1735a2295073241a1bb51818b93fe6e6e';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1735a2295073241a1bb51818b93fe6e6e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19d6e89ed84cf45e7ab1f61cbd5cca878';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19d6e89ed84cf45e7ab1f61cbd5cca878';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1692c88b2159942b49c778c85f0ca8ec4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1692c88b2159942b49c778c85f0ca8ec4';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr117a5683d8d1d40c0aeee20e4aed19172';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr117a5683d8d1d40c0aeee20e4aed19172';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1cfb2cc62f91448458ab0af2bc9708c02';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1cfb2cc62f91448458ab0af2bc9708c02';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15a521fdb71dd4e12844ea1b08ac0e0b0';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15a521fdb71dd4e12844ea1b08ac0e0b0';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr11f24ce03d81346ea8d426af8215308da';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr11f24ce03d81346ea8d426af8215308da';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1b935f65d857f42548046a71c31d61498';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1b935f65d857f42548046a71c31d61498';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr185d27b9c80b244b89fe2ef1d2cf12a53';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr185d27b9c80b244b89fe2ef1d2cf12a53';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr191479b68856e4ff2bbf0dc2ff5b97b27';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr191479b68856e4ff2bbf0dc2ff5b97b27';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c839b9494be24322a4659b696ad6e0f6';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c839b9494be24322a4659b696ad6e0f6';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1fbab1c6f208148af8610d1e65cf5891c';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1fbab1c6f208148af8610d1e65cf5891c';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr10f3c108c2a434f16a00b489f89d3a819';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr10f3c108c2a434f16a00b489f89d3a819';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1cfc5a00ac7274c18a66668136855638b';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1cfc5a00ac7274c18a66668136855638b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1db48b6ef2a5348c895f8d764523e06da';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1db48b6ef2a5348c895f8d764523e06da';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr125aa01e664d14d828b7646a5c6822791';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr125aa01e664d14d828b7646a5c6822791';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e406e46fc1984ebea032a05de9dc5da3';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e406e46fc1984ebea032a05de9dc5da3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14911d5d33e8e4bb38d49d29097d6e03e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14911d5d33e8e4bb38d49d29097d6e03e';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr18ff2821302b543bf9a60040698e8305d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr18ff2821302b543bf9a60040698e8305d';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr1277bee07a0094c13943883db21c4f50d';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr1277bee07a0094c13943883db21c4f50d';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1101c0f9faac54436813be98def738e7c';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1101c0f9faac54436813be98def738e7c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1498f4c670a9b40bb8fcdf2fb991a2bee';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1498f4c670a9b40bb8fcdf2fb991a2bee';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr147934730743744809d3aa9fa732988d2';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr147934730743744809d3aa9fa732988d2';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1a2753e5cb0494e9b8359477b6c872948';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1a2753e5cb0494e9b8359477b6c872948';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13f570153250e4d37a8b28bf8b1f3eb5a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13f570153250e4d37a8b28bf8b1f3eb5a';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr17d8e83877a8f43a4ae7105be2e99a17d';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr17d8e83877a8f43a4ae7105be2e99a17d';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr15d3d472571374c6b939c2b04fd138142';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr15d3d472571374c6b939c2b04fd138142';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19cba4865f07f44f78c9b65cb64179173';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19cba4865f07f44f78c9b65cb64179173';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1cef37682ecab4d5d85a0d199d5f760f4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1cef37682ecab4d5d85a0d199d5f760f4';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr10680d4144f1b493887b5a47c58785374';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr10680d4144f1b493887b5a47c58785374';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1cf1ea28a7dfb48b08bfebef04675363b';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1cf1ea28a7dfb48b08bfebef04675363b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c83cc8fd408c4ceb96ac25a820a63e1b';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c83cc8fd408c4ceb96ac25a820a63e1b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1ee729868f8414b28b01ba1ed0394ac63';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1ee729868f8414b28b01ba1ed0394ac63';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1105ec8ce6a0847e4b6141dba098593c5';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1105ec8ce6a0847e4b6141dba098593c5';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr179a96ef2ed1c44a0b5e9acdd55a9a1de';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr179a96ef2ed1c44a0b5e9acdd55a9a1de';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr180c958178c4a4b1eae86f33fe5c3fc87';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr180c958178c4a4b1eae86f33fe5c3fc87';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1c28afc2cbec84fd696a698925291f137';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1c28afc2cbec84fd696a698925291f137';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr151adec89990d4cbf8929f3e99f4bb875';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr151adec89990d4cbf8929f3e99f4bb875';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e93c3a75895c4e2ead53d5bafb3f0aaf';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e93c3a75895c4e2ead53d5bafb3f0aaf';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr16d1c234524634409a67b10693522b63c';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr16d1c234524634409a67b10693522b63c';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr13d9d36d1dbce4058a8df083a59331331';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr13d9d36d1dbce4058a8df083a59331331';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1f3102a0e53c442d0beb05ef6ac182e89';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1f3102a0e53c442d0beb05ef6ac182e89';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14eab6143d69141c38144a4f02c5296d4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14eab6143d69141c38144a4f02c5296d4';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr155a7b39698294fdc9b5e7add36697137';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr155a7b39698294fdc9b5e7add36697137';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c5c39cce1c7846dfab0a6369417a8938';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c5c39cce1c7846dfab0a6369417a8938';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1a3231b26cc874d65ae92c67a29e1e984';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1a3231b26cc874d65ae92c67a29e1e984';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1ba4e5df94f0f40c3b7ac4e30cf52247e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1ba4e5df94f0f40c3b7ac4e30cf52247e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18abbcfc68acf47c3bae109809c7ee1d8';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18abbcfc68acf47c3bae109809c7ee1d8';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1031b54433e064e6cb732fd9ca515be49';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1031b54433e064e6cb732fd9ca515be49';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a2b9cb949bbc45328a257102942d4788';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a2b9cb949bbc45328a257102942d4788';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1db77525eef5e4fb8a3a90c65afc388ee';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1db77525eef5e4fb8a3a90c65afc388ee';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr193bc71bc3de944fa9008a3abb12d335f';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr193bc71bc3de944fa9008a3abb12d335f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1edf8e7c26cc54fd1a8834103faf996b5';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1edf8e7c26cc54fd1a8834103faf996b5';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18ed1c4efa9944b07a4f1f8921768f60f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18ed1c4efa9944b07a4f1f8921768f60f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1c3c40a0c818b4643b6bdd41e239e547f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1c3c40a0c818b4643b6bdd41e239e547f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18a6eecbf630440eca03e06985d9e7983';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18a6eecbf630440eca03e06985d9e7983';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19f19e5e84a3644ec83359ac0372451c5';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19f19e5e84a3644ec83359ac0372451c5';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13487b91ac17c41729c82e22858c30595';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13487b91ac17c41729c82e22858c30595';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14a3fa4c1024f42c8a43ce52da8289b04';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14a3fa4c1024f42c8a43ce52da8289b04';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17e497bba3be44923ab85f7e526790888';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17e497bba3be44923ab85f7e526790888';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b103f43dc050413f8ed9cc530b2afb45';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b103f43dc050413f8ed9cc530b2afb45';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1592dfd8723b24156b69ff07746238d20';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1592dfd8723b24156b69ff07746238d20';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1be9b8b69416a4ec2940734cd69e51bc3';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1be9b8b69416a4ec2940734cd69e51bc3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b332b36b66d5465f9ed406e2e9d509b7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b332b36b66d5465f9ed406e2e9d509b7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr197144db0d22c4d049ca1bd132e6d4db1';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr197144db0d22c4d049ca1bd132e6d4db1';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1356cd982a1d0442c8060dd08c562e423';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1356cd982a1d0442c8060dd08c562e423';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr102c618cadcb147a8b83d483c5e1376a3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr102c618cadcb147a8b83d483c5e1376a3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr195c7efd41a8b4d02975692ae1c2973ed';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr195c7efd41a8b4d02975692ae1c2973ed';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1db668bc6ffaa464a83fee63c719dc49a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1db668bc6ffaa464a83fee63c719dc49a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1800917f3d3b84f4f8384506b670e72d8';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1800917f3d3b84f4f8384506b670e72d8';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16ba94dcfdd7e4d14b320ae5d3c17ac07';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16ba94dcfdd7e4d14b320ae5d3c17ac07';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f0e05f1f7a074f13b5aba3e8efc7f15f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f0e05f1f7a074f13b5aba3e8efc7f15f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12b6c17826f024bd99f5ff858f737bc39';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12b6c17826f024bd99f5ff858f737bc39';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fa53b3995f7348f286b6995e663245a3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fa53b3995f7348f286b6995e663245a3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15dbc3184c9924c61b9df81012bcfbe1a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15dbc3184c9924c61b9df81012bcfbe1a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr197e0bc6832464af499ecf39babe2b8ae';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr197e0bc6832464af499ecf39babe2b8ae';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19a47df3b9ed8481e82066661158e0912';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19a47df3b9ed8481e82066661158e0912';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b147d59c471243dc966d3a45bbab424d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b147d59c471243dc966d3a45bbab424d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1945d6de9396d403382d5d9e4a1ae0c5b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1945d6de9396d403382d5d9e4a1ae0c5b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ead88e6f2a134b3a9671a95a7c5b1d95';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ead88e6f2a134b3a9671a95a7c5b1d95';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d6d5171370e944f89c03828635ced50e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d6d5171370e944f89c03828635ced50e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f138c0548d994785b0c20e9b279b93c5';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f138c0548d994785b0c20e9b279b93c5';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1364958248e0e4359a8f5f3763fbed3c2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1364958248e0e4359a8f5f3763fbed3c2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr122582558fea2497689b6a4fd3a5be935';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr122582558fea2497689b6a4fd3a5be935';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18e60e6b765874fb392c907f3473c0ab7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18e60e6b765874fb392c907f3473c0ab7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17930c6fd341b44538955a48868d8fec0';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17930c6fd341b44538955a48868d8fec0';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13c663b7bc56e492f8b81e6390a0583d9';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13c663b7bc56e492f8b81e6390a0583d9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1af0c9ec3116749d280925c68756d8a24';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1af0c9ec3116749d280925c68756d8a24';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1854234cb0b804661af5207b11c391c2d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1854234cb0b804661af5207b11c391c2d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr106d1ac4df18d49a5a4b4e616238dd323';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr106d1ac4df18d49a5a4b4e616238dd323';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e860b1c0050742b686c3020a270055e8';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e860b1c0050742b686c3020a270055e8';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16de776b81f2f47c8b115d4c8171ab085';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16de776b81f2f47c8b115d4c8171ab085';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1b742fec6e13f4d07a03e53170b87eca3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1b742fec6e13f4d07a03e53170b87eca3';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1fa3d288c80494ef781ed18b92e58b3d2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1fa3d288c80494ef781ed18b92e58b3d2';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr172575632f70f494ab5502c04255dbfcb';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr172575632f70f494ab5502c04255dbfcb';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr147350e78be1b4b2fa32d9c0c9b4ef12d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr147350e78be1b4b2fa32d9c0c9b4ef12d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr13a489fd8e7754edfb58a20e4022e3ac6';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr13a489fd8e7754edfb58a20e4022e3ac6';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1117ea161d2a64de584d028226b9fe68e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1117ea161d2a64de584d028226b9fe68e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr166d2587f1e9b48d992799bc4ea522e9a';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr166d2587f1e9b48d992799bc4ea522e9a';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1f8a0afddec8b48a094f22de547024c8c';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1f8a0afddec8b48a094f22de547024c8c';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr10d39106155244070a3881698c741276b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr10d39106155244070a3881698c741276b';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr186f8ae24ff334be28337608c9538def4';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr186f8ae24ff334be28337608c9538def4';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1794b87ee70144a27be837d7ae22089ad';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1794b87ee70144a27be837d7ae22089ad';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr177299aa54550473a9f3b7140f61535b3';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr177299aa54550473a9f3b7140f61535b3';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1ecfa5440f33b4f8c9c600dfb4a16916f';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1ecfa5440f33b4f8c9c600dfb4a16916f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr181578e600e40460b8ab643ac094ddcda';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr181578e600e40460b8ab643ac094ddcda';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr10ec8e44c68e545f581ef643c6833cbf4';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr10ec8e44c68e545f581ef643c6833cbf4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1bc4d6d194a6d47e48c2aa01450cd4877';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1bc4d6d194a6d47e48c2aa01450cd4877';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1d9ea0fb3a45945aa8d728d1a7361de6d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1d9ea0fb3a45945aa8d728d1a7361de6d';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15d91ba8373d84832837afbb99d537f15';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15d91ba8373d84832837afbb99d537f15';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d2606944041947ebb5508a218b3b7fa9';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d2606944041947ebb5508a218b3b7fa9';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1476471b22d3a4750a4f08e51fffbcaaf';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1476471b22d3a4750a4f08e51fffbcaaf';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13eda1155091c4f308a3a7349db855955';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13eda1155091c4f308a3a7349db855955';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1fdb37bb0c58148c2b64191ce2af47887';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1fdb37bb0c58148c2b64191ce2af47887';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1081e1606f5674749bf824a768cb24668';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1081e1606f5674749bf824a768cb24668';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr17e0ddbb21d394105b4618fc2460e8654';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr17e0ddbb21d394105b4618fc2460e8654';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17407971be6d64efcad63e6bc5cb1b8a2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17407971be6d64efcad63e6bc5cb1b8a2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15251d6290a824999ae79d3822add6b3f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15251d6290a824999ae79d3822add6b3f';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1cc53415ebfdc405697c1486d47946b5e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1cc53415ebfdc405697c1486d47946b5e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr115d922085d454427b9df1340de6b2dc0';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr115d922085d454427b9df1340de6b2dc0';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1fe5b53685ff64eeca886dbe0304cf420';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1fe5b53685ff64eeca886dbe0304cf420';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr103d5cb9592a2441aa391bdcc70e2f34f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr103d5cb9592a2441aa391bdcc70e2f34f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fdae267c4bab419aaeec35310eff25dd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fdae267c4bab419aaeec35310eff25dd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18076fbca35354e16802b72c694453ef7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18076fbca35354e16802b72c694453ef7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d073eb1cbdeb491985d5923d3855e883';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d073eb1cbdeb491985d5923d3855e883';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1bf2417f404dd44d4b31be5fd27a6ca18';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1bf2417f404dd44d4b31be5fd27a6ca18';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr105f207caa72d4421a92661e5caf9bb07';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr105f207caa72d4421a92661e5caf9bb07';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr101755be598d4448c81615a66db9b36fe';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr101755be598d4448c81615a66db9b36fe';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr190e320d9ed494dec9bfef5319b464b8c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr190e320d9ed494dec9bfef5319b464b8c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ffeaacf992354153b9a8cd19ce61e92d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ffeaacf992354153b9a8cd19ce61e92d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10388c5d7264c47be88df2ad8b5518592';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10388c5d7264c47be88df2ad8b5518592';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19173b5f087c04c88a320f05322b3ab0e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19173b5f087c04c88a320f05322b3ab0e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10db1f8fe590c42c38483f5dce173afe9';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10db1f8fe590c42c38483f5dce173afe9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr115127b6509144f31945b963298de5437';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr115127b6509144f31945b963298de5437';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16b1aa158f1da4be3905b46380c8d25e0';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16b1aa158f1da4be3905b46380c8d25e0';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr139feae3357ab4911b934b7be525ffc15';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr139feae3357ab4911b934b7be525ffc15';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1c945933261d5484dab3654b33dc4b6b0';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1c945933261d5484dab3654b33dc4b6b0';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b59f9e3e593f45969dcb9768f49797d7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b59f9e3e593f45969dcb9768f49797d7';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr19ef2c195a782411792102c5ddf9fc0a4';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr19ef2c195a782411792102c5ddf9fc0a4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr196a06ce4f3a54e30a9ed5d51fdcaafdc';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr196a06ce4f3a54e30a9ed5d51fdcaafdc';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr184c932d45be64e3cbb15d87d61b544ee';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr184c932d45be64e3cbb15d87d61b544ee';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr116d9b39f3ffe4135a40d18041c6df02b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr116d9b39f3ffe4135a40d18041c6df02b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1488b2886a13f47cdb7d96cdaadf5e63d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1488b2886a13f47cdb7d96cdaadf5e63d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr15db0902bec90420283e74bb2e07cc004';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr15db0902bec90420283e74bb2e07cc004';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1b8dbe4f8c05a4d629ba78b9081cd17e1';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1b8dbe4f8c05a4d629ba78b9081cd17e1';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr14d2125f9a811459a96c24772870178c5';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr14d2125f9a811459a96c24772870178c5';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr10ef175d0c1bf44b5a5b4a953f4dd7efc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr10ef175d0c1bf44b5a5b4a953f4dd7efc';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr15c9738ad5ea049579c2a384f2cc6e3ad';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr15c9738ad5ea049579c2a384f2cc6e3ad';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr142887573272f48a8bc95eb0fabce1fa0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr142887573272f48a8bc95eb0fabce1fa0';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f3504207c5704e21bf0daade74a52e6c';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f3504207c5704e21bf0daade74a52e6c';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1b4063a2986a14815b40136926ef4cb70';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1b4063a2986a14815b40136926ef4cb70';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1be723845eea54c50939dba892c75f029';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1be723845eea54c50939dba892c75f029';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1bb3b8db5dd754e458e8ea449501de2c6';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1bb3b8db5dd754e458e8ea449501de2c6';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d109eda888494da3a8d436455bceb826';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d109eda888494da3a8d436455bceb826';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr18211f3b0626744398e9584ad13b0b6b1';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr18211f3b0626744398e9584ad13b0b6b1';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr16a9db2067a0346dea6a822eb8f14f109';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr16a9db2067a0346dea6a822eb8f14f109';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr135784ea21a5e4c1d8de2ed67a1f060c4';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr135784ea21a5e4c1d8de2ed67a1f060c4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1966badc8621e40dbb5bfea8aaaccde74';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1966badc8621e40dbb5bfea8aaaccde74';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr17e02a21caddf42dab1e45d77d0308e94';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr17e02a21caddf42dab1e45d77d0308e94';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr19e20720a7ce14bad8ff7a536616a2674';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr19e20720a7ce14bad8ff7a536616a2674';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr14c55130e14784a939b8e51d353fc7ae8';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr14c55130e14784a939b8e51d353fc7ae8';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr158042be3550045c281f05b34c73c0f9d';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr158042be3550045c281f05b34c73c0f9d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11f0cde82eecc4f88994689ceed35272a';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11f0cde82eecc4f88994689ceed35272a';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11fe2b8f1534e4e2ca64bd57dfaff996e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11fe2b8f1534e4e2ca64bd57dfaff996e';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr136c0daa9c915429dbb5ea54dbee59b61';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr136c0daa9c915429dbb5ea54dbee59b61';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12935d5787cd34e2ea9a6723bfdaefdb6';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12935d5787cd34e2ea9a6723bfdaefdb6';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b0e5aa509ffc491482ed8019c69c9af6';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b0e5aa509ffc491482ed8019c69c9af6';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11ef8fa6cbb14483b83b8c258d9a49772';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11ef8fa6cbb14483b83b8c258d9a49772';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d443004e15f64d1bb15e267316f57098';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d443004e15f64d1bb15e267316f57098';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11f9e4637ec824beba7e98efff29260de';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11f9e4637ec824beba7e98efff29260de';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr11cd013c31d0c4c1199a0eeb6c06b261d';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr11cd013c31d0c4c1199a0eeb6c06b261d';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr151b4ac54e3a44ea9b8dff0b5f29aeb10';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr151b4ac54e3a44ea9b8dff0b5f29aeb10';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1841e6e62e27942e7aed69f329ed431af';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1841e6e62e27942e7aed69f329ed431af';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1db6067252afc412bb0b7680b84dcfc6e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1db6067252afc412bb0b7680b84dcfc6e';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr198acc22519004e6daaaba4a6c7a41597';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr198acc22519004e6daaaba4a6c7a41597';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr189cd3990c3a24cf9b78f5a7be9f2ba4a';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr189cd3990c3a24cf9b78f5a7be9f2ba4a';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr19513716d861b4047938ba28bb454e261';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr19513716d861b4047938ba28bb454e261';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1526cd3fec15d42a78f7edcb724f7385a';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1526cd3fec15d42a78f7edcb724f7385a';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr11956449ee1be4bb0aba9ce9db8dcdfa0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr11956449ee1be4bb0aba9ce9db8dcdfa0';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1b7e03b30da154d7c9a489af423e2fc3c';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1b7e03b30da154d7c9a489af423e2fc3c';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1c6ce9c00ec0345ddb4b0b7ed1ec196ce';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1c6ce9c00ec0345ddb4b0b7ed1ec196ce';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1267ecb9bf7ed489ba1363f2e8f519ee2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1267ecb9bf7ed489ba1363f2e8f519ee2';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1e9e4577fb7674e04a460a6f7f6a7b200';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1e9e4577fb7674e04a460a6f7f6a7b200';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr12ebb20c65c0942908337cdfd7f8529fa';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr12ebb20c65c0942908337cdfd7f8529fa';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr13880952f563a402a886c715c2a78fc52';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr13880952f563a402a886c715c2a78fc52';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr14e3b54f3c7954050abf2695fde50c94b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr14e3b54f3c7954050abf2695fde50c94b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1540de84ce0bf44858e5761c0064146cf';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1540de84ce0bf44858e5761c0064146cf';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fabff0196beb4335bd10239ab5653920';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fabff0196beb4335bd10239ab5653920';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1003b6548f16c4e8b8e594a19b77e78b1';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1003b6548f16c4e8b8e594a19b77e78b1';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1181d31d8a4c24bd68016ea8fabcf32b2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1181d31d8a4c24bd68016ea8fabcf32b2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e15bb7ce839d4573bcfe088d1c68fdb4';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e15bb7ce839d4573bcfe088d1c68fdb4';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d1bb357aea7e43f29deb0f4185961300';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d1bb357aea7e43f29deb0f4185961300';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr167d3c2c0e98548b39c09a86816087b98';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr167d3c2c0e98548b39c09a86816087b98';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr104a9627c7832471fb6af46f001da35b8';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr104a9627c7832471fb6af46f001da35b8';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1fcc2ad397ff34f4b916cc98a8f2ffcb7';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1fcc2ad397ff34f4b916cc98a8f2ffcb7';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr158b0afedf8ce4724a2edbeeade0ce46a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr158b0afedf8ce4724a2edbeeade0ce46a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr102b901c5ac64430abc4bc8d5a210c28b';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr102b901c5ac64430abc4bc8d5a210c28b';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1bffdf6c2904a4db193c70e220a4fa6c6';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1bffdf6c2904a4db193c70e220a4fa6c6';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1dcdca9f665e8459691762abc6adbb21e';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1dcdca9f665e8459691762abc6adbb21e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr10e1f3bfb7cd64cb599a9d89618a19492';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr10e1f3bfb7cd64cb599a9d89618a19492';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr159476343db794a81bc75080a82655ef1';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr159476343db794a81bc75080a82655ef1';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1dfc176223ba941da8fb438f6268a87a4';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1dfc176223ba941da8fb438f6268a87a4';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr13058595f79ce4b4fbdca01dfdc29a0c4';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr13058595f79ce4b4fbdca01dfdc29a0c4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr16e5ccdac7a2640a6a2360fccdfcecaaf';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr16e5ccdac7a2640a6a2360fccdfcecaaf';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr12c60f1df025e49328e97259f0c74c8b3';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr12c60f1df025e49328e97259f0c74c8b3';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1dd6eb0fa434944bf9b09c08e27419677';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1dd6eb0fa434944bf9b09c08e27419677';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1922ddcb16e5a497cb21b7cdffb7ce4c6';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1922ddcb16e5a497cb21b7cdffb7ce4c6';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr12f19656fa42f48718cd12f7cdf3fcdf0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr12f19656fa42f48718cd12f7cdf3fcdf0';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr119d5eabbcb2d454d86a6ff2b0fc85eb3';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr119d5eabbcb2d454d86a6ff2b0fc85eb3';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1fd3550edb6f84acdb2dc9bee08338ee0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1fd3550edb6f84acdb2dc9bee08338ee0';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr13bc934e66d6b49cb8413be666a7b23aa';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr13bc934e66d6b49cb8413be666a7b23aa';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr12e0c27ef6aab463cb23aa9e3d5bb632c';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr12e0c27ef6aab463cb23aa9e3d5bb632c';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f78229ffdcb64db787f2180d67f3f715';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f78229ffdcb64db787f2180d67f3f715';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1f9aca01006224fa6befef8d1c70c2282';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1f9aca01006224fa6befef8d1c70c2282';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f173cbc3602645ffaf1b148842467557';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f173cbc3602645ffaf1b148842467557';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1792fdf25ff224ac598cfeeab14e26e35';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1792fdf25ff224ac598cfeeab14e26e35';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1a1161ed090014829a4cf63385456e557';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1a1161ed090014829a4cf63385456e557';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr16f5f0c94e98744f999c2c0bb1e1d646d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr16f5f0c94e98744f999c2c0bb1e1d646d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1b5de28409b034c09a16ff3402f03b463';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1b5de28409b034c09a16ff3402f03b463';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr189b2d8b87cae4a0aa2ce303a744c9b42';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr189b2d8b87cae4a0aa2ce303a744c9b42';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr14c854a55236149319243c2f8fb6823b2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr14c854a55236149319243c2f8fb6823b2';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1dbc6b724791e478e8d5828df5ae8247d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1dbc6b724791e478e8d5828df5ae8247d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18610c5a2fc4844439c74d24e6eed184d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18610c5a2fc4844439c74d24e6eed184d';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr1541eb69979244270bffc12443ae6a82a';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr1541eb69979244270bffc12443ae6a82a';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr10c4506bfac2e48608a119ad1c8456225';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr10c4506bfac2e48608a119ad1c8456225';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16267ea8f377243ce96b77d2d693067c2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16267ea8f377243ce96b77d2d693067c2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a3b368687cf14eb28b90813860c4c998';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a3b368687cf14eb28b90813860c4c998';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr136aefcd916f0455f9d773a0f8fa67fd4';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr136aefcd916f0455f9d773a0f8fa67fd4';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1b8272731240040ea8b7ad507e331dea2';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1b8272731240040ea8b7ad507e331dea2';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr14d2e16631b5f474593805d4584bf7466';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr14d2e16631b5f474593805d4584bf7466';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr121784870129c447583f0abf22fb5e859';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr121784870129c447583f0abf22fb5e859';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr170843f32af354c43ad3fd9a3488cbe99';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr170843f32af354c43ad3fd9a3488cbe99';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr181960f33347749da9d92d008b8f1c653';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr181960f33347749da9d92d008b8f1c653';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr137cafe0a8d954deabe8fc04fd033c80e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr137cafe0a8d954deabe8fc04fd033c80e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr12a59012d0283448496fbda5b75dbbdf1';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr12a59012d0283448496fbda5b75dbbdf1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19b24aaae0d9b4373a23f263aa30ecff7';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19b24aaae0d9b4373a23f263aa30ecff7';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1058936caf6944384a8649b926986b8e6';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1058936caf6944384a8649b926986b8e6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1789554263c984ec8a824aca39af1f0d2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1789554263c984ec8a824aca39af1f0d2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr105313b5c7d3347ca9a5d2cc1814aa203';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr105313b5c7d3347ca9a5d2cc1814aa203';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr18481d55a5eeb4ad3b835e8213350fcd2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr18481d55a5eeb4ad3b835e8213350fcd2';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr17fa3b86440864e26b548d113c1d6886c';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr17fa3b86440864e26b548d113c1d6886c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19a97ec3a04c446b98559e826ec9133a1';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19a97ec3a04c446b98559e826ec9133a1';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr182d8c94976cb4ccf97f1e9c4749046d5';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr182d8c94976cb4ccf97f1e9c4749046d5';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10917a3ba68344c44854f6bbb01040ea9';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10917a3ba68344c44854f6bbb01040ea9';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1cd7093d3545a43fd955c4521acf55414';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1cd7093d3545a43fd955c4521acf55414';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1939bf2b5dd6b49ceaa7af50d6d4a646b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1939bf2b5dd6b49ceaa7af50d6d4a646b';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1cd4295aff091444f868ec2a9b7a89cba';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1cd4295aff091444f868ec2a9b7a89cba';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr15f13159064af4c7ab5d48a0d681aeb1f';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr15f13159064af4c7ab5d48a0d681aeb1f';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr17d50eb51785c468faa3132e063a4b126';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr17d50eb51785c468faa3132e063a4b126';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1145212162fbf4e72a2f919c806cae2dc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1145212162fbf4e72a2f919c806cae2dc';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr185c2c0434df64e97917dcd8afc159dc6';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr185c2c0434df64e97917dcd8afc159dc6';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr118fe13de5aa0409e9a8b8987bb4fa637';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr118fe13de5aa0409e9a8b8987bb4fa637';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d07989419ed945e6b2709fa99a74bf54';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d07989419ed945e6b2709fa99a74bf54';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr145e06bd0c16a49eda20ef128ede7baf4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr145e06bd0c16a49eda20ef128ede7baf4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr12b9ffc839834418084ddbaad7f167f46';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr12b9ffc839834418084ddbaad7f167f46';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1d32f5093f41e4f26a7c54748824fd059';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1d32f5093f41e4f26a7c54748824fd059';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1a8cf7d944f144679982f36aff1344e9a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1a8cf7d944f144679982f36aff1344e9a';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1900b0e31e7b34290b858a8f357a7dbaa';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1900b0e31e7b34290b858a8f357a7dbaa';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr121d4f71e7a3a4e1fb5319965b0d5468c';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr121d4f71e7a3a4e1fb5319965b0d5468c';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr161b1228c83414625b35509c66c519c76';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr161b1228c83414625b35509c66c519c76';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1bfb65d1f244249ea80c94a26869f3be7';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1bfb65d1f244249ea80c94a26869f3be7';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr196fa3749e05a47348a9acc50c4b7a704';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr196fa3749e05a47348a9acc50c4b7a704';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr11f9001655916445fa6df60414f19ae03';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr11f9001655916445fa6df60414f19ae03';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr17556e256a84e4b4d98b59bfef0c7bdc2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr17556e256a84e4b4d98b59bfef0c7bdc2';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11b455af3ca704fcb9bd7d76d69b688bd';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11b455af3ca704fcb9bd7d76d69b688bd';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1aad3b9f3ee034ba7ba704504f94a38d1';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1aad3b9f3ee034ba7ba704504f94a38d1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr109af98197ffb472083928e51336b3150';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr109af98197ffb472083928e51336b3150';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ae69e7280ff14ea68a165a67b18743e1';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ae69e7280ff14ea68a165a67b18743e1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr142fdefdc9c0d4fe69809a38b0cacd3ca';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr142fdefdc9c0d4fe69809a38b0cacd3ca';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17f292f3e7fc142108a1499ab1eadf0b0';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17f292f3e7fc142108a1499ab1eadf0b0';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1b8daa5fa46204f508b75030d73c6d79d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1b8daa5fa46204f508b75030d73c6d79d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d25bb14e14f84ef4bea34496d62e2043';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d25bb14e14f84ef4bea34496d62e2043';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr13e46aa6191cb48699708a7c0634b7833';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr13e46aa6191cb48699708a7c0634b7833';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10ab8f22e799e43bcbb7cf84fb86469c4';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10ab8f22e799e43bcbb7cf84fb86469c4';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1f3e015467ffc4ff7893c6fea47c10e66';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1f3e015467ffc4ff7893c6fea47c10e66';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr140ed914fbddf42d7b2959da4e91c170d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr140ed914fbddf42d7b2959da4e91c170d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a66c724603ef4e4381269d4faf746896';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a66c724603ef4e4381269d4faf746896';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18c8b66c19f2f4aac902ecb39fb28d0b2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18c8b66c19f2f4aac902ecb39fb28d0b2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fe2fd0b6473a45bc8e34b190672e0ef0';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fe2fd0b6473a45bc8e34b190672e0ef0';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18613303f21ea43a5a70a7a93585f95ef';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18613303f21ea43a5a70a7a93585f95ef';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e50885e0e2c74cb0935bc20693c83c4e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e50885e0e2c74cb0935bc20693c83c4e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15a5777e2dd8041738fda27610695cc99';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15a5777e2dd8041738fda27610695cc99';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b03f7bd624294b49b06a0e3e39fa597a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b03f7bd624294b49b06a0e3e39fa597a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f7d49ba732684b1092f242921643d8ab';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f7d49ba732684b1092f242921643d8ab';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15c66c4d748b74ff8823c9ae848fdaadd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15c66c4d748b74ff8823c9ae848fdaadd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a674a81f030447e4b8a9ec9c027bb775';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a674a81f030447e4b8a9ec9c027bb775';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15a30bc924b074713b294c6089c610c84';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15a30bc924b074713b294c6089c610c84';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13874e99b208e48619c334ad8caebd7bf';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13874e99b208e48619c334ad8caebd7bf';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr189de4e2249bb4eadad68804de001781e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr189de4e2249bb4eadad68804de001781e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1af93a060bc1943008667290e0bed56f7';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1af93a060bc1943008667290e0bed56f7';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1195e1a72c12648f18ae54e0fbeb550fc';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1195e1a72c12648f18ae54e0fbeb550fc';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1765cedc6665546b69fdabcbc2c3909c1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1765cedc6665546b69fdabcbc2c3909c1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr109389f9a9156430083dc98d4855811f4';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr109389f9a9156430083dc98d4855811f4';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr10195fa56cd844f4a94cf4395b308a9f5';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr10195fa56cd844f4a94cf4395b308a9f5';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1761ef98c46bb4fe3b3994ad1a269b858';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1761ef98c46bb4fe3b3994ad1a269b858';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1da16d220c7fd431b94a54fbd0c25f03c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1da16d220c7fd431b94a54fbd0c25f03c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15f9df42025d942b086a2c3478c1c7bee';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15f9df42025d942b086a2c3478c1c7bee';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr195ed7996a7664a559b2d5b7ab98027c1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr195ed7996a7664a559b2d5b7ab98027c1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr194ff15d5ef654018b56ea75b8357d5ec';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr194ff15d5ef654018b56ea75b8357d5ec';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1be79c32c5ba54f428d15b1594f9ab904';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1be79c32c5ba54f428d15b1594f9ab904';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1cd85db5a852748c39cc5b539b56fd2b4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1cd85db5a852748c39cc5b539b56fd2b4';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1afe69f38d3894d04adc0ade126ac7b14';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1afe69f38d3894d04adc0ade126ac7b14';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr15fb81a49486340c6a0a3bdbc7f8d6169';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr15fb81a49486340c6a0a3bdbc7f8d6169';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1f3f86f3bb02643d49c8f5b4513124043';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1f3f86f3bb02643d49c8f5b4513124043';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr16b333bcd584d430d8d385a1091ac5ba4';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr16b333bcd584d430d8d385a1091ac5ba4';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr18abd2293080842aa974ff9f304f8f1a2';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr18abd2293080842aa974ff9f304f8f1a2';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f6da61928db142e18aed11e53ff60b1b';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f6da61928db142e18aed11e53ff60b1b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1cd0000a3d35c4dd7a3a6b5e97bcec879';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1cd0000a3d35c4dd7a3a6b5e97bcec879';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16b328047774643b29287845c5dfc05d8';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16b328047774643b29287845c5dfc05d8';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1c5419ebe75ed47b587dd6755c8d5cb15';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1c5419ebe75ed47b587dd6755c8d5cb15';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e12fc7e52fd342cc8379de20400c13cd';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e12fc7e52fd342cc8379de20400c13cd';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr14ee1d5ce1e0946f0b714bd072753d9d7';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr14ee1d5ce1e0946f0b714bd072753d9d7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1bde4dbb62a7e487c8cf75f819240e41d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1bde4dbb62a7e487c8cf75f819240e41d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e72c9f6246e74b189cd7e453915ad9a7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e72c9f6246e74b189cd7e453915ad9a7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1274a7bde5f134cecaf2241709051ee6b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1274a7bde5f134cecaf2241709051ee6b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11a4a95d03ebb4d3b83846375ef1e9fdb';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11a4a95d03ebb4d3b83846375ef1e9fdb';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f06a15c6fa31427fb577ac00a025f4c3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f06a15c6fa31427fb577ac00a025f4c3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14d383e69d24b4a0b9b28ad587fb16b5e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14d383e69d24b4a0b9b28ad587fb16b5e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr118f8845c1d9f4c32a38e7ca2ed4162fd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr118f8845c1d9f4c32a38e7ca2ed4162fd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr100a6486adcd04110a1e318148dbfc759';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr100a6486adcd04110a1e318148dbfc759';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr171efb0b218d14ec88ca5db9371a9f68e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr171efb0b218d14ec88ca5db9371a9f68e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ed0cf59d941440c7904555515cca13f7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ed0cf59d941440c7904555515cca13f7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a2a5bc7f524848259d5e17c89573d9e4';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a2a5bc7f524848259d5e17c89573d9e4';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15cc89f9279664472964ba44dc2dc1335';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15cc89f9279664472964ba44dc2dc1335';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13ab3d892480f494e994b66150e5a0fe2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13ab3d892480f494e994b66150e5a0fe2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17259fd22113044c18f0b401ca8f3a097';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17259fd22113044c18f0b401ca8f3a097';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1bcfcd44d83fe483096378ef66fd4190b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1bcfcd44d83fe483096378ef66fd4190b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f930979cf294411999e4ee6e4d969ceb';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f930979cf294411999e4ee6e4d969ceb';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b3ed9fdaa95f4e1cb24f900089dd8de3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b3ed9fdaa95f4e1cb24f900089dd8de3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17972c142582f4628a37d2114061c02da';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17972c142582f4628a37d2114061c02da';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10525f96590bc414798fabc20fb6831dd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10525f96590bc414798fabc20fb6831dd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b6c5f9e52bd24b4f97cecf544212585a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b6c5f9e52bd24b4f97cecf544212585a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr176491b24938f4514bb82fe02a4f3822c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr176491b24938f4514bb82fe02a4f3822c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1193c78ce51f64f06ae83be7daf99ddee';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1193c78ce51f64f06ae83be7daf99ddee';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1079e139e366f4130b551ecca80ddf5fa';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1079e139e366f4130b551ecca80ddf5fa';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18ab67e640fd843a2987ecc7a575cb156';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18ab67e640fd843a2987ecc7a575cb156';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18744938737e34d879a9e42551d899942';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18744938737e34d879a9e42551d899942';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1dd8d5e6e80274fda9b263b808f719dba';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1dd8d5e6e80274fda9b263b808f719dba';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1c555f7597e4346838a23b2cfb52a3c2c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1c555f7597e4346838a23b2cfb52a3c2c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13fc0a29d337746c4acfe3df39ef4bf53';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13fc0a29d337746c4acfe3df39ef4bf53';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12fbefa247e8844a2b5e9fdb4bc37a3dc';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12fbefa247e8844a2b5e9fdb4bc37a3dc';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b8a65d67f58c4a9d93860e5a0ee23562';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b8a65d67f58c4a9d93860e5a0ee23562';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fc199a3b4656448a8658189037213472';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fc199a3b4656448a8658189037213472';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1562713f4c56143debe65ecc290d4f978';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1562713f4c56143debe65ecc290d4f978';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ee17a98d45c341779da4379273fe23fb';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ee17a98d45c341779da4379273fe23fb';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11356b3d2cdb2480897215f771cdd3300';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11356b3d2cdb2480897215f771cdd3300';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fd944fee63874633bff915a2572cad22';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fd944fee63874633bff915a2572cad22';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1141746bf4e014902ae5a5b20ebfb4f30';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1141746bf4e014902ae5a5b20ebfb4f30';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr111069cf0f93c4252a986d2f37753af7c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr111069cf0f93c4252a986d2f37753af7c';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr10de605fe62854dfd8ecf5cd0709503fc';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr10de605fe62854dfd8ecf5cd0709503fc';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14b2c026ef5f34efdb77907c649a767af';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14b2c026ef5f34efdb77907c649a767af';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr116ccd0eb1c0c455892e027978162f1b8';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr116ccd0eb1c0c455892e027978162f1b8';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1e0a2afe31c4442b5a2b377eee0a449c4';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1e0a2afe31c4442b5a2b377eee0a449c4';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18bc3a4b4381247cab776f3322032e8d6';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18bc3a4b4381247cab776f3322032e8d6';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1bd7d1152fdd84edb9425601fe7ddbfec';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1bd7d1152fdd84edb9425601fe7ddbfec';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1db19617f85e941de83a78b612a2f5500';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1db19617f85e941de83a78b612a2f5500';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1dfd278d52b3443e5a5d3a4f9a7eeb3cd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1dfd278d52b3443e5a5d3a4f9a7eeb3cd';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr16eb274c26b6b4f6987421403917f2eb4';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr16eb274c26b6b4f6987421403917f2eb4';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr138291d2c27854fb99cf77aa4525428f5';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr138291d2c27854fb99cf77aa4525428f5';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11fe5ed5a361a4bcc83064db4b590d137';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11fe5ed5a361a4bcc83064db4b590d137';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr193963e87b549486c8b5b6f6372567344';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr193963e87b549486c8b5b6f6372567344';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16191c5340ed6456c82e8e04959fe9553';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16191c5340ed6456c82e8e04959fe9553';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14fa4d8c9137c4689be38fa51763d8ca9';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14fa4d8c9137c4689be38fa51763d8ca9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16a6f8f0594ce483b813f58dc032c0146';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16a6f8f0594ce483b813f58dc032c0146';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr13868db31f3ff41589ba987ddef046e84';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr13868db31f3ff41589ba987ddef046e84';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr16c11902a0485429eb666a2bd2aad1d37';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr16c11902a0485429eb666a2bd2aad1d37';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b09d5e7009d546d7b3d8c4b23ab7ed28';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b09d5e7009d546d7b3d8c4b23ab7ed28';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11e148a30226a4fa5b7351f334c2e8828';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11e148a30226a4fa5b7351f334c2e8828';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr163ef2f5f76664180aa65fe42ce8b86f6';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr163ef2f5f76664180aa65fe42ce8b86f6';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr142f8f52e15fa4b2688090c4e55111189';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr142f8f52e15fa4b2688090c4e55111189';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr135d6af3f2bd04ef993e8f233281266b3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr135d6af3f2bd04ef993e8f233281266b3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16864f72cbf914713b580b2e0ab5ccb45';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16864f72cbf914713b580b2e0ab5ccb45';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1191d5eb9b56c4ecb80e24aab1cb8a214';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1191d5eb9b56c4ecb80e24aab1cb8a214';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr110453025f8d143ecba42a69769f19427';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr110453025f8d143ecba42a69769f19427';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11d28316182664748ad69c63a6b73480c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11d28316182664748ad69c63a6b73480c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1c338a006082d45c1b743acbcb13bc49f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1c338a006082d45c1b743acbcb13bc49f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr192acacab41254f1eb262126f5a72d054';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr192acacab41254f1eb262126f5a72d054';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19160a362d1f845ce9c46da734dd21973';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19160a362d1f845ce9c46da734dd21973';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1168235ee957e4ae7bdb2df55281b8550';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1168235ee957e4ae7bdb2df55281b8550';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1507f5f9f96da4890813b736e8ab651e1';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1507f5f9f96da4890813b736e8ab651e1';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d5976b8e922d4b1f91484b22fe5d7570';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d5976b8e922d4b1f91484b22fe5d7570';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f6c3f29693cb4ac28d619db2d77349b8';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f6c3f29693cb4ac28d619db2d77349b8';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12eddba398fa943b4b44734c2d6753687';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12eddba398fa943b4b44734c2d6753687';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr112f584986c2a458ebd3d5e8f15defe83';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr112f584986c2a458ebd3d5e8f15defe83';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b0ce57e617484d0e9296b5599e05570a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b0ce57e617484d0e9296b5599e05570a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d6566fe556174cfcb354b84f1d7e4d97';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d6566fe556174cfcb354b84f1d7e4d97';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1641fc13aa0a345e0b418d8198166b16d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1641fc13aa0a345e0b418d8198166b16d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18218f82c6dbd49bb964d101b3b4a8e6f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18218f82c6dbd49bb964d101b3b4a8e6f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr104596a4b06f44cd48b6f4ca7813b2312';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr104596a4b06f44cd48b6f4ca7813b2312';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr157369e637d99439785d8c5a5280c031b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr157369e637d99439785d8c5a5280c031b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f0c0e29fcb3c42d196143ca3d379b4cd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f0c0e29fcb3c42d196143ca3d379b4cd';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1482c9268602149eabc6ed9c691996a9c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1482c9268602149eabc6ed9c691996a9c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13a0caff0e7b04b279f16f117c4409f83';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13a0caff0e7b04b279f16f117c4409f83';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e80e3b2705184a7ab2dbe155434ec1b2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e80e3b2705184a7ab2dbe155434ec1b2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr174026fe55858446e83f5aa037ff21957';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr174026fe55858446e83f5aa037ff21957';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b75d8b69c7ff48f3a5f0a551c6fd3462';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b75d8b69c7ff48f3a5f0a551c6fd3462';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ab9c30ce5f2c43328cab1b75832fbeb2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ab9c30ce5f2c43328cab1b75832fbeb2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10a45294c7f2247a78b1d4b5d8dd38ce6';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10a45294c7f2247a78b1d4b5d8dd38ce6';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a5996bff0770457db789df00a307ccb2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a5996bff0770457db789df00a307ccb2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e451aa8208274259a5459bedc45761b4';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e451aa8208274259a5459bedc45761b4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr191fd1370d7784316b572ec9cb9e447b9';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr191fd1370d7784316b572ec9cb9e447b9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr107ce0c2ce09e494a8d2e218e650d13a2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr107ce0c2ce09e494a8d2e218e650d13a2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1953eceaf9e084947935f0c215ce2cc96';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1953eceaf9e084947935f0c215ce2cc96';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e64055a962bc45518831a049b1618b1c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e64055a962bc45518831a049b1618b1c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10b39014d144545e4a8d915201b6046f3';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10b39014d144545e4a8d915201b6046f3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1cf969c479749472a91a815bf9c41ddda';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1cf969c479749472a91a815bf9c41ddda';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr135cc972ad86949aca1062164f05f6cb9';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr135cc972ad86949aca1062164f05f6cb9';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f2b8ebf8cd8c44efb50b76fe56df1079';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f2b8ebf8cd8c44efb50b76fe56df1079';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr154dbfd234f714122b5e88b2d9a94354d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr154dbfd234f714122b5e88b2d9a94354d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr19ce7ae18b2b94ee2a6ad417df8a48607';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr19ce7ae18b2b94ee2a6ad417df8a48607';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr15fb38865f9c24cdc99339efe2ed9a131';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr15fb38865f9c24cdc99339efe2ed9a131';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1e3d51f70f2ca462bb25c3fb039c6f012';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1e3d51f70f2ca462bb25c3fb039c6f012';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11a771666d8074781b6cf2c3508684674';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11a771666d8074781b6cf2c3508684674';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10e1d9e40d8094c9b821e653994a46d78';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10e1d9e40d8094c9b821e653994a46d78';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr145e2e040b0964b6cb052a5a816e5d8f4';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr145e2e040b0964b6cb052a5a816e5d8f4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e523d1d12dd84bee8278633a5f3ed0ea';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e523d1d12dd84bee8278633a5f3ed0ea';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15a674bbfca244c29b25035b040f18380';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15a674bbfca244c29b25035b040f18380';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1eaeaf80126d24d5298dd90d0aaabab88';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1eaeaf80126d24d5298dd90d0aaabab88';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10ec675f60d064b8e9f5a6f133f000a08';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10ec675f60d064b8e9f5a6f133f000a08';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr15ac59391a9c14328b050476b4af8ae2f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr15ac59391a9c14328b050476b4af8ae2f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr133c3a55f78864ebc9ad90e2f30d85d57';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr133c3a55f78864ebc9ad90e2f30d85d57';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f55d7673f355491a9b12b3cd371711a7';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f55d7673f355491a9b12b3cd371711a7';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1dfdc553963024a45acb9b71a0174eeba';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1dfdc553963024a45acb9b71a0174eeba';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1063ff4c864ab4648809adb506087de69';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1063ff4c864ab4648809adb506087de69';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fe952de3ac3549688ff3009c3fdb7857';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fe952de3ac3549688ff3009c3fdb7857';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr167d5145f9e524a9b81daf8ad925d1bdf';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr167d5145f9e524a9b81daf8ad925d1bdf';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19658cce4f8e9449fafad23126a9be9ac';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19658cce4f8e9449fafad23126a9be9ac';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1c3cb94dfd6bc46ccb128524e13c0f0db';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1c3cb94dfd6bc46ccb128524e13c0f0db';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr17e6228c56c974bd0916d51d6146adce3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr17e6228c56c974bd0916d51d6146adce3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr106f4df717f0f45b5b57fe0f435169217';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr106f4df717f0f45b5b57fe0f435169217';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr113547610b1f7497bb4a856745625565e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr113547610b1f7497bb4a856745625565e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr10e89b4ff1dad4b91a81343afa9f59b94';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr10e89b4ff1dad4b91a81343afa9f59b94';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b025e31dbfc343ab948287ea78a51000';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b025e31dbfc343ab948287ea78a51000';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18679a4c8ca824da3b7e3b8c6e7867337';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18679a4c8ca824da3b7e3b8c6e7867337';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1d81d0c3456414cb088f542cb76668aa2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1d81d0c3456414cb088f542cb76668aa2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1324f220d754a4b38a735b3eb58f18478';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1324f220d754a4b38a735b3eb58f18478';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr17a4ccd0a034e46c9a7646a4e6da72dae';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr17a4ccd0a034e46c9a7646a4e6da72dae';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1b1f88824949b4da5919531ed17c5cbd4';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1b1f88824949b4da5919531ed17c5cbd4';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1680bdbd02fd14a3095d173f64ea0a9ed';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1680bdbd02fd14a3095d173f64ea0a9ed';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1b49b8f0926144b96873ebb9ebad7d903';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1b49b8f0926144b96873ebb9ebad7d903';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1f72e15e2d2824e43b2b74cb73ef60ac5';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1f72e15e2d2824e43b2b74cb73ef60ac5';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1f4c341318b0c4462bf18683e7522984a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1f4c341318b0c4462bf18683e7522984a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14903f90b827149df9f5a90587e52e6e3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14903f90b827149df9f5a90587e52e6e3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17786a8e801db44fcac8aab2a7b9c250c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17786a8e801db44fcac8aab2a7b9c250c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1fb64b6654ad44d6bbae1f837a68bcae1';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1fb64b6654ad44d6bbae1f837a68bcae1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1d28429e19394459aa92f85225046ad9b';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1d28429e19394459aa92f85225046ad9b';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1dbdfdf3d2d234b86aff54d67b96816a0';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1dbdfdf3d2d234b86aff54d67b96816a0';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1bda2e2c4b5db4477a61c6f92625defea';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1bda2e2c4b5db4477a61c6f92625defea';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr14bf977a1d23e4e278d359870c6cf55f2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr14bf977a1d23e4e278d359870c6cf55f2';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11d7058e59bab404faca9584e42dfd850';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11d7058e59bab404faca9584e42dfd850';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1152097eba823432d9356b12e29f79f84';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1152097eba823432d9356b12e29f79f84';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14fd415bbe7c74008802e981c7e7e0c64';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14fd415bbe7c74008802e981c7e7e0c64';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1095f8ad6ab134d12a9df98b7bff1b434';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1095f8ad6ab134d12a9df98b7bff1b434';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr151c8d152f6cf43e89687cf82a87bb28e';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr151c8d152f6cf43e89687cf82a87bb28e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ad414ca09d534ebfab44030ba4d7599b';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ad414ca09d534ebfab44030ba4d7599b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12d5a5e5010d84c57b07b0f6bd61aafac';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12d5a5e5010d84c57b07b0f6bd61aafac';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr18bfb1eb93a804cad903da0de6c1367c5';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr18bfb1eb93a804cad903da0de6c1367c5';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1bdd6c525d1a94dc78035efe65188dfa6';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1bdd6c525d1a94dc78035efe65188dfa6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr18b43122d98db44c797bc63910b73cc99';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr18b43122d98db44c797bc63910b73cc99';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1c4a6e3c8f98f4c9ab768fef09578445f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1c4a6e3c8f98f4c9ab768fef09578445f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12bd29a2a12ec4be7a0ac11082cd6bbe8';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12bd29a2a12ec4be7a0ac11082cd6bbe8';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1eadeb50bfda147d59a32a72ce4d42c6c';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1eadeb50bfda147d59a32a72ce4d42c6c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr110078fb3fa5143a0a090759cd111c185';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr110078fb3fa5143a0a090759cd111c185';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr120471824cf50468ea42347e26f656669';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr120471824cf50468ea42347e26f656669';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16a4d0af73c1f45db8d1fd70301357972';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16a4d0af73c1f45db8d1fd70301357972';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr168cfb415503e4c3cae1184ed5e3fe576';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr168cfb415503e4c3cae1184ed5e3fe576';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1287145fa007c43e896e74339876192e8';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1287145fa007c43e896e74339876192e8';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1bda8ea6e961d45be84c189926616681d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1bda8ea6e961d45be84c189926616681d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1478c554938554fb6a020b5a433fd051c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1478c554938554fb6a020b5a433fd051c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1f6b89714b0044c8092f568fb0bd8523c';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1f6b89714b0044c8092f568fb0bd8523c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16738fc633dc34b009be3d4914f14f703';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16738fc633dc34b009be3d4914f14f703';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1a053dc05e506438f977812fb5bec4b0c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1a053dc05e506438f977812fb5bec4b0c';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1c7816972fb274a50bee9b2ecde0f0d27';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1c7816972fb274a50bee9b2ecde0f0d27';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ca4f787882e643c8851733934a525bd2';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ca4f787882e643c8851733934a525bd2';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1bfb0765c1fb4492aa62894ba5c74fb0d';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1bfb0765c1fb4492aa62894ba5c74fb0d';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr11462e3887f454162be3bce34b8a32d85';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr11462e3887f454162be3bce34b8a32d85';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr16a908c5ade2d45a1ae757ded1b529d90';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr16a908c5ade2d45a1ae757ded1b529d90';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1e53e63499b5b4fbd9f945a5404be0f45';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1e53e63499b5b4fbd9f945a5404be0f45';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr10c696d8d24dc454ebb61c26d1ec4be1c';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr10c696d8d24dc454ebb61c26d1ec4be1c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a88e7cbda5bf4aaeadca4e5e74d94837';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a88e7cbda5bf4aaeadca4e5e74d94837';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1745e092f44e24fa1813d89e8c204af59';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1745e092f44e24fa1813d89e8c204af59';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1cc10831a8db243a6b4dfc31eb1df6e0a';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1cc10831a8db243a6b4dfc31eb1df6e0a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr19ff86e90fd844633bd7a2d3ccf810c86';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr19ff86e90fd844633bd7a2d3ccf810c86';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr12a0d46daf5cc4eefa4c2447dfad7f06d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr12a0d46daf5cc4eefa4c2447dfad7f06d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr141dec5797dec4a6e826ad54120090340';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr141dec5797dec4a6e826ad54120090340';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13f2218289e1643008611aaf00eedabfe';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13f2218289e1643008611aaf00eedabfe';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1be1ab2e76ae64726a36cead0d0f2880e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1be1ab2e76ae64726a36cead0d0f2880e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1e392ff5e80b647c2b9421b104b717cb2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1e392ff5e80b647c2b9421b104b717cb2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr198a00a53809f449987da0e4cfbc01620';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr198a00a53809f449987da0e4cfbc01620';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1406fe1be71dd40f882e508f6d5f0a0c3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1406fe1be71dd40f882e508f6d5f0a0c3';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1c8324ba2068f4f4f804101052a5c79fb';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1c8324ba2068f4f4f804101052a5c79fb';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1b5a8d056eddc455f89449b87046f6a69';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1b5a8d056eddc455f89449b87046f6a69';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ec921002cc4246318a55a7564ff34476';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ec921002cc4246318a55a7564ff34476';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr14aba664977c847feae25f32da1123877';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr14aba664977c847feae25f32da1123877';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr182836b8d575a4c2caa7e410fd19d9703';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr182836b8d575a4c2caa7e410fd19d9703';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1de327e174270462a8f329a93ae5dd819';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1de327e174270462a8f329a93ae5dd819';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr19c6b4807347f4ea696d28afb32e6db8e';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr19c6b4807347f4ea696d28afb32e6db8e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1de4e96a965994ad68802af8778242904';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1de4e96a965994ad68802af8778242904';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1091a6b8920724e6da4cc0b08aea92a30';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1091a6b8920724e6da4cc0b08aea92a30';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr15d3271eb52314e0b95055181f0442a01';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr15d3271eb52314e0b95055181f0442a01';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1873ce32f53f240d9b23a53043426a85a';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1873ce32f53f240d9b23a53043426a85a';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr18880cdea3a134d5a9996fa58911bca4a';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr18880cdea3a134d5a9996fa58911bca4a';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr162a25bc89074455fbc777868c0f5ffb9';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr162a25bc89074455fbc777868c0f5ffb9';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr147b4654be5d24ee69ef19750690571b8';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr147b4654be5d24ee69ef19750690571b8';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10fc8051ba2ef44e184e0b6be2c14dbbb';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10fc8051ba2ef44e184e0b6be2c14dbbb';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ad2cda511a2e45d895a4ce7b4c3d33ef';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ad2cda511a2e45d895a4ce7b4c3d33ef';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr164309c79f11642888eeadb818083b180';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr164309c79f11642888eeadb818083b180';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10a12fa9096d24238988883581fc9a33a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10a12fa9096d24238988883581fc9a33a';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1acc08d1df73241f5b3d30e7fd9256ffd';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1acc08d1df73241f5b3d30e7fd9256ffd';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12591be40642149549a4b5add4d27fd7a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12591be40642149549a4b5add4d27fd7a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15ee0e3cc768f4f53b99cd72d0136fb41';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15ee0e3cc768f4f53b99cd72d0136fb41';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr172581aeba138467ab3cb98d958f2fed9';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr172581aeba138467ab3cb98d958f2fed9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1dc88a7d375bc49919c571832b880a67c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1dc88a7d375bc49919c571832b880a67c';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr11899d767e19a418aa993628c329c1986';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr11899d767e19a418aa993628c329c1986';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1ce713327295c44aab0ec85ada634277e';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1ce713327295c44aab0ec85ada634277e';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1955de12bc363400c858dde27db39028b';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1955de12bc363400c858dde27db39028b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr16b6b148a545d4a44b7e7a0f9ef95e6cb';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr16b6b148a545d4a44b7e7a0f9ef95e6cb';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1307bef6332784681896c7a2a184a193f';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1307bef6332784681896c7a2a184a193f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a7992654f3b3427fb6adfeecdcdae804';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a7992654f3b3427fb6adfeecdcdae804';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr17b2b0294c8174994b0f913e59a0e3648';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr17b2b0294c8174994b0f913e59a0e3648';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr113de01d3f2754248873179138425daa2';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr113de01d3f2754248873179138425daa2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr176b15b16724f4d7d9c1a85c4343b7aad';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr176b15b16724f4d7d9c1a85c4343b7aad';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr174c7e290f75b4e128619e94c3a636f49';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr174c7e290f75b4e128619e94c3a636f49';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1effb321d07cc4ad9900f96fffb641e17';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1effb321d07cc4ad9900f96fffb641e17';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1df6e015ede7949fe941d8963aae0f6ec';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1df6e015ede7949fe941d8963aae0f6ec';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr10975fe4908344d1e945231c82dc668cb';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr10975fe4908344d1e945231c82dc668cb';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1753199e227fd42e5a6868c6f564dac87';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1753199e227fd42e5a6868c6f564dac87';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr178a17d60863544e3b3c3d2244061a46f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr178a17d60863544e3b3c3d2244061a46f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr153ab48fb821d41348e64d65ed25ef980';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr153ab48fb821d41348e64d65ed25ef980';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr180fb7b68cf664e2aa920f65d5b03bd0c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr180fb7b68cf664e2aa920f65d5b03bd0c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1192d10fa92cf4dff9efb6b3cdfee9813';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1192d10fa92cf4dff9efb6b3cdfee9813';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr142399ca933c249ca9bf6bfdb5a62b424';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr142399ca933c249ca9bf6bfdb5a62b424';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a0a3c92c01ad4e3b83640430920c856c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a0a3c92c01ad4e3b83640430920c856c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1e506e850904e4626be0889bc3edfd398';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1e506e850904e4626be0889bc3edfd398';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1376516678d334023af6f629a0144f292';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1376516678d334023af6f629a0144f292';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15e72fe65e59c4b28b4a482614a10666d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15e72fe65e59c4b28b4a482614a10666d';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr169ecc74d3fba45958a469ce734de43ba';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr169ecc74d3fba45958a469ce734de43ba';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1419090cb297c4d9787fec82892214bb5';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1419090cb297c4d9787fec82892214bb5';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr184228bc6b8af4845a8b96739ac15cfbd';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr184228bc6b8af4845a8b96739ac15cfbd';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13260874b99f34b9084d48207efec306a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13260874b99f34b9084d48207efec306a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr12d9567d14c8d48d0960ffe86e62e008b';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr12d9567d14c8d48d0960ffe86e62e008b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11cd5f428907a403d9838192b96471123';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11cd5f428907a403d9838192b96471123';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1b28d8fe0fa3a4c77a744f217ec8528aa';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1b28d8fe0fa3a4c77a744f217ec8528aa';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16650fecca9fd4f9096b1563ab128de9e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16650fecca9fd4f9096b1563ab128de9e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18b205fda6a3b444d9c35f6b8790876e9';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18b205fda6a3b444d9c35f6b8790876e9';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr139a4438199cb4b8abcdd8d022d49adb2';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr139a4438199cb4b8abcdd8d022d49adb2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1650f3fb307b04670be00c714a4eb0596';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1650f3fb307b04670be00c714a4eb0596';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1539d70b0e01144748e2b54f4a603884d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1539d70b0e01144748e2b54f4a603884d';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr165be68868122458c81d5b11a51a7179b';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr165be68868122458c81d5b11a51a7179b';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11b5f6af6c3734f9384b003a82d5f0af2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11b5f6af6c3734f9384b003a82d5f0af2';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1843f0accacf144c4b9b57de69787c4ff';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1843f0accacf144c4b9b57de69787c4ff';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1f99d45be91a744a6b1c6a68c6e159028';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1f99d45be91a744a6b1c6a68c6e159028';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr16e36661b4c33402f9ec5cf346bc96ee7';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr16e36661b4c33402f9ec5cf346bc96ee7';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1be65b7258b284583a59b1a370a20fda6';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1be65b7258b284583a59b1a370a20fda6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a0a63088373c42c49377c6f0f9dbced0';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a0a63088373c42c49377c6f0f9dbced0';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr179cce391a41448aa857813bfa4434680';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr179cce391a41448aa857813bfa4434680';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1fb8c6b8a8b5c4076bc4eabe54787c5b3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1fb8c6b8a8b5c4076bc4eabe54787c5b3';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1a43f68fda9cf47d680ecc53a0197bef2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1a43f68fda9cf47d680ecc53a0197bef2';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr155600a0bb1ce4a9eb861ec3212337ad1';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr155600a0bb1ce4a9eb861ec3212337ad1';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr153927e7c01234002b41ad41050c13264';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr153927e7c01234002b41ad41050c13264';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ac1ac01972c94937ba6d12820e7f4c56';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ac1ac01972c94937ba6d12820e7f4c56';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr14a8d17cc8c6e49c4aeb4e4bb0929e739';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr14a8d17cc8c6e49c4aeb4e4bb0929e739';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1506fa6ea94204dafb955c0524e484dbe';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1506fa6ea94204dafb955c0524e484dbe';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr10c84d7ccec38462ab4190947d4939c63';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr10c84d7ccec38462ab4190947d4939c63';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr15b7b2d4eff7648f9b5ee4c971a59096e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr15b7b2d4eff7648f9b5ee4c971a59096e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1320d9329853f4af9ae5e7278dabb2437';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1320d9329853f4af9ae5e7278dabb2437';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1d767fc87eaa54bb390ca4992344a0ef3';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1d767fc87eaa54bb390ca4992344a0ef3';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1e8abdfd0e1994c27b5adf75c006479b7';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1e8abdfd0e1994c27b5adf75c006479b7';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr178b5ec051b51435c827caf0e8b100c71';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr178b5ec051b51435c827caf0e8b100c71';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1760e3ee3095042529e41757b47ba9111';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1760e3ee3095042529e41757b47ba9111';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr117861c8c53494945b2cab731ee37b8cf';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr117861c8c53494945b2cab731ee37b8cf';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr145b34a58643648b5aaaa1b4bd182fb54';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr145b34a58643648b5aaaa1b4bd182fb54';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11a3467ed19fb4487b4562006dab3089c';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11a3467ed19fb4487b4562006dab3089c';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14d1a64bd6da14c02bc7dece90d6aaf36';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14d1a64bd6da14c02bc7dece90d6aaf36';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1fe3e161dbc374f0ba31cfeeaf2a32e35';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1fe3e161dbc374f0ba31cfeeaf2a32e35';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1cf541942e41a4d1aa533e52104ed51a3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1cf541942e41a4d1aa533e52104ed51a3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1f4141a333bf44451a3e0cafbdc386804';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1f4141a333bf44451a3e0cafbdc386804';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1928a8683c8d94f91b615a4e8239d7e9a';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1928a8683c8d94f91b615a4e8239d7e9a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13848f2eed0b1410f8662d9e482779674';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13848f2eed0b1410f8662d9e482779674';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1f1e0f7574f484217875b0081da561d60';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1f1e0f7574f484217875b0081da561d60';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1463f7ed5ee7e4835bfbe1f4a3a5b1ec2';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1463f7ed5ee7e4835bfbe1f4a3a5b1ec2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d934bb21d07b4c2bad0a772a53d93dfd';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d934bb21d07b4c2bad0a772a53d93dfd';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1b80fc5ef7dc34e93b0b699d1b0e07c1a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1b80fc5ef7dc34e93b0b699d1b0e07c1a';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1296de5cefb844dbb89a48bc26d64d6ef';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1296de5cefb844dbb89a48bc26d64d6ef';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr170863f17e06c4e7fa448ece685e7d23d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr170863f17e06c4e7fa448ece685e7d23d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr118c9ad826ed548c1abb97619d9ad7772';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr118c9ad826ed548c1abb97619d9ad7772';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr179819ad8e58e4c099c05023db677e770';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr179819ad8e58e4c099c05023db677e770';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1bed0903b78db48eca04ad0f933fab143';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1bed0903b78db48eca04ad0f933fab143';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1a4c36968e46d4df689bdc2458f1e9945';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1a4c36968e46d4df689bdc2458f1e9945';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1689ead8605be40aaa9a14c0e569bf99d';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1689ead8605be40aaa9a14c0e569bf99d';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr10d99a89e98fe43d99c1774128cb703ee';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr10d99a89e98fe43d99c1774128cb703ee';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr14264c070edf642a9b4e581c027bea52b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr14264c070edf642a9b4e581c027bea52b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr132415457c4944b5f8534c8419127caa0';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr132415457c4944b5f8534c8419127caa0';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1b61396cb288c467daaa48f8fc9cf37f3';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1b61396cb288c467daaa48f8fc9cf37f3';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1624609e5ffd44691b624c67993045d12';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1624609e5ffd44691b624c67993045d12';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr150243d87a57247d394f7a9fa3a6a891f';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr150243d87a57247d394f7a9fa3a6a891f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1dc413d81382a4e8d963ff9706543bf2b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1dc413d81382a4e8d963ff9706543bf2b';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1eaf09a807d1d4db5a0e2494b531aa5ae';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1eaf09a807d1d4db5a0e2494b531aa5ae';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1d2d60e11286149dabc4e3edca1371d37';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1d2d60e11286149dabc4e3edca1371d37';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr131468e6d706541478ea68a6233a27fb2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr131468e6d706541478ea68a6233a27fb2';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr18a9145f1221947ab8e7743f08f922c73';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr18a9145f1221947ab8e7743f08f922c73';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr12fba8e72f99043a6b307e043c2f777d9';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr12fba8e72f99043a6b307e043c2f777d9';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr145d19fdec5b34f0bb3c68162a5c0d63a';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr145d19fdec5b34f0bb3c68162a5c0d63a';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1a530ef4656c04506bd582f255cdeb174';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1a530ef4656c04506bd582f255cdeb174';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ede588201ad344db82fdcd710a279a36';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ede588201ad344db82fdcd710a279a36';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1e05653f21a5a4bb3bb544b05de44135e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1e05653f21a5a4bb3bb544b05de44135e';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr15b891caa5c35493099a601a0939c9782';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr15b891caa5c35493099a601a0939c9782';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr14250e89884c141fd88ffd13d01b55449';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr14250e89884c141fd88ffd13d01b55449';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10df067f23dce48d2a2f42fd975b2ff3a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10df067f23dce48d2a2f42fd975b2ff3a';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1724f860bb93e486b987200fc69ffee29';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1724f860bb93e486b987200fc69ffee29';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1cd9969a3d83e444a8ece3053f2420370';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1cd9969a3d83e444a8ece3053f2420370';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1f619e864aa424254988c0f27a337730d';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1f619e864aa424254988c0f27a337730d';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr185bbbf2e20da44a499c654a02ff9494c';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr185bbbf2e20da44a499c654a02ff9494c';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ba88eefe8fb94a53adbd5ab3e660ef61';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ba88eefe8fb94a53adbd5ab3e660ef61';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr104ee01b3f70a4f08bc0ef04277d4dba7';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr104ee01b3f70a4f08bc0ef04277d4dba7';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr110099b70a42047afbd2dc389c7e978ca';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr110099b70a42047afbd2dc389c7e978ca';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr19e66788e9d9b4d4fb71b2aea662e5f67';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr19e66788e9d9b4d4fb71b2aea662e5f67';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr14e450947c29b4049b62263f078150122';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr14e450947c29b4049b62263f078150122';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr133fb2c602df141a3a3a27932ec557211';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr133fb2c602df141a3a3a27932ec557211';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d5cf10efaf5f4ba3a92b6581e1524b2d';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d5cf10efaf5f4ba3a92b6581e1524b2d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr13f260618ae084958a7b3ddd0c33e51bd';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr13f260618ae084958a7b3ddd0c33e51bd';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ddbe0bd1120144ca96fb558bfecd5d91';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ddbe0bd1120144ca96fb558bfecd5d91';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1af6b1e963a334131b086236b521be50e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1af6b1e963a334131b086236b521be50e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr12ec5b1ff343f4ab1a4d20d527ac06629';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr12ec5b1ff343f4ab1a4d20d527ac06629';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1459a9b2c83dc4df8ab7d35adce00ce1f';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1459a9b2c83dc4df8ab7d35adce00ce1f';


UPDATE named_entity
SET category='personnalités et fiction'
WHERE named_entity.id_uuid = 'qr1b33eb1a986424aa48a0379fda9d555d7';


UPDATE named_entity
SET category_slug='personnalites-et-fiction'
WHERE named_entity.id_uuid = 'qr1b33eb1a986424aa48a0379fda9d555d7';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr16799cad4b8ee4d65ad1b04657f4607e5';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr16799cad4b8ee4d65ad1b04657f4607e5';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1f2a07c736738458a9019211313e0f250';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1f2a07c736738458a9019211313e0f250';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1aa45a267d6f34a4ea17c197fa8d468b9';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1aa45a267d6f34a4ea17c197fa8d468b9';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr111ae94a8cfa642ee9d02f6a3f11fbb88';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr111ae94a8cfa642ee9d02f6a3f11fbb88';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1da20ccf399b343f1b6ab17aaa510f585';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1da20ccf399b343f1b6ab17aaa510f585';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr19676423ed8d24c72ac83668dcfdc7e76';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr19676423ed8d24c72ac83668dcfdc7e76';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e9cc6c7741074a3da6c70762a7cde271';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e9cc6c7741074a3da6c70762a7cde271';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr16f8d2de369ba4671885473852ddef3b1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr16f8d2de369ba4671885473852ddef3b1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1b391efb6237d45ccab788f85ae7c1ec4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1b391efb6237d45ccab788f85ae7c1ec4';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1aab67ccc642b422c8e604d2235bda359';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1aab67ccc642b422c8e604d2235bda359';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1bd9bde3f24ee41ed80d727b56bce0600';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1bd9bde3f24ee41ed80d727b56bce0600';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr130d683090a03484992639b1bed607ac0';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr130d683090a03484992639b1bed607ac0';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr12bac5a9246b0468d967c7e121d0b3be5';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr12bac5a9246b0468d967c7e121d0b3be5';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr162d0c4c181694991a5a15ae033482bf7';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr162d0c4c181694991a5a15ae033482bf7';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10e82f54e7eff4baa8747572a42f75680';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10e82f54e7eff4baa8747572a42f75680';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1aedf779740a846299accda71833214a3';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1aedf779740a846299accda71833214a3';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr16eb289e8a6b4489da26c04f8aa091dab';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr16eb289e8a6b4489da26c04f8aa091dab';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1357e392d1e7041d189fd71785330ee1c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1357e392d1e7041d189fd71785330ee1c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr144398930334841329ad3f648dfbf24a9';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr144398930334841329ad3f648dfbf24a9';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr124c58e7be59948fcbbf1b0dc36332159';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr124c58e7be59948fcbbf1b0dc36332159';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr16323c53c153c47038e1adb33f0cc20b6';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr16323c53c153c47038e1adb33f0cc20b6';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1263d3c1cf93341559916f316a581fe90';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1263d3c1cf93341559916f316a581fe90';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1087b42b969a248b9af7e7c3a4c8f7346';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1087b42b969a248b9af7e7c3a4c8f7346';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14b3c095e94904639bf9688533eeaef2a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14b3c095e94904639bf9688533eeaef2a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1fde399dbc39d47dfa4da70a19c95250c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1fde399dbc39d47dfa4da70a19c95250c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr127cb4d9e7c5c4dbcae2b658945599e73';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr127cb4d9e7c5c4dbcae2b658945599e73';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1cb9a4a56b4154571a70947ced6c31a33';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1cb9a4a56b4154571a70947ced6c31a33';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr14ceffd3d069740689be5b144f59f7d46';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr14ceffd3d069740689be5b144f59f7d46';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1167ec2c91bb34aa18fa117b6265caad7';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1167ec2c91bb34aa18fa117b6265caad7';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1518549c46a8d4190a6babf35fde8053e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1518549c46a8d4190a6babf35fde8053e';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1c0d7e9daa42541459265ef6a5049dbf9';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1c0d7e9daa42541459265ef6a5049dbf9';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e887068f9e4a427eab05c8daf5a60a88';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e887068f9e4a427eab05c8daf5a60a88';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ce701493001a4b7c96703309165ff4b7';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ce701493001a4b7c96703309165ff4b7';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14ab5a2ac9d594dd9acf76a6df53ff617';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14ab5a2ac9d594dd9acf76a6df53ff617';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr124ccb4834ccc4244b16af6d79eab4162';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr124ccb4834ccc4244b16af6d79eab4162';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr15f2a6cdf56284006b3ef783e8c5dcf3c';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr15f2a6cdf56284006b3ef783e8c5dcf3c';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr1c2f526a828744e0095b47477f60f0279';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr1c2f526a828744e0095b47477f60f0279';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1f4aa0c755ee545cdb746f039742b0fab';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1f4aa0c755ee545cdb746f039742b0fab';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr112bfe7522e494d9681b860fac6b22d6c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr112bfe7522e494d9681b860fac6b22d6c';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1dcb2c82423104d2f98bbd9a6f07479f1';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1dcb2c82423104d2f98bbd9a6f07479f1';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1df36cfae3ca4486783a2ebba09519299';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1df36cfae3ca4486783a2ebba09519299';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10b885dd4c59b42d4940d9b2c5033dfa3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10b885dd4c59b42d4940d9b2c5033dfa3';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr130eac31ae6554adbae5cab6049810404';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr130eac31ae6554adbae5cab6049810404';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr181f5b30d3f434503a14180bd87dad3e4';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr181f5b30d3f434503a14180bd87dad3e4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1ccf850342acf4c3b83e180322c8faeff';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1ccf850342acf4c3b83e180322c8faeff';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10d433cd9f94d4ab8b3f053277da3b8cb';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10d433cd9f94d4ab8b3f053277da3b8cb';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr125650acd2f964f91b9c8805fe0cf0e5b';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr125650acd2f964f91b9c8805fe0cf0e5b';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr16fd24750a08646bd9da67a6286ef9ae5';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr16fd24750a08646bd9da67a6286ef9ae5';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10b03c4c59fa747b7b5235163d7005e52';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10b03c4c59fa747b7b5235163d7005e52';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ed0ce0f4c44b4d48bafe073e24f34dfe';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ed0ce0f4c44b4d48bafe073e24f34dfe';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr130a397d1163d41a0a10a804177c5f77e';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr130a397d1163d41a0a10a804177c5f77e';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr142a9c453568e4055abd61a598ee852b3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr142a9c453568e4055abd61a598ee852b3';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1534952532bff48de9ba47a77204f2229';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1534952532bff48de9ba47a77204f2229';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1deec18361fe34e8fae2b19c1292248d0';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1deec18361fe34e8fae2b19c1292248d0';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14d659c66732046b79bc4f6ab92eeab76';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14d659c66732046b79bc4f6ab92eeab76';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr19023d0e572504fe7a630c2b7029ed751';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr19023d0e572504fe7a630c2b7029ed751';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr132d8bcfa8dd34a9d873abc0c48a16f48';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr132d8bcfa8dd34a9d873abc0c48a16f48';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14ef43718a1f54736a05580ca665e2f0d';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14ef43718a1f54736a05580ca665e2f0d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10ea884be40d245b28661a124abe81c53';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10ea884be40d245b28661a124abe81c53';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c8067caf2b49468cb5b32533566521c8';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c8067caf2b49468cb5b32533566521c8';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr126d9cd59afae4f65913ce8acfdd0f9a3';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr126d9cd59afae4f65913ce8acfdd0f9a3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr16c1b9cd01baf4d4e874fb990852c40b5';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr16c1b9cd01baf4d4e874fb990852c40b5';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr196aa9280e5b04b4587accd6d15fa8c93';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr196aa9280e5b04b4587accd6d15fa8c93';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1e6d8592e8e9d4064a7eb0f3a95639dd5';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1e6d8592e8e9d4064a7eb0f3a95639dd5';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr121bd4280255643c885b06f33a82e3a8a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr121bd4280255643c885b06f33a82e3a8a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr10a2d0aa426f744ebb40c45b1dd58e7f1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr10a2d0aa426f744ebb40c45b1dd58e7f1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1fdd79de5ce8d4325b8aece1dce975450';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1fdd79de5ce8d4325b8aece1dce975450';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1998a9cf963674c7ebcbc6d6f70083a2b';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1998a9cf963674c7ebcbc6d6f70083a2b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr193ede2996c32465fb5f42f6d93a0de4d';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr193ede2996c32465fb5f42f6d93a0de4d';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr19d81d8ad6b524d739c271897386a13be';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr19d81d8ad6b524d739c271897386a13be';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10284607ad84a447597ed8d4f2283ce99';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10284607ad84a447597ed8d4f2283ce99';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr162935868fe984c3cb044439039f502b9';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr162935868fe984c3cb044439039f502b9';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr148a718aaca6b42fcb4399943163a101e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr148a718aaca6b42fcb4399943163a101e';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr11cf8f20f62e14b0f963bb01f06192f1b';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr11cf8f20f62e14b0f963bb01f06192f1b';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr103e44628d2874706ba9265aa2c81bec7';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr103e44628d2874706ba9265aa2c81bec7';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr165aff4e9f6a948038f39161ab3d2907a';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr165aff4e9f6a948038f39161ab3d2907a';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13f978ba738e04066acfc88bd4d33bf28';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13f978ba738e04066acfc88bd4d33bf28';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10ec6564b5ea24f4e8902f891d027c253';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10ec6564b5ea24f4e8902f891d027c253';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1278e6226579841a09c12e20a15e09586';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1278e6226579841a09c12e20a15e09586';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr15a39a542d41d4149b19efc8d9fd0ad7d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr15a39a542d41d4149b19efc8d9fd0ad7d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c6f9c4499a8b47efab4a9bbd47671869';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c6f9c4499a8b47efab4a9bbd47671869';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr110632f2356be4936b61e32f16069e9c4';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr110632f2356be4936b61e32f16069e9c4';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr154e9e1df60c5478099cad77e598d1e92';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr154e9e1df60c5478099cad77e598d1e92';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr15d030b5d00fe4ac0a076f3551723f2a1';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr15d030b5d00fe4ac0a076f3551723f2a1';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr129e9faf700fa4ddba81c74a219e1bfdf';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr129e9faf700fa4ddba81c74a219e1bfdf';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr18ebe7e09727844eb878ad6dda3a7fabe';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr18ebe7e09727844eb878ad6dda3a7fabe';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1d99630e333434e2498fd9c92960124e0';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1d99630e333434e2498fd9c92960124e0';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr125559358172c405ebefe4520f6f04bb6';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr125559358172c405ebefe4520f6f04bb6';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr175cdd4d88f2d423ea8d611857a47ab62';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr175cdd4d88f2d423ea8d611857a47ab62';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e1cb390e9032467c9ecb7a4857c4232e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e1cb390e9032467c9ecb7a4857c4232e';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f22465b5a5c94ce9bc45f041ca9e97dc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f22465b5a5c94ce9bc45f041ca9e97dc';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr140525d79b39146d9835f5d3005c45b80';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr140525d79b39146d9835f5d3005c45b80';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1667752e6687c4757928f1d4e9191790f';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1667752e6687c4757928f1d4e9191790f';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr15460b2e708ce4050869a1e3b28f7d8f8';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr15460b2e708ce4050869a1e3b28f7d8f8';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr110c17e015f394752b94ae038c8b74aab';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr110c17e015f394752b94ae038c8b74aab';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1d44ff9b0f2834ff2b34d5e98bf08de48';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1d44ff9b0f2834ff2b34d5e98bf08de48';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1b94d7caf5c064c748a1bec07a0a4507e';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1b94d7caf5c064c748a1bec07a0a4507e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1bdfcd2ca68144480879a332eb9cae89e';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1bdfcd2ca68144480879a332eb9cae89e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr13feff6ba7c044f94a7970d84c1e5e84a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr13feff6ba7c044f94a7970d84c1e5e84a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr186328c76dcf24bfe8e348eb745772f8f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr186328c76dcf24bfe8e348eb745772f8f';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1c6feb17d37ab483b90a16f838bf02486';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1c6feb17d37ab483b90a16f838bf02486';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr127feb17744b84251966b8b0e7a54ef2f';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr127feb17744b84251966b8b0e7a54ef2f';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1ef7a1fcd67ba4401bc24c50fc5b4e868';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1ef7a1fcd67ba4401bc24c50fc5b4e868';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr169878266a9684679875e52863d209e23';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr169878266a9684679875e52863d209e23';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c455e783ad3f413088f473078e3a6082';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c455e783ad3f413088f473078e3a6082';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr14c68962471694738b5bc02ccb0ecf447';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr14c68962471694738b5bc02ccb0ecf447';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr1015901cb2ffd4fcdb79be0d3321671cd';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr1015901cb2ffd4fcdb79be0d3321671cd';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr145e61a2244df416cb05f88246a5f6965';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr145e61a2244df416cb05f88246a5f6965';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr106a73bae31e7419aaaca20f743476c68';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr106a73bae31e7419aaaca20f743476c68';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1ca3cce1e190e4402bae57b3f7e2dc15e';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1ca3cce1e190e4402bae57b3f7e2dc15e';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1a0e92b9aed9c49aa88bb37b3693c32b2';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1a0e92b9aed9c49aa88bb37b3693c32b2';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f97b3911930f4c279bea60728e395ac3';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f97b3911930f4c279bea60728e395ac3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr122ad8cca876c4cd09ebb62010fac70ec';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr122ad8cca876c4cd09ebb62010fac70ec';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr12d7ce46ebbe7478b88b3c8384f03c7bc';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr12d7ce46ebbe7478b88b3c8384f03c7bc';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11713cce87ff64a25bb8ceab5e0d3b616';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11713cce87ff64a25bb8ceab5e0d3b616';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr13608571d415845a28a0c80b9c6ca9507';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr13608571d415845a28a0c80b9c6ca9507';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1cbf223fd355b411aa5471b5f2ee7a1e8';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1cbf223fd355b411aa5471b5f2ee7a1e8';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1818088653b464ae283e2031b1729d14f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1818088653b464ae283e2031b1729d14f';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1619d4991d0c64717865e7f66da6a8cd1';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1619d4991d0c64717865e7f66da6a8cd1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c7f117a16a394cecb1e0a6a74344b04c';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c7f117a16a394cecb1e0a6a74344b04c';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1c79ffe3c72d34322ba934328fd5af880';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1c79ffe3c72d34322ba934328fd5af880';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr166e8678c2aa8473ea134e722bff13611';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr166e8678c2aa8473ea134e722bff13611';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19529e0fdc0d340b39f34a4c48f364f9f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19529e0fdc0d340b39f34a4c48f364f9f';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr11e3fdeb8cd514ac0a0b6f636c2b16b39';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr11e3fdeb8cd514ac0a0b6f636c2b16b39';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ce9106a53942410192d70f7ad4f2ca21';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ce9106a53942410192d70f7ad4f2ca21';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr148a03a1991e648cca62a6c34bd36d6ae';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr148a03a1991e648cca62a6c34bd36d6ae';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19a39ab3936e044128e9f400fb805d11d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19a39ab3936e044128e9f400fb805d11d';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1f1292dc840fc44dcbd2eb4334f231710';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1f1292dc840fc44dcbd2eb4334f231710';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1aac0ea67648d44e9bbb513580b96c1fe';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1aac0ea67648d44e9bbb513580b96c1fe';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1456b9562d78e47d69d8e530c5cbd923d';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1456b9562d78e47d69d8e530c5cbd923d';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr144b286c842a34572937fa9d8b9e4c42a';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr144b286c842a34572937fa9d8b9e4c42a';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1655e15e2c113476d8b96ddac4ba7ba89';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1655e15e2c113476d8b96ddac4ba7ba89';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1ee01c344f5c84253aa02dee64ccbb692';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1ee01c344f5c84253aa02dee64ccbb692';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr12684d071813a4e32bf7dd1644f747ccb';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr12684d071813a4e32bf7dd1644f747ccb';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr186929c52bc68404bac6f1a8f0a90544f';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr186929c52bc68404bac6f1a8f0a90544f';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr15a479bed3a5e48bf86cc6886b3835bee';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr15a479bed3a5e48bf86cc6886b3835bee';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr19b0bcd878482428e86c2057d8141a443';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr19b0bcd878482428e86c2057d8141a443';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1cde2f934d8994018868813b73799d24c';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1cde2f934d8994018868813b73799d24c';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1cc4d60647ea34deb8d838091ea176a7e';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1cc4d60647ea34deb8d838091ea176a7e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ba123e7bf9ab47e7af3acde2fc8d311d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ba123e7bf9ab47e7af3acde2fc8d311d';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e05e0ac58e944d0b9a887f6861818511';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e05e0ac58e944d0b9a887f6861818511';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1da699da71cd546ac9c6776aa5a93622b';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1da699da71cd546ac9c6776aa5a93622b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1af00f7238a494d2ba9869aeacbe25055';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1af00f7238a494d2ba9869aeacbe25055';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr169af23423c6c4d5a944bcd1eaf284f12';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr169af23423c6c4d5a944bcd1eaf284f12';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11c652715781e427581999c44e0da34c3';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11c652715781e427581999c44e0da34c3';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr132369017203747b5a7aa6d484215f6c4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr132369017203747b5a7aa6d484215f6c4';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr18a6b2da1dc164c3a94de23e5fe6aa602';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr18a6b2da1dc164c3a94de23e5fe6aa602';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1aeda75bb33c249a1b22e82dfac121f37';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1aeda75bb33c249a1b22e82dfac121f37';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1c0ff02b94e924456ae9740538f445410';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1c0ff02b94e924456ae9740538f445410';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1c5d31949b0374c04bd97871587c8aa24';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1c5d31949b0374c04bd97871587c8aa24';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1b414f212588f475fadd8b477cf252669';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1b414f212588f475fadd8b477cf252669';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr186efa028d2f3405ca407e472425c29d1';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr186efa028d2f3405ca407e472425c29d1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1ec25f538827c478d8e92fb324edd21f4';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1ec25f538827c478d8e92fb324edd21f4';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr131d5bbe5f5b64805a91431911f4219cb';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr131d5bbe5f5b64805a91431911f4219cb';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1074f3f01a68b4768b9baf7ecb3ab1c0f';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1074f3f01a68b4768b9baf7ecb3ab1c0f';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr10cc757f1260649bb8b1af5fbcc272bb4';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr10cc757f1260649bb8b1af5fbcc272bb4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr137d5ac9d73dc4402a3f6ca056e7c3191';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr137d5ac9d73dc4402a3f6ca056e7c3191';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e837ee9a6bd84410b08a5734782a51a6';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e837ee9a6bd84410b08a5734782a51a6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr16012acd494624a9fb91c35fa6c200e83';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr16012acd494624a9fb91c35fa6c200e83';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1987fb85ad4ec4823adfc6404ed278cbe';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1987fb85ad4ec4823adfc6404ed278cbe';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr14a2d72ce90cd43158603f21ad9b194c3';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr14a2d72ce90cd43158603f21ad9b194c3';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11aa90d203bbe4b52bea31a00d838f51e';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11aa90d203bbe4b52bea31a00d838f51e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1cce2674e83634f838d999327ab8e8796';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1cce2674e83634f838d999327ab8e8796';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1c282d7b960fa4036ae616da24a865278';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1c282d7b960fa4036ae616da24a865278';


UPDATE named_entity
SET category='évènements'
WHERE named_entity.id_uuid = 'qr1315856bee0964f88bfb8d6c8447ee384';


UPDATE named_entity
SET category_slug='evenements'
WHERE named_entity.id_uuid = 'qr1315856bee0964f88bfb8d6c8447ee384';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1334fe13eeeab4541a7386583ebcf318e';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1334fe13eeeab4541a7386583ebcf318e';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr107eca9e257e14d76bf2d2d9338224439';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr107eca9e257e14d76bf2d2d9338224439';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr174afcddfc05e491da01558e4812e1e64';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr174afcddfc05e491da01558e4812e1e64';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr11e3caf7cf32c42af93fc664b6eccff9c';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr11e3caf7cf32c42af93fc664b6eccff9c';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1cabb211562d24484869ff5e9f3340a3f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1cabb211562d24484869ff5e9f3340a3f';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr13fb62d3477214e32bdfd2ab35d48c99e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr13fb62d3477214e32bdfd2ab35d48c99e';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12d25bd2760544ad29fc53069104b307c';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12d25bd2760544ad29fc53069104b307c';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr14b4c96042a724514b772db4f59433862';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr14b4c96042a724514b772db4f59433862';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr11f0c0c80ca474b80b82e60bccc59e7d1';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr11f0c0c80ca474b80b82e60bccc59e7d1';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr127b30b4decd84ccca176210ff75b0c16';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr127b30b4decd84ccca176210ff75b0c16';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1754285119420454d834a96ec7cc313e2';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1754285119420454d834a96ec7cc313e2';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr12fff154fdfe9402d9a8a8d674a3ba2da';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr12fff154fdfe9402d9a8a8d674a3ba2da';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1b05476b5f7644e27bd3f4215660f0389';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1b05476b5f7644e27bd3f4215660f0389';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1e3b07d4dff244a459962e00fe20f3af0';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1e3b07d4dff244a459962e00fe20f3af0';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1c6c58a5847d64a43bce51c3071730da8';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1c6c58a5847d64a43bce51c3071730da8';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1f0cdc87a18f4442d8307f569985659d0';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1f0cdc87a18f4442d8307f569985659d0';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1fce7031c63dd4a54ba48c2d0d2642d2d';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1fce7031c63dd4a54ba48c2d0d2642d2d';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1254b40e4fc5946aaaa3083220216400d';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1254b40e4fc5946aaaa3083220216400d';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1f9e7e7ab2d36435083613d38a9560fb8';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1f9e7e7ab2d36435083613d38a9560fb8';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr180381ec897154f4c8733d00f16b909e3';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr180381ec897154f4c8733d00f16b909e3';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr12968eefef74e4011a4df7b5a81365cb7';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr12968eefef74e4011a4df7b5a81365cb7';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1dee9ba7ec37844ac8299989be1968b6e';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1dee9ba7ec37844ac8299989be1968b6e';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1ad80915c068a4dfa8009946c85a70f54';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1ad80915c068a4dfa8009946c85a70f54';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1fb0cd490afa64ccfbd9a50eb4192a684';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1fb0cd490afa64ccfbd9a50eb4192a684';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1c5b48514b28941c289fa783480c589d3';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1c5b48514b28941c289fa783480c589d3';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1cde5be770cf74fa1aca01d5ef28c0cf7';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1cde5be770cf74fa1aca01d5ef28c0cf7';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1400e4ff517c34bec96c1ca039511a68b';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1400e4ff517c34bec96c1ca039511a68b';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr15391c0e40bbf494598b9c320db99fdcd';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr15391c0e40bbf494598b9c320db99fdcd';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1e67d652bc9de4861b27c9140a71044d3';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1e67d652bc9de4861b27c9140a71044d3';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr103daa481994b40ec96f25166df94ddcc';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr103daa481994b40ec96f25166df94ddcc';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1a7be5e513569454d995342d2438f1b13';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1a7be5e513569454d995342d2438f1b13';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1f2d16727967d4cd9ad3256d198f84af5';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1f2d16727967d4cd9ad3256d198f84af5';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1b936165c85624f0dbb529bcfc7c04637';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1b936165c85624f0dbb529bcfc7c04637';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr13a15ed87cb344fc7ac2e20fbd84ca91d';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr13a15ed87cb344fc7ac2e20fbd84ca91d';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1eac893b6bd6d421c8bf00b83a4dbba13';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1eac893b6bd6d421c8bf00b83a4dbba13';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr150373215fb414d6293d8bc8cceea0646';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr150373215fb414d6293d8bc8cceea0646';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr19add2fa1e370410c9fac1ae2b90ae186';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr19add2fa1e370410c9fac1ae2b90ae186';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr11be0d38f684a4b8284036f308d61dfab';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr11be0d38f684a4b8284036f308d61dfab';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1516df93d37614811870ccba88b4c6206';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1516df93d37614811870ccba88b4c6206';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1e12681a401bf4a16b82b6516038e5d90';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1e12681a401bf4a16b82b6516038e5d90';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1fa139d1ec940430690c078a49e047d35';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1fa139d1ec940430690c078a49e047d35';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1638270fee2f941d0a848bb49f8927a1b';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1638270fee2f941d0a848bb49f8927a1b';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr15f9f002f1fc34ee79a079b5841a16fa2';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr15f9f002f1fc34ee79a079b5841a16fa2';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr151158f89f835465d88129ed37ce24d3b';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr151158f89f835465d88129ed37ce24d3b';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d385d2ae290446e481f3b07c073193ac';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d385d2ae290446e481f3b07c073193ac';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1678955361aea4d228e879c2842448726';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1678955361aea4d228e879c2842448726';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1003516f7e37140f9914ff1c1a3cb2ae6';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1003516f7e37140f9914ff1c1a3cb2ae6';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11c1ef31e1f064504b61b799c1ebeadf5';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11c1ef31e1f064504b61b799c1ebeadf5';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1e103712cfa324199b661aec7357c0b42';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1e103712cfa324199b661aec7357c0b42';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr10bad0f07c9294667ad9da2e75fd6ce2a';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr10bad0f07c9294667ad9da2e75fd6ce2a';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr17195ba42b91846dfa62756ab9b36e220';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr17195ba42b91846dfa62756ab9b36e220';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr1490777ad064c445aaf1b751179c2ebed';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr1490777ad064c445aaf1b751179c2ebed';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr153b52e351cc14ada930701a5117aa08a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr153b52e351cc14ada930701a5117aa08a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c6b3c9a08f634277b7e9c7bcf029d770';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c6b3c9a08f634277b7e9c7bcf029d770';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1e11ab5c46d61413a8a03058a59173c0d';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1e11ab5c46d61413a8a03058a59173c0d';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr156abf3d1e03647c2b52be6d072b72a2f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr156abf3d1e03647c2b52be6d072b72a2f';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1edff2815cea6498fa085e5c1407e1b4e';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1edff2815cea6498fa085e5c1407e1b4e';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1af075bd48af44707bfedbb9588a16dc3';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1af075bd48af44707bfedbb9588a16dc3';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr117003c31b58541369571454051810cb1';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr117003c31b58541369571454051810cb1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr11b6fe0900f0b43e5b3510937fd93516e';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr11b6fe0900f0b43e5b3510937fd93516e';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr165c40ade7fd5402aa22eb0195221e38f';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr165c40ade7fd5402aa22eb0195221e38f';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr173483d3729e04b90b1b31c71d0ee0d83';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr173483d3729e04b90b1b31c71d0ee0d83';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr13e1e86a7bac7491c8d1724652b3c303e';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr13e1e86a7bac7491c8d1724652b3c303e';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr11b9ef732cfaf4a4094fd9b536f34ed56';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr11b9ef732cfaf4a4094fd9b536f34ed56';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr16dc088f9525f4ffaa0b7fb5d35d3cc7b';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr16dc088f9525f4ffaa0b7fb5d35d3cc7b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr19cf124aa30a0412bbd1c056aaf7c5949';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr19cf124aa30a0412bbd1c056aaf7c5949';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr129412e07ac7c4eef9aae6a0cbec61ee8';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr129412e07ac7c4eef9aae6a0cbec61ee8';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1a2ac758efe8c40ab9f9917e52f0e8f27';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1a2ac758efe8c40ab9f9917e52f0e8f27';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1cf322988016547ff84446517d4a9a93a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1cf322988016547ff84446517d4a9a93a';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17f5daa8e9bbc4a66ba26a4e3f9ea4a53';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17f5daa8e9bbc4a66ba26a4e3f9ea4a53';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr11250a1ff2fad475b974f0af54402bbc6';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr11250a1ff2fad475b974f0af54402bbc6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1f37670f4e8ce491d83419565310e508d';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1f37670f4e8ce491d83419565310e508d';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1bac2aae9b3ec446bbfa7345e8872b8cc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1bac2aae9b3ec446bbfa7345e8872b8cc';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f64927b0465e4f88822699f7059d0827';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f64927b0465e4f88822699f7059d0827';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1ea57e2eca5db4db69623f97255d3eb75';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1ea57e2eca5db4db69623f97255d3eb75';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17e2348c6f5fe4f2c84963cca22bc59d5';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17e2348c6f5fe4f2c84963cca22bc59d5';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1f848ab7dbd434144a66cf5e46a4369d7';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1f848ab7dbd434144a66cf5e46a4369d7';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19447472dcc1245c28e2e3f7c58eca114';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19447472dcc1245c28e2e3f7c58eca114';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr17a1254be58d348e8bfc071bb65324757';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr17a1254be58d348e8bfc071bb65324757';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr14b55b98a95f44ba59abb075425e52c56';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr14b55b98a95f44ba59abb075425e52c56';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr19a026356f1644aa88c993279fec09c0a';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr19a026356f1644aa88c993279fec09c0a';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1a2ea0ad9969d46cb8910d08452541bb7';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1a2ea0ad9969d46cb8910d08452541bb7';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr12f6a4f79a76e427d8a0c25af8545b826';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr12f6a4f79a76e427d8a0c25af8545b826';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1752304c431814d13acbe610ad88d4bc0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1752304c431814d13acbe610ad88d4bc0';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr19a0ff94df7b1443cbd6d4d1a1c5f8cce';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr19a0ff94df7b1443cbd6d4d1a1c5f8cce';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1b1ff08cc67d64598b85807439f655705';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1b1ff08cc67d64598b85807439f655705';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr195d7aa7e0f7248768e53cb34608da2d0';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr195d7aa7e0f7248768e53cb34608da2d0';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1de582bbafc3f400a81bc3e30fb6c7d64';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1de582bbafc3f400a81bc3e30fb6c7d64';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr18788058968224c7abc70838b7487ebf0';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr18788058968224c7abc70838b7487ebf0';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1497cd8ad96ed4d7594c7d57158f4b526';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1497cd8ad96ed4d7594c7d57158f4b526';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d6933c49a10e42a1b2969d22d39ea06c';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d6933c49a10e42a1b2969d22d39ea06c';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr122b540f775c74c76ae06f15a8f394c33';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr122b540f775c74c76ae06f15a8f394c33';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr16a79673f24c24a2bba5498065c6dcdbd';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr16a79673f24c24a2bba5498065c6dcdbd';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr10d99538823d74ec3adafacfa587680d4';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr10d99538823d74ec3adafacfa587680d4';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d54d279ac87d4319937c1190ca858d21';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d54d279ac87d4319937c1190ca858d21';


UPDATE named_entity
SET category='ville et architecture'
WHERE named_entity.id_uuid = 'qr17ad41e72b98843158d88bab559423d7b';


UPDATE named_entity
SET category_slug='ville-et-architecture'
WHERE named_entity.id_uuid = 'qr17ad41e72b98843158d88bab559423d7b';


UPDATE named_entity
SET category='institutions et organisations'
WHERE named_entity.id_uuid = 'qr1417f4347962447999be988f7c06f8e41';


UPDATE named_entity
SET category_slug='institutions-et-organisations'
WHERE named_entity.id_uuid = 'qr1417f4347962447999be988f7c06f8e41';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1f418725914cc433788219133ac7629dc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1f418725914cc433788219133ac7629dc';


UPDATE named_entity
SET category='acteurs et actrices'
WHERE named_entity.id_uuid = 'qr1d7d3a5ef18f649f0b62fc6e76f1347cc';


UPDATE named_entity
SET category_slug='acteurs-et-actrices'
WHERE named_entity.id_uuid = 'qr1d7d3a5ef18f649f0b62fc6e76f1347cc';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr121339a56588b47e8aeff207259286e68';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr121339a56588b47e8aeff207259286e68';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1c6ef3895a2c946dfaae3f8eaf5375ea2';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1c6ef3895a2c946dfaae3f8eaf5375ea2';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr162a9ceca5fe3416896c4ac334f69e475';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr162a9ceca5fe3416896c4ac334f69e475';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr100560d50b6d04320ab93d11e89200a88';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr100560d50b6d04320ab93d11e89200a88';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr168b78205ba374fb8b8fe97169daf3ae3';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr168b78205ba374fb8b8fe97169daf3ae3';


UPDATE named_entity
SET category='théâtres et spectacles'
WHERE named_entity.id_uuid = 'qr1d9dfd6c394ec496684aa5f2b67540d77';


UPDATE named_entity
SET category_slug='theatres-et-spectacles'
WHERE named_entity.id_uuid = 'qr1d9dfd6c394ec496684aa5f2b67540d77';


UPDATE named_entity
SET category='santé'
WHERE named_entity.id_uuid = 'qr19392aaf4819443b1a78a0150a8376441';


UPDATE named_entity
SET category_slug='sante'
WHERE named_entity.id_uuid = 'qr19392aaf4819443b1a78a0150a8376441';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e3cb68e4c8ae460798b9c276f63307c1';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e3cb68e4c8ae460798b9c276f63307c1';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr17e83ab0c9d34410c9ffbe9443881c104';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr17e83ab0c9d34410c9ffbe9443881c104';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ab7dbdf379aa49c5b0f834fa42aec71b';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ab7dbdf379aa49c5b0f834fa42aec71b';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e1c84360f9f648258a944709c54bb0ac';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e1c84360f9f648258a944709c54bb0ac';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr116a56caae317483bbe54bc2296990752';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr116a56caae317483bbe54bc2296990752';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1a220854872d14b94ba3679d5f71a5e3a';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1a220854872d14b94ba3679d5f71a5e3a';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1aa86f6305811464ea38c42513b754cdd';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1aa86f6305811464ea38c42513b754cdd';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1d7baca696faa414fb75f30e9e3d93e98';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1d7baca696faa414fb75f30e9e3d93e98';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1e7c361f87f4d434f997244b73c1fdcc8';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1e7c361f87f4d434f997244b73c1fdcc8';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1ffbc3a9bf7044378a42e05af66302d95';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1ffbc3a9bf7044378a42e05af66302d95';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1d0f59aa2d8d44a36b6db155e44397997';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1d0f59aa2d8d44a36b6db155e44397997';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr19712ff8113cd42008320eb2839b5e35b';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr19712ff8113cd42008320eb2839b5e35b';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr11960b109bbcc421382b25cf2b55519f6';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr11960b109bbcc421382b25cf2b55519f6';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ead36f1fd08a466d9e1930d3537ed475';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ead36f1fd08a466d9e1930d3537ed475';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1a5ad1c30f142484994b7bbbb9a67d08f';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1a5ad1c30f142484994b7bbbb9a67d08f';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr18ce724281a404c149d38f578905507e5';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr18ce724281a404c149d38f578905507e5';


UPDATE named_entity
SET category='commerces'
WHERE named_entity.id_uuid = 'qr1f13812d66eac43abba89a1dca4a19220';


UPDATE named_entity
SET category_slug='commerces'
WHERE named_entity.id_uuid = 'qr1f13812d66eac43abba89a1dca4a19220';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1ebc3f541bd4d4e7697a44bf35d4d6ca1';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1ebc3f541bd4d4e7697a44bf35d4d6ca1';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr16aa20e9efb3246b6be3386d2b973d825';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr16aa20e9efb3246b6be3386d2b973d825';


UPDATE named_entity
SET category='banques'
WHERE named_entity.id_uuid = 'qr11c5803fa9aa74c918a961ea02b277f26';


UPDATE named_entity
SET category_slug='banques'
WHERE named_entity.id_uuid = 'qr11c5803fa9aa74c918a961ea02b277f26';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr1a86353f5c9704f56ae0217d4e128baf7';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr1a86353f5c9704f56ae0217d4e128baf7';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1fa5a9d0a13ff4d36ba07654377e653e5';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1fa5a9d0a13ff4d36ba07654377e653e5';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1c9b210656416438483199ed703e91fa1';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1c9b210656416438483199ed703e91fa1';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr15048bb61653643338b9145c00cb292fc';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr15048bb61653643338b9145c00cb292fc';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr119dc4662f50048adb6ba46263c124fac';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr119dc4662f50048adb6ba46263c124fac';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr16fe44eacb55b4788b41de517ec8fa913';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr16fe44eacb55b4788b41de517ec8fa913';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr14dd6adbb34c34d088053e274de0cdb2a';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr14dd6adbb34c34d088053e274de0cdb2a';


UPDATE named_entity
SET category='mode et objets'
WHERE named_entity.id_uuid = 'qr1b6f68f38d51d411282e8b4cc03a649ef';


UPDATE named_entity
SET category_slug='mode-et-objets'
WHERE named_entity.id_uuid = 'qr1b6f68f38d51d411282e8b4cc03a649ef';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr160a7904d8f6e41d2995955f8d74f5522';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr160a7904d8f6e41d2995955f8d74f5522';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1e913e628a30c4b69b96e2a9387b59b78';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1e913e628a30c4b69b96e2a9387b59b78';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr10a8f71fcc5504e83a4f39d6725840320';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr10a8f71fcc5504e83a4f39d6725840320';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1fc9ff4b37dc74cb696142f805d2f1fb2';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1fc9ff4b37dc74cb696142f805d2f1fb2';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d0899956585c4454ae5c7169061ba857';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d0899956585c4454ae5c7169061ba857';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr1d237d97fe8ff4f199e7be1ca3e832dd0';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr1d237d97fe8ff4f199e7be1ca3e832dd0';


UPDATE named_entity
SET category='cafés et restaurants'
WHERE named_entity.id_uuid = 'qr103e86edef03d43cebbf5a67e7be6c9e4';


UPDATE named_entity
SET category_slug='cafes-et-restaurants'
WHERE named_entity.id_uuid = 'qr103e86edef03d43cebbf5a67e7be6c9e4';


UPDATE named_entity
SET category='publications et photographies'
WHERE named_entity.id_uuid = 'qr1bc21dc24d79e4342859e16c186715530';


UPDATE named_entity
SET category_slug='publications-et-photographies'
WHERE named_entity.id_uuid = 'qr1bc21dc24d79e4342859e16c186715530';


UPDATE named_entity
SET category='épiceries et alimentation'
WHERE named_entity.id_uuid = 'qr19682ba45215a4c81a45583eb4c836011';


UPDATE named_entity
SET category_slug='epiceries-et-alimentation'
WHERE named_entity.id_uuid = 'qr19682ba45215a4c81a45583eb4c836011';

--set not-null constraint after updating
 -- this also allows to make sure that all
 -- rows have a category before committing

ALTER TABLE IF EXISTS named_entity
ALTER COLUMN category
SET NOT NULL;


ROLLBACK;