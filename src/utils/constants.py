import os
from sqlalchemy.dialects import postgresql


# file paths

UTILS         = os.path.join(os.path.abspath(os.path.dirname(__file__)))
SRC           = os.path.join(UTILS, os.pardir)
ORM           = os.path.join(SRC, "orm/")
ROOT          = os.path.join(SRC, os.pardir)
IN            = os.path.join(ROOT, "in/")
OUT           = os.path.join(ROOT, "out/")
CONFIDENTIALS = os.path.join(ROOT, "confidentials/")

COMPILE_PARAMS = { "dialect": postgresql.dialect(),
                   "compile_kwargs": { "literal_binds": True } }
