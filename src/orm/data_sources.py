from sqlalchemy.orm import Mapped, mapped_column, relationship, validates
from sqlalchemy.dialects import postgresql as psql
from sqlalchemy import ForeignKey, Text, Float, Boolean
from typing import List, Dict
import intervals

from ..utils.strings import _validate_uuid
from . import Base


# -----------------------------------------------------------------
# tables describing external ressources gathered by the project.
# these are the "main" tables, since they contain all of our
# documentary research
#
# contains
# ~~~~~~~~
# * `Iconography`: iconographic ressources (paintings, print...)
# * `Cartography`: cartographic ressources
# * `Filename`: external files, especially images of iconographic
#   and cartographic ressources
# * `Directory`: the Bottins et Annuaires.
# -----------------------------------------------------------------


class Iconography(Base):
    """
    main class for the iconography, describing image and documents
    presented on the website.
    """
    __tablename__: str = "iconography"

    id               : Mapped[int]       = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid          : Mapped[str]       = mapped_column(Text, nullable=False)
    id_richelieu     : Mapped[str]       = mapped_column(Text, nullable=False)
    iiif_url         : Mapped[str]       = mapped_column(Text, nullable=True)
    iiif_folio       : Mapped[List[int]] = mapped_column(psql.ARRAY(psql.INTEGER, dimensions=1), nullable=True)
    source_url       : Mapped[str]       = mapped_column(Text, nullable=True)
    date_source      : Mapped[str]       = mapped_column(Text, nullable=True)
    date_corr        : Mapped[str]       = mapped_column(Text, nullable=True)
    date             : Mapped[List[int]] = mapped_column(psql.INT4RANGE, nullable=True)
    technique        : Mapped[List[str]] = mapped_column(psql.ARRAY(Text, dimensions=1), nullable=True)
    description      : Mapped[str]       = mapped_column(Text, nullable=True)
    inscription      : Mapped[str]       = mapped_column(Text, nullable=True)
    corpus           : Mapped[str]       = mapped_column(Text, nullable=True)
    inventory_number : Mapped[str]       = mapped_column(Text, nullable=True)
    produced         : Mapped[bool]      = mapped_column(Boolean, nullable=True)
    represents       : Mapped[bool]      = mapped_column(Boolean, nullable=True)
    id_licence       : Mapped[int]       = mapped_column(psql.INTEGER, ForeignKey("licence.id"), nullable=False)

    title                      : Mapped[List["Title"]]                    = relationship("Title", back_populates="iconography")
    annotation                 : Mapped[List["Annotation"]]               = relationship("Annotation", back_populates="iconography")
    filename                   : Mapped[List["Filename"]]                 = relationship("Filename", back_populates="iconography")
    licence                    : Mapped["Licence"]                        = relationship("Licence", back_populates="iconography")
    r_iconography_theme        : Mapped[List["R_IconographyTheme"]]       = relationship("R_IconographyTheme", back_populates="iconography")
    r_iconography_actor        : Mapped[List["R_IconographyActor"]]       = relationship("R_IconographyActor", back_populates="iconography")
    r_iconography_place        : Mapped[List["R_IconographyPlace"]]       = relationship("R_IconographyPlace", back_populates="iconography")
    r_iconography_named_entity : Mapped[List["R_IconographyNamedEntity"]] = relationship("R_IconographyNamedEntity", back_populates="iconography")
    r_admin_person             : Mapped[List["R_AdminPerson"]]            = relationship("R_AdminPerson", back_populates="iconography")
    r_institution              : Mapped[List["R_Institution"]]            = relationship("R_Institution", back_populates="iconography")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)


class Cartography(Base):
    """
    class describing our cartographic ressources

    * `vector` is the shape of the place on a cartographic space. it
      is stored as a JSON (the structure is a GeoJSON geometry multipolygon)
    """
    __tablename__: str = "cartography"

    id               : Mapped[int]                   = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid          : Mapped[str]                   = mapped_column(Text, nullable=False)
    source_url       : Mapped[str]                   = mapped_column(Text, nullable=True)
    title            : Mapped[str]                   = mapped_column(Text, nullable=True)
    date_source      : Mapped[str]                   = mapped_column(Text, nullable=True)
    inventory_number : Mapped[str]                   = mapped_column(Text, nullable=True)
    date             : Mapped[intervals.IntInterval] = mapped_column(psql.INT4RANGE, nullable=True)
    vector           : Mapped[Dict]                  = mapped_column(psql.JSON, nullable=False)
    crs_epsg         : Mapped[int]                   = mapped_column(psql.INTEGER, nullable=False)
    granularity      : Mapped[str]                   = mapped_column(Text, nullable=False)
    map_source       : Mapped[str]                   = mapped_column(Text, nullable=False)
    id_licence       : Mapped[int]                   = mapped_column(psql.INTEGER, ForeignKey("licence.id"), nullable=False)

    r_cartography_place : Mapped[List["R_CartographyPlace"]] = relationship("R_CartographyPlace", back_populates="cartography")
    r_admin_person      : Mapped[List["R_AdminPerson"]]      = relationship("R_AdminPerson", back_populates="cartography")
    r_institution       : Mapped[List["R_Institution"]]      = relationship("R_Institution", back_populates="cartography")
    filename            : Mapped[List["Filename"]]           = relationship("Filename", back_populates="cartography")
    licence             : Mapped["Licence"]                  = relationship("Licence", back_populates="cartography")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)


class Directory(Base):
    """
    the third data source: directory (Bottins et Annuaires),
    mapping persons to an address and to the activity they
    practiced there. the Bottins were extracted from Gallica.
    """
    __tablename__ = "directory"

    id           : Mapped[int]       = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid      : Mapped[str]       = mapped_column(Text, nullable=False)
    gallica_ark  : Mapped[str]       = mapped_column(Text, nullable=False)
    gallica_page : Mapped[str]       = mapped_column(Text, nullable=False)  # the page on the directory
    gallica_row  : Mapped[str]       = mapped_column(Text, nullable=False)  # the page row in the directory
    entry_name   : Mapped[str]       = mapped_column(Text, nullable=False)
    occupation   : Mapped[str]       = mapped_column(Text, nullable=False)
    date         : Mapped[str]       = mapped_column(psql.INT4RANGE, nullable=False)
    tags         : Mapped[List[str]] = mapped_column(psql.ARRAY(Text, dimensions=1), nullable=True)
    id_address   : Mapped[int]       = mapped_column(psql.INTEGER, ForeignKey("address.id"), nullable=False)
    id_licence   : Mapped[int]       = mapped_column(psql.INTEGER, ForeignKey("licence.id"), nullable=False)

    address        : Mapped["Address"]             = relationship("Address", back_populates="directory")
    licence        : Mapped["Licence"]             = relationship("Licence", back_populates="directory")
    r_admin_person : Mapped[List["R_AdminPerson"]] = relationship("R_AdminPerson", back_populates="directory")
    r_institution  : Mapped[List["R_Institution"]] = relationship("R_Institution", back_populates="directory")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)


class Filename(Base):
    """
    class containing data on image and other external files (reproduction
    of iconographic ressources or cartographic ressources.
    """
    __tablename__ = "filename"

    id             : Mapped[int]                      = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid        : Mapped[str]                      = mapped_column(Text, nullable=False)
    url            : Mapped[str]                      = mapped_column(Text, nullable=False)
    latlngbounds   : Mapped[List[List[float]] | None] = mapped_column(psql.ARRAY(Float, dimensions=2), nullable=True)  # [ [<float>,<float>], [<float>,<float>] ]
    id_licence     : Mapped[int]                      = mapped_column(psql.INTEGER, ForeignKey("licence.id"), nullable=True)
    id_iconography : Mapped[int]                      = mapped_column(psql.INTEGER, ForeignKey("iconography.id"), nullable=True)
    id_cartography : Mapped[int]                      = mapped_column(psql.INTEGER, ForeignKey("cartography.id"), nullable=True)

    licence     : Mapped["Licence"]     = relationship("Licence", back_populates="filename")
    iconography : Mapped["Iconography"] = relationship("Iconography", back_populates="filename")
    cartography : Mapped["Cartography"] = relationship("Cartography", back_populates="filename")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)


