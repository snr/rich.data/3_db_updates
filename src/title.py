"""
for some obsure reason, leading/trailing spaces in title.entry_name
have not been stripped. here, we generate a script mapping each
title.id_uuid to a clean title, in order to run the updates
"""

from sqlalchemy.engine import Engine
from sqlalchemy import update
import pandas as pd
import sqlparse

from datetime import datetime
import re
import os

from .orm.qualifiers import Title
from .utils.constants import COMPILE_PARAMS, OUT

# *******************************************

title_cleaner = lambda col: (col.str.replace(r"[\n\t]+", " ", regex=True)      # remove tabs and linejumps
                                .str.replace(r"\s+", " ", regex=True)         # remove double spaces
                                .str.replace(r"(^\s+|\s+$)", "", regex=True)  # strip leading/trailing spaces
                            )

def pipeline(engine:Engine):
    engine = engine
    df_title = pd.read_sql("SELECT * FROM TITLE;", engine)

    # cleaning
    df_title["entry_name_clean"] = df_title.entry_name.pipe(title_cleaner)
    n = df_title.loc[ df_title.entry_name.ne(df_title.entry_name_clean) ].shape[0]
    print(f"* title cleaning : {n} rows affected")

    # build the sql query
    mapper = df_title[["id_uuid", "entry_name_clean"]].to_dict(orient="records")
    updates = []
    for mapping in mapper:
        updates.append(update( Title )
                      .where( Title.id_uuid==mapping["id_uuid"] )
                      .values( entry_name=mapping["entry_name_clean"] ))

    query      = "BEGIN;" + "\n".join( str(u.compile( **COMPILE_PARAMS ))
                                       for u in updates )
    query      = re.sub(r"(UPDATE[^\n]+)", r"\1;", query)
    test_query = sqlparse.format(query + " ROLLBACK; ", keyword="upper", reindent=True)
    query      = sqlparse.format(query + " COMMIT; ", keyword="upper", reindent=True)

    # write to output
    d = datetime.now().strftime(r"%Y%m%d")
    with open(os.path.join(OUT, f"{d}_title_update_test.sql"), mode="w") as fh:
        fh.write(test_query)
    with open(os.path.join(OUT, f"{d}_title_update.sql"), mode="w") as fh:
        fh.write(query)

    return
