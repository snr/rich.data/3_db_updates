"""
for the tables `theme` and `named_entity`, generate 2 new columns:
- category (TEXT NOT NULL)
    a broader category to which the entry belongs. this is
    obtained from a mapping table produced manually and found in `IN`
- category_slug (TEXT NOT NULL)
    a slugified and url-safe version of the above:
    in the app, `category` is used to build urls.
"""

from sqlalchemy.engine import Engine
from sqlalchemy import exc as sa_exc
from sqlalchemy import update
from unidecode import unidecode  # type:ignore
from datetime import datetime
import pandas as pd
import numpy as np
import typing as t
import sqlparse
import os
import re

from .orm import NamedEntity, Theme
from .utils.constants import COMPILE_PARAMS, IN, OUT


# simplify the string series `col`. to be used with `df.col.pipe()`
simplify_col = lambda col: (col.apply(unidecode)                         # normalize accents
                               .str.replace(r"œ", "oe")
                               .str.replace(r"[^\w ]+", "", regex=True)  # keep only alphanumeric chars and spaces
                               .str.replace(r"\s+", " ", regex=True)
                               .str.lower()
                               .str.strip())

# produces a url-safe version of string series `col`
# (unaccents + removes all characters that aren't allowed in urls)
slugify_col = lambda col: (col.apply(unidecode)
                              .str.replace(r"[\u0300-\u036f]", "", regex=True)
                              .str.replace(r"[!\"#$%&'()*+,./:;<=>?@[\]^`{|}~]", "-", regex=True)
                              .str.replace(r"[\s_]", "-", regex=True))


def prepare_df(df:pd.DataFrame) -> pd.DataFrame:
    """
    clean and prepare a df read from IN/.
    this is a generic function used for the named_entity and theme tables.

    take as an input a dataframe with a loose structure,
    (obtained by transposing the xlsx table): columns regroup
    entries within different categories (for example, category
    "s'habiller" will contain everything in relation to that category).

    from that, simplify to obtain a dataframe with 2 columns:
    > entry_name str : a theme name as a string
    > category   str : the category it belongs to as a scalar.
    basically, we return a mapper { <theme name>: <category> }.

    :param df     : the dataframe to clean
    """
    df = (df.dropna(axis=0, how="all")            # drop empty rows
            .transpose())                         # rotate: columns become rows
    df["entry_name"] = df.values.tolist()         # concatenate all columns to list
    df = (df.reset_index(names="category")        # the column headers (categories) were turned into an index. save them as a normal column
            [["entry_name", "category"]]          # drop useless columns + empty rows
            .explode(column="entry_name")         # explode to have a mapper of { category(str) : entry_name(str) }
            .dropna(axis=0, subset="entry_name")  # drop empty rows
            )

    # from now on, the dataframe has a straightforward structure:
    # { <category (str)> : <entry_name (str)> }
    df.entry_name = df.entry_name.pipe(simplify_col)
    df.category   = df.category.str.lower().str.replace("’", '\'').str.strip()
    df            = df.drop_duplicates(keep="first", ignore_index=True)
    df.loc[ ~df.apply(lambda x: x.entry_name == x.category, axis=1) ]  # transposing can copy the column names as cells => pop those rows

    return df


def read_and_prepare() -> t.List[pd.DataFrame]:
    """
    read the dataframes in IN/, prepare them and clean them
    """
    df_theme = (pd.read_excel( os.path.join(IN, "category_theme_20240830.xlsx")
                            , sheet_name=0
                            , index_col=None))
    df_named_entity = pd.read_excel( os.path.join(IN, "category_named_entity_20240830.xlsx")
                                   , sheet_name=0
                                   , index_col=None)

    # process the dfs
    df_theme = prepare_df(df_theme, "df_theme")
    df_named_entity = prepare_df(df_named_entity, "df_named_entity")

    return [ df_theme, df_named_entity ]


def sql_and_prepare(engine:Engine) -> t.List[pd.DataFrame]:
    """
    read the `theme` and `named_entity` tables into dataframes
    """
    df_sql_theme = pd.read_sql( """SELECT theme.id
                                        , theme.id_uuid
                                        , theme.entry_name
                                  FROM theme;"""
                              , engine
                              , columns=["id", "id_uuid", "entry_name"])
    df_sql_named_entity = pd.read_sql( """SELECT named_entity.id
                                               , named_entity.id_uuid
                                               , named_entity.entry_name
                                          FROM named_entity;"""
                                     , engine
                                     , columns=["id", "id_uuid", "entry_name"])

    df_sql_theme.entry_name = df_sql_theme.entry_name.pipe(simplify_col)
    df_sql_named_entity.entry_name = df_sql_named_entity.entry_name.pipe(simplify_col)
    return [ df_sql_theme, df_sql_named_entity ]


def join_and_verify( df_sql:pd.DataFrame
                   , df_read:pd.DataFrame
                   , tablename:str
                   ) -> t.List[bool|pd.DataFrame]:
    """
    join `df_sql` and `df_read`.
    make sure that we'll be able to map every entry of `df_sql`
    to a category in `df_read`. return a bool (True if validations passed,
    False otherwise) and the jointed dataframe.

    validation requires that:
    1) the data in `df_read` is valid (no `df.entry_names` are mapped to >1 category)
    2) all values in `df_read` can be joined to a value in `df_sql`
    3) all values in `df_sql` have an equivalent in `df_read`.

    if there are errors, write them to `OUT/category_verify_*`

    joinded output dataframe structure:
    > id            int : SQL id for the row
    > id_uuid       str : SQL id_uuid for the row
    > entry_name    str : simplified entry name
    > category      str : the category to which this row belongs
    > category_slug str : a slugified, url-safe version of `category`, which will be used to build urls.

    :param    df_sql: a simplified dataframe obtained for an sql table
    :param   df_read: a simplified df obtained by processing an input XLSX
    :param tablename: the name of the sql table we're working on
    """
    success = True  # will be switched to false if's there a problem, will stop the script

    # 1) assert that df_read is valid.
    #    (no duplicate rows, no `df.entry_name` associated to more than 1 category)
    test   = df_read.duplicated(keep="first").sum() == 0
    df_err = df_read.loc[ df_read.duplicated(keep=False) ]
    msg    = (f"\n* ON `{tablename}` : {df_read.duplicated(keep=False).sum()} "
               "duplicate rows were found but are not allowed : \n"\
               f"{df_err}\n")
    if not test:
        success = False
        print(msg)
        df_err.to_excel(os.path.join(OUT, f"category_verify_duplicate_rows_{tablename}.xlsx"))

    test   = df_read.duplicated(subset="entry_name").sum() == 0
    df_err = df_read.loc[ df_read.duplicated(subset='entry_name', keep=False) ]
    msg    = (f"\n* ON `{tablename}` : { df_read.duplicated(subset='entry_name').sum() } "\
              + "entry names have been associated with more than one category ! \n"\
              + f"{df_err}\n")
    if not test:
        success = False
        print(msg)
        df_err.to_excel(os.path.join(OUT, f"category_verify_multiple_category_{tablename}.xlsx"))

    # 2) all values in `df_read` are present in `df_sql`
    df_read2sql = df_read.merge( df_sql
                               , how="left"
                               , on="entry_name"
                               , suffixes=("_read", "_sql")
                               )
    errors = df_read2sql.id_uuid.isna()  # no id_uuid has been retrieved from a local entry name => join failed
    if errors.sum() != 0:
        success    = False
        outfile    = os.path.join(OUT, f"category_verify_read2sql_{ tablename }.xlsx")
        samplesize = min( len(df_read2sql.loc[ errors ].index), 5 )
        msg        = (f"\n* ON `{tablename}` : { errors.sum() } rows from `df_read` are "
                       "not found in `df_sql`! error sample: \n"
                       f"{ df_read2sql.loc[ errors ].sample(samplesize) }\n")
        print(msg)
        df_read2sql.loc[ errors ].to_excel( outfile )

    # 3) all rows of `df_sql` are present in `df_read`
    #    this is really what interests us
    df_sql2read = df_sql.merge( df_read
                              , how="left"
                              , on="entry_name"
                              , suffixes=("_sql", "_read")
                              )
    errors = df_sql2read.category.isna()  # no category has been retrieved from an entry name in the db => join failed
    if errors.sum() != 0:
        success    = False
        outfile    = os.path.join(OUT, f"category_verify_sql2read_{tablename}.xlsx")
        samplesize = min( len(df_sql2read.loc[ errors ].index), 5 )
        msg        = (f"\n* ON `{tablename}` : {errors.sum()} rows from `df_sql` "
                      "are not found in `df_read`! error sample:\n"
                      f"{ df_sql2read.loc[ errors ].sample(n=samplesize) }\n" )
        print(msg)
        df_sql2read.loc[ errors, ["entry_name", "category", "id_uuid", "id"] ].to_excel( outfile )

    # if no errors have been found (all items in df_sql have been matched
    # with a category in `df_read`), then create a new column `category_slug`
    df_sql2read["category_slug"] = df_sql2read.category.pipe(slugify_col)

    # finally, return
    return [ success, df_sql2read ]


def generate_sql(df:pd.DataFrame, tablename:str):
    """
    quick and dirty function to generate textual SQL queries to
    update `tablename` and  write the queries to OUT/

    our query does 3 things in 2 transactions:
    > transaction 1/
        create the columns `category` and `category_slug` of type `TEXT`
    > transaction 2/
        a. update rows by filling the `category` and `category_slug` columns
           based on the`id_uuid` <-> `category` mapping in `df`
        b. set a not-null constraint on the column

    2 variants of the query are created: a "test", that ends in a rollback,
    and a final query, with a "commit" at the end of the 2nd transaction.

    postgresql supports bulk updates but i can't find a way to compile
    bulk update params with sqlalchemy so we'll do 1 update / row instead

    input df structure:
    > id            int : SQL id for the row
    > id_uuid       str : SQL id_uuid for the row
    > entry_name    str : simplified entry name
    > category      str : the category to which this row belongs
    > category_slug str : a slugified, url-safe version of `category`, which will be used to build urls.


    see:
        https://docs.sqlalchemy.org/en/20/tutorial/data_update.html
        https://docs.sqlalchemy.org/en/20/faq/sqlexpressions.html

    :param df       : the dataframe to generate sql from
    :param tablename: the sql name for the talbe
    """
    # validate and prepare data
    if tablename not in [ "theme", "named_entity" ]:
        raise ValueError(f"generate_sql(): expected one of ['theme','named_entity'], got `{tablename}`")

    db_table = NamedEntity if tablename == "named_entity" else Theme
    mapper = df[[ "id_uuid", "category", "category_slug" ]].to_dict(orient="records")

    # sqlalchemy query generation + completing and cleaning.
    # 2 queries are generated: a test one (ends in a rollback)
    # and a final one (ends in a commit)
    updates = []
    for mapping in mapper:
        updates.append(update(db_table)
                      .where(db_table.id_uuid==mapping["id_uuid"])
                      .values(category=mapping["category"]))
        updates.append(update(db_table)
                      .where(db_table.id_uuid==mapping["id_uuid"])
                      .values(category_slug=mapping["category_slug"]))

    query = (f"""-- 1st transaction: create column
                 BEGIN;
                 ALTER TABLE IF EXISTS {tablename}
                 ADD COLUMN IF NOT EXISTS category TEXT,
                 ADD COLUMN IF NOT EXISTS category_slug TEXT;
                 COMMIT;

                 -- 2nd transaction: import data
                 BEGIN;""")

    try:
        query += "\n\n".join(str( u.compile( **COMPILE_PARAMS ))
                            for u in updates)
    except sa_exc.CompileError as e:
        raise sa_exc.CompileError(f"{e._message()}\nthe column to create must exist in your ORM !").with_traceback(e.__traceback__)

    query = re.sub(r"(UPDATE[^\n]+)", r"\1;", query)  # add `;` at the end of the query
    query += f"""\n--set not-null constraint after updating
                  -- this also allows to make sure that all
                  -- rows have a category before committing
                  ALTER TABLE IF EXISTS { tablename }
                  ALTER COLUMN category SET NOT NULL;\n"""

    test_query = sqlparse.format(query + " ROLLBACK; ", keyword="upper", reindent=True)
    query      = sqlparse.format(query + " COMMIT; ", keyword="upper", reindent=True)


    # write the queries to OUT/
    d = datetime.now().strftime(r"%Y%m%d")
    with open(os.path.join(OUT, f"{d}_category_{tablename}_update_test.sql"), mode="w") as fh:
        fh.write(test_query)
    with open(os.path.join(OUT, f"{d}_category_{tablename}_update.sql"), mode="w") as fh:
        fh.write(query)
    return


def pipeline(engine):
    """
    pipeline processing

    :param engine: the sqlalchemy engine (defined in ..main.cli())
    """

    # get and preprocess datasets
    df_sql_theme, df_sql_named_entity = sql_and_prepare(engine)
    df_theme, df_named_entity = read_and_prepare()

    # do the joins between local XSLX files and the sql tables
    success_theme, df_theme_join = join_and_verify(df_sql_theme, df_theme, "theme")
    success_named_entity, df_named_entity_join = join_and_verify(df_sql_named_entity, df_named_entity, "named_entity")
    if not success_theme or not success_named_entity:
        print("\n* a perfect join could not be made between input "
              "XSLX files and `theme` or `named_entity` SQL tables."
              "\n* check `OUT/category_verify_*.xslx` for errors.")
        exit(1)

    generate_sql(df_theme_join, "theme")                # type:ignore
    generate_sql(df_named_entity_join, "named_entity")  #type:ignore

    return



