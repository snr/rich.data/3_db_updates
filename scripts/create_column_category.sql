-- création des colonnes `category` sur les tables
-- `named_entity` et `theme`.
ALTER TABLE IF EXISTS theme
ADD COLUMN IF NOT EXISTS category VARCHAR(80);

ALTER TABLE IF EXISTS named_entity
ADD COLUMN IF NOT EXISTS category VARCHAR(80);

-- après avoir inséré les valeurs dans `category`, il
-- faudra ajouter une contrainte NOT NULL sur cette colonne.
-- requête:
--
-- ALTER TABLE IF EXISTS theme
-- ALTER COLUMN category SET not null;
--
-- ALTER TABLE IF EXISTS named_entity
-- ALTER COLUMN category SET not null;